# MIMOSAE documentation

About the model at runtime:

  * Y Wang, D. Conan, S. Chabridon, K. Bojnourdi, and J. Ma. Runtime models and evolution graphs for the version management of microservice architectures. In Proc. of the 28th Asia-Pacific Software Engineering Conference, Taipei, Taiwan, Virtually, December 2021. IEEE.
  * Accompanying [video](https://youtu.be/tgkX8ijU5K8) of the APSEC'2021 paper.
  * [`README` file](../Code/MIMOSAE/README.md) for compiling, installing, testing, etc. the model at runtime.

About the executor:

  * [`README` file](../Code/AutonomicManager/Executor/README.md) that describes the executor, and give instructions to install and test it.

