#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Julio Guzman

# This script delete old microservices images from minikube image repository

echo "===== deleting images ====="
if minikube image ls | grep us-v1-0-0-image:latest; then
  minikube image rm us-v1-0-0-image:latest
fi
if minikube image ls | grep ps-v1-0-0-image:latest; then
  minikube image rm ps-v1-0-0-image:latest
fi
if minikube image ls | grep perms-v1-0-0-image:latest; then
  minikube image rm perms-v1-0-0-image:latest
fi
if minikube image ls | grep auth-v1-0-0-image:latest; then
  minikube image rm auth-v1-0-0-image:latest
fi

# Eliminating the images based on the use case chosen 
if [ $# -eq 1 ]; then
    if [ $1 -eq 1 ]; then
        if minikube image ls | grep fs-v1-0-0-image:latest; then
          minikube image rm fs-v1-0-0-image:latest
        fi
    elif [ $1 -eq 2 ] || [ $1 -eq 3 ]; then
        if minikube image ls | grep fs-v1-1-0-image:latest; then
          minikube image rm fs-v1-1-0-image:latest
        fi
        if minikube image ls | grep ls-v1-0-0-image:latest; then
          minikube image rm ls-v1-0-0-image:latest
        fi
    fi
fi
echo "===== finish deleting images ====="

exit 0
