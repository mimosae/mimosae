#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Viktor Colas, Tsimafei Liashkevich

if ! ./scripts/waiting-for-response.sh "Are you sure you want to install minikube?"; then
    exit 0
fi

if [ -f "$HOME/.minikube" ]; then
    echo "Minikube is already installed"
    exit 2
fi

curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb

rm -f minikube_latest_amd64.deb

