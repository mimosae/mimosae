#! /bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Viktor Colas, Tsimafei Liashkevich

echo << EOF
Actions:
- install essential tools
- add pgp key + docker repository
- install docker
EOF

if ! ./scripts/waiting-for-response.sh "Are you sure you want to install Docker?"; then
    exit 0
fi

echo "===== installing essential tools to install Docker ====="
sudo apt install build-essential

sudo apt install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg \
     lsb-release

sudo apt update

echo "===== installing docker ====="
sudo apt-get install docker.io

sudo usermod -aG docker $USER

exit 0
