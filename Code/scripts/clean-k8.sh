#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Julio Guzman

# This script cleans Minikube of all Use Cases related items

echo "===== deleting deployments ====="
if minikube kubectl --  get deploy | grep auth-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy auth-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep perms-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy perms-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep ps-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy ps-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep us-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy us-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep fs-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy fs-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep dbus-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy dbus-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep dbperm-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy dbperm-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep dbps-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy dbps-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep dbfs-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy dbfs-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep fs-v1-1-0-deployment; then
    minikube kubectl -- delete -n default deploy fs-v1-1-0-deployment
fi
if minikube kubectl --  get deploy | grep ls-v1-0-0-deployment; then
    minikube kubectl -- delete -n default deploy ls-v1-0-0-deployment
fi
if minikube kubectl --  get deploy | grep pubsubconnector-v1-0-0; then
    minikube kubectl -- delete -n default deploy pubsubconnector-v1-0-0 
fi
echo "===== finish deleting deployments ====="

echo "===== deleting mimosae services ====="
if minikube kubectl --  get svc | grep authentication-service-app; then
    minikube kubectl -- delete -n default service authentication-service-app
fi
if minikube kubectl --  get svc | grep permission-service-app; then
    minikube kubectl -- delete -n default service permission-service-app
fi
if minikube kubectl --  get svc | grep permission-service-db; then
    minikube kubectl -- delete -n default service permission-service-db
fi
if minikube kubectl --  get svc | grep project-service-app; then
    minikube kubectl -- delete -n default service project-service-app
fi
if minikube kubectl --  get svc | grep project-service-db; then
    minikube kubectl -- delete -n default service project-service-db
fi
if minikube kubectl --  get svc | grep user-service-app ; then
    minikube kubectl -- delete -n default service user-service-app
fi
if minikube kubectl --  get svc | grep user-service-db; then
    minikube kubectl -- delete -n default service user-service-db
fi
if minikube kubectl --  get svc | grep file-service-app; then
    minikube kubectl -- delete -n default service file-service-app
fi
if minikube kubectl --  get svc | grep file-service-db; then
    minikube kubectl -- delete -n default service file-service-db
fi
if minikube kubectl --  get svc | grep logger-service; then
    minikube kubectl -- delete -n default service logger-service
fi
if minikube kubectl --  get svc | grep pubsubconnector-v1-0-0; then
    minikube kubectl -- delete -n default service pubsubconnector-v1-0-0
fi
echo "===== finish deleting mimosae services ====="

echo "===== deleting DB secrets ====="
if minikube kubectl --  get secret | grep dbfs-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret dbfs-v1-0-0-secret
fi
if minikube kubectl --  get secret | grep dbperm-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret dbperm-v1-0-0-secret
fi
if minikube kubectl --  get secret | grep dbps-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret dbps-v1-0-0-secret
fi
if minikube kubectl --  get secret | grep dbus-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret dbus-v1-0-0-secret
fi
if minikube kubectl --  get secret | grep perms-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret perms-v1-0-0-secret
fi
if minikube kubectl --  get secret | grep ps-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret ps-v1-0-0-secret
fi
if minikube kubectl --  get secret | grep us-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret us-v1-0-0-secret
fi
if minikube kubectl --  get secret | grep auth-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret auth-v1-0-0-secret
fi
if minikube kubectl --  get secret | grep fs-v1-1-0-secret; then
    minikube kubectl -- delete -n default secret fs-v1-1-0-secret
fi
if minikube kubectl --  get secret | grep pubsubconnector-v1-0-0-secret; then
    minikube kubectl -- delete -n default secret pubsubconnector-v1-0-0-secret 
fi
echo "===== finish deleting DB secrets ====="

echo "===== deleting DB persistent volumes claims ====="
if minikube kubectl --  get pvc | grep dbfs-v1-0-0-pvc; then
    minikube kubectl -- delete -n default persistentvolumeclaim dbfs-v1-0-0-pvc
fi
if minikube kubectl --  get pvc | grep dbperm-v1-0-0-pvc; then
    minikube kubectl -- delete -n default persistentvolumeclaim dbperm-v1-0-0-pvc
fi
if minikube kubectl --  get pvc | grep dbps-v1-0-0-pvc; then
    minikube kubectl -- delete -n default persistentvolumeclaim dbps-v1-0-0-pvc
fi
if minikube kubectl --  get pvc | grep dbus-v1-0-0-pvc; then
    minikube kubectl -- delete -n default persistentvolumeclaim dbus-v1-0-0-pvc
fi
echo "===== finish DB persistent volumes claims ====="

echo "===== deleting DB persistent volumes ====="
if minikube kubectl --  get pv | grep dbfs-v1-0-0-pv; then
    minikube kubectl -- delete -n default persistentvolume dbfs-v1-0-0-pv
fi
if minikube kubectl --  get pv | grep dbperm-v1-0-0-pv; then
    minikube kubectl -- delete -n default persistentvolume dbperm-v1-0-0-pv
fi
if minikube kubectl --  get pv | grep dbps-v1-0-0-pv; then
    minikube kubectl -- delete -n default persistentvolume dbps-v1-0-0-pv
fi
if minikube kubectl --  get pv | grep dbus-v1-0-0-pv; then
    minikube kubectl -- delete -n default persistentvolume dbus-v1-0-0-pv
fi
echo "===== finish DB persistent volumes ====="

exit 0
