#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Viktor Colas, Tsimafei Liashkevich, Denis Conan

if ! ./scripts/waiting-for-response.sh "Are you sure you want to install go and to configure it?"; then
    exit 0
fi

sudo apt install golang protobuf-compiler protoc-gen-go

export GOBIN=$HOME/go/bin
export GOPATH=$HOME/go

go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
go install github.com/golang/mock/mockgen@latest

if ./scripts/waiting-for-response.sh "Do you want to configure GOBIN and GOPATH to $HOME/.bashrc?"; then
    echo "export GOBIN=$HOME/go/bin" >> $HOME/.bashrc
    echo "export GOPATH=$HOME/go" >> $HOME/.bashrc
fi

exit 0
