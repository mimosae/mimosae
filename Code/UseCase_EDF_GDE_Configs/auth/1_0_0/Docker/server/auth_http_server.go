package server

import (
	"gde-common/common"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type AuthHTTPServer struct {
	Type string
}

func NewAuthHTTPServer(t string) *AuthHTTPServer {
	return &AuthHTTPServer{
		Type: t,
	}
}

func (hs *AuthHTTPServer) Starter(ip string, port string) {
	router := mux.NewRouter()

	router.Handle("/gde/" + common.AUTHENTICATION + "/login", LoginHandler()).Methods("POST")
	router.Handle("/gde/" + common.AUTHENTICATION + "/get_login", GetLoginHandler()).Methods("GET")

	log.Printf("http server serving on %s port %s", ip, port)
	log.Fatal(http.ListenAndServe(ip+":"+port, router))
}

