package server

import (
	"authentication-service/service"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

var SecretKey = []byte("private_key")

func LoginHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		as := service.AuthService{}
		var credentials service.Credentials
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&credentials); err != nil {
			log.Fatalf("decode command error")
		}
		defer r.Body.Close()
		ok, err := service.CheckPassword(credentials)
		if ok && err == nil {
			claims := service.NewClaims(credentials)
			token, err := as.EncodeJWT(string(SecretKey), claims)
			if err != nil {
				log.Fatal("Error encode JWT token")
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
			}
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(token))
		}
	})
}

func GetLoginHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		claims, err := AuthenticateJWTToken(string(SecretKey), r)
		if err != nil {
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
		login, ok := claims["login"].(string)
		if !ok {
			log.Fatal("login not exist in token")
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(login))
	})
}

func AuthenticateJWTToken(secretKey string, req *http.Request) (service.Claims, error) {
	as := service.AuthService{}
	jwtToken, err := ExtractJWTToken(req)
	if err != nil {
		return nil, fmt.Errorf("Failed get JWT token")
	}
	claims, err := as.ParseJWT(jwtToken, secretKey)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse token")
	}
	return claims, nil
}

func ExtractJWTToken(req *http.Request) (string, error) {
	tokenString := req.Header.Get("Authorization")
	if tokenString == "" {
		return "", fmt.Errorf("Could not find token")
	}
	tokenString, err := stripTokenPrefix(tokenString)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// strip token from authorization header
func stripTokenPrefix(tok string) (string, error) {
	tokenParts := strings.Split(tok, " ")
	if len(tokenParts) < 2 {
		return tokenParts[0], nil
	}
	return tokenParts[1], nil
}