module authentication-service

go 1.12

replace gde-common v0.0.0 => ./gde-common

require gde-common v0.0.0

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
)
