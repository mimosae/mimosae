package common

type DateComparator int

const (
	EQUAL         DateComparator = 1
	DIFFER        DateComparator = 2
	GREATER       DateComparator = 3
	LOWER         DateComparator = 4
	GREATER_EQUAL DateComparator = 5
	LOWER_EQUAL   DateComparator = 6
)
