package server

import (
	"encoding/json"
	"fmt"
	"gde-common/common"
	log "github.com/sirupsen/logrus"
	"net/http"
	"project-service/entities"
	"project-service/service"
)

func ProjectServiceProjectsHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ps service.ProjectService = &service.ProjectServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.PROJECT_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.CREATE_PROJECT:
					project := entities.Project{}
					json.Unmarshal(command.Data, &project)
					if result, err := ps.CreateProject(project); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_PROJECT:
					project := entities.Project{}
					json.Unmarshal(command.Data, &project)
					if result, err := ps.UpdateProject(project); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_PROJECT:
					projectId, err := command.GetLong("project_id")
					if err = ps.DeleteProject(projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.RESTORE_PROJECT:
					projectId, err := command.GetLong("project_id")
					if err = ps.RestoreProject(projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_PROJECT_BY_ID:
					projectId, _ := command.GetLong("project_id")
					if result, err := ps.FindProjectById(projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_NAME:
					projectName, _ := command.GetString("project_name")
					if result, err := ps.FindProjectsByName(projectName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_CREATTION_DATE:
					date, _ := command.GetTime("created_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := ps.FindProjectsByCreationDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_DELETION_DATE:
					date, _ := command.GetTime("deleted_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := ps.FindProjectsByDeletionDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_UPDATE_DATE:
					date, _ := command.GetTime("updated_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := ps.FindProjectsByUpdateDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_RESTORATION_DATE:
					date, _ := command.GetTime("restorated_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := ps.FindProjectsByRestorationDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_PROJECTS:
					if result, err := ps.ListProjects(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.PROJECT_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func ProjectServiceProjectLockHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ps service.ProjectService = &service.ProjectServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.PROJECT_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {

				switch command.MethodName {
				case common.GET_PROJECT_LOCKER:
					projectId, _ := command.GetLong("project_id")
					if result, err := ps.GetProjectLocker(projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.IS_PROJECT_LOCKED:
					projectId, _ := command.GetLong("project_id")
					// userId, _ := command.GetLong("user_id")
					userId, _ := service.FindUserIDByName(login, token)
					if result, err := ps.IsProjectLocked(userId, projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.SET_PROJECT_LOCK_STATE:
					projectId, _ := command.GetLong("project_id")
					state, _ := command.GetBool("lock")
					// userId, _ := command.GetLong("user_id")
					userId, _ := service.FindUserIDByName(login, token)
					if err := ps.SetProjectLockState(userId, projectId, state); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.PROJECT_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func ProjectServiceProjectFilesHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ps service.ProjectService = &service.ProjectServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.PROJECT_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {

				switch command.MethodName {
				case common.LIST_PROJECT_FILES: // request file service
					projectId, _ := command.GetLong("project_id")
					if result, err := ps.ListProjectFiles(projectId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.ATTACH_FILE_TO_PROJECT: // request file service
					projectId, _ := command.GetLong("project_id")
					fileId, _ := command.GetLong("file_id")
					// userId, _ := command.GetLong("user_id")
					userId, _ := service.FindUserIDByName(login, token)
					if err := ps.AttachFileToProject(userId, projectId, fileId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.DETACH_FILE_FROM_PROJECT: // request file service
					projectId, _ := command.GetLong("project_id")
					fileId, _ := command.GetLong("file_id")
					// userId, _ := command.GetLong("user_id")
					userId, _ := service.FindUserIDByName(login, token)
					if err := ps.DetachFileFromProject(userId, projectId, fileId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.PROJECT_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

/*
func ProjectServiceHandler()  http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var ps service.ProjectService = &service.ProjectServiceImpl{}

		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}

		// authentication
		token := common.GetToken(r)
		login, err := common.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := common.CheckPermission(login, common.PROJECT_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {

				switch command.MethodName {
				case common.CREATE_PROJECT:
					project := entities.Project{}
					json.Unmarshal(command.Data, &project)
					if result, err := ps.CreateProject(project); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_PROJECT:
					project := entities.Project{}
					json.Unmarshal(command.Data, &project)
					if result, err := ps.UpdateProject(project); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_PROJECT:
					projectId, err := command.GetLong("project_id")
					if err = ps.DeleteProject(projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.RESTORE_PROJECT:
					projectId, err := command.GetLong("project_id")
					if err = ps.RestoreProject(projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_PROJECT_BY_ID:
					projectId, _ := command.GetLong("project_id")
					if result, err := ps.FindProjectById(projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_NAME:
					projectName, _ := command.GetString("project_name")
					if result, err := ps.FindProjectsByName(projectName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_CREATTION_DATE:
					date, _ := command.GetTime("created_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := ps.FindProjectsByCreationDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_DELETION_DATE:
					date, _ := command.GetTime("deleted_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := ps.FindProjectsByDeletionDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_UPDATE_DATE:
					date, _ := command.GetTime("updated_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := ps.FindProjectsByUpdateDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PROJECTS_BY_RESTORATION_DATE:
					date, _ := command.GetTime("restorated_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := ps.FindProjectsByRestorationDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_PROJECTS:
					if result, err := ps.ListProjects(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.GET_PROJECT_LOCKER:
					projectId, _ := command.GetLong("project_id")
					if result, err := ps.GetProjectLocker(projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.IS_PROJECT_LOCKED:
					projectId, _ := command.GetLong("project_id")
					// userId, _ := command.GetLong("user_id")
					userId, _ := common.FindUserIDByName(login, token)
					if result, err := ps.IsProjectLocked(userId, projectId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.SET_PROJECT_LOCK_STATE:
					projectId, _ := command.GetLong("project_id")
					state, _ := command.GetBool("lock")
					// userId, _ := command.GetLong("user_id")
					userId, _ := common.FindUserIDByName(login, token)
					if err := ps.SetProjectLockState(userId, projectId, state); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.LIST_PROJECT_FILES: // request file service
					projectId, _ := command.GetLong("project_id")
					if result, err := ps.ListProjectFiles(projectId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.ATTACH_FILE_TO_PROJECT: // request file service
					projectId, _ := command.GetLong("project_id")
					fileId, _ := command.GetLong("file_id")
					// userId, _ := command.GetLong("user_id")
					userId, _ := common.FindUserIDByName(login, token)
					if err := ps.AttachFileToProject(userId, projectId, fileId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.DETACH_FILE_FROM_PROJECT: // request file service
					projectId, _ := command.GetLong("project_id")
					fileId, _ := command.GetLong("file_id")
					// userId, _ := command.GetLong("user_id")
					userId, _ := common.FindUserIDByName(login, token)
					if err := ps.DetachFileFromProject(userId, projectId, fileId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.USER_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}
 */