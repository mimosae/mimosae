package server

type ProjectServer interface {
	Starter(ip string, port string)
}
