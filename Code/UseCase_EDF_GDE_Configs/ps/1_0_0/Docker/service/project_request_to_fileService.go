package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

func GetFileById(fileId int64, token string) (common.GDEFileTO, error) {
	log.Printf("get file by id %v ...", fileId)
	fileServiceHost := os.Getenv("FileServiceHost")
	if fileServiceHost == "" {
		fileServiceHost = common.LocalHostIP
	}
	fileServicePort := os.Getenv("FileServicePort")
	if fileServicePort == "" {
		fileServicePort = common.FileServicePort
	}
	url := fmt.Sprintf("http://%s:%s/gde/%s/files", fileServiceHost, fileServicePort, common.FILE_SERVICE)
	client := &http.Client{Timeout: time.Minute}
	command := common.NewCommand(common.FIND_FILE_BY_ID)
	command.SetLong("file_id", fileId)
	body, _ := json.Marshal(command)
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Authorization", token) // important!!!
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		file := common.GDEFileTO{}
		err := json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&file)
		return file, err
	} else {
		return common.GDEFileTO{}, fmt.Errorf(result.Message)
	}
}

func CheckFileIsValid(fileId int64, token string) (bool, error) {
	log.Printf("check file %v is valid ...", fileId)
	fileServiceHost := os.Getenv("FileServiceHost")
	if fileServiceHost == "" {
		fileServiceHost = common.LocalHostIP
	}
	fileServicePort := os.Getenv("FileServicePort")
	if fileServicePort == "" {
		fileServicePort = common.FileServicePort
	}
	url := fmt.Sprintf("http://%s:%s/gde/%s/files", fileServiceHost, fileServicePort, common.FILE_SERVICE)
	client := &http.Client{Timeout: time.Minute}
	command := common.NewCommand(common.IS_FILE_VALID)
	command.SetLong("file_id", fileId)
	body, _ := json.Marshal(command)
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Authorization", token) // important!!!
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		return true, nil
	} else {
		return false, fmt.Errorf(result.Message)
	}
}
