package service

import (
	"gde-common/common"
	"project-service/entities"
	"time"
)

type ProjectService interface {
	CreateProject(p entities.Project) (*entities.Project, error)
	UpdateProject(p entities.Project) (*entities.Project, error)
	DeleteProject(id int64) error
	RestoreProject(id int64) error

	FindProjectById(id int64) (entities.Project, error)
	FindProjectsByName(name string) ([]entities.Project, error)
	FindProjectsByCreationDate(creationDate *time.Time, comparator common.DateComparator) ([]entities.Project, error)
	FindProjectsByDeletionDate(deletionDate *time.Time, comparator common.DateComparator) ([]entities.Project, error)
	FindProjectsByUpdateDate(updateDate *time.Time, comparator common.DateComparator) ([]entities.Project, error)
	FindProjectsByRestorationDate(restorationDate *time.Time, comparator common.DateComparator) ([]entities.Project, error)
	ListProjects() ([]entities.Project, error)

	GetProjectLocker(projectId int64) (int64, error)
	IsProjectLocked(userId, projectId int64) (bool, error)
	SetProjectLockState(userId int64, projectId int64, locked bool) error

	// Project-File association
	ListProjectFiles(projectId int64, token string) ([]common.GDEFileTO, error)
	AttachFileToProject(userId int64, projectId int64, fileId int64, token string) error //valid file existe -> file_service
	DetachFileFromProject(userId int64, projectId int64, fileId int64, token string) error

}
