module project-service

go 1.12

replace gde-common v0.0.0 => ./gde-common

require (
	gde-common v0.0.0
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.1.1
	github.com/sirupsen/logrus v1.7.0
)
