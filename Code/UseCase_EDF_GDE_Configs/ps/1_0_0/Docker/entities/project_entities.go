package entities

import (
	"gde-common/common"
	"time"
)

type Project struct {
	ID           int64              `json:"project_id" gorm:"primary_key;AUTO_INCREMENT"`
	Name         string             `json:"project_name"`
	Attributes   []common.Attribute `json:"project_attributes" gorm:"type:jsonb"`
	Deleted      bool               `json:"deleted"`
	CreatedAt    time.Time          `json:"created_at"`
	UpdatedAt    time.Time          `json:"updated_at"`
	DeletedAt    *time.Time         `json:"deleted_at"`
	RestoratedAt time.Time          `json:"restorated_at"`
	Files        []int64      `json:"files"  gorm:"type:integer[]"` //id
	Locker       int64              `json:"locker" gorm:"default:-1"`
}
