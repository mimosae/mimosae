package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/streadway/amqp"
)

const connectionRetry = 10

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	url := fmt.Sprintf("amqp://%s:%s@%s:%s/", os.Getenv("LOGGER_RABBITMQ_USERNAME"), os.Getenv("LOGGER_RABBITMQ_PASSWORD"), os.Getenv("LOGGER_RABBITMQ_HOST"), os.Getenv("LOGGER_RABBITMQ_PORT"))
	fmt.Println(url)
	for retry := 0; retry <= connectionRetry; retry++ {
		_, err := amqp.Dial(url)
		fmt.Println(err)
		if err == nil {
			break
		}
		time.Sleep(time.Second * 5)
	}
	conn, err := amqp.Dial(url)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"log",
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"",
		true,
		false,
		true,
		false,
		nil,
	)

	err = ch.QueueBind(
		q.Name,
		"logger.#",
		"log",
		false,
		nil,
	)
	failOnError(err, "Failed to bind a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto ack
		true,   // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf(" [x] %s", d.Body)
			fmt.Printf(" [x] %s", d.Body)
		}
	}()

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever
}
