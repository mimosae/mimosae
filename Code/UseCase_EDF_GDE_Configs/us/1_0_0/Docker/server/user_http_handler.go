package server

import (
	"encoding/json"
	"fmt"
	"gde-common/common"
	log "github.com/sirupsen/logrus"
	"net/http"
	"user-service/entities"
	"user-service/service"
)

func UserServiceUsersHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var us service.UserService = &service.UserServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.USER_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.CREATE_USER: // userName string, password string
					userName, _ := command.GetString("user_name")
					pwd, _ := command.GetString("user_password")
					if result, err := us.CreateUser(userName, pwd); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CREATE_USER_BY_USER:
					user := entities.User{}
					json.Unmarshal(command.Data, &user)
					if result, err := us.CreateUserByUser(&user); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_USER:
					userId, err := command.GetLong("user_id")
					if err = us.DeleteUser(userId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_USER_BY_ID:
					userId, _ := command.GetLong("user_id")
					if result, err := us.FindUserByID(userId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_USER_BY_NAME:
					userName, _ := command.GetString("user_name")
					if result, err := us.FindUserByName(userName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_USERS:
					if result, err := us.ListUsers(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.USER_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func UserServiceGroupsHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var us service.UserService = &service.UserServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.USER_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.CREATE_GROUP: // groupName string
					groupName, _ := command.GetString("group_name")
					if result, err := us.CreateGroup(groupName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CREATE_GROUP_BY_GROUP:
					group := entities.Group{}
					json.Unmarshal(command.Data, &group)
					if result, err := us.CreateGroupByGroup(&group); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_GROUP:
					groupId, err := command.GetLong("group_id")
					if err = us.DeleteGroup(groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_GROUP_BY_ID:
					groupId, _ := command.GetLong("group_id")
					if result, err := us.FindGroupByID(groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_GROUPS_BY_NAME:
					groupName, _ := command.GetString("group_name")
					if result, err := us.FindGroupsByName(groupName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_GROUPS:
					if result, err := us.ListGroups(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.USER_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func UserServiceUsersGroupsHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var us service.UserService = &service.UserServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.USER_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.ADD_USER_TO_GROUP:
					userId, _ := command.GetLong("user_id")
					groupId, _ := command.GetLong("group_id")
					if err = us.AddUserToGroup(userId, groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.REMOVE_USER_FROM_GROUP:
					userId, _ := command.GetLong("user_id")
					groupId, _ := command.GetLong("group_id")
					if err = us.RemoveUserFromGroup(userId, groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.IS_USER_IN_GROUP:
					userId, err := command.GetLong("user_id")
					groupId, err := command.GetLong("group_id")
					if result := us.IsUserInGroup(userId, groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_USERS_IN_GROUP:
					groupId, _ := command.GetLong("group_id")
					if result, err := us.ListUsersInGroup(groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_GROUPS_OF_USER:
					userId, _ := command.GetLong("user_id")
					if result, err := us.ListGroupsOfUser(userId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.USER_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func UserServiceCheckPasswordHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var us service.UserService = &service.UserServiceImpl{}
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		if command.MethodName == common.CHECK_PASSWORD {
			login, _ := command.GetString("login")
			pwd, _ := command.GetString("password")
			if result, err := us.CheckPassword(login, pwd); err != nil {
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				resp.Data, _ = json.Marshal(result)
				resp.Code = common.OK
			}
		} else {
			err := fmt.Errorf("invalid method %v : %v", common.USER_SERVICE, command.MethodName)
			resp.Code = common.ERROR
			resp.Message = err.Error()
		}
		SendResult(w, resp)
	})
}

func UserServiceWithoutCheckPermissionHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var us service.UserService = &service.UserServiceImpl{}
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		_, err = service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else {
			switch command.MethodName {
			case common.FIND_USER_BY_NAME:
				userName, _ := command.GetString("user_name")
				if result, err := us.FindUserByName(userName); err != nil {
					resp.Code = common.ERROR
					resp.Message = err.Error()
				} else {
					resp.Data, _ = json.Marshal(result)
					resp.Code = common.OK
				}
			case common.FIND_GROUP_BY_ID:
				groupId, _ := command.GetLong("group_id")
				if result, err := us.FindGroupByID(groupId); err != nil {
					resp.Code = common.ERROR
					resp.Message = err.Error()
				} else {
					resp.Data, _ = json.Marshal(result)
					resp.Code = common.OK
				}
			case common.LIST_GROUPS_OF_USER:
				userId, _ := command.GetLong("user_id")
				if result, err := us.ListGroupsOfUser(userId); err != nil {
					resp.Code = common.ERROR
					resp.Message = err.Error()
				} else {
					resp.Data, _ = json.Marshal(result)
					resp.Code = common.OK
				}
			}
		}
		SendResult(w, resp)
	})

}

/*
func UserServiceHandler()  http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var us service.UserService = &service.UserServiceImpl{}

		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}

		// authentication
		token := common.GetToken(r)
		login, err := common.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := common.CheckPermission(login, common.USER_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {

				switch command.MethodName {
				case common.CREATE_USER: // userName string, password string
					userName, _ := command.GetString("user_name")
					pwd, _ := command.GetString("user_password")
					if result, err := us.CreateUser(userName, pwd); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CREATE_USER_BY_USER:
					user := entities.User{}
					json.Unmarshal(command.Data, &user)
					if result, err := us.CreateUserByUser(&user); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_USER:
					userId, err := command.GetLong("user_id")
					if err = us.DeleteUser(userId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_USER_BY_ID:
					userId, _ := command.GetLong("user_id")
					if result, err := us.FindUserByID(userId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_USER_BY_NAME:
					userName, _ := command.GetString("user_name")
					if result, err := us.FindUserByName(userName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_USERS:
					if result, err := us.ListUsers(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CREATE_GROUP: // groupName string
					groupName, _ := command.GetString("group_name")
					if result, err := us.CreateGroup(groupName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CREATE_GROUP_BY_GROUP:
					group := entities.Group{}
					json.Unmarshal(command.Data, &group)
					if result, err := us.CreateGroupByGroup(&group); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_GROUP:
					groupId, err := command.GetLong("group_id")
					if err = us.DeleteGroup(groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_GROUP_BY_ID:
					groupId, _ := command.GetLong("group_id")
					if result, err := us.FindGroupByID(groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_GROUPS_BY_NAME:
					groupName, _ := command.GetString("group_name")
					if result, err := us.FindGroupsByName(groupName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_GROUPS:
					if result, err := us.ListGroups(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.ADD_USER_TO_GROUP:
					userId, _ := command.GetLong("user_id")
					groupId, _ := command.GetLong("group_id")
					if err = us.AddUserToGroup(userId, groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.REMOVE_USER_FROM_GROUP:
					userId, _ := command.GetLong("user_id")
					groupId, _ := command.GetLong("group_id")
					if err = us.RemoveUserFromGroup(userId, groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.IS_USER_IN_GROUP:
					userId, err := command.GetLong("user_id")
					groupId, err := command.GetLong("group_id")
					if result := us.IsUserInGroup(userId, groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_USERS_IN_GROUP:
					groupId, _ := command.GetLong("group_id")
					if result, err := us.ListUsersInGroup(groupId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_GROUPS_OF_USER:
					userId, _ := command.GetLong("user_id")
					if result, err := us.ListGroupsOfUser(userId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CHECK_PASSWORD:
					login, _ := command.GetString("login")
					pwd, _ := command.GetString("password")
					if result, err := us.CheckPassword(login, pwd); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.USER_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}
*/