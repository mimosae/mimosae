package main

import (
	"os"
	"user-service/config"
	"user-service/db"
	"user-service/server"
)

func main() {
	server := server.NewUserHTTPServer(config.DefaultProtocol)
	db.DB = &db.UserPostgresDB{}
	ip := os.Getenv("IP")
	if ip == "" {
		ip = config.DefaultIP
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = config.DefaultPort
	}
	server.Starter(ip, port)
}
