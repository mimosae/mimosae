package db

import (
	"user-service/entities"
)

var DB UserDatabase

//UserDatabase ...
type UserDatabase interface {
	ConnectDB()
	InitDB()
	Close()

	// User
	FindUsers() ([]entities.User, error)
	InsertUser(user *entities.User) (*entities.User, error)
	DeleteUser(id int64) error
	FindUserByID(id int64) (entities.User, error)
	FindUserByName(name string) (entities.User, error)

	CheckPassword(login, password string) (bool, error)

	// Group
	FindGroups() ([]entities.Group, error)
	InsertGroup(group *entities.Group) (*entities.Group, error)
	DeleteGroup(id int64) error
	FindGroupByID(id int64) (entities.Group, error)
	FindGroupByName(name string) ([]entities.Group, error)

	//User and group
	AddUserToGroup(userID int64, groupID int64) error
	AddUsersToGroup(userIDs []int64, groupID int64) error
	RemoveUserFromGroup(userID int64, groupID int64) error
	IsUserInGroup(userID int64, groupID int64) bool
	ListUsersInGroup(groupID int64) ([]entities.User, error)
	ListGroupsOfUser(userID int64) ([]entities.Group, error)
}
