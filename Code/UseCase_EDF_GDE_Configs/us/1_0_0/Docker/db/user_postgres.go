package db

import (
	"fmt"
	"os"
	"user-service/config"
	"user-service/entities"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //...
	log "github.com/sirupsen/logrus"
)

//UserPostgresDB repository of postgresql database
type UserPostgresDB struct {
	db *gorm.DB
}

//GetDB ...
func (pg *UserPostgresDB) GetDB() *gorm.DB {
	return pg.db
}

//ConnectDB connect to postgres database
func (pg *UserPostgresDB) ConnectDB() {
	var err error
	dbHost := os.Getenv("DBHOST")
	if dbHost == "" {
		dbHost = config.DbHost
	}
	dbPort := os.Getenv("DBPORT")
	if dbPort == "" {
		dbPort = config.DbPort
	}
	dbUser := os.Getenv("DBUSER")
	if dbUser == "" {
		dbUser = config.DbUser
	}
	dbName := os.Getenv("DBNAME")
	if dbName == "" {
		dbName = config.DbName
	}
	dbPassword := os.Getenv("DBPASSWORD")
	if dbPassword == "" {
		dbPassword = config.DbPassword
	}
	dbURI := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		dbUser,
		dbPassword,
		dbHost,
		dbPort,
		dbName)
	pg.db, err = gorm.Open("postgres", dbURI)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("User Service Postgres opened ...")
}

//InitDB ...
func (pg *UserPostgresDB) InitDB() {
	if err := pg.db.DropTableIfExists(&entities.User{}, &entities.Group{}, "user_groups").Error; err != nil {
		log.Fatal("dropTableIfExist fails: ", err)
	}

	if err := pg.db.AutoMigrate(&entities.User{}, &entities.Group{}).Error; err != nil {
		log.Fatal("automigrate fails: ", err)
	}

	InitUserDatabase(pg)
}

//Close postgres database
func (pg *UserPostgresDB) Close() {
	pg.db.Close()
}

/* User */

//InsertUser ...
func (pg *UserPostgresDB) InsertUser(user *entities.User) (*entities.User, error) {
	log.Println("Creating User ...")
	if err := pg.db.Create(&user).Error; err != nil {
		return nil, fmt.Errorf("insert into db fail: %v", err)
	}
	return user, nil
}

//DeleteUser ...
func (pg *UserPostgresDB) DeleteUser(id int64) error {
	log.Printf("Deleting User %v... \n", id)
	if err := pg.db.Where("id = ?", id).Delete(&entities.User{}).Error; err != nil {
		log.Errorf("delete user by id %d error", id)
		return err
	}
	return nil
}

//FindUserByID ...
func (pg *UserPostgresDB) FindUserByID(id int64) (entities.User, error) {
	log.Printf("Finding user %v... \n", id)
	user := entities.User{}
	if err := pg.db.Preload("Groups").Where(&entities.User{ID: id}).Take(&user).Error; err != nil {
		log.Errorf("Error finding user")
		return entities.User{}, err
	}
	return user, nil
}

//FindUserByName ...
func (pg *UserPostgresDB) FindUserByName(name string) (entities.User, error) {
	log.Printf("Finding user named %v... \n", name)
	user := entities.User{}
	if err := pg.db.Preload("Groups").Where(&entities.User{Name: name}).Take(&user).Error; err != nil {
		log.Errorf("Error finding user with this name")
		return entities.User{}, err
	}
	return user, nil
}

//FindUsers ...
func (pg *UserPostgresDB) FindUsers() ([]entities.User, error) {
	log.Println("Listing All Users ...")
	var list []entities.User
	if err := pg.db.Preload("Groups").Find(&list).Error; err != nil {
		log.Errorf("empty user list")
		return nil, err
	}
	return list, nil
}

/* Group */

//InsertGroup ...
func (pg *UserPostgresDB) InsertGroup(group *entities.Group) (*entities.Group, error) {
	log.Println("Creating Group ...")
	if err := pg.db.Create(&group).Error; err != nil {
		return nil, fmt.Errorf("insert into db fail: %v", err)
	}
	return group, nil
}

//DeleteGroup ...
func (pg *UserPostgresDB) DeleteGroup(id int64) error {
	log.Printf("Deleting Group %v... \n", id)
	if err := pg.db.Where("id = ?", id).Delete(&entities.Group{}).Error; err != nil {
		log.Errorf("delete group by id %d error", id)
	}
	return nil
}

//FindGroupByID ...
func (pg *UserPostgresDB) FindGroupByID(id int64) (entities.Group, error) {
	log.Printf("Finding group %v... \n", id)
	group := entities.Group{}
	if err := pg.db.Preload("Users").Where(&entities.Group{ID: id}).Take(&group).Error; err != nil {
		log.Errorf("Error finding group")
		return entities.Group{}, err
	}
	return group, nil
}

//FindGroupByName ...
func (pg *UserPostgresDB) FindGroupByName(name string) ([]entities.Group, error) {
	log.Printf("Finding group named %v... \n", name)
	var groups []entities.Group
	if err := pg.db.Preload("Users").Where(&entities.Group{Name: name}).Find(&groups).Error; err != nil {
		log.Errorf("Error finding group with this name")
		return nil, err
	}
	return groups, nil
}

//FindGroups ...
func (pg *UserPostgresDB) FindGroups() ([]entities.Group, error) {
	log.Println("Listing All Groups ...")
	var list []entities.Group
	if err := pg.db.Preload("Users").Find(&list).Error; err != nil {
		log.Errorf("empty group list")
	}
	return list, nil
}

/* User and Group */
//AddUserToGroup ...
func (pg *UserPostgresDB) AddUserToGroup(userID int64, groupID int64) error {
	log.Printf("Adding user %v to group %v... \n", userID, groupID)
	user, err := pg.FindUserByID(userID)
	if err != nil {
		return err
	}
	group, err := pg.FindGroupByID(groupID)
	if err != nil {
		return err
	}
	err = pg.db.Model(&group).Association("Users").Append([]entities.User{user}).Error
	if err != nil {
		return err
	}
	return nil
}

//AddUsersToGroup ...
func (pg *UserPostgresDB) AddUsersToGroup(userIDs []int64, groupID int64) error {
	for _, userID := range userIDs {
		err := pg.AddUserToGroup(userID, groupID)
		if err != nil {
			return err
		}
	}
	return nil
}

//RemoveUserFromGroup ...
func (pg *UserPostgresDB) RemoveUserFromGroup(userID int64, groupID int64) error {
	log.Printf("Removing user %v from group %v... \n", userID, groupID)
	user, err := pg.FindUserByID(userID)
	if err != nil {
		return err
	}
	group, err := pg.FindGroupByID(groupID)
	if err != nil {
		return err
	}
	err = pg.db.Model(&group).Association("Users").Delete([]entities.User{user}).Error
	if err != nil {
		return err
	}
	return nil
}

//IsUserInGroup ...
func (pg *UserPostgresDB) IsUserInGroup(userID int64, groupID int64) bool {
	user, err := pg.FindUserByID(userID)
	if err != nil {
		return false
	}
	group, err := pg.FindGroupByID(groupID)
	if err != nil {
		return false
	}
	users := []entities.User{}
	pg.db.Model(&group).Association("Users").Find(&users)
	for _, item := range users {
		if item.ID == user.ID && item.Name == user.Name && item.Password == user.Password {
			return true
		}
	}

	return false
}

//ListUsersInGroup ...
func (pg *UserPostgresDB) ListUsersInGroup(groupID int64) ([]entities.User, error) {
	group, err := pg.FindGroupByID(groupID)
	if err != nil {
		return nil, err
	}
	users := []entities.User{}
	err = pg.db.Model(&group).Association("Users").Find(&users).Error
	if err != nil {
		return nil, err
	}
	return users, nil
}

//ListGroupsOfUser ...
func (pg *UserPostgresDB) ListGroupsOfUser(userID int64) ([]entities.Group, error) {
	user, err := pg.FindUserByID(userID)
	if err != nil {
		return nil, err
	}
	groups := []entities.Group{}
	err = pg.db.Model(&user).Association("Groups").Find(&groups).Error
	if err != nil {
		return nil, err
	}
	return groups, nil
}

func (pg *UserPostgresDB) CheckPassword(login, password string) (bool,error) {
	user, err := pg.FindUserByName(login)
	if err != nil {
		log.Errorf("Error username")
		return false, err
	}
	if password == user.Password {
		return true, nil
	} else {
		log.Errorf("Error password")
		return false, fmt.Errorf("error password")
	}
}
