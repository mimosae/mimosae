package db

import (
	"gde-common/common"
	"log"
	"user-service/entities"
)

func InitUserDatabase(pg *UserPostgresDB) {
	log.Printf("Init User Database ...")

	testUser := &entities.User{
		Name: common.TestLogin,
		Password: common.TestPassword,
	}
	testGroup := &entities.Group{
		Name: "test-group",
	}
	testUser, _ = pg.InsertUser(testUser)
	testGroup, _ = pg.InsertGroup(testGroup)
	pg.AddUserToGroup(testUser.ID, testGroup.ID)
}
