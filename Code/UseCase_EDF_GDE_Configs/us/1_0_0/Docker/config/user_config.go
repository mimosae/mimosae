package config

import "gde-common/common"

//DefaultPort is defaut server post number for connection
const DefaultPort = common.UserServicePort

//DefaultProtocol is defaut protocol used in communcation
const DefaultProtocol = "HTTP"

//DefaultIP is default ip
const DefaultIP = "127.0.0.1"

//DbUser user of db
const DbUser string = "postgres"

//DbPassword password of db
const DbPassword string = "postgres"

//DbName name of db
const DbName string = "gde_user_service"

//DbHost host of db
const DbHost string = "127.0.0.1"

//DbPort port of db
const DbPort string = "5432"
