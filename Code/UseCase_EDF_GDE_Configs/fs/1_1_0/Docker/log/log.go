package log

import (
	"fmt"
	"os"
	"time"

	"github.com/streadway/amqp"
)

const connectionRetry = 10

var connection *amqp.Connection
var channel *amqp.Channel

type HeaderLog string

func headerToString(level HeaderLog) string {
	return string(level)
}

func ConnectToLogger() (err error) {
	if connection != nil {
		return
	}
	username := chooseValue(os.Getenv("LOGGER_RABBITMQ_USERNAME"), "guest")
	password := chooseValue(os.Getenv("LOGGER_RABBITMQ_PASSWORD"), "guest")
	host := chooseValue(os.Getenv("LOGGER_RABBITMQ_HOST"), "hostname")
	port := chooseValue(os.Getenv("LOGGER_RABBITMQ_PORT"), "5672")
	url := fmt.Sprintf("amqp://%s:%s@%s:%s/", username, password, host, port)
	retry := 0
	for {
		connection, err = amqp.Dial(url)
		if err == nil {
			break
		}
		if retry >= connectionRetry {
			return
		}
		fmt.Println(err)
		time.Sleep(time.Second * 5)
		retry++
	}

	channel, err = connection.Channel()
	return
}

func chooseValue(value string, defaultValue string) string {
	if value == "" {
		return defaultValue
	}
	return value
}

func CloseLoggerConnection() {
	channel.Close()
	connection.Close()
}

func DefineLogExchange() {
	err := channel.ExchangeDeclare(
		"log",
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		fmt.Printf("Can't declare exchange due to %v\n", err)
		return
	}
}

func log(message string) {

	if connection == nil || channel == nil {
		fmt.Println("----- logger is disconnected -----")

	}

	err := channel.Publish(
		"log",
		"logger.fs.log",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("file-service: " + message),
		})
	if err != nil {
		fmt.Printf("Can't publish logs due to %v\n", err)
		return
	}
}

func Log(message string) {
	log(message)
}

func Logf(format string, a ...interface{}) {
	message := fmt.Sprintf(format, a)
	log(message)
}
