module gde-common

require (
	usecase/log v0.0.0
)

replace (
	usecase/log v0.0.0 => ../log
)

go 1.12
