package common

import (
	"time"
)

// User Service
type UserTO struct {
	ID         int64       			`json:"user_id"`
	Name       string      			`json:"user_name"`
	Password   string      			`json:"user_password"`
	Attributes []Attribute 			`json:"user_attributes"`
	Groups     []GroupTO     		`json:"groups"`
}

type GroupTO struct {
	ID         int64       			`json:"group_id"`
	Name       string      			`json:"group_name"`
	Attributes []Attribute 			`json:"group_attributes"`
	Users      []UserTO      		`json:"users"`
}

// Project Service
type ProjectTO struct {
	ID           int64              `json:"project_id"`
	Name         string             `json:"project_name"`
	Attributes   []Attribute 		`json:"project_attributes"`
	Deleted      bool               `json:"deleted"`
	CreatedAt    time.Time          `json:"created_at"`
	UpdatedAt    time.Time          `json:"updated_at"`
	DeletedAt    *time.Time         `json:"deleted_at"`
	RestoratedAt time.Time          `json:"restorated_at"`
	Files        []int64      		`json:"files"` //id
	Locker       int64              `json:"locker"`
}

// File Service
type GDEFileTO struct {
	ID       int64 					`json:"file_id"`
	Name     string 				`json:"file_name"`
	Attributes []Attribute 			`json:"file_attributes"`
	Size int64 						`json:"file_size"`
	Checksum string 				`json:"file_checksum"`
	Valid bool 						`json:"file_valid"`
	CreatedAt time.Time				`json:"created_at"`
	UpdatedAt time.Time				`json:"updated_at"`
	DeletedAt *time.Time 			`json:"deleted_at"`
	RestoratedAt time.Time 			`json:"restorated_at"`
	Indexed bool 					`json:"file_indexed"`
	Chunks []ChunkTO 				`json:"chunks"`
}

type ChunkTO struct {
	ID   int64 						`json:"chunk_id"`
	Rank int64 						`json:"chunk_rank"`
	Checksum string 				`json:"chunk_checksum"`
	Size int64 						`json:"chunk_size"`
	Data []byte 					`json:"chunk_data"`
}

type ChunkInfoTO struct {
	ID   int64 						`json:"chunk_id"`
	Rank int64 						`json:"chunk_rank"`
	Checksum string 				`json:"chunk_checksum"`
	Size int64 						`json:"chunk_size"`
}

// Permission Service
type PermissionTO struct {
	ID int64 						`json:"permission_id"`
	GroupID int64 					`json:"group_id"`
	MethodID int64 					`json:"method_id"`
	CreatedAt time.Time				`json:"created_at"`
	UpdatedAt time.Time				`json:"updated_at"`
	DeletedAt *time.Time 			`json:"deleted_at"`
}

type ServiceTO struct {
	ID int64 						`json:"service_id"`
	Name string 					`json:"service_name"`
	Description string 				`json:"service_description"`
}

type MethodTO struct {
	ID int64 						`json:"method_id"`
	MethodIndex MethodName 			`json:"method_index"`
	Name string 					`json:"method_name"`
	Description string 				`json:"method_description"`
	ServiceID int64 				`json:"service_id"`
}