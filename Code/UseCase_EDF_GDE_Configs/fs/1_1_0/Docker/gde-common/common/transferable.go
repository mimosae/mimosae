package common

import (
	"fmt"
	"strconv"
	"time"
	"usecase/log"
)

type Command struct {
	MethodName MethodName
	Parameters map[string]string
	Data []byte //json
}

func NewCommand(name MethodName) *Command {
	return &Command{
		MethodName: name,
		Parameters: make(map[string]string),
	}
}

type CommandResult struct {
	Code int // OK or ERROR
	Message string
	Data []byte
}

const (
	OK = 1
	ERROR = 2
)

func (c *Command) GetParameter(name string) (string, error) {
	v, ok :=c.Parameters[name]
	if !ok {
		log.Log("parameter" + name + "not found")
		return "", fmt.Errorf("parameter " + name + " not found")
	}
	return v, nil
}

func (c *Command) GetLong(name string) (int64, error) {
	value, err := c.GetParameter(name)
	if err != nil {
		return 0, err
	}
	long, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		log.Log("parameter" + name + "not long")
		return 0, err
	}
	return  long, nil
}

func (c *Command) SetLong(name string, value int64) {
	c.Parameters[name] = strconv.FormatInt(value, 10)
}

func (c *Command) GetInt(name string) (int, error) {
	value, err := c.GetParameter(name)
	if err != nil {
		return 0, err
	}
	integer, err := strconv.Atoi(value)
	if err != nil {
		log.Log("parameter" + name + "not int")
		return 0, err
	}
	return  integer, nil
}

func (c *Command) SetInt(name string, value int) {
	c.Parameters[name] = strconv.Itoa(value)
}

func (c *Command) GetBool(name string) (bool, error) {
	value, err := c.GetParameter(name)
	if err != nil {
		return false, err
	}
	boolean, err := strconv.ParseBool(value)
	if err != nil {
		log.Log("parameter" + name + "not bool")
		return false, err
	}
	return  boolean, nil
}

func (c *Command) SetBool(name string, value bool) {
	c.Parameters[name] = strconv.FormatBool(value)
}

func (c *Command) GetFloat(name string) (float32, error) {
	value, err := c.GetParameter(name)
	if err != nil {
		return 0, err
	}
	float, err := strconv.ParseFloat(value, 32)
	if err != nil {
		log.Log("parameter" + name + "not float32")
		return 0, err
	}
	return  float32(float), nil
}

func (c *Command) SetFloat(name string, value float32) {
	c.Parameters[name] = fmt.Sprintf("%f", value)
}

func (c *Command) GetDouble(name string) (float64, error) {
	value, err := c.GetParameter(name)
	if err != nil {
		return 0, err
	}
	float, err := strconv.ParseFloat(value, 64)
	if err != nil {
		log.Log("parameter" + name + "not float64")
		return 0, err
	}
	return  float, nil
}

func (c *Command) SetDouble(name string, value float64) {
	c.Parameters[name] = fmt.Sprintf("%g", value)
}

func (c *Command) GetString(name string) (string, error) {
	return  c.GetParameter(name)
}

func (c *Command) SetString(name string, value string) {
	c.Parameters[name] = value
}

func (c *Command) GetTime(name string) (time.Time, error) {
	value, err := c.GetParameter(name)
	if err != nil {
		return time.Time{}, err
	}
	t, err := time.Parse(time.RFC3339, value)
	if err != nil {
		log.Log("parameter" + name + "not time")
		return time.Time{}, err
	}
	return  t, nil
}

func (c *Command) SetTime(name string, t time.Time) {
	c.Parameters[name] = fmt.Sprint(t.String())
}