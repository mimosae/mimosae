package common

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
	"usecase/log"
)

type Credentials struct {
	Login string `json:"login"`
	Password string `json:"password"`
}

func Login(login string, password string) string {
	log.Logf("user %v log in ...", login)
	creds := &Credentials{
		Login:    login,
		Password: password,
	}
	body, _ := json.Marshal(creds)
	client := &http.Client{Timeout: time.Minute}
	url := LocalHostIP + AuthServicePort + "/gde/" + AUTHENTICATION + "/login"
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	resp, _ := client.Do(req)
	token, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	return string(token)
}

/*
func SetTokenToHeader(req *http.Request, token string){
	req.Header.Set("Authorization", token)
}

func GetLogin(token string) (string, error) {
	client := &http.Client{Timeout: time.Minute}
	url := LocalHostIP + AuthServicePort + "/gde/" + AUTHENTICATION + "/get_login"
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Set("Authorization", "Token " + token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		login, _ := ioutil.ReadAll(resp.Body)
		return string(login), nil
	} else {
		return "", fmt.Errorf("error authentication")
	}

}
 */

/*
func GetToken(req *http.Request) string {
	return req.Header.Get("Authorization")
}
 */
