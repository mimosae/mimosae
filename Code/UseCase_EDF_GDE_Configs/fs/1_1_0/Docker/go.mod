module file-service

go 1.12

replace gde-common v0.0.0 => ./gde-common
replace usecase/log v0.0.0 => ./log
require gde-common v0.0.0

require (
	//github.com/golang/protobuf v1.4.3
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	usecase/log v0.0.0
	github.com/sirupsen/logrus v1.7.0
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.2.2
)
