package server

import (
	"file-service/db"
	"gde-common/common"
	"github.com/gorilla/mux"
	"usecase/log"
	"net/http"
)

type FileHTTPServer struct {
	Type string
}

func NewFileHTTPServer(t string) *FileHTTPServer {
	return &FileHTTPServer{
		Type: t,
	}
}

func (hs *FileHTTPServer) Starter(ip string, port string) {
	database := db.DB
	router := mux.NewRouter()

	log.Logf("Init Database...")
	database.ConnectDB()
	database.InitDB()
	defer database.Close()

	url := "/gde/" + common.FILE_SERVICE
	urlFiles := url + "/files"
	urlFileLock := url + "/file_lock"
	urlChunks := url + "/chunks"
	urlFileChunks := url + "/file_chunks"

	router.Handle(urlFiles, FileServiceFilesHandler()).Methods("POST")
	router.Handle(urlFileLock, FileServiceFileLockHandler()).Methods("POST")
	router.Handle(urlChunks, FileServiceChunksHandler()).Methods("POST")
	router.Handle(urlFileChunks, FileServiceFileChunksHandler()).Methods("POST")

	/*
	router.Handle(url, FileServiceHandler()).Methods("POST")
	// Find File By Id (metadata, without chunk data)
	router.Handle("/fs/file/metadata/{file_id}", FindFileById()).Methods("GET")
	// Is Valid
	router.Handle("/fs/file/is_valid/{file_id}", IsValid()).Methods("GET")
	// Create file
	router.Handle("/fs/file/create", CreateFile()).Methods("POST")
	 */

	log.Logf("http server serving on %s port %s", ip, port)
	log.Logf("%v", http.ListenAndServe(ip+":"+port, router))
}
