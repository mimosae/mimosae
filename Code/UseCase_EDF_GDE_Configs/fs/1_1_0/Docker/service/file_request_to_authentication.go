package service

import (
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func GetLogin(token string) (string, error) {
	client := &http.Client{Timeout: time.Minute}
	authServiceHost := os.Getenv("AuthenticationServiceHost")
	if authServiceHost == "" {
		authServiceHost = common.LocalHostIP
	}

	url := authServiceHost + ":" + common.AuthServicePort + "/gde/" + common.AUTHENTICATION + "/get_login"
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Set("Authorization", "Token " + token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		login, _ := ioutil.ReadAll(resp.Body)
		return string(login), nil
	} else {
		return "", fmt.Errorf("error authentication")
	}
}

func GetToken(req *http.Request) string {
	return req.Header.Get("Authorization")
}
