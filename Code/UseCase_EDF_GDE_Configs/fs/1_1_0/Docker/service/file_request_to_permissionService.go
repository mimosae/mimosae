package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"usecase/log"
	"net/http"
	"os"
	"time"
)

func CheckPermission(login string, serviceName string, methodName common.MethodName, token string) (bool, error) {
	log.Logf("check user %v has permission on %v : %v ...", login, serviceName, methodName)
	client := &http.Client{Timeout: time.Minute}
	permissionServiceHost := os.Getenv("PermissionServiceHost")
	if permissionServiceHost == "" {
		permissionServiceHost = common.LocalHostIP
	}
	url := permissionServiceHost + ":" + common.PermissionServicePort + "/gde/" + common.PERMISSION_SERVICE + "/check_permission"
	command := common.NewCommand(common.CHECK_USER_PERMISSION)
	command.SetString("login", login)
	command.SetString("service_name", serviceName)
	command.SetInt("method_name", int(methodName))
	body, _ := json.Marshal(command)
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Authorization", token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		return true, nil
	} else {
		return false, fmt.Errorf(result.Message)
	}
}