package main

import (

	"file-service/config"
	"file-service/db"
	"file-service/server"
	"os"
)

func main() {
	ip := os.Getenv("IP")
	if ip == "" {
		ip = config.DefaultIP
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = config.DefaultPort
	}

	var server = server.NewFileHTTPServer(config.DefaultProtocol)
	db.DB = &db.FilePostgresDB{}
	server.Starter(ip, port)
}
