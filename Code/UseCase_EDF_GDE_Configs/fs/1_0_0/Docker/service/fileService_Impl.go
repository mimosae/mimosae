package service

import (
	"crypto/md5"
	"encoding/hex"
	"file-service/db"
	"file-service/entities"
	"fmt"
	"gde-common/common"
	log "github.com/sirupsen/logrus"
	"strings"
	"time"
)

type FileServiceImpl struct {}

func (fs *FileServiceImpl) CreateFile(f *entities.GDEFile) (*entities.GDEFile, error) {
	return db.DB.CreateFile(f)
}

func (fs *FileServiceImpl) UpdateFile(f *entities.GDEFile) (*entities.GDEFile, error) {
	if v, err := fs.IsValid(f.ID); !v || err != nil {
		log.Printf("file %v is not valid, update error... \n", f.ID)
		return nil, err
	}
	return  db.DB.UpdateFile(f)
}

func (fs *FileServiceImpl) DeleteFile(id int64) error {
	if v, err := fs.IsValid(id); !v || err != nil {
		log.Printf("file %v is not valid, deleted error... \n", id)
		return err
	}
	return db.DB.DeleteFile(id)
}

func (fs *FileServiceImpl) RestoreFile(id int64) error {
	if v, err := fs.IsValid(id); !v || err != nil {
		log.Printf("file %v is not valid, deleted error... \n", id)
		return err
	}
	return db.DB.RestoreFile(id)
}

func (fs *FileServiceImpl) FindFileById(id int64) (entities.GDEFile, error) {
	return db.DB.FindFileById(id)
}

func (fs *FileServiceImpl) ReadFile(id int64) (entities.GDEFile, error) {
	if v, err := fs.IsValid(id); !v || err != nil {
		log.Printf("file %v is not valid, read error... \n", id)
		return entities.GDEFile{}, err
	}
	return db.DB.ReadFile(id)
}

func (fs *FileServiceImpl) FindFilesByName(name string) ([]entities.GDEFile, error) {
	return db.DB.FindFilesByName(name)
}
func (fs *FileServiceImpl) FindFilesByCreationDate(creationDate *time.Time, comparator common.DateComparator) ([]entities.GDEFile, error) {
	return db.DB.FindFilesByCreationDate(creationDate, comparator)
}
func (fs *FileServiceImpl) FindFilesByDeletionDate(deletionDate *time.Time, comparator common.DateComparator) ([]entities.GDEFile, error) {
	return db.DB.FindFilesByDeletionDate(deletionDate, comparator)
}
func (fs *FileServiceImpl) FindFilesByUpdateDate(updateDate *time.Time, comparator common.DateComparator) ([]entities.GDEFile, error) {
	return db.DB.FindFilesByUpdateDate(updateDate, comparator)
}
func (fs *FileServiceImpl) FindFilesByRestorationDate(restorationDate *time.Time, comparator common.DateComparator) ([]entities.GDEFile, error) {
	return db.DB.FindFilesByRestorationDate(restorationDate, comparator)
}
func (fs *FileServiceImpl) ListFiles() ([]entities.GDEFile, error) {
	return db.DB.ListFiles()
}
func (fs *FileServiceImpl) FindFilesToIndex() ([]entities.GDEFile, error) {
	return db.DB.FindFilesToIndex()
}
func (fs *FileServiceImpl) FindFilesDeleted() ([]entities.GDEFile, error) {
	return db.DB.FindFilesDeleted()
}

func (fs *FileServiceImpl) OpenFile(userId int64, fileId int64) (bool, error) { //LockFile
	log.Printf("Opening file %v by user %v... \n", fileId, userId)
	return GetFileLocker().LockFile(userId, fileId)
}

func (fs *FileServiceImpl) CloseFile(userId int64, fileId int64) (bool, error) { //UnlockFile
	log.Printf("Closing file %v by user %v... \n", fileId, userId)
	fs.SetValid(fileId, true)
	return GetFileLocker().UnlockFile(userId, fileId)
}

func (fs *FileServiceImpl) SetValid(fileId int64, isValid bool) error {
	return db.DB.SetValid(fileId, isValid)
}

func (fs *FileServiceImpl) IsValid(fileId int64) (bool, error) {
	return db.DB.IsValid(fileId)
}

func (fs *FileServiceImpl) SetIndexed(fileId int64, isIndexed bool) error {
	return db.DB.SetIndexed(fileId, isIndexed)
}

func (fs *FileServiceImpl) IsIndexed(fileId int64) (bool, error) {
	return db.DB.IsIndexed(fileId)
}

//Chunks
func (fs *FileServiceImpl) CreateChunk(c *entities.Chunk) (*entities.Chunk, error) {
	log.Printf("Checksum...")
	var err error
	check := md5.Sum(c.Data)
	checkStr := hex.EncodeToString(check[:])
	if c.Checksum == "" {
		err = fmt.Errorf("null checksum")
		return nil, err
	}
	if !strings.EqualFold(checkStr, c.Checksum) {
		err = fmt.Errorf("bad checksum")
		return nil, err
	}
	c.Size = int64(len(c.Data))
	return db.DB.CreateChunk(c)
}

func (fs *FileServiceImpl) ReadChunk(chunkId int64) (entities.Chunk, error) {
	return db.DB.ReadChunk(chunkId)
}

//File-Chunk association
func (fs *FileServiceImpl) AttachChunkToFile(fileId int64, chunkId int64) error {
	var err error
	if err = db.DB.AttachChunkToFile(fileId, chunkId); err != nil {
		return err
	}
	if err = db.DB.SetValid(fileId, false); err != nil {
		return err
	}
	return nil
}

func (fs *FileServiceImpl) DetachChunkFromFile(fileId int64, chunkId int64) error {
	var err error
	if err = db.DB.DetachChunkFromFile(fileId, chunkId); err != nil {
		return err
	}
	if err = db.DB.SetValid(fileId, false); err != nil {
		return err
	}
	return nil
}

func (fs *FileServiceImpl) FindChunksByFileId(fileId int64) ([]entities.Chunk, error) {
	return db.DB.FindChunksByFileId(fileId)
}

func (fs *FileServiceImpl) FindChunksInfoByFileId(fileId int64) ([]entities.ChunkInfo, error) {
	return db.DB.FindChunksInfoByFileId(fileId)
}
