package service

import (
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func GetLogin(token string) (string, error) {
	client := &http.Client{Timeout: time.Minute}
	authServiceHost := os.Getenv("AuthenticationServiceHost")
	if authServiceHost == "" {
		authServiceHost = common.LocalHostIP
	}
	authServicePort := os.Getenv("AuthenticationServicePort")
	if authServicePort == "" {
		authServicePort = common.AuthServicePort
	}
	url := fmt.Sprintf("http://%s:%s/gde/%s/get_login", authServiceHost, authServicePort, common.AUTHENTICATION)
	req, _ := http.NewRequest(http.MethodGet, url, nil)
	req.Header.Set("Authorization", "Token "+token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		login, _ := ioutil.ReadAll(resp.Body)
		return string(login), nil
	} else {
		return "", fmt.Errorf("error authentication")
	}
}

func GetToken(req *http.Request) string {
	return req.Header.Get("Authorization")
}
