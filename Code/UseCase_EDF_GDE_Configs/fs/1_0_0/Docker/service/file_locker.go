package service

import (
	"fmt"
	"sync"
	log "github.com/sirupsen/logrus"
)

//Singleton
type FileLocker struct {
	LockedFilesMap map[int64]int64 //key: file_id, value: user_id
}

var lockedFiles *FileLocker

var once sync.Once

func GetFileLocker() *FileLocker {
	once.Do(func() {
		lockedFiles = &FileLocker{LockedFilesMap: make(map[int64]int64)}
	})
	return lockedFiles
}

func (fl *FileLocker) LockFile(userId int64, fileId int64) (bool, error) {
	log.Printf("Locking file %v by %v... \n", fileId, userId)
	v, ok := fl.LockedFilesMap[fileId]
	if ok { // file_id exist ?
		if v == userId { // file locked by user_id ?
			return true, nil
		}
		return false, fmt.Errorf("locked by another user")
	}
	fl.LockedFilesMap[fileId] = userId
	return true, nil
}

func (fl *FileLocker) UnlockFile(userId int64, fileId int64) (bool, error) {
	log.Printf("Unlocking file %v by %v... \n", fileId, userId)
	v, ok := fl.LockedFilesMap[fileId]
	if ok { // file_id exist ?
		if v == userId { // file locked by user_id ?
			delete(fl.LockedFilesMap, fileId)
			return true, nil
		}
	}
	return false, fmt.Errorf("unlock file error, file not exist or file locked by another user")
}

