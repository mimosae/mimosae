package db

import (
	"file-service/entities"
	"gde-common/common"
	"time"
)

// global variable - singleton
var DB FileDatabase

type FileDatabase interface {
	ConnectDB()
	InitDB()
	Close()

	CreateFile(f *entities.GDEFile) (*entities.GDEFile, error)
	UpdateFile(f *entities.GDEFile) (*entities.GDEFile, error)
	DeleteFile(id int64) error
	RestoreFile(id int64) error

	FindFileById(id int64) (entities.GDEFile, error)
	ReadFile(id int64) (entities.GDEFile, error)
	FindFilesByName(name string) ([]entities.GDEFile, error)
	FindFilesByCreationDate(creationDate *time.Time, comparator common.DateComparator) ([]entities.GDEFile, error)
	FindFilesByDeletionDate(deletionDate *time.Time, comparator common.DateComparator) ([]entities.GDEFile, error)
	FindFilesByUpdateDate(updateDate *time.Time, comparator common.DateComparator) ([]entities.GDEFile, error)
	FindFilesByRestorationDate(restorationDate *time.Time, comparator common.DateComparator) ([]entities.GDEFile, error)
	ListFiles() ([]entities.GDEFile, error)
	FindFilesToIndex() ([]entities.GDEFile, error) //file not indexed
	FindFilesDeleted() ([]entities.GDEFile, error)

	SetValid(fileId int64, isValid bool) error //not for client, close file then set valid
	IsValid(fileId int64) (bool, error)

	SetIndexed(fileId int64, isIndexed bool) error //not for client
	IsIndexed(fileId int64) (bool, error) //not for client

	//Chunks
	CreateChunk(c *entities.Chunk) (*entities.Chunk, error)
	ReadChunk(chunkId int64) (entities.Chunk, error)


	//File-Chunk association
	AttachChunkToFile(fileId int64, chunkId int64) error
	DetachChunkFromFile(fileId int64, chunkId int64) error
	FindChunksByFileId(fileId int64) ([]entities.Chunk, error)
	FindChunksInfoByFileId(fileId int64) ([]entities.ChunkInfo, error)
}

