package main

import (
	"os"
	"permission-service/config"
	"permission-service/db"
	"permission-service/server"
)

func main() {
	server := server.NewPermissionHTTPServer(config.DefaultProtocol)
	db.DB = &db.PermissionPostgresDB{}
	ip := os.Getenv("IP")
	if ip == "" {
		ip = config.DefaultIP
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = config.DefaultPort
	}
	server.Starter(ip, port)
}