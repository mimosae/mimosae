package server

import (
	"encoding/json"
	"fmt"
	"gde-common/common"
	"net/http"
	"permission-service/entities"
	"permission-service/service"

	log "github.com/sirupsen/logrus"
)

func PermissionServiceServicesHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ps service.PermissionService = &service.PermissionServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else {
			// check permission
			ok, err := ps.CheckUserPermission(login, common.PERMISSION_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.CREATE_SERVICE:
					srv := entities.Service{}
					json.Unmarshal(command.Data, &srv)
					if result, err := ps.CreateService(&srv); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_SERVICE:
					srv := entities.Service{}
					json.Unmarshal(command.Data, &srv)
					if result, err := ps.UpdateService(&srv); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_SERVICE:
					serviceId, _ := command.GetLong("service_id")
					if ok, err := ps.DeleteService(serviceId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(ok)
						resp.Code = common.OK
					}
				case common.FIND_SERVICE_BY_ID:
					serviceId, _ := command.GetLong("service_id")
					if result, err := ps.FindServiceByID(serviceId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_SERVICE_BY_NAME:
					serviceName, _ := command.GetString("service_name")
					if result, err := ps.FindServiceByName(serviceName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_SERVICES:
					if result, err := ps.ListServices(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.PERMISSION_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func PermissionServiceMethodsHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ps service.PermissionService = &service.PermissionServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else {
			// check permission
			ok, err := ps.CheckUserPermission(login, common.PERMISSION_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.CREATE_METHOD:
					method := entities.Method{}
					json.Unmarshal(command.Data, &method)
					if result, err := ps.CreateMethod(&method); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_METHOD:
					method := entities.Method{}
					json.Unmarshal(command.Data, &method)
					if result, err := ps.UpdateMethod(&method); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_METHOD:
					methodId, _ := command.GetLong("method_id")
					if ok, err := ps.DeleteService(methodId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(ok)
						resp.Code = common.OK
					}
				case common.FIND_METHOD_BY_ID:
					methodId, _ := command.GetLong("method_id")
					if result, err := ps.FindMethodByID(methodId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_METHOD_BY_NAME:
					methodName, _ := command.GetString("method_name")
					if result, err := ps.FindMethodByName(methodName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_METHOD_BY_SERVICE_NAME_METHOD_INDEX:
					serviceName, _ := command.GetString("service_name")
					methodIndex, _ := command.GetInt("method_index")
					if result, err := ps.FindMethodByServiceNameMethodIndex(serviceName, common.MethodName(methodIndex)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_METHODS_BY_SERVICE_ID:
					serviceId, _ := command.GetLong("service_id")
					if result, err := ps.FindMethodsByService(serviceId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_METHODS:
					if result, err := ps.ListMethods(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.PERMISSION_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func PermissionServicePermissionsHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ps service.PermissionService = &service.PermissionServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else {
			// check permission
			ok, err := ps.CheckUserPermission(login, common.PERMISSION_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.CREATE_PERMISSION:
					permission := entities.Permission{}
					json.Unmarshal(command.Data, &permission)
					if result, err := ps.CreatePermission(&permission); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_PERMISSION:
					permission := entities.Permission{}
					json.Unmarshal(command.Data, &permission)
					if result, err := ps.UpdatePermission(&permission); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_PERMISSION:
					permissionId, _ := command.GetLong("permission_id")
					if ok, err := ps.DeletePermission(permissionId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(ok)
						resp.Code = common.OK
					}
				case common.FIND_PERMISSION_BY_ID:
					permissionId, _ := command.GetLong("permission_id")
					if result, err := ps.FindPermissionByID(permissionId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PERMISSIONS_BY_GROUP_ID:
					groupId, _ := command.GetLong("group_id")
					if result, err := ps.FindPermissionsByGroupID(groupId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PERMISSIONS_BY_USER_ID:
					userId, _ := command.GetLong("user_id")
					if result, err := ps.FindPermissionsByUserID(userId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PERMISSIONS_BY_METHOD_ID:
					methodId, _ := command.GetLong("method_id")
					if result, err := ps.FindPermissionsByMethodID(methodId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_PERMISSIONS:
					if result, err := ps.ListPermissions(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.PERMISSION_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func PermissionServiceCheckPermissionHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var ps service.PermissionService = &service.PermissionServiceImpl{}
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		token := service.GetToken(r)
		switch command.MethodName {
		case common.CHECK_USER_PERMISSION:
			login, _ := command.GetString("login")
			serviceName, _ := command.GetString("service_name")
			methodName, _ := command.GetInt("method_name")
			if ok, err := ps.CheckUserPermission(login, serviceName, common.MethodName(methodName), token); !ok || err != nil {
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				resp.Data, _ = json.Marshal(ok)
				resp.Code = common.OK
			}
		case common.HAS_GROUP_THE_PERMISSION_ON:
			groupId, _ := command.GetLong("group_id")
			serviceName, _ := command.GetString("service_name")
			methodIndex, _ := command.GetInt("method_index")
			if result, err := ps.HasGroupThePermissionOn(groupId, serviceName, common.MethodName(methodIndex), token); err != nil {
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				resp.Data, _ = json.Marshal(result)
				resp.Code = common.OK
			}
		case common.HAS_USER_THE_PERMISSION_ON:
			userId, _ := command.GetLong("user_id")
			serviceName, _ := command.GetString("service_name")
			methodIndex, _ := command.GetInt("method_index")
			if result, err := ps.HasUserThePermissionOn(userId, serviceName, common.MethodName(methodIndex), token); err != nil {
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				resp.Data, _ = json.Marshal(result)
				resp.Code = common.OK
			}
		default:
			err := fmt.Errorf("invalid method %v : %v", common.PERMISSION_SERVICE, command.MethodName)
			resp.Code = common.ERROR
			resp.Message = err.Error()
		}
		SendResult(w, resp)
	})
}

/*
func PermissionServiceHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var ps service.PermissionService = &service.PermissionServiceImpl{}

		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}

		// authentication
		token := common.GetToken(r)
		//login, err := common.GetLogin(token)
		//if err != nil {
		//	resp.Code = common.ERROR
		//	resp.Message = err.Error()
		//} else {
			// check permission
			//ok, err := ps.CheckUserPermission(login, common.PERMISSION_SERVICE, command.MethodName);
		//	 ok, err := common.CheckPermission(login, common.PERMISSION_SERVICE, command.MethodName, token)
		//	if !ok || err != nil {
		//		log.Printf("Check user permission error")
		//		resp.Code = common.ERROR
		//		resp.Message = err.Error()
		//	} else {
				switch command.MethodName {
				case common.CREATE_SERVICE:
					srv := entities.Service{}
					json.Unmarshal(command.Data, &srv)
					if result, err := ps.CreateService(&srv); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_SERVICE:
					srv := entities.Service{}
					json.Unmarshal(command.Data, &srv)
					if result, err := ps.UpdateService(&srv); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_SERVICE:
					serviceId, _ := command.GetLong("service_id")
					if ok, err := ps.DeleteService(serviceId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(ok)
						resp.Code = common.OK
					}
				case common.FIND_SERVICE_BY_ID:
					serviceId, _ := command.GetLong("service_id")
					if result, err := ps.FindServiceByID(serviceId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_SERVICE_BY_NAME:
					serviceName, _ := command.GetString("service_name")
					if result, err := ps.FindServiceByName(serviceName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_SERVICES:
					if result, err := ps.ListServices(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CREATE_METHOD:
					method := entities.Method{}
					json.Unmarshal(command.Data, &method)
					if result, err := ps.CreateMethod(&method); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_METHOD:
					method := entities.Method{}
					json.Unmarshal(command.Data, &method)
					if result, err := ps.UpdateMethod(&method); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_METHOD:
					methodId, _ := command.GetLong("method_id")
					if ok, err := ps.DeleteService(methodId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(ok)
						resp.Code = common.OK
					}
				case common.FIND_METHOD_BY_ID:
					methodId, _ := command.GetLong("method_id")
					if result, err := ps.FindMethodByID(methodId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_METHOD_BY_NAME:
					methodName, _ := command.GetString("method_name")
					if result, err := ps.FindMethodByName(methodName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_METHOD_BY_SERVICE_NAME_METHOD_INDEX:
					serviceName, _ := command.GetString("service_name")
					methodIndex, _ := command.GetInt("method_index")
					if result, err := ps.FindMethodByServiceNameMethodIndex(serviceName, common.MethodName(methodIndex)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_METHODS_BY_SERVICE_ID:
					serviceId, _ := command.GetLong("service_id")
					if result, err := ps.FindMethodsByService(serviceId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_METHODS:
					if result, err := ps.ListMethods(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CREATE_PERMISSION:
					permission := entities.Permission{}
					json.Unmarshal(command.Data, &permission)
					if result, err := ps.CreatePermission(&permission); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_PERMISSION:
					permission := entities.Permission{}
					json.Unmarshal(command.Data, &permission)
					if result, err := ps.UpdatePermission(&permission); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_PERMISSION:
					permissionId, _ := command.GetLong("permission_id")
					if ok, err := ps.DeletePermission(permissionId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(ok)
						resp.Code = common.OK
					}
				case common.FIND_PERMISSION_BY_ID:
					permissionId, _ := command.GetLong("permission_id")
					if result, err := ps.FindPermissionByID(permissionId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PERMISSIONS_BY_GROUP_ID:
					groupId, _ := command.GetLong("group_id")
					if result, err := ps.FindPermissionsByGroupID(groupId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PERMISSIONS_BY_USER_ID:
					userId, _ := command.GetLong("user_id")
					if result, err := ps.FindPermissionsByUserID(userId, token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_PERMISSIONS_BY_METHOD_ID:
					methodId, _ := command.GetLong("method_id")
					if result, err := ps.FindPermissionsByMethodID(methodId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_PERMISSIONS:
					if result, err := ps.ListPermissions(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.HAS_GROUP_THE_PERMISSION_ON:
					groupId, _ := command.GetLong("group_id")
					serviceName, _ := command.GetString("service_name")
					methodIndex, _ := command.GetInt("method_index")
					if result, err := ps.HasGroupThePermissionOn(groupId, serviceName, common.MethodName(methodIndex), token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.HAS_USER_THE_PERMISSION_ON:
					userId, _ := command.GetLong("user_id")
					serviceName, _ := command.GetString("service_name")
					methodIndex, _ := command.GetInt("method_index")
					if result, err := ps.HasUserThePermissionOn(userId, serviceName, common.MethodName(methodIndex), token); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.USER_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			//}
		//}
		// finally
		SendResult(w, resp)
	})
}
*/
