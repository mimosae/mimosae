package db

import (
	"fmt"
	"gde-common/common"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //...
	log "github.com/sirupsen/logrus"
	"os"
	"permission-service/config"
	"permission-service/entities"
)

type PermissionPostgresDB struct {
	db *gorm.DB
}

func (pg *PermissionPostgresDB) GetDB() *gorm.DB {
	return pg.db
}

//ConnectDB connect to postgres database
func (pg *PermissionPostgresDB) ConnectDB() {
	var err error
	dbHost := os.Getenv("DBHOST")
	if dbHost == "" {
		dbHost = config.DbHost
	}
	dbPort := os.Getenv("DBPORT")
	if dbPort == "" {
		dbPort = config.DbPort
	}
	dbUser := os.Getenv("DBUSER")
	if dbUser == "" {
		dbUser = config.DbUser
	}
	dbName := os.Getenv("DBNAME")
	if dbName == "" {
		dbName = config.DbName
	}
	dbPassword := os.Getenv("DBPASSWORD")
	if dbPassword == "" {
		dbPassword = config.DbPassword
	}
	dbURI := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		dbUser,
		dbPassword,
		dbHost,
		dbPort,
		dbName)
	pg.db, err = gorm.Open("postgres", dbURI)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Postgres opened ...")
}

func (pg *PermissionPostgresDB) InitDB() {
	if err := pg.db.DropTableIfExists(&entities.Permission{}, &entities.Service{}, &entities.Method{}).Error; err != nil {
		log.Fatal("dropTableIfExist fails: ", err)
	}

	if err := pg.db.AutoMigrate(&entities.Permission{}, &entities.Service{}, &entities.Method{}).Error; err != nil {
		log.Fatal("automigrate fails: ", err)
	}

	// init db
	InitPermissionDatabase(pg)
}

//Close postgres database
func (pg *PermissionPostgresDB) Close() {
	pg.db.Close()
}

func (pg *PermissionPostgresDB) CreateService(service *entities.Service) (*entities.Service, error) {
	log.Println("Creating Service ...")
	if err := pg.db.Create(&service).Error; err != nil {
		return nil, fmt.Errorf("insert service into db fail: %v", err)
	}
	return service, nil
}

func (pg *PermissionPostgresDB) DeleteService(id int64) (bool,error) {
	log.Printf("Deleting Service %v... \n", id)
	if err := pg.db.Delete(&entities.Service{}, id).Error; err != nil {
		log.Errorf("delete file by id %d error", id)
		return false, err
	}
	return true, nil
}

func (pg *PermissionPostgresDB) UpdateService(service *entities.Service) (*entities.Service, error) {
	log.Println("Updating Service ...")
	if _, err := pg.FindServiceByID(service.ID); err != nil {
		log.Errorf("Error finding service")
		return nil, err
	}
	if err := pg.db.Save(&service).Error; err != nil {
		return nil, fmt.Errorf("update service fail: %v", err)
	}
	return service, nil
}

func (pg *PermissionPostgresDB) FindServiceByID(id int64) (entities.Service, error) {
	log.Printf("Finding service %v... \n", id)
	service := entities.Service{}
	if err := pg.db.First(&service, id).Error; err != nil {
		log.Errorf("Error finding service")
		return entities.Service{}, err
	}
	return service, nil
}

func (pg *PermissionPostgresDB) FindServiceByName(name string) (entities.Service, error) {
	log.Printf("Finding service named %v... \n", name)
	service := entities.Service{}
	if err := pg.db.Where(&entities.Service{Name: name}).Find(&service).Error; err != nil {
		log.Errorf("Error finding service with this name")
		return entities.Service{}, err
	}
	return service, nil
}

func (pg *PermissionPostgresDB) ListServices() ([]entities.Service, error) {
	log.Println("Listing All Services ...")
	var services []entities.Service
	if err := pg.db.Find(&services).Error; err != nil {
		log.Errorf("Error listing services")
		return nil, err
	}
	return services, nil
}

func (pg *PermissionPostgresDB) CreateMethod(method *entities.Method) (*entities.Method, error) {
	log.Println("Creating Method ...")
	// check service id exist
	if _, err := pg.FindServiceByID(method.ServiceID) ; err != nil {
		return nil, fmt.Errorf("service id of method fail: %v", err)
	}
	if err := pg.db.Create(&method).Error; err != nil {
		return nil, fmt.Errorf("insert method into db fail: %v", err)
	}
	return method, nil
}

func (pg *PermissionPostgresDB) DeleteMethod(id int64) (bool,error) {
	log.Printf("Deleting Method %v... \n", id)
	if err := pg.db.Delete(&entities.Method{}, id).Error; err != nil {
		log.Errorf("delete method by id %d error", id)
		return false, err
	}
	return true, nil
}

func (pg *PermissionPostgresDB) UpdateMethod(method *entities.Method) (*entities.Method, error) {
	log.Println("Updating Method ...")
	// check service id exist
	if _, err := pg.FindServiceByID(method.ServiceID) ; err != nil {
		return nil, fmt.Errorf("service id of method fail: %v", err)
	}
	if err := pg.db.Save(&method).Error; err != nil {
		return nil, fmt.Errorf("update method fail: %v", err)
	}
	return method, nil
}

func (pg *PermissionPostgresDB) FindMethodByID(id int64) (entities.Method, error) {
	log.Printf("Finding method %v... \n", id)
	method := entities.Method{}
	if err := pg.db.First(&method, id).Error; err != nil {
		log.Errorf("Error finding method")
		return entities.Method{}, err
	}
	return method, nil
}

func (pg *PermissionPostgresDB) FindMethodByName(name string) (entities.Method, error) {
	log.Printf("Finding method named %v... \n", name)
	method := entities.Method{}
	if err := pg.db.Where(&entities.Method{Name: name}).Find(&method).Error; err != nil {
		log.Errorf("Error finding method with this name")
		return entities.Method{}, err
	}
	return method, nil
}

func (pg *PermissionPostgresDB) FindMethodByServiceNameMethodIndex(serviceName string, methodIndex common.MethodName) (entities.Method, error) {
	log.Printf("Finding method by service named %v and method index %v... \n", serviceName, methodIndex)
	service, err := pg.FindServiceByName(serviceName)
	if err != nil {
		return entities.Method{}, fmt.Errorf("service name fail: %v", err)
	}
	method := entities.Method{}
	if err := pg.db.Where(&entities.Method{MethodIndex: methodIndex, ServiceID: service.ID}).Find(&method).Error; err != nil {
		log.Errorf("Error finding method")
		return entities.Method{}, err
	}
	return method, nil
}

func (pg *PermissionPostgresDB) FindMethodsByService(serviceId int64) ([]entities.Method, error) {
	log.Printf("Finding methods by service %v... \n", serviceId)
	var methods []entities.Method
	if err := pg.db.Where(&entities.Method{ServiceID: serviceId}).Find(&methods).Error; err != nil {
		log.Errorf("Error finding methods")
		return nil, err
	}
	return methods, nil
}

func (pg *PermissionPostgresDB) ListMethods() ([]entities.Method, error) {
	log.Println("Listing All Methods ...")
	var methods []entities.Method
	if err := pg.db.Find(&methods).Error; err != nil {
		log.Errorf("Error listing methods")
		return nil, err
	}
	return methods, nil
}

func (pg *PermissionPostgresDB) CreatePermission(permission *entities.Permission) (*entities.Permission, error) {
	log.Println("Creating Permission ...")
	if err := pg.db.Create(&permission).Error; err != nil {
		return nil, fmt.Errorf("insert permission into db fail: %v", err)
	}
	return permission, nil
}

func (pg *PermissionPostgresDB) DeletePermission(id int64) (bool,error) {
	log.Printf("Deleting Permission %v... \n", id)
	if err := pg.db.Delete(&entities.Permission{}, id).Error; err != nil {
		log.Errorf("delete permission by id %d error", id)
		return false, err
	}
	return true, nil
}

func (pg *PermissionPostgresDB) UpdatePermission(permission *entities.Permission) (*entities.Permission, error) {
	log.Println("Updating Permission ...")
	if err := pg.db.Save(&permission).Error; err != nil {
		return nil, fmt.Errorf("updating permission into db fail: %v", err)
	}
	return permission, nil
}

func (pg *PermissionPostgresDB) FindPermissionByID(id int64) (entities.Permission, error) {
	log.Printf("Finding Permission %v... \n", id)
	permission := entities.Permission{}
	if err := pg.db.First(&permission, id).Error; err != nil {
		log.Errorf("Error finding permission")
		return entities.Permission{}, err
	}
	return permission, nil
}

func (pg *PermissionPostgresDB) FindPermissionByGroupID(groupId int64) ([]entities.Permission, error) {
	// check group id valid in permission service
	log.Println("Finding Permission by group id %v... \n", groupId)
	var permissions []entities.Permission
	if err := pg.db.Where(&entities.Permission{GroupID: groupId}).Find(&permissions).Error; err != nil {
		log.Errorf("Error finding permissions")
		return permissions, err
	}
	return permissions, nil
}

func (pg *PermissionPostgresDB) FindPermissionByMethodID(methodId int64) ([]entities.Permission, error) {
	log.Println("Finding Permission by method id %v... \n", methodId)
	// check method id valid
	if _, err := pg.FindMethodByID(methodId); err != nil {
		log.Errorf("Method ID invalid")
		return nil, err
	}
	var permissions []entities.Permission
	if err := pg.db.Where(&entities.Permission{MethodID: methodId}).Find(&permissions).Error; err != nil {
		log.Errorf("Error finding permissions")
		return permissions, err
	}
	return permissions, nil
}

func (pg *PermissionPostgresDB) ListPermissions() ([]entities.Permission, error) {
	log.Println("Listing All Permissions ...")
	var permissions []entities.Permission
	if err := pg.db.Find(&permissions).Error; err != nil {
		log.Errorf("Error finding permissions")
		return permissions, err
	}
	return permissions, nil
}

func (pg *PermissionPostgresDB) HasGroupThePermissionOn(groupId int64, serviceName string, methodIndex common.MethodName) (bool, error) {
	log.Println("Check group %v permission ...", groupId)
	// check group id valid in service
	// find method by service name and method index
	method, err := pg.FindMethodByServiceNameMethodIndex(serviceName, methodIndex)
	if err != nil {
		log.Errorf("No method by service name %v and method index %v", serviceName, methodIndex)
		return false, err
	}
	var permissions []entities.Permission
	if err := pg.db.Where(&entities.Permission{GroupID: groupId, MethodID: method.ID}).Find(&permissions).Error; err != nil || permissions == nil {
		log.Errorf("Error finding permission")
		return false, err
	}
	return true, nil
}