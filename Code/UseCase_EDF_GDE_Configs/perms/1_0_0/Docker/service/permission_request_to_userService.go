package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func FindUserIDByName(name string, token string) (int64, error) {
	// create http client
	client := &http.Client{Timeout: time.Minute}
	// create Command as json
	userServiceHost := os.Getenv("UserServiceHost")
	if userServiceHost == "" {
		userServiceHost = common.LocalHostIP
	}
	userServicePort := os.Getenv("UserServicePort")
	if userServicePort == "" {
		userServicePort = common.UserServicePort
	}
	url := fmt.Sprintf("http://%s:%s/gde/%s/without_check_permission", userServiceHost, userServicePort, common.USER_SERVICE)
	command := common.NewCommand(common.FIND_USER_BY_NAME)
	command.SetString("user_name", name)
	body, _ := json.Marshal(command)
	// Send Command
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Authorization", token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	// Get CommandResult
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		user := common.UserTO{}
		err := json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&user)
		return user.ID, err
	} else {
		return -1, fmt.Errorf(result.Message)
	}
}

func CheckGroupIDValid(groupID int64, token string) (bool, error) {
	// create http client
	client := &http.Client{Timeout: time.Minute}
	// create Command as json
	userServiceHost := os.Getenv("UserServiceHost")
	if userServiceHost == "" {
		userServiceHost = common.LocalHostIP
	}
	userServicePort := os.Getenv("UserServicePort")
	if userServicePort == "" {
		userServicePort = common.UserServicePort
	}
	url := fmt.Sprintf("http://%s:%s/gde/%s/without_check_permission", userServiceHost, userServicePort, common.USER_SERVICE)
	command := common.NewCommand(common.FIND_GROUP_BY_ID)
	command.SetLong("group_id", groupID)
	body, _ := json.Marshal(command)
	// Send Command
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Authorization", token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	// Get CommandResult
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		if result.Data == nil || len(result.Data) == 0 {
			return false, fmt.Errorf(result.Message)
		} else {
			// Get CommandResult.Data
			group := common.GroupTO{}
			err := json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&group)
			if group.ID == groupID {
				return true, err
			} else {
				return false, fmt.Errorf("group id not valid")
			}
		}
	} else {
		return false, fmt.Errorf(result.Message)
	}
}

func GetUserGroups(userID int64, token string) ([]int64, error) {
	// create http client
	client := &http.Client{Timeout: time.Minute}
	// create Command as json
	userServiceHost := os.Getenv("UserServiceHost")
	if userServiceHost == "" {
		userServiceHost = common.LocalHostIP
	}
	userServicePort := os.Getenv("UserServicePort")
	if userServicePort == "" {
		userServicePort = common.UserServicePort
	}
	url := fmt.Sprintf("http://%s:%s/gde/%s/without_check_permission", userServiceHost, userServicePort, common.USER_SERVICE)
	command := common.NewCommand(common.LIST_GROUPS_OF_USER)
	command.SetLong("user_id", userID)
	body, _ := json.Marshal(command)
	// Send Command
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Authorization", token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	// Get CommandResult
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		var groups []common.GroupTO
		groupIds := make([]int64, 0)
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&groups)
		for _, group := range groups {
			groupIds = append(groupIds, group.ID)
		}
		return groupIds, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}
