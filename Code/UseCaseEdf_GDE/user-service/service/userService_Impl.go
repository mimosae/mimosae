package service

import (
	"user-service/db"
	"user-service/entities"
)

//UserServiceImpl ...
type UserServiceImpl struct {}

/* User */
//ListUsers ...
func (u *UserServiceImpl) ListUsers() ([]entities.User, error) {
	return db.DB.FindUsers()
}

//CreateUser ...
func (u *UserServiceImpl) CreateUser(userName string, password string) (*entities.User, error) {
	user := &entities.User{}
	user.Name = userName
	user.Password = password
	return db.DB.InsertUser(user)
}

func (u *UserServiceImpl) CreateUserByUser(user *entities.User) (*entities.User, error) {
	return db.DB.InsertUser(user)
}

//DeleteUser ...
func (u *UserServiceImpl) DeleteUser(id int64) error {
	return db.DB.DeleteUser(id)
}

//FindUserByID ...
func (u *UserServiceImpl) FindUserByID(id int64) (entities.User, error) {
	return db.DB.FindUserByID(id)
}

//FindUserByName ...
func (u *UserServiceImpl) FindUserByName(userName string) (entities.User, error) {
	return db.DB.FindUserByName(userName)
}

/* Group */

//CreateGroup ...
func (u *UserServiceImpl) CreateGroup(groupName string) (*entities.Group, error) {
	group := &entities.Group{}
	group.Name = groupName
	return db.DB.InsertGroup(group)
}

func (u *UserServiceImpl) CreateGroupByGroup(group *entities.Group) (*entities.Group, error) {
	return db.DB.InsertGroup(group)
}

//ListGroups ...
func (u *UserServiceImpl) ListGroups() ([]entities.Group, error) {
	return db.DB.FindGroups()
}

//FindGroupByID ...
func (u *UserServiceImpl) FindGroupByID(id int64) (entities.Group, error) {
	return db.DB.FindGroupByID(id)
}

//FindGroupByName ...
func (u *UserServiceImpl) FindGroupsByName(groupName string) ([]entities.Group, error) {
	return db.DB.FindGroupByName(groupName)
}

//DeleteGroup ...
func (u *UserServiceImpl) DeleteGroup(id int64) error {
	return db.DB.DeleteGroup(id)
}

/* User and Group */
//AddUserToGroup ...
func (u *UserServiceImpl) AddUserToGroup(userID int64, groupID int64) error {
	return db.DB.AddUserToGroup(userID, groupID)
}

//AddUsersToGroup ...
func (u *UserServiceImpl) AddUsersToGroup(userIDs []int64, groupID int64) error {
	return db.DB.AddUsersToGroup(userIDs, groupID)
}

//RemoveUserFromGroup ...
func (u *UserServiceImpl) RemoveUserFromGroup(userID int64, groupID int64) error {
	return db.DB.RemoveUserFromGroup(userID, groupID)
}

//IsUserInGroup ...
func (u *UserServiceImpl) IsUserInGroup(userID int64, groupID int64) bool {
	return db.DB.IsUserInGroup(userID, groupID)
}

//ListUsersInGroup ...
func (u *UserServiceImpl) ListUsersInGroup(groupID int64) ([]entities.User, error) {
	return db.DB.ListUsersInGroup(groupID)
}

//ListGroupsOfUser ...
func (u *UserServiceImpl) ListGroupsOfUser(userID int64) ([]entities.Group, error) {
	return db.DB.ListGroupsOfUser(userID)
}

func (u *UserServiceImpl) CheckPassword(login, password string) (bool,error) {
	return db.DB.CheckPassword(login, password)
}
