package entities

import "gde-common/common"

//User ...
type User struct {
	ID         int64       `json:"user_id" gorm:"primary_key;AUTO_INCREMENT"`
	Name       string      `json:"user_name"`
	Password   string      `json:"user_password"`
	Attributes []common.Attribute `json:"user_attributes"`
	Groups     []Group     `json:"groups" gorm:"many2many:user_groups;"`
}

type Group struct {
	ID         int64       `json:"group_id" gorm:"primary_key;AUTO_INCREMENT"`
	Name       string      `json:"group_name"`
	Attributes []common.Attribute `json:"group_attributes"`
	Users      []User      `json:"users" gorm:"many2many:user_groups;"`
}
