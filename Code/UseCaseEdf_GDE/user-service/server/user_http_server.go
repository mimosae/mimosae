package server

import (
	"gde-common/common"
	"user-service/db"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"net/http"
)

type UserHTTPServer struct {
	Type string
}

func NewUserHTTPServer(t string) *UserHTTPServer {
	return &UserHTTPServer{
		Type: t,
	}
}

func (hs *UserHTTPServer) Starter(ip string, port string) {
	database := db.DB
	router := mux.NewRouter()

	log.Printf("Init Database...")
	database.ConnectDB()
	database.InitDB()
	defer database.Close()

	url := "/gde/" + common.USER_SERVICE
	urlUsers := url + "/users"
	urlGroups := url + "/groups"
	urlUsersgroups := url + "/usersgroups"
	urlCheckpsw := url + "/check_password"
	urlWithoutCheckpsw := url + "/without_check_permission"

	router.Handle(urlUsers, UserServiceUsersHandler()).Methods("POST")
	router.Handle(urlGroups, UserServiceGroupsHandler()).Methods("POST")
	router.Handle(urlUsersgroups, UserServiceUsersGroupsHandler()).Methods("POST")
	router.Handle(urlCheckpsw, UserServiceCheckPasswordHandler()).Methods("POST")
	router.Handle(urlWithoutCheckpsw, UserServiceWithoutCheckPermissionHandler()).Methods("POST")

	//router.Handle(url, UserServiceHandler()).Methods("POST")
	//router.Handle("/gde/check_password", UserServiceCheckPasswordHandler()).Methods("POST")

	log.Printf("http server serving on %s port %s", ip, port)
	log.Fatal(http.ListenAndServe(ip+":"+port, router))

}
