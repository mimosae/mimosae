module user-service

go 1.13

require (
	//github.com/golang/protobuf v1.4.3
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/sirupsen/logrus v1.7.0
)

replace gde-common v0.0.0 => ./gde-common

require gde-common v0.0.0
