package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	AuthServiceURLBase       = common.LocalHostIP + ":" + common.AuthServicePort + "/gde/" + common.AUTHENTICATION
	PermissionServiceURLBase = common.LocalHostIP + ":" + common.PermissionServicePort + "/gde/" + common.PERMISSION_SERVICE
	UserServiceURLBase       = common.LocalHostIP + ":" + common.UserServicePort + "/gde/" + common.USER_SERVICE
	ProjectServiceURLBase    = common.LocalHostIP + ":" + common.ProjectServicePort + "/gde/" + common.PROJECT_SERVICE
	FileServiceURLBase       = common.LocalHostIP + ":" + common.FileServicePort + "/gde/" + common.FILE_SERVICE
)

func main() {
	token := common.Login(common.TestLogin, common.TestPassword)
	client := &http.Client{Timeout: time.Minute}
	println("Client - Create User...")
	CreateUser(client, token)
}

func CreateUser(client *http.Client, token string) {
	command := common.NewCommand(common.CREATE_USER)
	command.SetString("user_name", "User1")
	command.SetString("user_password", "user1")
	body, _ := json.Marshal(command)
	req, _ := http.NewRequest(http.MethodPost, UserServiceURLBase+"/users", bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Authorization", token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		fmt.Println(string(result.Data))
	} else {
		fmt.Println(result.Message)
	}
}
