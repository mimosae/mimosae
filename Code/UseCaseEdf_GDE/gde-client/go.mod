module gde-client

go 1.12

replace gde-common v0.0.0 => ../gde-common

require gde-common v0.0.0
