package db

import (
	"gde-common/common"
	log "github.com/sirupsen/logrus"
	"permission-service/entities"
)

const TestGroupID = 1

func InitPermissionDatabase(pg *PermissionPostgresDB) {
	log.Printf("Init Permission Database ...")

	authService := &entities.Service{
		Name:        common.AUTHENTICATION,
		Description: "authentication service",
	}
	permissionService := &entities.Service{
		Name:        common.PERMISSION_SERVICE,
		Description: "permission service",
	}
	userService := &entities.Service{
		Name:        common.USER_SERVICE,
		Description: "user service",
	}
	projectService := &entities.Service{
		Name:        common.PROJECT_SERVICE,
		Description: "project service",
	}
	fileService := &entities.Service{
		Name:        common.FILE_SERVICE,
		Description: "file service",
	}
	authService, _ = pg.CreateService(authService)
	permissionService, _ = pg.CreateService(permissionService)
	userService, _ = pg.CreateService(userService)
	projectService, _ = pg.CreateService(projectService)
	fileService, _ = pg.CreateService(fileService)

	// init method
	method1 := &entities.Method{
		MethodIndex: common.CREATE_USER,
		Name:        "CreateUser",
		Description: "create user",
		ServiceID:   userService.ID,
	}
	method2 := &entities.Method{
		MethodIndex: common.FIND_USER_BY_NAME,
		Name:        "Find_user_by_name",
		Description: "find user by name",
		ServiceID:   userService.ID,
	}
	method3 := &entities.Method{
		MethodIndex: common.LIST_GROUPS_OF_USER,
		Name:        "List_groups_of_user",
		Description: "list groups of user",
		ServiceID:   userService.ID,
	}
	method4 := &entities.Method{
		MethodIndex: common.HAS_GROUP_THE_PERMISSION_ON,
		Name:        "Has_group_permission",
		Description: "has group permission",
		ServiceID:   permissionService.ID,
	}
	method5 := &entities.Method{
		MethodIndex: common.HAS_USER_THE_PERMISSION_ON,
		Name:        "Has_user_permission",
		Description: "has user permission",
		ServiceID:   permissionService.ID,
	}

	method1, _ = pg.CreateMethod(method1)
	method2, _ = pg.CreateMethod(method2)
	method3, _ = pg.CreateMethod(method3)
	method4, _ = pg.CreateMethod(method4)
	method5, _ = pg.CreateMethod(method5)

	permission1 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method1.ID,
	}
	permission2 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method2.ID,
	}
	permission3 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method3.ID,
	}
	permission4 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method4.ID,
	}
	permission5 := &entities.Permission{
		GroupID:  TestGroupID,
		MethodID: method5.ID,
	}

	pg.CreatePermission(permission1)
	pg.CreatePermission(permission2)
	pg.CreatePermission(permission3)
	pg.CreatePermission(permission4)
	pg.CreatePermission(permission5)
}
