package server

import (
	"gde-common/common"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"permission-service/db"
)

type PermissionHTTPServer struct{
	Type string
}

func NewPermissionHTTPServer(t string) *PermissionHTTPServer {
	return &PermissionHTTPServer{
		Type: t,
	}
}

func (ps *PermissionHTTPServer) Starter(ip string, port string) {
	database := db.DB
	router := mux.NewRouter()

	log.Printf("Init Database...")
	database.ConnectDB()
	database.InitDB()
	defer database.Close()

	url := "/gde/" + common.PERMISSION_SERVICE
	urlServices := url + "/services"
	urlMethods := url + "/methods"
	urlPermissions := url + "/permissions"
	urlCheckPerm := url + "/check_permission"

	router.Handle(urlServices, PermissionServiceServicesHandler()).Methods("POST")
	router.Handle(urlMethods, PermissionServiceMethodsHandler()).Methods("POST")
	router.Handle(urlPermissions, PermissionServicePermissionsHandler()).Methods("POST")
	router.Handle(urlCheckPerm, PermissionServiceCheckPermissionHandler()).Methods("POST")

	//router.Handle(url, PermissionServiceHandler()).Methods("POST")
	//router.Handle("/gde/check_permission", PermissionServiceCheckPermissionHandler()).Methods("POST")

	log.Printf("http server serving on %s port %s", ip, port)
	log.Fatal(http.ListenAndServe(ip+":"+port, router))
}

