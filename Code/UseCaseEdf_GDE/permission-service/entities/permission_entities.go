package entities

import (
	"gde-common/common"
	"time"
)

type Permission struct {
	ID int64 `json:"permission_id" gorm:"primary_key;AUTO_INCREMENT"`
	GroupID int64 `json:"group_id"`
	MethodID int64 `json:"method_id"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

type Service struct {
	ID int64 `json:"service_id" gorm:"primary_key;AUTO_INCREMENT"`
	Name string `json:"service_name"`
	Description string `json:"service_description"`
}

type Method struct {
	ID int64 `json:"method_id" gorm:"primary_key;AUTO_INCREMENT"`
	MethodIndex common.MethodName `json:"method_index"`
	Name string `json:"method_name"`
	Description string `json:"method_description"`
	ServiceID int64 `json:"service_id"`
}
