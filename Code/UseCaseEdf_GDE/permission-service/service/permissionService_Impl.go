package service

import (
	"fmt"
	"gde-common/common"
	"log"
	"permission-service/db"
	"permission-service/entities"
)

type PermissionServiceImpl struct{}

func (ps *PermissionServiceImpl) CreateService(service *entities.Service) (*entities.Service, error) {
	return db.DB.CreateService(service)
}

func (ps *PermissionServiceImpl) DeleteService(id int64) (bool, error) {
	return db.DB.DeleteService(id)
}

func (ps *PermissionServiceImpl) UpdateService(service *entities.Service) (*entities.Service, error) {
	return db.DB.UpdateService(service)
}

func (ps *PermissionServiceImpl) FindServiceByID(id int64) (entities.Service, error) {
	return db.DB.FindServiceByID(id)
}

func (ps *PermissionServiceImpl) FindServiceByName(name string) (entities.Service, error) {
	return db.DB.FindServiceByName(name)
}

func (ps *PermissionServiceImpl) ListServices() ([]entities.Service, error) {
	return db.DB.ListServices()
}

func (ps *PermissionServiceImpl) CreateMethod(method *entities.Method) (*entities.Method, error) {
	return db.DB.CreateMethod(method)
}

func (ps *PermissionServiceImpl) DeleteMethod(id int64) (bool, error) {
	return db.DB.DeleteMethod(id)
}

func (ps *PermissionServiceImpl) UpdateMethod(method *entities.Method) (*entities.Method, error) {
	return db.DB.UpdateMethod(method)
}

func (ps *PermissionServiceImpl) FindMethodByID(id int64) (entities.Method, error) {
	return db.DB.FindMethodByID(id)
}

func (ps *PermissionServiceImpl) FindMethodByName(name string) (entities.Method, error) {
	return db.DB.FindMethodByName(name)
}

func (ps *PermissionServiceImpl) FindMethodByServiceNameMethodIndex(serviceName string, methodIndex common.MethodName) (entities.Method, error) {
	return db.DB.FindMethodByServiceNameMethodIndex(serviceName, methodIndex)
}

func (ps *PermissionServiceImpl) FindMethodsByService(serviceId int64) ([]entities.Method, error) {
	return db.DB.FindMethodsByService(serviceId)
}

func (ps *PermissionServiceImpl) ListMethods() ([]entities.Method, error) {
	return db.DB.ListMethods()
}

func (ps *PermissionServiceImpl) CreatePermission(permission *entities.Permission) (*entities.Permission, error) {
	return db.DB.CreatePermission(permission)
}

func (ps *PermissionServiceImpl) DeletePermission(id int64) (bool, error) {
	return db.DB.DeletePermission(id)
}

func (ps *PermissionServiceImpl) UpdatePermission(permission *entities.Permission) (*entities.Permission, error) {
	return db.DB.UpdatePermission(permission)
}

func (ps *PermissionServiceImpl) FindPermissionByID(id int64) (entities.Permission, error) {
	return db.DB.FindPermissionByID(id)
}

func (ps *PermissionServiceImpl) FindPermissionsByGroupID(groupId int64, token string) ([]entities.Permission, error) {
	// check group id valid
	if ok, err := CheckGroupIDValid(groupId, token); !ok || err != nil { // request to User Service
		return nil, fmt.Errorf("group id invalid")
	}
	return db.DB.FindPermissionByGroupID(groupId)
}

func (ps *PermissionServiceImpl) FindPermissionsByUserID(userId int64, token string) ([]entities.Permission, error) {
	permissions := make([]entities.Permission, 0)
	groupIds, err := GetUserGroups(userId, token)
	if err != nil {
		log.Printf("get groups ids by user id error")
		return nil, err
	}
	for _, groupId := range groupIds {
		groupPermissions, err := ps.FindPermissionsByGroupID(groupId, token)
		if err != nil {
			return nil, err
		}
		for _, permission := range groupPermissions {
			permissions = append(permissions, permission)
		}
	}
	return permissions, nil
}

func (ps *PermissionServiceImpl) FindPermissionsByMethodID(methodId int64) ([]entities.Permission, error) {
	return db.DB.FindPermissionByMethodID(methodId)
}

func (ps *PermissionServiceImpl) ListPermissions() ([]entities.Permission, error) {
	return db.DB.ListPermissions()
}

func (ps *PermissionServiceImpl) HasGroupThePermissionOn(groupId int64, serviceName string, methodIndex common.MethodName, token string) (bool, error) {
	if ok, err := CheckGroupIDValid(groupId, token); !ok || err != nil {
		return false, fmt.Errorf("group id invalid")
	}
	return db.DB.HasGroupThePermissionOn(groupId, serviceName, methodIndex)
}

func (ps *PermissionServiceImpl) HasUserThePermissionOn(userId int64, serviceName string, methodIndex common.MethodName, token string) (bool, error) {
	groupIds, err := GetUserGroups(userId, token)
	if err != nil {
		log.Printf("get groups ids by user id error")
		return false, err
	}
	for _, groupId := range groupIds {
		ok, err := ps.HasGroupThePermissionOn(groupId, serviceName, methodIndex, token)
		if err != nil {
			return false, err
		}
		if ok {
			return true, nil
		}
	}
	return false, err
}

func (ps *PermissionServiceImpl) CheckUserPermission(login string, serviceName string, methodIndex common.MethodName, token string) (bool, error) {
	userId, err := FindUserIDByName(login, token)
	groupIds, err := GetUserGroups(userId, token)
	if err != nil {
		log.Printf("get groups ids by user id error")
		return false, err
	}
	for _, groupId := range groupIds {
		ok, err := ps.HasGroupThePermissionOn(groupId, serviceName, methodIndex, token)
		if err != nil {
			return false, err
		}
		if ok {
			return true, nil
		}
	}
	return false, err
}

