package server

import (
	"encoding/json"
	"file-service/entities"
	"file-service/service"
	"fmt"
	"gde-common/common"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func FileServiceFilesHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var fs service.FileService = &service.FileServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.FILE_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.CREATE_FILE:
					file := entities.GDEFile{}
					json.Unmarshal(command.Data, &file)
					if result, err := fs.CreateFile(&file); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_FILE:
					file := entities.GDEFile{}
					json.Unmarshal(command.Data, &file)
					if result, err := fs.UpdateFile(&file); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_FILE:
					fileId, err := command.GetLong("file_id")
					if err = fs.DeleteFile(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.RESTORE_FILE:
					fileId, err := command.GetLong("file_id")
					if err = fs.RestoreFile(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_FILE_BY_ID:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.FindFileById(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.READ_FILE:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.ReadFile(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_NAME:
					fileName, _ := command.GetString("file_name")
					if result, err := fs.FindFilesByName(fileName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_CREATTION_DATE:
					date, _ := command.GetTime("created_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := fs.FindFilesByCreationDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_DELETION_DATE:
					date, _ := command.GetTime("deleted_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := fs.FindFilesByDeletionDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_UPDATE_DATE:
					date, _ := command.GetTime("updated_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := fs.FindFilesByUpdateDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_RESTORATION_DATE:
					date, _ := command.GetTime("restorated_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := fs.FindFilesByCreationDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_FILES:
					if result, err := fs.ListFiles(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_TO_INDEX:
					if result, err := fs.FindFilesToIndex(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_DELETED:
					if result, err := fs.FindFilesDeleted(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.SET_FILE_VALID:
					fileId, _ := command.GetLong("file_id")
					isValid, _ := command.GetBool("is_valid")
					if err := fs.SetValid(fileId, isValid); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.IS_FILE_VALID:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.IsValid(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.SET_FILE_INDEXED:
					fileId, _ := command.GetLong("file_id")
					isIndexed, _ := command.GetBool("is_indexed")
					if err := fs.SetIndexed(fileId, isIndexed); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.IS_FILE_INDEXED:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.IsIndexed(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.FILE_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func FileServiceFileLockHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var fs service.FileService = &service.FileServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.FILE_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.OPEN_FILE:
					// userId, _ := command.GetLong("user_id")
					userId, _ := service.FindUserIDByName(login, token)
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.OpenFile(userId, fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CLOSE_FILE:
					// userId, _ := command.GetLong("user_id")
					userId, _ := service.FindUserIDByName(login, token)
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.CloseFile(userId, fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.FILE_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func FileServiceChunksHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var fs service.FileService = &service.FileServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.FILE_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.CREATE_CHUNK:
					chunk := entities.Chunk{}
					json.Unmarshal(command.Data, &chunk)
					if result, err := fs.CreateChunk(&chunk); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.READ_CHUNK:
					chunkId, _ := command.GetLong("chunk_id")
					if result, err := fs.ReadChunk(chunkId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.FILE_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

func FileServiceFileChunksHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var fs service.FileService = &service.FileServiceImpl{}
		// parse command
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}
		// authentication
		token := service.GetToken(r)
		login, err := service.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := service.CheckPermission(login, common.FILE_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {
				switch command.MethodName {
				case common.ATTACH_CHUNK_TO_FILE:
					fileId, _ := command.GetLong("file_id")
					chunkId, _ := command.GetLong("chunk_id")
					if err := fs.AttachChunkToFile(fileId, chunkId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.DETACH_CHUNK_FROM_FILE:
					fileId, _ := command.GetLong("file_id")
					chunkId, _ := command.GetLong("chunk_id")
					if err := fs.DetachChunkFromFile(fileId, chunkId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_CHUNKS_BY_FILE_ID:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.FindChunksByFileId(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_CHUNKS_INFO_BY_FILE_ID:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.FindChunksInfoByFileId(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.FILE_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
}

/*
func FileServiceHandler()  http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var fs service.FileService = &service.FileServiceImpl{}
		resp := &common.CommandResult{}
		command, err := GetCommand(r)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
			SendResult(w, resp)
			return
		}

		token := common.GetToken(r)
		login, err := common.GetLogin(token)
		if err != nil {
			resp.Code = common.ERROR
			resp.Message = err.Error()
		} else { // check permission
			ok, err := common.CheckPermission(login, common.FILE_SERVICE, command.MethodName, token)
			if !ok || err != nil {
				log.Printf("Check user permission error")
				resp.Code = common.ERROR
				resp.Message = err.Error()
			} else {

				switch command.MethodName {
				case common.CREATE_FILE:
					file := entities.GDEFile{}
					json.Unmarshal(command.Data, &file)
					if result, err := fs.CreateFile(&file); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.UPDATE_FILE:
					file := entities.GDEFile{}
					json.Unmarshal(command.Data, &file)
					if result, err := fs.UpdateFile(&file); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.DELETE_FILE:
					fileId, err := command.GetLong("file_id")
					if err = fs.DeleteFile(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.RESTORE_FILE:
					fileId, err := command.GetLong("file_id")
					if err = fs.RestoreFile(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_FILE_BY_ID:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.FindFileById(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.READ_FILE:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.ReadFile(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_NAME:
					fileName, _ := command.GetString("file_name")
					if result, err := fs.FindFilesByName(fileName); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_CREATTION_DATE:
					date, _ := command.GetTime("created_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := fs.FindFilesByCreationDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_DELETION_DATE:
					date, _ := command.GetTime("deleted_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := fs.FindFilesByDeletionDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_UPDATE_DATE:
					date, _ := command.GetTime("updated_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := fs.FindFilesByUpdateDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_BY_RESTORATION_DATE:
					date, _ := command.GetTime("restorated_at")
					comparator, _ := command.GetInt("comparator")
					if result, err := fs.FindFilesByCreationDate(&date, common.DateComparator(comparator)); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.LIST_FILES:
					if result, err := fs.ListFiles(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_TO_INDEX:
					if result, err := fs.FindFilesToIndex(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_FILES_DELETED:
					if result, err := fs.FindFilesDeleted(); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.SET_FILE_VALID:
					fileId, _ := command.GetLong("file_id")
					isValid, _ := command.GetBool("is_valid")
					if err := fs.SetValid(fileId, isValid); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.IS_FILE_VALID:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.IsValid(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.SET_FILE_INDEXED:
					fileId, _ := command.GetLong("file_id")
					isIndexed, _ := command.GetBool("is_indexed")
					if err := fs.SetIndexed(fileId, isIndexed); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.IS_FILE_INDEXED:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.IsIndexed(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.OPEN_FILE:
					// userId, _ := command.GetLong("user_id")
					userId, _ := common.FindUserIDByName(login, token)
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.OpenFile(userId, fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CLOSE_FILE:
					// userId, _ := command.GetLong("user_id")
					userId, _ := common.FindUserIDByName(login, token)
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.CloseFile(userId, fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.CREATE_CHUNK:
					chunk := entities.Chunk{}
					json.Unmarshal(command.Data, &chunk)
					if result, err := fs.CreateChunk(&chunk); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.READ_CHUNK:
					chunkId, _ := command.GetLong("chunk_id")
					if result, err := fs.ReadChunk(chunkId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.ATTACH_CHUNK_TO_FILE:
					fileId, _ := command.GetLong("file_id")
					chunkId, _ := command.GetLong("chunk_id")
					if err := fs.AttachChunkToFile(fileId, chunkId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.DETACH_CHUNK_FROM_FILE:
					fileId, _ := command.GetLong("file_id")
					chunkId, _ := command.GetLong("chunk_id")
					if err := fs.DetachChunkFromFile(fileId, chunkId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Code = common.OK
					}
				case common.FIND_CHUNKS_BY_FILE_ID:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.FindChunksByFileId(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				case common.FIND_CHUNKS_INFO_BY_FILE_ID:
					fileId, _ := command.GetLong("file_id")
					if result, err := fs.FindChunksInfoByFileId(fileId); err != nil {
						resp.Code = common.ERROR
						resp.Message = err.Error()
					} else {
						resp.Data, _ = json.Marshal(result)
						resp.Code = common.OK
					}
				default:
					err := fmt.Errorf("invalid method %v : %v", common.FILE_SERVICE, command.MethodName)
					resp.Code = common.ERROR
					resp.Message = err.Error()
				}
			}
		}
		// finally
		SendResult(w, resp)
	})
	
}

func FindFileById() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id, err := strconv.ParseInt(vars["file_id"], 10, 64)
		if err != nil {
			respondError(w, http.StatusInternalServerError, err.Error())
			return
		}
		file, err := fs.FindFileById(id)
		if err != nil || file.ID == 0 {
			respondError(w, http.StatusNotFound, err.Error())
			return
		}
		respondJSON(w, http.StatusOK, file)
	})
}

func IsValid() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id, err := strconv.ParseInt(vars["file_id"], 10, 64)
		if err != nil {
			respondError(w, http.StatusInternalServerError, err.Error())
			return
		}
		valid, err := fs.IsValid(id)
		if err != nil {
			respondError(w, http.StatusNotFound, err.Error())
			return
		}
		respondJSON(w, http.StatusOK, valid)
	})
}

func CreateFile() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		file := entities.GDEFile{}
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&file); err != nil {
			respondError(w, http.StatusBadRequest, err.Error())
			return
		}
		defer r.Body.Close()
		result, err := fs.CreateFile(&file)
		if err != nil {
			respondError(w, http.StatusInternalServerError, err.Error())
			return
		}
		respondJSON(w, http.StatusCreated, result)
	})
}
*/

