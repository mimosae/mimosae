package server

type FileServer interface {
	Starter(ip string, port string)
}
