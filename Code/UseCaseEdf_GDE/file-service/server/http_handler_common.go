package server

import (
	"encoding/json"
	"gde-common/common"
	"log"
	"net/http"
)

func SendResult(w http.ResponseWriter, result *common.CommandResult) {
	response, err := json.Marshal(result)
	if err != nil {
		log.Fatal("json Marshal error")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte(response))
}

func GetCommand(r *http.Request) (*common.Command, error) {
	command := &common.Command{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&command); err != nil {
		log.Fatalf("decode command error")
		return nil, err
	}
	defer r.Body.Close()
	return command, nil
}


func respondJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

// respondError makes the error response with payload as json format
func respondError(w http.ResponseWriter, code int, message string) {
	respondJSON(w, code, map[string]string{"error": message})
}
