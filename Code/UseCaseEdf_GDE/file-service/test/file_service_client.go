package client

import (
	"bytes"
	"encoding/json"
	"file-service/config"
	"file-service/entities"
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"net/http"
	"time"
)

type FileServiceClient struct {
	ServiceURL string
	HTTPClient *http.Client
}

func NewClient() *FileServiceClient {
	return &FileServiceClient{
		ServiceURL:    "http://" + config.DefaultIP + ":" + config.ClientPort + "/gde/" + common.FILE_SERVICE, //http://localhost:8080/gde/FileService
		HTTPClient: &http.Client{
			Timeout: time.Minute,
		},
	}
}

func (c *FileServiceClient) PostCommand(command *common.Command, object interface{}) *common.CommandResult {
	// send Command
	if object != nil {
		command.Data, _ = json.Marshal(object)
	}
	body, _ := json.Marshal(command)
	req, _ := http.NewRequest(http.MethodPost, c.ServiceURL, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	// Set Authentication
	resp, _ := c.HTTPClient.Do(req)
	defer resp.Body.Close()
	// Parse CommandResult
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	//err := json.NewDecoder(resp.Body).Decode(&result)
	return &result
}

func (c *FileServiceClient) CreateFile(file *entities.GDEFile) (*entities.GDEFile, error)  {
	command := common.NewCommand(common.CREATE_FILE)
	command.Data, _ = json.Marshal(file)
	result := c.PostCommand(command, file)
	if result.Code == common.OK {
		newFile := &entities.GDEFile{}
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&newFile)
		return newFile, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) UpdateFile(file *entities.GDEFile) (*entities.GDEFile, error){
	command := common.NewCommand(common.UPDATE_FILE)
	command.Data, _ = json.Marshal(file)
	result := c.PostCommand(command, file)
	if result.Code == common.OK {
		updateFile := &entities.GDEFile{}
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&updateFile)
		return updateFile, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) DeleteFile(id int64) error {
	command := common.NewCommand(common.DELETE_FILE)
	command.SetLong("file_id", id)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		return nil
	} else {
		return fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) RestoreFile(id int64) error {
	command := common.NewCommand(common.RESTOREFILE)
	command.SetLong("file_id", id)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		return nil
	} else {
		return fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) FindFileById(id int64) (entities.GDEFile, error) {
	command := common.NewCommand(common.FIND_FILE_BY_ID)
	command.SetLong("file_id", id)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		foundFile := entities.GDEFile{}
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&foundFile)
		return foundFile, nil
	} else {
		return entities.GDEFile{}, fmt.Errorf(result.Message)
	}
}
func (c *FileServiceClient) ReadFile(id int64) (entities.GDEFile, error) {
	command := common.NewCommand(common.READ_CHUNK)
	command.SetLong("file_id", id)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		foundFile := entities.GDEFile{}
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&foundFile)
		return foundFile, nil
	} else {
		return entities.GDEFile{}, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) FindFilesByName(name string) ([]entities.GDEFile, error) {
	command := common.NewCommand(common.READ_CHUNK)
	command.SetString("file_name", name)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		var list []entities.GDEFile
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&list)
		return list, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) ListFiles() ([]entities.GDEFile, error) {
	command := common.NewCommand(common.LIST_FILES)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		var list []entities.GDEFile
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&list)
		return list, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) FindFilesDeleted() ([]entities.GDEFile, error) {
	command := common.NewCommand(common.FIND_FILES_DELETED)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		var list []entities.GDEFile
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&list)
		return list, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) SetValid(fileId int64, isValid bool) error {
	command := common.NewCommand(common.SET_FILE_VALID)
	command.SetLong("file_id", fileId)
	command.SetBool("is_valid", isValid)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		return nil
	} else {
		return fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) IsValid(fileId int64) (bool, error) {
	command := common.NewCommand(common.IS_FILE_VALID)
	command.SetLong("file_id", fileId)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		var valid bool
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&valid)
		return valid, nil
	} else {
		return false, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) OpenFile(userId int64, fileId int64) (bool, error) { // no userid but login
	command := common.NewCommand(common.OPEN_FILE)
	command.SetLong("user_id", userId)
	command.SetLong("file_id", fileId)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		var ok bool
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&ok)
		return ok, nil
	} else {
		return false, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) CloseFile(userId int64, fileId int64) (bool, error) {
	command := common.NewCommand(common.CLOSE_FILE)
	command.SetLong("user_id", userId)
	command.SetLong("file_id", fileId)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		var ok bool
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&ok)
		return ok, nil
	} else {
		return false, fmt.Errorf(result.Message)
	}
}

//Chunks
func (c *FileServiceClient) CreateChunk(chunk *entities.Chunk) (*entities.Chunk, error) {
	command := common.NewCommand(common.CREATE_CHUNK)
	command.Data, _ = json.Marshal(chunk)
	result := c.PostCommand(command, chunk)
	if result.Code == common.OK {
		newChunk := &entities.Chunk{}
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&newChunk)
		return newChunk, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) ReadChunk(chunkId int64) (entities.Chunk, error) {
	command := common.NewCommand(common.READ_CHUNK)
	command.SetLong("chunk_id", chunkId)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		readChunk := entities.Chunk{}
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&readChunk)
		return readChunk, nil
	} else {
		return entities.Chunk{}, fmt.Errorf(result.Message)
	}
}

//File-Chunk association
func (c *FileServiceClient) AttachChunkToFile(fileId int64, chunkId int64) error {
	command := common.NewCommand(common.ATTACH_CHUNK_TO_FILE)
	command.SetLong("file_id", fileId)
	command.SetLong("chunk_id", chunkId)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		return nil
	} else {
		return fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) DetachChunkFromFile(fileId int64, chunkId int64) error {
	command := common.NewCommand(common.DETACH_CHUNK_FROM_FILE)
	command.SetLong("file_id", fileId)
	command.SetLong("chunk_id", chunkId)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		return nil
	} else {
		return fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) FindChunksByFileId(fileId int64) ([]entities.Chunk, error) {
	command := common.NewCommand(common.FIND_CHUNKS_BY_FILE_ID)
	command.SetLong("file_id", fileId)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		var list []entities.Chunk
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&list)
		return list, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}

func (c *FileServiceClient) FindChunksInfoByFileId(fileId int64) ([]entities.ChunkInfo, error) {
	command := common.NewCommand(common.FIND_CHUNKS_INFO_BY_FILE_ID)
	command.SetLong("file_id", fileId)
	result := c.PostCommand(command, nil)
	if result.Code == common.OK {
		var list []entities.ChunkInfo
		json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&list)
		return list, nil
	} else {
		return nil, fmt.Errorf(result.Message)
	}
}




