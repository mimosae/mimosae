package client

import (
	"file-service/entities"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestCreateFile(t *testing.T) {
	//fmt.Println(t.Name())
	c := NewClient()
	file := &entities.GDEFile{}
	file.Name = "test.txt"
	createdFile, err := c.CreateFile(file)
	assert.Nil(t, err)
	assert.NotNil(t, createdFile)
	assert.NotEqual(t, createdFile.ID, 0)
	assert.Equal(t, createdFile.Name, file.Name)
}

func TestFindFileById(t *testing.T) {
	c := NewClient()
	file := &entities.GDEFile{}
	file.Name = "test.txt"
	createdFile, _ := c.CreateFile(file)
	readFile, err := c.FindFileById(createdFile.ID)
	assert.Nil(t, err)
	assert.NotNil(t, readFile)
	assert.Equal(t, createdFile.ID, readFile.ID)
	assert.Equal(t, createdFile.CreatedAt.Format(time.RFC3339), readFile.CreatedAt.Format(time.RFC3339))
	assert.Equal(t, createdFile.UpdatedAt.Format(time.RFC3339), readFile.UpdatedAt.Format(time.RFC3339))
}

func TestIsValid(t *testing.T) {
	c := NewClient()
	file := &entities.GDEFile{}
	file.Name = "test.txt"
	createdFile, _ := c.CreateFile(file)
	isValid, err := c.IsValid(createdFile.ID)
	assert.Nil(t, err)
	assert.IsType(t, true, isValid)
}

/*
func main() {
	file := &entities.GDEFile{Name: "file1"}

	const uriBase string = "http://" + config.DefaultIP + ":" + config.DefaultPort
	SendHttpRequestAndPrint(http.MethodPost, uriBase+"/fs/file/create", file)
}

func SendHttpRequestAndPrint(httpMethod string, uri string, body interface{}) {
	var httpReq *http.Request
	var httpResp *http.Response
	var err error
	if httpMethod == "GET" {
		httpResp, err = http.Get(uri)
		if err != nil {
			log.Fatalf("client.Get() failed with '%s'\n", err)
		}
	} else {
		bodyJson, err := json.Marshal(body)
		if err != nil {
			log.Fatalf("json.Marshal() failed with '%s'\n", err)
		}
		httpReq, err = http.NewRequest(httpMethod, uri, bytes.NewBuffer(bodyJson))
		if err != nil {
			log.Fatalf("http.NewRequest() failed with '%s'\n", err)
		}
		httpReq.Header.Set("Content-Type", "application/json; charset=utf-8")
		client := &http.Client{}
		httpResp, err = client.Do(httpReq)
		if err != nil {
			log.Fatalf("client.Do() failed with '%s'\n", err)
		}
	}

	defer httpResp.Body.Close()

	respBody, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		log.Fatalf("ioutil.ReadAll failed with '%s'\n", err)
	}
	fmt.Println("Response status:", httpResp.Status)
	fmt.Println(string(respBody))
}
 */
