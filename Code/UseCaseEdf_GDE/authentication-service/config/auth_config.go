package config

import "gde-common/common"

//DefaultPort is defaut server post number for connection
const DefaultPort = common.AuthServicePort

//DefaultProtocol is defaut protocol used in communcation
const DefaultProtocol = "HTTP"

//ClientPort is default client port
const ClientPort = "8080"

//DefaultIP is default ip
const DefaultIP = "127.0.0.1"
