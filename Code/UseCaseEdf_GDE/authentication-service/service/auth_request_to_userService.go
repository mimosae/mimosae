package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func CheckPassword(creds Credentials) (bool, error) {
	// create http client
	client := &http.Client{Timeout: time.Minute}
	// create Command as json
	userServiceIP := os.Getenv("UserServiceIP")
	if userServiceIP == "" {
		userServiceIP = common.LocalHostIP
	}
	url := userServiceIP + ":" + common.UserServicePort + "/gde/" + common.USER_SERVICE + "/check_password"
	fmt.Println(url)
	command := common.NewCommand(common.CHECK_PASSWORD)
	command.SetString("login", creds.Login)
	command.SetString("password", creds.Password)
	body, _ := json.Marshal(command)
	// Send Command
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	// Get CommandResult
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		// Get CommandResult.Data
		var check bool
		err := json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&check)
		return check, err
	} else {
		return false, fmt.Errorf(result.Message)
	}
}
