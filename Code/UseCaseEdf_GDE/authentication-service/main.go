package main

import (
	"authentication-service/config"
	"authentication-service/server"
	"os"
)

func main() {
	server := server.NewAuthHTTPServer(config.DefaultProtocol)
	ip := os.Getenv("IP")
	if ip == "" {
		ip = config.DefaultIP
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = config.DefaultPort
	}
	server.Starter(ip, port)
}

