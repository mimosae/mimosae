#! /bin/bash

GDE_COMMON=gde-common
services="authentication-service file-service permission-service project-service user-service"

for i in $services; do
	if [ ! -d "$i/$GDE_COMMON" ]; then
	  echo "copy $GDE_COMMON into $i/$GDE_COMMON"
	  cp -r "$GDE_COMMON" "$i/$GDE_COMMON"
	fi
done
