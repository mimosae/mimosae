package service

import (
	"fmt"
	"gde-common/common"
	"log"
	"project-service/db"
	"project-service/entities"
	"time"
)

//ProjectServiceImpl ...
type ProjectServiceImpl struct{}

//ListProjects ...
func (ps *ProjectServiceImpl) ListProjects() ([]entities.Project, error) {
	return db.DB.FindProjects()
}

//CreateProject ...
func (ps *ProjectServiceImpl) CreateProject(p entities.Project) (*entities.Project, error) {
	return db.DB.InsertProject(p)
}

//FindProjectById ...
func (ps *ProjectServiceImpl) FindProjectById(id int64) (entities.Project, error) {
	return db.DB.FindProjectByID(id)
}

//UpdateProject ...
func (ps *ProjectServiceImpl) UpdateProject(p entities.Project) (*entities.Project, error) {
	return db.DB.UpdateProject(p)
}

//DeleteProject ...
func (ps *ProjectServiceImpl) DeleteProject(id int64) error {
	return db.DB.DeleteProject(id)
}

//RestoreProject ...
func (ps *ProjectServiceImpl) RestoreProject(id int64) error {
	return db.DB.RestoreProject(id)
}

//FindProjectsByName ...
func (ps *ProjectServiceImpl) FindProjectsByName(name string) ([]entities.Project, error) {
	return db.DB.FindProjectsByName(name)
}

//FindProjectsByCreationDate ...
func (ps *ProjectServiceImpl) FindProjectsByCreationDate(creationDate *time.Time, comparator common.DateComparator) ([]entities.Project, error) {
	return db.DB.FindProjectsByCreationDate(creationDate, comparator)
}

//FindProjectsByDeletionDate ...
func (ps *ProjectServiceImpl) FindProjectsByDeletionDate(deletionDate *time.Time, comparator common.DateComparator) ([]entities.Project, error) {
	return db.DB.FindProjectsByDeletionDate(deletionDate, comparator)
}

//FindProjectsByUpdateDate ...
func (ps *ProjectServiceImpl) FindProjectsByUpdateDate(updateDate *time.Time, comparator common.DateComparator) ([]entities.Project, error) {
	return db.DB.FindProjectsByUpdateDate(updateDate, comparator)
}

//FindProjectsByRestorationDate ...
func (ps *ProjectServiceImpl) FindProjectsByRestorationDate(restorationDate *time.Time, comparator common.DateComparator) ([]entities.Project, error) {
	return db.DB.FindProjectsByRestorationDate(restorationDate, comparator)
}

//GetProjectLocker ...
func (ps *ProjectServiceImpl) GetProjectLocker(projectId int64) (int64, error) {
	return db.DB.FindProjectLocker(projectId)
}

//IsProjectLocked ...
func (ps *ProjectServiceImpl) IsProjectLocked(userId, projectId int64) (bool, error) {
	return db.DB.IsProjectLocked(userId, projectId)
}

func (ps *ProjectServiceImpl) SetProjectLockState(userId int64, projectId int64, locked bool) error {
	return db.DB.SetProjectLockState(userId, projectId, locked)
}

//ListProjectFiles ...
func (ps *ProjectServiceImpl) ListProjectFiles(projectId int64, token string) ([]common.GDEFileTO, error) {
	log.Printf("Listing project %v's files\n", projectId)
	p, err := db.DB.FindProjectByID(projectId)
	if err != nil {
		return nil, err
	}
	files := make([]common.GDEFileTO, 0)
	for _, fileID := range p.Files {
		file, err := GetFileById(fileID, token)
		if err != nil {
			return nil, err
		}
		files = append(files, file)
	}
	return files, nil
}

//AttachFileToProject ...
func (ps *ProjectServiceImpl) AttachFileToProject(userId int64, projectId int64, fileId int64, token string) error {
	test, err := db.DB.IsProjectLocked(userId, projectId)
	if err != nil {
		return err
	}
	if test {
		return fmt.Errorf("This project is Locked")
	}
	p, err := db.DB.FindProjectByID(projectId)
	if err != nil {
		return err
	}
	ok, err := CheckFileIsValid(fileId, token)
	if err != nil || !ok {
		log.Printf("check file is valid error")
		return err
	}
	if ok {
		for _, f := range p.Files {
			if f == fileId {
				return fmt.Errorf("Already attached")
			}
		}
		p.Files = append(p.Files, fileId)
		_, err = db.DB.UpdateProject(p)
		if err != nil {
			panic(err)
		}
		log.Printf("Attach file %v to project %v\n", fileId, projectId)
	} else {
		return fmt.Errorf("This file does not exist")
	}
	return nil
}

//DetachFileFromProject ...
func (ps *ProjectServiceImpl) DetachFileFromProject(userId int64, projectId int64, fileId int64, token string) error {
	test, err := db.DB.IsProjectLocked(userId, projectId)
	if err != nil {
		return err
	}
	if test {
		return fmt.Errorf("This project is Locked")
	}
	p, err := db.DB.FindProjectByID(projectId)
	if err != nil {
		return err
	}
	ok, err := CheckFileIsValid(fileId, token)
	if err != nil || !ok {
		log.Printf("check file is valid error")
		return err
	}
	if ok {
		for i, file := range p.Files {
			if fileId == file {
				p.Files[i] = p.Files[len(p.Files)-1]
				p.Files = p.Files[:len(p.Files)-1]
				_, err = db.DB.UpdateProject(p)
				if err != nil {
					panic(err)
				}
				log.Printf("Detach file %v from project %v\n", fileId, projectId)
				return nil
			}
		}
	} else {
		return fmt.Errorf("This file does not exist")
	}
	return nil
}

