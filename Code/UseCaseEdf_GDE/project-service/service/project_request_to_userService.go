package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gde-common/common"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func FindUserIDByName(name string, token string) (int64, error) {
	// create http client
	client := &http.Client{Timeout: time.Minute}
	// create Command as json
	userServiceIP := os.Getenv("UserServiceIP")
	if userServiceIP == "" {
		userServiceIP = common.LocalHostIP
	}
	url := userServiceIP + ":" + common.UserServicePort + "/gde/" + common.USER_SERVICE + "/users"
	command := common.NewCommand(common.FIND_USER_BY_NAME)
	command.SetString("user_name", name)
	body, _ := json.Marshal(command)
	// Send Command
	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Authorization", token)
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	// Get CommandResult
	var result common.CommandResult
	body, _ = ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &result)
	if result.Code == common.OK {
		user := common.UserTO{}
		err := json.NewDecoder(bytes.NewBuffer(result.Data)).Decode(&user)
		return user.ID, err
	} else {
		return -1, fmt.Errorf(result.Message)
	}
}
