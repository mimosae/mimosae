package main

import (
	"os"
	"project-service/config"
	"project-service/db"
	"project-service/server"
)

func main() {
	ip := os.Getenv("IP")
	if ip == "" {
		ip = config.DefaultIP
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = config.DefaultPort
	}
	server := server.NewProjectHTTPServer(config.DefaultProtocol)
	db.DB = &db.ProjectPostgresDB{}
	server.Starter(ip, port)
}
