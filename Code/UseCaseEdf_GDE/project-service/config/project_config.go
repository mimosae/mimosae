package config

import "gde-common/common"

//DefaultPort is defaut server port number for connection
const DefaultPort = common.ProjectServicePort

//DefaultProtocol is defaut protocol used in communcation
const DefaultProtocol = "HTTP"

//DefaultIP is default ip
const DefaultIP = common.LocalHostIP

//DbUser user of db
const DbUser string = "postgres"

//DbPassword password of db
const DbPassword string = "postgres"

//DbName name of db
const DbName string = "gde_project_service"

//DbHost host of db
const DbHost string = "127.0.0.1"

//DbPort port of db
const DbPort string = "5432"

//FSIP is file service ip
const FSIP = common.LocalHostIP

//FSPort is file service server port number for connection
const FSPort = common.FileServicePort
