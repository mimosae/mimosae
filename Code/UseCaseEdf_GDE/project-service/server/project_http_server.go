package server

import (
	"gde-common/common"
	"project-service/db"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	"net/http"
)

type ProjectHTTPServer struct {
	Type string
}

func NewProjectHTTPServer(t string) *ProjectHTTPServer {
	return &ProjectHTTPServer{
		Type: t,
	}
}

func (hs ProjectHTTPServer) Starter(ip string, port string) {
	database := db.DB
	router := mux.NewRouter()

	log.Printf("Init Database...")
	database.ConnectDB()
	database.InitDB()
	defer database.Close()

	url := "/gde/" + common.PROJECT_SERVICE
	urlProjects := url + "/projects"
	urlProjectLock := url + "/project_lock"
	urlProjectFiles := url + "/project_files"

	router.Handle(urlProjects, ProjectServiceProjectsHandler()).Methods("POST")
	router.Handle(urlProjectLock, ProjectServiceProjectLockHandler()).Methods("POST")
	router.Handle(urlProjectFiles, ProjectServiceProjectFilesHandler()).Methods("POST")

	//router.Handle(url, ProjectServiceHandler()).Methods("POST")

	log.Printf("http server serving on %s port %s", ip, port)
	log.Fatal(http.ListenAndServe(ip+":"+port, router))

}
