#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Viktor Colas, Tsimafei Liashkevich, Denis Conan

chmod u+x scripts/install-docker.sh
chmod u+x scripts/install-go.sh
chmod u+x scripts/install-minikube.sh
chmod u+x scripts/install-utils.sh
chmod u+x scripts/waiting-for-response.sh

./scripts/install-docker.sh
./scripts/install-go.sh

./scripts/install-minikube.sh
./scripts/install-utils.sh

echo "The installation process has ended successfully... you may want to reboot you computer... this is the right time to do it"
exit 0
