#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Denis Conan

RED=$(tput setaf 1)
ORANGE=$(tput setaf 3)
NC=$(tput reset)

SCENARIO_SCRIPT_FOLDER=$PWD
MIMOSAE_FOLDER=$PWD/MIMOSAE
AUTONOMIC_MANAGER_FOLDER=$PWD/AutonomicManager
SCRIPTS_FOLDER=$PWD/scripts
DEV_GDE_FOLDER=$PWD/UseCaseEdf_GDE/

SCENARIO_TEST=$PWD/UseCaseEdf_GDE/gde-client

# Check go installation folder
# if [ ! -d $GOROOT ]; then
#     echo "${RED}-----go isn't installed. Please run install.sh-----"
#     exit 1
# fi

if [ ! -v LPG_PROGRAM ]; then
    echo "No LPG_PROGRAM please export it before executing any scenario"
    exit 1
fi

# if [ ! -v GOROOT ]; then
#     echo "No GOROOT please export it before executing any scenario"
#     exit 1
# fi

if [ ! -v GOPATH ]; then
    echo "No GOPATH please export it before executing any scenario"
    exit 1
fi

rm -rf AutonomicManager/Executor/server/proto

(cd $MIMOSAE_FOLDER; mvn clean)

find . -name "*~" -exec rm {} \;
find . -name .project -exec rm {} \;
find . -name .classpath -exec rm {} \;
find . -name .settings -exec rm -rf {} \;
find . -name .checkstyle -exec rm -rf {} \;

(cd $AUTONOMIC_MANAGER_FOLDER; ./clean-k8-with-images.sh)

(cd $SCENARIO_TEST; go clean)

exit 0
