
module="Executor/common Executor/concurrency Executor/entity Executor/implementation_registry Executor/orchestrator Executor/server"

for i in $module; do
    (cd $i; echo Building $i; go install; go mod tidy)
done
