cd Executor

module="common concurrency config entity implementation_registry orchestrator server"

for i in $module; do
    (cd $i; go clean)
done

go clean
