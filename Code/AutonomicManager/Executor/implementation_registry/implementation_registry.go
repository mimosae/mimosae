/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package implementation_registry

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"tsp.eu/executor/entity"
)

type KubernetesConfigFileTypes string

const (
	Deployment            KubernetesConfigFileTypes = "deployment"
	Service               KubernetesConfigFileTypes = "service"
	Secret                KubernetesConfigFileTypes = "secret"
	PersistentVolume      KubernetesConfigFileTypes = "pv"
	PersistentVolumeClaim KubernetesConfigFileTypes = "pvc"
)

type ConfigFolderType string

const (
	DockerConfigFolder     ConfigFolderType = "Docker"
	KubernetesConfigFolder ConfigFolderType = "Kubernetes"
)

const (
	PATHEDFCASE      = "../../UseCaseEDF_GDE"
	DEVGDE           = "../../UseCaseEdf_GDE"
	K8SCONFIGS       = PATHEDFCASE + "/K8S-configuration"
	PATHLOGGERCONFIG = "../Logger/K8sConfig/"
)

var (
	entityConfigurationMap = map[string]entity.Configuration{}
)

type ImplementationRegistry struct{}

////go:generate mockgen -destination=mocks/mock_registry.go -package=mocks tsp.eu/executor/implementation_registry RegistryHandler
type RegistryHandler interface {
	InitImplementationRegistry(configs ...string) error
	CreateConfiguration(getter entity.MetaDataGetter) (entity.Configuration, error)
}

func (registry *ImplementationRegistry) InitImplementationRegistry(configs ...string) error {
	for _, configPath := range configs {
		entityFolders, err := os.ReadDir(configPath)
		if err != nil {
			panic(err)
		}

		for _, entityFolder := range entityFolders {
			if !entityFolder.IsDir() {
				panic("Should be a folder")
			}

			entityFolderPath := filepath.Join(configPath, entityFolder.Name())
			versionFolders, err := os.ReadDir(entityFolderPath)
			if err != nil {
				panic(err)
			}

			for _, versionFolder := range versionFolders {
				if !versionFolder.IsDir() {
					panic("Should be a folder")
				}

				versionFolderPath := filepath.Join(entityFolderPath, versionFolder.Name())
				config, err := getConfigFiles(versionFolderPath)
				if err != nil {
					panic(err)
				}

				entityConfigurationMap[entity.FormatMetaData(entity.NewMetaData("", versionFolder.Name(), "", entityFolder.Name()), "", "")] = config
			}
		}
	}
	return nil
}

func getConfigFiles(path string) (entity.Configuration, error) {
	var config entity.Configuration

	configFolders, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}

	switch len(configFolders) {
	case 0:
		return nil, fmt.Errorf("%s: Should at least one Folder", path)
	case 1:
		configFolder := configFolders[0]
		if !configFolder.IsDir() {
			return nil, fmt.Errorf("%s: Should be a folder", configFolder.Name())
		}
		if configFolder.Name() != string(KubernetesConfigFolder) {
			return nil, fmt.Errorf("%s: Should contain at least a kubernetes configuration folder", path)
		}
		config, err = getKubernetesYAMLFiles(filepath.Join(path, string(KubernetesConfigFolder)))
		if err != nil {
			return nil, err
		}
	case 2:
		fmt.Sprintln(path)
		yamlConfigWithDocker := &entity.YAMLConfigWithDocker{}
		for _, configFolder := range configFolders {
			if !configFolder.IsDir() {
				return nil, fmt.Errorf("%s: Should be a folder", configFolder.Name())
			}
			fmt.Sprintln(configFolder.Name())
			if configFolder.Name() != string(KubernetesConfigFolder) && configFolder.Name() != string(DockerConfigFolder) {
				return nil, fmt.Errorf("%s: Should contain a Kubernetes or Docker configuration folder", path)
			}

			if configFolder.Name() != string(KubernetesConfigFolder) {
				yamlConfigWithDocker.YamlConfig, err = getKubernetesYAMLFiles(filepath.Join(path, string(KubernetesConfigFolder)))
				if err != nil {
					return nil, err
				}
			}
			if configFolder.Name() != string(DockerConfigFolder) {
				dockerFolder := filepath.Join(path, string(DockerConfigFolder))
				yamlConfigWithDocker.DockerFolder = dockerFolder

				validated, err := validateDockerConfigFolder(dockerFolder)
				if err != nil {
					return nil, err
				}
				if !validated {
					return nil, fmt.Errorf("%s: Docker folder does not contain a Dockerfile", path)
				}

			}
		}

		config = yamlConfigWithDocker
	default:
		return nil, fmt.Errorf("%s: Should have a maximum of 2 Folders", path)
	}

	return config, nil
}

func validateDockerConfigFolder(path string) (bool, error) {
	files, err := os.ReadDir(path)
	if err != nil {
		return false, nil
	}

	containDockerfile := false
	for _, file := range files {
		if !file.IsDir() {
			if file.Name() == "Dockerfile" {
				containDockerfile = true
			}
		}
	}

	return containDockerfile, nil
}

func getKubernetesYAMLFiles(path string) (*entity.YAMLConfig, error) {
	kubernetesYAMLFiles, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}

	pattern, err := regexp.Compile(`(deployment|service|pv|pvc|secret)_(\d+)\.(yml|yaml)`)
	if err != nil {
		return nil, err
	}
	configFilesMap := map[KubernetesConfigFileTypes][]string{
		Deployment:            {},
		Service:               {},
		Secret:                {},
		PersistentVolume:      {},
		PersistentVolumeClaim: {},
	}

	for _, kFile := range kubernetesYAMLFiles {
		if kFile.IsDir() {
			return nil, fmt.Errorf("%s: Should be a file", kFile.Name())
		}
		if match := pattern.MatchString(kFile.Name()); !match {
			return nil, fmt.Errorf("%s: Not the right filename", kFile.Name())
		}

		groups := pattern.FindAllStringSubmatch(kFile.Name(), -1)[0]

		configFilesMap[KubernetesConfigFileTypes(groups[1])] = append(configFilesMap[KubernetesConfigFileTypes(groups[1])], filepath.Join(path, groups[0]))
	}

	for _, v := range configFilesMap {
		err := validateYAMLFileFormat(v)
		if err != nil {
			return nil, err
		}
	}

	configFiles := &entity.ConfigFiles{
		DeployFiles:                configFilesMap[Deployment],
		ServiceFiles:               configFilesMap[Service],
		SecretFiles:                configFilesMap[Secret],
		PersistentVolumeFiles:      configFilesMap[PersistentVolume],
		PersistentVolumeClaimFiles: configFilesMap[PersistentVolumeClaim],
	}

	return &entity.YAMLConfig{
		MetaData:   nil,
		YamlConfig: configFiles,
	}, nil
}

// The list need to be sorted
func validateYAMLFileFormat(s []string) error {
	for i := 0; i < len(s); i++ {
		paths := strings.Split(s[i], "/")
		extension := strings.Split(paths[len(paths)-1], ".")
		split_ := strings.Split(extension[0], "_")

		if len(split_) != 2 {
			return fmt.Errorf("wrong configName")
		}

		if split_[1] != strconv.Itoa(i) {
			return fmt.Errorf("%s: not the right format", s[i])
		}
	}

	return nil
}

// ContainsKey
/*
Return whether the map of config contains the key or not
*/
func ContainsKey(key string) bool {
	_, containsKey := entityConfigurationMap[key]
	return containsKey
}

// CreateConfiguration
/*
Create a configuration according to the metadata given as input to the function.
This function mostly contains all the paths to the YAML Files that are needed for each entity
*/
func (registry *ImplementationRegistry) CreateConfiguration(getter entity.MetaDataGetter) (entity.Configuration, error) {
	var yamlConfig entity.Configuration

	entityKey := entity.FormatMetaData(getter, "", "")
	if !ContainsKey(entityKey) {
		return nil, fmt.Errorf("can't configure a entity with this key %s", entityKey)
	}

	yamlConfig = entityConfigurationMap[entityKey]
	yamlConfig.SetMeta(entity.MetaDataFromGetter(getter))

	return yamlConfig, nil
}
