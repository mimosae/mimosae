# Module tsp.eu/executor/implementation_registry

## Contents

* implementation_registry.go:

* implementation_registry_test.go: 


* go.mod: generated with the following commands:
```
go mod init tsp.eu/executor/implementation_registry
go mod edit -replace tsp.eu/executor/common=../common
go mod edit -replace tsp.eu/executor/entity=../entity
go mod edit -replace tsp.eu/executor/entity/utils=../entity/utils
go mod tidy
```

