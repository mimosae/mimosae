# Module tsp.eu/executor/main

## Contents

* common/: 

* concurrency/: 

* config/:

* docs/:

* entity/:

* go.sum/:

* implementation_registry/:

* main.go: 

* main_test.go: 

* orchestrator/:

* server/: 

* go.mod: generated with the following commands:
```
go mod init tsp.eu/executor/main
go mod edit -replace tsp.eu/executor/common=./common
go mod edit -replace tsp.eu/executor/concurrency=./concurrency
go mod edit -replace tsp.eu/executor/entity=./entity
go mod edit -replace tsp.eu/executor/entity/utils=./entity/utils
go mod edit -replace tsp.eu/executor/orchestrator=./orchestrator
go mod edit -replace tsp.eu/executor/implementation_registry=./implementation_registry
go mod edit -replace tsp.eu/executor/server=./server
go mod edit -replace tsp.eu/executor/server/proto=./server/proto
go mod tidy
```

# Executor

## Prerequisite

In order to start all the scripts: `scenario.sh` and `test.sh`. Some environment variables for go should be exported: `GOROOT`, `GOPATH`. By default if no export has been done when executing `install.sh` their values should be:

``` BASH
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
```

## Overview

1. [Some concept](#some-concept) is a prerequisites section speaking about the
   concepts you must know in order to understand other sections:
   - [YAML Files](#yaml-files)
   - [Docker](#docker)
   - [Kubernetes](#kubernetes)
   - [Struct, Interface](#struct-interface)
   - [Goroutine](#goroutine)
   - [Channel](#channel)
   - [WaitGroup](#waitgroup)
   - [Defer](#defer)
   - [go generate and mockgen]()
   
2. [Executor functioning](#executor-functioning)

3. [Entity Package](#entity-package) is a section that will give you a sufficient understanding of 
   architectural entity abstraction and [its implementation](./entity):
   - [Architectural Entity](#architectural-entity)
   - [Config](#config)
   - [Utils](#utils) contains some utils to create an architectural entity, parse YAML files and build docker images:
   
        - [Parser](#parser)
        - [Docker Utility](#docker-utility)
        - [Association Graph](#association-graph)
   
4. [Concurrency Package](#concurrency-package) explains how have been implemented concurrency and how to use method from this package:
   [Action](#action)

5. [Server](#server) explains how planner-executor communication was made and how the plan is processed in order to complete connections
   - [Communication](#communication)
   - [Process Plan](#process-plan)

6. [Orchestrator Package](#orchestrator-package)  explains how different packages have been composed in
   order to implement microservice creation:
   - [Actions](#actions)
   - [Orchestrator](#orchestrator)

7. [Implementation registry](#implementation-registry)

---

## Some Concept

### YAML Files

In this context a YAML file is the configuration of an element from kubernetes. The command `kubectl` is usually used to apply the configuration by typing `kubectl apply -f <your_YAML_File>`. In our case the executor uses them to create the structs associate with and store them in the right microservice.  

[Here](../../UseCasEDF_GDE_PFE/K8S-configuration) is EDF use-case as an example
### Docker
Docker is a tool that allows the containerization of one or multiple services. It uses the concept of images which are the base of the container. For the most part, they are are usually lighweight and specialized for a specific application such as go, node or python for instance. Some of them can also deploy debian, ubuntu or even archlinux. In our case they are used to build an image of the service that needs to be deployed in the kubernetes cluster.  
To install docker just follow the documentation which is quite understandable. However here is the commands for an installation on debian (amd64).

``` bash
# Uninstall any old version
sudo apt-get remove docker docker-engine docker.io containerd runc

# Setup the repository
sudo apt-get update
sudo apt-get install \
	apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Add docker official GPG key
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Add the repository
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install docker engine
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

After the installation, to start systemd is used to start/stop docker
``` bash
# Start Docker
sudo systemctl start docker

# Stop Docker
sudo systemctl stop docker
```
[Here](../../UseCasEDF_GDE/file-service) is a basic service using docker

### Kubernetes

Kubernetes is container orchestration tool. When you work with Kubernetes, you usually create
a cluster composed of control pane and nodes. You have some pods(in general not much) within each node, and a
container within a pod(in general one container per pod). You also have *kubelet* - kubernetes control unit 
within each node. This scheme shows basic kubernetes cluster architecture:

![Docker general view ](./docs/UML-models/components-of-kubernetes.svg).

If you want to know more on this topic, consult this [site](https://kubernetes.io/docs/tutorials/kubernetes-basics/) or this [book](../../../Documents/)

[**Link to the client-go library for Kubernetes**](https://github.com/kubernetes/client-go). Take a look at examples folder
in this repository.

Finally, we use a single node kubernetes cluster with ***Minikube***. Next commands will help you
to install Minikube locally on your **linux**, and to run it(*you must have docker installed*):

```bash
  $curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 \
  && chmod +x minikube
  
  $sudo mkdir -p /usr/local/bin/
  $sudo install minikube /usr/local/bin/
  
  $sudo usermod -aG docker $USER && newgrp docker
  
  $minikube start
```

**We use client-go library, that allows us to interact easily with Kubernetes api.**

### Struct, Interface

A **struct** is an aggreagate data type that group zero or more named values of arbitrary types. Each value is called a field.
```go
type student struct {
	ID int
	name string
	surname string
	createAt time.Time
	lessons []*Lesson
}
```

This part is based on [this document](../../../Documents/2016_Donovan_Kernighan_The_Go_Programming_Language.pdf) (chapter 7, p.171)  
> "Interface types express generalizations or abstractions about the behaviors of other types"

That is why interface can be used to generalize some functions. However, compared to other languages that contains the same concept, in Go if the types pocesses at least the necessary methods it will implement the interface. For instance, the interface **ConfigInterface** is used to define all the structs that can be used to create a Microservice and it is used in the [Implementation Registry](factory/implementation_registry.go) to return a more versatile object.

```go
// microservice.ConfigInterface corresponds to the interface all the configs that can create a microservice need to implement
func createConfiguration(metaData *microservice.Meta) (microservice.ConfigInterface, error)
```

### Goroutine

Go was mainly designed to make a competitive programming. The mechanism used for execute the
processes in parallel is goroutine. To start a new goroutine just write `go`:

```go
    go your_best_function()
```

### Channel

Channels are fundamental bricks of competitive programming in go. They allow the communication
between goroutines. We will explain basic concepts, sufficient to understand the code. If you want to
know more, take a look to [this](../../../Documents/2016_Donovan_Kernighan_The_Go_Programming_Language.pdf).

#### Unbuffered channel

To define a non-buffered channel use:

```go
    channel := make(chan type)
```

You can write to channel and read from it:

```go
    a := 5
    var b int
    channel := make(chan int)
    channel <- a // writing to channel
    b := <- channel // reading from channel, b = 5
```

When channel is empty, its reading is blocked:

```go
    func main() {
        ch := make(chan bool)
        go func() {
        	time.Sleep(10000)
            ch <- true	
        }()
        <-ch // blocked until true is written to channel by goroutine
        close(ch) // used to close the channel
    }
```

For more complex examples, and explanation of *channel draining* or *leak* you can
read [this document](../../../Documents/2016_Donovan_Kernighan_The_Go_Programming_Language.pdf)

#### Buffered channel

Buffered channels is basically the same thing, but we can also define a capacity of channel:

```go
    channel := make(chan type, n) // n is a channel's capacity
```

Buffered channel is represented by a queue. The writing to channel is blocked when queue is full. The reading is
blocked if the queue is empty.

When we specify a capacity, we unintentionally decouple the reading and the writing to
channel. For example if our channel has a capacity of 3, and we have written only 2
elements in it, nor reading, neither writing are blocked.

#### Unidirectional channel

Unidirectional channels are authorized only in function arguments. When we pass a channel as an argument
to the function, we can limit writing to or reading from channel:

```go
    func a(b chan<- int, c <-chan int){...} // b is the write-only and c is a read-only
```

### WaitGroup

WaitGroup is a struct that comes with the built-in package sync. It allows the synchronization between multiple goroutines. This struct behaves like a counter and implements 3 functions.  
The Add() function increment the counter with the value insert as an argument (it can be negative). However this must be called before Wait() is called.

```go
func (wg *WaitGroup) Add(delta int)
```
The Done() function decrement the counter. In the package it is simply a Add(-1).
```go
func (wg *WaitGroup) Done() {
	wg.Add(-1)
}
```
The function Wait waits until the counter is zero.
```go
func (wg *WaitGroup) Wait()
```
### Defer
Defer can be put before any call. It is used to be sure that the execution is called juste before the parent function/goroutine return (the return can be null). It is mainly useful when many "if" with "return" occure in a function but an action need to be executes at the end of the parent function. For example, to decrement a WaitGroup in a goroutine even when there is an error, ```defer wg.Done()``` can be used at the beginning of the goroutine.

### Go generate and mockgen

`go generate` command allows us to generate a go code (given some instructions) before
compilation.

We combine `go generate` and `mockgen` tools in order to generate mocks
used for tests.

For more info look at [mockgen](https://github.com/golang/mock) and [go generate](https://go.dev/blog/generate)

---

## Executor Functioning

![executor diagram](./docs/UML-models/ExecutorDiagram.svg)

This is the communication diagram, which represents the general 
functioning of executor. We will explain each communication step separately (***letters correspond to numbers on the scheme***): 

0. Sending data(plan) from actor(planner) to executor via **Executor Frontend**
1. In reality **Executor Frontend** is represented by two modules: **Server** and **Concurrency**. In step 1 we validate plan with the help of **Server**
2. This step demands to **Concurrency** to treat plan and to create parallel actions.
3. This is a huge step, that is composed of substeps:
   1. This step signifies, that multiple actions like this can be executed at the same time(this is what we call **parallel actions**). Each action is sent to **Orchestrator**. All substeps are executed in the context of **parallel action**.
      1. For each action **Orchestrator** requests **Implementation registry** in order to find project configuration files(Kubernetes configs or Dockerfile for example).
         1. **Implementation Registry** creates a **Configuration** with a configuration files paths inside it.
      2. **Orchestrator** then takes each instance of **Configuration** that have been created and calls **buildDockerImages(...)** on each of them.
         1. Each **Configuration** builds docker images using paths contained inside it
      3. After docker images have been built, **Orchestrator** calls **initializeArchitecturalEntity()** on each **Configuration**
         1. **Configuration** parses configuration files, accessed via **Configuration** paths, in order to create go structures. Then **Configuration** instantiates an **ArchitecturalEntity** (an abstraction, which represents microservice, database or a connector).
      4. **Orchestrator** takes each instance of created **ArchitecturalEntity** and initializes its intern dependency graph(it is a graph of dependencies between Kubernetes components)
      5. In this step dependency graph is filled.
      6. **Orchestrator** then calls a **KubernetesHandler** providing created **ArchitecturalEntity** to it. **KubernetesHandler** is responsible to call Kubernetes API and to deploy all entities respecting dependencies.

---

## Entity Package
Link to the package: [entity/](entity/)
### Architectural Entity

[architectural_entity.go](entity/architectural_entity.go) contains the definition of architectural entity and its meta. 
Architectural entity is just a model, a theoretic representation of real-life microservice or a database system:

```go
    type Meta struct {
        Name string
        Version string
        Namespace string
        MicroServiceType string
    }


    type ArchitecturalEntity struct {
        MetaData *Meta
        Pods []*v1.Pod
        Services []*v1.Service
        Secrets []*v1.Secret
        Deployments []*v12.Deployment
        PersistentVolume []*v1.PersistentVolume
        PersistentVolumeClaim []*v1.PersistentVolumeClaim
        DependencyGraph *utils.AssociationGraph
    }

```

`Meta` is a the entity description.

`Architectural Entity` contains a `Meta`, kubernetes objects (read [this](#kubernetes) if you are not familiar with kubernetes) and
`DependencyGraph` of type [AssociationGraph](#association-graph).

`InitDependencyGraph(capacity int)` method creates an empty `AssociationGraph` of given capacity

`CreateDependencies()` populates entity's `DependencyGraph`. Dependencies between kubernetes objects
are determined by this same method.

`CreateDependencies` method uses `addDependecies(key string, dependencies []string)` function, which creates
the arcs departing from the node associated with the `key` and arriving to the nodes associated with
each element of `dependecies`.

### Config
Link to the file: [entity/config.go](entity/config.go)

An Architectural Entity can be totally created from a Config.
Each config is instantiated by the [Implementation Registry](factory/implementation_registry.go). Each configuration must implement a `Configuration`.
In other words, It must implement at least 3 functions `InitializeArchitecturalEntity`, `BuildDockerImages` and `SetMeta`.

As explained [previously](#interface) it is because each Config has to respect a specific interface:
```go
type Configuration interface {
	InitializeArchitecturalEntity() (*ArchitecturalEntity, error)
	BuildDockerImages(args map[string]*string) (bool, error)
	SetMeta(meta *MetaData)
}
```

There are for now 3 Configs that can be used to create an Architectural Entity. Some of them implement the structure that contains a list of all the YAML that need to be read/parse to create an Architectural Entity.
```go
type ConfigFiles struct {
	DeployFiles []*string // Paths to Deployment YAML Files
	ServiceFiles []*string // Paths to Service YAML Files
	PersistentVolumeFiles []*string // Paths to Persistent Volume YAML Files
	PersistentVolumeClaimFiles []*string // Paths to Persistent Volume Claim YAML Files
	SecretFiles []*string // Paths to Secret YAML Files
}
```
1. ***SimpleConfig***: the most basic of them that only contains the MetaData of an Architectural Entity
```go
type SimpleConfig Meta
```
This structure for now only implement ```InitializeArchitecturalEntity``` and return necessary *nil*
```go
func (config SimpleConfig) InitializeArchitecturalEntity() (*ArchitecturalEntity, error) {
	return nil, nil
}
```

2. ***YAMLConfig***: Contains the MetaData of an Architectural Entity but also the paths to all the YAML file for the deployment
```go
type YAMLConfig struct {
    MetaData *Meta
    YamlConfig *ConfigFiles
}
```
This structure implement both functions. The `InitializeArchitecturalEntity` function cover all the paths of the YAML files stored and create the right structs that are then put into the Architectural Entity struct. However, the `BuildDockerImage` function does nothing more than print that there is no docker folder / image associate to the Architectural Entity.
```go
func (config *YAMLConfig) InitializeArchitecturalEntity() (*ArchitecturalEntity, error) {
    entity := new(ArchitecturalEntity)
    var deployments []*v12.Deployment
	[...] // Definition of other empty list that need to be filled

    for _, deployConfig := range config.YamlConfig.DeployFiles {
        deploy := &v12.Deployment{}
        err := utils.DecodeYAML(deployConfig, &deploy)
        deploy.SetName(FormatMetaData(config.MetaData, "", "-deployment"))
        
        for _, container := range deploy.Spec.Template.Spec.Containers {
            for _, envSource := range container.EnvFrom {
                envSource.SecretRef.Name = FormatMetaData(config.MetaData, "", "-secret")
            }
        }
        if err != nil {
            return nil, err
        }
        
        deployments = append(deployments, deploy)
    }

    [...] // Other loops for the other type of structs

	entity.MetaData = config.MetaData
    entity.Deployments = deployments
	[...] // Put the other filled list in the created microservice

	return entity, nil
}
```
```go
func (config *YAMLConfig) BuildDockerImages(args map[string]*string) (bool, error) {
	fmt.Println("*** No Image to Build ***")
	return false, nil
}
```

3. ***YAMLConfigWithDocker***: Contains the same element than the previous config but can also have an access to a folder with a Dockerfile to build the image for the deployment of the Architectural Entity
```go
type YAMLConfig struct {
    YamlConfig *YAMLConfig
    DockerImage *string
    DockerFolder *string
}
```
It implements both functions too. While `InitializeArchitecturalEntity` function uses the one of the previous Config (one is include in the other), the `BuildDockerImage` function build the Docker Image directly into minikube (for now) but at the end might be able to use a private registry directly in kubernetes.
```go
func (config *YAMLConfigWithDocker) InitializeArchitecturalEntity() (*ArchitecturalEntity, error) {
	entity, _err := config.YamlConfig.InitializeArchitecturalEntity()
	if _err != nil {
		return nil, _err
	}

	return entity, nil
}
```
```go
func (config *YAMLConfigWithDocker) BuildDockerImages(args map[string]*string) (bool, error) {
    err := utils.BuildDockerImage(config.DockerFolder, FormatMetaData(config.YamlConfig.MetaData, "", "-image"), args)
    fmt.Println("***** Docker image built *****")
    if err != nil {
        return false, err
    }
    return true, nil
}
```
### Utils

#### Docker Utility
[microservice/utils/docker.go](./microservice/utils/docker.go)  contains only one function called `BuildDockerImage`.
``` go
func BuildDockerImage(path string, imageName string)  error
```
This function take as argument the path of the folder that contains the Dockerfile and the name of the docker image when built. These parameters are defined in the Configuration Structs and initialized in the [Implementation registry](./factory/implementation_registry.go).  
For now, the build has to be made in minikube which already implement a docker client. In order to have a more standard use of the Executor, a docker registry will be used to store the built images and use them to deploy the microservices. However, a change in one of the dependency of the docker package in go lead to certain errors. These have already been corrected in the master branch of the github repository but still needs to be released.
#### Association Graph

[Association graph](./microservice/utils/association_graph.go) is the extension of [go basic graph library](https://pkg.go.dev/github.com/yourbasic/graph)
There is a definition of `AssociationGraph` : 

```go
    type AssociationGraph struct {
        Graph *graph.Mutable
        associationList []string
        associationMap map[string]int
    }
```

* `Graph` is a part of a basic graph library, where nodes are represented by numbers from `0` to `n-1` with `n - graph's capacity`.

* `associationList` is a list, that associates node number to the microservice's name.

* `associationMap` is a map, that associates microservice's name to its node number.

`Order` and `Visit` methods are implemented on `AssociationGraph` in order to directly access corresponding methods of
`Graph` :

```go
    func(a *AssociationGraph) Order() int{
        return a.Graph.Order()
    }

    func(a *AssociationGraph) Visit(source int, f func(target int, c int64) bool) bool{
        return a.Graph.Visit(source, f)
    }
```

It allows us to directly iterate on `AssociationGraph`. If you want to know more about
`Order` and `Visit` methods, look [there](https://pkg.go.dev/github.com/yourbasic/graph)

There is another functions definitions, that operate directly on `AssociationGraph`.

You can read the description of each file by running the following commands in terminal:
```bash
    cd "path_to_tsivik/Code/AutomaticManager/Executor/microservice/utils"
    go doc utils.function_you_are_interested_of
```

`Association graph` aims to reflect dependencies between kubernetes objects needed to deploy a microservice.
For example the creation of deployment depends on correspondent persistence volume claim. 


#### Parser

[parser.go](./microservice/utils/parser.go)  contains only one function called `decodeYAML`.
This function opens a file with YAML config and uses a kubernetes built-in parser to decode a YAML in interface passed in parameter:

```go
    yaml.NewYAMLOrJSONDecoder(file, 8192).Decode(k8sStruct)
```

By default, kubernetes works with JSON format, so it traduces YAML to JSON and then parses it.  

---


## Concurrency Package
It defines the Methods and Structs that can be used to execute action in parallel with some dependencies.  
### Action
[action.go](concurrency/action.go) contains an Action definition :
```go
    type Action struct {
        instruction     *func() error
        ToSend      	[]*sync.WaitGroup
        ToWait 			*sync.WaitGroup
    }
```

It was designed to provide a possibility of a competitive execution. In fact, the 
instruction can't be executed before the `ToWait` counter is down to zero. After instruction is
executed, it decrements the counter of each `ToSend` waitGroup. Thus, we communicate to dependent actions that
they can be executed.

Technically it is what `execute` implemented by `Action` does:

```go
    func (a Action) execute(errChannel chan <- error) {
        a.ToWait.Wait()

        err := (*a.instruction)()
        if err != nil {
            errChannel <- err
        }
        
        for _, sending := range a.ToSend {
            sending.Done()
        }
    }
```

For better understanding the waiting mechanism and [association-graph](#association-graph) take a look at the scheme below:

![DependencyGraph diagram](./docs/UML-models/DependencyGraph-kubernetesComponents.svg)

Initially the counter of `D_0 ToWait` is up to 2, because it has 2 corresponding `ToSend`.
But technically, `ToWait` and `ToSend` in red are the same thing, as they refer to the same `waitingGroup`. When
we call `ToSend.Done()` the counter decrements (the counter of `ToWait` too). Once the counter is down to zero, `D_0` can be executed.


### ExecuteActions

This function is the application of a standard pattern used with [WaitGroup](#waitgroup) explain here [chapter 8, p. 238](../../../Documents/2016_Donovan_Kernighan_The_Go_Programming_Language.pdf) 
```go
func ExecuteActions(actions map[string]*Action) error{
	errChannel := make(chan error, len(actions))
	var wg sync.WaitGroup

	for k, a := range actions {
		wg.Add(1)
		fmt.Printf("===== Executing routine: %s =====\n", k)
		go func(a *Action, errChannel chan <- error) {
			defer wg.Done()
			a.execute(errChannel)
		}(a, errChannel)
	}

	go func() {
		wg.Wait()
		close(errChannel)
	}()

	for err := range errChannel {
		if err != nil {
			return err
		}
	}
	return nil
}
```
Here is all the code for the function. Yet it is essential to explain how the pattern work  

The function needs a map of actions and return any error that can occure in one of the goroutine created
```go
func ExecuteActions(actions map[string]*Action) error
```
Initialization of the WaitGroup
```go
	var wg sync.WaitGroup
```
Loop on all the actions created to execute them in parallel with a [Goroutine](#goroutine) for each.
```go
	for k, a := range actions {
		wg.Add(1) // Increment the WaitGroup of one
		fmt.Printf("===== Executing routine: %s =====\n", k)
		go func(a *Action, errChannel chan <- error) { 
			defer wg.Done() // When the routine is finished decrement the Counter
			a.execute(errChannel) // Execute the Action
		}(a, errChannel) // Use of an anonymous function
	}
```
[Goroutine](#goroutine) that wait for all the previous one to be finished thanks to wg.Wait(). `close` is used to close the errChannel
```go
	go func() {
		wg.Wait()
		close(errChannel)
	}() // Use of an anonymous function
```
This loop listen on errChannel (a [bufferized channel](#channel)) until it is closed. So it insures that the function **ExecuteActions** end when each action is finished 
```go
	for err := range errChannel {
		if err != nil {
			return err
		}
	}
```
Always return nil when the actions are finished with no error&
``` go
	return nil
```
If no errors return nil.  

This diagram is an example of the sequences that appened in ExecuteActions function.  
For this diagram to be as simple as possible only 4 things are needed/defined to deploy a Microservice (a Sercret, a Deployment, a Persistent Volume and a Persistent Volume Claim) and the depencies between each deployment are not taken into account
![ExecuteActions Diagram](docs/UML-models/ExecuteActions.jpg "ExecuteActions Diagram")

---

## Server

### Communication

In order to communicate with the client we use a gRPC server. All methods are generated by
`protoc`, which uses [this **protobuf** file](../Protobuff/plannification.proto). Inside
this file you can find messages and rpc-methods definition. The code is automatically generated by
`test.sh` or `scenario.sh` scripts. After that we need just implement methods defined in generated code.

You can read more about protobuf and gRPC [here](https://grpc.io/docs/languages/go/quickstart/).

### Process Plan

`processPlan` is a function defined in [server package](server/server.go). It checks the correctness of the
plan, that has been sent to the server, using timestamps and creates a dependency cache that is
used to complete architectural entity's secret with connector's secret. [There](server/server.go) you can
find a code of `processPlan`.

---

## Orchestrator Package
### Handler

[handler.go](./orchestrator/handler.go) file contains two types of actions: direct interactions with Kubernetes API
which create, update or delete a kubernetes object, and a high-level actions such as `manipulateArchitecturalEntity`
or `instantiateAction`. It also contains an interface `Handler` which implements API initialization and low level interactions
with Kubernetes.

We are going to take a quick look on Handler and API interactions, and then we'll consider 
`manipulateArchitecturalEntity` and instantiateAction in details.

#### Handler interface

The code of interface `Handler` is represented below. Basically it defines methods to initialize an api connection
and to send the requests to this api via client-go library:
```go

type Handler interface {
	CreatePersistentVolume(*v1.PersistentVolume) (*v1.PersistentVolume, error)
	CreatePersistentVolumeClaim(*v1.PersistentVolumeClaim) (*v1.PersistentVolumeClaim, error)
	CreateSecret(*v1.Secret) (*v1.Secret, error)
	CreateDeployment(*v12.Deployment) (*v12.Deployment, error)
	CreateService(*v1.Service) (*v1.Service, error)
	RemovePersistentVolume(*v1.PersistentVolume) (bool, error)
	RemovePersistentVolumeClaim(*v1.PersistentVolumeClaim) (bool, error)
	RemoveSecret(*v1.Secret) (bool, error)
	RemoveDeployment(*v12.Deployment) (bool, error)
	RemoveService(*v1.Service) (bool, error)
	GetConnectorSecrets([]*entity.Connector) ([]*v1.Secret, error)
	InitApi() error
}
```

#### InitApi

Instantiates api if it has not been done yet and returns it. The initialization is
based on the local minikube config:

```go
    kubeConfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "")
```

#### API interactions

Kubernetes, its API and client-go library have been explained in 
[Some Concept](#kubernetes) section. We will concentrate our attention on the one of
the API interactions, for example, createService:
```go
func (m *Manager) CreateService(service *v1.Service) (*v1.Service, error) {
    common.InfoLogger.Printf("**** Create Service %s ****\n", service.Name)
    ctx, cancel := context.WithTimeout(context.TODO(), SERVICETIMEOUT)
    defer cancel()
    if service.GetNamespace() == "" {
        service.SetNamespace(DEFAULTNAMESPACE)
    }
    _service, err := m.api.CoreV1().Services(service.GetNamespace()).Get(ctx, service.GetName(), metav1.GetOptions{})
    if err != nil {
        if !k8serrors.IsNotFound(err) {
        common.ErrorLogger.Println(err)
        return nil, err
    }
    } else {
        common.WarningLogger.Printf("** Service %s already exists **\n", service.Name)
        return _service, nil
    }
    
    _service, err = m.api.CoreV1().Services(service.GetNamespace()).Create(ctx, service, metav1.CreateOptions{})
    if err != nil {
        common.ErrorLogger.Println(err)
        return nil, err
    }
    return _service, nil
}
```

* First we create a timeout context. [Context](https://pkg.go.dev/context) is a built-in go package, which
  manages deadlines and cancellation. If you want to know more about contexts, click on the link above. All you
  must know to understand this code is 2 things:
    1) If you are not sure about context, don't leave it nil and call context.TODO()
    2) context.WithTimeout sets a timeout on the context. If execution isn't finished after designated time
        context cancels and safely releases all resources. In order to properly cancel this context, we call 
    ```go
        defer cancel()
    ```
  [defer](#defer) has been examined in some concepts section


-   Next we are trying to get a service with help of [client-go](https://github.com/kubernetes/client-go) library:
```go
    _service, err := api.CoreV1().Services(service.Namespace).Get(ctx, service.Name, metav1.GetOptions{})
```
`api` represents a kubernetes API `struct`. We can extract 2 type of interfaces from it `Corev1Interface` 
and `Appsv1Interface` depending on with which kubernetes object we work. In the service case we call `api.CoreV1()` to
access `Corev1Interface`.

We specify a namespace and call a `Get` with already created context. 

* The Last step is to check whether service already exists. If this is the case we print a message
and exit from function. In the other case we create a service:
```go
  _service, err = api.CoreV1().Services(service.Namespace).Create(ctx, service, metav1.CreateOptions{})
```
That's it. This is a basic, classic API interaction action function.

---

#### manipulateArchitecturalEntity

`manipulateArchitecturalEntity` is more complex, because it is a high-level function, which
takes in argument a hypothetical model representation of what we call an architectural entity. `ArchitecturalEntity` is a structure, 
and you can take a look to its representation in corresponding [section](#architectural-entity) or
directly [there](./entity/architectural_entity.go)

Here is the code of `manipulateArchitecturalEntity`:

```go
func manipulateArchitecturalEntity(entity *entity.ArchitecturalEntity, handler Handler, operation Operation) error {
    common.InfoLogger.Printf("========== Begin Deployment of ArchitecturalEntity: %s ==========\n", entity.MetaData.GetName())
    actions := map[string]*concurrency.Action{}
    for v := 0; v < entity.DependencyGraph.Order(); v++ {
        if _, contains := entity.DependencyGraph.GetAssociation(v); !contains {
            return errors.New("association does not exist ")
        }
        key, _ := entity.DependencyGraph.GetAssociation(v)
        action, err := instantiateAction(entity, &key, handler, operation)
        if err != nil {
            return err
        }
        actions[key] = action
    }
    var err error
    for v := 0; v < entity.DependencyGraph.Order(); v++ {
        entity.DependencyGraph.Visit(v, func(w int, c int64) (skip bool) {
            if err != nil {
                return
            }
            source, _ := entity.DependencyGraph.GetAssociation(v)
            target, _ := entity.DependencyGraph.GetAssociation(w)
            if action, containsKey := actions[source]; containsKey {
                if dependency, containsKey := actions[target]; containsKey {
                    action.ToWait.Add(1)
                    dependency.ToSend = append(dependency.ToSend, action.ToWait)
                    return
                }
            }
            err = errors.New("there is an action not contained in actions map ")
            return
        })
    }
    if err != nil {
        return err
    }
    err = concurrency.ExecuteActions(actions)
    common.InfoLogger.Printf("========== End Deployment Of ArchitecturalEntity: %s ==========\n", entity.MetaData.GetName())
    return err
}
```

Briefly, this function go through architectural entity's dependency graph and create actions
and dependencies between them, that we will execute in determined order, in the purpose to deploy a
microservice or a database system. This deployment creates some of kubernetes objects, described in
[kubernetes](#kubernetes) section

There is a few steps in this function. Some of them suppose that you have already read
[architectural entity](#architectural-entity) and [association graph](#association-graph) sections, as we use
those terms a lot. You must read [concurrency](#concurrency-package) section
two, in order to understand how dependency and actions synchronisation has been implemented.

The steps are the following:
* Create a map, that associates a name of action to its structure `Action` ([see there](#concurrency-package)) 
* Traverse graph's vertexes and instantiate actions (instantiate is described in section below)
* Traverse graph's edges and set up dependencies. For this, every action has associated `WatingGroups`. (see [WaitingGroups](#waitgroup) and [concurrency](#concurrency-package))
* Finally, call `concurrency.ExecuteActions(actions)` on `actions` map with all dependencies embedded due to WaitingGroups mechanism.

Let's explain it in details:

1. Just creating an empty map :
```go
    actions := map[string]*concurrency.Action{}
```
2. We iterate over architectural entity's dependency graph vertexes. To do so, we must implement
    `Order()` and `Visit()` method on dependency graph (see [graph](https://pkg.go.dev/github.com/yourbasic/graph))
   
    This graph reflects in
    what order kubernetes objects must be created. However, we must first search a name of corresponding 
    action for each vertex represented by a number and then instantiate an action:

```go
    key, _ := entity.DependencyGraph.GetAssociation(v)
    action, err := instantiateAction(entity, &key, handler, operation)
```

3. Then, for each edge of this same graph we set up a dependency. Graph is oriented and
arc (v,w) signifies that v depends on w :
```go
    source, _ := entity.DependencyGraph.GetAssociation(v)
    target, _ := entity.DependencyGraph.GetAssociation(w)
    if action, containsKey := actions[source]; containsKey {
        if dependency, containsKey := actions[target]; containsKey {
            action.ToWait.Add(1)
            dependency.ToSend = append(dependency.ToSend, action.ToWait)
            return
        }
    }
```

4. Finally, we call a concurrent execution (explained in [concurrency](#concurrency-package)):

```go
    err = concurrency.ExecuteActions(actions, errChannel)
```

---

#### Instantiate Action
This function create an action according to what need to be executed. It can be a Persistent Volume creation or a Service, Deployment removal, etc...  

``` go
func instantiateAction(entity *entity.ArchitecturalEntity, typeAction *string, handler Handler, operation Operation) (*concurrency.Action, error) {
	split := strings.Split(*typeAction, "_")
	if len(split) != 2 {
		return nil, errors.New("error in the dependencies ")
	}
	i, err := strconv.ParseInt(split[1], 10, 64)
	if err != nil {
		return nil, err
	}

	toWait := sync.WaitGroup{}
	var toSend []*sync.WaitGroup
	instruction := func(a interface{}) error {
		return nil
	}
	switch split[0] {
	case "pv":
		if int64(len(entity.PersistentVolume)) <= i {
			return nil, errors.New("persistent volume: Out of range")
		}

		switch operation {
		case CreateEntity:
			instruction = func(a interface{}) error {
				_, err := handler.CreatePersistentVolume(entity.PersistentVolume[i])
				return err
			}
		case RemoveEntity:
			instruction = func(a interface{}) error {
				_, err := handler.RemovePersistentVolume(entity.PersistentVolume[i])
				return err
			}
		}
	[...] // other kubernetes objects creation

	return concurrency.NewAction(&instruction, toSend, &toWait, nil), nil
}
```
This function use the fact that `typeAction` consists of a kubernetes component short name and its index in corresponding
slice inside architectural entity. For example, to create a Persistent Volume which corresponds to the 1st element of `ArchitecturalEntity.PersistentVolumes` the `typeAction` will be ***`pv_0`***.  
So the split is used to separate the component that needs to be deployed from its index. This is what the following code do.
``` go
split := strings.Split(*typeAction, "_")

if len(split) != 2 {
    return nil, errors.New("Error in the dependencies ")
}

i, err := strconv.ParseInt(split[1], 10, 64)
if err != nil {
    return nil, err
}
```
Then the fields of an [action](#action) are initialized. We create empty wait groups and an empty instruction.
We construct instruction depending on operation and using a handler passed in parameters.

---

### Orchestrator
[orchestrator/orchestrator.go](./orchestrator/orchestrator.go). This file describs the behaviour of the Orchestrator and notably the interface Orchestrator.
``` go
type Orchestrator interface {
	CreateArchitecturalEntity(metaData entity.MetaDataGetter) (*entity.ArchitecturalEntity, error)
	RemoveArchitecturalEntity(metaData entity.MetaDataGetter) (*entity.ArchitecturalEntity, error)
}
```
This interface contains all the functions that act on the kubernetes system and create an architectural entity. In our case, for the executor, it is the struct Factory that implement it.
``` go
type Factory struct {
	handler         Handler
	registryHandler implementation_registry.RegistryHandler
}
```
Both fields are an interface. 
- `Handler` is defined [here](#handler)
- `RegistryHandler` is defined [here](#implementation-registry)

Functions that are implemented by `Factory`  
This function create an architectural entity from its MetaData which is represent by the interface [MetaDataGetter](#config). They are multiple steps when creating a new entity.  
``` go
func (f *Factory) CreateArchitecturalEntity(metaData entity.MetaDataGetter) (*entity.ArchitecturalEntity, error) {
	common.InfoLogger.Printf("=============== Begin Creation of ArchitecturalEntity: %s ===============\n", metaData.GetName())
	yamlConfig, err := f.registryHandler.CreateConfiguration(metaData)
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}
	_, err = yamlConfig.BuildDockerImages()
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	common.InfoLogger.Println("===== Config Created =====")
	var microService *entity.ArchitecturalEntity
	microService, err = yamlConfig.InitializeArchitecturalEntity()
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	microService.InitDependencyGraph(utils.ParentChild)
	common.InfoLogger.Println("===== ArchitecturalEntity Initialized =====")
	microService.CreateDependencies()

	isAcyclic := graph.Acyclic(microService.DependencyGraph)
	if !isAcyclic {
		return nil, errors.New("dependency graph is has cycles ")
	}
	err = manipulateArchitecturalEntity(microService, f.handler, CreateEntity)
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	common.InfoLogger.Printf("=============== End Creation of ArchitecturalEntity: %s ===============\n", metaData.GetName())
	return microService, nil
}
```
1. Create the right Configuration with the implementation registry (i.e. [*`Implementation Registry`*](#implementation-registry))
```go
yamlConfig, err := f.registryHandler.CreateConfiguration(metaData)
if err != nil {
    common.ErrorLogger.Println(err)
    return nil, err
}
```
2. Build all the docker images that could be needed for the specific entity. (A project with a Dockerfile will usually be used) (i.e. [*`docker`*](#docker))
```go
_, err = yamlConfig.BuildDockerImages()
if err != nil {
    common.ErrorLogger.Println(err)
    return nil, err
}
```
3. Initialize the architectural entity from this configuration (i.e. [*`Config`*](#config))
```go
var microService *entity.ArchitecturalEntity
microService, err = yamlConfig.InitializeArchitecturalEntity()
if err != nil {
    common.ErrorLogger.Println(err)
    return nil, err
}
```
4. Initialize and create the dependency graph (i.e. [*`ArchitecturalEntity`*](#entity))
```go
microService.InitDependencyGraph(utils.ParentChild)
microService.CreateDependencies()
isAcyclic := graph.Acyclic(microService.DependencyGraph)
if !isAcyclic {
    return nil, errors.New("dependency graph is has cycles ")
}
```
5. Deploy the Components of that Microservice (i.e. [*`manipulateArchitecturalEntity`*](#handler))
```go
err = manipulateArchitecturalEntity(microService, f.handler, RemvoeEntity)
if err != nil {
    common.ErrorLogger.Println(err)
    return nil, err
}
```
This function remove an architectural entity defined by its MetaData.
```go
func (f *Factory) RemoveArchitecturalEntity(metaData entity.MetaDataGetter) (*entity.ArchitecturalEntity, error) {
	common.InfoLogger.Printf("=============== Begin Creation of ArchitecturalEntity: %s ===============\n", metaData.GetName())
	yamlConfig, err := f.registryHandler.CreateConfiguration(metaData)
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	common.InfoLogger.Println("===== Config Created =====")
	var microService *entity.ArchitecturalEntity
	microService, err = yamlConfig.InitializeArchitecturalEntity()
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	microService.InitDependencyGraph(utils.ChildParent)
	common.InfoLogger.Println("===== ArchitecturalEntity Initialized =====")
	microService.CreateDependencies()

	isAcyclic := graph.Acyclic(microService.DependencyGraph)
	if !isAcyclic {
		return nil, errors.New("dependency graph is has cycles ")
	}
	err = manipulateArchitecturalEntity(microService, f.handler, RemoveEntity)
	if err != nil {
		return nil, err
	}

	common.InfoLogger.Printf("=============== End Creation of ArchitecturalEntity: %s ===============\n", metaData.GetName())
	return microService, nil
}
```
1. Create the right Configuration with the implementation registry (i.e. [*`Implementation Registry`*](#implementation-registry))
```go
yamlConfig, err := f.registryHandler.CreateConfiguration(metaData)
if err != nil {
    common.ErrorLogger.Println(err)
    return nil, err
}
```
2. Initialize the architectural entity from this configuration (i.e. [*`Config`*](#config))
```go
var microService *entity.ArchitecturalEntity
microService, err = yamlConfig.InitializeArchitecturalEntity()
if err != nil {
    common.ErrorLogger.Println(err)
    return nil, err
}
```
3. Initialize and create the dependency graph (i.e. [*`ArchitecturalEntity`*](#entity))
```go
microService.InitDependencyGraph(utils.ParentChild)
microService.CreateDependencies()
isAcyclic := graph.Acyclic(microService.DependencyGraph)
if !isAcyclic {
    return nil, errors.New("dependency graph is has cycles ")
}
```
4. Deploy the Components of that Microservice (i.e. [*`manipulateArchitecturalEntity`*](#handler))
```go
err = manipulateArchitecturalEntity(microService, f.handler, RemvoeEntity)
if err != nil {
    common.ErrorLogger.Println(err)
    return nil, err
}
```

---

## Implementation Registry
[implementation_registry/implementation_registry.go](./implementation_registry/implementation_registry.go). This file contains the interface RegistryHandler which define the function that create a [`Configuration`](#config) from a [`MetaDataGetter`](#config) and Initialize the implementation registry.
```go
type RegistryHandler interface {
	InitImplementationRegistry(configs ...string) error
	CreateConfiguration(getter entity.MetaDataGetter) (entity.Configuration, error)
}
```
The struct `ImplementationRegistry` implement this interface.
```go
type ImplementationRegistry struct {}
```
This function define how to create the Configurations from a MetaDataGetter.
``` go
func (registry *ImplementationRegistry) CreateConfiguration(getter entity.MetaDataGetter) (entity.Configuration, error) {
	var yamlConfig entity.Configuration

	entityKey := entity.FormatMetaData(getter, "", "")
	if !ContainsKey(entityKey) {
		return nil, fmt.Errorf("can't configure a entity with this key %s", entityKey)
	}

	yamlConfig = entityConfigurationMap[entityKey]
	yamlConfig.SetMeta(entity.MetaDataFromGetter(getter))

	common.InfoLogger.Println(yamlConfig)

	return yamlConfig, nil
}
```
In order to use this specific implementation registry it is mandatory to have a specific folder structure for all the configuration files and the project for each entity. 

**example:**
```bash
Configs
├── entity-type1
│   └── 1_0_0
│       ├── Docker
│       │   ├── Dockerfile
│       └── Kubernetes
│           ├── deployment_0.yaml
│           ├── secret_0.yaml
│           └── service_0.yaml
└── entity-type2
    └── 1_2_0
    │   └── Kubernetes
    │       ├── deployment_0.yaml
    │       ├── pv_0.yaml
    │       ├── pvc_0.yaml
    │       └── service_0.yaml
    └── 3_2_0
        └── Kubernetes
            ├── deployment_0.yaml
            ├── pv_0.yaml
            ├── pvc_0.yaml
            └── service_0.yaml
```
In this example, the path to the folder **Configs** is the one that needs to use with `InitImplementationRegistry`. This folder must contain only folders that have to be named with the type of entity it corresponds to (for instance, in the use case it could be **fs** which is the type of the micro-service **file-service**).  
In each of those folder only folders are allowed and they should be named with the version of the entity the folder correspons to. The version must have `"_"` instead of `"."` as a separation between the numbers.  
Finally in this folder, only 2 Folders are allowed. `Docker` and `Kubernetes` (the syntax is important). However, the folder `Docker` is not necessary, only `Kubernetes` is mandatory.
1. `Kubernetes` folder:  
This folder contains all the yaml config files. However they should be named as follow.
```bash
<kubernetes_component_type>_<number>.<yml>
or
<kubernetes_component_type>_<number>.<yaml>
```
- kubernetes_component_type:
    - deployment -> deployment
    - service -> service
    - secret -> secret
    - persistentVolume -> pv
    - persistentVolumeClaim -> pvc
- number: need to start from 0 and the increment each time a new config file of a specific component is added
2. `Docker` folder:  
This folder needs at least a `Dockerfile` file that will be used when building the Docker Image

