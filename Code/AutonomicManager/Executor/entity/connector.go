package entity

type ConnectorType string

const (
	PubSubConnector         ConnectorType = "PubSubConnector"
	DatabaseSystemConnector ConnectorType = "DatabaseSystemConnector"
	ClientServerContract    ConnectorType = "ClientServerContract"
)

type Connector struct {
	GlobalType ConnectorType
	MetaData   MetaDataGetter
}
