/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package entity

import (
	"fmt"
	"strings"
)

func FormatMetaData(getter MetaDataGetter, prefix string, postfix string) string {
	return fmt.Sprintf("%s%s-v%s%s", prefix, getter.GetType(), strings.Replace(strings.Replace(getter.GetVersion(), ".", "-", -1), "_", "-", -1), postfix)
}
