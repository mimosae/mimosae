/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package entity

import (
	"fmt"

	v12 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"tsp.eu/executor/entity/utils"
)

//go:generate mockgen -destination=local_mocks/mock_meta_data_getter.go -package=mocks tsp.eu/executor/entity MetaDataGetter
type MetaDataGetter interface {
	GetName() string
	GetVersion() string
	GetType() string
}

type MetaData struct {
	name             string
	version          string
	Namespace        string
	microServiceType string
}

func (m *MetaData) GetName() string {
	return m.name
}

func (m *MetaData) GetVersion() string {
	return m.version
}

func (m *MetaData) GetType() string {
	return m.microServiceType
}

func NewMetaData(name string, version string, namespace string, microServiceType string) *MetaData {
	return &MetaData{
		name:             name,
		version:          version,
		Namespace:        namespace,
		microServiceType: microServiceType,
	}
}

func MetaDataFromGetter(getter MetaDataGetter) *MetaData {
	return &MetaData{
		name:             getter.GetName(),
		version:          getter.GetVersion(),
		microServiceType: getter.GetType(),
		Namespace:        "default",
	}
}

type ArchitecturalEntity struct {
	MetaData              *MetaData
	Pods                  []*v1.Pod
	Services              []*v1.Service
	Secrets               []*v1.Secret
	Deployments           []*v12.Deployment
	PersistentVolume      []*v1.PersistentVolume
	PersistentVolumeClaim []*v1.PersistentVolumeClaim
	DependencyGraph       *utils.AssociationGraph
}

func (entity *ArchitecturalEntity) InitDependencyGraph(orientation utils.Orientation) {
	vertexNumber := len(entity.Services) + len(entity.Secrets) + len(entity.Deployments) +
		len(entity.PersistentVolume) + len(entity.PersistentVolumeClaim)
	entity.DependencyGraph = utils.NewAssociationGraph(vertexNumber, orientation)
}

func (entity ArchitecturalEntity) addDependencies(key string, dependencies []string) bool {
	parent, contains := entity.DependencyGraph.GetIndex(key)
	if !contains {
		return false
	}
	for _, dependency := range dependencies {
		var child int
		if child, contains = entity.DependencyGraph.GetIndex(dependency); !contains {
			return false
		}

		if entity.DependencyGraph.GetOrientation() == utils.ParentChild {
			entity.DependencyGraph.AddArc(parent, child)
		} else {
			entity.DependencyGraph.AddArc(child, parent)
		}
	}
	return true
}

func (entity ArchitecturalEntity) CreateDependencies() {
	vertexNumber := 0
	for i := range entity.Services {
		key := fmt.Sprintf("service_%d", i)
		entity.DependencyGraph.AddAssociation(vertexNumber, key)
		vertexNumber++
	}

	var pvDependencies []string
	for i := range entity.PersistentVolume {
		key := fmt.Sprintf("pv_%d", i)
		entity.DependencyGraph.AddAssociation(vertexNumber, key)
		vertexNumber++
		pvDependencies = append(pvDependencies, key)
	}

	secretsMap := map[string]string{}
	for i, secret := range entity.Secrets {
		key := fmt.Sprintf("secret_%d", i)
		entity.DependencyGraph.AddAssociation(vertexNumber, key)
		vertexNumber++
		if _, containsKey := secretsMap[secret.Name]; !containsKey {
			secretsMap[secret.Name] = key
		}
	}

	persistentVolumeClaimMap := map[string]string{}
	for i, persistentVolumeClaim := range entity.PersistentVolumeClaim {
		key := fmt.Sprintf("pvc_%d", i)
		entity.DependencyGraph.AddAssociation(vertexNumber, key)
		vertexNumber++
		_ = entity.addDependencies(key, pvDependencies)

		if _, containsKey := persistentVolumeClaimMap[persistentVolumeClaim.Name]; !containsKey {
			persistentVolumeClaimMap[persistentVolumeClaim.Name] = key
		}
	}

	for i, deployment := range entity.Deployments {
		key := fmt.Sprintf("deployment_%d", i)
		entity.DependencyGraph.AddAssociation(vertexNumber, key)
		vertexNumber++
		var dependencies []string
		for _, volume := range deployment.Spec.Template.Spec.Volumes {
			if volume.PersistentVolumeClaim != nil {
				dependencies = append(dependencies, persistentVolumeClaimMap[volume.PersistentVolumeClaim.ClaimName])
			}
		}
		for _, container := range deployment.Spec.Template.Spec.Containers {
			for _, env := range container.EnvFrom {
				if env.SecretRef != nil {
					dependencies = append(dependencies, secretsMap[env.SecretRef.Name])
				}
			}
		}
		_ = entity.addDependencies(key, dependencies)
	}
}

func (entity *ArchitecturalEntity) CompleteWithConnectionSecrets(connectionSecrets []*v1.Secret) error {
	for _, connectionSecret := range connectionSecrets {
		entity.Secrets = append(entity.Secrets, connectionSecret)
		if len(entity.Deployments) == 0 {
			return fmt.Errorf("there is no deployments in %v", entity)
		}
		for _, deployment := range entity.Deployments {
			for i, container := range deployment.Spec.Template.Spec.Containers {
				container.EnvFrom = append(container.EnvFrom, v1.EnvFromSource{
					SecretRef: &v1.SecretEnvSource{
						LocalObjectReference: v1.LocalObjectReference{
							Name: connectionSecret.Name,
						},
					},
				})
				deployment.Spec.Template.Spec.Containers[i] = container
			}
		}
	}
	return nil
}
