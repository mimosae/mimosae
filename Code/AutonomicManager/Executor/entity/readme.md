# Module tsp.eu/executor/entity

## Contents

* architectural_entity.go

* architectural_entity_test.go

* config.go

* config_test.go

* connector.go

* formatter.go

* formatter_test.go

* utils/: 

* go.mod: generated with the following commands:
```
go mod init tsp.eu/executor/entity
go mod edit -replace tsp.eu/executor/entity/utils=./utils
go mod edit -replace tsp.eu/executor/common=../common
go mod tidy
```

