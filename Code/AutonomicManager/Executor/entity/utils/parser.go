/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package utils

import (
	"k8s.io/apimachinery/pkg/util/yaml"
	"os"
)

// DecodeYAML
/*
Parse YAML file to a specific struct
 */
func DecodeYAML(filePath string, k8sStruct interface{}) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	err = yaml.NewYAMLOrJSONDecoder(file, 8192).Decode(k8sStruct)
	errFileClose := file.Close()
	if errFileClose != nil {
		return errFileClose
	}
	return err
}