/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package utils

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// testGraph is a graph on which tests are performed
var testGraph *AssociationGraph = nil

// testNewAssociationGraph initializes a new association graph of capacity 4
// testGraph is a package-level variable, because it must be used in multiple tests
func testNewAssociationGraph() {
	testGraph = NewAssociationGraph(4, ParentChild)
}

// testAssociationGraphAddAssociation adds some associations to testGraph
func testAssociationGraphAddAssociation(t *testing.T) {
	if testGraph == nil {
		t.Error("Graph was not created correctly")
	}
	testGraph.AddAssociation(0, "a")
	testGraph.AddAssociation(1, "b")
	testGraph.AddAssociation(2, "c")
	testGraph.AddAssociation(3, "d")
}

// testAssociationGraphAddArc adds some arcs to testGraph
func testAssociationGraphAddArc(t *testing.T) {
	if testGraph == nil {
		t.Error("Graph was not created correctly")
	}
	testGraph.AddArc(0, 1)
	testGraph.AddArc(0, 2)
	testGraph.AddArc(1, 2)
	testGraph.AddArc(1, 3)
	testGraph.AddArc(2, 3)
}

// testAssociationGraphOrientationAndArcs tests whether arcs were created in good order or not
func testAssociationGraphOrientationAndArcs(t *testing.T) {
	if testGraph == nil {
		t.Error("Graph was not created correctly")
	}
	hasEdge := false
	for i := 0; i < testGraph.Order(); i++ {
		for j := 0; j < testGraph.Order(); j++ {
			if !((i == 0 && (j == 1 || j == 2)) || (i == 1 && (j == 2 || j == 3)) || (i == 2 && j == 3)) {
				hasEdge = hasEdge || testGraph.Graph.Edge(i,j)
			}
		}
	}
	assert.False(t, hasEdge)
}

// TestAssociationGraph_BeforeAllCreationTest runs before each test in condition that testGraph does not exist
// and were not tested. In the other case this test is ignored
// It checks if graph has correct size, correct orientation and correct edges
func TestAssociationGraph_BeforeAllCreationTest(t *testing.T) {
	if testGraph != nil {
		t.Log("graph has been created and tested")
		return
	}
	testNewAssociationGraph()
	testAssociationGraphAddAssociation(t)
	testAssociationGraphAddArc(t)

	assert.Equal(t, 4, len(testGraph.associationList))
	assert.Equal(t, 4, len(testGraph.associationMap))
	assert.Equal(t, 4, testGraph.Order())
	assert.Equal(t, ParentChild, testGraph.GetOrientation())

	testAssociationGraphOrientationAndArcs(t)
}

// TestAssociationGraph_Unit is a set of all unit tests
func TestAssociationGraph_Unit(t *testing.T) {
	t.Run("Test-Bad-Association", TestAssociationGraph_IncorrectAssociation)
	t.Run("Test-Bad-Index", TestAssociationGraph_IncorrectIndex)
	t.Run("Test-Get-Index", TestAssociationGraph_GetIndex)
	t.Run("Test-Get-Association", TestAssociationGraph_GetAssociation)
	t.Run("Test-Visit", TestAssociationGraph_Visit)
}

func TestAssociationGraph_IncorrectAssociation(t *testing.T) {
	TestAssociationGraph_BeforeAllCreationTest(t)
	_, ok := testGraph.GetIndex("e")
	assert.False(t, ok)
}

func TestAssociationGraph_IncorrectIndex(t *testing.T) {
	TestAssociationGraph_BeforeAllCreationTest(t)
	_, ok := testGraph.GetAssociation(5)
	assert.False(t, ok)
}

func TestAssociationGraph_GetIndex(t *testing.T) {
	TestAssociationGraph_BeforeAllCreationTest(t)
	_, okA := testGraph.GetIndex("a")
	_, okB := testGraph.GetIndex("b")
	_, okC := testGraph.GetIndex("c")
	_, okD := testGraph.GetIndex("d")
	assert.True(t, okA && okB && okC && okD)
}

func TestAssociationGraph_GetAssociation(t *testing.T) {
	TestAssociationGraph_BeforeAllCreationTest(t)
	_, ok0 := testGraph.GetAssociation(0)
	_, ok1 := testGraph.GetAssociation(1)
	_, ok2 := testGraph.GetAssociation(2)
	_, ok3 := testGraph.GetAssociation(3)
	assert.True(t, ok0 && ok1 && ok2 && ok3)
}

func TestAssociationGraph_Visit(t *testing.T) {
	TestAssociationGraph_BeforeAllCreationTest(t)
	for i := 0; i < testGraph.Order(); i++ {
		testGraph.Visit(i, func(target int, c int64) bool {
			sourceAssociation, ok := testGraph.GetAssociation(i)
			assert.True(t, ok)
			targetAssociation, ok := testGraph.GetAssociation(target)
			assert.True(t, ok)
			switch sourceAssociation {
			case "a": assert.True(t, targetAssociation == "b" || targetAssociation == "c")
			case "b": assert.True(t, targetAssociation == "c" || targetAssociation == "d")
			case "c": assert.True(t, targetAssociation == "d")
			default: t.Errorf("Vertex %d should not be visited", i)
			}
			return true
		})
	}
}

