# Module tsp.eu/executor/entity/utils

## Contents

* association_graph.go:

* association_graph_test.go:

* docker.go:

* docker_test.go:

* parser.go:

* parser_test.go:

* go.mod: generated with the following commands:
```
go mod init tsp.eu/executor/entity/utils
go mod edit -replace tsp.eu/executor/common=../../common
go mod tidy
```

