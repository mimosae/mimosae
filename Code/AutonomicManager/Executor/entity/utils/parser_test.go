/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
Contributor(s): Viktor Colas, Tsimafei Liashkevich
*/

package utils

import (
	v12 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"testing"
)

func TestParser_Unit(t *testing.T) {
	t.Parallel()
	t.Run("Parse-Deployment", TestParseDeployment)
	t.Run("Parse-PersistentVolume", TestParseDeployment)
	t.Run("Parse-PersistentVolumeClaim", TestParseDeployment)
	t.Run("Parse-Secret", TestParseDeployment)
	t.Run("Parse-Service", TestParseDeployment)
}

func TestParseDeployment(t *testing.T) {
	d := &v12.Deployment{}

	err := DecodeYAML("../../../../UseCase_EDF_GDE_Configs/dbps/1_0_0/Kubernetes/deployment_0.yaml", &d)

	if err != nil {
		t.Error(err)
	}

}

func TestParseService(t *testing.T) {
	s := &v1.Service{}

	err := DecodeYAML("../../../../UseCase_EDF_GDE_Configs/dbps/1_0_0/Kubernetes/service_0.yaml", &s)

	if err != nil {
		t.Error(err)
	}
}

func TestParsePersistentVolume(t *testing.T) {
	s := &v1.PersistentVolume{}

	err := DecodeYAML("../../../../UseCase_EDF_GDE_Configs/dbps/1_0_0/Kubernetes/pv_0.yaml", &s)

	if err != nil {
		t.Error(err)
	}
}

func TestParsePersistentVolumeClaim(t *testing.T) {
	s := &v1.PersistentVolumeClaim{}

	err := DecodeYAML("../../../../UseCase_EDF_GDE_Configs/dbps/1_0_0/Kubernetes/pvc_0.yaml", &s)

	if err != nil {
		t.Error(err)
	}
}

func TestSecret(t *testing.T) {
	s := &v1.Secret{}

	err := DecodeYAML("../../../../UseCase_EDF_GDE_Configs/dbps/1_0_0/Kubernetes/pvc_0.yaml", &s)

	if err != nil {
		t.Error(err)
	}
}
