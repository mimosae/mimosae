/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package utils

import (
	"bufio"
	"context"
	"encoding/json"
	"io"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/archive"
	"tsp.eu/executor/common"
)

// BuildDockerImage
/*
Create the binary of the docker image.
Use the certificates of minikube to build the docker image and then build it inside minikube
Show in realtime the actions when building
*/
func BuildDockerImage(path string, imageName string, args map[string]*string) error {
	_, err := os.Open(path + "/Dockerfile")
	if err != nil {
		return err
	}

	// Connection to use Docker Client with the Docker Driver inside Minikube
	// First, get the certificates from minikube and build the image
	user_, err := user.Current()
	if err != nil {
		return err
	}
	homeDirectory := user_.HomeDir
	DockerCertPath := filepath.Join(homeDirectory, ".minikube/certs")
	caFile := filepath.Join(DockerCertPath, "ca.pem")
	certFile := filepath.Join(DockerCertPath, "cert.pem")
	keyFile := filepath.Join(DockerCertPath, "key.pem")
	cli, err := client.NewClientWithOpts(client.WithTLSClientConfig(caFile, certFile, keyFile), client.WithHost("tcp://192.168.49.2:2376"), client.WithAPIVersionNegotiation())
	if err != nil {
		return err
	}

	// Check if the image exist in Minikube Image Repository before image build
	imageExist := false

	// Obtain the list of images in Minikube Repository
	images, err := cli.ImageList(context.Background(), types.ImageListOptions{})
	if err != nil {
		panic(err)
	}

	// Future work can be to store the version of the image in the right side instead of use latest
	for _, image := range images {
		if imageName == strings.Split(image.RepoTags[0], ":")[0] {
			imageExist = true
			break
		}
	}

	// If the image does not exist in Minikube Image Repository then build the image
	if !imageExist {
		// Create the TAR file
		common.InfoLogger.Printf("++ Building Image: %s ++\n", imageName)
		tar, _ := archive.TarWithOptions(path, &archive.TarOptions{})
		opts := types.ImageBuildOptions{
			Tags:      []string{imageName},
			Remove:    true,
			BuildArgs: args,
		}
		common.InfoLogger.Printf("++ TAR Image: %s ++\n", imageName)
		// Build the image
		imageBuildResponse, _err := cli.ImageBuild(context.Background(), tar, opts)
		if _err != nil {
			return _err
		}
		common.InfoLogger.Printf("++ Image Built: %s ++\n", imageName)
		defer func(Body io.ReadCloser) {
			err := Body.Close()
			if err != nil {
				common.ErrorLogger.Println(err)
			}
		}(imageBuildResponse.Body)
		printImageBuildResponse(imageBuildResponse.Body)
		if err != nil {
			return err
		}

	} else {
		// If the image exist just log that the image is in Minikube Image Repository
		common.InfoLogger.Printf("++ Image Already in Minikube: %s ++\n", imageName)
	}
	return nil
}

type jsonDocker struct {
	ID             string                   `json:"id"`
	Stream         string                   `json:"stream"`
	Status         string                   `json:"status"`
	ProgressDetail jsonProgressDetailDocker `json:"progressDetail"`
	Progress       string                   `json:"progress"`
}

type jsonProgressDetailDocker struct {
	Current int `json:"current"`
	Total   int `json:"total"`
}

/*
Show in realtime the building of a docker image
*/
func printImageBuildResponse(rd io.Reader) {
	scanner := bufio.NewScanner(rd)
	for scanner.Scan() {
		jsonLine := &jsonDocker{}
		err := json.Unmarshal([]byte(scanner.Text()), jsonLine)
		if err != nil {
			common.ErrorLogger.Println(err)
		}
		if jsonLine.Stream != "" {
			common.InfoLogger.Printf(jsonLine.Stream)
		}
		switch jsonLine.Status {
		case "Waiting":
			common.InfoLogger.Printf("%s: %s", jsonLine.Status, jsonLine.ID)
		case "Downloading":
			common.InfoLogger.Printf("%s %s: %s", jsonLine.ID, jsonLine.Status, jsonLine.Progress)
		}
	}
}
