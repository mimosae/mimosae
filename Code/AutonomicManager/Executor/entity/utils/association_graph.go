/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package utils

import (
	"github.com/yourbasic/graph"
)

type Orientation string

const(
	ParentChild Orientation = "parentChild"
	ChildParent Orientation = "childParent"
)

// AssociationGraph is an extension of basic graph, which nodes are represented by numbers.
// It has one association list and one association map in plus.
type AssociationGraph struct {
	Graph *graph.Mutable
	associationList []string
	associationMap map[string]int
	orientation Orientation
}

// Order encapsulates Graph's Order method
func(a *AssociationGraph) Order() int{
	return a.Graph.Order()
}

// Visit encapsulates Graph's Visit method
func(a *AssociationGraph) Visit(source int, f func(target int, c int64) bool) bool{
	return a.Graph.Visit(source, f)
}

// NewAssociationGraph creates an empty AssociationGraph of given capacity
func NewAssociationGraph(capacity int, orientation Orientation) *AssociationGraph{
	associatedGraph := graph.New(capacity)
	associationGraph := AssociationGraph{
		Graph: associatedGraph,
		associationList: make([]string, capacity),
		associationMap: make(map[string]int),
		orientation: orientation,
	}
	return &associationGraph
}

// GetIndex provides a node number associated to a given string.
// Returns (-1,false) if association does not exist
func(a *AssociationGraph) GetIndex(association string) (int,bool){
	if _,contains := a.associationMap[association]; !contains {
		return -1,false
	}
	return a.associationMap[association],true
}

// GetAssociation provides a microservice name associated to a given node number.
// Returns ("",false) if association does not exist
func(a *AssociationGraph) GetAssociation(index int) (string, bool) {
	if index >= len(a.associationList) {
		return "",false
	}
	return a.associationList[index],true
}

// AddAssociation creates an association and saves it.
// Returns a boolean that indicates whether operation was successful or not
func(a *AssociationGraph) AddAssociation(index int, association string) bool {
	_,contains := a.associationMap[association]
	if index >= len(a.associationList) || contains {
		return false
	}
	a.associationList[index] = association
	a.associationMap[association] = index
	return true
}

// AddArc adds a an arc in graph representation.
// This arc indicates that "from"(parent) depends on "to"(child)
func(a *AssociationGraph) AddArc(from int, to int) {
	a.Graph.Add(from, to)
}

func (a *AssociationGraph) GetOrientation() Orientation {
	return a.orientation
}
