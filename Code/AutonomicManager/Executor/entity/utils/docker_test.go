/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package utils

import (
	"fmt"
	"testing"
)

func TestBuildDockerImage(t *testing.T) {
	err := BuildDockerImage("../../../../UseCase_EDF_GDE_Configs/fs/1_1_0/Docker", "fs-v1-1-0-image", map[string]*string{})
	if err != nil {
		fmt.Println("!!!!!!You probably must have Minikube Started!!!!!!")
		t.Error(err)
	}
}

func TestDocker_Unit(t *testing.T) {
	t.Run("Test-Build-Docker-Image", TestBuildDockerImage)
}
