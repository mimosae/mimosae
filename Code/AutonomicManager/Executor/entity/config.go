/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package entity

import (
	"fmt"

	v12 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"tsp.eu/executor/entity/utils"
)

// SimpleConfig
// Basic configuration for a Microservice
type SimpleConfig MetaData

// ConfigFiles
// It contains all the paths to the YAML files that are needed to create
// All the necessary structures for kubernetes
type ConfigFiles struct {
	DeployFiles                []string
	ServiceFiles               []string
	PersistentVolumeFiles      []string
	PersistentVolumeClaimFiles []string
	SecretFiles                []string
}

// YAMLConfig
// Store all the YAML config files and the Metadata of a ArchitecturalEntity
type YAMLConfig struct {
	MetaData   *MetaData
	YamlConfig *ConfigFiles
}

// YAMLConfigWithDocker
// Store all the YAML config files, the path of the Docker folder to build the image of the application and the Metadata of a ArchitecturalEntity
type YAMLConfigWithDocker struct {
	YamlConfig   *YAMLConfig
	DockerFolder string
}

// Configuration
// Define all the functions that need to be implemented for a config that can create a ArchitecturalEntity
//go:generate mockgen -destination=mocks/mock_configuration.go -package=mocks tsp.eu/executor/entity Configuration
type Configuration interface {
	InitializeArchitecturalEntity() (*ArchitecturalEntity, error)
	BuildDockerImages(args map[string]*string) (bool, error)
	SetMeta(meta *MetaData)
}

func (config *YAMLConfig) SetMeta(meta *MetaData) {
	config.MetaData = meta
}

func (config *YAMLConfigWithDocker) SetMeta(meta *MetaData) {
	config.YamlConfig.MetaData = meta
}

// InitializeArchitecturalEntity
// For a SimpleConfig return always nil
func (config SimpleConfig) InitializeArchitecturalEntity() (*ArchitecturalEntity, error) {
	return nil, nil
}

// InitializeArchitecturalEntity
// For a YAMLConfig return a ArchitecturalEntity with all the structure for kubernetes created
func (config *YAMLConfig) InitializeArchitecturalEntity() (*ArchitecturalEntity, error) {
	entity := new(ArchitecturalEntity)
	var deployments []*v12.Deployment
	var services []*v1.Service
	var persistentVolumes []*v1.PersistentVolume
	var persistentVolumeClaims []*v1.PersistentVolumeClaim
	var secrets []*v1.Secret
	label := FormatMetaData(config.MetaData, "", "")
	for _, deployConfig := range config.YamlConfig.DeployFiles {
		deploy := &v12.Deployment{}
		err := utils.DecodeYAML(deployConfig, &deploy)
		deploy.SetName(FormatMetaData(config.MetaData, "", "-deployment"))

		deploy.Labels = setLabelMap(deploy.Labels, label)
		deploy.Spec.Selector.MatchLabels = setLabelMap(deploy.Spec.Selector.MatchLabels, label)
		deploy.Spec.Template.Labels = setLabelMap(deploy.Spec.Template.Labels, label)
		for _, container := range deploy.Spec.Template.Spec.Containers {
			for _, envSource := range container.EnvFrom {
				envSource.SecretRef.Name = FormatMetaData(config.MetaData, "", "-secret")
			}
		}
		if err != nil {
			return nil, err
		}
		for _, volume := range deploy.Spec.Template.Spec.Volumes {
			if volume.PersistentVolumeClaim != nil {
				volume.PersistentVolumeClaim.ClaimName = FormatMetaData(config.MetaData, "", "-pvc")
			}
		}

		deployments = append(deployments, deploy)
	}
	for _, serviceConfig := range config.YamlConfig.ServiceFiles {
		service := &v1.Service{}
		err := utils.DecodeYAML(serviceConfig, &service)
		if err != nil {
			return nil, err
		}
		service.Labels = setLabelMap(service.Labels, label)
		service.Spec.Selector = setLabelMap(service.Spec.Selector, label)

		services = append(services, service)
	}
	for _, persistentVolumeConfig := range config.YamlConfig.PersistentVolumeFiles {
		persistentVolume := &v1.PersistentVolume{}
		err := utils.DecodeYAML(persistentVolumeConfig, &persistentVolume)
		persistentVolume.SetName(FormatMetaData(config.MetaData, "", "-pv"))
		if err != nil {
			return nil, err
		}
		persistentVolume.Labels = setLabelMap(persistentVolume.Labels, label)

		persistentVolumes = append(persistentVolumes, persistentVolume)
	}
	for _, persistentVolumeClaimConfig := range config.YamlConfig.PersistentVolumeClaimFiles {
		persistentVolumeClaim := &v1.PersistentVolumeClaim{}
		err := utils.DecodeYAML(persistentVolumeClaimConfig, &persistentVolumeClaim)
		persistentVolumeClaim.SetName(FormatMetaData(config.MetaData, "", "-pvc"))
		if err != nil {
			return nil, err
		}
		persistentVolumeClaim.Labels = setLabelMap(persistentVolumeClaim.Labels, label)

		persistentVolumeClaims = append(persistentVolumeClaims, persistentVolumeClaim)
	}
	for _, secretConfig := range config.YamlConfig.SecretFiles {
		secret := &v1.Secret{}
		err := utils.DecodeYAML(secretConfig, &secret)
		secret.SetName(FormatMetaData(config.MetaData, "", "-secret"))
		if err != nil {
			return nil, err
		}

		secrets = append(secrets, secret)
	}
	entity.MetaData = config.MetaData
	entity.Services = services
	entity.Secrets = secrets
	entity.Deployments = deployments
	entity.PersistentVolume = persistentVolumes
	entity.PersistentVolumeClaim = persistentVolumeClaims
	return entity, nil
}

// InitializeArchitecturalEntity
// For a YAMLConfigWithDocker return a ArchitecturalEntity with all the structure for kubernetes created
func (config *YAMLConfigWithDocker) InitializeArchitecturalEntity() (*ArchitecturalEntity, error) {
	entity, _err := config.YamlConfig.InitializeArchitecturalEntity()
	if _err != nil {
		return nil, _err
	}

	return entity, nil
}

// BuildDockerImages
// For a YAMLConfigWithDocker build the Docker image
func (config *YAMLConfigWithDocker) BuildDockerImages(args map[string]*string) (bool, error) {
	fmt.Println(FormatMetaData(config.YamlConfig.MetaData, "", "-image"))
	err := utils.BuildDockerImage(config.DockerFolder, FormatMetaData(config.YamlConfig.MetaData, "", "-image"), args)
	fmt.Println("***** Docker image built *****")
	if err != nil {
		return false, err
	}
	return true, nil
}

// BuildDockerImages
// For a YAMLConfig build no image because there is none
func (config *YAMLConfig) BuildDockerImages(args map[string]*string) (bool, error) {
	fmt.Println(FormatMetaData(config.MetaData, "", "-image"))
	fmt.Println("*** No Image to Build ***")
	return false, nil
}

func setLabelMap(labels map[string]string, app string) (_labels map[string]string) {
	if labels != nil {
		_labels = labels
		_labels["app"] = app
	} else {
		_labels = map[string]string{"app": app}
	}

	return
}
