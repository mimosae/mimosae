/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package entity

import (
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	v12 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
//	"tsp.eu/executor/entity/local_mocks"
	"tsp.eu/executor/entity/utils"
)

// mockArchitecturalEntity mocks an ArchitecturalEntity with a mocked metaData, empty dependency graph
// and custom(even empty) kubernetes objects
func mockArchitecturalEntity() *ArchitecturalEntity{
	return &ArchitecturalEntity{
		MetaData: mockMetaData(),
		Pods: nil,
		Deployments: []*v12.Deployment{&v12.Deployment{Status: v12.DeploymentStatus{Replicas: 0}}},
		Secrets: []*v1.Secret{&v1.Secret{Type: v1.SecretTypeOpaque}, &v1.Secret{Type: v1.SecretTypeOpaque}},
		Services: []*v1.Service{&v1.Service{}},
		PersistentVolumeClaim: []*v1.PersistentVolumeClaim{&v1.PersistentVolumeClaim{}},
		PersistentVolume: []*v1.PersistentVolume{&v1.PersistentVolume{}},
		DependencyGraph: nil,
	}
}

func TestArchitecturalEntity_Unit(t *testing.T) {
	t.Run("Test-MetaData-Getters", TestMetaDataGetters)
	t.Run("Test-MetaData-From-Getter", TestMetaDataFromGetter)
	t.Run("Test-Dependencies", TestDependencies)
	t.Run("Test-Complete-With-Connection-Secrets", TestArchitecturalEntity_CompleteWithConnectionSecrets)
}

func TestMetaDataGetters(t *testing.T) {
	metaData := mockMetaData()
	assert.Equal(t, "test-service", metaData.GetName())
	assert.Equal(t, "1.0.0", metaData.GetVersion())
	assert.Equal(t, "test-service", metaData.GetType())
}

func TestMetaDataFromGetter(t *testing.T) {
	ctrl := gomock.NewController(t)
	metaDataGetter := mocks.NewMockMetaDataGetter(ctrl)

	metaDataGetter.EXPECT().GetName().Return("test-service")
	metaDataGetter.EXPECT().GetVersion().Return("1.0.0")
	metaDataGetter.EXPECT().GetType().Return("test-service")

	metaData := MetaDataFromGetter(metaDataGetter)
	assert.Equal(t, "test-service", metaData.GetName())
	assert.Equal(t, "1.0.0", metaData.GetVersion())
	assert.Equal(t, "test-service", metaData.GetType())
}

func TestArchitecturalEntity_CompleteWithConnectionSecrets(t *testing.T) {
	entity := mockArchitecturalEntity()
	entitySecretsNum := len(entity.Secrets)

	var secrets []*v1.Secret
	for i := 0; i < 3; i++ {
		secrets = append(secrets, &v1.Secret{
			ObjectMeta: metav1.ObjectMeta{Name: fmt.Sprint(i)},
		})
	}

	err := entity.CompleteWithConnectionSecrets(secrets)
	if err != nil {
		t.Error(err)
	}

	containsRef := func(sources []v1.EnvFromSource, refName string) bool{
		for _, source := range sources {
			if source.SecretRef.Name == refName {
				return true
			}
		}
		return false
	}

	assert.Equal(t, 3+entitySecretsNum, len(entity.Secrets))
	for _, deployment := range entity.Deployments {
		for _, container := range deployment.Spec.Template.Spec.Containers {
			for i := 0; i < 3; i++ {
				assert.True(t, containsRef(container.EnvFrom, fmt.Sprint(i)))
			}
		}
	}
}

// TestDependencies takes a mocked entity and creates dependencies(dependency graph) on it.
// It goes through the graph and fills a compare map. We verify, then the correctness of created arcs.
func TestDependencies(t *testing.T) {
	entity := mockArchitecturalEntity()
	entity.InitDependencyGraph(utils.ParentChild)
	entity.CreateDependencies()

	assert.Equal(t, 6, entity.DependencyGraph.Order())
	compareDependenciesMap := make(map[string][]string)
	for i := 0; i < entity.DependencyGraph.Order(); i++ {
		entity.DependencyGraph.Visit(i, func(target int, c int64) bool {
			sourceAssociation, ok := entity.DependencyGraph.GetAssociation(i)
			if !ok {
				t.Errorf("Association to vertex number %d must exist", i)
			}

			targetAssociation, ok := entity.DependencyGraph.GetAssociation(target)
			if !ok {
				t.Errorf("Association to vertex number %d must exist", i)
			}

			compareDependenciesMap[sourceAssociation] = append(compareDependenciesMap[sourceAssociation], targetAssociation)

			return true
		})
	}
	for key, value := range compareDependenciesMap {
		switch key {
		case "deployment_0":
			assert.Equal(t, 3, len(value))
			assert.Contains(t, value, "secret_0")
			assert.Contains(t, value, "secret_1")
			assert.Contains(t, value, "pvc_0")
		case "pvc_0":
			assert.Equal(t, 1, len(value))
			assert.Contains(t, value, "pv_0")
		default:
			assert.Equal(t, 0, len(value))
		}
	}
}
