/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package entity

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// mockMetaData mocks a custom metaData
func mockMetaData() *MetaData {
	return NewMetaData("test-service", "1.0.0", "test", "test-service")
}

// mockConfigFiles mocks a ConfigFiles filled by hand
func mockConfigFiles() *ConfigFiles {
	pathToMicroserviceConfig := "../../../UseCase_EDF_GDE_Configs/fs/1_0_0/Kubernetes/"
	return &ConfigFiles{
		DeployFiles: []string{pathToMicroserviceConfig + "deployment_0.yaml"},
		SecretFiles: []string{pathToMicroserviceConfig + "secret_0.yaml"},
		ServiceFiles: []string{pathToMicroserviceConfig + "service_0.yaml"},
		PersistentVolumeFiles: []string{},
		PersistentVolumeClaimFiles: []string{},
	}
}

func mockSimpleConfig() *SimpleConfig {
	simpleConfig := SimpleConfig(*mockMetaData())
	return &simpleConfig
}

func mockYAMLConfig() *YAMLConfig {
	return &YAMLConfig {
		MetaData: mockMetaData(),
		YamlConfig: mockConfigFiles(),
	}
}

func mockYAMLConfigWithDocker() *YAMLConfigWithDocker {
	return &YAMLConfigWithDocker{
		YamlConfig: &YAMLConfig{
			MetaData: mockMetaData(),
			YamlConfig: mockConfigFiles(),
		},
		DockerFolder: "../../../UseCase_EDF_GDE_Configs/fs/1_0_0/Docker",
	}
}

func TestConfig_Unit(t *testing.T) {
	t.Run("Test-SimpleConfig", TestSimpleConfig_InitializeArchitecturalEntity)
	t.Run("Test-YAMLConfig", TestYAMLConfig_InitializeArchitecturalEntity)
	t.Run("Test-YAMLConfigWithDocker", TestYAMLConfigWithDocker)
}

func TestSimpleConfig_InitializeArchitecturalEntity(t *testing.T) {
	simpleConfig := mockSimpleConfig()
	entity, err := simpleConfig.InitializeArchitecturalEntity()
	assert.Nil(t, entity)
	assert.Nil(t, err)
}

func TestYAMLConfig_InitializeArchitecturalEntity(t *testing.T) {
	YAMLConfig := mockYAMLConfig()
	_, err := YAMLConfig.InitializeArchitecturalEntity()
	assert.Nil(t, err)
}

func TestYAMLConfigWithDocker(t *testing.T) {
	t.Run("Test-YAMLConfigWithDocker-Init-ArchitecturalEntity", TestYAMLConfigWithDocker_InitializeArchitecturalEntity)
	t.Run("Test-YAMLConfigWithDocker-BuildDockerImages", TestYAMLConfigWithDocker_BuildDockerImages)
}

func TestYAMLConfigWithDocker_InitializeArchitecturalEntity(t *testing.T) {
	YAMLConfigWithDocker := mockYAMLConfigWithDocker()
	_, err := YAMLConfigWithDocker.InitializeArchitecturalEntity()
	assert.Nil(t, err)
}

func TestYAMLConfigWithDocker_BuildDockerImages(t *testing.T) {
	YAMLConfigWithDocker := mockYAMLConfigWithDocker()
	_, err := YAMLConfigWithDocker.BuildDockerImages(map[string]*string{})
	if err != nil {
		t.Errorf("You must probably have minikube started: %v\n", err)
	}
}