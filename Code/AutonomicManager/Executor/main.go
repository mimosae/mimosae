/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package main

import (
	"net"

	"tsp.eu/executor/common"
	"tsp.eu/executor/implementation_registry"

	"google.golang.org/grpc"
	"tsp.eu/executor/server"
	"tsp.eu/executor/server/proto"
)

func main() {
	registry := implementation_registry.ImplementationRegistry{}
	err := registry.InitImplementationRegistry("../../UseCase_EDF_GDE_Configs")
	if err != nil {
		panic(err)
	}
	listener, err := net.Listen("tcp", ":4040")
	if err != nil {
		panic(err)
	}

	srv := grpc.NewServer()
	grpcServer := server.NewServer(&registry)
	proto.RegisterCommandServer(srv, grpcServer)

	common.InfoLogger.Println("========== Executor listening on Port: 4040 ==========")
	err = srv.Serve(listener)
	if err != nil {
		panic(err)
	}
}
