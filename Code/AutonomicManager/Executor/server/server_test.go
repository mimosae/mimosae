/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package server

import (
	"context"
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"strconv"
	"strings"
	"testing"
	"tsp.eu/executor/entity"
	"tsp.eu/executor/implementation_registry"
//	factoryMock "tsp.eu/executor/orchestrator/mocks"
	"tsp.eu/executor/server/proto"
)

// defineOrchestratorBehavior defines a behavior of an orchestrator given in parameters.
// It allows defining two types of behavior: correct and with an error
func defineOrchestratorBehavior(orchestrator *factoryMock.MockOrchestrator, returnsError bool, maxTimes int) {
	if returnsError {
		orchestrator.EXPECT().CreateArchitecturalEntity(gomock.Any(), gomock.Any()).Return(nil, errors.New("Test error "))
		orchestrator.EXPECT().RemoveArchitecturalEntity(gomock.Any()).Return(nil, errors.New("Test error "))
	} else {
		orchestrator.EXPECT().CreateArchitecturalEntity(gomock.Any(), gomock.Any()).Return(&entity.ArchitecturalEntity{}, nil)
		orchestrator.EXPECT().RemoveArchitecturalEntity(gomock.Any()).Return(&entity.ArchitecturalEntity{}, nil)
	}
	orchestrator.EXPECT().CreateArchitecturalEntity(gomock.Any(), gomock.Any()).Times(maxTimes)
	orchestrator.EXPECT().RemoveArchitecturalEntity(gomock.Any()).Times(maxTimes)
}

// serverMock creates a server instance with mocked handler.
// See handler.go and gomock to know more
func serverMock(t *testing.T) *Server {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	return &Server{
		factory: factoryMock.NewMockOrchestrator(ctrl),
	}
}

// planMock creates a custom plan with given meta arguments.
// Each meta is a list of for elements of the following format:
// [entity_type_to_create, name, version, type] in the microservice case
// and [entity_type_to_create, name, version, type, clientID, serverID] in client_server case,
// look at the following example:
// [ [ ["microservice", "fdbsi", "0.0.1", "fdbs"],[...],[...] ] , [ ["client_server_contract", "csi", "0.0.1", "cs", "0", ["1"]],[...] ] ]
// Operation is unique for all types.
func planMock(operation proto.Operation, metas ...[][]string) *proto.ConfigurationPlan {
	plan := &proto.ConfigurationPlan{
		ConcurrentActions: []*proto.ConfigurationPlan_ConcurrentActions{},
	}
	current := 1
	for i := range(metas) {
		actions := []*proto.Action{}
		for _, meta := range (metas[i]) {
			entityMeta := &proto.Meta{
				Name:    meta[1],
				Version: meta[2],
				Type:    meta[3],
			}
			switch strings.ToUpper(meta[0]) {
			case "MICROSERVICE":
				action := &proto.Action{}
				action.InnerAction = &proto.Action_Microservice{
					Microservice: &proto.Microservice{
						ID: uint64(current),
						Meta: entityMeta,
						Operation: proto.Operation_CREATE,
					},
				}
				actions = append(actions, action)
			case "CLIENT_SERVER_CONTRACT":
				action := &proto.Action{}
				cID, err := strconv.Atoi(meta[4])
				if err != nil {
					panic(err)
				}
				sID, err := strconv.Atoi(meta[5])
				if err != nil {
					panic(err)
				}
				action.InnerAction = &proto.Action_ClientServerContract{
					ClientServerContract: &proto.ClientServerContract{
						ID: uint64(current),
						Meta: entityMeta,
						Operation: proto.Operation_LINK,
						ClientID: uint64(cID),
						ServerID: uint64(sID),
					},
				}
				actions = append(actions, action)
			}
			current++
		}
		plan.ConcurrentActions = append(plan.ConcurrentActions, &proto.ConfigurationPlan_ConcurrentActions{
			Action: actions,
		})
	}
	return plan
}

func TestServer_Unit(t *testing.T) {
	t.Run("Error-Test", TestServer_FailingPlan)
	t.Run("Success-test", TestServer_SucceededPlan)
}

func TestServer_FailingPlan(t *testing.T) {
	//t.Parallel()
	t.Run("Entity-Does-Not-Exist", TestServer_EntityDoesNotExist)
	t.Run("Dependencies-Error", TestServer_InternDependenciesError)
	t.Run("Plan-Fail", TestServer_PlanExecutionFails)
}

func TestServer_SucceededPlan(t *testing.T) {
	t.Run("Execute-Plan", TestServer_ExecuteConfigurationPlan)
}


// TestServer_EntityDoesNotExist checks if an error has occurred, when
// entity is not described in implementation_registry
func TestServer_EntityDoesNotExist(t *testing.T) {
	server := serverMock(t)
	metas := make([][]string, 2)
	metas[0] = []string {"microservice", "permsi", "1.0.0", "perms"}
	metas[1] = []string {"microservice", "abra", "152.187.1.1", "cadabra"}
	plan := planMock(proto.Operation_CREATE, metas)

	registry := implementation_registry.ImplementationRegistry{}
	err := registry.InitImplementationRegistry("../../../UseCase_EDF_GDE_Configs")
	if err != nil {
		t.Error(err)
	}

	res, err := server.ExecuteConfigurationPlan(context.TODO(), plan)
	fmt.Printf("message: %s\n", res.Message)
	assert.Equal(t, uint32(400), res.StatusError)
	assert.Nil(t, err)
}

//TestServer_InternDependenciesError checks if an error has occurred, when
// there is some dependencies between microservices in concurrent actions
func TestServer_InternDependenciesError(t *testing.T) {
	server := serverMock(t)
	metas := make([][]string, 3)
	metas[0] = []string {"microservice", "permsi", "1.0.0", "perms"}
	metas[1] = []string {"microservice", "usi", "1.0.0", "us"}
	metas[2] = []string {"client_server_contract", "link", "1.0.0", "link", "1", "2"}
	plan := planMock(proto.Operation_CREATE, metas)

	registry := implementation_registry.ImplementationRegistry{}
	err := registry.InitImplementationRegistry("../../../UseCase_EDF_GDE_Configs")
	if err != nil {
		t.Error(err)
	}

	res, err := server.ExecuteConfigurationPlan(context.TODO(), plan)
	fmt.Printf("message: %s\n", res.Message)
	assert.Equal(t, uint32(400), res.StatusError)
	assert.Nil(t, err)
}

func TestServer_PlanExecutionFails(t *testing.T) {
	server := serverMock(t)
	mockFactory, ok := server.factory.(*factoryMock.MockOrchestrator)
	if !ok {
		t.Error("Error in cast")
	}
	defineOrchestratorBehavior(mockFactory, true, 5)

	registry := implementation_registry.ImplementationRegistry{}
	err := registry.InitImplementationRegistry("../../../UseCase_EDF_GDE_Configs")
	if err != nil {
		t.Error(err)
	}

	server.factory = mockFactory
	metas1 := make([][]string, 3)
	metas2 := make([][]string, 2)

	metas1[0] = []string {"microservice", "permsi", "1.0.0", "perms"}
	metas1[1] = []string {"microservice", "usi", "1.0.0", "us"}
	metas1[2] = []string {"microservice", "fsi", "1.0.0", "fs"}

	metas2[0] = []string {"client_server_contract", "link1", "1.0.0", "link1", "1", "2"}
	metas2[1] = []string {"client_server_contract", "link2", "1.0.0", "link2", "2", "3"}

	plan := planMock(proto.Operation_CREATE, metas1, metas2)
	res, err := server.ExecuteConfigurationPlan(context.TODO(), plan)

	fmt.Println(plan)

	fmt.Printf("message: %s\n", res.Message)
	assert.Equal(t, uint32(500), res.StatusError)
	assert.Nil(t, err)
}

func TestServer_ExecuteConfigurationPlan(t *testing.T) {
	server := serverMock(t)
	mockFactory, ok := server.factory.(*factoryMock.MockOrchestrator)
	if !ok {
		t.Error("Error in cast")
	}
	defineOrchestratorBehavior(mockFactory, false, 5)

	registry := implementation_registry.ImplementationRegistry{}
	err := registry.InitImplementationRegistry("../../../UseCase_EDF_GDE_Configs")
	if err != nil {
		t.Error(err)
	}

	server.factory = mockFactory
	metas1 := make([][]string, 3)
	metas2 := make([][]string, 2)

	metas1[0] = []string {"microservice", "permsi", "1.0.0", "perms"}
	metas1[1] = []string {"microservice", "usi", "1.0.0", "us"}
	metas1[2] = []string {"microservice", "fsi", "1.0.0", "fs"}

	metas2[0] = []string {"client_server_contract", "link1", "1.0.0", "link1", "1", "2"}
	metas2[1] = []string {"client_server_contract", "link2", "1.0.0", "link2", "2", "3"}

	plan := planMock(proto.Operation_CREATE, metas1, metas2)
	res, err := server.ExecuteConfigurationPlan(context.TODO(), plan)
	fmt.Printf("message: %s\n", res.Message)
	assert.Equal(t, uint32(200), res.StatusError)
	assert.Nil(t, err)
}
