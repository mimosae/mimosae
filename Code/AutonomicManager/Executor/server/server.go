/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package server

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"sync"

	"tsp.eu/executor/common"
	"tsp.eu/executor/concurrency"
	"tsp.eu/executor/entity"
	"tsp.eu/executor/implementation_registry"
	"tsp.eu/executor/orchestrator"
	"tsp.eu/executor/server/proto"
)

var (
	databaseSystemMap       = map[uint64]*proto.DatabaseSystem{}
	databaseConnectorMap    = map[uint64]*proto.DatabaseConnector{}
	pubSubConnectorMap      = map[uint64]*proto.PubSubConnector{}
	produceContractMap      = map[uint64]*proto.ProduceContract{}
	consumeContractMap      = map[uint64]*proto.ConsumeContract{}
	clientServerContractMap = map[uint64]*proto.ClientServerContract{}
)

type Server struct {
	factory orchestrator.Orchestrator
}

func NewServer(registry implementation_registry.RegistryHandler) *Server {
	srv := &Server{
		factory: orchestrator.NewFactory(registry),
	}
	return srv
}

func (s *Server) ExecuteConfigurationPlan(ctx context.Context, plan *proto.ConfigurationPlan) (*proto.Response, error) {
	common.InfoLogger.Println("========== New plan to execute ==========")
	microservices, err := s.processPlan(plan)
	if err != nil {
		return &proto.Response{
			StatusError: 400,
			Message:     fmt.Sprintf("Plan cannot be processed: %s", err),
		}, nil
	}

	for i, concurrentActions := range plan.ConcurrentActions {
		common.InfoLogger.Printf("=== Current Actions %d ===\n", i)
		actions := map[string]*concurrency.Action{}
		for _, protoAction := range concurrentActions.GetAction() {
			var instruction *func(a interface{}) error
			var getter *proto.Meta

			if microService := protoAction.GetMicroservice(); microService != nil {
				instruction = s.selectOperation(microService.GetOperation(), microservices[microService.GetID()])
				getter = microService.GetMeta()
			}
			if databaseSystem := protoAction.GetDatabaseSystem(); databaseSystem != nil {
				instruction = s.selectOperation(databaseSystem.GetOperation(), microservices[databaseSystem.GetID()])
				getter = databaseSystem.GetMeta()
			}
			if pubSubConnector := protoAction.GetPubSubConnector(); pubSubConnector != nil {
				connector := &entity.Connector{
					GlobalType: entity.PubSubConnector,
					MetaData:   pubSubConnector.GetMeta(),
				}
				switch pubSubConnector.GetOperation() {
				case proto.Operation_CREATE:
					f := func(a interface{}) error {
						_, err := s.factory.CreateConnector(connector)
						return err
					}
					instruction = &f
				case proto.Operation_DELETE:
					f := func(a interface{}) error {
						_, err := s.factory.RemoveConnector(connector)
						return err
					}
					instruction = &f
				}
				getter = pubSubConnector.GetMeta()
			}

			if instruction == nil || getter == nil {
				continue
			}

			getter.Type = strings.Replace(getter.GetType(), ".", "_", -1)
			action := concurrency.NewAction(instruction, []*sync.WaitGroup{}, &sync.WaitGroup{}, getter)
			actions[fmt.Sprintf("%d", protoAction.GetID())] = action
		}

		err := concurrency.ExecuteActions(actions)
		if err != nil {
			return &proto.Response{StatusError: 500, Message: fmt.Sprintln(err)}, nil
		}
	}

	common.InfoLogger.Println("========== The plan has been executed ==========")
	return &proto.Response{StatusError: 200, Message: "ok!"}, nil
}

func (s *Server) selectOperation(operation proto.Operation, connectors []*entity.Connector) *func(a interface{}) error {
	var instruction func(a interface{}) error

	switch operation {
	case proto.Operation_CREATE:
		instruction = func(a interface{}) error {
			_, err := s.factory.CreateArchitecturalEntity(a.(entity.MetaDataGetter), connectors)
			return err
		}
	case proto.Operation_DELETE:
		instruction = func(a interface{}) error {
			_, err := s.factory.RemoveArchitecturalEntity(a.(entity.MetaDataGetter))
			return err
		}
	}

	return &instruction
}

func (s *Server) processPlan(plan *proto.ConfigurationPlan) (microServiceMap map[uint64][]*entity.Connector, err error) {
	timeStampMap := map[uint64]uint32{}

	microServiceMap = map[uint64][]*entity.Connector{}

	timeStamp := uint32(0)
	for i, concurrentActions := range plan.GetConcurrentActions() {
		timeStamp = uint32(len(plan.GetConcurrentActions()))*uint32(i) + timeStamp

		for _, action := range concurrentActions.GetAction() {
			switch action.GetInnerAction().(type) {
			case *proto.Action_Microservice:
				microService := action.GetMicroservice()
				meta := microService.GetMeta()
				if !checkInImplementationRegistry(meta) {
					return nil, errors.New("not in implementation registry, see logs for more information ")
				}
				microServiceMap[microService.GetID()] = []*entity.Connector{}
				timeStampMap[microService.GetID()] = timeStamp
			case *proto.Action_ClientServerContract:
				clientServerContract := action.GetClientServerContract()
				clientServerContractMap[clientServerContract.GetID()] = clientServerContract
				timeStampMap[clientServerContract.GetID()] = timeStamp
			case *proto.Action_DatabaseSystem:
				databaseSystem := action.GetDatabaseSystem()
				meta := databaseSystem.GetMeta()
				if !checkInImplementationRegistry(meta) {
					return nil, errors.New("not in implementation registry, see logs for more information ")
				}
				databaseSystemMap[databaseSystem.GetID()] = databaseSystem
				timeStampMap[databaseSystem.GetID()] = timeStamp
			case *proto.Action_PubSubConnector:
				pubSubConnector := action.GetPubSubConnector()
				pubSubConnectorMap[pubSubConnector.GetID()] = pubSubConnector
				timeStampMap[pubSubConnector.GetID()] = timeStamp
			case *proto.Action_DatabaseConnector:
				databaseConnector := action.GetDatabaseConnector()
				databaseConnectorMap[databaseConnector.GetID()] = databaseConnector
				timeStampMap[databaseConnector.GetID()] = timeStamp
			case *proto.Action_ProduceContract:
				produceContract := action.GetProduceContract()
				produceContractMap[produceContract.GetID()] = produceContract
				timeStampMap[produceContract.GetID()] = timeStamp
			case *proto.Action_ConsumeContract:
				consumeContract := action.GetConsumeContract()
				consumeContractMap[consumeContract.GetID()] = consumeContract
				timeStampMap[consumeContract.GetID()] = timeStamp
			}
		}
	}

	for key, databaseConnector := range databaseConnectorMap {
		timeStamp := timeStampMap[key]
		invert := databaseConnector.GetOperation() == proto.Operation_DISCONNECT
		err = checkTimeStamp(timeStamp, timeStampMap, databaseConnector.GetDatabaseID(), key, invert)
		if err != nil {
			return
		}
		err = checkTimeStamp(timeStamp, timeStampMap, databaseConnector.GetClientID(), key, invert)
		if err != nil {
			return
		}

		if value, containsKey := microServiceMap[databaseConnector.GetClientID()]; containsKey {
			microServiceMap[databaseConnector.GetClientID()] = append(value, &entity.Connector{
				GlobalType: entity.DatabaseSystemConnector,
				MetaData:   entity.MetaDataFromGetter(databaseSystemMap[databaseConnector.GetDatabaseID()].GetMeta()),
			})
		}
	}

	for key, produceContract := range produceContractMap {
		invert := produceContract.GetOperation() == proto.Operation_UNLINK
		timeStamp := timeStampMap[key]
		err = checkTimeStamp(timeStamp, timeStampMap, produceContract.GetConnectorID(), key, invert)
		if err != nil {
			return
		}
		err = checkTimeStamp(timeStamp, timeStampMap, produceContract.GetProducerID(), key, invert)
		if err != nil {
			return
		}

		if value, containsKey := microServiceMap[produceContract.GetProducerID()]; containsKey {
			microServiceMap[produceContract.GetProducerID()] = append(value, &entity.Connector{
				GlobalType: entity.PubSubConnector,
				MetaData:   entity.MetaDataFromGetter(pubSubConnectorMap[produceContract.GetConnectorID()].GetMeta()),
			})
		}
	}

	for key, consumeContract := range consumeContractMap {
		invert := consumeContract.GetOperation() == proto.Operation_UNLINK
		timeStamp := timeStampMap[key]
		err = checkTimeStamp(timeStamp, timeStampMap, consumeContract.GetConnectorID(), key, invert)
		if err != nil {
			return
		}
		err = checkTimeStamp(timeStamp, timeStampMap, consumeContract.GetConsumerID(), key, invert)
		if err != nil {
			return
		}

		if value, containsKey := microServiceMap[consumeContract.GetConsumerID()]; containsKey {
			microServiceMap[consumeContract.GetConsumerID()] = append(value, &entity.Connector{
				GlobalType: entity.PubSubConnector,
				MetaData:   entity.MetaDataFromGetter(pubSubConnectorMap[consumeContract.GetConnectorID()].GetMeta()),
			})
		}
	}

	for key, clientServerContract := range clientServerContractMap {
		timeStamp := timeStampMap[key]
		invert := clientServerContract.GetOperation() == proto.Operation_UNLINK
		err = checkTimeStamp(timeStamp, timeStampMap, clientServerContract.GetServerID(), clientServerContract.GetID(), invert)
		if err != nil {
			return
		}
		err = checkTimeStamp(timeStamp, timeStampMap, clientServerContract.GetClientID(), clientServerContract.GetID(), invert)
		if err != nil {
			return
		}
	}
	return
}

func checkInImplementationRegistry(meta *proto.Meta) bool {
	if meta == nil {
		common.ErrorLogger.Printf("The micro-service %s does not have any meta", meta.GetName())
		return false
	}

	microserviceKey := entity.FormatMetaData(meta, "", "")
	if !implementation_registry.ContainsKey(microserviceKey) {
		common.ErrorLogger.Printf("The micro-service %s does not exist", microserviceKey)
		return false
	}
	return true
}

func checkTimeStamp(timeStampToCheck uint32, timeStampMap map[uint64]uint32, idToCheck uint64, idChecking uint64, invertCheck bool) (err error) {
	err = nil
	if value, containsKey := timeStampMap[idToCheck]; containsKey {
		if !invertCheck {
			if value >= timeStampToCheck {
				err = fmt.Errorf("cannot create connector %d: Object %d does not exist yet", idChecking, idToCheck)
			}
		} else {
			if value <= timeStampToCheck {
				err = fmt.Errorf("cannot create connector %d: Object %d does not exist yet", idChecking, idToCheck)
			}
		}
	}
	return
}
