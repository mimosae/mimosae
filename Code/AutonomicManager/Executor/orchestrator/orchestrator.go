/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package orchestrator

import (
	"errors"
	"math/rand"

	"github.com/yourbasic/graph"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"tsp.eu/executor/common"
	"tsp.eu/executor/entity"
	"tsp.eu/executor/entity/utils"
	"tsp.eu/executor/implementation_registry"
)

//go:generate mockgen -destination=mocks/mock_orchestrator.go -package=mocks tsp.eu/executor/orchestrator Orchestrator
type Orchestrator interface {
	CreateArchitecturalEntity(metaData entity.MetaDataGetter, connectors []*entity.Connector) (*entity.ArchitecturalEntity, error)
	RemoveArchitecturalEntity(metaData entity.MetaDataGetter) (*entity.ArchitecturalEntity, error)
	CreateConnector(connector *entity.Connector) (*entity.ArchitecturalEntity, error)
	RemoveConnector(connector *entity.Connector) (*entity.ArchitecturalEntity, error)
}

type Factory struct {
	handler         Handler
	registryHandler implementation_registry.RegistryHandler
}

func NewFactory(registryHandler implementation_registry.RegistryHandler) *Factory {
	factory := &Factory{
		handler:         &Manager{},
		registryHandler: registryHandler,
	}
	err := factory.handler.InitApi()
	if err != nil {
		panic(err)
	}

	return factory
}

func (f *Factory) CreateArchitecturalEntity(metaData entity.MetaDataGetter, connectors []*entity.Connector) (*entity.ArchitecturalEntity, error) {
	common.InfoLogger.Printf("=============== Begin Creation of ArchitecturalEntity: %s ===============\n", metaData.GetName())
	// TODO: Possibility to use TCP to get the configuration from the implementation Registry
	yamlConfig, err := f.registryHandler.CreateConfiguration(metaData)
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}
	_, err = yamlConfig.BuildDockerImages(map[string]*string{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	common.InfoLogger.Println("===== Config Created =====")
	var microService *entity.ArchitecturalEntity
	microService, err = yamlConfig.InitializeArchitecturalEntity()
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	connectionSecrets, err := f.handler.GetConnectorSecrets(connectors)
	if err != nil {
		return nil, err
	}

	err = microService.CompleteWithConnectionSecrets(connectionSecrets)
	if err != nil {
		return nil, err
	}

	microService.InitDependencyGraph(utils.ParentChild)
	common.InfoLogger.Println("===== ArchitecturalEntity Initialized =====")
	microService.CreateDependencies()

	isAcyclic := graph.Acyclic(microService.DependencyGraph)
	if !isAcyclic {
		return nil, errors.New("dependency graph is has cycles ")
	}
	err = manipulateArchitecturalEntity(microService, f.handler, CreateEntity)
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	common.InfoLogger.Printf("=============== End Creation of ArchitecturalEntity: %s ===============\n", metaData.GetName())
	return microService, nil
}

func (f *Factory) RemoveArchitecturalEntity(metaData entity.MetaDataGetter) (*entity.ArchitecturalEntity, error) {
	common.InfoLogger.Printf("=============== Begin Creation of ArchitecturalEntity: %s ===============\n", metaData.GetName())
	// TODO: Possibility to use TCP to get the configuration from the implementation Registry
	yamlConfig, err := f.registryHandler.CreateConfiguration(metaData)
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	common.InfoLogger.Println("===== Config Created =====")
	var microService *entity.ArchitecturalEntity
	microService, err = yamlConfig.InitializeArchitecturalEntity()
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	microService.InitDependencyGraph(utils.ChildParent)
	common.InfoLogger.Println("===== ArchitecturalEntity Initialized =====")
	microService.CreateDependencies()

	isAcyclic := graph.Acyclic(microService.DependencyGraph)
	if !isAcyclic {
		return nil, errors.New("dependency graph is has cycles ")
	}
	err = manipulateArchitecturalEntity(microService, f.handler, RemoveEntity)
	if err != nil {
		return nil, err
	}

	common.InfoLogger.Printf("=============== End Creation of ArchitecturalEntity: %s ===============\n", metaData.GetName())
	return microService, nil
}

func (f *Factory) CreateConnector(connector *entity.Connector) (*entity.ArchitecturalEntity, error) {
	common.InfoLogger.Printf("=============== Begin Creation of Connector: %s of type %s ===============\n", connector.MetaData.GetName(), connector.GlobalType)

	connectorName := entity.FormatMetaData(connector.MetaData, "", "")
	secretName := entity.FormatMetaData(connector.MetaData, "", "-secret")
	username := randomString(10)
	password := randomString(15)
	port := "5672"

	immutable := true
	secret := &v1.Secret{
		TypeMeta:   metav1.TypeMeta{APIVersion: "v1", Kind: "Secret"},
		ObjectMeta: metav1.ObjectMeta{Name: secretName},
		Immutable:  &immutable,
		Data:       map[string][]byte{},
		StringData: map[string]string{},
		Type:       v1.SecretTypeOpaque,
	}

	secretRef := v1.SecretEnvSource{}
	secretRef.Name = secretName

	switch connector.GlobalType {
	case entity.PubSubConnector:
		yamlConfig, err := f.registryHandler.CreateConfiguration(entity.NewMetaData("rabbitmq-broker-i", "3.9.4", "", "rabbitmq-broker"))
		if err != nil {
			return nil, err
		}
		_, err = yamlConfig.BuildDockerImages(
			map[string]*string{
				"LOGGER_RABBITMQ_USERNAME": &username,
				"LOGGER_RABBITMQ_PASSWORD": &password,
				"LOGGER_RABBITMQ_HOST":     &connectorName,
				"LOGGER_RABBITMQ_PORT":     &port,
			},
		)
		if err != nil {
			return nil, err
		}
		architecturalEntity, err := yamlConfig.InitializeArchitecturalEntity()
		if err != nil {
			return nil, err
		}

		secret.StringData = map[string]string{
			"LOGGER_RABBITMQ_USERNAME": username,
			"LOGGER_RABBITMQ_PASSWORD": password,
			"LOGGER_RABBITMQ_HOST":     connectorName,
			"LOGGER_RABBITMQ_PORT":     port,
		}
		architecturalEntity.Secrets = append(architecturalEntity.Secrets, secret)

		architecturalEntity.Deployments[0].SetName(connectorName)
		architecturalEntity.Deployments[0].Spec.Template.Spec.Containers[0].EnvFrom = append(architecturalEntity.Deployments[0].Spec.Template.Spec.Containers[0].EnvFrom, v1.EnvFromSource{
			SecretRef: &secretRef,
		})
		architecturalEntity.Deployments[0].Spec.Template.Spec.Containers[0].Name = connectorName
		architecturalEntity.Services[0].SetName(connectorName)

		architecturalEntity.InitDependencyGraph(utils.ParentChild)
		architecturalEntity.CreateDependencies()

		isAcyclic := graph.Acyclic(architecturalEntity.DependencyGraph)
		if !isAcyclic {
			return nil, errors.New("dependency graph is has cycles ")
		}
		err = manipulateArchitecturalEntity(architecturalEntity, f.handler, CreateEntity)
		if err != nil {
			return nil, err
		}

		return architecturalEntity, nil
	}
	return nil, nil
}

func (f *Factory) RemoveConnector(connector *entity.Connector) (*entity.ArchitecturalEntity, error) {
	common.InfoLogger.Printf("=============== Begin Removal of Connector: %s of type %s ===============\n", connector.MetaData.GetName(), connector.GlobalType)

	connectorName := entity.FormatMetaData(connector.MetaData, "", "")
	secretName := entity.FormatMetaData(connector.MetaData, "", "-secret")

	immutable := true
	secret := &v1.Secret{
		TypeMeta:   metav1.TypeMeta{APIVersion: "v1", Kind: "Secret"},
		ObjectMeta: metav1.ObjectMeta{Name: secretName},
		Immutable:  &immutable,
		Data:       map[string][]byte{},
		StringData: map[string]string{},
		Type:       v1.SecretTypeOpaque,
	}

	secretRef := v1.SecretEnvSource{}
	secretRef.Name = secretName

	switch connector.GlobalType {
	case entity.PubSubConnector:
		yamlConfig, err := f.registryHandler.CreateConfiguration(entity.NewMetaData("rabbitmq-broker-i", "3.9.4", "", "rabbitmq-broker"))
		if err != nil {
			return nil, err
		}
		if err != nil {
			return nil, err
		}
		architecturalEntity, err := yamlConfig.InitializeArchitecturalEntity()
		if err != nil {
			return nil, err
		}
		architecturalEntity.Secrets = append(architecturalEntity.Secrets, secret)

		architecturalEntity.Deployments[0].SetName(connectorName)
		architecturalEntity.Deployments[0].Spec.Template.Spec.Containers[0].EnvFrom = append(architecturalEntity.Deployments[0].Spec.Template.Spec.Containers[0].EnvFrom, v1.EnvFromSource{
			SecretRef: &secretRef,
		})
		architecturalEntity.Deployments[0].Spec.Template.Spec.Containers[0].Name = connectorName
		architecturalEntity.Services[0].SetName(connectorName)

		architecturalEntity.InitDependencyGraph(utils.ChildParent)
		architecturalEntity.CreateDependencies()

		isAcyclic := graph.Acyclic(architecturalEntity.DependencyGraph)
		if !isAcyclic {
			return nil, errors.New("dependency graph is has cycles ")
		}
		err = manipulateArchitecturalEntity(architecturalEntity, f.handler, RemoveEntity)
		if err != nil {
			return nil, err
		}

		return architecturalEntity, nil
	}
	return nil, nil
}

func randomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}
