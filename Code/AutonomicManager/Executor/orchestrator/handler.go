/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package orchestrator

import (
	"context"
	"errors"
	"flag"
	"os"

	"tsp.eu/executor/common"
	"tsp.eu/executor/concurrency"
	"tsp.eu/executor/entity"

	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	v12 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

const VOLUMETIMEOUT = time.Minute * 2
const VOLUMECLAIMTIMEOUT = time.Minute
const SERVICETIMEOUT = time.Minute
const SECRETTIMEOUT = time.Minute
const DEPLOYMENTTIMEOUT = time.Minute * 5

const DEFAULTNAMESPACE = "default"

type Operation string

const (
	CreateEntity Operation = "Create"
	RemoveEntity Operation = "Remove"
)

type Manager struct {
	api kubernetes.Interface
}

//go:generate mockgen -destination=mocks/mock_actions.go -package=mocks tsp.eu/executor/orchestrator Handler
type Handler interface {
	CreatePersistentVolume(*v1.PersistentVolume) (*v1.PersistentVolume, error)
	CreatePersistentVolumeClaim(*v1.PersistentVolumeClaim) (*v1.PersistentVolumeClaim, error)
	CreateSecret(*v1.Secret) (*v1.Secret, error)
	CreateDeployment(*v12.Deployment) (*v12.Deployment, error)
	CreateService(*v1.Service) (*v1.Service, error)
	RemovePersistentVolume(*v1.PersistentVolume) (bool, error)
	RemovePersistentVolumeClaim(*v1.PersistentVolumeClaim) (bool, error)
	RemoveSecret(*v1.Secret) (bool, error)
	RemoveDeployment(*v12.Deployment) (bool, error)
	RemoveService(*v1.Service) (bool, error)
	GetConnectorSecrets([]*entity.Connector) ([]*v1.Secret, error)
	InitApi() error
}

func (m *Manager) InitApi() error {
	if m.api != nil {
		return nil
	}

	var kubeConfig *string
	if flagKubeConfig := flag.CommandLine.Lookup("kubeconfig"); flagKubeConfig == nil {
		if envKubeConfig := os.Getenv("KUBECONFIG"); envKubeConfig != "" {
			kubeConfig = flag.String("kubeconfig", envKubeConfig, "")
		} else if home := homedir.HomeDir(); home != "" {
			kubeConfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "")
		} else {
			return errors.New("no home directory or KUBECONFIG environment variable ")
		}
	} else {
		val := flagKubeConfig.Value.String()
		kubeConfig = &val
	}

	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeConfig)
	if err != nil {
		return err
	}
	m.api, err = kubernetes.NewForConfig(config)
	if err != nil {
		return err
	}

	_, err = m.api.(*kubernetes.Clientset).ServerVersion()
	if err != nil {
		return err
	}

	return nil
}

// ################################
// Persistent Volume Handling
// ################################

// CreatePersistentVolume
// This function create a Persistent Volume and wait its complete creation
//
// Define the context with a Timeout: VOLUMETIMEOUT
// Check if the Persistent Volume already exist, if true return
// Create Persistent Volume
// Wait it is fully created
func (m *Manager) CreatePersistentVolume(persistentVolume *v1.PersistentVolume) (*v1.PersistentVolume, error) {
	common.InfoLogger.Printf("**** Create Persistent Volume %s ****\n", persistentVolume.GetName())
	ctx, cancel := context.WithTimeout(context.TODO(), VOLUMETIMEOUT)
	defer cancel()

	_persistentVolume, err := m.api.CoreV1().PersistentVolumes().Get(ctx, persistentVolume.GetName(), metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			common.ErrorLogger.Println(err)
			return nil, err
		}
	} else {
		common.WarningLogger.Printf("** Persistent Volume %s already exists **\n", persistentVolume.GetName())
		return _persistentVolume, nil
	}

	_persistentVolume, err = m.api.CoreV1().PersistentVolumes().Create(ctx, persistentVolume, metav1.CreateOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	for _persistentVolume.Status.Phase != v1.VolumeAvailable && _persistentVolume.Status.Phase != v1.VolumeBound && _persistentVolume.Status.Phase != v1.VolumeReleased {
		_persistentVolume, err = m.api.CoreV1().PersistentVolumes().Get(ctx, _persistentVolume.GetName(), metav1.GetOptions{})
		if err != nil {
			common.ErrorLogger.Println(err)
			return nil, err
		}
		if _persistentVolume.Status.Phase == v1.VolumeFailed {
			return nil, errors.New("volume not available ")
		}
	}

	return _persistentVolume, nil
}

// RemovePersistentVolume
// This function remove a Persistent Volume and wait its complete removal
//
// Define the context with a Timeout: VOLUMETIMEOUT
// Check if the Persistent Volume already exist, if not return
// Check if it is still bound, if true return
// Remove Persistent Volume
// Wait the removal by checking if the Persistent Volume exists
func (m *Manager) RemovePersistentVolume(persistentVolume *v1.PersistentVolume) (bool, error) {
	common.InfoLogger.Printf("**** Remove Persistent Volume %s ****\n", persistentVolume.GetName())
	ctx, cancel := context.WithTimeout(context.TODO(), VOLUMETIMEOUT)
	defer cancel()

	_persistentVolume, err := m.api.CoreV1().PersistentVolumes().Get(ctx, persistentVolume.GetName(), metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			common.WarningLogger.Printf("===== Persistent Volume %s already Removed =====\n", persistentVolume.GetName())
			return false, nil
		}
		common.ErrorLogger.Println(err)
		return false, err
	}

	if _persistentVolume.Status.Phase == v1.VolumeBound || _persistentVolume.Status.Phase == v1.VolumePending {
		common.WarningLogger.Printf("Persistent Volume %s still bound: %s\n", _persistentVolume.Status.Phase, _persistentVolume.Status.Message)
		return false, nil
	}

	err = m.api.CoreV1().PersistentVolumes().Delete(ctx, persistentVolume.GetName(), metav1.DeleteOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return false, err
	}

	common.InfoLogger.Printf("===== Wait Removal of the Persistent Volume %s =====\n", persistentVolume.GetName())
	_, err = m.api.CoreV1().PersistentVolumes().Get(ctx, persistentVolume.GetName(), metav1.GetOptions{})
	for err == nil {
		common.ErrorLogger.Println(err)
		_, err = m.api.CoreV1().PersistentVolumes().Get(ctx, persistentVolume.GetName(), metav1.GetOptions{})
	}
	if k8serrors.IsNotFound(err) {
		common.InfoLogger.Printf("===== Persistent Volume Claim %s completelly Removed =====\n", persistentVolume.GetName())
		return true, nil
	}

	return true, err
}

// ################################
// Persistent Volume Claim Handling
// ################################

// CreatePersistentVolumeClaim
// This function create a Persistent Volume Claim and wait its complete creation
//
// Define the context with a Timeout: VOLUMECLAIMTIMEOUT
// Check if the Persistent Volume Claim already exist, if true return
// Create Persistent Volume Claim
// Then Wait it is fully created
func (m *Manager) CreatePersistentVolumeClaim(persistentVolumeClaim *v1.PersistentVolumeClaim) (*v1.PersistentVolumeClaim, error) {
	common.InfoLogger.Printf("**** Create Persistent Volume Claim %s ****\n", persistentVolumeClaim.GetName())
	ctx, cancel := context.WithTimeout(context.TODO(), VOLUMECLAIMTIMEOUT)
	defer cancel()
	if persistentVolumeClaim.GetNamespace() == "" {
		persistentVolumeClaim.SetNamespace(DEFAULTNAMESPACE)
	}
	_persistentVolumeClaim, err := m.api.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Get(ctx, persistentVolumeClaim.GetName(), metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			common.ErrorLogger.Println(err)
			return nil, err
		}
	} else {
		common.WarningLogger.Printf("** Persistent Volume Claim %s already exists **\n", persistentVolumeClaim.GetName())
		return _persistentVolumeClaim, nil
	}

	_persistentVolumeClaim, err = m.api.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Create(ctx, persistentVolumeClaim, metav1.CreateOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	common.InfoLogger.Printf("===== Waiting for Claim %s =====\n", _persistentVolumeClaim.GetName())
	for {
		if _persistentVolumeClaim.Status.Phase == v1.ClaimBound {
			common.WarningLogger.Printf("===== Claim %s Bounded =====\n", _persistentVolumeClaim.GetName())
			break
		}
		_persistentVolumeClaim, err = m.api.CoreV1().PersistentVolumeClaims(_persistentVolumeClaim.GetNamespace()).Get(ctx, persistentVolumeClaim.GetName(), metav1.GetOptions{})
		if err != nil {
			common.ErrorLogger.Println(err)
			return nil, err
		}
	}

	return _persistentVolumeClaim, nil
}

// RemovePersistentVolumeClaim
// This function remove a Persistent Volume Claim and wait its complete removal
//
// Define the context with a Timeout: VOLUMECLAIMTIMEOUT
// Check if the Persistent Volume Claim already exist, if not return
// Check if it is already used by a deployment, if true return
// Delete Persistent Volume Claim
// Wait the removal by checking if the Persistent Volume Claim exists
func (m *Manager) RemovePersistentVolumeClaim(persistentVolumeClaim *v1.PersistentVolumeClaim) (bool, error) {
	common.InfoLogger.Printf("**** Remove Persistent Volume Claim %s ****\n", persistentVolumeClaim.GetName())
	ctx, cancel := context.WithTimeout(context.TODO(), VOLUMECLAIMTIMEOUT)
	defer cancel()
	if persistentVolumeClaim.GetNamespace() == "" {
		persistentVolumeClaim.SetNamespace(DEFAULTNAMESPACE)
	}

	_, err := m.api.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Get(ctx, persistentVolumeClaim.GetName(), metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			common.WarningLogger.Printf("** Persistent Volume Claim %s already Removed **\n", persistentVolumeClaim.GetName())
			return false, nil
		}
		common.ErrorLogger.Println(err)
		return false, err
	}

	deployments, err := m.api.AppsV1().Deployments(persistentVolumeClaim.GetNamespace()).List(ctx, metav1.ListOptions{})
	if err != nil {
		common.WarningLogger.Printf("There is no deployment in this namespace: %s\n", err)
	}

	for _, deployment := range deployments.Items {
		for _, volume := range deployment.Spec.Template.Spec.Volumes {
			if volume.PersistentVolumeClaim != nil {
				if volume.PersistentVolumeClaim.ClaimName == persistentVolumeClaim.GetName() {
					common.WarningLogger.Printf("Claim %s still used by the Deployment %s\n", persistentVolumeClaim.GetName(), deployment.GetName())
					return false, nil
				}
			}
		}
	}

	err = m.api.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Delete(ctx, persistentVolumeClaim.GetName(), metav1.DeleteOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return false, err
	}

	common.InfoLogger.Printf("** Wait Removal of the Persistent Volume Claim %s **\n", persistentVolumeClaim.GetName())
	_, err = m.api.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Get(ctx, persistentVolumeClaim.GetName(), metav1.GetOptions{})
	for err == nil {
		_, err = m.api.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Get(ctx, persistentVolumeClaim.GetName(), metav1.GetOptions{})
	}
	if k8serrors.IsNotFound(err) {
		common.InfoLogger.Printf("** Persistent Volume Claim %s completely Removed **\n", persistentVolumeClaim.GetName())
		return true, nil
	}

	common.ErrorLogger.Println(err)
	return true, err
}

// ################
// Secret Handling
// ################

// CreateSecret
// This function create a Secret and wait its complete creation
//
// Define the context with a Timeout: SECRETTIMEOUT
// Check if the Secret already exist, if true create return
// Create Secret
// Wait it is fully created
func (m *Manager) CreateSecret(secret *v1.Secret) (*v1.Secret, error) {
	common.InfoLogger.Printf("**** Create Secret %s ****\n", secret.GetName())
	ctx, cancel := context.WithTimeout(context.TODO(), SECRETTIMEOUT)
	defer cancel()
	if secret.GetNamespace() == "" {
		secret.SetNamespace(DEFAULTNAMESPACE)
	}
	_secret, err := m.api.CoreV1().Secrets(secret.GetNamespace()).Get(ctx, secret.GetName(), metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			common.ErrorLogger.Println(err)
			return nil, err
		}
	} else {
		common.WarningLogger.Printf("** Secret %s already exists **\n", secret.Name)
		return _secret, nil
	}

	_secret, err = m.api.CoreV1().Secrets(secret.GetNamespace()).Create(ctx, secret, metav1.CreateOptions{})
	if err != nil {
		common.ErrorLogger.Println(secret)
		common.ErrorLogger.Println(err)
		return nil, err
	}
	return _secret, nil
}

// RemoveSecret
// This function remove a Secret and wait its complete removal
//
// Define the context with a specific timeout: SECRETTIMEOUT
// Check if the Secret already exist, if not return
// Check if it is already used by a deployment, if true return
// Remove Secret
// Wait the removal by checking if the Secret exists
func (m *Manager) RemoveSecret(secret *v1.Secret) (bool, error) {
	common.InfoLogger.Printf("**** Remove Secret %s ****\n", secret.GetName())
	ctx, cancel := context.WithTimeout(context.TODO(), SECRETTIMEOUT)
	defer cancel()
	if secret.GetNamespace() == "" {
		secret.SetNamespace(DEFAULTNAMESPACE)
	}

	_, err := m.api.CoreV1().Secrets(secret.GetNamespace()).Get(ctx, secret.GetName(), metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			common.WarningLogger.Printf("** Secret %s already Removed **\n", secret.GetName())
			return false, nil
		}
		common.ErrorLogger.Println(err)
		return false, err
	}

	deployments, err := m.api.AppsV1().Deployments(secret.GetNamespace()).List(ctx, metav1.ListOptions{})
	if err != nil {
		common.WarningLogger.Printf("There is no deployment in this namespace: %s\n", err)
	}
	for _, deployment := range deployments.Items {
		for _, container := range deployment.Spec.Template.Spec.Containers {
			for _, envFrom := range container.EnvFrom {
				if envFrom.SecretRef.Name == secret.GetName() {
					common.WarningLogger.Printf("** Secret %s still used by the deployment %s **\n", secret.GetName(), deployment.GetName())
					//TODO: AlreadyUsedError
					return false, nil
				}
			}
		}
	}

	err = m.api.CoreV1().Secrets(secret.GetNamespace()).Delete(ctx, secret.GetName(), metav1.DeleteOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return false, err
	}
	common.InfoLogger.Printf("** Waiting Removal of the Secret %s **\n", secret.GetName())
	_, err = m.api.CoreV1().Secrets(secret.GetNamespace()).Get(ctx, secret.GetName(), metav1.GetOptions{})
	for err == nil {
		_, err = m.api.CoreV1().Secrets(secret.GetNamespace()).Get(ctx, secret.GetName(), metav1.GetOptions{})
	}
	if k8serrors.IsNotFound(err) {
		common.InfoLogger.Printf("** Secret %s completly removed **\n", secret.GetName())
		return true, nil
	}

	return true, err
}

// #######################
// v12.Deployment Handling
// #######################

// CreateDeployment
// This function create a deployment and wait its complete creation
//
// Define the context with a Timeout: DEPLOYMENTTIMEOUT
// Check if the Deployment already exist, if true return
// Create Deployment
// Wait it is fully created
func (m *Manager) CreateDeployment(deployment *v12.Deployment) (*v12.Deployment, error) {
	common.InfoLogger.Printf("**** Create Deployment %s ****\n", deployment.Name)
	ctx, cancel := context.WithTimeout(context.TODO(), DEPLOYMENTTIMEOUT)
	defer cancel()
	if deployment.GetNamespace() == "" {
		deployment.SetNamespace(DEFAULTNAMESPACE)
	}

	// Check the deployment exists
	_deployment, err := m.api.AppsV1().Deployments(deployment.GetNamespace()).Get(ctx, deployment.GetName(), metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			common.ErrorLogger.Println(err)
			return nil, err
		}
	} else {
		common.WarningLogger.Printf("** Deployment %s already exists **\n", deployment.Name)
		return _deployment, nil
	}

	// If deployment depends on a secret, check if it exists
	for _, container := range deployment.Spec.Template.Spec.Containers {
		for _, env := range container.EnvFrom {
			if env.SecretRef == nil {
				continue
			}
			_, err := m.api.CoreV1().Secrets(deployment.GetNamespace()).Get(ctx, env.SecretRef.Name, metav1.GetOptions{})
			if k8serrors.IsNotFound(err) {
				common.ErrorLogger.Println(err)
				return nil, err
			}
		}
	}

	// If deployment depends on a claim, check if it exists
	for _, volume := range deployment.Spec.Template.Spec.Volumes {
		if volume.PersistentVolumeClaim == nil {
			continue
		}
		_, err := m.api.CoreV1().PersistentVolumeClaims(deployment.GetNamespace()).Get(ctx, volume.PersistentVolumeClaim.ClaimName, metav1.GetOptions{})
		if k8serrors.IsNotFound(err) {
			common.ErrorLogger.Println(err)
			return nil, err
		}
	}

	_deployment, err = m.api.AppsV1().Deployments(deployment.GetNamespace()).Create(ctx, deployment, metav1.CreateOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}

	common.InfoLogger.Printf("** Waiting of Deployment %s **\n", _deployment.Name)
	for {
		isDeployed := false
		for _, condition := range _deployment.Status.Conditions {
			if condition.Type == v12.DeploymentAvailable {
				if condition.Status == v1.ConditionTrue {
					isDeployed = true
					break
				}
			}
		}
		if isDeployed {
			common.InfoLogger.Printf("** Deployment %s deployed **\n", _deployment.Name)
			break
		}

		time.Sleep(1000)
		_deployment, err = m.api.AppsV1().Deployments(_deployment.GetNamespace()).Get(ctx, _deployment.GetName(), metav1.GetOptions{})
		if err != nil {
			common.ErrorLogger.Println(err)
			return nil, err
		}
	}
	return _deployment, nil
}

// RemoveDeployment
// This function remove a Deployment and wait its complete removal
//
// Define the context with a Timeout: DEPLOYMENTTIMEOUT
// Check if the Deployment already exist, if not return
// Delete Deployment
// Wait the removal by checking if the Deployment exists
func (m *Manager) RemoveDeployment(deployment *v12.Deployment) (bool, error) {
	common.InfoLogger.Printf("**** Remove Deployment %s ****\n", deployment.GetName())
	ctx, cancel := context.WithTimeout(context.TODO(), DEPLOYMENTTIMEOUT)
	defer cancel()
	if deployment.GetNamespace() == "" {
		deployment.SetNamespace(DEFAULTNAMESPACE)
	}

	_, err := m.api.AppsV1().Deployments(deployment.GetNamespace()).Get(ctx, deployment.GetName(), metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			common.WarningLogger.Printf("** Deployment %s completly removed **\n", deployment.GetName())
			return false, nil
		}
		common.ErrorLogger.Println(err)
		return false, err
	}

	err = m.api.AppsV1().Deployments(deployment.GetNamespace()).Delete(ctx, deployment.GetName(), metav1.DeleteOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return false, err
	}
	common.InfoLogger.Printf("** Wait Removal of the Deployment %s **\n", deployment.GetName())
	_, err = m.api.AppsV1().Deployments(deployment.GetNamespace()).Get(ctx, deployment.GetName(), metav1.GetOptions{})
	for err == nil {
		_, err = m.api.AppsV1().Deployments(deployment.GetNamespace()).Get(ctx, deployment.GetName(), metav1.GetOptions{})
	}
	if k8serrors.IsNotFound(err) {
		common.InfoLogger.Printf("** Deployment %s completly removed **\n", deployment.GetName())
		return true, nil
	}

	common.ErrorLogger.Println(err)
	return true, err
}

// ################
// Service Handling
// ################

// CreateService
// This function create a service and wait its complete creation
// The timeout set for the creation of a Service is defined in the constant: SERVICETIMEOUT
//
// Define the context with a Timeout: SERVICETIMEOUT
// Check if the Service already exist, return if true
// Create Service
// Wait it is fully created
func (m *Manager) CreateService(service *v1.Service) (*v1.Service, error) {
	common.InfoLogger.Printf("**** Create Service %s ****\n", service.Name)
	ctx, cancel := context.WithTimeout(context.TODO(), SERVICETIMEOUT)
	defer cancel()
	if service.GetNamespace() == "" {
		service.SetNamespace(DEFAULTNAMESPACE)
	}
	_service, err := m.api.CoreV1().Services(service.GetNamespace()).Get(ctx, service.GetName(), metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			common.ErrorLogger.Println(err)
			return nil, err
		}
	} else {
		common.WarningLogger.Printf("** Service %s already exists **\n", service.Name)
		return _service, nil
	}

	_service, err = m.api.CoreV1().Services(service.GetNamespace()).Create(ctx, service, metav1.CreateOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return nil, err
	}
	return _service, nil
}

// RemoveService
// This function remove a Service and wait its complete removal
//
// Define the context with a Timeout: SERVICETIMEOUT
// Check if the Service already exist, if not return
// Delete Service
// Wait the removal by checking if the Service exists
func (m *Manager) RemoveService(service *v1.Service) (bool, error) {
	common.InfoLogger.Printf("**** Remove Service %s ****\n", service.GetName())
	ctx, cancel := context.WithTimeout(context.TODO(), SERVICETIMEOUT)
	defer cancel()
	if service.GetNamespace() == "" {
		service.SetNamespace(DEFAULTNAMESPACE)
	}

	_, err := m.api.CoreV1().Services(service.GetNamespace()).Get(ctx, service.GetName(), metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			common.WarningLogger.Printf("** Service %s completly removed **\n", service.GetName())
			return false, nil
		}
		common.ErrorLogger.Println(err)
		return false, err
	}

	err = m.api.CoreV1().Services(service.GetNamespace()).Delete(ctx, service.GetName(), metav1.DeleteOptions{})
	if err != nil {
		common.ErrorLogger.Println(err)
		return false, err
	}
	common.InfoLogger.Printf("** Wait Removal of the Service %s **\n", service.GetName())
	_, err = m.api.CoreV1().Services(service.GetNamespace()).Get(ctx, service.GetName(), metav1.GetOptions{})
	for err == nil {
		_, err = m.api.CoreV1().Services(service.GetNamespace()).Get(ctx, service.GetName(), metav1.GetOptions{})
	}
	if k8serrors.IsNotFound(err) {
		common.InfoLogger.Printf("** Service %s completly removed **\n", service.GetName())
		return true, nil
	}

	common.ErrorLogger.Println(err)
	return true, err
}

func (m *Manager) GetConnectorSecrets(connectors []*entity.Connector) ([]*v1.Secret, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), SECRETTIMEOUT)
	defer cancel()
	var secrets []*v1.Secret
	for _, connector := range connectors {
		secretName := entity.FormatMetaData(connector.MetaData, "", "-secret")
		secret, err := m.api.CoreV1().Secrets(DEFAULTNAMESPACE).Get(ctx, secretName, metav1.GetOptions{})
		if err != nil {
			if k8serrors.IsNotFound(err) {
				common.ErrorLogger.Printf("** Secret with name %s is not found **\n", secretName)
			}
			return nil, err
		}
		secrets = append(secrets, secret)
	}

	return secrets, nil
}

func manipulateArchitecturalEntity(entity *entity.ArchitecturalEntity, handler Handler, operation Operation) error {
	common.InfoLogger.Printf("========== Begin Deployment of ArchitecturalEntity: %s ==========\n", entity.MetaData.GetName())
	actions := map[string]*concurrency.Action{}
	for v := 0; v < entity.DependencyGraph.Order(); v++ {
		if _, contains := entity.DependencyGraph.GetAssociation(v); !contains {
			return errors.New("association does not exist ")
		}
		key, _ := entity.DependencyGraph.GetAssociation(v)
		action, err := instantiateAction(entity, &key, handler, operation)
		if err != nil {
			return err
		}
		actions[key] = action
	}
	var err error
	for v := 0; v < entity.DependencyGraph.Order(); v++ {
		entity.DependencyGraph.Visit(v, func(w int, c int64) (skip bool) {
			if err != nil {
				return
			}
			source, _ := entity.DependencyGraph.GetAssociation(v)
			target, _ := entity.DependencyGraph.GetAssociation(w)
			if action, containsKey := actions[source]; containsKey {
				if dependency, containsKey := actions[target]; containsKey {
					action.ToWait.Add(1)
					dependency.ToSend = append(dependency.ToSend, action.ToWait)
					return
				}
			}
			err = errors.New("there is an action not contained in actions map ")
			return
		})
	}
	if err != nil {
		return err
	}
	err = concurrency.ExecuteActions(actions)
	common.InfoLogger.Printf("========== End Deployment Of ArchitecturalEntity: %s ==========\n", entity.MetaData.GetName())
	return err
}

func instantiateAction(entity *entity.ArchitecturalEntity, typeAction *string, handler Handler, operation Operation) (*concurrency.Action, error) {
	split := strings.Split(*typeAction, "_")
	if len(split) != 2 {
		return nil, errors.New("error in the dependencies ")
	}
	i, err := strconv.ParseInt(split[1], 10, 64)
	if err != nil {
		return nil, err
	}

	toWait := sync.WaitGroup{}
	var toSend []*sync.WaitGroup
	instruction := func(a interface{}) error {
		return nil
	}
	switch split[0] {
	case "pv":
		if int64(len(entity.PersistentVolume)) <= i {
			return nil, errors.New("persistent volume: Out of range")
		}

		switch operation {
		case CreateEntity:
			instruction = func(a interface{}) error {
				_, err := handler.CreatePersistentVolume(entity.PersistentVolume[i])
				return err
			}
		case RemoveEntity:
			instruction = func(a interface{}) error {
				_, err := handler.RemovePersistentVolume(entity.PersistentVolume[i])
				return err
			}
		}
	case "deployment":
		if int64(len(entity.Deployments)) <= i {
			return nil, errors.New("deployment: Out of range")
		}

		switch operation {
		case CreateEntity:
			instruction = func(a interface{}) error {
				_, err := handler.CreateDeployment(entity.Deployments[i])
				return err
			}
		case RemoveEntity:
			instruction = func(a interface{}) error {
				_, err := handler.RemoveDeployment(entity.Deployments[i])
				return err
			}
		}
	case "service":
		if int64(len(entity.Services)) <= i {
			return nil, errors.New("service: Out of range")
		}

		switch operation {
		case CreateEntity:
			instruction = func(a interface{}) error {
				_, err := handler.CreateService(entity.Services[i])
				return err
			}
		case RemoveEntity:
			instruction = func(a interface{}) error {
				_, err := handler.RemoveService(entity.Services[i])
				return err
			}
		}
	case "secret":
		if int64(len(entity.Secrets)) <= i {
			return nil, errors.New("secret: Out of range")
		}

		switch operation {
		case CreateEntity:
			instruction = func(a interface{}) error {
				_, err := handler.CreateSecret(entity.Secrets[i])
				return err
			}
		case RemoveEntity:
			instruction = func(a interface{}) error {
				_, err := handler.RemoveSecret(entity.Secrets[i])
				return err
			}
		}
	case "pvc":
		if int64(len(entity.PersistentVolumeClaim)) <= i {
			return nil, errors.New("persistent volume claim: Out of range")
		}

		switch operation {
		case CreateEntity:
			instruction = func(a interface{}) error {
				_, err := handler.CreatePersistentVolumeClaim(entity.PersistentVolumeClaim[i])
				return err
			}
		case RemoveEntity:
			instruction = func(a interface{}) error {
				_, err := handler.RemovePersistentVolumeClaim(entity.PersistentVolumeClaim[i])
				return err
			}
		}
	}
	return concurrency.NewAction(&instruction, toSend, &toWait, nil), nil
}
