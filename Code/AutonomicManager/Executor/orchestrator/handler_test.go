/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package orchestrator

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	v12 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/client-go/kubernetes/fake"
)

const (
	APIVERSION                     = "v1"
	testNamespace                  = "default"
	testApp                        = "test-app"
	testNameDeploymentClaimStorage = "test-deployment-claim"
	testNameSecret                 = "test-secret"
	testNameDeployment             = "test-deployment"
	testNameService                = "test-service"
	testNamePersistentVolume       = "test-persistentvolume"
	testNamePersistentVolumeClaim  = "test-persistentvolumeclaim"
)

type testCasesList []struct {
	Name       string
	TestAction func(t *testing.T)
}

func run(t *testing.T, testCases testCasesList) {
	for _, testCase := range testCases {
		t.Run(testCase.Name, testCase.TestAction)
	}
}

// #############################################################################
// ######################### Unit test with a fake API #########################
// #############################################################################
// /!\ The fake API given by kubernetes does not handle all the errors that can occur.

// TestManager_Unit
// Test Function that test all the actions that can be performed to the API (the fake API here)
//
// Service: TestManager_Service -> Create/Remove a Service
// Secret: TestManager_Secret -> Create/Remove a Secret
// Deployment: TestManager_Deployment -> Create/Remove a Deployment
// PersistentVolume: TestManager_PersistentVolume -> Create/Remove a PersistentVolume
// PersistentVolumeClaim: TestManager_PersistentVolumeClaim -> Create/Remove a PersistentVolumeClaim
func TestManager_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Service", TestManager_Service_Unit},
		{"Secret", TestManager_Secret_Unit},
		{"Deployment", TestManager_Deployment_Unit},
		{"PersistentVolume", TestManager_PersistentVolume_Unit},
		{"PersistentVolumeClaim", TestManager_PersistentVolumeClaim_Unit},
	}

	run(t, testCases)
}

// ##################
// Service Unit Tests
// ##################

// TestManager_Service_Unit
// Start all the unit tests that occurs on a Service (use a fake API)
func TestManager_Service_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Remove-EmptyManager", TestManager_RemoveService_EmptyManager_Unit},
		{"Remove-Once", TestManager_RemoveService_AlreadyExists_Unit},
		{"Create-Once", TestManager_CreateService_NotExisting_Unit},
		{"Create-Duplicate", TestManager_CreateService_Duplicate_Unit},
	}

	run(t, testCases)
}

// TestManager_RemoveService_EmptyManager_Unit
// Use a fake API filled with only one component
// Remove Service once when no instance exists
func TestManager_RemoveService_EmptyManager_Unit(t *testing.T) {
	deleted, err := testEmptyManager().RemoveService(testService())
	assert.False(t, deleted, "The removal should failed")
	assert.Nil(t, err)
}

// TestManager_RemoveService_AlreadyExists_Unit
// Use a fake API filled with only one component
// Remove Service once when one instance already exists
func TestManager_RemoveService_AlreadyExists_Unit(t *testing.T) {
	manager := testManagerService()

	deleted, err := manager.RemoveService(testService())
	assert.True(t, deleted, "The removal should succeed")
	assert.Nil(t, err)
}

func TestManager_CreateService_NotExisting_Unit(t *testing.T) {
	service, err := testEmptyManager().CreateService(testService())
	assert.Nil(t, err)
	assert.Equal(t, testService(), service)
}

func TestManager_CreateService_Duplicate_Unit(t *testing.T) {
	service, err := testManagerService().CreateService(testService())
	assert.Nil(t, err)
	assert.Equal(t, testService(), service)
}

// #################
// Secret Unit Tests
// #################

// TestManager_Secret_Unit
// Start all the unit tests that occurs on a Secret (use a fake API)
func TestManager_Secret_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Remove-EmptyManager", TestManager_RemoveSecret_EmptyManager_Unit},
		{"Remove-Once", TestManager_RemoveSecret_AlreadyExists_Unit},
		{"Remove-UsedByDeployment", TestManager_RemoveUsedSecret_Unit},
		{"Create-Once", TestManager_CreateSecret_NotExisting_Unit},
		{"Create-Duplicate", TestManager_CreateSecret_Duplicate_Unit},
	}

	run(t, testCases)
}

// TestManager_RemoveSecret_EmptyManager_Unit
// Use a fake API filled with only one component
// Remove Secret once when no instance exists
func TestManager_RemoveSecret_EmptyManager_Unit(t *testing.T) {
	deleted, err := testEmptyManager().RemoveSecret(testSecret())
	assert.False(t, deleted, "The removal should failed")
	assert.Nil(t, err)
}

// TestManager_RemoveSecret_AlreadyExists_Unit
// Use a fake API filled with only one component
// Remove Secret once when one instance already exists
func TestManager_RemoveSecret_AlreadyExists_Unit(t *testing.T) {
	manager := testManagerSecret()

	deleted, err := manager.RemoveSecret(testSecret())
	assert.True(t, deleted, "The removal should succeed")
	assert.Nil(t, err)
}

// TestManager_RemoveUsedSecret_Unit
// Check that a secret is not deleted when still used by a deployment
func TestManager_RemoveUsedSecret_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testDeploymentUsingTestSecret(), testSecret()),
	}

	deleted, err := manager.RemoveSecret(testSecret())
	assert.False(t, deleted, "Secret still used: should not be deleted")
	assert.Nil(t, err)
}

func TestManager_CreateSecret_NotExisting_Unit(t *testing.T) {
	secret, err := testEmptyManager().CreateSecret(testSecret())
	assert.Nil(t, err)
	assert.Equal(t, testSecret(), secret)
}

func TestManager_CreateSecret_Duplicate_Unit(t *testing.T) {
	secret, err := testManagerSecret().CreateSecret(testSecret())
	assert.Nil(t, err)
	assert.Equal(t, testSecret(), secret)
}

// #####################
// Deployment Unit Tests
// #####################

// TestManager_Deployment_Unit
// Start all the unit tests that occurs on a Deployment (use a fake API)
func TestManager_Deployment_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Remove-EmptyManager", TestManager_RemoveDeployment_EmptyManager_Unit},
		{"Remove-Once", TestManager_RemoveDeployment_AlreadyExists_Unit},
		{"Create-Once-AlwaysAvailable", TestManager_CreateDeployment_Available_Unit},
		// {"Create-Once-AlwaysProgressing", TestManager_CreateDeployment_Progressing_Unit},
		{"Create-Duplicate", TestManager_CreateDeployment_Duplicate_Unit},
	}

	run(t, testCases)
}

// TestManager_RemoveDeployment_EmptyManager_Unit
// Use a fake API filled with only one component
// Remove Deployment once when no instance exists
func TestManager_RemoveDeployment_EmptyManager_Unit(t *testing.T) {
	deleted, err := testEmptyManager().RemoveDeployment(testDeployment())
	assert.False(t, deleted, "The removal should failed")
	assert.Nil(t, err)
}

// TestManager_RemoveDeployment_AlreadyExists_Unit
// Use a fake API filled with only one component
// Remove Deployment once when one instance already exists
func TestManager_RemoveDeployment_AlreadyExists_Unit(t *testing.T) {
	manager := testManagerDeployment()

	deleted, err := manager.RemoveDeployment(testDeployment())
	assert.True(t, deleted, "The removal should succeed")
	assert.Nil(t, err)
}

func TestManager_CreateDeployment_Duplicate_Unit(t *testing.T) {
	deployment, err := testManagerDeployment().CreateDeployment(testDeployment())
	assert.Nil(t, err)
	assert.Equal(t, testDeployment(), deployment)
}

// TestManager_CreateDeployment_Available_Unit
// Create a Deployment that become available
func TestManager_CreateDeployment_Available_Unit(t *testing.T) {
	manager := testEmptyManager()

	deployment, err := manager.CreateDeployment(testDeploymentAvailable())
	assert.Nil(t, err)
	assert.NotEmpty(t, deployment)
}

// TestManager_CreateDeployment_Progressing_Unit
// Create a deployment that's always progressing but never available
// TODO: Use Context and timeout
func TestManager_CreateDeployment_Progressing_Unit(t *testing.T) {
	manager := testEmptyManager()

	deployment, err := manager.CreateDeployment(testDeploymentProgressing())
	assert.Nil(t, err)
	assert.NotEmpty(t, deployment)
}

// ############################
// Persistent Volume Unit Tests
// ############################

// TestManager_PersistentVolume_Unit
// Start all the unit tests that occurs on a PersistentVolume (use a fake API)
func TestManager_PersistentVolume_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Remove-EmptyManager", TestManager_RemovePersistentVolume_EmptyManager_Unit},
		{"Remove-Once", TestManager_RemovePersistentVolume_AlreadyExists_Unit},
		{"Remove-ByStatus", TestManager_RemovePersistentVolumeFromStatus_Unit},
		{"Create-ByStatus", TestManager_CreatePersistentVolumeFromStatus_Unit},
		{"Create-Duplicate", TestManager_CreatePersistentVolume_Duplicate_Unit},
	}

	run(t, testCases)
}

// TestManager_RemovePersistentVolume_EmptyManager_Unit
// Use a fake API filled with only one component
// Remove PersistentVolume once when no instance exists
func TestManager_RemovePersistentVolume_EmptyManager_Unit(t *testing.T) {
	deleted, err := testEmptyManager().RemovePersistentVolume(testPersistentVolume())
	assert.False(t, deleted, "The removal should failed")
	assert.Nil(t, err)
}

// TestManager_RemovePersistentVolume_AlreadyExists_Unit
// Use a fake API filled with only one component
// Remove PersistentVolume once when one instance already exists
func TestManager_RemovePersistentVolume_AlreadyExists_Unit(t *testing.T) {
	manager := testManagerPersistentVolume()

	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())
	assert.True(t, deleted, "The removal should succeed")
	assert.Nil(t, err)
}

// TestManager_CreatePersistentVolumeFromStatus_Unit
// Test all the status of a persistent volume when creating it
func TestManager_CreatePersistentVolumeFromStatus_Unit(t *testing.T) {
	t.Parallel()
	testCases := testCasesList{
		{"Available", TestManager_CreateAvailablePersistentVolume_Unit},
		{"Released", TestManager_CreateReleasedPersistentVolume_Unit},
		// {"Pending", TestManager_CreatePendingPersistentVolume_Unit},
		{"Bound", TestManager_CreateBoundPersistentVolume_Unit},
		{"Failed", TestManager_CreateFailedPersistentVolume_Unit},
	}

	run(t, testCases)
}

// TestManager_RemovePersistentVolumeFromStatus_Unit
// Test all the status of a persistent volume when removing it
func TestManager_RemovePersistentVolumeFromStatus_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Available", TestManager_RemoveAvailablePersistentVolume_Unit},
		{"Released", TestManager_RemoveReleasedPersistentVolume_Unit},
		{"Pending", TestManager_RemovePendingPersistentVolume_Unit},
		{"Failed", TestManager_RemoveFailedPersistentVolume_Unit},
		{"Bound", TestManager_RemoveBoundPersistentVolume_Unit},
	}

	run(t, testCases)
}

// TestManager_RemoveAvailablePersistentVolume_Unit
// Check that an available persistent volume is not deleted
func TestManager_RemoveAvailablePersistentVolume_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testAvailablePersistentVolume()),
	}
	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())

	assert.True(t, deleted, "Persistent volume should be deleted")
	assert.Nil(t, err, "There should be no errors")
}

// TestManager_RemoveReleasedPersistentVolume_Unit
// Check that an available persistent volume is not deleted
func TestManager_RemoveReleasedPersistentVolume_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testReleasedPersistentVolume()),
	}
	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())

	assert.True(t, deleted, "Persistent volume should be deleted")
	assert.Nil(t, err, "There should be no errors")
}

// TestManager_RemoveBoundPersistentVolume_Unit
// Check that a bound persistent volume is not deleted
func TestManager_RemoveBoundPersistentVolume_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testBoundPersistentVolume()),
	}
	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())

	assert.False(t, deleted, "Persistent volume should not be deleted")
	assert.Nil(t, err, "There should be no errors")
}

// TestManager_RemoveFailedPersistentVolume_Unit
// Check that a failed persistent volume is deleted
func TestManager_RemoveFailedPersistentVolume_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testFailedPersistentVolume()),
	}
	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())

	assert.True(t, deleted, "Persistent volume should be deleted")
	assert.Nil(t, err, "There should be no errors")
}

// TestManager_RemovePendingPersistentVolume_Unit
// Check that a failed persistent volume is deleted
func TestManager_RemovePendingPersistentVolume_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testPendingPersistentVolume()),
	}
	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())

	assert.False(t, deleted, "Persistent volume should not be deleted")
	assert.Nil(t, err, "There should be no errors")
}

// TestManager_CreateAvailablePersistentVolume_Unit
// Create a persistent volume that's always Available
func TestManager_CreateAvailablePersistentVolume_Unit(t *testing.T) {
	manager := testEmptyManager()
	pv, err := manager.CreatePersistentVolume(testAvailablePersistentVolume())

	assert.Equal(t, testAvailablePersistentVolume(), pv)
	assert.Nil(t, err, "There should be no errors")
}

// TestManager_CreateFailedPersistentVolume_Unit
// Create a persistent volume that always fails
func TestManager_CreateFailedPersistentVolume_Unit(t *testing.T) {
	manager := testEmptyManager()
	pv, err := manager.CreatePersistentVolume(testFailedPersistentVolume())

	assert.Nil(t, pv)
	assert.Error(t, err, "There should be an error")
}

// TestManager_CreatePendingPersistentVolume_Unit
// Create a persistent volume that's always Pending
func TestManager_CreatePendingPersistentVolume_Unit(t *testing.T) {
	manager := testEmptyManager()
	pv, err := manager.CreatePersistentVolume(testPendingPersistentVolume())

	assert.Nil(t, pv)
	assert.Error(t, err)
}

// TestManager_CreatePendingPersistentVolume_Unit
// Create a persistent volume that's always Pending
func TestManager_CreateReleasedPersistentVolume_Unit(t *testing.T) {
	manager := testEmptyManager()
	pv, err := manager.CreatePersistentVolume(testReleasedPersistentVolume())

	assert.Equal(t, testReleasedPersistentVolume(), pv)
	assert.Nil(t, err, "There should be no error")
}

// TestManager_CreateBoundPersistentVolume_Unit
// Create a persistent volume that's always Bond
func TestManager_CreateBoundPersistentVolume_Unit(t *testing.T) {
	manager := testEmptyManager()
	pv, err := manager.CreatePersistentVolume(testBoundPersistentVolume())

	assert.Equal(t, testBoundPersistentVolume(), pv)
	assert.Nil(t, err, "There should be no error")
}

func TestManager_CreatePersistentVolume_Duplicate_Unit(t *testing.T) {
	pv, err := testManagerPersistentVolume().CreatePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.Equal(t, testPersistentVolume(), pv)
}

// ##################################
// Persistent Volume Claim Unit Tests
// ##################################

// TestManager_PersistentVolumeClaim_Unit
// Start all the unit tests that occurs on a PersistentVolumeClaim (use a fake API)
func TestManager_PersistentVolumeClaim_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Remove-EmptyManager", TestManager_RemovePersistentVolumeClaim_EmptyManager_Unit},
		{"Remove-Once", TestManager_RemovePersistentVolumeClaim_AlreadyExists_Unit},
		{"Remove-UsedByDeployment", TestManager_RemoveUsedPersistentVolumeClaim_Unit},
		{"Remove-ByStatus", TestManager_RemovePersistentVolumeClaimFromStatus_Unit},
		{"Create-Duplicate", TestManager_CreatePersistentVolumeClaim_Duplicate_Unit},
	}

	run(t, testCases)
}

// TestManager_RemovePersistentVolumeClaim_EmptyManager_Unit
// Use a fake API filled with only one component
// Remove PersistentVolumeClaim once when no instance exists
func TestManager_RemovePersistentVolumeClaim_EmptyManager_Unit(t *testing.T) {
	deleted, err := testEmptyManager().RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.False(t, deleted, "The removal should failed")
	assert.Nil(t, err)
}

// TestManager_RemovePersistentVolumeClaim_AlreadyExists_Unit
// Use a fake API filled with only one component
// Remove PersistentVolumeClaim once when one instance already exists
func TestManager_RemovePersistentVolumeClaim_AlreadyExists_Unit(t *testing.T) {
	manager := testManagerPersistentVolumeClaim()

	deleted, err := manager.RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.True(t, deleted, "The removal should succeed")
	assert.Nil(t, err)
}

// TestManager_RemoveUsedPersistentVolumeClaim_Unit
// Check that a persistent volume claim is not deleted when still used by a deployment
func TestManager_RemoveUsedPersistentVolumeClaim_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testDeploymentUsingTestClaim(), testPersistentVolumeClaim()),
	}

	deleted, err := manager.RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.False(t, deleted, "Persistent Volume Claim should not be deleted")
	assert.Nil(t, err, "There should be no error")
}

func TestManager_RemovePersistentVolumeClaimFromStatus_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Bound", TestManager_RemovePersistentVolumeClaim_Bound_Unit},
		{"Pending", TestManager_RemovePersistentVolumeClaim_Pending_Unit},
		{"Lost", TestManager_RemovePersistentVolumeClaim_Lost_Unit},
	}

	run(t, testCases)
}

// TestManager_RemovePersistentVolumeClaim_Pending_Unit
// Check the claim should be deleted if in pending state
func TestManager_RemovePersistentVolumeClaim_Pending_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testPendingPersistentVolumeClaim()),
	}

	deleted, err := manager.RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.True(t, deleted, "Persistent Volume Claim should be deleted")
	assert.Nil(t, err)
}

// TestManager_RemovePersistentVolumeClaim_Bound_Unit
// Check the claim should be deleted if in bound state
func TestManager_RemovePersistentVolumeClaim_Bound_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testBoundPersistentVolumeClaim()),
	}

	deleted, err := manager.RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.True(t, deleted, "Persistent Volume Claim should be deleted")
	assert.Nil(t, err)
}

// TestManager_RemovePersistentVolumeClaim_Lost_Unit
// Check the claim should be deleted if in lost state
func TestManager_RemovePersistentVolumeClaim_Lost_Unit(t *testing.T) {
	manager := Manager{
		api: fake.NewSimpleClientset(testLostPersistentVolumeClaim()),
	}

	deleted, err := manager.RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.True(t, deleted, "Persistent Volume Claim should be deleted")
	assert.Nil(t, err)
}

func TestManager_CreatePersistentVolumeClaimFromStatus_Unit(t *testing.T) {
	testCases := testCasesList{
		{"Bound", TestManager_CreateBondPersistentVolumeClaim_Unit},
		// {"Pending", TestManager_CreatePendingPersistentVolumeClaim_Unit},
		// {"Lost", TestManager_CreateLostPersistentVolumeClaim_Unit},
	}

	run(t, testCases)
}

// TestManager_CreatePersistentVolumeClaim_Duplicate_Unit
// Create a persistent volume claim that already exists
func TestManager_CreatePersistentVolumeClaim_Duplicate_Unit(t *testing.T) {
	pvc, err := testManagerPersistentVolumeClaim().CreatePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.Nil(t, err)
	assert.Equal(t, testPersistentVolumeClaim(), pvc)
}

// TestManager_CreateBondPersistentVolumeClaim_Unit
// Create a persistent volume claim that's always bound
func TestManager_CreateBondPersistentVolumeClaim_Unit(t *testing.T) {
	pvc, err := testEmptyManager().CreatePersistentVolumeClaim(testBoundPersistentVolumeClaim())
	assert.Nil(t, err)
	assert.Equal(t, testBoundPersistentVolumeClaim(), pvc)
}

// TestManager_CreateLostPersistentVolumeClaim_Unit
// Create a persistent volume claim that's always lost
func TestManager_CreateLostPersistentVolumeClaim_Unit(t *testing.T) {
	pvc, err := testEmptyManager().CreatePersistentVolumeClaim(testLostPersistentVolumeClaim())
	assert.Error(t, err)
	assert.Nil(t, pvc)
}

// TestManager_CreatePendingPersistentVolumeClaim_Unit
// Create a persistent volume claim that's always pending
func TestManager_CreatePendingPersistentVolumeClaim_Unit(t *testing.T) {
	pvc, err := testEmptyManager().CreatePersistentVolumeClaim(testPendingPersistentVolumeClaim())
	assert.Error(t, err)
	assert.Nil(t, pvc)
}

// #############################################################################
// ##################### Integration test with a fake API ######################
// #############################################################################
// /!\ Using kind (Kubernetes IN Docker) to test the actions with a real API.

// TestManager_Integration
// Test Function that test all the actions that can be performed to the API
//
// Service: TestManager_Service -> Create/Remove a Service
// Secret: TestManager_Secret -> Create/Remove a Secret
// Deployment: TestManager_Deployment -> Create/Remove a Deployment
// PersistentVolume: TestManager_PersistentVolume -> Create/Remove a PersistentVolume
// PersistentVolumeClaim: TestManager_PersistentVolumeClaim -> Create/Remove a PersistentVolumeClaim
func TestManager_Integration(t *testing.T) {
	testCases := testCasesList{
		{"Secret", TestManager_Secret_Integration},
		{"Service", TestManager_Service_Integration},
		{"Deployment", TestManager_Deployment_Integration},
		{"PersistentVolume", TestManager_PersistentVolume_Integration},
		{"PersistentVolumeClaim", TestManager_PersistentVolumeClaim_Integration},
	}
	run(t, testCases)
}

// ###################################
// Persistent Volume Integration Tests
// ###################################
// TestManager_PersistentVolume_Integration
// All integration tests on a PersistentVolume
func TestManager_PersistentVolume_Integration(t *testing.T) {
	testCases := testCasesList{
		{"Create-Once", TestManager_CreatePersistentVolume_NotExisting_Integration},
		{"Create-Twice", TestManager_CreatePersistentVolume_AlreadyExists_Integration},
		{"Remove-NotExisting", TestManager_RemovePersistentVolume_NotExisting_Integration},
		{"Remove-Once", TestManager_RemovePersistentVolume_AlreadyExists_Integration},
		{"Remove-UsedByClaim", TestManager_RemovePersistentVolume_Used_Integration},
	}
	run(t, testCases)
}

// TestManager_CreatePersistentVolume_NotExisting_Integration
// Create a Persistent Volume when it does not exist
func TestManager_CreatePersistentVolume_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	checkCorrectCreation(t, err, &testPersistentVolume().ObjectMeta, &pv.ObjectMeta)

	resetAPI(manager)
}

// TestManager_CreatePersistentVolume_AlreadyExists_Integration
// Create a Persistent Volume when it already exists
func TestManager_CreatePersistentVolume_AlreadyExists_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	checkCorrectCreation(t, err, &testPersistentVolume().ObjectMeta, &pv.ObjectMeta)
	pv, err = manager.CreatePersistentVolume(testPersistentVolume())
	checkCorrectCreation(t, err, &testPersistentVolume().ObjectMeta, &pv.ObjectMeta)

	resetAPI(manager)
}

// TestManager_RemovePersistentVolume_NotExisting_Integration
// Remove a Persistent Volume when it does exist
func TestManager_RemovePersistentVolume_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.False(t, deleted)

	resetAPI(manager)
}

// TestManager_RemovePersistentVolume_AlreadyExists_Integration
// Remove a Persistent Volume when it already exists
func TestManager_RemovePersistentVolume_AlreadyExists_Integration(t *testing.T) {
	// Init value in the cluster
	manager := testManagerRealAPI()
	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.NotEmpty(t, pv)

	// Real test
	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.True(t, deleted)
}

// TestManager_RemovePersistentVolume_Used_Integration
// Remove a Persistent Volume when it is still used by a Persistent Volume Claim
func TestManager_RemovePersistentVolume_Used_Integration(t *testing.T) {
	// Init value in the cluster
	manager := testManagerRealAPI()
	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.NotEmpty(t, pv)
	pvc, err := manager.CreatePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.Nil(t, err)
	assert.NotEmpty(t, pvc)

	// Real test
	deleted, err := manager.RemovePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.False(t, deleted)
}

// #########################################
// Persistent Volume Claim Integration Tests
// #########################################

func TestManager_PersistentVolumeClaim_Integration(t *testing.T) {
	testCases := testCasesList{
		{"Create-Once", TestManager_CreatePersistentVolumeClaim_NotExisting_Integration},
		{"Create-Twice", TestManager_CreatePersistentVolumeClaim_AlreadyExists_Integration},
		{"Create-NoPersistentVolumeAvailable", TestManager_CreatePersistentVolumeClaim_NoPersistentVolumeAvailable_Integration},
		{"Remove-NotExisting", TestManager_RemovePersistentVolumeClaim_NotExisting_Integration},
		{"Remove-Once", TestManager_RemovePersistentVolumeClaim_AlreadyExists_Integration},
		{"Remove-UsedByDeployment", TestManager_RemovePersistentVolumeClaim_Used_Integration},
	}
	run(t, testCases)
}

// TestManager_CreatePersistentVolumeClaim_NotExisting_Integration
// Create a Persistent Volume Claim when it does not exist
func TestManager_CreatePersistentVolumeClaim_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()
	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.NotEmpty(t, pv)

	pvc, err := manager.CreatePersistentVolumeClaim(testPersistentVolumeClaim())
	checkCorrectCreation(t, err, &testPersistentVolumeClaim().ObjectMeta, &pvc.ObjectMeta)

	resetAPI(manager)
}

// TestManager_CreatePersistentVolumeClaim_AlreadyExists_Integration
// Create a Persistent Volume Claim when it already exists
func TestManager_CreatePersistentVolumeClaim_AlreadyExists_Integration(t *testing.T) {
	manager := testManagerRealAPI()
	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.NotEmpty(t, pv)

	pvc, err := manager.CreatePersistentVolumeClaim(testPersistentVolumeClaim())
	checkCorrectCreation(t, err, &testPersistentVolumeClaim().ObjectMeta, &pvc.ObjectMeta)
	pvc, err = manager.CreatePersistentVolumeClaim(testPersistentVolumeClaim())
	checkCorrectCreation(t, err, &testPersistentVolumeClaim().ObjectMeta, &pvc.ObjectMeta)

	resetAPI(manager)
}

// TestManager_CreatePersistentVolumeClaim_NoPersistentVolumeAvailable_Integration
// Create a Persistent Volume Claim with no Persistent Volume available
func TestManager_CreatePersistentVolumeClaim_NoPersistentVolumeAvailable_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deployment, err := manager.CreatePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.Error(t, err)
	assert.Nil(t, deployment)
}

// TestManager_RemovePersistentVolumeClaim_NotExisting_Integration
// Remove a Persistent Volume Claim when it does not exist
func TestManager_RemovePersistentVolumeClaim_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()
	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.NotEmpty(t, pv)

	deleted, err := manager.RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.Nil(t, err)
	assert.False(t, deleted)

	resetAPI(manager)
}

// TestManager_RemovePersistentVolumeClaim_AlreadyExists_Integration
// Remove a Persistent Volume Claim when it already exists
func TestManager_RemovePersistentVolumeClaim_AlreadyExists_Integration(t *testing.T) {
	// Init value in the cluster
	manager := testManagerRealAPI()
	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.NotEmpty(t, pv)
	pvc, err := manager.CreatePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.Nil(t, err)
	assert.NotEmpty(t, pvc)

	// Real test
	deleted, err := manager.RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.Nil(t, err)
	assert.True(t, deleted)
}

// TestManager_RemovePersistentVolumeClaim_Used_Integration
// Remove a Persistent Volume Claim when it is still used by a Deployment
func TestManager_RemovePersistentVolumeClaim_Used_Integration(t *testing.T) {
	// Init value in the cluster
	manager := testManagerRealAPI()
	pv, err := manager.CreatePersistentVolume(testPersistentVolume())
	assert.Nil(t, err)
	assert.NotEmpty(t, pv)
	pvc, err := manager.CreatePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.Nil(t, err)
	assert.NotEmpty(t, pvc)
	deployment, err := manager.CreateDeployment(testDeploymentUsingTestClaim())
	assert.Nil(t, err)
	assert.NotEmpty(t, deployment)

	// Real test
	deleted, err := manager.RemovePersistentVolumeClaim(testPersistentVolumeClaim())
	assert.Nil(t, err)
	assert.False(t, deleted)

	resetAPI(manager)
}

// ########################
// Secret Integration Tests
// ########################
// TestManager_Secret_Integration
// All integration tests on a Secret
func TestManager_Secret_Integration(t *testing.T) {
	testCases := testCasesList{
		{"Create-Once", TestManager_CreateSecret_NotExisting_Integration},
		{"Create-Twice", TestManager_CreateSecret_AlreadyExists_Integration},
		{"Remove-NotExisting", TestManager_RemoveSecret_NotExisting_Integration},
		{"Remove-Once", TestManager_RemoveSecret_AlreadyExists_Integration},
		{"Remove-UsedByDeployment", TestManager_RemoveSecret_Used_Integration},
	}
	run(t, testCases)
}

// TestManager_CreateSecret_NotExisting_Integration
// Create a Secret when it does not exist
func TestManager_CreateSecret_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	secret, err := manager.CreateSecret(testSecret())
	checkCorrectCreation(t, err, &testSecret().ObjectMeta, &secret.ObjectMeta)

	resetAPI(manager)
}

// TestManager_CreateSecret_AlreadyExists_Integration
// Create a Secret when it already exists
func TestManager_CreateSecret_AlreadyExists_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	secret, err := manager.CreateSecret(testSecret())
	checkCorrectCreation(t, err, &testSecret().ObjectMeta, &secret.ObjectMeta)
	secret, err = manager.CreateSecret(testSecret())
	checkCorrectCreation(t, err, &testSecret().ObjectMeta, &secret.ObjectMeta)

	resetAPI(manager)
}

// TestManager_RemoveSecret_NotExisting_Integration
// Remove a Secret when it does not exist
func TestManager_RemoveSecret_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deleted, err := manager.RemoveSecret(testSecret())
	assert.Nil(t, err)
	assert.False(t, deleted)

	resetAPI(manager)
}

// TestManager_RemoveSecret_AlreadyExists_Integration
// Remove a Secret when it already exists
func TestManager_RemoveSecret_AlreadyExists_Integration(t *testing.T) {
	// Init value in the cluster
	manager := testManagerRealAPI()
	secret, err := manager.CreateSecret(testSecret())
	assert.Nil(t, err)
	assert.NotEmpty(t, secret)

	// Real test
	deleted, err := manager.RemoveSecret(testSecret())
	assert.Nil(t, err)
	assert.True(t, deleted)
}

// TestManager_RemoveSecret_Used_Integration
// Remove a Secret when it is still used by a Deployment
func TestManager_RemoveSecret_Used_Integration(t *testing.T) {
	// Init value in the cluster
	manager := testManagerRealAPI()
	secret, err := manager.CreateSecret(testSecret())
	assert.Nil(t, err)
	assert.NotEmpty(t, secret)
	deployment, err := manager.CreateDeployment(testDeploymentUsingTestSecret())
	assert.Nil(t, err)
	assert.NotEmpty(t, deployment)

	// Real test
	deleted, err := manager.RemoveSecret(testSecret())
	assert.Nil(t, err)
	assert.False(t, deleted)

	resetAPI(manager)
}

// ############################
// Deployment Integration Tests
// ############################
// TestManager_Deployment_Integration
// All integration tests on a Deployment
func TestManager_Deployment_Integration(t *testing.T) {
	testCases := testCasesList{
		{"Create-Once", TestManager_CreateDeployment_NotExisting_Integration},
		{"Create-Twice", TestManager_CreateDeployment_AlreadyExists_Integration},
		{"Create-SecretDependency", TestManager_CreateDeployment_SecretDependency_Integration},
		{"Create-ClaimDependency", TestManager_CreateDeployment_ClaimDependency_Integration},
		{"Remove-NotExisting", TestManager_RemoveDeployment_NotExisting_Integration},
		{"Remove-Once", TestManager_RemoveDeployment_AlreadyExists_Integration},
	}
	run(t, testCases)
}

// TestManager_CreateDeployment_NotExisting_Integration
// Create a Deployment when it does not exist
func TestManager_CreateDeployment_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deployment, err := manager.CreateDeployment(testDeployment())
	checkCorrectCreation(t, err, &testDeployment().ObjectMeta, &deployment.ObjectMeta)

	resetAPI(manager)
}

// TestManager_CreateDeployment_AlreadyExists_Integration
// Create a Deployment when it already exists
func TestManager_CreateDeployment_AlreadyExists_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deployment, err := manager.CreateDeployment(testDeployment())
	checkCorrectCreation(t, err, &testDeployment().ObjectMeta, &deployment.ObjectMeta)
	deployment, err = manager.CreateDeployment(testDeployment())
	checkCorrectCreation(t, err, &testDeployment().ObjectMeta, &deployment.ObjectMeta)

	resetAPI(manager)
}

// TestManager_CreateDeployment_SecretDependency_Integration
// Create a Deployment that depends on a Secret which does not exist
func TestManager_CreateDeployment_SecretDependency_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deployment, err := manager.CreateDeployment(testDeploymentUsingTestSecret())
	assert.Error(t, err)
	assert.Nil(t, deployment)
}

// TestManager_CreateDeployment_ClaimDependency_Integration
// Create a Deployment that depends on a Persistent Volume Claim which does not exist
func TestManager_CreateDeployment_ClaimDependency_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deployment, err := manager.CreateDeployment(testDeploymentUsingTestClaim())
	assert.Error(t, err)
	assert.Nil(t, deployment)
}

// TestManager_RemoveDeployment_NotExisting_Integration
// Remove a Deployment when it does not exist
func TestManager_RemoveDeployment_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deleted, err := manager.RemoveDeployment(testDeployment())
	assert.Nil(t, err)
	assert.False(t, deleted)

	resetAPI(manager)
}

// TestManager_RemoveDeployment_AlreadyExists_Integration
// Remove a Deployment when it already exists
func TestManager_RemoveDeployment_AlreadyExists_Integration(t *testing.T) {
	// Init value in the cluster
	manager := testManagerRealAPI()
	deployment, err := manager.CreateDeployment(testDeployment())
	assert.Nil(t, err)
	assert.NotEmpty(t, deployment)

	// Real test
	deleted, err := manager.RemoveDeployment(testDeployment())
	assert.Nil(t, err)
	assert.True(t, deleted)
}

// #########################
// Service Integration Tests
// #########################
// TestManager_Service_Integration
// All integration tests on a Service
func TestManager_Service_Integration(t *testing.T) {
	testCases := testCasesList{
		{"Create-Once", TestManager_CreateService_NotExisting_Integration},
		{"Create-Twice", TestManager_CreateService_AlreadyExists_Integration},
		{"Remove-NotExisting", TestManager_RemoveService_NotExisting_Integration},
		{"Remove-Once", TestManager_RemoveService_AlreadyExists_Integration},
	}

	run(t, testCases)
}

// TestManager_CreateService_NotExisting_Integration
// Create a Service when it does not exist
func TestManager_CreateService_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	service, err := manager.CreateService(testService())
	checkCorrectCreation(t, err, &testService().ObjectMeta, &service.ObjectMeta)

	resetAPI(manager)
}

// TestManager_CreateService_AlreadyExists_Integration
// Create a Service when it already exists
func TestManager_CreateService_AlreadyExists_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	service, err := manager.CreateService(testService())
	checkCorrectCreation(t, err, &testService().ObjectMeta, &service.ObjectMeta)
	service, err = manager.CreateService(testService())
	checkCorrectCreation(t, err, &testService().ObjectMeta, &service.ObjectMeta)

	resetAPI(manager)
}

// TestManager_RemoveService_NotExisting_Integration
// Remove a Service when it does not exist
func TestManager_RemoveService_NotExisting_Integration(t *testing.T) {
	manager := testManagerRealAPI()

	deleted, err := manager.RemoveService(testService())
	assert.Nil(t, err)
	assert.False(t, deleted)

	resetAPI(manager)
}

// TestManager_RemoveService_AlreadyExists_Integration
// Remove a Service when it already exists
func TestManager_RemoveService_AlreadyExists_Integration(t *testing.T) {
	// Init value in the cluster
	manager := testManagerRealAPI()
	secret, err := manager.CreateService(testService())
	assert.Nil(t, err)
	assert.NotEmpty(t, secret)

	// Real test
	deleted, err := manager.RemoveService(testService())
	checkCorrectRemoval(t, err, deleted)
}

func checkCorrectCreation(t *testing.T, err error, expected *metav1.ObjectMeta, obtained *metav1.ObjectMeta) {
	assert.Nil(t, err)
	assert.Equal(t, expected.GetName(), obtained.GetName())
	assert.Equal(t, expected.GetNamespace(), obtained.GetNamespace())
}

func checkCorrectRemoval(t *testing.T, err error, deleted bool) {
	assert.Nil(t, err)
	assert.True(t, deleted, "object should be removed")
}

// #############################################################################
// ########################## Templates for the tests ##########################
// #############################################################################

// ####################
// Service test objects
// ####################

func testServiceNoNamespace() *v1.Service {
	return &v1.Service{
		TypeMeta: metav1.TypeMeta{
			APIVersion: APIVERSION,
			Kind:       "Service",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: testNameService,
		},
		Spec: v1.ServiceSpec{
			Ports: []v1.ServicePort{
				{
					Name:       "http",
					Port:       8080,
					TargetPort: intstr.FromInt(8080),
				},
			},
		},
	}
}

func testService() *v1.Service {
	service := testServiceNoNamespace()
	service.SetNamespace(testNamespace)
	return service
}

// #######################
// Deployment test objects
// #######################

func testDeploymentNoNamespace() *v12.Deployment {
	return &v12.Deployment{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Deployment",
			APIVersion: APIVERSION,
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:   testNameDeployment,
			Labels: map[string]string{"app": testApp},
		},
		Spec: v12.DeploymentSpec{
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"app": testApp},
			},
			Template: v1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:      testNameDeployment,
					Namespace: testNameDeployment,
					Labels:    map[string]string{"app": testApp},
				},
				Spec: v1.PodSpec{
					Containers: []v1.Container{
						{
							Name:  "nginx",
							Image: "nginx",
						},
					},
				},
			},
		},
	}
}

func testDeployment() *v12.Deployment {
	deployment := testDeploymentNoNamespace()
	deployment.SetNamespace(testNamespace)
	return deployment
}

func testDeploymentProgressing() *v12.Deployment {
	deployment := testDeploymentNoNamespace()
	deployment.SetNamespace(testNamespace)

	deployment.Status.Conditions = []v12.DeploymentCondition{
		{
			Type:   v12.DeploymentProgressing,
			Status: v1.ConditionTrue,
		},
		{
			Type:   v12.DeploymentAvailable,
			Status: v1.ConditionFalse,
		},
	}

	return deployment
}

func testDeploymentAvailable() *v12.Deployment {
	deployment := testDeploymentNoNamespace()
	deployment.SetNamespace(testNamespace)

	deployment.Status.Conditions = []v12.DeploymentCondition{
		{
			Type:   v12.DeploymentProgressing,
			Status: v1.ConditionTrue,
		},
		{
			Type:   v12.DeploymentAvailable,
			Status: v1.ConditionTrue,
		},
	}

	return deployment
}

func testDeploymentUsingTestClaim() *v12.Deployment {
	deployment := testDeployment()
	deployment.Spec.Template.Spec.Volumes = []v1.Volume{
		{
			Name: testNameDeploymentClaimStorage,
			VolumeSource: v1.VolumeSource{
				PersistentVolumeClaim: &v1.PersistentVolumeClaimVolumeSource{
					ClaimName: testNamePersistentVolumeClaim,
					ReadOnly:  false,
				},
			},
		},
	}
	return deployment
}

func testDeploymentUsingTestSecret() *v12.Deployment {
	deployment := testDeployment()
	deployment.Spec.Template.Spec.Containers[0].EnvFrom = []v1.EnvFromSource{
		{
			SecretRef: &v1.SecretEnvSource{
				LocalObjectReference: v1.LocalObjectReference{
					Name: testNameSecret,
				},
			},
		},
	}
	return deployment
}

// ##############################
// Persistent Volume test objects
// ##############################

func testPersistentVolume() *v1.PersistentVolume {
	quantity, err := resource.ParseQuantity("5Gi")
	if err != nil {
		fmt.Println(err)
		return nil
	}

	volumeMode := v1.PersistentVolumeFilesystem

	return &v1.PersistentVolume{
		TypeMeta: metav1.TypeMeta{
			APIVersion: APIVERSION,
			Kind:       "PersistentVolume",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: testNamePersistentVolume,
			Labels: map[string]string{
				"type": "local",
				"app":  testApp,
			},
		},
		Spec: v1.PersistentVolumeSpec{
			Capacity: map[v1.ResourceName]resource.Quantity{
				"storage": quantity,
			},
			PersistentVolumeSource: v1.PersistentVolumeSource{
				HostPath: &v1.HostPathVolumeSource{
					Path: "/tmp/test-pv",
					Type: nil,
				},
			},
			AccessModes:                   []v1.PersistentVolumeAccessMode{v1.ReadWriteMany},
			ClaimRef:                      nil,
			PersistentVolumeReclaimPolicy: v1.PersistentVolumeReclaimRetain,
			StorageClassName:              "manual",
			MountOptions:                  []string{"/tmp/test-pv/"},
			VolumeMode:                    &volumeMode,
			NodeAffinity:                  nil,
		},
	}
}

func testBoundPersistentVolume() *v1.PersistentVolume {
	pv := testPersistentVolume()
	pv.Status = v1.PersistentVolumeStatus{
		Phase: v1.VolumeBound,
	}
	return pv
}

func testAvailablePersistentVolume() *v1.PersistentVolume {
	pv := testPersistentVolume()
	pv.Status = v1.PersistentVolumeStatus{
		Phase: v1.VolumeAvailable,
	}
	return pv
}

func testReleasedPersistentVolume() *v1.PersistentVolume {
	pv := testPersistentVolume()
	pv.Status = v1.PersistentVolumeStatus{
		Phase: v1.VolumeReleased,
	}
	return pv
}

func testPendingPersistentVolume() *v1.PersistentVolume {
	pv := testPersistentVolume()
	pv.Status = v1.PersistentVolumeStatus{
		Phase: v1.VolumePending,
	}
	return pv
}

func testFailedPersistentVolume() *v1.PersistentVolume {
	pv := testPersistentVolume()
	pv.Status = v1.PersistentVolumeStatus{
		Phase: v1.VolumeFailed,
	}
	return pv
}

// ####################################
// Persistent Volume Claim test objects
// ####################################

func testPersistentVolumeClaimNoNamespace() *v1.PersistentVolumeClaim {
	quantity, err := resource.ParseQuantity("5Gi")
	if err != nil {
		fmt.Println(err)
		return nil
	}

	manual := "manual"
	return &v1.PersistentVolumeClaim{
		TypeMeta: metav1.TypeMeta{
			APIVersion: APIVERSION,
			Kind:       "PersistentVolumeClaim",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: testNamePersistentVolumeClaim,
			Labels: map[string]string{
				"type": "local",
				"app":  testApp,
			},
		},
		Spec: v1.PersistentVolumeClaimSpec{
			AccessModes:      []v1.PersistentVolumeAccessMode{v1.ReadWriteMany},
			StorageClassName: &manual,
			Resources: v1.ResourceRequirements{
				Requests: v1.ResourceList{"storage": quantity},
			},
		},
	}
}

func testPersistentVolumeClaim() *v1.PersistentVolumeClaim {
	pvc := testPersistentVolumeClaimNoNamespace()
	pvc.SetNamespace(testNamespace)

	return pvc
}

func testBoundPersistentVolumeClaim() *v1.PersistentVolumeClaim {
	pvc := testPersistentVolumeClaim()
	pvc.Status = v1.PersistentVolumeClaimStatus{Phase: v1.ClaimBound}

	return pvc
}

func testLostPersistentVolumeClaim() *v1.PersistentVolumeClaim {
	pvc := testPersistentVolumeClaim()
	pvc.Status = v1.PersistentVolumeClaimStatus{Phase: v1.ClaimLost}

	return pvc
}

func testPendingPersistentVolumeClaim() *v1.PersistentVolumeClaim {
	pvc := testPersistentVolumeClaim()
	pvc.Status = v1.PersistentVolumeClaimStatus{Phase: v1.ClaimPending}

	return pvc
}

// ###################
// Secret test objects
// ###################

func testSecretNoNamespace() *v1.Secret {
	return &v1.Secret{
		TypeMeta: metav1.TypeMeta{
			APIVersion: APIVERSION,
			Kind:       "Secret",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      testNameSecret,
			Namespace: testNamespace,
		},
	}
}

func testSecret() *v1.Secret {
	secret := testSecretNoNamespace()
	secret.SetNamespace(testNamespace)
	return secret
}

// ####################
// Manager test objects
// ####################

func testEmptyManager() *Manager {
	return &Manager{
		api: fake.NewSimpleClientset(),
	}
}

func testManagerService() *Manager {
	return &Manager{
		api: fake.NewSimpleClientset(testService()),
	}
}

func testManagerSecret() *Manager {
	return &Manager{
		api: fake.NewSimpleClientset(testSecret()),
	}
}

func testManagerDeployment() *Manager {
	return &Manager{
		api: fake.NewSimpleClientset(testDeployment()),
	}
}

func testManagerPersistentVolume() *Manager {
	return &Manager{
		api: fake.NewSimpleClientset(testPersistentVolume()),
	}
}

func testManagerPersistentVolumeClaim() *Manager {
	return &Manager{
		api: fake.NewSimpleClientset(testPersistentVolumeClaim()),
	}
}

func testManagerRealAPI() *Manager {
	homedir, _ := os.UserHomeDir()
	_ = os.Setenv("KUBECONFIG", filepath.Join(homedir, ".kube", "config-test"))

	m := &Manager{}
	_ = m.InitApi()

	resetAPI(m)

	return m
}

func resetAPI(m *Manager) {
	deployments, _ := m.api.AppsV1().Deployments("default").List(context.TODO(), metav1.ListOptions{})
	for _, deployment := range deployments.Items {
		if deployment.GetName() == testNameDeployment {
			_, _ = m.RemoveDeployment(&deployment)
		}
	}

	services, _ := m.api.CoreV1().Services("default").List(context.TODO(), metav1.ListOptions{})
	for _, service := range services.Items {
		if service.GetName() == testNameService {
			_, _ = m.RemoveService(&service)
		}
	}

	pvcs, _ := m.api.CoreV1().PersistentVolumeClaims("default").List(context.TODO(), metav1.ListOptions{})
	for _, pvc := range pvcs.Items {
		if pvc.GetName() == testNamePersistentVolumeClaim {
			_, _ = m.RemovePersistentVolumeClaim(&pvc)
		}
	}

	secrets, _ := m.api.CoreV1().Secrets("default").List(context.TODO(), metav1.ListOptions{})
	for _, secret := range secrets.Items {
		if secret.GetName() == testNameSecret {
			_, _ = m.RemoveSecret(&secret)
		}
	}

	pvs, _ := m.api.CoreV1().PersistentVolumes().List(context.TODO(), metav1.ListOptions{})
	for _, pv := range pvs.Items {
		if pv.GetName() == testNamePersistentVolume {
			_, _ = m.RemovePersistentVolume(&pv)
		}
	}
}
