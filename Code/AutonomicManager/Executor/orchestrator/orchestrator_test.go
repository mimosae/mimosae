/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package orchestrator

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	v12 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"tsp.eu/executor/common"
	"tsp.eu/executor/entity"
//	mockEntity "tsp.eu/executor/entity/mocks"
	"tsp.eu/executor/entity/utils"
	"tsp.eu/executor/implementation_registry"
//	mockRegistry "tsp.eu/executor/implementation_registry/mocks"
//	"tsp.eu/executor/orchestrator/mocks"
)

const (
	testMetadataName    = "test"
	testMetadataVersion = "1_0_0"
	testMetadataType    = "test"
)

func TestFactory_Unit(t *testing.T) {
	t.Run("Create", TestFactory_CreateArchitecturalEntity_Unit)
	t.Run("Remove", TestFactory_RemoveArchitecturalEntity_Unit)
}

func TestFactory_CreateArchitecturalEntity_Unit(t *testing.T) {
	t.Run("EmptyConfig", TestFactory_CreateArchitecturalEntity_Empty_Unit)
	t.Run("NonDependentComponents", TestFactory_CreateArchitecturalEntity_FillNonDependentComponents_Unit)
	t.Run("DependentComponents", TestFactory_CreateArchitecturalEntity_FillDependentComponents_Unit)
	t.Run("Error-Configuration", TestFactory_CreateArchitecturalEntity_ErrorConfiguration_Unit)
	t.Run("Error-BuildDockerImage", TestFactory_CreateArchitecturalEntity_ErrorBuildDockerImage_Unit)
	t.Run("Error-InitializeEntityFromConfig", TestFactory_CreateArchitecturalEntity_ErrorInitEntityFromConfig_Unit)
	t.Run("Error-CreationKubernetesComponent", TestFactory_CreateArchitecturalEntity_ErrorCreationComponent)
}

// TestFactory_CreateArchitecturalEntity_Empty_Unit
// Create an empty architectural entity
func TestFactory_CreateArchitecturalEntity_Empty_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockHandler, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(mockHandler, mockRegistryHandler)

	emptyArchitecturalEntity := emptyArchitecturalEntity()
	mockConfiguration.EXPECT().BuildDockerImages(map[string]*string{}).Return(true, nil).Times(1)
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(emptyArchitecturalEntity, nil).Times(1)
	mockHandler.EXPECT().GetConnectorSecrets(testEmptyConnectList()).Return(testEmptyConnectSecretsList(), nil).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)

	architecturalEntity, err := factory.CreateArchitecturalEntity(testMetaData(), []*entity.Connector{})
	assert.Nil(t, err)
	assert.Equal(t, initGraphArchitecturalEntity(emptyArchitecturalEntity, utils.ParentChild), architecturalEntity)
}

// TestFactory_CreateArchitecturalEntity_FillNonDependentComponents_Unit
// Create an architectural entity that contains a service and a secret
func TestFactory_CreateArchitecturalEntity_FillNonDependentComponents_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockHandler, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(mockHandler, mockRegistryHandler)

	filledArchitecturalEntity := filledArchitecturalEntity(testService(), testSecret())
	mockConfiguration.EXPECT().BuildDockerImages(map[string]*string{}).Return(true, nil).Times(1)
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(filledArchitecturalEntity, nil).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)
	mockHandler.EXPECT().GetConnectorSecrets(testEmptyConnectList()).Return(testEmptyConnectSecretsList(), nil).Times(1)
	mockHandler.EXPECT().CreateService(testService()).Return(testService(), nil).Times(1)
	mockHandler.EXPECT().CreateSecret(testSecret()).Return(testSecret(), nil).Times(1)

	architecturalEntity, err := factory.CreateArchitecturalEntity(testMetaData(), []*entity.Connector{})
	assert.Nil(t, err)
	assert.Equal(t, initGraphArchitecturalEntity(filledArchitecturalEntity, utils.ParentChild), architecturalEntity)
}

// TestFactory_CreateArchitecturalEntity_FillDependentComponents_Unit
// Create an architectural entity that contains a service and a secret that have dependencies
func TestFactory_CreateArchitecturalEntity_FillDependentComponents_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockHandler, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(mockHandler, mockRegistryHandler)

	filledArchitecturalEntity := filledArchitecturalEntity(testDeploymentUsingTestSecret(), testSecret())
	mockConfiguration.EXPECT().BuildDockerImages(map[string]*string{}).Return(true, nil).Times(1)
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(filledArchitecturalEntity, nil).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)
	mockHandler.EXPECT().GetConnectorSecrets(testEmptyConnectList()).Return(testEmptyConnectSecretsList(), nil).Times(1)
	mockHandler.EXPECT().CreateDeployment(testDeploymentUsingTestSecret()).Return(testDeploymentUsingTestSecret(), nil).Times(1)
	mockHandler.EXPECT().CreateSecret(testSecret()).Return(testSecret(), nil).Times(1)

	architecturalEntity, err := factory.CreateArchitecturalEntity(testMetaData(), []*entity.Connector{})
	assert.Nil(t, err)
	assert.Equal(t, initGraphArchitecturalEntity(filledArchitecturalEntity, utils.ParentChild), architecturalEntity)
}

// TestFactory_CreateArchitecturalEntity_ErrorConfiguration_Unit
// Error in the configuration
func TestFactory_CreateArchitecturalEntity_ErrorConfiguration_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	_, mockRegistryHandler, _ := mockHandlers(mockCtrl)
	factory := Factory{registryHandler: mockRegistryHandler}
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(nil, testError()).Times(1)

	architecturalEntity, err := factory.CreateArchitecturalEntity(testMetaData(), []*entity.Connector{})
	assert.Error(t, err)
	assert.Nil(t, architecturalEntity)
}

// TestFactory_CreateArchitecturalEntity_ErrorBuildDockerImage_Unit
// Error when building a docker image from a configuration
func TestFactory_CreateArchitecturalEntity_ErrorBuildDockerImage_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockRegistryHandler := mockRegistry.NewMockRegistryHandler(mockCtrl)
	mockConfiguration := mockEntity.NewMockConfiguration(mockCtrl)
	factory := mockFactory(nil, mockRegistryHandler)

	mockConfiguration.EXPECT().BuildDockerImages(map[string]*string{}).Return(false, testError()).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)

	architecturalEntity, err := factory.CreateArchitecturalEntity(testMetaData(), []*entity.Connector{})
	assert.Error(t, err)
	assert.Nil(t, architecturalEntity)
}

// TestFactory_CreateArchitecturalEntity_ErrorInitEntityFromConfig_Unit
// Error creation of an architectural entity from a configuration
func TestFactory_CreateArchitecturalEntity_ErrorInitEntityFromConfig_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	_, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(nil, mockRegistryHandler)

	mockConfiguration.EXPECT().BuildDockerImages(map[string]*string{}).Return(true, nil).Times(1)
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(nil, testError()).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)

	architecturalEntity, err := factory.CreateArchitecturalEntity(testMetaData(), []*entity.Connector{})
	assert.Error(t, err)
	assert.Nil(t, architecturalEntity)
}

// TestFactory_CreateArchitecturalEntity_ErrorCreationComponent
// Error creation of the kubernetes component of an architectural entity
func TestFactory_CreateArchitecturalEntity_ErrorCreationComponent(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockHandler, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(mockHandler, mockRegistryHandler)

	filledArchitecturalEntity := filledArchitecturalEntity(testSecret())
	mockConfiguration.EXPECT().BuildDockerImages(map[string]*string{}).Return(true, nil).Times(1)
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(filledArchitecturalEntity, nil).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)
	mockHandler.EXPECT().GetConnectorSecrets(testEmptyConnectList()).Return(testEmptyConnectSecretsList(), nil).Times(1)
	mockHandler.EXPECT().CreateSecret(testSecret()).Return(nil, testError()).Times(1)

	architecturalEntity, err := factory.CreateArchitecturalEntity(testMetaData(), []*entity.Connector{})
	assert.Error(t, err)
	assert.Nil(t, architecturalEntity)
}

func TestFactory_RemoveArchitecturalEntity_Unit(t *testing.T) {
	t.Run("EmptyConfig", TestFactory_RemoveArchitecturalEntity_Empty_Unit)
	t.Run("NonDependentComponents", TestFactory_RemoveArchitecturalEntity_FillNonDependentComponents_Unit)
	t.Run("DependentComponents", TestFactory_RemoveArchitecturalEntity_FillDependentComponents_Unit)
	t.Run("Error-Configuration", TestFactory_RemoveArchitecturalEntity_ErrorConfiguration_Unit)
	t.Run("Error-InitializeEntityFromConfig", TestFactory_RemoveArchitecturalEntity_ErrorInitEntityFromConfig_Unit)
	t.Run("Error-RemovalKubernetesComponent", TestFactory_RemoveArchitecturalEntity_ErrorRemovalComponent)
}

// TestFactory_RemoveArchitecturalEntity_Empty_Unit
// Remove an empty architectural entity
func TestFactory_RemoveArchitecturalEntity_Empty_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockHandler, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(mockHandler, mockRegistryHandler)

	emptyArchitecturalEntity := emptyArchitecturalEntity()
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(emptyArchitecturalEntity, nil).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)

	architecturalEntity, err := factory.RemoveArchitecturalEntity(testMetaData())
	assert.Nil(t, err)
	assert.Equal(t, initGraphArchitecturalEntity(emptyArchitecturalEntity, utils.ChildParent), architecturalEntity)
}

// TestFactory_RemoveArchitecturalEntity_FillNonDependentComponents_Unit
// Remove an architectural entity that contains a service and a secret
func TestFactory_RemoveArchitecturalEntity_FillNonDependentComponents_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockHandler, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(mockHandler, mockRegistryHandler)

	filledArchitecturalEntity := filledArchitecturalEntity(testService(), testSecret())
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(filledArchitecturalEntity, nil).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)
	mockHandler.EXPECT().RemoveService(testService()).Return(true, nil).Times(1)
	mockHandler.EXPECT().RemoveSecret(testSecret()).Return(true, nil).Times(1)

	architecturalEntity, err := factory.RemoveArchitecturalEntity(testMetaData())
	assert.Nil(t, err)
	assert.Equal(t, initGraphArchitecturalEntity(filledArchitecturalEntity, utils.ChildParent), architecturalEntity)
}

// TestFactory_RemoveArchitecturalEntity_FillDependentComponents_Unit
// Remove an architectural entity that contains a service and a secret that have dependencies
func TestFactory_RemoveArchitecturalEntity_FillDependentComponents_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockHandler, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(mockHandler, mockRegistryHandler)

	filledArchitecturalEntity := filledArchitecturalEntity(testDeploymentUsingTestSecret(), testSecret())
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(filledArchitecturalEntity, nil).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)
	mockHandler.EXPECT().RemoveDeployment(testDeploymentUsingTestSecret()).Return(true, nil).Times(1)
	mockHandler.EXPECT().RemoveSecret(testSecret()).Return(true, nil).Times(1)

	architecturalEntity, err := factory.RemoveArchitecturalEntity(testMetaData())
	assert.Nil(t, err)
	assert.Equal(t, initGraphArchitecturalEntity(filledArchitecturalEntity, utils.ChildParent), architecturalEntity)
}

// TestFactory_RemoveArchitecturalEntity_ErrorConfiguration_Unit
// Error in the configuration
func TestFactory_RemoveArchitecturalEntity_ErrorConfiguration_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	_, mockRegistryHandler, _ := mockHandlers(mockCtrl)
	factory := mockFactory(nil, mockRegistryHandler)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(nil, testError()).Times(1)

	architecturalEntity, err := factory.RemoveArchitecturalEntity(testMetaData())
	assert.Error(t, err)
	assert.Nil(t, architecturalEntity)
}

// TestFactory_RemoveArchitecturalEntity_ErrorInitEntityFromConfig_Unit
// Error creation of an architectural entity from a configuration
func TestFactory_RemoveArchitecturalEntity_ErrorInitEntityFromConfig_Unit(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	_, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(nil, mockRegistryHandler)
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(nil, testError()).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)

	architecturalEntity, err := factory.RemoveArchitecturalEntity(testMetaData())
	assert.Error(t, err)
	assert.Nil(t, architecturalEntity)
}

// TestFactory_RemoveArchitecturalEntity_ErrorRemovalComponent
// Error removal of the kubernetes component of an architectural entity
func TestFactory_RemoveArchitecturalEntity_ErrorRemovalComponent(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	mockHandler, mockRegistryHandler, mockConfiguration := mockHandlers(mockCtrl)
	factory := mockFactory(mockHandler, mockRegistryHandler)

	filledArchitecturalEntity := filledArchitecturalEntity(testSecret())
	mockConfiguration.EXPECT().InitializeArchitecturalEntity().Return(filledArchitecturalEntity, nil).Times(1)
	mockRegistryHandler.EXPECT().CreateConfiguration(testMetaData()).Return(mockConfiguration, nil).Times(1)
	mockHandler.EXPECT().RemoveSecret(testSecret()).Return(false, testError()).Times(1)

	architecturalEntity, err := factory.RemoveArchitecturalEntity(testMetaData())
	assert.Error(t, err)
	assert.Nil(t, architecturalEntity)
}

func TestFactory_CreateConnector(t *testing.T) {
	implementationRegistry := implementation_registry.ImplementationRegistry{}
	implementationRegistry.InitImplementationRegistry("../../../UseCase_EDF_GDE_Configs")

	factory := NewFactory(&implementationRegistry)

	elem, err := factory.CreateConnector(&entity.Connector{
		GlobalType: entity.PubSubConnector,
		MetaData:   entity.NewMetaData("logger-broker-i", "1.0.0", "", "logger-broker"),
	})
	if err != nil {
		t.Error(err)
	}
	common.InfoLogger.Println(elem)
}

func testMetaData() *entity.MetaData {
	return entity.NewMetaData(testMetadataName, testMetadataVersion, testMetadataType, testNamespace)
}

func emptyArchitecturalEntity() *entity.ArchitecturalEntity {
	return &entity.ArchitecturalEntity{
		MetaData:              testMetaData(),
		Pods:                  []*v1.Pod{},
		Services:              []*v1.Service{},
		Secrets:               []*v1.Secret{},
		Deployments:           []*v12.Deployment{},
		PersistentVolume:      []*v1.PersistentVolume{},
		PersistentVolumeClaim: []*v1.PersistentVolumeClaim{},
	}
}

func initGraphArchitecturalEntity(architecturalEntity *entity.ArchitecturalEntity, orientation utils.Orientation) *entity.ArchitecturalEntity {
	archEntity := architecturalEntity
	archEntity.InitDependencyGraph(orientation)
	return archEntity
}

func filledArchitecturalEntity(a ...interface{}) *entity.ArchitecturalEntity {
	archEntity := emptyArchitecturalEntity()
	for _, component := range a {
		switch elem := component.(type) {
		case *v1.Secret:
			archEntity.Secrets = append(archEntity.Secrets, elem)
		case *v1.Service:
			archEntity.Services = append(archEntity.Services, elem)
		case *v12.Deployment:
			archEntity.Deployments = append(archEntity.Deployments, elem)
		case *v1.PersistentVolume:
			archEntity.PersistentVolume = append(archEntity.PersistentVolume, elem)
		case *v1.PersistentVolumeClaim:
			archEntity.PersistentVolumeClaim = append(archEntity.PersistentVolumeClaim, elem)
		}
	}
	return archEntity
}

func mockFactory(handler Handler, registry implementation_registry.RegistryHandler) *Factory {
	return &Factory{
		handler:         handler,
		registryHandler: registry,
	}
}

func mockHandlers(mockCtrl *gomock.Controller) (*mocks.MockHandler, *mockRegistry.MockRegistryHandler, *mockEntity.MockConfiguration) {
	mockHandler := mocks.NewMockHandler(mockCtrl)
	mockRegistryHandler := mockRegistry.NewMockRegistryHandler(mockCtrl)
	mockConfiguration := mockEntity.NewMockConfiguration(mockCtrl)
	return mockHandler, mockRegistryHandler, mockConfiguration
}

func testError() error {
	return errors.New("Test Error ")
}

func testEmptyConnectList() []*entity.Connector {
	return []*entity.Connector{}
}

func testEmptyConnectSecretsList() []*v1.Secret {
	return []*v1.Secret{}
}
