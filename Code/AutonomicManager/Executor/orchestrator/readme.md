# Module tsp.eu/executor/orchestrator

## Contents

* handler.go: 

* handler_test.go: 

* orchestrator.go:

* orchestrator_test.go:

* go.mod: generated with the following commands:
```
go mod init tsp.eu/executor/orchestrator
go mod edit -replace tsp.eu/executor/common=../common
go mod edit -replace tsp.eu/executor/concurrency=../concurrency
go mod edit -replace tsp.eu/executor/entity=../entity
go mod edit -replace tsp.eu/executor/entity/utils=../entity/utils
go mod edit -replace tsp.eu/executor/implementation_registry=../implementation_registry
go mod tidy
```
