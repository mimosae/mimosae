/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package concurrency

import (
	"fmt"
	"sync"
)

// Action contains an instruction to execute, a waiting group list ToSend to communicate the end of
// instruction's execution and a waiting group ToWait that blocks instruction's execution until
// ToWait counter runs down to zero
type Action struct {
	instruction     *func(interface{}) error
	argument 		interface{}
	ToSend      	[]*sync.WaitGroup
	ToWait 			*sync.WaitGroup
}


// NewAction creates a new action with given instruction and waiting groups
func NewAction(instruction *func(interface{}) error, toSend []*sync.WaitGroup, toWait *sync.WaitGroup,  argument interface{}) *Action {
	return &Action{
		instruction: instruction,
		argument: argument,
		ToSend:  toSend,
		ToWait: toWait,
	}
}

/*
Wait all the actions it depends on to finish and then execute a predefined instruction. Then communicates the end of execution decrementing the counter of
// each ToSend waiting group. If an error occurs, it is written in errChannel
 */
func (a Action) execute(errChannel chan <- error) {
	a.ToWait.Wait()
	err := (*a.instruction)(a.argument)
	if err != nil {
		errChannel <- err
	}
	for _, sending := range a.ToSend {
		sending.Done()
	}
}

// ExecuteActions
/*
Execute all the defined actions give as argument
Use sync.WaitGroup to assure all the go routines are waited
sync.WaitGroup acts as a counter and while its not 0 wait
Each goroutine uses "defer wg.Done()" which decrement the counter by 1
Defer force the action wg.Done() to be execute when the goroutine return (when it finished)
 */
func ExecuteActions(actions map[string]*Action) error{
	errChannel := make(chan error, len(actions))
	var wg sync.WaitGroup
	for k, a := range actions {
		wg.Add(1)
		fmt.Printf("===== Executing routine: %s =====\n", k)
		go func(a *Action, errChannel chan <- error) {
			defer wg.Done()
			a.execute(errChannel)
		}(a, errChannel)
	}
	go func() {
		wg.Wait()
		close(errChannel)
	}()
	for err := range errChannel {
		if err != nil {
			return err
		}
	}
	return nil
}
