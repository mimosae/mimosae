/*
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Author(s): Viktor Colas, Tsimafei Liashkevich
*/

package concurrency

import (
	"fmt"
	"math/rand"
	"sync"
	"testing"
	"time"
)

var checkList []int

//creates a new wait group
func newWaitGroup() *sync.WaitGroup {
	return &sync.WaitGroup{}
}

//mocks an action with given parameters
func mockAction(toWait *sync.WaitGroup, toSend []*sync.WaitGroup, catchNumber int) *Action {
	instruction := func(n interface{}) error{
		fmt.Printf("Current action catch number is %v\n", n)
		checkList = append(checkList, n.(int))
		t := (rand.Intn(2) + 1) * int(time.Second.Seconds())
		time.Sleep(time.Duration(t))
		return nil
	}

	return NewAction(&instruction, toSend, toWait, catchNumber)
}

func TestConcurrency_Unit(t *testing.T) {
	t.Run("Test-Sequential", TestSequentialActions)
	t.Run("Test-Execute-With-Graph", TestExecuteActionsWithGraph)
}

// Tests waiting for action by another action
// Dependencies between actions are of the next form:
/*
				4 -> 3 -> 2 -> 1 -> 0
*/
func TestSequentialActions(t *testing.T) {
	checkList = make([]int, 5)
	actions := make(map[string]*Action, 5)

	actions["0"] = mockAction(newWaitGroup(), nil, 0)
	for i := 1; i < 5; i++ {
		action := mockAction(newWaitGroup(), nil, i)
		actions[fmt.Sprint(i)] = action
		action.ToWait.Add(1)
		var toSend []*sync.WaitGroup

		actions[fmt.Sprint(i-1)].ToSend = append(toSend, action.ToWait)
	}

	err := ExecuteActions(actions)

	if err != nil {
		t.Error(err)
	}

	for i := 1; i < 5; i++ {
		if checkList[i-1] > checkList[i] {
			t.Error("Execution order isn't good")
		}
	}
}

// Tests execution of actions given the following dependecies
/*

               0
                |
                1    2
                 \ /   \
                  3     4
                   \
                    5

*/
func TestExecuteActionsWithGraph(t *testing.T) {
	checkList = make([]int, 6)
	actions := make(map[string]*Action, 6)
	checkGroups := make([]*sync.WaitGroup, 6)

	for i := 0; i < 6; i++ {
		checkGroups[i] = newWaitGroup()
	}

	fmt.Println(checkGroups)

	actions["0"] = mockAction(checkGroups[0], []*sync.WaitGroup{checkGroups[1]}, 0)

	action := mockAction(checkGroups[1], []*sync.WaitGroup{checkGroups[3]}, 1)
	action.ToWait.Add(1)
	actions["1"] = action

	actions["2"] = mockAction(checkGroups[2], []*sync.WaitGroup{checkGroups[3], checkGroups[4]}, 2)

	action = mockAction(checkGroups[3], []*sync.WaitGroup{checkGroups[5]}, 3)
	action.ToWait.Add(2)
	actions["3"] = action

	action = mockAction(checkGroups[4], []*sync.WaitGroup{}, 4)
	action.ToWait.Add(1)
	actions["4"] = action

	action = mockAction(checkGroups[5], []*sync.WaitGroup{}, 5)
	action.ToWait.Add(1)
	actions["5"] = action

	err := ExecuteActions(actions)

	if err != nil {
		t.Error(err)
	}

	meetList := make([]bool, 6)
	for i := 0; i < 6; i++ {
		meetList[i] = true
		switch i {
		case 1:
			if !meetList[0] {
				t.Error("Execution order isn't good")
			}
		case 3:
			if !meetList[1] || !meetList[2] {
				t.Error("Execution order isn't good")
			}
		case 4:
			if !meetList[2] {
				t.Error("Execution order isn't good")
			}
		case 5:
			if !meetList[3] {
				t.Error("Execution order isn't good")
			}
		}
	}
}
