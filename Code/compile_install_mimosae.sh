#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Viktor Colas, Tsimafei Liashkevich, Julio Guzman, Denis Conan

usage="usage:
$0 --help
   display usage
$0 [--offline]
   --offline: run the demonstration without downloading files from remote repositories
"
while [ "$#" -gt 0 ]; do
    arg=$1
    if [ "$arg" = "--help" ]; then
	echo -e "$usage"
	exit 1
    elif [ "$arg" = "--offline" ]; then
        export OFFLINE="true"
    else
     	echo -e "$usage"
	exit 1
    fi
    shift
done

function isoffline() {
    if [ -z $OFFLINE ]; then
        return 1
    else
        return 0
    fi
}

BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
ORANGE=$(tput setaf 3)

SCENARIO_SCRIPT_FOLDER=$PWD
MIMOSAE_FOLDER=$PWD/MIMOSAE
AUTONOMIC_MANAGER_FOLDER=$PWD/AutonomicManager
SCRIPTS_FOLDER=$PWD/scripts
DEV_GDE_FOLDER=$PWD/UseCaseEdf_GDE/

SCENARIO_TEST=$PWD/UseCaseEdf_GDE/gde-client

if [ ! -v LPG_PROGRAM ]; then
    echo "No LPG_PROGRAM please export it before executing any scenario"
    exit 1
fi

if [ ! -v GOPATH ]; then
    echo "No GOPATH please export it before executing any scenario"
    exit 1
fi

export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOROOT/bin:$GOBIN

trap "cleanup; exit" SIGINT

echo "===== server protoc generation ====="
if [ ! -d AutonomicManager/Executor/server/proto ]; then
    mkdir AutonomicManager/Executor/server/proto
else
    if isoffline; then
        echo "-----Directory Executor/server/proto already exists-----"
    elif [ $# -eq 0 ]; then      
        rm -rf AutonomicManager/Executor/server/proto
        mkdir AutonomicManager/Executor/server/proto

        protoc --go_out=AutonomicManager/Executor/server/proto \
       --go-grpc_out=require_unimplemented_servers=false:AutonomicManager/Executor/server/proto \
       AutonomicManager/Protobuff/plannification.proto

        if [ ! $? ]; then
            echo "${RED}-----Protoc generation failed-----${BLACK}"
            exit 1
        fi

        if [ -d AutonomicManager/Executor/server/proto ] && [ ! -e AutonomicManager/Executor/server/proto/go.mod ]; then
            (cd AutonomicManager/Executor/server/proto;
            go mod init tsp.eu/executor/implementation_registry
            go mod tidy
            )
        fi
    fi
fi



echo "===== server protoc generation finished ====="

echo "===== building scenario client ====="
if ! mvn -f $MIMOSAE_FOLDER clean install -q -Dmaven.test.skip=true -DskipTests -Dmaven.javadoc.skip=true; then
    echo "${RED}----client build failed-----${BLACK}"
    exit 1
fi
echo "===== build scenario client finished ====="

echo "===== building executor ====="
actual_path=$PWD
cd $AUTONOMIC_MANAGER_FOLDER || echo "invalid path $AUTONOMIC_MANAGER_FOLDER"
if ! ./build.sh; then
    echo "${RED}-----modules of executor build failed-----${BLACK}"
    exit 1
fi
echo "Building Executor/main.go"
cd Executor || echo "invalid path $AUTONOMIC_MANAGER_FOLDER/Executor"
if ! (go mod tidy; go build main.go); then
    echo "${RED}-----main of executor build failed-----${BLACK}"
    exit 1
fi

cd $actual_path || echo "invalid path $actual_path"

echo "===== build executor finished ====="
echo "===== building scenario test ====="

if [ ! -d $SCENARIO_TEST ]; then
    echo "${RED}-----path to scenario is not valid (\"$SCENARIO_TEST\")-----"
    exit 1
else
    echo "Path to scenario is \"$SCENARIO_TEST\""
fi

if ! ( cd $SCENARIO_TEST && go build scenario.go ); then
    echo "${RED}-----scenario test build failed"
    exit 1
fi

echo "===== build scenario finished ====="

# exit 0 # do not exit because this shell script is sourced in scenario.sh
