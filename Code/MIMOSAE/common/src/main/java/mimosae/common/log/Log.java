/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.common.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.LoggerConfig;

/**
 * This class contains the configuration of some logging facilities.
 * 
 * To recapitulate, logging levels are: TRACE, DEBUG, INFO, WARN, ERROR, FATAL.
 * 
 * @author Denis Conan
 * 
 */
public final class Log {
	/**
	 * states whether logging is enabled or not.
	 */
	public static final boolean ON = true;
	/**
	 * name of logger for the model at runtime.
	 */
	public static final String LOGGER_MODEL_AT_RUNTIME = "model-at-runtime";
	/**
	 * logger object for the model at runtime.
	 */
	public static final Logger MODEL_AT_RUNTIME = LogManager.getLogger(LOGGER_MODEL_AT_RUNTIME);
	/**
	 * name of logger for the model at runtime.
	 */
	public static final String LOGGER_PLANNER = "planner";
	/**
	 * logger object for the model at runtime.
	 */
	public static final Logger PLANNER = LogManager.getLogger(LOGGER_PLANNER);
	/**
	 * name of logger for the model at runtime.
	 */
	public static final String LOGGER_SCENARIO = "scenario";
	/**
	 * logger object for the model at runtime.
	 */
	public static final Logger SCENARIO = LogManager.getLogger(LOGGER_SCENARIO);

	/**
	 * name of logger for the client proxy.
	 */
	public static final String LOGGER_PROTOBUF_CLIENT_PROXY = "protobuf-client-proxy";

	/**
	 * logger object for the client proxy.
	 */
	public static final Logger PROTOBUF_CLIENT_PROXY = LogManager.getLogger(LOGGER_PROTOBUF_CLIENT_PROXY);

	/*
	 * static configuration, which can be changed by command line options.
	 */
	static {
		setLevel(MODEL_AT_RUNTIME, Level.WARN);
		setLevel(PLANNER, Level.WARN);
		setLevel(SCENARIO, Level.WARN);
		setLevel(PROTOBUF_CLIENT_PROXY, Level.WARN);
	}
	
	/**
	 * private constructor to avoid instantiation of utility class.
	 */
	private Log() {
	}

	/**
	 * configures a logger to a level.
	 * 
	 * @param logger
	 *            the logger.
	 * @param level
	 *            the level.
	 */
	public static void setLevel(final Logger logger, final Level level) {
	    final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
	    final var config = ctx.getConfiguration();
	    var loggerConfig = config.getLoggerConfig(logger.getName());
	    var specificConfig = loggerConfig;
	    // We need a specific configuration for this logger,
	    // otherwise we would change the level of all other loggers
	    // having the original configuration as parent as well
	    if (!loggerConfig.getName().equals(logger.getName())) {
	        specificConfig = new LoggerConfig(logger.getName(), level, true);
	        specificConfig.setParent(loggerConfig);
	        config.addLogger(logger.getName(), specificConfig);
	    }
	    specificConfig.setLevel(level);
	    ctx.updateLoggers();
	}

	/**
	 * stringifies a stack trace.
	 * 
	 * @param stack
	 *            the array of stack trace elements.
	 * @return the string.
	 */
	public static String printStackTrace(final StackTraceElement[] stack) {
		var result = new StringBuilder();
		for (var i = 0; i < stack.length; i++) {
			result.append(stack[i]).append("\n");
		}
		return result.toString();
	}
}
