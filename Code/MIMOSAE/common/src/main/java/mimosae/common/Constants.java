/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.common;

/**
 * This is a utility class for defining common constants.
 * 
 * @author Denis Conan
 */
public final class Constants {
	/**
	 * the word separator for identifiers, e.g. when concatenating the identifier of
	 * the microservice and the type identifier of the client contract type for the
	 * identifier of a client contract, when concatenating client and server
	 * contracts for the identifier of the client-server contract, when
	 * concatenating the database contract and the database system identifier for
	 * the identifier of the database connector.
	 */
	public static final String WORD_SEPARATOR_FOR_IDENTIFIERS = "-";
	/**
	 * the separator for PDDL identifiers, e.g. when concatenating an instance
	 * identifier with the identifier of the type and with the version of the type.
	 */
	public static final String SEPARATOR_FOR_PDDL_IDENTIFIERS = "_";

	/**
	 * this class has no instance.
	 */
	private Constants() {
	}
}
