/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.common.pddl;

import java.util.Objects;

/**
 * This class models the constraint stating that an instance must be created
 * before another one, e.g. a database system instance before its client
 * microservice instances and a publish-subscribe connector must be created
 * before its client microservice instances.
 * 
 * @author Denis Conan
 */
public final class HappenBeforeRelationship {
	/**
	 * the identifier of the instance that must be created before the other one.
	 */
	private final String beforeIdentifier;
	/**
	 * the index in the collection of parallel actions that contains the creation of
	 * the {@link #beforeIdentifier} instance.
	 */
	private int beforeIndex;
	/**
	 * the identifier of the instance that must be created after the other one.
	 */
	private final String afterIdentifier;
	/**
	 * the index in the collection of parallel actions that contains the creation of
	 * the {@link #afterIdentifier} instance.
	 */
	private int afterIndex;

	/**
	 * constructs a new "happen before" relationship.
	 * 
	 * @param beforeIdentifier the identifier of the instance that must be created
	 *                         before the other one.
	 * @param afterIdentifier  the identifier of the instance that must be created
	 *                         after the other one.
	 */
	public HappenBeforeRelationship(final String beforeIdentifier, final String afterIdentifier) {
		Objects.requireNonNull(beforeIdentifier, "beforeIdentifier cannot be null");
		if (beforeIdentifier.isBlank()) {
			throw new IllegalArgumentException("beforeIdentifier cannot be blank");
		}
		Objects.requireNonNull(afterIdentifier, "afterIdentifier cannot be null");
		if (afterIdentifier.isBlank()) {
			throw new IllegalArgumentException("afterIdentifier cannot be blank");
		}
		this.beforeIdentifier = beforeIdentifier.toLowerCase();
		beforeIndex = -1;
		this.afterIdentifier = afterIdentifier.toLowerCase();
		afterIndex = -1;
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return beforeIdentifier != null && !beforeIdentifier.isBlank() && afterIdentifier != null
				&& !afterIdentifier.isBlank()
				&& ((beforeIndex == -1 && afterIndex == -1) || (beforeIndex >= 0 && afterIndex >= 0));
	}

	/**
	 * obtains the identifier of the instance that must be created before the other
	 * one.
	 * 
	 * @return the identifier of before.
	 */
	public String getBeforeIdentifier() {
		return beforeIdentifier;
	}

	/**
	 * obtains the index of the instance that must be created before the other one.
	 * 
	 * @return the index of before.
	 */
	public int getBeforeIndex() {
		return beforeIndex;
	}

	/**
	 * sets the index of the instance that must be created before the other one.
	 * 
	 * @param beforeIndex the new index of before.
	 */
	public void setBeforeIndex(final int beforeIndex) {
		this.beforeIndex = beforeIndex;
	}

	/**
	 * obtains the index of the instance that must be created after the other one.
	 * 
	 * @return the identifier of after.
	 */
	public String getAfterIdentifier() {
		return afterIdentifier;
	}

	/**
	 * obtains the identifier of the instance that must be created after the other
	 * one.
	 * 
	 * @return the index of after.
	 */
	public int getAfterIndex() {
		return afterIndex;
	}

	/**
	 * sets the index of the instance that must be created after the other one.
	 * 
	 * @param afterIndex the new index of before.
	 */
	public void setAfterIndex(final int afterIndex) {
		this.afterIndex = afterIndex;
	}

	@Override
	public int hashCode() {
		return Objects.hash(afterIdentifier, beforeIdentifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof HappenBeforeRelationship)) {
			return false;
		}
		HappenBeforeRelationship other = (HappenBeforeRelationship) obj;
		return Objects.equals(afterIdentifier, other.afterIdentifier)
				&& Objects.equals(beforeIdentifier, other.beforeIdentifier);
	}

	@Override
	public String toString() {
		return "[beforeIdentifier=" + beforeIdentifier + ", beforeIndex=" + beforeIndex
				+ ", afterIdentifier=" + afterIdentifier + ", afterIndex=" + afterIndex + "]";
	}

}
