/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.common.pddl;

import java.util.Objects;

/**
 * The enumerators of this enumeration are the names of the PDDL predicates in
 * the PDDL domain file.
 * 
 * @author Denis Conan
 */
public enum PredicateNames {
	/**
	 * (microservice ?m - microservice).
	 */
	MICROSERVICE("microservice "),
	/**
	 * (databasesystem ?db - databasesystem).
	 */
	DATABASESYSTEM("databasesystem "),
//	/**
//	 * (clientcontract ?c - clientcontract).
//	 */
//	CLIENT_CONTRACT("clientcontract "),
//	/**
//	 * (microservice_has_clientcontract ?m - microservice ?c - clientcontract).
//	 */
//	MICROSERVICE_HAS_CLIENT_CONTRACT("microservice_has_clientcontract "),
//	/**
//	 * (servercontract ?c - servercontract).
//	 */
//	SERVER_CONTRACT("servercontract "),
//	/**
//	 * (microservice_has_servercontract ?m - microservice ?c - servercontract).
//	 */
//	MICROSERVICE_HAS_SERVER_CONTRACT("microservice_has_servercontract "),
//	/**
//	 * (producercontract ?c - producercontract).
//	 */
//	PRODUCER_CONTRACT("producercontract "),
//	/**
//	 * (microservice_has_producercontract ?m - microservice ?c - producercontract).
//	 */
//	MICROSERVICE_HAS_PRODUCER_CONTRACT("microservice_has_producercontract "),
//	/**
//	 * (consumercontract ?c - consumercontract).
//	 */
//	CONSUMER_CONTRACT("consumercontract "),
//	/**
//	 * (microservice_has_consumercontract ?m - microservice ?c - consumercontract).
//	 */
//	MICROSERVICE_HAS_CONSUMER_CONTRACT("microservice_has_consumercontract "),
//	/**
//	 * (databasecontract ?c - databasecontract).
//	 */
//	DATABASE_CONTRACT("databasecontract "),
//	/**
//	 * (microservice_has_databasecontract ?m - microservice ?c - databasecontract).
//	 */
//	MICROSERVICE_HAS_DATABASE_CONTRACT("microservice_has_databasecontract "),
	/**
	 * (pubsubconnector ?c - pubsubconnector).
	 */
	PUBSUB_CONNECTOR("pubsubconnector "),
	/**
	 * (client_server_link ?mc - microservice ?c - clientcontract ?ms - microservice
	 * ?s - servercontract).
	 */
	CLIENT_SERVER_LINK("client_server_link "),
	/**
	 * (producer_to_connector_link ?m - microservice ?p - producercontract ?c -
	 * pubsubconnector).
	 */
	PRODUCER_TO_CONNECTOR_LINK("producer_to_connector_link "),
	/**
	 * (consumer_to_connector_link ?m - microservice ?c - consumercontract ?p -
	 * pubsubconnector).
	 */
	CONSUMER_TO_CONNECTOR_LINK("consumer_to_connector_link "),
	/**
	 * (database_connection ?mc - microservice ?c - databasecontract ?db -
	 * databasesystem).
	 */
	DATABASE_CONNECTION("database_connection ");

	/**
	 * the PDDL name of the predicate in the PDDL domain file.
	 */
	private final String pddlName;

	/**
	 * constructs an enumerator.
	 * 
	 * @param pddlName the PDDL name of the enumerator.
	 */
	PredicateNames(final String pddlName) {
		this.pddlName = pddlName;
	}

	/**
	 * obtains the PDDL name.
	 * 
	 * @return the PDDL name.
	 */
	public String getPddlName() {
		return pddlName;
	}

	/**
	 * is the given string the name of one of the enumerators.
	 * 
	 * @param predicateName the name of the object type, i.e. PDDL name.
	 * @return true when there is a corresponding enumerator.
	 */
	public static PredicateNames isPredicateName(final String predicateName) {
		Objects.requireNonNull(predicateName, "predicateName cannot be null");
		for (PredicateNames an : values()) {
			if (an.pddlName.equals(predicateName)) {
				return an;
			}
		}
		throw new IllegalArgumentException("unknown predicate name (" + predicateName + ")");
	}

	@Override
	public String toString() {
		return pddlName;
	}
}
