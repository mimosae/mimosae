/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.common.pddl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class defines the collection of {@link HappenBeforeRelationship}
 * objects.
 * 
 * @author Denis Conan
 */
public final class HappenBeforeRelationships {
	/**
	 * the collection of relationships.
	 */
	private Map<HappenBeforeRelationship, HappenBeforeRelationship> constraintsHappenBefore;
	/**
	 * the same collection as {@link #constraintsHappenBefore}, but grouped by the
	 * before identifier.
	 */
	private Map<String, List<HappenBeforeRelationship>> beforeAccessToConstraints;
	/**
	 * the same collection as {@link #constraintsHappenBefore}, but grouped by the
	 * after identifier.
	 */
	private Map<String, List<HappenBeforeRelationship>> afterAccessToConstraints;

	/**
	 * construcs a new empty collection.
	 */
	public HappenBeforeRelationships() {
		constraintsHappenBefore = new HashMap<>();
		beforeAccessToConstraints = new HashMap<>();
		afterAccessToConstraints = new HashMap<>();
	}

	/**
	 * obtains the "happen before" relationship corresponding to the given before
	 * and the after identifiers.
	 * 
	 * @param beforeIdentifier the before identifier of the constraint.
	 * @param afterIdentifier  the after identifier of the constraint.
	 * @return the relationship searched for.
	 */
	public HappenBeforeRelationship getHappenBeforeRelationship(final String beforeIdentifier,
			final String afterIdentifier) {
		return constraintsHappenBefore.get(new HappenBeforeRelationship(beforeIdentifier, afterIdentifier));
	}

	/**
	 * obtains the "happen before" relationship corresponding to the given before
	 * identifier.
	 * 
	 * @param beforeIdentifier the before identifier of the constraints.
	 * @return the relationships searched for.
	 */
	public List<HappenBeforeRelationship> getRelationshipsOfBefore(final String beforeIdentifier) {
		return beforeAccessToConstraints.get(beforeIdentifier.toLowerCase());
	}

	/**
	 * obtains the "happen before" relationship corresponding to the given before
	 * identifier.
	 * 
	 * @param afterIdentifier the before identifier of the constraints.
	 * @return the relationships searched for.
	 */
	public List<HappenBeforeRelationship> getRelationshipsOfAfter(final String afterIdentifier) {
		return afterAccessToConstraints.get(afterIdentifier.toLowerCase());
	}

	/**
	 * obtains a copy of the collection of the happen-before relationships.
	 * 
	 * @return a copy of the collection.
	 */
	public Collection<HappenBeforeRelationship> getHappenBeforeRelationships() {
		return Collections.unmodifiableCollection(constraintsHappenBefore.values());
	}

	/**
	 * add a new constraint.
	 * 
	 * @param beforeIdentifier the before identifier.
	 * @param afterIdentifier  the after identifier.
	 */
	public void addHappenBeforeRelationship(final String beforeIdentifier, final String afterIdentifier) {
		var constraint = new HappenBeforeRelationship(beforeIdentifier.toLowerCase(), afterIdentifier.toLowerCase());
		if (!constraintsHappenBefore.containsKey(constraint)) {
			constraintsHappenBefore.put(constraint, constraint);
			beforeAccessToConstraints.computeIfAbsent(beforeIdentifier.toLowerCase(), c -> new ArrayList<>())
					.add(constraint);
			afterAccessToConstraints.computeIfAbsent(afterIdentifier.toLowerCase(), c -> new ArrayList<>())
					.add(constraint);
		}
	}

	/**
	 * states whether there still exists some "happen before" with either the before
	 * index or the after index equal to -1.
	 * 
	 * @return true when some index not assigned.
	 */
	public boolean existHappenBeforRelationshipsWithNoAssignedIndex() {
		return this.constraintsHappenBefore.values().stream()
				.anyMatch(c -> c.getBeforeIndex() == -1 || c.getAfterIndex() == -1);
	}

	@Override
	public String toString() {
		return "HappenBeforeRelationships = " + constraintsHappenBefore.values().stream()
				.map(HappenBeforeRelationship::toString).collect(Collectors.joining("\n\t", "\n\t", ""));
	}

}
