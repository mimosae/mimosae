// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Yuwei Wang
Contributor(s): Denis Conan
 */
package mimosae.scenario.edf.gde;

import java.util.Arrays;
import java.util.Objects;

import mimosae.common.log.Log;
import mimosae.modelatruntime.MimosaeRuntimeModelInstance;
import mimosae.modelatruntime.MimosaeRuntimeModelType;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.exception.AlreadyExistsModelElementException;
import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.exception.IsNotDeployableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.IsNotInstantiableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.MalformedOperationDeclarationException;
import mimosae.modelatruntime.exception.UnknownInstanceException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.exception.UnmatchingTypesException;
import mimosae.modelatruntime.exception.WrongNumberOfIdentifiersException;

public final class Step1CreateInitialGDEConfiguration {

	private Step1CreateInitialGDEConfiguration() {
		// no instance
	}

	public static String createInitialGDEConfiguration()
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedOperationDeclarationException, UnknownTypeException, UnknownInstanceException,
			UnmatchingTypesException, AlreadyConnectedException, IsCommittedAndCannotBeModifiedException,
			IsNotInstantiableAndCannotBeCommittedException, IsNotDeployableAndCannotBeCommittedException {
		// Facades
		// facade for types
		var facadeType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		// facade for instances
		var facadeInstance = MimosaeRuntimeModelInstance.getMimosaeRuntimeModelInstance();
		// Operation declarations: "name paramType... : returnType"
		// user service
		var usOps1 = new String[] { "CreateUser String String : gde.User", "CreateUserByUser gde.User : gde.User",
				"DeleteUser long", "FindUserByName String : gde.User", "FindUserById long : gde.User",
				"ListUsers : List<gde.User>" };
		var usOps2 = new String[] { "CheckPassword String String : boolean" };
		var usOps3 = new String[] { "CreateGroup String : gde.Group", "CreateGroupByGroup gde.Group : gde.Group",
				"DeleteGroup long", "FindGroupsByName String : List<gde.Group>", "FindGroupById long : gde.Group",
				"ListGroups : List<gde.Group>" };
		var usOps4 = new String[] { "AddUserToGroup long long", "AddUsersToGroup : List<Long> long",
				"RemoveUserFromGroup long long", "IsUserInGroup long long : boolean",
				"ListUsersInGroup long : List<gde.User>", "ListGroupsOfUser long : List<gde.Group>" };
		// project service
		var psOps1 = new String[] { "CreateProject gde.Project : gde.Project",
				"UpdateProject gde.Project : gde.Project", "DeleteProject long", "RestoreProject long",
				"FindProjectById long : gde.Project", "FindProjectsByName String : List<gde.Project>",
				"FindProjectsByName String : List<gde.Project>",
				"FindProjectsByCreationDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByDeletionDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByUpdateDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByRestorationDate Date gde.DateComparator : List<gde.Project>",
				"ListProjects : List<gde.Project>" };
		var psOps2 = new String[] { "GetProjectLocker long : long", "IsProjectLocked long long : boolean",
				"SetProjectLockState long long boolean" };
		var psOps3 = new String[] { "ListProjectFiles long String : List<gde.GDEFile>",
				"AttachFileToProject long long long String", "DetachFileFromProject long long long String" };
		// file service
		var fsOps1 = new String[] { "CreateFile gde.GDEFile : gde.GDEFile", "UpdateFile gde.GDEFile : gde.GDEFile",
				"DeleteFile long", "RestoreFile long", "FindFileById long : gde.GDEFile", "ReadFile long : gde.GDEFile",
				"FindFilesByName String : List<gde.GDEFile>",
				"FindFilesByCreationDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByDeletionDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByUpdateDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByRestorationDate Date gde.DateComparator : List<gde.GDEFile>",
				"ListFiles : List<gde.GDEFile>", "FindFilesToIndex : List<gde.GDEFile>",
				"FindFilesDeleted : List<gde.GDEFile>", "SetValid long boolean", "IsValid long : boolean",
				"SetIndexed long boolean", "IsIndexed long : boolean" };
		var fsOps2 = new String[] { "OpenFile long long : boolean", "CloseFile long long : boolean" };
		var fsOps3 = new String[] { "CreateChunk gde.Chunk : gde.Chunk", "ReadChunk long : gde.Chunk" };
		var fsOps4 = new String[] { "AttachChunkToFile long long", "DetachChunkFromFile long long",
				"FindChunksByFileId long : gde.Chunk", "FindChunksInfoByFileId long : gde.ChunkInfo" };
		// authentication
		var asOps1 = new String[] { "Login String String : String" };
		var asOps2 = new String[] { "GetLogin String : String" };
		// permission service
		var pmsOps1 = new String[] { "CreateService gde.Service : gde.Service", "DeleteService long : boolean",
				"UpdateService gde.Service : gde.Service", "FindServiceById long : gde.Service",
				"FindServiceByName String : gde.Service", "ListServices : List<gde.Service>" };

		var pmsOps2 = new String[] { "CreateMethod gde.Method : gde.Method", "DeleteMethod long : boolean",
				"UpdateMethod gde.Method : gde.Method", "FindMethodById long : gde.Method",
				"FindMethodByName String : gde.Method", "FindMethodByServiceNameMethodIndex String int : gde.Method",
				"FindMethodsByService long : List<gde.Method>", "ListMethods : List<gde.Method>" };
		var pmsOps3 = new String[] { "CreatePermission gde.Permission : gde.Permission",
				"DeletePermission long : boolean", "UpdatePermission gde.Permission : gde.Permission",
				"FindPermissionById long : gde.Permission", "FindPermissionsByMethodID long : List<gde.Permission>",
				"ListPermissions : List<gde.Permission>" };
		var pmsOps4 = new String[] { "FindPermissionsByGroupID long String : List<gde.Permission>",
				"FindPermissionsByUserID long String : List<gde.Permission>",
				"HasGroupThePermissionOn long String int String : boolean",
				"HasUserThePermissionOn long String int String : boolean",
				"CheckUserPermission String String int String : boolean" };
		// Server contract types
		// user service
		var userServiceusers = "USusers";
		var userServicecheckpassword = "UScheckpasswd";
		var userServicegroups = "USgroups";
		var userServiceusersGroups = "UService/usersgroups";
		facadeType.createServerContractType(userServiceusers, Scenario1.VERSION1_0_0, usOps1);
		facadeType.createServerContractType(userServicecheckpassword, Scenario1.VERSION1_0_0, usOps2);
		facadeType.createServerContractType(userServicegroups, Scenario1.VERSION1_0_0, usOps3);
		facadeType.createServerContractType(userServiceusersGroups, Scenario1.VERSION1_0_0, usOps4);
		var usServerContract = new String[] { userServiceusers, Scenario1.VERSION1_0_0, userServicecheckpassword,
				Scenario1.VERSION1_0_0, userServicegroups, Scenario1.VERSION1_0_0, userServiceusersGroups,
				Scenario1.VERSION1_0_0 };
		// project service
		var projectServiceprojects = "PSprojects";
		var projectServiceprojectlock = "PSlock";
		var projectServiceprojectfiles = "PSfiles";
		facadeType.createServerContractType(projectServiceprojects, Scenario1.VERSION1_0_0, psOps1);
		facadeType.createServerContractType(projectServiceprojectlock, Scenario1.VERSION1_0_0, psOps2);
		facadeType.createServerContractType(projectServiceprojectfiles, Scenario1.VERSION1_0_0, psOps3);
		var psServerContract = new String[] { projectServiceprojects, Scenario1.VERSION1_0_0, projectServiceprojectlock,
				Scenario1.VERSION1_0_0, projectServiceprojectfiles, Scenario1.VERSION1_0_0 };
		// file service
		var fileServicefiles = "FSfiles";
		var fileServicefilelock = "FSfilelock";
		var fileServicechunks = "FSchunks";
		var fileServicefilechunks = "FSfilechunks";
		facadeType.createServerContractType(fileServicefiles, Scenario1.VERSION1_0_0, fsOps1);
		facadeType.createServerContractType(fileServicefilelock, Scenario1.VERSION1_0_0, fsOps2);
		facadeType.createServerContractType(fileServicechunks, Scenario1.VERSION1_0_0, fsOps3);
		facadeType.createServerContractType(fileServicefilechunks, Scenario1.VERSION1_0_0, fsOps4);
		var fsServerContract = new String[] { fileServicefiles, Scenario1.VERSION1_0_0, fileServicefilelock,
				Scenario1.VERSION1_0_0, fileServicechunks, Scenario1.VERSION1_0_0, fileServicefilechunks,
				Scenario1.VERSION1_0_0 };
		// authentication
		var authenticationlogin = "Alogin";
		var authenticationgetlogin = "Agetlogin";
		facadeType.createServerContractType(authenticationlogin, Scenario1.VERSION1_0_0, asOps1);
		facadeType.createServerContractType(authenticationgetlogin, Scenario1.VERSION1_0_0, asOps2);
		var asServerContract = new String[] { authenticationlogin, Scenario1.VERSION1_0_0, authenticationgetlogin,
				Scenario1.VERSION1_0_0 };
		// permission service
		var permissionServiceservices = "PermSservices";
		var permissionServicemethods = "PermSmethods";
		var permissionServicepermissions = "PermSperms";
		var permissionServicecheckpermission = "PermScheck";
		facadeType.createServerContractType(permissionServiceservices, Scenario1.VERSION1_0_0, pmsOps1);
		facadeType.createServerContractType(permissionServicemethods, Scenario1.VERSION1_0_0, pmsOps2);
		facadeType.createServerContractType(permissionServicepermissions, Scenario1.VERSION1_0_0, pmsOps3);
		facadeType.createServerContractType(permissionServicecheckpermission, Scenario1.VERSION1_0_0, pmsOps4);
		var pmsServerContract = new String[] { permissionServiceservices, Scenario1.VERSION1_0_0,
				permissionServicemethods, Scenario1.VERSION1_0_0, permissionServicepermissions, Scenario1.VERSION1_0_0,
				permissionServicecheckpermission, Scenario1.VERSION1_0_0 };
		// Client contract types
		// user service
		var userServiceRequiresAuthenticationgetlogin = "USrAgetlogin";
		var userServiceRequiresPermissionServicecheckpermission = "USrPermScheck";
		var projectServiceRequiresAuthenticationgetlogin = "PSrAgetlogin";
		var projectServiceRequiresPermissionServicecheckpermission = "PSrPermScheck";
		var projectServiceRequiresFileServicefiles = "PSrFSfiles";
		var projectServiceRequiresUserServiceusers = "PSrUSusers";
		var fileServiceRequiresAuthenticationgetlogin = "FSrAgetlogin";
		var fileServiceRequiresPermissionServicecheckpermission = "FSrPermScheck";
		var fileServiceRequiresUserServiceusers = "FSrUSusers";
		var authenticationRequiresUserServicecheckpassword = "ArUScheckpassword";
		var permissionServiceRequiresAuthenticationgetlogin = "PermSrAgetlogin";
		var permissionServiceRequiresUserServiceusers = "PermSrUSusers";
		var permissionServiceRequiresUserServiceusersgroups = "PermSrUSusersgroups";
		facadeType.createClientContractType(userServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0, asOps2);
		facadeType.createClientContractType(userServiceRequiresPermissionServicecheckpermission, Scenario1.VERSION1_0_0,
				pmsOps4);
		// project service
		facadeType.createClientContractType(projectServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				asOps2);
		facadeType.createClientContractType(projectServiceRequiresPermissionServicecheckpermission,
				Scenario1.VERSION1_0_0, pmsOps4);
		facadeType.createClientContractType(projectServiceRequiresFileServicefiles, Scenario1.VERSION1_0_0, fsOps1);
		facadeType.createClientContractType(projectServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0, usOps1);
		// file service
		facadeType.createClientContractType(fileServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0, asOps2);
		facadeType.createClientContractType(fileServiceRequiresPermissionServicecheckpermission, Scenario1.VERSION1_0_0,
				pmsOps4);
		facadeType.createClientContractType(fileServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0, usOps1);
		// authentication
		facadeType.createClientContractType(authenticationRequiresUserServicecheckpassword, Scenario1.VERSION1_0_0,
				usOps2);
		// permission service
		facadeType.createClientContractType(permissionServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				asOps2);
		facadeType.createClientContractType(permissionServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0, usOps1);
		facadeType.createClientContractType(permissionServiceRequiresUserServiceusersgroups, Scenario1.VERSION1_0_0,
				usOps4);
		// Combine Client contract types for microservice type
		var usClientContract = new String[] { userServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				userServiceRequiresPermissionServicecheckpermission, Scenario1.VERSION1_0_0 };
		var psClientContract = new String[] { projectServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				projectServiceRequiresPermissionServicecheckpermission, Scenario1.VERSION1_0_0,
				projectServiceRequiresFileServicefiles, Scenario1.VERSION1_0_0, projectServiceRequiresUserServiceusers,
				Scenario1.VERSION1_0_0 };
		var fsClientContract = new String[] { fileServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				fileServiceRequiresPermissionServicecheckpermission, Scenario1.VERSION1_0_0,
				fileServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0 };
		var asClientContract = new String[] { authenticationRequiresUserServicecheckpassword, Scenario1.VERSION1_0_0 };
		var pmsClientContract = new String[] { permissionServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				permissionServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0,
				permissionServiceRequiresUserServiceusersgroups, Scenario1.VERSION1_0_0 };
		// Database contract types
		// user service
		var usDBContract = "USDBContract";
		// file service
		var fsDBContract = "FSDBContract";
		// project service
		var psDBContract = "PSDBContract";
		// permission service
		var permDBContract = "PERMDBContract";
		facadeType.createDatabaseContractType(usDBContract, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseContractType(fsDBContract, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseContractType(psDBContract, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseContractType(permDBContract, Scenario1.VERSION1_0_0);
		// join client, server, producer, and consumer contract types
		String[] usDBC = new String[] { usDBContract, Scenario1.VERSION1_0_0 };
		String[] usContracts = Arrays.copyOf(usClientContract,
				usClientContract.length + usServerContract.length + usDBC.length);
		System.arraycopy(usServerContract, 0, usContracts, usClientContract.length, usServerContract.length);
		System.arraycopy(usDBC, 0, usContracts, usClientContract.length + usServerContract.length, usDBC.length);
		String[] psDBC = new String[] { psDBContract, Scenario1.VERSION1_0_0 };
		String[] psContracts = Arrays.copyOf(psClientContract,
				psClientContract.length + psServerContract.length + psDBC.length);
		System.arraycopy(psServerContract, 0, psContracts, psClientContract.length, psServerContract.length);
		System.arraycopy(psDBC, 0, psContracts, psClientContract.length + psServerContract.length, psDBC.length);
		String[] fsDBC = new String[] { fsDBContract, Scenario1.VERSION1_0_0 };
		String[] fsContracts = Arrays.copyOf(fsClientContract,
				fsClientContract.length + fsServerContract.length + fsDBC.length);
		System.arraycopy(fsServerContract, 0, fsContracts, fsClientContract.length, fsServerContract.length);
		System.arraycopy(fsDBC, 0, fsContracts, fsClientContract.length + fsServerContract.length, fsDBC.length);
		String[] asContracts = Arrays.copyOf(asClientContract, asClientContract.length + asServerContract.length);
		System.arraycopy(asServerContract, 0, asContracts, asClientContract.length, asServerContract.length);
		String[] permDBC = new String[] { permDBContract, Scenario1.VERSION1_0_0 };
		String[] pmsContracts = Arrays.copyOf(pmsClientContract,
				pmsClientContract.length + pmsServerContract.length + permDBC.length);
		System.arraycopy(pmsServerContract, 0, pmsContracts, pmsClientContract.length, pmsServerContract.length);
		System.arraycopy(permDBC, 0, pmsContracts, pmsClientContract.length + pmsServerContract.length, permDBC.length);
		// Database system type
		// user service
		var dbUS = "DBUS";
		// file service
		var dbFS = "DBFS";
		// project service
		var dbPS = "DBPS";
		// permission service
		var dbPERM = "DBPERM";
		facadeType.createDatabaseSystemType(dbUS, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseSystemType(dbFS, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseSystemType(dbPS, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseSystemType(dbPERM, Scenario1.VERSION1_0_0);
		// Microservice type
		var userServiceType = "US";
		var projectServiceType = "PS";
		var fileServiceType = "FS";
		var authenticationType = "Auth";
		var permissionServiceType = "PermS";
		facadeType.createMicroserviceType(userServiceType, Scenario1.VERSION1_0_0, usContracts);
		facadeType.createMicroserviceType(projectServiceType, Scenario1.VERSION1_0_0, psContracts);
		facadeType.createMicroserviceType(fileServiceType, Scenario1.VERSION1_0_0, fsContracts);
		facadeType.createMicroserviceType(authenticationType, Scenario1.VERSION1_0_0, asContracts);
		facadeType.createMicroserviceType(permissionServiceType, Scenario1.VERSION1_0_0, pmsContracts);
		// Connector types
		// client-server connector types
		facadeType.createClientServerConnectorType(userServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				authenticationgetlogin, Scenario1.VERSION1_0_0);
		facadeType.createClientServerConnectorType(userServiceRequiresPermissionServicecheckpermission,
				Scenario1.VERSION1_0_0, permissionServicecheckpermission, Scenario1.VERSION1_0_0);

		facadeType.createClientServerConnectorType(projectServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				authenticationgetlogin, Scenario1.VERSION1_0_0);
		facadeType.createClientServerConnectorType(projectServiceRequiresPermissionServicecheckpermission,
				Scenario1.VERSION1_0_0, permissionServicecheckpermission, Scenario1.VERSION1_0_0);
		facadeType.createClientServerConnectorType(projectServiceRequiresFileServicefiles, Scenario1.VERSION1_0_0,
				fileServicefiles, Scenario1.VERSION1_0_0);
		facadeType.createClientServerConnectorType(projectServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0,
				userServiceusers, Scenario1.VERSION1_0_0);

		facadeType.createClientServerConnectorType(fileServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0,
				authenticationgetlogin, Scenario1.VERSION1_0_0);
		facadeType.createClientServerConnectorType(fileServiceRequiresPermissionServicecheckpermission,
				Scenario1.VERSION1_0_0, permissionServicecheckpermission, Scenario1.VERSION1_0_0);
		facadeType.createClientServerConnectorType(fileServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0,
				userServiceusers, Scenario1.VERSION1_0_0);

		facadeType.createClientServerConnectorType(authenticationRequiresUserServicecheckpassword,
				Scenario1.VERSION1_0_0, userServicecheckpassword, Scenario1.VERSION1_0_0);

		facadeType.createClientServerConnectorType(permissionServiceRequiresAuthenticationgetlogin,
				Scenario1.VERSION1_0_0, authenticationgetlogin, Scenario1.VERSION1_0_0);
		facadeType.createClientServerConnectorType(permissionServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0,
				userServiceusers, Scenario1.VERSION1_0_0);
		facadeType.createClientServerConnectorType(permissionServiceRequiresUserServiceusersgroups,
				Scenario1.VERSION1_0_0, userServiceusersGroups, Scenario1.VERSION1_0_0);
		// database connector types
		facadeType.createDatabaseConnectorType(usDBContract, Scenario1.VERSION1_0_0, dbUS, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseConnectorType(fsDBContract, Scenario1.VERSION1_0_0, dbFS, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseConnectorType(psDBContract, Scenario1.VERSION1_0_0, dbPS, Scenario1.VERSION1_0_0);
		facadeType.createDatabaseConnectorType(permDBContract, Scenario1.VERSION1_0_0, dbPERM, Scenario1.VERSION1_0_0);
		// Commit configuration types
		facadeType.commitConfigurationType();
		var s1 = facadeType.toStringCurrentConfigurationType();
		Log.SCENARIO.info("{}", () -> "\n====================\nConfiguration type for the first instanciation \n" + s1);
		Objects.requireNonNull(facadeInstance, "facade instance cannot be null");
		facadeInstance.setConfigurationTypeOfCurrentConfiguration(
				MimosaeRuntimeModelType.getMimosaeRuntimeModelType().getIdentifierOfLastCommittedConfigurationType());
		// Instances
		// database system instance
		// user service
		var dbUSInstance = "DBUSi";
		// file service
		var dbFSInstance = "DBFSi";
		// project service
		var dbPSInstance = "DBPSi";
		// permission service
		var dbPERMInstance = "DBPERMi";
		facadeInstance.createDatabaseSystem(dbUSInstance, dbUS, Scenario1.VERSION1_0_0);
		facadeInstance.createDatabaseSystem(dbFSInstance, dbFS, Scenario1.VERSION1_0_0);
		facadeInstance.createDatabaseSystem(dbPSInstance, dbPS, Scenario1.VERSION1_0_0);
		facadeInstance.createDatabaseSystem(dbPERMInstance, dbPERM, Scenario1.VERSION1_0_0);
		// microservice instance
		var userServiceInstance = "USi";
		var projectServiceInstance = "PSi";
		var fileServiceInstance = "FSi";
		var authenticationInstance = "Ai";
		var permissionServiceInstance = "PermSi";
		facadeInstance.createMicroservice(userServiceInstance, userServiceType, Scenario1.VERSION1_0_0);
		facadeInstance.createMicroservice(projectServiceInstance, projectServiceType, Scenario1.VERSION1_0_0);
		facadeInstance.createMicroservice(fileServiceInstance, fileServiceType, Scenario1.VERSION1_0_0);
		facadeInstance.createMicroservice(authenticationInstance, authenticationType, Scenario1.VERSION1_0_0);
		facadeInstance.createMicroservice(permissionServiceInstance, permissionServiceType, Scenario1.VERSION1_0_0);
		// client-server link
		facadeInstance.linkClientServer(userServiceInstance, userServiceType, Scenario1.VERSION1_0_0,
				userServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0, authenticationInstance,
				authenticationType, Scenario1.VERSION1_0_0, authenticationgetlogin, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(userServiceInstance, userServiceType, Scenario1.VERSION1_0_0,
				userServiceRequiresPermissionServicecheckpermission, Scenario1.VERSION1_0_0, permissionServiceInstance,
				permissionServiceType, Scenario1.VERSION1_0_0, permissionServicecheckpermission, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(projectServiceInstance, projectServiceType, Scenario1.VERSION1_0_0,
				projectServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0, authenticationInstance,
				authenticationType, Scenario1.VERSION1_0_0, authenticationgetlogin, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(projectServiceInstance, projectServiceType, Scenario1.VERSION1_0_0,
				projectServiceRequiresPermissionServicecheckpermission, Scenario1.VERSION1_0_0,
				permissionServiceInstance, permissionServiceType, Scenario1.VERSION1_0_0, permissionServicecheckpermission,
				Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(projectServiceInstance, projectServiceType, Scenario1.VERSION1_0_0,
				projectServiceRequiresFileServicefiles, Scenario1.VERSION1_0_0, fileServiceInstance, fileServiceType,
				Scenario1.VERSION1_0_0, fileServicefiles, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(projectServiceInstance, projectServiceType, Scenario1.VERSION1_0_0,
				projectServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0, userServiceInstance, userServiceType,
				Scenario1.VERSION1_0_0, userServiceusers, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileServiceType, Scenario1.VERSION1_0_0,
				fileServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0, authenticationInstance,
				authenticationType, Scenario1.VERSION1_0_0, authenticationgetlogin, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileServiceType, Scenario1.VERSION1_0_0,
				fileServiceRequiresPermissionServicecheckpermission, Scenario1.VERSION1_0_0, permissionServiceInstance,
				permissionServiceType, Scenario1.VERSION1_0_0, permissionServicecheckpermission, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileServiceType, Scenario1.VERSION1_0_0,
				fileServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0, userServiceInstance, userServiceType,
				Scenario1.VERSION1_0_0, userServiceusers, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(authenticationInstance, authenticationType, Scenario1.VERSION1_0_0,
				authenticationRequiresUserServicecheckpassword, Scenario1.VERSION1_0_0, userServiceInstance,
				userServiceType, Scenario1.VERSION1_0_0, userServicecheckpassword, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(permissionServiceInstance, permissionServiceType, Scenario1.VERSION1_0_0,
				permissionServiceRequiresAuthenticationgetlogin, Scenario1.VERSION1_0_0, authenticationInstance,
				authenticationType, Scenario1.VERSION1_0_0, authenticationgetlogin, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(permissionServiceInstance, permissionServiceType, Scenario1.VERSION1_0_0,
				permissionServiceRequiresUserServiceusers, Scenario1.VERSION1_0_0, userServiceInstance, userServiceType,
				Scenario1.VERSION1_0_0, userServiceusers, Scenario1.VERSION1_0_0);
		facadeInstance.linkClientServer(permissionServiceInstance, permissionServiceType, Scenario1.VERSION1_0_0,
				permissionServiceRequiresUserServiceusersgroups, Scenario1.VERSION1_0_0, userServiceInstance,
				userServiceType, Scenario1.VERSION1_0_0, userServiceusersGroups, Scenario1.VERSION1_0_0);
		// connect to database
		facadeInstance.connectMicroserviceToDatabaseSystem(userServiceInstance, userServiceType, Scenario1.VERSION1_0_0,
				usDBContract, Scenario1.VERSION1_0_0, dbUSInstance, dbUS, Scenario1.VERSION1_0_0);
		facadeInstance.connectMicroserviceToDatabaseSystem(fileServiceInstance, fileServiceType, Scenario1.VERSION1_0_0,
				fsDBContract, Scenario1.VERSION1_0_0, dbFSInstance, dbFS, Scenario1.VERSION1_0_0);
		facadeInstance.connectMicroserviceToDatabaseSystem(projectServiceInstance, projectServiceType,
				Scenario1.VERSION1_0_0, psDBContract, Scenario1.VERSION1_0_0, dbPSInstance, dbPS,
				Scenario1.VERSION1_0_0);
		facadeInstance.connectMicroserviceToDatabaseSystem(permissionServiceInstance, permissionServiceType,
				Scenario1.VERSION1_0_0, permDBContract, Scenario1.VERSION1_0_0, dbPERMInstance, dbPERM,
				Scenario1.VERSION1_0_0);
		// commit
		facadeInstance.commitConfiguration();

		var s2 = facadeInstance.toStringLastCommittedConfiguration();
		Log.SCENARIO.info("{}", () -> "\n====================\nConfiguration after instantiation\n" + s2);
		Log.SCENARIO.info("{}", () -> "Test True - microservice UserServiceInstance is attached to some contracts...");
		return facadeInstance.getIdentifierOfLastCommittedConfiguration();
	}
}
