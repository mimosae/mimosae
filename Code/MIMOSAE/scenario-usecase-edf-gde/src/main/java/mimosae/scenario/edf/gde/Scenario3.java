// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.scenario.edf.gde;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Level;

import mimosae.common.log.Log;
import mimosae.modelatruntime.MimosaeRuntimeModelInstance;
import mimosae.planner.pddl.Action;

public class Scenario3 {
	public static final String VERSION1_0_0 = "1.0.0";
	public static final String VERSION1_1_0 = "1.1.0";

	private Scenario3() {
		// empty
	}

	public static void main(final String[] args) throws Exception {
		Log.setLevel(Log.MODEL_AT_RUNTIME, Level.INFO);
		Log.setLevel(Log.PLANNER, Level.INFO);
		Log.setLevel(Log.SCENARIO, Level.INFO);
		Log.setLevel(Log.PROTOBUF_CLIENT_PROXY, Level.INFO);
		var emptyConfiguration = MimosaeRuntimeModelInstance.getMimosaeRuntimeModelInstance()
				.getIdentifierOfLastCommittedConfiguration();
		var initialGDEConfiguration = Step1CreateInitialGDEConfiguration.createInitialGDEConfiguration();
		var argList = new ArrayList<String>();
		if (args.length == 0) {
			argList.add("all");
		} else {
			argList = new ArrayList<>(Arrays.asList(args));
		}
		Log.SCENARIO.info("{}", () -> "______________" + Arrays.toString(args) + "_______________");
		List<List<Action>> plan = new ArrayList<>();
		boolean mustCreate = argList.contains("create") || argList.contains("all");
		boolean mustRemove = argList.contains("remove") || argList.contains("all");
		boolean mustReviseMinor = argList.contains("reviseminor") || argList.contains("all");
		if (mustCreate) {
			plan = Step2PlanDeploymentOfInitialGDEConfiguration
					.planDeploymentOfInitialGDEConfiguration(emptyConfiguration, initialGDEConfiguration);
		}
		if (mustReviseMinor) {
			var revisedGDEConfiguration = Step4ChangeMinorRevisionGDEConfigurationWithLogging
					.changeMinorRevisionGDEConfigurationWithLogging();
			plan = Step2PlanDeploymentOfInitialGDEConfiguration
					.planDeploymentOfInitialGDEConfiguration(initialGDEConfiguration, revisedGDEConfiguration);
		}
		if (mustRemove) {
			var revisedGDEConfiguration = Step4ChangeMinorRevisionGDEConfigurationWithLogging
					.changeMinorRevisionGDEConfigurationWithLogging();
			plan = Step2PlanDeploymentOfInitialGDEConfiguration
					.planDeploymentOfInitialGDEConfiguration(revisedGDEConfiguration, emptyConfiguration);
		}
		if (args.length != 0) {
			Step3ExecuteDeploymentOfInitialGDEConfiguration.executeDeploymentOfInitialGDEConfiguration(plan);
		}
	}
}
