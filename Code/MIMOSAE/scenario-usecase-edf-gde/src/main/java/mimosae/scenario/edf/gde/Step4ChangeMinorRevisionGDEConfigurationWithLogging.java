// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Yuwei Wang
Contributor(s): Denis Conan
 */
package mimosae.scenario.edf.gde;

import java.util.Arrays;
import java.util.Objects;

import mimosae.common.log.Log;
import mimosae.modelatruntime.MimosaeRuntimeModelInstance;
import mimosae.modelatruntime.MimosaeRuntimeModelType;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.exception.AlreadyExistsModelElementException;
import mimosae.modelatruntime.exception.ConnectedMicroserviceInConfigurationException;
import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.exception.IsNotDeployableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.IsNotInstantiableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.MalformedSubscriptionFilterException;
import mimosae.modelatruntime.exception.UnknownInstanceException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.exception.UnmatchingTypesException;
import mimosae.modelatruntime.exception.WrongNumberOfIdentifiersException;

public final class Step4ChangeMinorRevisionGDEConfigurationWithLogging {

	private Step4ChangeMinorRevisionGDEConfigurationWithLogging() {
		// no instance
	}

	public static String changeMinorRevisionGDEConfigurationWithLogging()
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			UnknownTypeException, UnknownInstanceException, UnmatchingTypesException, AlreadyConnectedException,
			IsCommittedAndCannotBeModifiedException, IsNotInstantiableAndCannotBeCommittedException,
			IsNotDeployableAndCannotBeCommittedException, MalformedSubscriptionFilterException,
			ConnectedMicroserviceInConfigurationException {
		// Facades
		// facade for types
		var facadeType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		// facade for instances
		var facadeInstance = MimosaeRuntimeModelInstance.getMimosaeRuntimeModelInstance();
		// Already existing contracts
		var fsDBContract = "FSDBContract";
		var fileServiceRequiresAuthenticationgetlogin = "FSrAgetlogin";
		var fileServiceRequiresPermissionServicecheckpermission = "FSrPermScheck";
		var fileServiceRequiresUserServiceusers = "FSrUSusers";
		var fileServicefiles = "FSfiles";
		var fileServicefilelock = "FSfilelock";
		var fileServicechunks = "FSchunks";
		var fileServicefilechunks = "FSfilechunks";
		var fsClientContract = new String[] { fileServiceRequiresAuthenticationgetlogin, Scenario3.VERSION1_0_0,
				fileServiceRequiresPermissionServicecheckpermission, Scenario3.VERSION1_0_0,
				fileServiceRequiresUserServiceusers, Scenario3.VERSION1_0_0 };
		var fsServerContract = new String[] { fileServicefiles, Scenario3.VERSION1_0_0, fileServicefilelock,
				Scenario3.VERSION1_0_0, fileServicechunks, Scenario3.VERSION1_0_0, fileServicefilechunks,
				Scenario3.VERSION1_0_0 };
		var projectServiceRequiresFileServicefiles = "PSrFSfiles";
		var authenticationgetlogin = "Agetlogin";
		var permissionServicecheckpermission = "PermScheck";
		var userServiceusers = "USusers";
		// Producer contract types
		var fileServiceLog = "FSLog";
		var fileServiceLogPubFilter = "FSLogPubFilter";
		var fileServiceLogRoutingKey = "logger.fileservice.log";
		var logEventType = "LogEventType";
		facadeType.createAMQPTopicBasedProducerContractType(fileServiceLog, Scenario3.VERSION1_1_0,
				fileServiceLogPubFilter, Scenario3.VERSION1_1_0, fileServiceLogRoutingKey, logEventType,
				Scenario3.VERSION1_1_0);
		var fsProducerContract = new String[] { fileServiceLog, Scenario3.VERSION1_1_0 };
		// Consumer contract types
		var loggerServiceLog = "LSLog";
		var loggerServiceLogConsFilter = "FSLogConsFilter";
		var loggerServiceLogBindingKey = "logger.#";
		facadeType.createAMQPTopicBasedConsumerContractType(loggerServiceLog, Scenario3.VERSION1_0_0,
				loggerServiceLogConsFilter, Scenario3.VERSION1_0_0, loggerServiceLogBindingKey, logEventType,
				Scenario3.VERSION1_0_0);
		var lsConsumerContract = new String[] { loggerServiceLog, Scenario3.VERSION1_0_0 };
		// join client, server, producer, and consumer contract types
		String[] fsDBC = new String[] { fsDBContract, Scenario3.VERSION1_0_0 };
		String[] fsContracts = Arrays.copyOf(fsClientContract,
				fsClientContract.length + fsServerContract.length + fsProducerContract.length + fsDBC.length);
		System.arraycopy(fsServerContract, 0, fsContracts, fsClientContract.length, fsServerContract.length);
		System.arraycopy(fsProducerContract, 0, fsContracts, fsClientContract.length + fsServerContract.length,
				fsProducerContract.length);
		System.arraycopy(fsDBC, 0, fsContracts,
				fsClientContract.length + fsServerContract.length + fsProducerContract.length, fsDBC.length);
		String[] logContracts = Arrays.copyOf(lsConsumerContract, lsConsumerContract.length);
		// Microservice type
		var fileServiceType = "FS";
		var loggerServiceType = "LS";
		facadeType.createMicroserviceType(fileServiceType, Scenario3.VERSION1_1_0, fsContracts);
		facadeType.createMicroserviceType(loggerServiceType, Scenario3.VERSION1_0_0, logContracts);
		// publish-subscribe connector types
		var pubSubConnectorType = "PubSubConnector";
		facadeType.createPubSubConnectorType(pubSubConnectorType, Scenario3.VERSION1_0_0);
		facadeType.addProducerContractTypeToPubSubConnectorType(fileServiceLog, Scenario3.VERSION1_1_0,
				pubSubConnectorType, Scenario3.VERSION1_0_0);
		facadeType.addConsumerContractTypeToPubSubConnectorType(loggerServiceLog, Scenario3.VERSION1_0_0,
				pubSubConnectorType, Scenario3.VERSION1_0_0);
		// Commit configuration type
		facadeType.commitConfigurationType();
		var s1 = facadeType.toStringConfigurationType(facadeType.getIdentifierOfLastCommittedConfigurationType());
		Log.SCENARIO.info("{}", () -> "\n====================\nConfiguration type for the revision \n" + s1);
		Objects.requireNonNull(facadeInstance, "facade instance cannot be null");
		facadeInstance
				.setConfigurationTypeOfCurrentConfiguration(facadeType.getIdentifierOfLastCommittedConfigurationType());
		// Change configuration type of current configuration
		facadeInstance.changeConfigurationTypeOfCurrentConfiguration(
				facadeType.getIdentifierOfLastCommittedConfigurationType());
		// Instances
		// microservice instances
		var fileServiceInstance = "FSi";
		var loggerServiceInstance = "LogSi";
		facadeInstance.createMicroservice(fileServiceInstance, fileServiceType, Scenario3.VERSION1_1_0);
		facadeInstance.createMicroservice(loggerServiceInstance, loggerServiceType, Scenario3.VERSION1_0_0);
		// publish-subscribe connectors
		var pubSubConnector = "PubSubCi";
		facadeInstance.createPubSubConnector(pubSubConnector, pubSubConnectorType, Scenario3.VERSION1_0_0);
		// Links
		// client-server links
		var projectServiceType = "PS";
		var projectServiceInstance = "PSi";
		var authenticationType = "Auth";
		var authenticationInstance = "Ai";
		var permissionServiceType = "PermS";
		var permissionServiceInstance = "PermSi";
		var userServiceType = "US";
		var userServiceInstance = "USi";
		var dbFS = "DBFS";
		var dbFSInstance = "DBFSi";
		// disconnect database connections
		facadeInstance.disconnectMicroserviceToDatabaseSystem(fileServiceInstance, fileServiceType,
				Scenario3.VERSION1_0_0, fsDBContract, Scenario3.VERSION1_0_0, dbFSInstance, dbFS,
				Scenario3.VERSION1_0_0);
		// unlink
		facadeInstance.unlinkClientServer(projectServiceInstance, projectServiceType, Scenario3.VERSION1_0_0,
				projectServiceRequiresFileServicefiles, Scenario3.VERSION1_0_0, fileServiceInstance, fileServiceType,
				Scenario3.VERSION1_0_0, fileServicefiles, Scenario3.VERSION1_0_0);
		facadeInstance.unlinkClientServer(fileServiceInstance, fileServiceType, Scenario3.VERSION1_0_0,
				fileServiceRequiresAuthenticationgetlogin, Scenario3.VERSION1_0_0, authenticationInstance,
				authenticationType, Scenario3.VERSION1_0_0, authenticationgetlogin, Scenario3.VERSION1_0_0);
		facadeInstance.unlinkClientServer(fileServiceInstance, fileServiceType, Scenario3.VERSION1_0_0,
				fileServiceRequiresPermissionServicecheckpermission, Scenario3.VERSION1_0_0, permissionServiceInstance,
				permissionServiceType, Scenario3.VERSION1_0_0, permissionServicecheckpermission,
				Scenario3.VERSION1_0_0);
		facadeInstance.unlinkClientServer(fileServiceInstance, fileServiceType, Scenario3.VERSION1_0_0,
				fileServiceRequiresUserServiceusers, Scenario3.VERSION1_0_0, userServiceInstance, userServiceType,
				Scenario3.VERSION1_0_0, userServiceusers, Scenario3.VERSION1_0_0);
		// new links
		facadeInstance.linkClientServer(projectServiceInstance, projectServiceType, Scenario3.VERSION1_0_0,
				projectServiceRequiresFileServicefiles, Scenario3.VERSION1_0_0, fileServiceInstance, fileServiceType,
				Scenario3.VERSION1_1_0, fileServicefiles, Scenario3.VERSION1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileServiceType, Scenario3.VERSION1_1_0,
				fileServiceRequiresAuthenticationgetlogin, Scenario3.VERSION1_0_0, authenticationInstance,
				authenticationType, Scenario3.VERSION1_0_0, authenticationgetlogin, Scenario3.VERSION1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileServiceType, Scenario3.VERSION1_1_0,
				fileServiceRequiresPermissionServicecheckpermission, Scenario3.VERSION1_0_0, permissionServiceInstance,
				permissionServiceType, Scenario3.VERSION1_0_0, permissionServicecheckpermission,
				Scenario3.VERSION1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileServiceType, Scenario3.VERSION1_1_0,
				fileServiceRequiresUserServiceusers, Scenario3.VERSION1_0_0, userServiceInstance, userServiceType,
				Scenario3.VERSION1_0_0, userServiceusers, Scenario3.VERSION1_0_0);
		// database connections
		facadeInstance.connectMicroserviceToDatabaseSystem(fileServiceInstance, fileServiceType, Scenario3.VERSION1_1_0,
				fsDBContract, Scenario3.VERSION1_0_0, dbFSInstance, dbFS, Scenario3.VERSION1_0_0);
		// links to publish-subscribe connectors
		facadeInstance.linkProducerToConnector(fileServiceInstance, fileServiceType, Scenario3.VERSION1_1_0,
				fileServiceLog, Scenario3.VERSION1_1_0, pubSubConnector);
		facadeInstance.linkConsumerToConnector(loggerServiceInstance, loggerServiceType, Scenario3.VERSION1_0_0,
				loggerServiceLog, Scenario3.VERSION1_0_0, pubSubConnector);
		// remove microservice instance with previous version
		facadeInstance.removeMicroservice(fileServiceInstance, fileServiceType, Scenario3.VERSION1_0_0);
		// commit configuration
		facadeInstance.commitConfiguration();

		var s2 = facadeInstance.toStringLastCommittedConfiguration();
		Log.SCENARIO.info("{}", () -> "\n====================\nConfiguration after revision\n" + s2);
		return facadeInstance.getIdentifierOfLastCommittedConfiguration();
	}
}
