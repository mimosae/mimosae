// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Yuwei Wang
Contributor(s): Denis Conan
 */
package mimosae.scenario.edf.gde;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import mimosae.common.log.Log;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.UnknownInstanceException;
import mimosae.planner.MimosaePlanner;
import mimosae.planner.pddl.Action;

public final class Step2PlanDeploymentOfInitialGDEConfiguration {

	private Step2PlanDeploymentOfInitialGDEConfiguration() {
		// no instance
	}

	public static List<List<Action>> planDeploymentOfInitialGDEConfiguration(final String sourceConfiguration,
			final String targetConfiguration)
			throws MalformedIdentifierException, UnknownInstanceException, IOException {
		// facade for the planner
		var facadePlanner = MimosaePlanner.getMimosaeRuntimeModelInstance();
		// compute plan
		var plan = facadePlanner.computeAIPlanToReconfigureWithLPG(sourceConfiguration, targetConfiguration);
		if (Log.ON) {
			Log.SCENARIO.info("{}",
					() -> "\n====================\nConfiguration plan for transiting from " + sourceConfiguration
							+ " to " + targetConfiguration + "\n"
							+ plan.stream()
									.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
											.collect(Collectors.joining("\n\t", "\n\t", "")))
									.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		}
		return plan;
	}
}
