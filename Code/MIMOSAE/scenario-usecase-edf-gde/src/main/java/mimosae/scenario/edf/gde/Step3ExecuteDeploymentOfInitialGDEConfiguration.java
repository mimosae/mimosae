// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Yuwei Wang
Contributor(s): Denis Conan
 */
package mimosae.scenario.edf.gde;

import java.util.List;

import io.grpc.ManagedChannelBuilder;
import mimosae.executor.clientproxy.Client;
import mimosae.planner.pddl.Action;

public final class Step3ExecuteDeploymentOfInitialGDEConfiguration {

	private Step3ExecuteDeploymentOfInitialGDEConfiguration() {
		// no instance
	}

	public static void executeDeploymentOfInitialGDEConfiguration(final List<List<Action>> plan) {
		Client client = new Client(ManagedChannelBuilder.forAddress("localhost", 4040).usePlaintext().build());
		client.executeConfigurationPlan(plan);
	}
}
