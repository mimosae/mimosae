package mimosae.executor.clientproxy;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import external.CommandGrpc;
import external.CommandGrpc.CommandBlockingStub;

import external.PlannificationProto;
import external.PlannificationProto.ConfigurationPlan;
import io.grpc.Channel;
import io.grpc.StatusRuntimeException;
import mimosae.common.log.Log;
import mimosae.planner.pddl.Action;

/**
 * This client class is used as a client-proxy to send messages to the
 * server(Executor) It uses <a href="https://grpc.io/">gRPC</a> to communicate.
 */
public class Client {

	/**
	 * the stub to send request messages. See {@link CommandBlockingStub} for more
	 * information.
	 */
	private final CommandBlockingStub commandBlockingStub;

	/**
	 * constructs a new Client. It initializes the stub.
	 * 
	 * @param channel the channel for the stub.
	 */
	public Client(final Channel channel) {
		commandBlockingStub = CommandGrpc.newBlockingStub(channel);
	}

	/**
	 * takes a plan representation in the output of planner, parses it and sends it
	 * to the executor.
	 * 
	 * @param planAI a plan of actions to execute.
	 * @see Action in order to understand what is planner's action.
	 * @see external.PlannificationProto.ConfigurationPlan.Builder in order to
	 *      understand how protobuf plan is constructed.
	 */
	public void executeConfigurationPlan(final Iterable<? extends Iterable<? extends Action>> planAI) {
		ConfigurationPlan.Builder configurationPlanBuilder = ConfigurationPlan.newBuilder();
		Log.PROTOBUF_CLIENT_PROXY.info("{}", () -> "______________Starting serialization of plan______________");
		planAI.forEach(parallelActions -> {
			ConfigurationPlan.ConcurrentActions.Builder concurrentActionsBuilder = ConfigurationPlan.ConcurrentActions
					.newBuilder();
			parallelActions.forEach(action -> {
				PlannificationProto.Action planAction = pddlActionAsPlanAction(action);
				concurrentActionsBuilder.addAction(planAction);
			});
			concurrentActionsBuilder.setID(parallelActions.hashCode());
			configurationPlanBuilder.addConcurrentActions(concurrentActionsBuilder);
		});
		ConfigurationPlan configurationPlan = configurationPlanBuilder.build();
		Log.PROTOBUF_CLIENT_PROXY.info("{}", () -> "______________Plan has been serialized:______________");
		Log.PROTOBUF_CLIENT_PROXY.info("{}", configurationPlan::toString);
		try {
			PlannificationProto.Response response = commandBlockingStub
					.executeConfigurationPlan(configurationPlanBuilder.build());
			Log.PROTOBUF_CLIENT_PROXY.info("{}", () -> "Executor response:\n " + response);
		} catch (StatusRuntimeException ex) {
			Log.PROTOBUF_CLIENT_PROXY.info("{}", () -> "gRPC failed: " + ex.getStatus());
		}
	}

	/**
	 * serializes a planner's action depending on it type (microservice, database
	 * system, publish-subscribe connector, etc.).
	 * 
	 * @param action planner's action to serialize.
	 * @return serialized action, which is a protobuf representation of planner's
	 *         action.
	 */
	private PlannificationProto.Action pddlActionAsPlanAction(final Action action) {
		PlannificationProto.Action.Builder planActionBuilder = PlannificationProto.Action.newBuilder();
		planActionBuilder.setID(action.hashCode());
		switch (action.getActionName()) {
		case CREATE_MICROSERVICE:
			assert action.getArguments().length == 1;
			PlannificationProto.Microservice microservice = microserviceFromDescription(action.getArguments()[0],
					PlannificationProto.Operation.CREATE);
			planActionBuilder.setMicroservice(microservice);
			break;
		case REMOVE_MICROSERVICE:
			assert action.getArguments().length == 1;
			microservice = microserviceFromDescription(action.getArguments()[0], PlannificationProto.Operation.DELETE);
			planActionBuilder.setMicroservice(microservice);
			break;
		case CREATE_DATABASESYSTEM:
			assert action.getArguments().length == 1;
			PlannificationProto.DatabaseSystem databaseSystem = databaseSystemFromDescription(action.getArguments()[0],
					PlannificationProto.Operation.CREATE);
			planActionBuilder.setDatabaseSystem(databaseSystem);
			break;
		case REMOVE_DATABASESYSTEM:
			assert action.getArguments().length == 1;
			databaseSystem = databaseSystemFromDescription(action.getArguments()[0],
					PlannificationProto.Operation.DELETE);
			planActionBuilder.setDatabaseSystem(databaseSystem);
			break;
		case CREATE_PUBSUBCONNECTOR:
			assert action.getArguments().length == 1;
			PlannificationProto.PubSubConnector pubSubConnector = pubSubConnectorFromDescription(
					action.getArguments()[0], PlannificationProto.Operation.CREATE);
			planActionBuilder.setPubSubConnector(pubSubConnector);
			break;
		case REMOVE_PUBSUBCONNECTOR:
			assert action.getArguments().length == 1;
			pubSubConnector = pubSubConnectorFromDescription(action.getArguments()[0],
					PlannificationProto.Operation.DELETE);
			planActionBuilder.setPubSubConnector(pubSubConnector);
			break;
		case CONNECT_MICROSERVICE_TO_DATABASESYSTEM:
			assert action.getArguments().length == 2;
			PlannificationProto.DatabaseConnector databaseConnector = databaseConnectorFromArguments(
					action.getArguments(), PlannificationProto.Operation.CONNECT);
			planActionBuilder.setDatabaseConnector(databaseConnector);
			break;
		case DISCONNECT_MICROSERVICE_TO_DATABASESYSTEM:
			assert action.getArguments().length == 2;
			databaseConnector = databaseConnectorFromArguments(action.getArguments(),
					PlannificationProto.Operation.DISCONNECT);
			planActionBuilder.setDatabaseConnector(databaseConnector);
			break;
		case LINK_CLIENT_SERVER:
			assert action.getArguments().length == 2;
			PlannificationProto.ClientServerContract clientServerContract = clientServerConnectorFromArguments(
					action.getArguments(), PlannificationProto.Operation.LINK);
			planActionBuilder.setClientServerContract(clientServerContract);
			break;
		case UNLINK_CLIENT_SERVER:
			assert action.getArguments().length == 2;
			clientServerContract = clientServerConnectorFromArguments(action.getArguments(),
					PlannificationProto.Operation.UNLINK);
			planActionBuilder.setClientServerContract(clientServerContract);
			break;
		case LINK_PRODUCER_TO_PUBSUBCONNECTOR:
			assert action.getArguments().length == 2;
			PlannificationProto.ProduceContract produceContract = producerLinkFromArguments(action.getArguments(),
					PlannificationProto.Operation.LINK);
			planActionBuilder.setProduceContract(produceContract);
			break;
		case UNLINK_PRODUCER_FROM_PUBSUBCONNECTOR:
			assert action.getArguments().length == 2;
			produceContract = producerLinkFromArguments(action.getArguments(), PlannificationProto.Operation.UNLINK);
			planActionBuilder.setProduceContract(produceContract);
			break;
		case LINK_CONSUMER_TO_PUBSUBCONNECTOR:
			assert action.getArguments().length == 2;
			PlannificationProto.ConsumeContract consumeContract = consumeContractFromArguments(action.getArguments(),
					PlannificationProto.Operation.LINK);
			planActionBuilder.setConsumeContract(consumeContract);
			break;
		case UNLINK_CONSUMER_FROM_PUBSUBCONNECTOR:
			assert action.getArguments().length == 2;
			consumeContract = consumeContractFromArguments(action.getArguments(), PlannificationProto.Operation.UNLINK);
			planActionBuilder.setConsumeContract(consumeContract);
			break;
		default:
			Log.PROTOBUF_CLIENT_PROXY.error("{}", () -> "unknown action name (" + action.getActionName() + ")");
		}
		return planActionBuilder.build();
	}

	/**
	 * is a helper method to serialize the description of a microservice.
	 * 
	 * @param description microservice's description.
	 * @param operation   operation to perform on the microservice.
	 * @return proto the protobuf request for the microservice.
	 */
	private PlannificationProto.Microservice microserviceFromDescription(final String description,
			final PlannificationProto.Operation operation) {
		PlannificationProto.Microservice.Builder microserviceBuilder = PlannificationProto.Microservice.newBuilder();
		PlannificationProto.Meta meta = metaFromDescription(description);
		microserviceBuilder.setID(description.hashCode());
		microserviceBuilder.setMeta(meta);
		microserviceBuilder.setOperation(operation);
		return microserviceBuilder.build();
	}

	/**
	 * is a helper method to serialize a database system.
	 * 
	 * @param description database's description.
	 * @param operation   operation to perform on the database system.
	 * @return proto the protobuf request for the database system.
	 */
	private PlannificationProto.DatabaseSystem databaseSystemFromDescription(final String description,
			final PlannificationProto.Operation operation) {
		PlannificationProto.DatabaseSystem.Builder databaseBuilder = PlannificationProto.DatabaseSystem.newBuilder();
		PlannificationProto.Meta meta = metaFromDescription(description);
		databaseBuilder.setID(description.hashCode());
		databaseBuilder.setMeta(meta);
		databaseBuilder.setOperation(operation);
		return databaseBuilder.build();
	}

	/**
	 * is a helper method to serialize a publish subscribe connector.
	 * 
	 * @param description connector's description.
	 * @param operation   operation that we would like to perform on the connector.
	 * @return proto the protobuf request for the publish-subscribe connector.
	 */
	private PlannificationProto.PubSubConnector pubSubConnectorFromDescription(final String description,
			final PlannificationProto.Operation operation) {
		PlannificationProto.PubSubConnector.Builder pubSubConnectorBuilder = PlannificationProto.PubSubConnector
				.newBuilder();
		PlannificationProto.Meta meta = metaFromDescription(description);
		pubSubConnectorBuilder.setID(description.hashCode());
		pubSubConnectorBuilder.setMeta(meta);
		pubSubConnectorBuilder.setOperation(operation);
		return pubSubConnectorBuilder.build();
	}

	/**
	 * is a helper method to serialize a database connector.
	 * 
	 * @param arguments arguments provided with database connector (client
	 *                  identifier and database identifier).
	 * @param operation operation that we would like to perform on the database
	 *                  connector.
	 * @return proto the protobuf request for the database connector.
	 */
	private PlannificationProto.DatabaseConnector databaseConnectorFromArguments(final String[] arguments,
			final PlannificationProto.Operation operation) {
		PlannificationProto.DatabaseConnector.Builder databaseConnectorBuilder = PlannificationProto.DatabaseConnector
				.newBuilder();
		PlannificationProto.Meta meta = metaFromDescription(arguments[1]);
		databaseConnectorBuilder.setID(arguments[0].concat(arguments[1]).concat("_database").hashCode());
		databaseConnectorBuilder.setMeta(meta);
		databaseConnectorBuilder.setOperation(operation);
		databaseConnectorBuilder.setClientID(arguments[0].hashCode());
		databaseConnectorBuilder.setDatabaseID(arguments[1].hashCode());
		return databaseConnectorBuilder.build();
	}

	/**
	 * is a helper method to serialize a client server connector.
	 * 
	 * @param arguments arguments provided with client server contract (client
	 *                  identifier and server identifier).
	 * @param operation operation that we would like to perform on the client server
	 *                  connector.
	 * @return proto the protobuf request for the client-server connector.
	 */
	private PlannificationProto.ClientServerContract clientServerConnectorFromArguments(final String[] arguments,
			final PlannificationProto.Operation operation) {
		PlannificationProto.ClientServerContract.Builder clientServerContractBuilder = PlannificationProto.ClientServerContract
				.newBuilder();
		PlannificationProto.Meta meta = metaFromDescription(arguments);
		clientServerContractBuilder.setID(arguments[0].concat(arguments[1]).concat("_client_server").hashCode());
		clientServerContractBuilder.setMeta(meta);
		clientServerContractBuilder.setOperation(operation);
		clientServerContractBuilder.setClientID(arguments[0].hashCode());
		clientServerContractBuilder.setServerID(arguments[1].hashCode());
		return clientServerContractBuilder.build();
	}

	/**
	 * is a helper method to serialize a producer link to a publish subscribe
	 * connector.
	 * 
	 * @param arguments arguments provided with producer link (producer identifier
	 *                  and connector identifier).
	 * @param operation operation to perform to link the producer.
	 * @return proto the protobuf request for producer to connector link.
	 */
	private PlannificationProto.ProduceContract producerLinkFromArguments(final String[] arguments,
			final PlannificationProto.Operation operation) {
		PlannificationProto.ProduceContract.Builder produceContractBuilder = PlannificationProto.ProduceContract
				.newBuilder();
		PlannificationProto.Meta meta = metaFromDescription(arguments);
		produceContractBuilder.setID(arguments[0].concat(arguments[1]).concat("_produce").hashCode());
		produceContractBuilder.setMeta(meta);
		produceContractBuilder.setOperation(operation);
		produceContractBuilder.setProducerID(arguments[0].hashCode());
		produceContractBuilder.setConnectorID(arguments[1].hashCode());
		return produceContractBuilder.build();
	}

	/**
	 * is a helper method to serialize a consumer link to a publish-subscribe
	 * connector.
	 * 
	 * @param arguments arguments provided with consumer link (consumer identifier
	 *                  and connector identifier).
	 * @param operation operation that to perform to the link the consumer.
	 * @return proto the protobuf request for consumer to connector link.
	 */
	private PlannificationProto.ConsumeContract consumeContractFromArguments(final String[] arguments,
			final PlannificationProto.Operation operation) {
		PlannificationProto.ConsumeContract.Builder consumeContractBuilder = PlannificationProto.ConsumeContract
				.newBuilder();
		PlannificationProto.Meta meta = metaFromDescription(arguments);
		consumeContractBuilder.setID(arguments[0].concat(arguments[1]).concat("_consume").hashCode());
		consumeContractBuilder.setMeta(meta);
		consumeContractBuilder.setOperation(operation);
		consumeContractBuilder.setConsumerID(arguments[0].hashCode());
		consumeContractBuilder.setConnectorID(arguments[1].hashCode());
		return consumeContractBuilder.build();
	}

	/**
	 * metaFromDescription helps us to create from any number of descriptors For
	 * instance if we have more than 1 descriptor, we omit meta(connector case) When
	 * there is only one descriptor, we extract meta from descriptor and return it.
	 * 
	 * @param descriptors descriptors are used to create the meta-data.
	 * @return the meta-data.
	 */
	private PlannificationProto.Meta metaFromDescription(final String... descriptors) {
		PlannificationProto.Meta.Builder metaBuilder = PlannificationProto.Meta.newBuilder();
		if (descriptors.length >= 2) {
			metaBuilder.setName("empty_meta");
			metaBuilder.setType("empty_meta");
			metaBuilder.setVersion("0.0.0");
		} else {
			final int size = 3;
			List<String> arguments = Arrays.stream(descriptors[0].split("_", size)).collect(Collectors.toList());
			assert arguments.size() == size;
			metaBuilder.setName(arguments.get(0));
			metaBuilder.setType(arguments.get(1));
			metaBuilder.setVersion(arguments.get(2).replace("_", "."));
		}
		return metaBuilder.build();
	}
}
