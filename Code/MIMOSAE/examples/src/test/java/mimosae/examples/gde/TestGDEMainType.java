// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Yuwei Wang
Contributor(s): Denis Conan
 */
package mimosae.examples.gde;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mimosae.common.log.Log;
import mimosae.examples.UtilGDEForTest;
import mimosae.modelatruntime.MimosaeRuntimeModelType;

public final class TestGDEMainType {

	private MimosaeRuntimeModelType facadeType;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void teardown() {
		facadeType = null;
	}

	@Test
	public void testGDEConfiguration() throws Exception {
		facadeType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		facadeType.clearAllConfigurationTypes();
		// create types
		UtilGDEForTest.createGDEConfigurationType(facadeType);
		// tests
		String s1 = facadeType.toStringConfigurationType(facadeType.getIdentifierOfLastCommittedConfigurationType());
		Log.SCENARIO.info("{}", () -> s1);
		Log.SCENARIO.info("{}", () -> "1.Test True - Check GDEConfigTypev.0.0.1 is runnable...");
		assertTrue(facadeType.checkConfigurationTypeIsInstantiable(facadeType.getIdentifierOfLastCommittedConfigurationType()));
		Log.SCENARIO.info("{}",
				() -> "2.Test True - Set \"require /gde/Authentication/get_login\", \"0.0.1\" must be connected, then check GDEConfigTypev.0.0.1 is runnable...");
		facadeType.setClientContractTypeMustBeConnected("UserService_requires_/gde/Authentication/get_login", "0.0.1",
				true);
		assertTrue(facadeType.checkConfigurationTypeIsInstantiable(facadeType.getIdentifierOfLastCommittedConfigurationType()));
	}
}
