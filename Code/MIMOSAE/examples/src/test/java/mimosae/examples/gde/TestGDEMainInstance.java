// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Yuwei Wang
Contributor(s): Denis Conan
 */
package mimosae.examples.gde;

import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import mimosae.common.log.Log;
import mimosae.examples.UtilGDEForTest;
import mimosae.modelatruntime.MimosaeRuntimeModelInstance;
import mimosae.modelatruntime.MimosaeRuntimeModelType;
import mimosae.planner.MimosaePlanner;
import mimosae.planner.pddl.Action;

public class TestGDEMainInstance {

	private MimosaeRuntimeModelType facadeType;
	private MimosaeRuntimeModelInstance facadeInstance;
	private MimosaePlanner facadePlanner;

	@Before
	public void setUp() throws Exception {
		Log.setLevel(Log.SCENARIO, Level.WARN);
		Log.setLevel(Log.PLANNER, Level.WARN);
		facadeType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		facadeInstance = MimosaeRuntimeModelInstance.getMimosaeRuntimeModelInstance();
		facadePlanner = MimosaePlanner.getMimosaeRuntimeModelInstance();
		facadeInstance.clearAllConfigurations();
		facadeType.clearAllConfigurationTypes();
	}

	@After
	public void teardown() {
		facadeType = null;
		facadeInstance = null;
		facadePlanner = null;
	}

	@Test
	public void testGDEConfigurationInstance() throws Exception {
		// create types
		UtilGDEForTest.createGDEConfigurationType(facadeType);
		facadeInstance.setConfigurationTypeOfCurrentConfiguration(
				MimosaeRuntimeModelType.getMimosaeRuntimeModelType().getIdentifierOfLastCommittedConfigurationType());
		String firstConfiguration = facadeInstance.getIdentifierOfLastCommittedConfiguration();
		// microservice instance
		facadeInstance.createMicroservice("UserServiceInstance", "UserService", "0.0.1");
		facadeInstance.createMicroservice("ProjectServiceInstance", "ProjectService", "0.0.1");
		facadeInstance.createMicroservice("FileServiceInstance", "FileService", "0.0.1");
		facadeInstance.createMicroservice("AuthenticationInstance", "Authentication", "0.0.1");
		facadeInstance.createMicroservice("PermissionServiceInstance", "PermissionService", "0.0.1");
		// link
		facadeInstance.linkClientServer("UserServiceInstance", "UserService", "0.0.1",
				"UserService_requires_/gde/Authentication/get_login", "0.0.1", "AuthenticationInstance",
				"Authentication", "0.0.1", "/gde/Authentication/get_login", "0.0.1");
		facadeInstance.linkClientServer("UserServiceInstance", "UserService", "0.0.1",
				"UserService_requires_/gde/PermissionService/check_permission", "0.0.1", "PermissionServiceInstance",
				"PermissionService", "0.0.1", "/gde/PermissionService/check_permission", "0.0.1");
		facadeInstance.linkClientServer("ProjectServiceInstance", "ProjectService", "0.0.1",
				"ProjectService_requires_/gde/Authentication/get_login", "0.0.1", "AuthenticationInstance",
				"Authentication", "0.0.1", "/gde/Authentication/get_login", "0.0.1");
		facadeInstance.linkClientServer("ProjectServiceInstance", "ProjectService", "0.0.1",
				"ProjectService_requires_/gde/PermissionService/check_permission", "0.0.1", "PermissionServiceInstance",
				"PermissionService", "0.0.1", "/gde/PermissionService/check_permission", "0.0.1");
		facadeInstance.linkClientServer("ProjectServiceInstance", "ProjectService", "0.0.1",
				"ProjectService_requires_/gde/FileService/files", "0.0.1", "FileServiceInstance", "FileService",
				"0.0.1", "/gde/FileService/files", "0.0.1");
		facadeInstance.linkClientServer("ProjectServiceInstance", "ProjectService", "0.0.1",
				"ProjectService_requires_/gde/UserService/users", "0.0.1", "UserServiceInstance", "UserService",
				"0.0.1", "/gde/UserService/users", "0.0.1");
		facadeInstance.linkClientServer("FileServiceInstance", "FileService", "0.0.1",
				"FileService_requires_/gde/Authentication/get_login", "0.0.1", "AuthenticationInstance",
				"Authentication", "0.0.1", "/gde/Authentication/get_login", "0.0.1");
		facadeInstance.linkClientServer("FileServiceInstance", "FileService", "0.0.1",
				"FileService_requires_/gde/PermissionService/check_permission", "0.0.1", "PermissionServiceInstance",
				"PermissionService", "0.0.1", "/gde/PermissionService/check_permission", "0.0.1");
		facadeInstance.linkClientServer("FileServiceInstance", "FileService", "0.0.1",
				"FileService_requires_/gde/UserService/users", "0.0.1", "UserServiceInstance", "UserService", "0.0.1",
				"/gde/UserService/users", "0.0.1");
		facadeInstance.linkClientServer("AuthenticationInstance", "Authentication", "0.0.1",
				"Authentication_requires_/gde/UserService/check_password", "0.0.1", "UserServiceInstance",
				"UserService", "0.0.1", "/gde/UserService/check_password", "0.0.1");
		facadeInstance.linkClientServer("PermissionServiceInstance", "PermissionService", "0.0.1",
				"PermissionService_requires_/gde/Authentication/get_login", "0.0.1", "AuthenticationInstance",
				"Authentication", "0.0.1", "/gde/Authentication/get_login", "0.0.1");
		facadeInstance.linkClientServer("PermissionServiceInstance", "PermissionService", "0.0.1",
				"PermissionService_requires_/gde/UserService/users", "0.0.1", "UserServiceInstance", "UserService",
				"0.0.1", "/gde/UserService/users", "0.0.1");
		facadeInstance.linkClientServer("PermissionServiceInstance", "PermissionService", "0.0.1",
				"PermissionService_requires_/gde/UserService/users_groups", "0.0.1", "UserServiceInstance",
				"UserService", "0.0.1", "/gde/UserService/users_groups", "0.0.1");
		// test
		String s1 = facadeInstance.toStringCurrentConfiguration();
		Log.SCENARIO.info("{}", () -> "configuration after instantiation\n" + s1);
		Log.SCENARIO.info("{}",
				() -> "1. Test True - microservice UserServiceInstance is attached to some contracts...");
		Assert.assertTrue(
				facadeInstance.isMicroserviceConnectedToOthers("UserServiceInstance", "UserService", "0.0.1"));
		facadeInstance.commitConfiguration();
		String secondConfiguration = facadeInstance.getIdentifierOfLastCommittedConfiguration();
		var firstAIPlan = facadePlanner.computeAIPlanToReconfigureWithLPG(firstConfiguration, secondConfiguration);
		Log.SCENARIO.info("{}",
				() -> firstAIPlan.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		Assert.assertFalse(firstAIPlan.isEmpty());

		// unlink
		facadeInstance.unlinkClientServer("UserServiceInstance", "UserService", "0.0.1",
				"UserService_requires_/gde/Authentication/get_login", "0.0.1", "AuthenticationInstance",
				"Authentication", "0.0.1", "/gde/Authentication/get_login", "0.0.1");
		facadeInstance.unlinkClientServer("UserServiceInstance", "UserService", "0.0.1",
				"UserService_requires_/gde/PermissionService/check_permission", "0.0.1", "PermissionServiceInstance",
				"PermissionService", "0.0.1", "/gde/PermissionService/check_permission", "0.0.1");
		facadeInstance.unlinkClientServer("ProjectServiceInstance", "ProjectService", "0.0.1",
				"ProjectService_requires_/gde/Authentication/get_login", "0.0.1", "AuthenticationInstance",
				"Authentication", "0.0.1", "/gde/Authentication/get_login", "0.0.1");
		facadeInstance.unlinkClientServer("ProjectServiceInstance", "ProjectService", "0.0.1",
				"ProjectService_requires_/gde/PermissionService/check_permission", "0.0.1", "PermissionServiceInstance",
				"PermissionService", "0.0.1", "/gde/PermissionService/check_permission", "0.0.1");
		facadeInstance.unlinkClientServer("ProjectServiceInstance", "ProjectService", "0.0.1",
				"ProjectService_requires_/gde/FileService/files", "0.0.1", "FileServiceInstance", "FileService",
				"0.0.1", "/gde/FileService/files", "0.0.1");
		facadeInstance.unlinkClientServer("ProjectServiceInstance", "ProjectService", "0.0.1",
				"ProjectService_requires_/gde/UserService/users", "0.0.1", "UserServiceInstance", "UserService",
				"0.0.1", "/gde/UserService/users", "0.0.1");
		facadeInstance.unlinkClientServer("FileServiceInstance", "FileService", "0.0.1",
				"FileService_requires_/gde/Authentication/get_login", "0.0.1", "AuthenticationInstance",
				"Authentication", "0.0.1", "/gde/Authentication/get_login", "0.0.1");
		facadeInstance.unlinkClientServer("FileServiceInstance", "FileService", "0.0.1",
				"FileService_requires_/gde/PermissionService/check_permission", "0.0.1", "PermissionServiceInstance",
				"PermissionService", "0.0.1", "/gde/PermissionService/check_permission", "0.0.1");
		facadeInstance.unlinkClientServer("FileServiceInstance", "FileService", "0.0.1",
				"FileService_requires_/gde/UserService/users", "0.0.1", "UserServiceInstance", "UserService", "0.0.1",
				"/gde/UserService/users", "0.0.1");
		facadeInstance.unlinkClientServer("AuthenticationInstance", "Authentication", "0.0.1",
				"Authentication_requires_/gde/UserService/check_password", "0.0.1", "UserServiceInstance",
				"UserService", "0.0.1", "/gde/UserService/check_password", "0.0.1");
		facadeInstance.unlinkClientServer("PermissionServiceInstance", "PermissionService", "0.0.1",
				"PermissionService_requires_/gde/Authentication/get_login", "0.0.1", "AuthenticationInstance",
				"Authentication", "0.0.1", "/gde/Authentication/get_login", "0.0.1");
		facadeInstance.unlinkClientServer("PermissionServiceInstance", "PermissionService", "0.0.1",
				"PermissionService_requires_/gde/UserService/users", "0.0.1", "UserServiceInstance", "UserService",
				"0.0.1", "/gde/UserService/users", "0.0.1");
		facadeInstance.unlinkClientServer("PermissionServiceInstance", "PermissionService", "0.0.1",
				"PermissionService_requires_/gde/UserService/users_groups", "0.0.1", "UserServiceInstance",
				"UserService", "0.0.1", "/gde/UserService/users_groups", "0.0.1");
		Log.SCENARIO.info("{}",
				() -> "2. Test False - Remove all connector contained microservice Authentication, then check it is not attached to any contracts...");
		Assert.assertFalse(
				facadeInstance.isMicroserviceConnectedToOthers("AuthenticationInstance", "Authentication", "0.0.1"));
		// remove auth
		facadeInstance.removeMicroservice("UserServiceInstance", "UserService", "0.0.1");
		facadeInstance.removeMicroservice("ProjectServiceInstance", "ProjectService", "0.0.1");
		facadeInstance.removeMicroservice("FileServiceInstance", "FileService", "0.0.1");
		facadeInstance.removeMicroservice("AuthenticationInstance", "Authentication", "0.0.1");
		facadeInstance.removeMicroservice("PermissionServiceInstance", "PermissionService", "0.0.1");
		String s2 = facadeInstance.toStringCurrentConfiguration();
		Log.SCENARIO.info("{}", () -> "configuration after instantiation\n" + s2);
		facadeInstance.commitConfiguration();
		String thirdConfiguration = facadeInstance.getIdentifierOfLastCommittedConfiguration();
		var secondAIPlan = facadePlanner.computeAIPlanToReconfigureWithLPG(secondConfiguration, thirdConfiguration);
		Log.SCENARIO.info("{}",
				() -> secondAIPlan.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		Assert.assertFalse(secondAIPlan.isEmpty());
	}
}
