// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Yuwei Wang
Contributor(s): Denis Conan
 */
package mimosae.examples.gde;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mimosae.common.log.Log;
import mimosae.examples.UtilGDEForTest;
import mimosae.modelatruntime.MimosaeRuntimModelTypeDependency;
import mimosae.modelatruntime.MimosaeRuntimeModelType;

public class TestGDEDependency {

	private MimosaeRuntimeModelType facadeType;
	private MimosaeRuntimModelTypeDependency dependencyGraph;

	@Before
	public void setUp() throws Exception {
		facadeType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		dependencyGraph = MimosaeRuntimModelTypeDependency.getMicroserviceTypeDependency();
		dependencyGraph.clear();
	}

	@After
	public void teardown() {
		facadeType = null;
		dependencyGraph = null;
	}

	@Test
	public void testGDEDependency() throws Exception {
		// create types
		UtilGDEForTest.createGDEConfigurationType(facadeType);
		// create dependency
		dependencyGraph.createMicroserviceTypeDependency("UserServicev.0.0.1->Authentication", "Authentication",
				">=0.0.1", "UserService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("UserServicev.0.0.1->PermissionService", "PermissionService",
				">=0.0.1", "UserService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("ProjectServicev.0.0.1->Authentication", "Authentication",
				">=0.0.1", "ProjectService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("ProjectServicev.0.0.1->PermissionService",
				"PermissionService", ">=0.0.1", "ProjectService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("ProjectServicev.0.0.1->FileService", "FileService", ">=0.0.1",
				"ProjectService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("ProjectServicev.0.0.1->UserService", "UserService", ">=0.0.1",
				"ProjectService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("FileServicev.0.0.1->Authentication", "Authentication",
				">=0.0.1", "FileService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("FileServicev.0.0.1->PermissionService", "PermissionService",
				">=0.0.1", "FileService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("FileServicev.0.0.1->UserService", "UserService", ">=0.0.1",
				"FileService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("Authenticationv.0.0.1->UserService", "UserService", ">=0.0.1",
				"Authentication", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("PermissionServicev.0.0.1->Authentication", "Authentication",
				">=0.0.1", "PermissionService", "0.0.1");
		dependencyGraph.createMicroserviceTypeDependency("PermissionServicev.0.0.1->UserService", "UserService",
				">=0.0.1", "PermissionService", "0.0.1");
		// add dependee to dependency
		dependencyGraph.addDependeeToDependency("UserServicev.0.0.1->Authentication", "Authentication", "0.0.1");
		dependencyGraph.addDependeeToDependency("UserServicev.0.0.1->PermissionService", "PermissionService", "0.0.1");
		dependencyGraph.addDependeeToDependency("ProjectServicev.0.0.1->Authentication", "Authentication", "0.0.1");
		dependencyGraph.addDependeeToDependency("ProjectServicev.0.0.1->PermissionService", "PermissionService",
				"0.0.1");
		dependencyGraph.addDependeeToDependency("ProjectServicev.0.0.1->FileService", "FileService", "0.0.1");
		dependencyGraph.addDependeeToDependency("ProjectServicev.0.0.1->UserService", "UserService", "0.0.1");
		dependencyGraph.addDependeeToDependency("FileServicev.0.0.1->Authentication", "Authentication", "0.0.1");
		dependencyGraph.addDependeeToDependency("FileServicev.0.0.1->PermissionService", "PermissionService", "0.0.1");
		dependencyGraph.addDependeeToDependency("FileServicev.0.0.1->UserService", "UserService", "0.0.1");
		dependencyGraph.addDependeeToDependency("Authenticationv.0.0.1->UserService", "UserService", "0.0.1");
		dependencyGraph.addDependeeToDependency("PermissionServicev.0.0.1->Authentication", "Authentication", "0.0.1");
		dependencyGraph.addDependeeToDependency("PermissionServicev.0.0.1->UserService", "UserService", "0.0.1");
		// test
		Log.SCENARIO.info("{}", () -> "print dependency UserServicev.0.0.1->Authentication : "
				+ dependencyGraph.toStringMicroserviceTypeDependencies("UserServicev.0.0.1->Authentication"));
		Log.SCENARIO.info("{}",
				() -> "1. Test True - Check there is a dependency from UserServicev.0.0.1 to Authenticationv0.0.1...");
		assertTrue(dependencyGraph.checkDependencyBetweenMicroserviceTypes("UserService", "0.0.1", "Authentication",
				"0.0.1"));
		Log.SCENARIO.info("{}",
				() -> "2. Test False - Check there is not a dependency from UserServicev.0.0.1 to ProjectServicev0.0.1...");
		assertFalse(dependencyGraph.checkDependencyBetweenMicroserviceTypes("UserService", "0.0.1", "ProjectService",
				"0.0.1"));
		Log.SCENARIO.info("{}",
				() -> "3. Test No Exception - Change version requirement of UserServicev.0.0.1->Authentication from '>=0.0.1' to '0.0.*'...");
		dependencyGraph.changeDependeeVersionRequirement("UserServicev.0.0.1->Authentication", "0.0.*");
		assertEquals("0.0.*", dependencyGraph.getMicroserviceTypeDependency("UserServicev.0.0.1->Authentication")
				.getDependeeVersionRequirement());
		assertNotNull(
				dependencyGraph.getMicroserviceTypeDependency("UserServicev.0.0.1->Authentication").getDependees());
	}

}
