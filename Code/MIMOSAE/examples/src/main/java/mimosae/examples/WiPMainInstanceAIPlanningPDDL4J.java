// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.examples;

import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;

import mimosae.common.log.Log;
import mimosae.modelatruntime.MimosaeRuntimeModelInstance;
import mimosae.modelatruntime.MimosaeRuntimeModelType;
import mimosae.planner.MimosaePlanner;
import mimosae.planner.pddl.Action;

public final class WiPMainInstanceAIPlanningPDDL4J {

	public static void main(String[] args) throws Exception {
		Log.setLevel(Log.SCENARIO, Level.INFO);
		// facade for the instances
		var facadeInstance = MimosaeRuntimeModelInstance.getMimosaeRuntimeModelInstance();
		// facade for the planner
		var facadePlanner = MimosaePlanner.getMimosaeRuntimeModelInstance();
		// create the types
		WiPMainType.main(new String[0]);
		String configuration1 = facadeInstance.getIdentifierOfLastCommittedConfiguration();
		facadeInstance.setConfigurationTypeOfCurrentConfiguration(
				MimosaeRuntimeModelType.getMimosaeRuntimeModelType().getIdentifierOfLastCommittedConfigurationType());
		facadeInstance.createMicroservice("microservice1", "microserviceType1", "0.0.1");
		facadeInstance.createMicroservice("microservice2", "microserviceType2", "0.0.1");
		facadeInstance.createMicroservice("microservice3", "microserviceType3", "0.0.1");
		facadeInstance.createMicroservice("microservice4", "microserviceType4", "0.0.1");
		facadeInstance.createMicroservice("microservice5", "microserviceType5", "0.0.1");
		facadeInstance.createDatabaseSystem("databaseSystem1", "databaseSystemType1", "0.0.1");
		facadeInstance.linkClientServer("microservice1", "microserviceType1", "0.0.1", "clientContractType1", "0.0.1",
				"microservice2", "microserviceType2", "0.0.1", "serverContractType2", "0.0.1");
		facadeInstance.createPubSubConnector("channelPubSubConnector1", "channelPubSubConnectorType1", "0.0.1");
		facadeInstance.createPubSubConnector("amqpPubSubConnector1", "amqpPubSubConnectorType1", "0.0.1");
		facadeInstance.createPubSubConnector("mqttPubSubConnector1", "mqttPubSubConnectorType1", "0.0.1");
		facadeInstance.linkProducerToConnector("microservice1", "microserviceType1", "0.0.1",
				"channelProducerContractType1", "0.0.1", "channelPubSubConnector1");
		facadeInstance.linkConsumerToConnector("microservice3", "microserviceType3", "0.0.1",
				"channelConsumerContractType1", "0.0.1", "channelPubSubConnector1");
		facadeInstance.linkProducerToConnector("microservice1", "microserviceType1", "0.0.1",
				"amqpProducerContractType2", "0.0.1", "amqpPubSubConnector1");
		facadeInstance.linkConsumerToConnector("microservice4", "microserviceType4", "0.0.1",
				"amqpConsumerContractType1", "0.0.1", "amqpPubSubConnector1");
		facadeInstance.linkProducerToConnector("microservice1", "microserviceType1", "0.0.1",
				"mqttProducerContractType2", "0.0.1", "mqttPubSubConnector1");
		facadeInstance.linkConsumerToConnector("microservice5", "microserviceType5", "0.0.1",
				"mqttConsumerContractType1", "0.0.1", "mqttPubSubConnector1");
		facadeInstance.connectMicroserviceToDatabaseSystem("microservice1", "microserviceType1", "0.0.1",
				"databaseContractType1", "0.0.1", "databaseSystem1", "databaseSystemType1", "0.0.1");
		boolean c1 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "01.01 microservice1 is attached to some contracts must be true: " + c1);
		facadeInstance.commitConfiguration();
		var s1 = facadeInstance.toStringLastCommittedConfiguration();
		Log.SCENARIO.info("{}", () -> "configuration after instanciation\n" + s1);
		String configuration2 = facadeInstance.getIdentifierOfLastCommittedConfiguration();

		Log.SCENARIO.info("{}", () -> "AI planning from empty configuration to configuration2: ");
		var actions1 = facadePlanner.computeAIPlanToReconfigureWithPDDL4J(configuration1, configuration2);
		Log.SCENARIO.info("{}",
				() -> actions1.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		if (actions1.isEmpty()) {
			throw new IllegalStateException("error in AI planning");
		}

		Log.SCENARIO.info("{}", () -> "Modifying configuration1");
		facadeInstance.unlinkClientServer("microservice1", "microserviceType1", "0.0.1", "clientContractType1", "0.0.1",
				"microservice2", "microserviceType2", "0.0.1", "serverContractType2", "0.0.1");
		boolean c2 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "02.01 microservice1 is attached to some contracts must be true: " + c2);
		boolean c3 = facadeInstance.isMicroserviceConnectedToOthers("microservice2", "microserviceType2", "0.0.1");
		Log.SCENARIO.info("{}", () -> "02.02 microservice2 is attached to some contracts must be false: " + c3);
		facadeInstance.removeMicroservice("microservice2", "microserviceType2", "0.0.1");
		facadeInstance.unlinkProducerFromConnector("microservice1", "microserviceType1", "0.0.1",
				"channelProducerContractType1", "0.0.1", "channelPubSubConnector1");
		boolean c4 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "03.01 microservice1 is attached to some contracts must be true: " + c4);
		boolean c5 = facadeInstance.isMicroserviceConnectedToOthers("microservice3", "microserviceType3", "0.0.1");
		Log.SCENARIO.info("{}", () -> "03.02 microservice3 is attached to some contracts must be true: " + c5);
		facadeInstance.unlinkConsumerFromConnector("microservice3", "microserviceType3", "0.0.1",
				"channelConsumerContractType1", "0.0.1", "channelPubSubConnector1");
		boolean c6 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "04.01 microservice1 is attached to some contracts must be true: " + c6);
		boolean c7 = facadeInstance.isMicroserviceConnectedToOthers("microservice3", "microserviceType3", "0.0.1");
		Log.SCENARIO.info("{}", () -> "04.02 microservice3 is attached to some contracts must be false: " + c7);
		facadeInstance.removeMicroservice("microservice3", "microserviceType3", "0.0.1");
		facadeInstance.unlinkProducerFromConnector("microservice1", "microserviceType1", "0.0.1",
				"amqpProducerContractType2", "0.0.1", "amqpPubSubConnector1");
		boolean c8 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "05.01 microservice1 is attached to some contracts must be true: " + c8);
		facadeInstance.disconnectMicroserviceToDatabaseSystem("microservice1", "microserviceType1", "0.0.1",
				"databaseContractType1", "0.0.1", "databaseSystem1", "databaseSystemType1", "0.0.1");
		boolean c9 = facadeInstance.isMicroserviceConnectedToOthers("microservice4", "microserviceType4", "0.0.1");
		Log.SCENARIO.info("{}", () -> "05.02 microservice4 is attached to some contracts must be true: " + c9);
		facadeInstance.unlinkConsumerFromConnector("microservice4", "microserviceType4", "0.0.1",
				"amqpConsumerContractType1", "0.0.1", "amqpPubSubConnector1");
		boolean c10 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "06.01 microservice1 is attached to some contracts must be true: " + c10);
		boolean c11 = facadeInstance.isMicroserviceConnectedToOthers("microservice4", "microserviceType4", "0.0.1");
		Log.SCENARIO.info("{}", () -> "06.02 microservice4 is attached to some contracts must be false: " + c11);
		facadeInstance.removeMicroservice("microservice4", "microserviceType4", "0.0.1");
		facadeInstance.removePubSubConnectorAndUnlink("channelPubSubConnector1");
		boolean c12 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "07.01 microservice1 is attached to some contracts must be true: " + c12);
		facadeInstance.removePubSubConnectorAndUnlink("amqpPubSubConnector1");
		boolean c13 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "08.01 microservice1 is attached to some contracts must be true: " + c13);
		boolean c14 = facadeInstance.isMicroserviceConnectedToOthers("microservice5", "microserviceType5", "0.0.1");
		Log.SCENARIO.info("{}", () -> "08.02 microservice5 is attached to some contracts must be true: " + c14);
		facadeInstance.removePubSubConnectorAndUnlink("mqttPubSubConnector1");
		boolean c15 = facadeInstance.isMicroserviceConnectedToOthers("microservice1", "microserviceType1", "0.0.1");
		Log.SCENARIO.info("{}", () -> "09.01 microservice1 is attached to some contracts must be false: " + c15);
		boolean c16 = facadeInstance.isMicroserviceConnectedToOthers("microservice5", "microserviceType5", "0.0.1");
		Log.SCENARIO.info("{}", () -> "09.02 microservice5 is attached to some contracts must be false: " + c16);
		facadeInstance.removeMicroservice("microservice1", "microserviceType1", "0.0.1");
		facadeInstance.removeMicroservice("microservice5", "microserviceType5", "0.0.1");
		facadeInstance.removeDatabaseSystem("databaseSystem1", "databaseSystemType1", "0.0.1");
		facadeInstance.commitConfiguration();
		var s2 = facadeInstance.toStringLastCommittedConfiguration();
		String configuration3 = facadeInstance.getIdentifierOfLastCommittedConfiguration();
		Log.SCENARIO.info("{}", () -> "configuration after cleaning\n" + s2);

		Log.SCENARIO.info("{}", () -> "AI planning from configuration2 to empty configuration: ");
		var actions2 = facadePlanner.computeAIPlanToReconfigureWithPDDL4J(configuration2, configuration3);
		Log.SCENARIO.info("{}",
				() -> actions2.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		if (actions2.isEmpty()) {
			throw new IllegalStateException("error in AI planning");
		}
	}
}
