// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.examples;

import org.apache.logging.log4j.Level;

import mimosae.common.log.Log;
import mimosae.modelatruntime.MimosaeRuntimModelTypeDependency;

public class WiPMainDependency {

	public static void main(String[] args) throws Exception {
		Log.setLevel(Log.SCENARIO, Level.INFO);
		// get dep graph
		var dependencyGraph = MimosaeRuntimModelTypeDependency.getMicroserviceTypeDependency();
		// create the types
		WiPMainType.main(new String[0]);
		// create dependencies
		dependencyGraph.createMicroserviceTypeDependency("dependency1", "microserviceType2", "0.0.1",
				"microserviceType1", "0.0.1");
		// add dependee
		dependencyGraph.addDependeeToDependency("dependency1", "microserviceType2", "0.0.1");
		// tests
		Log.SCENARIO.info("{}", () -> dependencyGraph.toStringMicroserviceTypeDependencies("dependency1"));
		dependencyGraph.changeDependeeVersionRequirement("dependency1", ">=0.0.1");
		Log.SCENARIO.info("{}", () -> dependencyGraph.toStringMicroserviceTypeDependencies("dependency1"));
		final boolean check1 = dependencyGraph.checkDependencyBetweenMicroserviceTypes("microserviceType1", "0.0.1",
				"microserviceType2", "0.0.1");
		Log.SCENARIO.info("{}",
				() -> "(Test should be true) Check MicroserviceType microserviceType1v.0.0.1 depending on MicroserviceType microserviceType2v.0.0.1 : "
						+ check1);
		final boolean check2 = dependencyGraph.checkDependencyBetweenMicroserviceTypes("microserviceType1", "0.0.1",
				"microserviceType3", "0.0.1");
		Log.SCENARIO.info("{}",
				() -> "(Test should be false) Check MicroserviceType microserviceType1v.0.0.1 depending on MicroserviceType microserviceType3v.0.0.1 : "
						+ check2);
	}
}
