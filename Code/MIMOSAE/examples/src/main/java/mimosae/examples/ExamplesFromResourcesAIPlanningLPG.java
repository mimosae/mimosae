// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.examples;

import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;

import mimosae.common.log.Log;
import mimosae.planner.pddl.Action;
import mimosae.planner.pddl.PDDLPlanWithLPG;

public final class ExamplesFromResourcesAIPlanningLPG {

	public static void main(String[] args) throws Exception {
		Log.setLevel(Log.SCENARIO, Level.INFO);
		Log.setLevel(Log.PLANNER, Level.INFO);
		String domainFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_domain.pddl").getPath();
		Objects.requireNonNull(domainFileName, "domain file resource file not found");
		var pddlPlan = new PDDLPlanWithLPG();
		// 01_create_from_empty_configuration
		String problemFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_01_create_from_empty_configuration.pddl").getPath();
		Objects.requireNonNull(problemFileName, "problem file resource file not found");
		var actions1 = pddlPlan.planWithLPG(domainFileName, problemFileName);
		Log.SCENARIO.info("{}", () -> "Example mimosae_planner_problem_01_create_from_empty_configuration.pddl");
		Log.SCENARIO.info("{}", () -> "from configuration: empty");
		Log.SCENARIO.info("{}", () -> "to configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1 connected to databasesystem1");
		Log.SCENARIO.info("{}", () -> "- microservice2 connected to databasesystem2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1 with m1_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "Actions of a plan to reconfigure (first entry is empty):");
		Log.SCENARIO.info("{}",
				() -> actions1.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		if (actions1.isEmpty()) {
			throw new IllegalStateException("error in AI planning");
		}
		// 02_remove_to_empty_configuration.pddl
		problemFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_02_remove_to_empty_configuration.pddl").getPath();
		Objects.requireNonNull(problemFileName, "problem file resource file not found");
		var actions2 = pddlPlan.planWithLPG(domainFileName, problemFileName);
		Log.SCENARIO.info("{}", () -> "Example mimosae_planner_problem_02_remove_to_empty_configuration.pddl");
		Log.SCENARIO.info("{}", () -> "from configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1 with m1_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "to configuration: empty");
		Log.SCENARIO.info("{}", () -> "Actions of a plan to reconfigure (first entry is empty):");
		Log.SCENARIO.info("{}",
				() -> actions2.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		if (actions2.isEmpty()) {
			throw new IllegalStateException("error in AI planning");
		}
		// 03_from_microservice1_to_microservice1bis.pddl
		problemFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_03_from_microservice1_to_microservice1bis.pddl").getPath();
		Objects.requireNonNull(problemFileName, "problem file resource file not found");
		var actions3 = pddlPlan.planWithLPG(domainFileName, problemFileName);
		Log.SCENARIO.info("{}",
				() -> "Example mimosae_planner_problem_03_from_microservice1_to_microservice1bis.pddl");
		Log.SCENARIO.info("{}", () -> "from configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1 with m1_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "to configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1bis with m1bis_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1bis with m1bis_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "Actions of a plan to reconfigure (first entry is empty):");
		Log.SCENARIO.info("{}",
				() -> actions3.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		if (actions3.isEmpty()) {
			throw new IllegalStateException("error in AI planning");
		}
		// 04_from_microservice2_to_microservice2bis.pddl
		problemFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_04_from_microservice2_to_microservice2bis.pddl").getPath();
		Objects.requireNonNull(problemFileName, "problem file resource file not found");
		var actions4 = pddlPlan.planWithLPG(domainFileName, problemFileName);
		Log.SCENARIO.info("{}",
				() -> "Example mimosae_planner_problem_04_from_microservice2_to_microservice2bis.pddl");
		Log.SCENARIO.info("{}", () -> "from configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1 with m1_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "to configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2bis_servercontract of microservice2bis");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1bis with m1_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "Actions of a plan to reconfigure (first entry is empty):");
		Log.SCENARIO.info("{}",
				() -> actions4.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		if (actions4.isEmpty()) {
			throw new IllegalStateException("error in AI planning");
		}
		// 05_from_microservice3_to_microservice3bis.pddl
		problemFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_05_from_microservice3_to_microservice3bis.pddl").getPath();
		Objects.requireNonNull(problemFileName, "problem file resource file not found");
		var actions5 = pddlPlan.planWithLPG(domainFileName, problemFileName);
		Log.SCENARIO.info("{}",
				() -> "Example mimosae_planner_problem_05_from_microservice3_to_microservice3bis.pddl");
		Log.SCENARIO.info("{}", () -> "from configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1 with m1_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "to configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1bis with m1_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3bis with m3bis_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "Actions of a plan to reconfigure (first entry is empty):");
		Log.SCENARIO.info("{}",
				() -> actions5.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		if (actions5.isEmpty()) {
			throw new IllegalStateException("error in AI planning");
		}
		// 06_from_pubsubconnector1_to_pubsubconnector1bis.pddl
		problemFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_06_from_pubsubconnector1_to_pubsubconnector1bis.pddl")
				.getPath();
		Objects.requireNonNull(problemFileName, "problem file resource file not found");
		var actions6 = pddlPlan.planWithLPG(domainFileName, problemFileName);
		Log.SCENARIO.info("{}",
				() -> "Example mimosae_planner_problem_06_from_pubsubconnector1_to_pubsubconnector1bis.pddl");
		Log.SCENARIO.info("{}", () -> "from configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice1 with m1_producercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1");
		Log.SCENARIO.info("{}", () -> "to configuration:");
		Log.SCENARIO.info("{}",
				() -> "- microservice1 with m1_clientcontract connected to m2_servercontract of microservice2");
		Log.SCENARIO.info("{}", () -> "- pubsubconnector1bis");
		Log.SCENARIO.info("{}", () -> "- microservice1bis with m1_producercontract1 connected to pubsubconnector1bis");
		Log.SCENARIO.info("{}", () -> "- microservice3 with m3_consumercontract1 connected to pubsubconnector1bis");
		Log.SCENARIO.info("{}", () -> "Actions of a plan to reconfigure (first entry is empty):");
		Log.SCENARIO.info("{}",
				() -> actions6.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		if (actions6.isEmpty()) {
			throw new IllegalStateException("error in AI planning");
		}
	}
}
