// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Yuwei Wang
Contributor(s): Denis Conan
 */
package mimosae.examples;

import java.util.Arrays;
import java.util.Objects;

import mimosae.modelatruntime.MimosaeRuntimeModelType;
import mimosae.modelatruntime.exception.AlreadyExistsModelElementException;
import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.exception.IsNotInstantiableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.MalformedOperationDeclarationException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.exception.WrongNumberOfIdentifiersException;

public final class UtilGDEForTest {

	private UtilGDEForTest() {
		// no instance
	}

	public static void createGDEConfigurationType(final MimosaeRuntimeModelType facade)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedOperationDeclarationException, UnknownTypeException, IsCommittedAndCannotBeModifiedException,
			IsNotInstantiableAndCannotBeCommittedException {
		Objects.requireNonNull(facade, "facade cannot be null");
		// Operation declarations - "name paramType... : returnType"
		// user service
		var usOps1 = new String[] { "CreateUser String String : gde.User", "CreateUserByUser gde.User : gde.User",
				"DeleteUser long", "FindUserByName String : gde.User", "FindUserById long : gde.User",
				"ListUsers : List<gde.User>" };
		var usOps2 = new String[] { "CheckPassword String String : boolean" };
		var usOps3 = new String[] { "CreateGroup String : gde.Group", "CreateGroupByGroup gde.Group : gde.Group",
				"DeleteGroup long", "FindGroupsByName String : List<gde.Group>", "FindGroupById long : gde.Group",
				"ListGroups : List<gde.Group>" };
		var usOps4 = new String[] { "AddUserToGroup long long", "AddUsersToGroup : List<Long> long",
				"RemoveUserFromGroup long long", "IsUserInGroup long long : boolean",
				"ListUsersInGroup long : List<gde.User>", "ListGroupsOfUser long : List<gde.Group>" };
		// project service
		var psOps1 = new String[] { "CreateProject gde.Project : gde.Project",
				"UpdateProject gde.Project : gde.Project", "DeleteProject long", "RestoreProject long",
				"FindProjectById long : gde.Project", "FindProjectsByName String : List<gde.Project>",
				"FindProjectsByName String : List<gde.Project>",
				"FindProjectsByCreationDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByDeletionDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByUpdateDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByRestorationDate Date gde.DateComparator : List<gde.Project>",
				"ListProjects : List<gde.Project>" };
		var psOps2 = new String[] { "GetProjectLocker long : long", "IsProjectLocked long long : boolean",
				"SetProjectLockState long long boolean" };
		var psOps3 = new String[] { "ListProjectFiles long String : List<gde.GDEFile>",
				"AttachFileToProject long long long String", "DetachFileFromProject long long long String" };
		// file service
		var fsOps1 = new String[] { "CreateFile gde.GDEFile : gde.GDEFile", "UpdateFile gde.GDEFile : gde.GDEFile",
				"DeleteFile long", "RestoreFile long", "FindFileById long : gde.GDEFile", "ReadFile long : gde.GDEFile",
				"FindFilesByName String : List<gde.GDEFile>",
				"FindFilesByCreationDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByDeletionDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByUpdateDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByRestorationDate Date gde.DateComparator : List<gde.GDEFile>",
				"ListFiles : List<gde.GDEFile>", "FindFilesToIndex : List<gde.GDEFile>",
				"FindFilesDeleted : List<gde.GDEFile>", "SetValid long boolean", "IsValid long : boolean",
				"SetIndexed long boolean", "IsIndexed long : boolean" };
		var fsOps2 = new String[] { "OpenFile long long : boolean", "CloseFile long long : boolean" };
		var fsOps3 = new String[] { "CreateChunk gde.Chunk : gde.Chunk", "ReadChunk long : gde.Chunk" };
		var fsOps4 = new String[] { "AttachChunkToFile long long", "DetachChunkFromFile long long",
				"FindChunksByFileId long : gde.Chunk", "FindChunksInfoByFileId long : gde.ChunkInfo" };
		// authentication
		var asOps1 = new String[] { "Login String String : String" };
		var asOps2 = new String[] { "GetLogin String : String" };
		// permission service
		var pmsOps1 = new String[] { "CreateService gde.Service : gde.Service", "DeleteService long : boolean",
				"UpdateService gde.Service : gde.Service", "FindServiceById long : gde.Service",
				"FindServiceByName String : gde.Service", "ListServices : List<gde.Service>" };

		var pmsOps2 = new String[] { "CreateMethod gde.Method : gde.Method", "DeleteMethod long : boolean",
				"UpdateMethod gde.Method : gde.Method", "FindMethodById long : gde.Method",
				"FindMethodByName String : gde.Method", "FindMethodByServiceNameMethodIndex String int : gde.Method",
				"FindMethodsByService long : List<gde.Method>", "ListMethods : List<gde.Method>" };
		var pmsOps3 = new String[] { "CreatePermission gde.Permission : gde.Permission",
				"DeletePermission long : boolean", "UpdatePermission gde.Permission : gde.Permission",
				"FindPermissionById long : gde.Permission", "FindPermissionsByMethodID long : List<gde.Permission>",
				"ListPermissions : List<gde.Permission>" };
		var pmsOps4 = new String[] { "FindPermissionsByGroupID long String : List<gde.Permission>",
				"FindPermissionsByUserID long String : List<gde.Permission>",
				"HasGroupThePermissionOn long String int String : boolean",
				"HasUserThePermissionOn long String int String : boolean",
				"CheckUserPermission String String int String : boolean" };
		// Server contract types
		// user service
		facade.createServerContractType("/gde/UserService/users", "0.0.1", usOps1);
		facade.createServerContractType("/gde/UserService/check_password", "0.0.1", usOps2);
		facade.createServerContractType("/gde/UserService/groups", "0.0.1", usOps3);
		facade.createServerContractType("/gde/UserService/users_groups", "0.0.1", usOps4);
		var usServerContract = new String[] { "/gde/UserService/users", "0.0.1", "/gde/UserService/check_password",
				"0.0.1", "/gde/UserService/groups", "0.0.1", "/gde/UserService/users_groups", "0.0.1" };
		// project service
		facade.createServerContractType("/gde/ProjectService/projects", "0.0.1", psOps1);
		facade.createServerContractType("/gde/ProjectService/project_lock", "0.0.1", psOps2);
		facade.createServerContractType("/gde/ProjectService/project_files", "0.0.1", psOps3);
		var psServerContract = new String[] { "/gde/ProjectService/projects", "0.0.1",
				"/gde/ProjectService/project_lock", "0.0.1", "/gde/ProjectService/project_files", "0.0.1" };
		// file service
		facade.createServerContractType("/gde/FileService/files", "0.0.1", fsOps1);
		facade.createServerContractType("/gde/FileService/file_lock", "0.0.1", fsOps2);
		facade.createServerContractType("/gde/FileService/chunks", "0.0.1", fsOps3);
		facade.createServerContractType("/gde/FileService/file_chunks", "0.0.1", fsOps4);
		var fsServerContract = new String[] { "/gde/FileService/files", "0.0.1", "/gde/FileService/file_lock", "0.0.1",
				"/gde/FileService/chunks", "0.0.1", "/gde/FileService/file_chunks", "0.0.1" };
		// authentication
		facade.createServerContractType("/gde/Authentication/login", "0.0.1", asOps1);
		facade.createServerContractType("/gde/Authentication/get_login", "0.0.1", asOps2);
		var asServerContract = new String[] { "/gde/Authentication/login", "0.0.1", "/gde/Authentication/get_login",
				"0.0.1" };
		// permission service
		facade.createServerContractType("/gde/PermissionService/services", "0.0.1", pmsOps1);
		facade.createServerContractType("/gde/PermissionService/methods", "0.0.1", pmsOps2);
		facade.createServerContractType("/gde/PermissionService/permissions", "0.0.1", pmsOps3);
		facade.createServerContractType("/gde/PermissionService/check_permission", "0.0.1", pmsOps4);
		var pmsServerContract = new String[] { "/gde/PermissionService/services", "0.0.1",
				"/gde/PermissionService/methods", "0.0.1", "/gde/PermissionService/permissions", "0.0.1",
				"/gde/PermissionService/check_permission", "0.0.1" };
		// Client contract types
		// user service
		facade.createClientContractType("UserService_requires_/gde/Authentication/get_login", "0.0.1", asOps2);
		facade.createClientContractType("UserService_requires_/gde/PermissionService/check_permission", "0.0.1",
				pmsOps4);
		// project service
		facade.createClientContractType("ProjectService_requires_/gde/Authentication/get_login", "0.0.1", asOps2);
		facade.createClientContractType("ProjectService_requires_/gde/PermissionService/check_permission", "0.0.1",
				pmsOps4);
		facade.createClientContractType("ProjectService_requires_/gde/FileService/files", "0.0.1", fsOps1);
		facade.createClientContractType("ProjectService_requires_/gde/UserService/users", "0.0.1", usOps1);
		// file service
		facade.createClientContractType("FileService_requires_/gde/Authentication/get_login", "0.0.1", asOps2);
		facade.createClientContractType("FileService_requires_/gde/PermissionService/check_permission", "0.0.1",
				pmsOps4);
		facade.createClientContractType("FileService_requires_/gde/UserService/users", "0.0.1", usOps1);
		// authentication
		facade.createClientContractType("Authentication_requires_/gde/UserService/check_password", "0.0.1", usOps2);
		// permission service
		facade.createClientContractType("PermissionService_requires_/gde/Authentication/get_login", "0.0.1", asOps2);
		facade.createClientContractType("PermissionService_requires_/gde/UserService/users", "0.0.1", usOps1);
		facade.createClientContractType("PermissionService_requires_/gde/UserService/users_groups", "0.0.1", usOps4);
		// Combine Client contract types for microservice type
		var usClientContract = new String[] { "UserService_requires_/gde/Authentication/get_login", "0.0.1",
				"UserService_requires_/gde/PermissionService/check_permission", "0.0.1" };
		var psClientContract = new String[] { "ProjectService_requires_/gde/Authentication/get_login", "0.0.1",
				"ProjectService_requires_/gde/PermissionService/check_permission", "0.0.1",
				"ProjectService_requires_/gde/FileService/files", "0.0.1",
				"ProjectService_requires_/gde/UserService/users", "0.0.1" };
		var fsClientContract = new String[] { "FileService_requires_/gde/Authentication/get_login", "0.0.1",
				"FileService_requires_/gde/PermissionService/check_permission", "0.0.1",
				"FileService_requires_/gde/UserService/users", "0.0.1" };
		var asClientContract = new String[] { "Authentication_requires_/gde/UserService/check_password", "0.0.1" };
		var pmsClientContract = new String[] { "PermissionService_requires_/gde/Authentication/get_login", "0.0.1",
				"PermissionService_requires_/gde/UserService/users", "0.0.1",
				"PermissionService_requires_/gde/UserService/users_groups", "0.0.1" };
		// join client contract and server contract
		String[] usContracts = Arrays.copyOf(usClientContract, usClientContract.length + usServerContract.length);
		System.arraycopy(usServerContract, 0, usContracts, usClientContract.length, usServerContract.length);
		String[] psContracts = Arrays.copyOf(psClientContract, psClientContract.length + psServerContract.length);
		System.arraycopy(psServerContract, 0, psContracts, psClientContract.length, psServerContract.length);
		String[] fsContracts = Arrays.copyOf(fsClientContract, fsClientContract.length + fsServerContract.length);
		System.arraycopy(fsServerContract, 0, fsContracts, fsClientContract.length, fsServerContract.length);
		String[] asContracts = Arrays.copyOf(asClientContract, asClientContract.length + asServerContract.length);
		System.arraycopy(asServerContract, 0, asContracts, asClientContract.length, asServerContract.length);
		String[] pmsContracts = Arrays.copyOf(pmsClientContract, pmsClientContract.length + pmsServerContract.length);
		System.arraycopy(pmsServerContract, 0, pmsContracts, pmsClientContract.length, pmsServerContract.length);
		// Microservice type
		facade.createMicroserviceType("UserService", "0.0.1", usContracts);
		facade.createMicroserviceType("ProjectService", "0.0.1", psContracts);
		facade.createMicroserviceType("FileService", "0.0.1", fsContracts);
		facade.createMicroserviceType("Authentication", "0.0.1", asContracts);
		facade.createMicroserviceType("PermissionService", "0.0.1", pmsContracts);
		// Connector types
		facade.createClientServerConnectorType("UserService_requires_/gde/Authentication/get_login", "0.0.1",
				"/gde/Authentication/get_login", "0.0.1");
		facade.createClientServerConnectorType("UserService_requires_/gde/PermissionService/check_permission", "0.0.1",
				"/gde/PermissionService/check_permission", "0.0.1");

		facade.createClientServerConnectorType("ProjectService_requires_/gde/Authentication/get_login", "0.0.1",
				"/gde/Authentication/get_login", "0.0.1");
		facade.createClientServerConnectorType("ProjectService_requires_/gde/PermissionService/check_permission",
				"0.0.1", "/gde/PermissionService/check_permission", "0.0.1");
		facade.createClientServerConnectorType("ProjectService_requires_/gde/FileService/files", "0.0.1",
				"/gde/FileService/files", "0.0.1");
		facade.createClientServerConnectorType("ProjectService_requires_/gde/UserService/users", "0.0.1",
				"/gde/UserService/users", "0.0.1");

		facade.createClientServerConnectorType("FileService_requires_/gde/Authentication/get_login", "0.0.1",
				"/gde/Authentication/get_login", "0.0.1");
		facade.createClientServerConnectorType("FileService_requires_/gde/PermissionService/check_permission", "0.0.1",
				"/gde/PermissionService/check_permission", "0.0.1");
		facade.createClientServerConnectorType("FileService_requires_/gde/UserService/users", "0.0.1",
				"/gde/UserService/users", "0.0.1");

		facade.createClientServerConnectorType("Authentication_requires_/gde/UserService/check_password", "0.0.1",
				"/gde/UserService/check_password", "0.0.1");

		facade.createClientServerConnectorType("PermissionService_requires_/gde/Authentication/get_login", "0.0.1",
				"/gde/Authentication/get_login", "0.0.1");
		facade.createClientServerConnectorType("PermissionService_requires_/gde/UserService/users", "0.0.1",
				"/gde/UserService/users", "0.0.1");
		facade.createClientServerConnectorType("PermissionService_requires_/gde/UserService/users_groups", "0.0.1",
				"/gde/UserService/users_groups", "0.0.1");
		facade.commitConfigurationType();
	}
}
