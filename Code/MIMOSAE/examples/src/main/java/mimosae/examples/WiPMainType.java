// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.examples;

import org.apache.logging.log4j.Level;

import mimosae.common.log.Log;
import mimosae.modelatruntime.MimosaeRuntimeModelType;

public final class WiPMainType {

	public static void main(String[] args) throws Exception {
		Log.setLevel(Log.SCENARIO, Level.INFO);
		// facade
		var facade = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		// client and server contract types
		facade.createClientContractType("clientContractType1", "0.0.1", "m1 int boolean", "m2 : boolean ");
		facade.createClientContractType("clientContractType2", "0.0.1", "m3 boolean : float", "m4");
		facade.createServerContractType("serverContractType1", "0.0.1", "m3 boolean : float", "m4");
		facade.createServerContractType("serverContractType2", "0.0.1", "m1 int boolean", "m2 : boolean ");
		facade.createServerContractType("serverContractType3", "0.0.1", "m4 java.lang.Object : double");
		// channel-based contract types
		facade.createChannelBasedProducerContractType("channelProducerContractType1", "0.0.1", "channel1", "0.0.1",
				"eventtype1", "0.0.1");
		facade.createChannelBasedConsumerContractType("channelConsumerContractType1", "0.0.1", "channel1", "0.0.1",
				"eventtype1", "0.0.1");
		facade.createChannelBasedProducerContractType("channelProducerContractType2", "0.0.1", "channel2", "0.0.1",
				"eventtype2", "0.0.1");
		// AMQP topic-based contract types
		facade.createAMQPTopicBasedProducerContractType("amqpProducerContractType1", "0.0.1", "amqp1", "0.0.1", "a.b.c",
				"eventtype1", "0.0.1");
		facade.createAMQPTopicBasedConsumerContractType("amqpConsumerContractType1", "0.0.1", "amqp2", "0.0.1", "a.b.#",
				"eventtype1", "0.0.1");
		facade.createAMQPTopicBasedProducerContractType("amqpProducerContractType2", "0.0.1", "amqp1", "0.0.1", "x.y.z",
				"eventtype2", "0.0.1");
		// MQTT topic-based contract types
		facade.createMQTTTopicBasedProducerContractType("mqttProducerContractType1", "0.0.1", "mqtt1", "0.0.1", "a/b/c",
				"eventtype1", "0.0.1");
		facade.createMQTTTopicBasedConsumerContractType("mqttConsumerContractType1", "0.0.1", "mqtt2", "0.0.1", "a/b/#",
				"eventtype1", "0.0.1");
		facade.createMQTTTopicBasedProducerContractType("mqttProducerContractType2", "0.0.1", "mqtt1", "0.0.1", "x/y/z",
				"eventtype2", "0.0.1");
		// database systems
		facade.createDatabaseSystemType("databaseSystemType1", "0.0.1");
		// database contract types
		facade.createDatabaseContractType("databaseContractType1", "0.0.1");
		
		// microservice types 1, 3 et 4
		facade.createMicroserviceType("microserviceType1", "0.0.1", "clientContractType1", "0.0.1",
				"clientContractType2", "0.0.1", "serverContractType1", "0.0.1", "channelProducerContractType1", "0.0.1",
				"channelProducerContractType2", "0.0.1", "amqpProducerContractType1", "0.0.1",
				"amqpProducerContractType2", "0.0.1", "mqttProducerContractType1", "0.0.1", "mqttProducerContractType2",
				"0.0.1", "databaseContractType1", "0.0.1");
		facade.createMicroserviceType("microserviceType2", "0.0.1", "serverContractType2", "0.0.1",
				"serverContractType3", "0.0.1");
		facade.createMicroserviceType("microserviceType3", "0.0.1", "channelConsumerContractType1", "0.0.1");
		facade.createMicroserviceType("microserviceType4", "0.0.1", "amqpConsumerContractType1", "0.0.1");
		facade.createMicroserviceType("microserviceType5", "0.0.1", "mqttConsumerContractType1", "0.0.1");
		// connector types
		facade.createClientServerConnectorType("clientContractType1", "0.0.1", "serverContractType2", "0.0.1");
		facade.createPubSubConnectorType("channelPubSubConnectorType1", "0.0.1");
		facade.addProducerContractTypeToPubSubConnectorType("channelProducerContractType1", "0.0.1",
				"channelPubSubConnectorType1", "0.0.1");
		facade.addConsumerContractTypeToPubSubConnectorType("channelConsumerContractType1", "0.0.1",
				"channelPubSubConnectorType1", "0.0.1");
		facade.createPubSubConnectorType("amqpPubSubConnectorType1", "0.0.1");
		facade.addProducerContractTypeToPubSubConnectorType("amqpProducerContractType1", "0.0.1",
				"amqpPubSubConnectorType1", "0.0.1");
		facade.addConsumerContractTypeToPubSubConnectorType("amqpConsumerContractType1", "0.0.1",
				"amqpPubSubConnectorType1", "0.0.1");
		facade.createPubSubConnectorType("mqttPubSubConnectorType1", "0.0.1");
		facade.addProducerContractTypeToPubSubConnectorType("mqttProducerContractType1", "0.0.1",
				"mqttPubSubConnectorType1", "0.0.1");
		facade.addConsumerContractTypeToPubSubConnectorType("mqttConsumerContractType1", "0.0.1",
				"mqttPubSubConnectorType1", "0.0.1");
		facade.createDatabaseConnectorType("databaseContractType1", "0.0.1", "databaseSystemType1", "0.0.1");
		// tests
		var s1 = facade.toStringCurrentConfigurationType();
		Log.SCENARIO.info("{}", () -> s1);
		boolean c1 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "01.true...             c1v.0.0.1 is instantiable: " + c1);
		facade.setClientContractTypeMustBeConnected("clientContractType1", "0.0.1", true);
		boolean c2 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "02.still true...       c1v.0.0.1 is instantiable: " + c2);
		facade.setClientContractTypeMustBeConnected("clientContractType2", "0.0.1", true);
		boolean c3 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "03.to be false...      c1v.0.0.1 is instantiable: " + c3);
		facade.setClientContractTypeMustBeConnected("clientContractType2", "0.0.1", false);
		boolean c4 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "04.to be true again... c1v.0.0.1 is instantiable: " + c4);
		facade.setProducerContractTypeMustHaveConsumers("channelProducerContractType2", "0.0.1", true);
		boolean c5 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "05.to be false...      c1v.0.0.1 is instantiable: " + c5);
		facade.setProducerContractTypeMustHaveConsumers("channelProducerContractType2", "0.0.1", false);
		boolean c6 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "06.to be true again... c1v.0.0.1 is instantiable: " + c6);
		facade.setProducerContractTypeMustHaveConsumers("amqpProducerContractType2", "0.0.1", true);
		boolean c7 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "07.to be false...      c1v.0.0.1 is instantiable: " + c7);
		facade.setProducerContractTypeMustHaveConsumers("amqpProducerContractType2", "0.0.1", false);
		boolean c8 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "08.to be true again... c1v.0.0.1 is instantiable: " + c8);
		facade.setProducerContractTypeMustHaveConsumers("mqttProducerContractType2", "0.0.1", true);
		boolean c9 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "09.to be false...      c1v.0.0.1 is instantiable: " + c9);
		facade.setProducerContractTypeMustHaveConsumers("mqttProducerContractType2", "0.0.1", false);
		boolean c10 = facade.checkCurrentConfigurationTypeIsInstantiable();
		Log.SCENARIO.info("{}", () -> "10.to be true again... c1v.0.0.1 is instantiable: " + c10);
		facade.commitConfigurationType();
		var s2 = "The graph of configuration types is\n" + facade.toStringGraphOfConfigurationTypeInDotFormat();
		Log.SCENARIO.info("{}", () -> s2);
	}
}
