// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.scenarios;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import org.apache.logging.log4j.Level;

import mimosae.common.log.Log;
import mimosae.modelatruntime.MimosaeRuntimeModelInstance;
import mimosae.modelatruntime.MimosaeRuntimeModelType;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.exception.AlreadyExistsModelElementException;
import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.exception.IsNotDeployableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.IsNotInstantiableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.MalformedOperationDeclarationException;
import mimosae.modelatruntime.exception.UnknownInstanceException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.exception.UnmatchingTypesException;
import mimosae.modelatruntime.exception.WrongNewVersionException;
import mimosae.modelatruntime.exception.WrongNumberOfIdentifiersException;
import mimosae.planner.MimosaePlanner;
import mimosae.planner.exception.GoalCanBeSimplifiedToFalseException;
import mimosae.planner.exception.NoPlanFoundException;
import mimosae.planner.exception.ParsingDomainProblemFileException;

public class Scenario1Dot1PatchVersionOfAMicroservice {
	public static final String version1_0_0 = "1.0.0";
	public static final String versionPatch = "1.0.1";

	private Scenario1Dot1PatchVersionOfAMicroservice() {
		// empty
	}

	public static void main(String[] args) throws Exception {
		Log.setLevel(Log.SCENARIO, Level.INFO);
		// facade for types
		var facadeType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		// facade for instances
		var facadeInstance = MimosaeRuntimeModelInstance.getMimosaeRuntimeModelInstance();
		// facade for the planner
		var facadePlanner = MimosaePlanner.getMimosaeRuntimeModelInstance();
		// create initial GDP configuration type and initial GDP configuration
		createInitialGDEConfigurationTypeAndThenInitialGDEConfiguration(facadeType, facadeInstance);
		var configurationBeforeReconfiguration = facadeInstance.getIdentifierOfLastCommittedConfiguration();
		// create a patch version of microservice type 'Authentication' ("Auth")
		replaceAMicroserviceTypeWithPatchVersion(facadeType, "Auth", versionPatch);
		// change configuration type of current configuration to the last committed
		// configuration type
		facadeInstance.changeConfigurationTypeOfCurrentConfiguration(
				facadeType.getIdentifierOfLastCommittedConfigurationType());
		var s1 = facadeInstance.toStringGraphOfConfigurationInDotFormat();
		Log.SCENARIO.info("{}", () -> "\n====================\nGraph of configurations\n" + s1);
		var s2 = facadeType.toStringGraphOfConfigurationTypeInDotFormat();
		Log.SCENARIO.info("{}", () -> "\n====================\nGraph of configuration types\n" + s2);
		var s3 = facadeInstance.getIdentifierOfConfigurationTypeOfCurrentConfiguration();
		Log.SCENARIO.info("{}", () -> "The configuration type of the current configuration is \"" + s3 + "\"");
		// replace microservice 'AuthenticationInstance' ("Ai") of microservice type
		// 'Authentication' ("Auth")
		replaceMicroserviceInstancesWithPatchVersion(facadeInstance, "Ai", "Auth", versionPatch);
		var s4 = facadeInstance.toStringGraphOfConfigurationInDotFormat();
		Log.SCENARIO.info("{}", () -> "\n====================\nGraph of configurations\n" + s4);
		var s5 = configurationBeforeReconfiguration + " to last committed configuration ("
				+ facadeInstance.getIdentifierOfLastCommittedConfiguration() + ")";
		Log.SCENARIO.info("{}", () -> "Reconfigure with LPG from configurations " + s5);
		reconfigureWithLPG(facadePlanner, facadeInstance, configurationBeforeReconfiguration);
	}

	private static void reconfigureWithLPG(final MimosaePlanner facadePlanner,
			final MimosaeRuntimeModelInstance facadeInstance, final String configurationBeforeReconfiguration)
			throws MalformedIdentifierException, UnknownInstanceException, IOException, InterruptedException {
		facadePlanner.computeAIPlanToReconfigureWithLPG(configurationBeforeReconfiguration,
				facadeInstance.getIdentifierOfLastCommittedConfiguration());
	}

	@SuppressWarnings("unused")
	private static void reconfigureWithPDDL4J(final MimosaePlanner facadePlanner,
			final MimosaeRuntimeModelInstance facadeInstance, final String configurationBeforeReconfiguration)
			throws MalformedIdentifierException, UnknownInstanceException, IOException,
			ParsingDomainProblemFileException, GoalCanBeSimplifiedToFalseException, NoPlanFoundException {
		facadePlanner.computeAIPlanToReconfigureWithPDDL4J(configurationBeforeReconfiguration,
				facadeInstance.getIdentifierOfLastCommittedConfiguration());
	}

	private static void replaceMicroserviceInstancesWithPatchVersion(final MimosaeRuntimeModelInstance facadeInstance,
			final String instanceIdentifier, final String typeIdentifier, final String newVersion)
			throws UnknownTypeException, MalformedIdentifierException, UnknownInstanceException,
			IsCommittedAndCannotBeModifiedException, IsNotDeployableAndCannotBeCommittedException {
		Objects.requireNonNull(facadeInstance, "facadeInstance cannot be null");
		Objects.requireNonNull(instanceIdentifier, "instanceIdentifier cannot be null");
		Objects.requireNonNull(typeIdentifier, "typeIdentifier cannot be null");
		Objects.requireNonNull(newVersion, "newVersion cannot be null");
		facadeInstance.replaceMicroserviceWithPatchVersion(instanceIdentifier, typeIdentifier, version1_0_0,
				newVersion);
		facadeInstance.commitConfiguration();
		var s1 = facadeInstance.toStringCurrentConfiguration();
		Log.SCENARIO.info("{}", () -> "\n====================\nConfiguration after instantiation\n" + s1);
		Log.SCENARIO.info("{}", () -> "Test True - microservice UserServiceInstance is attached to some contracts...");
	}

	private static void replaceAMicroserviceTypeWithPatchVersion(final MimosaeRuntimeModelType facadeType,
			final String nameOfMicroservicType, final String newVersion) throws MalformedIdentifierException,
			AlreadyExistsModelElementException, UnknownTypeException, WrongNewVersionException,
			IsCommittedAndCannotBeModifiedException, IsNotInstantiableAndCannotBeCommittedException {
		Objects.requireNonNull(facadeType, "facadeType cannot be null");
		Objects.requireNonNull(nameOfMicroservicType, "nameOfMicroservicType cannot be null");
		Objects.requireNonNull(newVersion, "newVersion cannot be null");
		facadeType.createSameMicroserviceTypeAsPatchVersion(nameOfMicroservicType, version1_0_0, newVersion);
		facadeType.commitConfigurationType();
		var s1 = facadeType.toStringCurrentConfigurationType();
		Log.SCENARIO.info("{}", () -> "\n====================\nConfiguration type for the reconfiguration\n" + s1);
	}

	private static void createInitialGDEConfigurationTypeAndThenInitialGDEConfiguration(
			final MimosaeRuntimeModelType facadeType, final MimosaeRuntimeModelInstance facadeInstance)
			throws WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedOperationDeclarationException, UnknownTypeException, MalformedIdentifierException,
			UnknownInstanceException, UnmatchingTypesException, AlreadyConnectedException,
			IsCommittedAndCannotBeModifiedException, IsNotInstantiableAndCannotBeCommittedException,
			IsNotDeployableAndCannotBeCommittedException {
		Objects.requireNonNull(facadeType, "facade type cannot be null");
		Objects.requireNonNull(facadeInstance, "facade instance cannot be null");
		// Operation declarations - "name paramType... : returnType"
		// user service
		var usOps1 = new String[] { "CreateUser String String : gde.User", "CreateUserByUser gde.User : gde.User",
				"DeleteUser long", "FindUserByName String : gde.User", "FindUserById long : gde.User",
				"ListUsers : List<gde.User>" };
		var usOps2 = new String[] { "CheckPassword String String : boolean" };
		var usOps3 = new String[] { "CreateGroup String : gde.Group", "CreateGroupByGroup gde.Group : gde.Group",
				"DeleteGroup long", "FindGroupsByName String : List<gde.Group>", "FindGroupById long : gde.Group",
				"ListGroups : List<gde.Group>" };
		var usOps4 = new String[] { "AddUserToGroup long long", "AddUsersToGroup : List<Long> long",
				"RemoveUserFromGroup long long", "IsUserInGroup long long : boolean",
				"ListUsersInGroup long : List<gde.User>", "ListGroupsOfUser long : List<gde.Group>" };
		// project service
		var psOps1 = new String[] { "CreateProject gde.Project : gde.Project",
				"UpdateProject gde.Project : gde.Project", "DeleteProject long", "RestoreProject long",
				"FindProjectById long : gde.Project", "FindProjectsByName String : List<gde.Project>",
				"FindProjectsByName String : List<gde.Project>",
				"FindProjectsByCreationDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByDeletionDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByUpdateDate Date gde.DateComparator : List<gde.Project>",
				"FindProjectsByRestorationDate Date gde.DateComparator : List<gde.Project>",
				"ListProjects : List<gde.Project>" };
		var psOps2 = new String[] { "GetProjectLocker long : long", "IsProjectLocked long long : boolean",
				"SetProjectLockState long long boolean" };
		var psOps3 = new String[] { "ListProjectFiles long String : List<gde.GDEFile>",
				"AttachFileToProject long long long String", "DetachFileFromProject long long long String" };
		// file service
		var fsOps1 = new String[] { "CreateFile gde.GDEFile : gde.GDEFile", "UpdateFile gde.GDEFile : gde.GDEFile",
				"DeleteFile long", "RestoreFile long", "FindFileById long : gde.GDEFile", "ReadFile long : gde.GDEFile",
				"FindFilesByName String : List<gde.GDEFile>",
				"FindFilesByCreationDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByDeletionDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByUpdateDate Date gde.DateComparator : List<gde.GDEFile>",
				"FindFilesByRestorationDate Date gde.DateComparator : List<gde.GDEFile>",
				"ListFiles : List<gde.GDEFile>", "FindFilesToIndex : List<gde.GDEFile>",
				"FindFilesDeleted : List<gde.GDEFile>", "SetValid long boolean", "IsValid long : boolean",
				"SetIndexed long boolean", "IsIndexed long : boolean" };
		var fsOps2 = new String[] { "OpenFile long long : boolean", "CloseFile long long : boolean" };
		var fsOps3 = new String[] { "CreateChunk gde.Chunk : gde.Chunk", "ReadChunk long : gde.Chunk" };
		var fsOps4 = new String[] { "AttachChunkToFile long long", "DetachChunkFromFile long long",
				"FindChunksByFileId long : gde.Chunk", "FindChunksInfoByFileId long : gde.ChunkInfo" };
		// authentication
		var asOps1 = new String[] { "Login String String : String" };
		var asOps2 = new String[] { "GetLogin String : String" };
		// permission service
		var pmsOps1 = new String[] { "CreateService gde.Service : gde.Service", "DeleteService long : boolean",
				"UpdateService gde.Service : gde.Service", "FindServiceById long : gde.Service",
				"FindServiceByName String : gde.Service", "ListServices : List<gde.Service>" };

		var pmsOps2 = new String[] { "CreateMethod gde.Method : gde.Method", "DeleteMethod long : boolean",
				"UpdateMethod gde.Method : gde.Method", "FindMethodById long : gde.Method",
				"FindMethodByName String : gde.Method", "FindMethodByServiceNameMethodIndex String int : gde.Method",
				"FindMethodsByService long : List<gde.Method>", "ListMethods : List<gde.Method>" };
		var pmsOps3 = new String[] { "CreatePermission gde.Permission : gde.Permission",
				"DeletePermission long : boolean", "UpdatePermission gde.Permission : gde.Permission",
				"FindPermissionById long : gde.Permission", "FindPermissionsByMethodID long : List<gde.Permission>",
				"ListPermissions : List<gde.Permission>" };
		var pmsOps4 = new String[] { "FindPermissionsByGroupID long String : List<gde.Permission>",
				"FindPermissionsByUserID long String : List<gde.Permission>",
				"HasGroupThePermissionOn long String int String : boolean",
				"HasUserThePermissionOn long String int String : boolean",
				"CheckUserPermission String String int String : boolean" };
		// Server contract types
		// user service
		var userServiceusers = "USusers";
		var userServicecheckpassword = "UScheckpasswd";
		var userServicegroups = "USgroups";
		var userServiceusersGroups = "UService/usersgroups";
		facadeType.createServerContractType(userServiceusers, version1_0_0, usOps1);
		facadeType.createServerContractType(userServicecheckpassword, version1_0_0, usOps2);
		facadeType.createServerContractType(userServicegroups, version1_0_0, usOps3);
		facadeType.createServerContractType(userServiceusersGroups, version1_0_0, usOps4);
		var usServerContract = new String[] { userServiceusers, version1_0_0, userServicecheckpassword, version1_0_0,
				userServicegroups, version1_0_0, userServiceusersGroups, version1_0_0 };
		// project service
		var projectServiceprojects = "PSprojects";
		var projectServiceprojectlock = "PSlock";
		var projectServiceprojectfiles = "PSfiles";
		facadeType.createServerContractType(projectServiceprojects, version1_0_0, psOps1);
		facadeType.createServerContractType(projectServiceprojectlock, version1_0_0, psOps2);
		facadeType.createServerContractType(projectServiceprojectfiles, version1_0_0, psOps3);
		var psServerContract = new String[] { projectServiceprojects, version1_0_0, projectServiceprojectlock,
				version1_0_0, projectServiceprojectfiles, version1_0_0 };
		// file service
		var fileServicefiles = "FSfiles";
		var fileServicefilelock = "FSfilelock";
		var fileServicechunks = "FSchunks";
		var fileServicefilechunks = "FSfilechunks";
		facadeType.createServerContractType(fileServicefiles, version1_0_0, fsOps1);
		facadeType.createServerContractType(fileServicefilelock, version1_0_0, fsOps2);
		facadeType.createServerContractType(fileServicechunks, version1_0_0, fsOps3);
		facadeType.createServerContractType(fileServicefilechunks, version1_0_0, fsOps4);
		var fsServerContract = new String[] { fileServicefiles, version1_0_0, fileServicefilelock, version1_0_0,
				fileServicechunks, version1_0_0, fileServicefilechunks, version1_0_0 };
		// authentication
		var authenticationlogin = "Alogin";
		var authenticationgetlogin = "Agetlogin";
		facadeType.createServerContractType(authenticationlogin, version1_0_0, asOps1);
		facadeType.createServerContractType(authenticationgetlogin, version1_0_0, asOps2);
		var asServerContract = new String[] { authenticationlogin, version1_0_0, authenticationgetlogin, version1_0_0 };
		// permission service
		var permissionServiceservices = "PermSservices";
		var permissionServicemethods = "PermSmethods";
		var permissionServicepermissions = "PermSperms";
		var permissionServicecheckpermission = "PermScheck";
		facadeType.createServerContractType(permissionServiceservices, version1_0_0, pmsOps1);
		facadeType.createServerContractType(permissionServicemethods, version1_0_0, pmsOps2);
		facadeType.createServerContractType(permissionServicepermissions, version1_0_0, pmsOps3);
		facadeType.createServerContractType(permissionServicecheckpermission, version1_0_0, pmsOps4);
		var pmsServerContract = new String[] { permissionServiceservices, version1_0_0, permissionServicemethods,
				version1_0_0, permissionServicepermissions, version1_0_0, permissionServicecheckpermission,
				version1_0_0 };
		// Client contract types
		// user service
		var userServiceRequiresAuthenticationgetlogin = "USrAgetlogin";
		var userServiceRequiresPermissionServicecheckpermission = "USrPermScheck";
		var projectServiceRequiresAuthenticationgetlogin = "PSrAgetlogin";
		var projectServiceRequiresPermissionServicecheckpermission = "PSrPermScheck";
		var projectServiceRequiresFileServicefiles = "PSrFSfiles";
		var projectServiceRequiresUserServiceusers = "PSrUSusers";
		var fileServiceRequiresAuthenticationgetlogin = "FSrAgetlogin";
		var fileServiceRequiresPermissionServicecheckpermission = "FSrPermScheck";
		var fileServiceRequiresUserServiceusers = "FSrUSusers";
		var authenticationRequiresUserServicecheckpassword = "ArUScheckpassword";
		var permissionServiceRequiresAuthenticationgetlogin = "PermSrAgetlogin";
		var permissionServiceRequiresUserServiceusers = "PermSrUSusers";
		var permissionServiceRequiresUserServiceusersgroups = "PermSrUSusersgroups";
		facadeType.createClientContractType(userServiceRequiresAuthenticationgetlogin, version1_0_0, asOps2);
		facadeType.createClientContractType(userServiceRequiresPermissionServicecheckpermission, version1_0_0, pmsOps4);
		// project service
		facadeType.createClientContractType(projectServiceRequiresAuthenticationgetlogin, version1_0_0, asOps2);
		facadeType.createClientContractType(projectServiceRequiresPermissionServicecheckpermission, version1_0_0,
				pmsOps4);
		facadeType.createClientContractType(projectServiceRequiresFileServicefiles, version1_0_0, fsOps1);
		facadeType.createClientContractType(projectServiceRequiresUserServiceusers, version1_0_0, usOps1);
		// file service
		facadeType.createClientContractType(fileServiceRequiresAuthenticationgetlogin, version1_0_0, asOps2);
		facadeType.createClientContractType(fileServiceRequiresPermissionServicecheckpermission, version1_0_0, pmsOps4);
		facadeType.createClientContractType(fileServiceRequiresUserServiceusers, version1_0_0, usOps1);
		// authentication
		facadeType.createClientContractType(authenticationRequiresUserServicecheckpassword, version1_0_0, usOps2);
		// permission service
		facadeType.createClientContractType(permissionServiceRequiresAuthenticationgetlogin, version1_0_0, asOps2);
		facadeType.createClientContractType(permissionServiceRequiresUserServiceusers, version1_0_0, usOps1);
		facadeType.createClientContractType(permissionServiceRequiresUserServiceusersgroups, version1_0_0, usOps4);
		// Combine Client contract types for microservice type
		var usClientContract = new String[] { userServiceRequiresAuthenticationgetlogin, version1_0_0,
				userServiceRequiresPermissionServicecheckpermission, version1_0_0 };
		var psClientContract = new String[] { projectServiceRequiresAuthenticationgetlogin, version1_0_0,
				projectServiceRequiresPermissionServicecheckpermission, version1_0_0,
				projectServiceRequiresFileServicefiles, version1_0_0, projectServiceRequiresUserServiceusers,
				version1_0_0 };
		var fsClientContract = new String[] { fileServiceRequiresAuthenticationgetlogin, version1_0_0,
				fileServiceRequiresPermissionServicecheckpermission, version1_0_0, fileServiceRequiresUserServiceusers,
				version1_0_0 };
		var asClientContract = new String[] { authenticationRequiresUserServicecheckpassword, version1_0_0 };
		var pmsClientContract = new String[] { permissionServiceRequiresAuthenticationgetlogin, version1_0_0,
				permissionServiceRequiresUserServiceusers, version1_0_0,
				permissionServiceRequiresUserServiceusersgroups, version1_0_0 };
		// join client contract and server contract
		String[] usContracts = Arrays.copyOf(usClientContract, usClientContract.length + usServerContract.length);
		System.arraycopy(usServerContract, 0, usContracts, usClientContract.length, usServerContract.length);
		String[] psContracts = Arrays.copyOf(psClientContract, psClientContract.length + psServerContract.length);
		System.arraycopy(psServerContract, 0, psContracts, psClientContract.length, psServerContract.length);
		String[] fsContracts = Arrays.copyOf(fsClientContract, fsClientContract.length + fsServerContract.length);
		System.arraycopy(fsServerContract, 0, fsContracts, fsClientContract.length, fsServerContract.length);
		String[] asContracts = Arrays.copyOf(asClientContract, asClientContract.length + asServerContract.length);
		System.arraycopy(asServerContract, 0, asContracts, asClientContract.length, asServerContract.length);
		String[] pmsContracts = Arrays.copyOf(pmsClientContract, pmsClientContract.length + pmsServerContract.length);
		System.arraycopy(pmsServerContract, 0, pmsContracts, pmsClientContract.length, pmsServerContract.length);
		// Microservice type
		var userService = "US";
		var projectService = "PS";
		var fileService = "FS";
		var authentication = "Auth";
		var permissionService = "PermS";
		facadeType.createMicroserviceType(userService, version1_0_0, usContracts);
		facadeType.createMicroserviceType(projectService, version1_0_0, psContracts);
		facadeType.createMicroserviceType(fileService, version1_0_0, fsContracts);
		facadeType.createMicroserviceType(authentication, version1_0_0, asContracts);
		facadeType.createMicroserviceType(permissionService, version1_0_0, pmsContracts);
		// Connector types
		facadeType.createClientServerConnectorType(userServiceRequiresAuthenticationgetlogin, version1_0_0,
				authenticationgetlogin, version1_0_0);
		facadeType.createClientServerConnectorType(userServiceRequiresPermissionServicecheckpermission, version1_0_0,
				permissionServicecheckpermission, version1_0_0);

		facadeType.createClientServerConnectorType(projectServiceRequiresAuthenticationgetlogin, version1_0_0,
				authenticationgetlogin, version1_0_0);
		facadeType.createClientServerConnectorType(projectServiceRequiresPermissionServicecheckpermission, version1_0_0,
				permissionServicecheckpermission, version1_0_0);
		facadeType.createClientServerConnectorType(projectServiceRequiresFileServicefiles, version1_0_0,
				fileServicefiles, version1_0_0);
		facadeType.createClientServerConnectorType(projectServiceRequiresUserServiceusers, version1_0_0,
				userServiceusers, version1_0_0);

		facadeType.createClientServerConnectorType(fileServiceRequiresAuthenticationgetlogin, version1_0_0,
				authenticationgetlogin, version1_0_0);
		facadeType.createClientServerConnectorType(fileServiceRequiresPermissionServicecheckpermission, version1_0_0,
				permissionServicecheckpermission, version1_0_0);
		facadeType.createClientServerConnectorType(fileServiceRequiresUserServiceusers, version1_0_0, userServiceusers,
				version1_0_0);

		facadeType.createClientServerConnectorType(authenticationRequiresUserServicecheckpassword, version1_0_0,
				userServicecheckpassword, version1_0_0);

		facadeType.createClientServerConnectorType(permissionServiceRequiresAuthenticationgetlogin, version1_0_0,
				authenticationgetlogin, version1_0_0);
		facadeType.createClientServerConnectorType(permissionServiceRequiresUserServiceusers, version1_0_0,
				userServiceusers, version1_0_0);
		facadeType.createClientServerConnectorType(permissionServiceRequiresUserServiceusersgroups, version1_0_0,
				userServiceusersGroups, version1_0_0);
		facadeType.commitConfigurationType();
		var s1 = facadeType.toStringCurrentConfigurationType();
		Log.SCENARIO.info("{}", () -> "\n====================\nConfiguration type for the first instanciation \n" + s1);

		Objects.requireNonNull(facadeInstance, "facade instance cannot be null");
		facadeInstance.setConfigurationTypeOfCurrentConfiguration(
				MimosaeRuntimeModelType.getMimosaeRuntimeModelType().getIdentifierOfLastCommittedConfigurationType());
		// microservice instance
		var userServiceInstance = "USi";
		var projectServiceInstance = "PSi";
		var fileServiceInstance = "FSi";
		var authenticationInstance = "Ai";
		var permissionServiceInstance = "PermSi";
		facadeInstance.createMicroservice(userServiceInstance, userService, version1_0_0);
		facadeInstance.createMicroservice(projectServiceInstance, projectService, version1_0_0);
		facadeInstance.createMicroservice(fileServiceInstance, fileService, version1_0_0);
		facadeInstance.createMicroservice(authenticationInstance, authentication, version1_0_0);
		facadeInstance.createMicroservice(permissionServiceInstance, permissionService, version1_0_0);
		// link
		facadeInstance.linkClientServer(userServiceInstance, userService, version1_0_0,
				userServiceRequiresAuthenticationgetlogin, version1_0_0, authenticationInstance, authentication,
				version1_0_0, authenticationgetlogin, version1_0_0);
		facadeInstance.linkClientServer(userServiceInstance, userService, version1_0_0,
				userServiceRequiresPermissionServicecheckpermission, version1_0_0, permissionServiceInstance,
				permissionService, version1_0_0, permissionServicecheckpermission, version1_0_0);
		facadeInstance.linkClientServer(projectServiceInstance, projectService, version1_0_0,
				projectServiceRequiresAuthenticationgetlogin, version1_0_0, authenticationInstance, authentication,
				version1_0_0, authenticationgetlogin, version1_0_0);
		facadeInstance.linkClientServer(projectServiceInstance, projectService, version1_0_0,
				projectServiceRequiresPermissionServicecheckpermission, version1_0_0, permissionServiceInstance,
				permissionService, version1_0_0, permissionServicecheckpermission, version1_0_0);
		facadeInstance.linkClientServer(projectServiceInstance, projectService, version1_0_0,
				projectServiceRequiresFileServicefiles, version1_0_0, fileServiceInstance, fileService, version1_0_0,
				fileServicefiles, version1_0_0);
		facadeInstance.linkClientServer(projectServiceInstance, projectService, version1_0_0,
				projectServiceRequiresUserServiceusers, version1_0_0, userServiceInstance, userService, version1_0_0,
				userServiceusers, version1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileService, version1_0_0,
				fileServiceRequiresAuthenticationgetlogin, version1_0_0, authenticationInstance, authentication,
				version1_0_0, authenticationgetlogin, version1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileService, version1_0_0,
				fileServiceRequiresPermissionServicecheckpermission, version1_0_0, permissionServiceInstance,
				permissionService, version1_0_0, permissionServicecheckpermission, version1_0_0);
		facadeInstance.linkClientServer(fileServiceInstance, fileService, version1_0_0,
				fileServiceRequiresUserServiceusers, version1_0_0, userServiceInstance, userService, version1_0_0,
				userServiceusers, version1_0_0);
		facadeInstance.linkClientServer(authenticationInstance, authentication, version1_0_0,
				authenticationRequiresUserServicecheckpassword, version1_0_0, userServiceInstance, userService,
				version1_0_0, userServicecheckpassword, version1_0_0);
		facadeInstance.linkClientServer(permissionServiceInstance, permissionService, version1_0_0,
				permissionServiceRequiresAuthenticationgetlogin, version1_0_0, authenticationInstance, authentication,
				version1_0_0, authenticationgetlogin, version1_0_0);
		facadeInstance.linkClientServer(permissionServiceInstance, permissionService, version1_0_0,
				permissionServiceRequiresUserServiceusers, version1_0_0, userServiceInstance, userService, version1_0_0,
				userServiceusers, version1_0_0);
		facadeInstance.linkClientServer(permissionServiceInstance, permissionService, version1_0_0,
				permissionServiceRequiresUserServiceusersgroups, version1_0_0, userServiceInstance, userService,
				version1_0_0, userServiceusersGroups, version1_0_0);
		facadeInstance.commitConfiguration();
		var s2 = facadeInstance.toStringCurrentConfiguration();
		Log.SCENARIO.info("{}", () -> "\n====================\nConfiguration after instantiation\n" + s2);
		Log.SCENARIO.info("{}", () -> "Test True - microservice UserServiceInstance is attached to some contracts...");
	}
}
