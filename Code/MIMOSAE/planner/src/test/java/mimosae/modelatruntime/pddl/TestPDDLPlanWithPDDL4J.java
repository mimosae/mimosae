// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.pddl;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import mimosae.common.log.Log;
import mimosae.planner.pddl.Action;
import mimosae.planner.pddl.PDDLPlanWithPDDL4J;

public class TestPDDLPlanWithPDDL4J {
	private String domainFileName;
	private PDDLPlanWithPDDL4J pddlPlan;

	@Before
	public void setUp() throws Exception {
		domainFileName = "pddl" + File.separator + "mimosae_planner_domain.pddl";
		pddlPlan = new PDDLPlanWithPDDL4J();
		Log.setLevel(Log.SCENARIO, Level.INFO);
	}

	@After
	public void tearDown() throws Exception {
		domainFileName = null;
		pddlPlan = null;
	}

	private static <T> boolean hasDuplicate(Iterable<T> all) {
		Set<T> set = new HashSet<T>();
		// Set#add returns false if the set does not change, which
		// indicates that a duplicate element has been added.
		for (T each : all) {
			if (!set.add(each)) {
				return true;
			}
		}
		return false;
	}

	@Test
	public void test01() throws Exception {
		// from configuration: empty
		// to configuration:
		// - microservice1 with m1_clientcontract connected to m2_servercontract of
		// microservice2
		// - pubsubconnector1
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1
		// - microservice3 with m3_consumercontract1 connected to pubsubconnector1
		URL domainResource = Thread.currentThread().getContextClassLoader().getResource(domainFileName);
		URL problemResource = Thread.currentThread().getContextClassLoader().getResource(
				"pddl" + File.separator + "mimosae_planner_problem_01_create_from_empty_configuration.pddl");
		Assert.assertNotNull(domainResource);
		Assert.assertNotNull(problemResource);
		List<List<Action>> listOfLists = pddlPlan.planWithPDDL4J(new File(domainResource.toURI()),
				new File(problemResource.toURI()));
		listOfLists.removeIf(List::isEmpty);
		List<Action> actions = listOfLists.stream().<Action>flatMap(List::stream).collect(Collectors.toList());
		List<Action> toCompare = Arrays.asList(new Action[] { new Action("create_microservice microservice1"),
				new Action("create_databasesystem databasesystem1"), new Action("create_microservice microservice2"),
				new Action("create_databasesystem databasesystem2"), new Action("create_microservice microservice3"),
				new Action("create_pubsubconnector pubsubconnector1"),
				new Action("link_client_server microservice1 microservice2"),
				new Action("link_producer_to_pubsubconnector microservice1 pubsubconnector1"),
				new Action("link_consumer_to_pubsubconnector microservice3 pubsubconnector1"),
				new Action("connect_microservice_to_databasesystem microservice1 databasesystem1"),
				new Action("connect_microservice_to_databasesystem microservice2 databasesystem2") });
		Assert.assertFalse(hasDuplicate(actions));
		Assert.assertEquals(toCompare.size(), actions.size());
		for (Action action : toCompare) {
			Assert.assertTrue(actions.contains(action));
		}
	}

	@Test
	public void test02() throws Exception {
		// from configuration:
		// - microservice1 with m1_clientcontract connected to m2_servercontract of
		// microservice2
		// - pubsubconnector1
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1
		// - microservice3 with m3_consumercontract1 connected to pubsubconnector1
		// to configuration: empty
		URL domainResource = Thread.currentThread().getContextClassLoader().getResource(domainFileName);
		URL problemResource = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_02_remove_to_empty_configuration.pddl");
		Assert.assertNotNull(domainResource);
		Assert.assertNotNull(problemResource);
		List<List<Action>> listOfLists = pddlPlan.planWithPDDL4J(new File(domainResource.toURI()),
				new File(problemResource.toURI()));
		listOfLists.removeIf(List::isEmpty);
		List<Action> actions = listOfLists.stream().<Action>flatMap(List::stream).collect(Collectors.toList());
		List<Action> toCompare = Arrays.asList(new Action[] {
				new Action("unlink_client_server microservice1 microservice2"),
				new Action("unlink_producer_from_pubsubconnector microservice1 pubsubconnector1"),
				new Action("disconnect_microservice_from_databasesystem microservice1 databasesystem1"),
				new Action("disconnect_microservice_from_databasesystem microservice2 databasesystem2"),
				new Action("unlink_consumer_from_pubsubconnector microservice3 pubsubconnector1"),
				new Action("remove_microservice microservice1"), new Action("remove_databasesystem databasesystem1"),
				new Action("remove_microservice microservice2"), new Action("remove_databasesystem databasesystem2"),
				new Action("remove_microservice microservice3"),
				new Action("remove_pubsubconnector pubsubconnector1") });
		Assert.assertFalse(hasDuplicate(actions));
		Assert.assertEquals(toCompare.size(), actions.size());
		for (Action action : toCompare) {
			Assert.assertTrue(actions.contains(action));
		}
	}

	@Test
	public void test03() throws Exception {
		// from configuration:
		// - microservice1 with m1_clientcontract connected to m2_servercontract of
		// microservice2
		// - pubsubconnector1
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1
		// - microservice3 with m3_consumercontract1 connected to pubsubconnector1
		// to configuration:
		// - microservice1bis with m1bis_clientcontract connected to m2_servercontract
		// of
		// microservice2
		// - pubsubconnector1
		// - microservice1bis with m1bis_producercontract1 connected to pubsubconnector1
		// - microservice3 with m3_consumercontract1 connected to pubsubconnector1
		URL domainResource = Thread.currentThread().getContextClassLoader().getResource(domainFileName);
		URL problemResource = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_03_from_microservice1_to_microservice1bis.pddl");
		Assert.assertNotNull(domainResource);
		Assert.assertNotNull(problemResource);
		List<List<Action>> listOfLists = pddlPlan.planWithPDDL4J(new File(domainResource.toURI()),
				new File(problemResource.toURI()));
		listOfLists.removeIf(List::isEmpty);
		List<Action> actions = listOfLists.stream().<Action>flatMap(List::stream).collect(Collectors.toList());
		List<Action> toCompare = Arrays.asList(new Action[] { new Action("create_microservice microservice1bis"),
				new Action("disconnect_microservice_from_databasesystem microservice1 databasesystem1"),
				new Action("unlink_producer_from_pubsubconnector microservice1 pubsubconnector1"),
				new Action("unlink_client_server microservice1 microservice2"),
				new Action("remove_microservice microservice1"),
				new Action("link_client_server microservice1bis microservice2"),
				new Action("link_producer_to_pubsubconnector microservice1bis pubsubconnector1"),
				new Action("connect_microservice_to_databasesystem microservice1bis databasesystem1") });
		Assert.assertFalse(hasDuplicate(actions));
		Assert.assertEquals(toCompare.size(), actions.size());
		for (Action action : toCompare) {
			Assert.assertTrue(actions.contains(action));
		}
	}

	@Test
	public void test04() throws Exception {
		// from configuration:
		// - microservice1 with m1_clientcontract connected to m2_servercontract of
		// microservice2
		// - pubsubconnector1
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1
		// - microservice3 with m3_consumercontract1 connected to pubsubconnector1
		// to configuration:
		// - microservice1 with m1_clientcontract connected to m2bis_servercontract of
		// microservice2bis
		// - pubsubconnector1
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1
		// - microservice3 with m3_consumercontract1 connected to pubsubconnector1
		URL domainResource = Thread.currentThread().getContextClassLoader().getResource(domainFileName);
		URL problemResource = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_04_from_microservice2_to_microservice2bis.pddl");
		Assert.assertNotNull(domainResource);
		Assert.assertNotNull(problemResource);
		List<List<Action>> listOfLists = pddlPlan.planWithPDDL4J(new File(domainResource.toURI()),
				new File(problemResource.toURI()));
		listOfLists.removeIf(List::isEmpty);
		List<Action> actions = listOfLists.stream().<Action>flatMap(List::stream).collect(Collectors.toList());
		List<Action> toCompare = Arrays.asList(new Action[] {
				new Action("disconnect_microservice_from_databasesystem microservice2 databasesystem2"),
				new Action("unlink_client_server microservice1 microservice2"),
				new Action("create_microservice microservice2bis"), new Action("remove_microservice microservice2"),
				new Action("link_client_server microservice1 microservice2bis"),
				new Action("connect_microservice_to_databasesystem microservice2bis databasesystem2") });
		Assert.assertFalse(hasDuplicate(actions));
		Assert.assertEquals(toCompare.size(), actions.size());
		for (Action action : toCompare) {
			Assert.assertTrue(actions.contains(action));
		}
	}

	@Test
	public void test05() throws Exception {
		// from configuration:
		// - microservice1 with m1_clientcontract connected to m2_servercontract of
		// microservice2
		// - pubsubconnector1
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1
		// - microservice3 with m3_consumercontract1 connected to pubsubconnector1
		// to configuration:
		// - microservice1 with m1_clientcontract connected to m2bis_servercontract of
		// microservice2bis
		// - pubsubconnector1
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1
		// - microservice3bis with m3bis_consumercontract1 connected to pubsubconnector1
		URL domainResource = Thread.currentThread().getContextClassLoader().getResource(domainFileName);
		URL problemResource = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_05_from_microservice3_to_microservice3bis.pddl");
		Assert.assertNotNull(domainResource);
		Assert.assertNotNull(problemResource);
		List<List<Action>> listOfLists = pddlPlan.planWithPDDL4J(new File(domainResource.toURI()),
				new File(problemResource.toURI()));
		listOfLists.removeIf(List::isEmpty);
		List<Action> actions = listOfLists.stream().<Action>flatMap(List::stream).collect(Collectors.toList());
		List<Action> toCompare = Arrays.asList(new Action[] { new Action("create_microservice microservice3bis"),
				new Action("link_consumer_to_pubsubconnector microservice3bis pubsubconnector1"),
				new Action("unlink_consumer_from_pubsubconnector microservice3 pubsubconnector1"),
				new Action("remove_microservice microservice3") });
		Assert.assertFalse(hasDuplicate(actions));
		Assert.assertEquals(toCompare.size(), actions.size());
		for (Action action : toCompare) {
			Assert.assertTrue(actions.contains(action));
		}
	}

	@Test
	public void test06() throws Exception {
		// from configuration:
		// - microservice1 with m1_clientcontract connected to m2_servercontract of
		// microservice2
		// - pubsubconnector1
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1
		// - microservice3 with m3_consumercontract1 connected to pubsubconnector1
		// to configuration:
		// - microservice1 with m1_clientcontract connected to m2bis_servercontract of
		// microservice2bis
		// - pubsubconnector1bis
		// - microservice1 with m1_producercontract1 connected to pubsubconnector1bis
		// - microservice3s with m3s_consumercontract1 connected to pubsubconnector1bis
		URL domainResource = Thread.currentThread().getContextClassLoader().getResource(domainFileName);
		URL problemResource = Thread.currentThread().getContextClassLoader()
				.getResource("pddl/mimosae_planner_problem_06_from_pubsubconnector1_to_pubsubconnector1bis.pddl");
		Assert.assertNotNull(domainResource);
		Assert.assertNotNull(problemResource);
		List<List<Action>> listOfLists = pddlPlan.planWithPDDL4J(new File(domainResource.toURI()),
				new File(problemResource.toURI()));
		listOfLists.removeIf(List::isEmpty);
		List<Action> actions = listOfLists.stream().<Action>flatMap(List::stream).collect(Collectors.toList());
		List<Action> toCompare = Arrays.asList(
				new Action[] { new Action("unlink_consumer_from_pubsubconnector microservice3 pubsubconnector1"),
						new Action("unlink_producer_from_pubsubconnector microservice1 pubsubconnector1"),
						new Action("create_pubsubconnector pubsubconnector1bis"),
						new Action("remove_pubsubconnector pubsubconnector1"),
						new Action("link_producer_to_pubsubconnector microservice1 pubsubconnector1bis"),
						new Action("link_consumer_to_pubsubconnector microservice3 pubsubconnector1bis") });
		Assert.assertFalse(hasDuplicate(actions));
		Assert.assertEquals(toCompare.size(), actions.size());
		for (Action action : toCompare) {
			Assert.assertTrue(actions.contains(action));
		}
	}
}
