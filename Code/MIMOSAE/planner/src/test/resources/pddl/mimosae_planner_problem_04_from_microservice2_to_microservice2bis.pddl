(define (problem microservice_planner_problem)
    (:domain microservice_planner_domain)
    (:objects
        microservice1 - microservice
        databasesystem1 - databasesystem
        microservice2 - microservice
        databasesystem2 - databasesystem
        microservice3 - microservice
        pubsubconnector1 - pubsubconnector
        
        microservice2bis - microservice
    )
    (:init
        (microservice microservice1)
        (databasesystem databasesystem1)
        (microservice microservice2)
        (databasesystem databasesystem2)
        (microservice microservice3)
        (pubsubconnector pubsubconnector1)
        (client_server_link microservice1 microservice2)
        (producer_to_connector_link microservice1 pubsubconnector1)
        (consumer_to_connector_link microservice3 pubsubconnector1)
        (database_connection microservice1 databasesystem1)
        (database_connection microservice2 databasesystem2)
    )
    (:goal (and
        (not (microservice microservice2))
        (not (client_server_link microservice1 microservice2))
        (not (database_connection microservice2 databasesystem2))

        (microservice microservice1)
        (microservice microservice3)
        (pubsubconnector pubsubconnector1)
        (producer_to_connector_link microservice1 pubsubconnector1)
        (consumer_to_connector_link microservice3 pubsubconnector1)
        
        (microservice microservice2bis)
        (client_server_link microservice1 microservice2bis)
        (database_connection microservice2bis databasesystem2)
        )
    )
)
