(define (domain microservice_planner_domain)

(:requirements :adl :typing)

(:types
    architecturalentity - object
    microservice - architecturalentity
    databasesystem - architecturalentity
    connector - architecturalentity
    pubsubconnector - connector
)

(:predicates
    (microservice ?m - microservice)
    (databasesystem ?db - databasesystem)
    (pubsubconnector ?c - pubsubconnector)
    (client_server_link ?mc - microservice ?ms - microservice)
    (producer_to_connector_link ?m - microservice ?c - pubsubconnector)
    (consumer_to_connector_link ?m - microservice ?p - pubsubconnector)
    (database_connection ?mc - microservice ?db - databasesystem)
)

(:action create_microservice
    :parameters (?m - microservice)
    :precondition (and (not (microservice ?m)))
    :effect (and (microservice ?m))
)

(:action create_databasesystem
    :parameters (?db - databasesystem)
    :precondition (and (not (databasesystem ?db)))
    :effect (and (databasesystem ?db))
)

(:action remove_microservice
    :parameters (?m - microservice)
    :precondition (and (microservice ?m))
    :effect (and (not (microservice ?m)))
)

(:action remove_databasesystem
    :parameters (?db - databasesystem)
    :precondition (and (databasesystem ?db))
    :effect (and (not (databasesystem ?db)))
)

(:action link_client_server
    :parameters (?mc - microservice ?ms - microservice)
    :precondition (and (microservice ?mc)
                       (microservice ?ms)
                       (not (client_server_link ?mc ?ms)))
    :effect (and (client_server_link ?mc ?ms))
)

(:action unlink_client_server
    :parameters (?mc - microservice ?ms - microservice)
    :precondition (and (microservice ?mc)
                       (microservice ?ms)
                       (client_server_link ?mc ?ms))
    :effect (and (not (client_server_link ?mc ?ms)))
)
        
(:action create_pubsubconnector
    :parameters (?c - pubsubconnector)
    :precondition (and (not (pubsubconnector ?c)))
    :effect (and (pubsubconnector ?c))
)
        
(:action remove_pubsubconnector
    :parameters (?c - pubsubconnector)
    :precondition (and (pubsubconnector ?c))
    :effect (and (not (pubsubconnector ?c)))
)

(:action link_producer_to_pubsubconnector
    :parameters (?m - microservice ?c - pubsubconnector)
    :precondition (and (microservice ?m)
                       (pubsubconnector ?c)
                       (not (producer_to_connector_link ?m ?c)))
    :effect (and (producer_to_connector_link ?m ?c))
)

(:action unlink_producer_from_pubsubconnector
    :parameters (?m - microservice ?c - pubsubconnector)
    :precondition (and (microservice ?m)
                       (pubsubconnector ?c)
                       (producer_to_connector_link ?m ?c))
    :effect (and (not (producer_to_connector_link ?m ?c)))
)

(:action link_consumer_to_pubsubconnector
    :parameters (?m - microservice ?c - pubsubconnector)
    :precondition (and (microservice ?m)
                       (pubsubconnector ?c)
                       (not (consumer_to_connector_link ?m ?c)))
    :effect (and (consumer_to_connector_link ?m ?c))
)

(:action unlink_consumer_from_pubsubconnector
    :parameters (?m - microservice ?c - pubsubconnector)
    :precondition (and (microservice ?m)
                       (pubsubconnector ?c)
                       (consumer_to_connector_link ?m ?c))
    :effect (and (not (consumer_to_connector_link ?m ?c)))
)

(:action connect_microservice_to_databasesystem
    :parameters (?ms - microservice ?dbs - databasesystem)
    :precondition (and (microservice ?ms)
                       (databasesystem ?dbs)
                       (not (database_connection ?ms ?dbs)))
    :effect (and (database_connection ?ms ?dbs))
)

(:action disconnect_microservice_from_databasesystem
    :parameters (?ms - microservice ?dbs - databasesystem)
    :precondition (and (microservice ?ms)
                       (databasesystem ?dbs)
                       (database_connection ?ms ?dbs))
    :effect (and (not (database_connection ?ms ?dbs)))
)
)
