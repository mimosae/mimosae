(define (problem microservice_planner_problem)
    (:domain microservice_planner_domain)
    (:objects
        microservice1 - microservice
        databasesystem1 - databasesystem
        microservice2 - microservice
        databasesystem2 - databasesystem
        microservice3 - microservice
        pubsubconnector1 - pubsubconnector
        
        microservice1bis - microservice
    )
    (:init
        (microservice microservice1)
        (databasesystem databasesystem1)
        (microservice microservice2)
        (databasesystem databasesystem2)
        (microservice microservice3)
        (pubsubconnector pubsubconnector1)
        (client_server_link microservice1 microservice2)
        (producer_to_connector_link microservice1 pubsubconnector1)
        (consumer_to_connector_link microservice3 pubsubconnector1)
        (database_connection microservice1 databasesystem1)
        (database_connection microservice2 databasesystem2)
    )
    (:goal (and
        (not (microservice microservice1))
        (not (client_server_link microservice1 microservice2))
        (not (producer_to_connector_link microservice1 pubsubconnector1))
        (not (database_connection microservice1 databasesystem1))

        (microservice microservice1bis)
        
        (microservice microservice2)
        (microservice microservice3)
        (pubsubconnector pubsubconnector1)
        (consumer_to_connector_link microservice3 pubsubconnector1)
        
        (client_server_link microservice1bis microservice2)
        (producer_to_connector_link microservice1bis pubsubconnector1)
        (database_connection microservice1bis databasesystem1)
        )
    )
)
