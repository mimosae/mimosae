// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.planner.pddl;

import java.util.Arrays;
import java.util.Objects;

public class Action {
	private final ActionNames actionName;
	private final String[] arguments;

	public Action(final String[] words) {
		Objects.requireNonNull(words, "words cannot be null");
		actionName = ActionNames.isActionName(words[0]);
		arguments = new String[words.length - 1];
		System.arraycopy(words, 1, arguments, 0, arguments.length);
	}

	public Action(final String action) {
		this(action.split("\\s"));
	}

	public static boolean isOfInterest(final String actionName) {
		return Arrays.asList(ActionNames.values()).stream().map(ActionNames::getPddlName)
				.anyMatch(n -> n.equals(actionName));
	}

	public ActionNames getActionName() {
		return actionName;
	}

	public String[] getArguments() {
		return arguments;
	}

	@Override
	public int hashCode() {
		final var prime = 31;
		var result = 1;
		result = prime * result + Arrays.hashCode(arguments);
		result = prime * result + Objects.hash(actionName);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Action)) {
			return false;
		}
		Action other = (Action) obj;
		return actionName == other.actionName && Arrays.equals(arguments, other.arguments);
	}

	@Override
	public String toString() {
		return "Action [actionName=" + actionName + ", arguments=" + Arrays.toString(arguments) + "]";
	}
}
