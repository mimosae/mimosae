/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.planner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import mimosae.common.log.Log;
import mimosae.common.pddl.HappenBeforeRelationship;
import mimosae.common.pddl.HappenBeforeRelationships;
import mimosae.modelatruntime.MimosaeRuntimeModelInstance;
import mimosae.modelatruntime.MimosaeRuntimeModelType;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.UnknownInstanceException;
import mimosae.planner.exception.GoalCanBeSimplifiedToFalseException;
import mimosae.planner.exception.NoPlanFoundException;
import mimosae.planner.exception.ParsingDomainProblemFileException;
import mimosae.planner.pddl.Action;
import mimosae.planner.pddl.PDDLPlanWithLPG;
import mimosae.planner.pddl.PDDLPlanWithPDDL4J;

/**
 * This class is the Façade of the instance model at runtime. It implements the
 * Singleton design pattern. This façade requires the instantiation of the
 * Façade of the type model at runtime, e.g. class
 * {@link mimosae.modelatruntime.MimosaeRuntimeModelType}.
 * 
 * @author Denis Conan
 */
public final class MimosaePlanner {
	/**
	 * the singleton instance of this façade.
	 */
	private static MimosaePlanner instance;
	/**
	 * the singleton instance of the type façade.
	 */
	private MimosaeRuntimeModelType facadeModelType;
	/**
	 * the singleton instance of the instance façade.
	 */
	private MimosaeRuntimeModelInstance facadeModelInstance;

	/**
	 * constructs the façade.
	 */
	private MimosaePlanner() {
		facadeModelType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		facadeModelInstance = MimosaeRuntimeModelInstance.getMimosaeRuntimeModelInstance();
		assert invariant();
	}

	/**
	 * obtains the singleton object of the façade.
	 * 
	 * @return the façade.
	 */
	public static MimosaePlanner getMimosaeRuntimeModelInstance() {
		if (instance == null) {
			instance = new MimosaePlanner();
		}
		return instance;
	}

	/**
	 * computes the invariant of the façade singleton object.
	 * 
	 * @return true when the invariant is true.
	 */
	public boolean invariant() {
		return facadeModelType != null && facadeModelInstance != null;
	}

	/**
	 * computes the AI plan to transit from a source configuration to a target
	 * configuration using AI planner LPG.
	 * 
	 * @param fromConfigurationIdentifier the identifier of the source
	 *                                    configuration.
	 * @param toConfigurationIdentifier   the identifier of the target
	 *                                    configuration.
	 * @return the AI plan as a List of List of Action.
	 * @see Action
	 * @throws MalformedIdentifierException one of the identifier is malformed.
	 * @throws UnknownInstanceException     one of the configuration is unknown.
	 * @throws IOException                  the usage of the LPG AI planner requires
	 *                                      the manipulation of files; problem in
	 *                                      I/O.
	 */
	public List<List<Action>> computeAIPlanToReconfigureWithLPG(final String fromConfigurationIdentifier,
			final String toConfigurationIdentifier)
			throws MalformedIdentifierException, UnknownInstanceException, IOException {
		HappenBeforeRelationships happenBeforeConstraints = new HappenBeforeRelationships();
		List<List<String>> input = facadeModelInstance.computeAIPlanningToReconfigure(fromConfigurationIdentifier,
				toConfigurationIdentifier, happenBeforeConstraints);
		List<String> objects = input.get(0);
		List<String> init = input.get(1);
		List<String> goal = input.get(2);
		var problemFileName = "with_LPG_problem-";
		var tempFile = File.createTempFile(problemFileName, null);
		try (var f = new PrintWriter(new FileWriter(tempFile.getAbsoluteFile()));) {
			f.println("(define (problem microservice_planner_problem)");
			f.println("(:domain microservice_planner_domain)");
			f.println("(:objects " + objects.stream().collect(Collectors.joining("\n", "\n", "\n")) + ")");
			if (init.isEmpty()) {
				throw new IllegalStateException("the set of init predicates cannot be empty");
			}
			f.println("(:init " + init.stream().collect(Collectors.joining("\n", "\n", "\n")) + ")");
			if (goal.isEmpty()) {
				throw new IllegalStateException("the set of goal predicates cannot be empty");
			}
			f.println("(:goal (and" + goal.stream().collect(Collectors.joining("\n", "\n", "\n")) + ")");
			f.println(")\n)");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		if (Log.PLANNER.isInfoEnabled()) {
			Log.PLANNER.error("{}", () -> "PDDL problem file is " + tempFile.getAbsolutePath());
			try (var fis = new java.io.FileInputStream(tempFile.getAbsolutePath());
					var reader = new java.io.BufferedReader(new java.io.InputStreamReader(fis));
					var p = new PrintWriter(tempFile.getName());) {
				String s = reader.readLine();
				while (s != null) {
					final String toPrint = s;
					Log.PLANNER.info("{}", () -> toPrint);
					p.write(toPrint + "\n");
					s = reader.readLine();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		String domainFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl" + File.separator + "mimosae_planner_domain.pddl").getPath();
		Objects.requireNonNull(domainFileName, "domain file resource file not found");
		// the following is for executing with mvn exec:java
		// CHECKSTYLE:OFF: checkstyle:emptyblockcheck
		try (FileInputStream fin = new FileInputStream(domainFileName);) {
			// nop
			// CHECKSTYLE:ON: checkstyle:emptyblockcheck
		} catch (FileNotFoundException ex) {
			domainFileName = ".." + File.separator + "planner" + File.separator + "src" + File.separator + "main"
					+ File.separator + "resources" + File.separator + "pddl" + File.separator
					+ "mimosae_planner_domain.pddl";
		}
		var pddlPlan = new PDDLPlanWithLPG();
		var actions = pddlPlan.planWithLPG(domainFileName, tempFile.getAbsolutePath());
		if (Log.ON) {
			Log.PLANNER.debug("{}",
					() -> "- happen-before constraints NOT taken into account " + actions.stream()
							.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
									.collect(Collectors.joining("\n\t", "\n\t", "")))
							.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		}
		postTreatmentPlan(actions, happenBeforeConstraints);
		if (Log.ON) {
			Log.PLANNER.debug("{}",
					() -> "- happen-before constraints taken into account " + actions.stream()
							.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
									.collect(Collectors.joining("\n\t", "\n\t", "")))
							.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		}
		// remove empty lists, e.g. first empty entry added in
		// PDDLPlanWithLPG::planWithLPG
		tempFile.deleteOnExit();
		assert invariant();
		return actions;
	}

	/**
	 * add constraints "databasesystem before client microservice" and
	 * "publish-subscribe connector before client microservice".
	 * 
	 * @param actions                 collection of collections of actions without
	 *                                the constraints.
	 * @param happenBeforeConstraints the collection of "happen before" that are
	 *                                going to be collected.
	 * @return collection of collections of actions with the constraints.
	 */
	private List<List<Action>> postTreatmentPlan(final List<List<Action>> actions,
			final HappenBeforeRelationships happenBeforeConstraints) {
		var result = actions;
		if (Log.ON) {
			Log.PLANNER.trace("{}",
					() -> "- happen before constraints WITHOUT indexes: " + happenBeforeConstraints.toString());
		}
		for (int i = 0; i < result.size(); i++) {
			var parallelActions = result.get(i);
			for (int j = 0; j < parallelActions.size(); j++) {
				var action = parallelActions.get(j);
				var constraints = happenBeforeConstraints
						.getRelationshipsOfBefore(action.getArguments()[0].toLowerCase());
				if (constraints != null) {
					for (HappenBeforeRelationship constraint : constraints) {
						if (constraint.getBeforeIndex() == -1) {
							constraint.setBeforeIndex(i);
						}
					}
				}
				constraints = happenBeforeConstraints.getRelationshipsOfAfter(action.getArguments()[0].toLowerCase());
				if (constraints != null) {
					for (HappenBeforeRelationship constraint : constraints) {
						if (constraint.getAfterIndex() == -1) {
							constraint.setAfterIndex(i);
						}
					}
				}
			}
		}
		if (Log.ON) {
			Log.PLANNER.trace("{}",
					() -> "- happen before constraints WITH indexes: " + happenBeforeConstraints.toString());
		}
		if (Log.ON && happenBeforeConstraints.existHappenBeforRelationshipsWithNoAssignedIndex()) {
			Log.PLANNER.trace("{}", () -> "some indexes not assigned, set= " + happenBeforeConstraints.toString());
		}
		var needAnotherTraversal = true;
		while (needAnotherTraversal) {
			needAnotherTraversal = false;
			for (HappenBeforeRelationship constraint : happenBeforeConstraints.getHappenBeforeRelationships()) {
				if (constraint.getBeforeIndex() >= constraint.getAfterIndex()) {
					Action found = null;
					if (constraint.getBeforeIndex() >= 0) {
						for (int i = 0; i < actions.get(constraint.getBeforeIndex()).size(); i++) {
							var action = actions.get(constraint.getBeforeIndex()).get(i);
							if (action.getArguments()[0].toLowerCase().equals(constraint.getBeforeIdentifier())
									&& action.getActionName().isActionCreateEntity()) {
								found = action;
								break;
							}
						}
						if (found != null) {
							actions.get(constraint.getBeforeIndex()).remove(found);
							actions.get(constraint.getAfterIndex() - 1).add(found);
							constraint.setAfterIndex(constraint.getAfterIndex() - 1);
							needAnotherTraversal = true;
						}
					}
				}
			}
		}
		actions.removeIf(List::isEmpty);
		return result;
	}

	/**
	 * computes the AI plan to transit from a source configuration to a target
	 * configuration using AI planner PDDL4J.
	 * 
	 * @param fromConfigurationIdentifier the identifier of the source
	 *                                    configuration.
	 * @param toConfigurationIdentifier   the identifier of the target
	 *                                    configuration.
	 * @return the AI plan as a List of List of Action.
	 * @see Action
	 * @throws MalformedIdentifierException        one of the identifier is
	 *                                             malformed.
	 * @throws UnknownInstanceException            one of the configuration is
	 *                                             unknown.
	 * @throws IOException                         the usage of the LPG AI planner
	 *                                             requires the manipulation of
	 *                                             files; problem in I/O.
	 * @throws ParsingDomainProblemFileException   problem in the parsing of the
	 *                                             domain file.
	 * @throws GoalCanBeSimplifiedToFalseException problem in searching for a plan:
	 *                                             trivially false.
	 * @throws NoPlanFoundException                no plan can be found.
	 */
	public List<List<Action>> computeAIPlanToReconfigureWithPDDL4J(final String fromConfigurationIdentifier,
			final String toConfigurationIdentifier) throws MalformedIdentifierException, UnknownInstanceException,
			IOException, ParsingDomainProblemFileException, GoalCanBeSimplifiedToFalseException, NoPlanFoundException {
		HappenBeforeRelationships happenBeforeConstraints = new HappenBeforeRelationships();
		List<List<String>> input = facadeModelInstance.computeAIPlanningToReconfigure(fromConfigurationIdentifier,
				toConfigurationIdentifier, happenBeforeConstraints);
		List<String> objects = input.get(0);
		List<String> init = input.get(1);
		List<String> goal = input.get(2);
		String problemFileName = "with_LPG_problem" + Instant.now().toString().replace(":", "_") + ".pddl";
		try (var f = new PrintWriter(new FileWriter(problemFileName));) {
			f.println("(define (problem microservice_planner_problem)");
			f.println("(:domain microservice_planner_domain)");
			f.println("(:objects " + objects.stream().collect(Collectors.joining("\n", "\n", "\n")) + ")");
			if (init.isEmpty()) {
				throw new IllegalStateException("the set of init predicates cannot be empty");
			}
			f.println("(:init " + init.stream().collect(Collectors.joining("\n", "\n", "\n")) + ")");
			if (goal.isEmpty()) {
				throw new IllegalStateException("the set of goal predicates cannot be empty");
			}
			f.println("(:goal (and" + goal.stream().collect(Collectors.joining("\n", "\n", "\n")) + ")");
			f.println(")\n)");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		String domainFileName = Thread.currentThread().getContextClassLoader()
				.getResource("pddl" + File.separator + "mimosae_planner_domain.pddl").getPath();
		Objects.requireNonNull(domainFileName, "domain file resource file not found");
		// the following is for executing with mvn exec:java
		// CHECKSTYLE:OFF: checkstyle:emptyblockcheck
		try (FileInputStream fin = new FileInputStream(domainFileName);) {
			// nop
			// CHECKSTYLE:ON: checkstyle:emptyblockcheck
		} catch (FileNotFoundException ex) {
			domainFileName = ".." + File.separator + "modelatruntime" + File.separator + "src" + File.separator + "main"
					+ File.separator + "resources" + File.separator + "pddl" + File.separator
					+ "mimosae_planner_domain.pddl";
		}
		var pddlPlan = new PDDLPlanWithPDDL4J();
		List<List<Action>> actions = pddlPlan.planWithPDDL4J(new File(domainFileName), new File(problemFileName));
		postTreatmentPlan(actions, happenBeforeConstraints);
		if (Log.ON) {
			for (List<Action> actionAtSameTimestamp : actions) {
				Log.PLANNER.debug("{}", () -> actionAtSameTimestamp.stream().map(Action::toString)
						.collect(Collectors.joining(" || ", "- actions in parallel:\n  ", "\n")));
			}
		}
		assert invariant();
		return actions;
	}
}
