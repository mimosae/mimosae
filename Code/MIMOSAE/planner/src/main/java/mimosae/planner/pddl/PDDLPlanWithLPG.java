// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.planner.pddl;

import java.util.stream.Collectors;

import mimosae.common.log.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class PDDLPlanWithLPG {

	public PDDLPlanWithLPG() {
		// empty
	}

	private String logError(final BufferedReader buffer) {
		var line = "";
		var result = new StringBuilder();
		try {
			while ((line = buffer.readLine()) != null) {
				result.append(line);
			}
		} catch (IOException ex) {
			result.append(ex.getMessage());
		}
		return result.toString();
	}

	public List<List<Action>> planWithLPG(final String domainFileName, final String problemFileName)
			throws IOException {
		String lpgProgram = System.getenv("LPG_PROGRAM");
		Objects.requireNonNull(lpgProgram, "environment variable LPG_PROGRAM not found");
		Process wc = Runtime.getRuntime()
				.exec(lpgProgram + " -o " + domainFileName + " -f " + problemFileName + " -n 1");
		try {
			if (wc.waitFor() != 0) {
				var b = new BufferedReader(new InputStreamReader(wc.getErrorStream()));
				if (Log.ON) {
					Log.MODEL_AT_RUNTIME.error("{}", () -> logError(b));
				}
			}
		} catch (InterruptedException ex) {
			if (Log.ON) {
				Log.MODEL_AT_RUNTIME.warn("{}", () -> "command " + lpgProgram + " has been interrupted.");
			}
			// Restore interrupted state...
			Thread.currentThread().interrupt();
		}
		var b = new BufferedReader(new InputStreamReader(wc.getInputStream()));
		var s = "";
		List<List<Action>> actions = new ArrayList<>();
		// add empty list at the beginning in order to add constraint "start before"
		// if still empty after post-treatment, then remove
		actions.add(new ArrayList<>());
		var currentTimestamp = "";
		List<Action> actionsAtSameTime = new ArrayList<>();
		while ((s = b.readLine()) != null) {
			if (s.contains("(") && s.contains(")") && !s.contains("Time: (ACTION) [action Duration; action Cost]")) {
				var newTimestamp = s.substring(0, s.indexOf(":"));
				s = s.substring(s.indexOf("(") + 1, s.indexOf(")")).trim().toLowerCase();
				if (!s.isBlank()) {
					String[] action = s.split("\\s");
					if (Action.isOfInterest(action[0])) {
						if (currentTimestamp.equals(newTimestamp)) {
							actionsAtSameTime.add(new Action(action));
						} else {
							currentTimestamp = newTimestamp;
							actionsAtSameTime = new ArrayList<>();
							actionsAtSameTime.add(new Action(action));
							actions.add(actionsAtSameTime);
						}
					}
				}
			}
		}
		Log.PLANNER.info("{}",
				() -> "Plan to execute= " + actions.stream()
						.map(actionsInParallel -> actionsInParallel.stream().map(Action::toString)
								.collect(Collectors.joining("\n\t", "\n\t", "")))
						.collect(Collectors.joining("\nin parallel:", "\nin parallel:", "")));
		return actions;
	}

	public static void main(final String[] args) throws IOException {
		if (args.length != 2) {
			Log.MODEL_AT_RUNTIME.error("{}", () -> "usage : " + PDDLPlanWithLPG.class.getCanonicalName()
					+ " <domain file> <problem file>" + "\nbut args are " + Arrays.toString(args));
			return;
		}
		var plan = new PDDLPlanWithLPG();
		plan.planWithLPG(args[0], args[1]);
	}
}
