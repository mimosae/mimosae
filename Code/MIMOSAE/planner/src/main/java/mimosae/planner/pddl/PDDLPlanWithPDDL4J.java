// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.planner.pddl;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.parser.ErrorManager;
import fr.uga.pddl4j.planners.Planner;
import fr.uga.pddl4j.planners.ProblemFactory;
import fr.uga.pddl4j.planners.statespace.StateSpacePlannerFactory;
import mimosae.common.log.Log;
import mimosae.planner.exception.GoalCanBeSimplifiedToFalseException;
import mimosae.planner.exception.NoPlanFoundException;
import mimosae.planner.exception.ParsingDomainProblemFileException;

public class PDDLPlanWithPDDL4J {
	public PDDLPlanWithPDDL4J() {
		// empty
	}

	public List<List<Action>> planWithPDDL4J(final File domainFile, final File problemFile)
			throws ParsingDomainProblemFileException, GoalCanBeSimplifiedToFalseException, NoPlanFoundException {
		ErrorManager errorManager = null;
		var factory = ProblemFactory.getInstance();
		try {
			errorManager = factory.parse(domainFile, problemFile);
		} catch (IOException e) {
			throw new ParsingDomainProblemFileException(
					"Unexpected error when parsing the PDDL planning domain and/or problem files.");
		}
		if (!errorManager.isEmpty()) {
			errorManager.printAll();
			throw new ParsingDomainProblemFileException(
					"Unexpected error when parsing the PDDL planning domain and/or problem files.");
		}
		final CodedProblem pb = factory.encode();
//		System.out.println("Encoding problem done successfully (" + pb.getOperators().size() + " ops, "
//				+ pb.getRelevantFacts().size() + " facts).");
		if (!pb.isSolvable()) {
			throw new GoalCanBeSimplifiedToFalseException("Goal can be simplified to FALSE. No search will solve it.");
		}
		var stateSpacePlannerFactory = StateSpacePlannerFactory.getInstance();
		Planner planner = stateSpacePlannerFactory.getPlanner(Planner.Name.FF);
		var plan = planner.search(pb);
		if (plan == null) {
			throw new NoPlanFoundException("No plan found");
		}
		String[] planFound = pb.toString(plan).split("\\n");
		List<List<Action>> allActions = new ArrayList<>();
		// add empty list at the beginning in order to add constraint "start before"
		// if still empty after post-treatment, then remove
		allActions.add(new ArrayList<>());
		var currentTimestamp = "";
		List<Action> actionsAtSameTime = new ArrayList<>();
		for (String s : planFound) {
			if (s.contains("(") && s.contains(")")) {
				var newTimestamp = s.substring(0, s.indexOf(":"));
				s = s.substring(s.indexOf("(") + 1, s.indexOf(")")).trim();
				if (!s.isBlank()) {
					String[] action = s.split("\\s");
					if (Action.isOfInterest(action[0])) {
						if (currentTimestamp.equals(newTimestamp)) {
							actionsAtSameTime.add(new Action(action));
						} else {
							currentTimestamp = newTimestamp;
							actionsAtSameTime = new ArrayList<>();
							actionsAtSameTime.add(new Action(action));
							allActions.add(actionsAtSameTime);
						}
					}
				}
			}
		}
		String s = "Plan to execute=" + allActions;
		Log.MODEL_AT_RUNTIME.debug("{}", () -> s);
		return allActions;
	}

	public static void main(final String[] args) throws ParsingDomainProblemFileException,
			GoalCanBeSimplifiedToFalseException, NoPlanFoundException, URISyntaxException {
		if (args.length != 2) {
			Log.MODEL_AT_RUNTIME.error("{}",
					() -> "usage : " + PDDLPlanWithPDDL4J.class.getCanonicalName() + " <domain file> <problem file>");
			return;
		}
		var plan = new PDDLPlanWithPDDL4J();
		URL domainResource = Thread.currentThread().getContextClassLoader().getResource(args[0]);
		URL problemResource = Thread.currentThread().getContextClassLoader().getResource(args[1]);
		Objects.requireNonNull(domainResource, "domainFileName " + args[0] + " not found");
		Objects.requireNonNull(problemResource, "problemFileName " + args[1] + " not found");
		plan.planWithPDDL4J(new File(domainResource.toURI()), new File(problemResource.toURI()));
	}
}
