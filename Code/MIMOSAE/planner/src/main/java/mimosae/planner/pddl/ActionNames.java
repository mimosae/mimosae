// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.planner.pddl;

import java.util.Objects;

public enum ActionNames {
	/**
	 * create_microservice microservice.
	 */
	CREATE_MICROSERVICE("create_microservice"),
	/**
	 * remove_microservice microservice.
	 */
	REMOVE_MICROSERVICE("remove_microservice"),
	/**
	 * create_databasesystem databasesystem.
	 */
	CREATE_DATABASESYSTEM("create_databasesystem"),
	/**
	 * remove_databasesystem databasesystem.
	 */
	REMOVE_DATABASESYSTEM("remove_databasesystem"),
	/**
	 * link_client_server microserviceclient microserviceserver.
	 */
	LINK_CLIENT_SERVER("link_client_server"),
	/**
	 * unlink_client_server microserviceclient microserviceserver.
	 */
	UNLINK_CLIENT_SERVER("unlink_client_server"),
	/**
	 * create_pubsubconnector pubsubconnector.
	 */
	CREATE_PUBSUBCONNECTOR("create_pubsubconnector"),
	/**
	 * remove_pubsubconnector pubsubconnector.
	 */
	REMOVE_PUBSUBCONNECTOR("remove_pubsubconnector"),
	/**
	 * link_producer_to_pubsubconnector microservice pubsubconnector.
	 */
	LINK_PRODUCER_TO_PUBSUBCONNECTOR("link_producer_to_pubsubconnector"),
	/**
	 * unlink_producer_to_pubsubconnector microservice pubsubconnector.
	 */
	UNLINK_PRODUCER_FROM_PUBSUBCONNECTOR("unlink_producer_from_pubsubconnector"),
	/**
	 * link_consumer_to_pubsubconnector microservice pubsubconnector.
	 */
	LINK_CONSUMER_TO_PUBSUBCONNECTOR("link_consumer_to_pubsubconnector"),
	/**
	 * unlink_consumer_to_pubsubconnector microservice pubsubconnector.
	 */
	UNLINK_CONSUMER_FROM_PUBSUBCONNECTOR("unlink_consumer_from_pubsubconnector"),
	/**
	 * connect_microservice_to_databasesystem microservice databasesystem.
	 */
	CONNECT_MICROSERVICE_TO_DATABASESYSTEM("connect_microservice_to_databasesystem"),
	/**
	 * disconnect_microservice_from_databasesystem microservice databasesystem.
	 */
	DISCONNECT_MICROSERVICE_TO_DATABASESYSTEM("disconnect_microservice_from_databasesystem");

	private final String pddlName;

	private ActionNames(final String pddlName) {
		this.pddlName = pddlName;
	}

	public String getPddlName() {
		return pddlName;
	}

	public static ActionNames isActionName(final String actionName) {
		Objects.requireNonNull(actionName, "actionName cannot be null");
		for (ActionNames an : values()) {
			if (an.pddlName.equals(actionName)) {
				return an;
			}
		}
		throw new IllegalArgumentException("unknown action name (" + actionName + ")");
	}

	public boolean isActionCreateEntity() {
		return pddlName.contains("create_");
	}

	@Override
	public String toString() {
		return pddlName;
	}
}
