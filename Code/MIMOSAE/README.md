MIMOSAE middleware

Copyright (C) 2021
Contact: Denis.Conan_at_telecom-sudparis.eu and yuwei.wang_at_telecom-sudparis.eu

This product includes software developed at Télécom SudParis and EDF Lab, except for the AI Planner LPG and the classes referenced in file LICENSE.txt.

License: See file LICENSE.txt

### Software prerequisites:

  * JAVA version ≥ 11
  * Maven version ≥ 3.6.3
  * UNIX shell with shell variable `LPG_PROGRAM=.../LPG-td-1.4/lpg-td`


### Content

    * Maven modules:
        * `build-tools`: for build tools like Checkstyle
        * `modelatruntime`: the core module
        * `examples`: some examples using the core module

    * Software:
        * `LPG-td-1.4`: AI Planner [LPG](http://lpg.ing.unibs.it/); the UNIX runtime of the software was obtained from the authors

### Compilation and installation

    mvn clean install

### Execution of the examples

Go to the directory of the Maven `examples` module:

    cd examples/

Then try:

    mvn exec:java@lpg
    ...
    mvn exec:java@pddl4j
    ...
    mvn exec:java@wipmaintype
    ...
    mvn exec:java@wipmaininstance
    ...


