// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package external.util;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class TestAMQPTopicChecker {

	/**
	 * The usage demonstrated in RabbitMQ tutorial, step 5.
	 * 
	 * See
	 * <a href="https://www.rabbitmq.com/tutorials/tutorial-five-java.html" target=
	 * "rabbitmq">RabbitMQ tutorial, step 5</a>.
	 */
	@Test
	public void test() {
		Pattern p = Pattern.compile("a*b");
		Assert.assertTrue(p.matcher("aaaaab").matches());
		// Q1 with "*.orange.*" and Q2 with "*.*.rabbit" and "lazy.#".
		Pattern pQ1 = AMQPTopicChecker.toRegex("*.orange.*");
		Pattern pQ2B1 = AMQPTopicChecker.toRegex("*.*.rabbit");
		Pattern pQ2B2 = AMQPTopicChecker.toRegex("lazy.#");
		// A message with a routing key set to "quick.orange.rabbit" will be delivered
		// to both queues.
		Assert.assertTrue(pQ1.matcher("quick.orange.rabbit").matches());
		Assert.assertTrue(pQ2B1.matcher("quick.orange.rabbit").matches());
		Assert.assertFalse(pQ2B2.matcher("quick.orange.rabbit").matches());
		// Message "lazy.orange.elephant" also will go to both of them.
		Assert.assertTrue(pQ1.matcher("lazy.orange.elephant").matches());
		Assert.assertFalse(pQ2B1.matcher("lazy.orange.elephant").matches());
		Assert.assertTrue(pQ2B2.matcher("lazy.orange.elephant").matches());
		// On the other hand "quick.orange.fox" will only go to the first queue, and
		// "lazy.brown.fox" only to the second.
		Assert.assertTrue(pQ1.matcher("quick.orange.fox").matches());
		Assert.assertFalse(pQ2B1.matcher("quick.orange.fox").matches());
		Assert.assertFalse(pQ2B2.matcher("quick.orange.fox").matches());
		Assert.assertFalse(pQ1.matcher("lazy.brown.fox").matches());
		Assert.assertFalse(pQ2B1.matcher("lazy.brown.fox").matches());
		Assert.assertTrue(pQ2B2.matcher("lazy.brown.fox").matches());
		// "lazy.pink.rabbit" will be delivered to
		// the second queue only once, even though it matches two bindings.
		Assert.assertFalse(pQ1.matcher("lazy.pink.rabbit").matches());
		Assert.assertTrue(pQ2B1.matcher("lazy.pink.rabbit").matches());
		Assert.assertTrue(pQ2B2.matcher("lazy.pink.rabbit").matches());
		// "quick.brown.fox" doesn't match any binding so it will be discarded.
		Assert.assertFalse(pQ1.matcher("quick.brown.fox").matches());
		Assert.assertFalse(pQ2B1.matcher("quick.brown.fox").matches());
		Assert.assertFalse(pQ2B2.matcher("quick.brown.fox").matches());
	}

}
