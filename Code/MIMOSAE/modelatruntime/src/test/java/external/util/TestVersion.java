// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package external.util;

import org.junit.Assert;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;
import com.vdurmont.semver4j.Semver.SemverType;
import com.vdurmont.semver4j.Semver.VersionDiff;
import com.vdurmont.semver4j.SemverException;

public class TestVersion {
	private Semver sem;

	/**
	 * The usage demonstrated in Semver4j home page.
	 * 
	 * See
	 * <a href="https://github.com/vdurmont/semver4j" target= "semver4j">Semver4j
	 * home page</a>.
	 */
	@Test
	public void test() {
		// The Semver object
		// Create a version by using one of the 2 constructors.
		sem = new Semver("1.2.3-beta.4+sha899d8g79f87"); // Defaults to STRICT mode
		sem = new Semver("1.2.3-beta.4+sha899d8g79f87", SemverType.NPM); // Specify the mode
		// Is the version stable?
		Assert.assertTrue(new Semver("1.2.3").isStable());
		Assert.assertTrue(new Semver("1.2.3+sHa.0nSFGKjkjsdf").isStable());
		Assert.assertFalse(new Semver("0.1.2").isStable());
		Assert.assertFalse(new Semver("0.1.2+sHa.0nSFGKjkjsdf").isStable());
		Assert.assertFalse(new Semver("1.2.3-BETA.11+sHa.0nSFGKjkjsdf").isStable());
		// Comparing the versions.
		// isGreaterThan returns true if the version is strictly greater than the other
		// one.
		sem = new Semver("1.2.3");
		Assert.assertTrue(sem.isGreaterThan("1.2.2")); // true
		Assert.assertFalse(sem.isGreaterThan("1.2.4")); // false
		Assert.assertFalse(sem.isGreaterThan("1.2.3")); // false
		// isLowerThan returns true if the version is strictly lower than the other one.
		sem = new Semver("1.2.3");
		Assert.assertFalse(sem.isLowerThan("1.2.2")); // false
		Assert.assertTrue(sem.isLowerThan("1.2.4")); // true
		Assert.assertFalse(sem.isLowerThan("1.2.3")); // false
		// isEqualTo returns true if the versions are exactly the same.
		sem = new Semver("1.2.3+sha123456789");
		Assert.assertTrue(sem.isEqualTo("1.2.3+sha123456789")); // true
		Assert.assertFalse(sem.isEqualTo("1.2.3+shaABCDEFGHI")); // false
		// isEquivalentTo returns true if the versions are the same (does not take the
		// build information into account).
		sem = new Semver("1.2.3+sha123456789");
		Assert.assertTrue(sem.isEquivalentTo("1.2.3+sha123456789")); // true
		Assert.assertTrue(sem.isEquivalentTo("1.2.3+shaABCDEFGHI")); // true
		// If you want to know what is the main difference between 2 versions, use the
		// diff method. It will return a VersionDiff enum value among: NONE, MAJOR,
		// MINOR, PATCH, SUFFIX, BUILD. It will always return the biggest difference.
		sem = new Semver("1.2.3-beta.4+sha899d8g79f87");
		Assert.assertEquals(VersionDiff.NONE, sem.diff("1.2.3-beta.4+sha899d8g79f87")); // NONE
		Assert.assertEquals(VersionDiff.MAJOR, sem.diff("2.3.4-alpha.5+sha32iddfu987")); // MAJOR
		Assert.assertEquals(VersionDiff.MINOR, sem.diff("1.3.4-alpha.5+sha32iddfu987")); // MINOR
		Assert.assertEquals(VersionDiff.PATCH, sem.diff("1.2.4-alpha.5+sha32iddfu987")); // PATCH
		Assert.assertEquals(VersionDiff.SUFFIX, sem.diff("1.2.3-alpha.5+sha32iddfu987")); // SUFFIX
		Assert.assertEquals(VersionDiff.BUILD, sem.diff("1.2.3-beta.4+sha32iddfu987")); // BUILD
		// If you want to check if a version satisfies a requirement, use the satisfies
		// method.
		// In STRICT and LOOSE modes, the requirement can only be another version.
		Semver semStrict = new Semver("1.2.3", SemverType.STRICT);
		Assert.assertTrue(semStrict.satisfies("1.2.3")); // true
		Assert.assertFalse(semStrict.satisfies("1.2.2")); // false
		Assert.assertFalse(semStrict.satisfies("1.2.4")); // false
		try {
			semStrict.satisfies(">1.2.2"); // SemverException, incompatible requirement for a STRICT mode
		} catch (SemverException ex) {
			// normal
		}
		// NPM mode (those are just examples, check NPM documentation to see all the
		// cases)
		Semver semNPM = new Semver("1.2.3", SemverType.NPM);
		Assert.assertTrue(semNPM.satisfies(">1.2.2")); // true
		Assert.assertTrue(semNPM.satisfies("1.1.1 || 1.2.3 - 2.0.0")); // true
		Assert.assertFalse(semNPM.satisfies("1.1.*")); // false
		Assert.assertTrue(semNPM.satisfies("~1.2.1")); // true
		Assert.assertTrue(semNPM.satisfies("^1.1.1")); // true
		// COCOAPODS mode (those are just examples, check CocoaPods documentation to see
		// all the cases)
		Semver semPOD = new Semver("1.2.3", SemverType.COCOAPODS);
		Assert.assertTrue(semPOD.satisfies("> 1.2.2")); // true
		Assert.assertTrue(semPOD.satisfies("~> 1.2.1")); // true
		Assert.assertFalse(semPOD.satisfies("<= 1.1.1")); // false
		// IVY mode (those are just examples, check Ivy/gradle documentation to see all
		// the cases)
		Semver semIVY = new Semver("1.2.3", SemverType.IVY);
		Assert.assertTrue(semIVY.satisfies("1.2.+")); // true
		Assert.assertTrue(semIVY.satisfies("(,1.8.9]")); // true
		Assert.assertTrue(semIVY.satisfies("[0.2,1.4]")); // true
		// The Semver object is immutable. However, it provides a set of methods that
		// will help you create new versions:
		// withIncMajor() and withIncMajor(int increment) returns a Semver object with
		// the major part incremented
		sem = new Semver("1.2.3+sha123456789");
		Assert.assertEquals(new Semver("2.2.3+sha123456789"), sem.withIncMajor());
		// withIncMinor() and withIncMinor(int increment) returns a Semver object with
		// the minor part incremented
		Assert.assertEquals(new Semver("1.3.3+sha123456789"), sem.withIncMinor());
		// withIncPatch() and withIncPatch(int increment) returns a Semver object with
		// the patch part incremented
		Assert.assertEquals(new Semver("1.2.4+sha123456789"), sem.withIncPatch());
		// withClearedSuffix() returns a Semver object with no suffix
		sem = new Semver("1.2.3-BETA.11+sHa.0nSFGKjkjsdf");
		Assert.assertEquals(new Semver("1.2.3+sHa.0nSFGKjkjsdf"), sem.withClearedSuffix());
		// withClearedBuild() returns a Semver object with no build information
		Assert.assertEquals(new Semver("1.2.3-BETA.11"), sem.withClearedBuild());
		// You can also use built-in versioning methods such as:
		// nextMajor(): 1.2.3-beta.4+sha32iddfu987 => 2.0.0
		sem = new Semver("1.2.3-beta.4+sha32iddfu987");
		Assert.assertEquals(new Semver("2.0.0"), sem.nextMajor());
		// nextMinor(): 1.2.3-beta.4+sha32iddfu987 => 1.3.0
		Assert.assertEquals(new Semver("1.3.0"), sem.nextMinor());
		// nextPatch(): 1.2.3-beta.4+sha32iddfu987 => 1.2.4
		Assert.assertEquals(new Semver("1.2.4"), sem.nextPatch());
	}
}
