// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package external.util;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class TestMQTTTopicChecker {

	@Test
	public void test() {
		// check correctness of publication topic
		Assert.assertFalse(MQTTTopicChecker.pubTopicCheck("foo/#/bar"));
		Assert.assertFalse(MQTTTopicChecker.pubTopicCheck("foo/+bar"));
		Assert.assertFalse(MQTTTopicChecker.pubTopicCheck("foo/bar#"));
		Assert.assertTrue(MQTTTopicChecker.pubTopicCheck("foo/bar"));
		// check correctness of subscription topic
		Assert.assertFalse(MQTTTopicChecker.subcriptionCheck("foo/#/bar"));
		Assert.assertFalse(MQTTTopicChecker.subcriptionCheck("foo/+bar"));
		Assert.assertFalse(MQTTTopicChecker.subcriptionCheck("foo/bar#"));
		Assert.assertTrue(MQTTTopicChecker.subcriptionCheck("foo/bar/#"));
		Assert.assertTrue(MQTTTopicChecker.subcriptionCheck("foo/+/bar"));
		Assert.assertTrue(MQTTTopicChecker.subcriptionCheck("foo/bar"));
		// check matching of subscription and publicatin topic
		Pattern p1 = MQTTTopicChecker.toRegex("myhome/groundfloor/livingroom/temperature");
		Assert.assertTrue(p1.matcher("myhome/groundfloor/livingroom/temperature").matches());
		// single level: '+'
		Pattern p2 = MQTTTopicChecker.toRegex("myhome/groundfloor/+/temperature");
		Assert.assertTrue(p2.matcher("myhome/groundfloor/livingroom/temperature").matches());
		Assert.assertTrue(p2.matcher("myhome/groundfloor/kitchen/temperature").matches());
		Assert.assertFalse(p2.matcher("myhome/groundfloor/kitchen/brightness").matches());
		Assert.assertFalse(p2.matcher("myhome/firstfloor/kitchen/temperature").matches());
		Assert.assertFalse(p2.matcher("myhome/groundfloor/kitchen/fridge/temperature").matches());
		// multi level: #
		Pattern p3 = MQTTTopicChecker.toRegex("myhome/groundfloor/#");
		Assert.assertTrue(p3.matcher("myhome/groundfloor/livingroom/temperature").matches());
		Assert.assertTrue(p3.matcher("myhome/groundfloor/kitchen/temperature").matches());
		Assert.assertTrue(p3.matcher("myhome/groundfloor/kitchen/brightness").matches());
		Assert.assertFalse(p3.matcher("myhome/firstfloor/kitchen/temperature").matches());
		Assert.assertTrue(p3.matcher("myhome/groundfloor/kitchen/fridge/temperature").matches());

		// Assert.assertTrue(MQTTTopicChecker.match("myhome/groundfloor/livingroom/temperature",
		// "myhome/groundfloor/livingroom/temperature"));
		// Assert.assertTrue(MQTTTopicChecker.match("myhome/groundfloor/+/temperature",
		// "myhome/groundfloor/livingroom/temperature"));
	}
}
