// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.filtermodel;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

public class TestAMQPTopicBasedAdvertisementFilter {
	private AMQPTopicBasedAdvertisementFilter eventFilter;
	private String identifier;
	private String version;
	private String routingKey;

	@Before
	public void setUp() {
		identifier = "id";
		version = "0.0.1";
		routingKey = "a.b.c";
		eventFilter = new AMQPTopicBasedAdvertisementFilter(identifier, new Semver(version), routingKey);
	}

	@After
	public void teardown() {
		eventFilter = null;
		identifier = null;
		version = null;
		routingKey = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() {
		new AMQPTopicBasedAdvertisementFilter(null, new Semver(version), routingKey);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() {
		new AMQPTopicBasedAdvertisementFilter("", new Semver(version), routingKey);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() {
		new AMQPTopicBasedAdvertisementFilter(identifier, null, routingKey);
	}

	@Test(expected = NullPointerException.class)
	public void constructs3_1() {
		new AMQPTopicBasedAdvertisementFilter(identifier, new Semver(version), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_2() {
		new AMQPTopicBasedAdvertisementFilter(identifier, new Semver(version), "");
	}

	@Ignore("to do when AMQPTopicChecker::pubTopicCheck implemented")
	@Test(expected = IllegalArgumentException.class)
	public void constructs4() {
		new AMQPTopicBasedAdvertisementFilter(identifier, new Semver(version), "....");
	}

	@Test
	public void constructs5() {
		Assert.assertEquals(identifier, eventFilter.getIdentifier());
		Assert.assertEquals(routingKey, eventFilter.getTopic());
	}

	@Test(expected = NullPointerException.class)
	public void isCompatibleWith1() {
		eventFilter.isCompatibleWith(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void isCompatibleWith2() {
		eventFilter.isCompatibleWith(eventFilter);
	}

	@Test
	public void isCompatibleWith3() {
		AMQPTopicBasedSubscriptionFilter eventFilter2 = new AMQPTopicBasedSubscriptionFilter("id2", new Semver(version),
				"a.b.#");
		Assert.assertTrue(eventFilter.isCompatibleWith(eventFilter2));
	}
}
