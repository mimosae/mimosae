// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime;

import org.apache.logging.log4j.Level;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import mimosae.common.log.Log;
import mimosae.modelatruntime.instance.EmptyInitConfiguration;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;

public class TestFacadeInstance {

	private MimosaeRuntimeModelType facadeType = null;
	private MimosaeRuntimeModelInstance facadeInstance;

	@Before
	public void setUp() throws Exception {
		Log.setLevel(Log.MODEL_AT_RUNTIME, Level.WARN);
		facadeType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		// create types in next configuration type to commit
		facadeType.createMicroserviceType("microserviceType1", "0.0.1");
		var s1 = facadeType.toStringCurrentConfigurationType();
		Log.MODEL_AT_RUNTIME.info("{}", () -> s1);
		Assert.assertFalse(facadeType.checkCurrentConfigurationTypeIsInstantiable());
		var identifier = facadeType.getIdentifierOfLastCommittedConfigurationType();
		Assert.assertNotNull(identifier);
		Assert.assertEquals(EmptyInitConfigurationType.IDENTIFIER_EMPTY_INIT_CONFIGURATION_TYPE, identifier);
		facadeType.commitConfigurationType();
		identifier = facadeType.getIdentifierOfLastCommittedConfigurationType();
		Assert.assertNotNull(identifier);
		Assert.assertNotSame(EmptyInitConfigurationType.IDENTIFIER_EMPTY_INIT_CONFIGURATION_TYPE, identifier);
		var s2 = "Graph of configuration types:\n" + facadeType.toStringGraphOfConfigurationTypeInDotFormat();
		Log.MODEL_AT_RUNTIME.info("{}", () -> s2);
	}

	@After
	public void teardown() {
		facadeType.clearAllConfigurationTypes();
		facadeType = null;
	}

	public void constructs1_1() throws Exception {
		facadeInstance = MimosaeRuntimeModelInstance.getMimosaeRuntimeModelInstance();
		facadeInstance.setConfigurationTypeOfCurrentConfiguration(
				MimosaeRuntimeModelType.getMimosaeRuntimeModelType().getIdentifierOfLastCommittedConfigurationType());
		facadeInstance.createMicroservice("microservice1", "microserviceType1", "0.0.1");
		var s1 = facadeInstance.toStringCurrentConfiguration();
		Log.MODEL_AT_RUNTIME.info("{}", () -> s1);
		var identifier = facadeInstance.getIdentifierOfLastCommittedConfiguration();
		Assert.assertNotNull(identifier);
		Assert.assertEquals(EmptyInitConfiguration.IDENTIFIER_EMPTY_INIT_CONFIGURATION, identifier);
		facadeInstance.commitConfiguration();
		identifier = facadeInstance.getIdentifierOfLastCommittedConfiguration();
		Assert.assertNotNull(identifier);
		var s3 = "Graph of configuration types:\n" + facadeInstance.toStringGraphOfConfigurationInDotFormat();
		Log.MODEL_AT_RUNTIME.info("{}", () -> s3);
	}
}
