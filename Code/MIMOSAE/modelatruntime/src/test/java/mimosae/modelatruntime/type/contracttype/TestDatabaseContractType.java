// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

public class TestDatabaseContractType {
	private String identifier;
	private Semver version;

	@Before
	public void setUp() throws Exception {
		identifier = "id";
		version = new Semver("0.0.1");
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new DatabaseContractType(null, version);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new DatabaseContractType("", version);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new DatabaseContractType(identifier, null);
	}

	@Test
	public void constructs3() throws Exception {
		DatabaseContractType databaseContractType = new DatabaseContractType(identifier, version);
		Assert.assertEquals(identifier, databaseContractType.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, databaseContractType.getTypeIdentifier().getVersion());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new DatabaseContractType(null);
	}

	@Test
	public void constructsByCloning2() throws Exception {
		DatabaseContractType contractType = new DatabaseContractType(identifier, version);
		DatabaseContractType contractType2 = new DatabaseContractType(contractType);
		Assert.assertEquals(identifier, contractType.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType.getTypeIdentifier().getVersion());
		Assert.assertEquals(identifier, contractType2.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType2.getTypeIdentifier().getVersion());
	}
}
