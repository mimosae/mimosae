// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.instance.InstanceIdentifier;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.FullyQualifiedClass;
import mimosae.modelatruntime.type.contracttype.OperationDeclaration;
import mimosae.modelatruntime.type.contracttype.OperationSignature;
import mimosae.modelatruntime.type.contracttype.OperationType;
import mimosae.modelatruntime.type.contracttype.PrimitiveType;

public class TestClientContract {
	private String identifier;
	private Semver version;
	private OperationDeclaration declaration1;
	private OperationDeclaration declaration2;
	private ClientContractType clientContractType;
	private MicroserviceType microserviceType;
	private ConfigurationType configurationType;
	private Microservice microservice;

	@Before
	public void setUp() throws Exception {
		version = new Semver("0.0.1");
		OperationSignature signature1 = new OperationSignature("op", new FullyQualifiedClass("java.lang.Object"),
				PrimitiveType.BOOLEAN);
		OperationType returnType1 = PrimitiveType.INT;
		declaration1 = new OperationDeclaration(signature1, returnType1);
		OperationSignature signature2 = new OperationSignature("op", PrimitiveType.INT);
		OperationType returnType2 = null;
		declaration2 = new OperationDeclaration(signature2, returnType2);
		clientContractType = new ClientContractType("clientContractType1", version, declaration1, declaration2);
		microserviceType = new MicroserviceType("microserviceType1", version);
		microserviceType.addClientContracType(clientContractType);
		microservice = new Microservice("microservice1", microserviceType);
		identifier = ClientContract.computeIdentifier(microservice, clientContractType);
		configurationType = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType.addMicroserviceType(microserviceType);
		configurationType.addClientContractType(clientContractType);
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
		declaration1 = null;
		declaration2 = null;
		clientContractType = null;
		microserviceType = null;
		microservice = null;
		configurationType = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() throws Exception {
		new ClientContract((ClientContractType) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new ClientContract(clientContractType, null);
	}

	@Test
	public void constructs3() {
		ClientContract clientContract = new ClientContract(clientContractType, microservice);
		Assert.assertEquals(identifier, clientContract.getIdentifier());
		Assert.assertEquals("clientContractType1",
				clientContract.getClientContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, clientContract.getClientContractType().getTypeIdentifier().getVersion());
		Assert.assertEquals(2, clientContract.getClientContractType().getOperationDeclarations().size());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new ClientContract((ClientContract) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning2() throws Exception {
		ClientContract clientContract = new ClientContract(clientContractType, microservice);
		new ClientContract(clientContract, null);
	}

	@Test
	public void constructsByCloning3() throws Exception {
		ClientContract clientContract = new ClientContract(clientContractType, microservice);
		Assert.assertEquals(identifier, clientContract.getIdentifier());
		Assert.assertEquals("clientContractType1",
				clientContract.getClientContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, clientContract.getClientContractType().getTypeIdentifier().getVersion());
		Assert.assertEquals(2, clientContract.getClientContractType().getOperationDeclarations().size());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		ClientContract clientContract2 = new ClientContract(clientContract, microservice2);
		identifier = ClientContract.computeIdentifier(microservice2, clientContractType);
		Assert.assertEquals(identifier, clientContract2.getIdentifier());
		Assert.assertNotSame(microservice, clientContract2.getMicroservice());
		Assert.assertEquals(new InstanceIdentifier("microservice2", microserviceType.getTypeIdentifier()),
				clientContract2.getMicroservice().getIdentifier());
		Assert.assertEquals("clientContractType1",
				clientContract2.getClientContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, clientContract2.getClientContractType().getTypeIdentifier().getVersion());
		Assert.assertEquals(2, clientContract2.getClientContractType().getOperationDeclarations().size());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType1() throws Exception {
		new ClientContract((ClientContract) null, microservice, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType2() throws Exception {
		ClientContract clientContract = new ClientContract(clientContractType, microservice);
		new ClientContract(clientContract, null, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType3() throws Exception {
		ClientContract clientContract = new ClientContract(clientContractType, microservice);
		new ClientContract(clientContract, microservice, null);
	}

	@Test
	public void constructsByCloningWithNewConfigurationType4() throws Exception {
		ClientContract clientContract = new ClientContract(clientContractType, microservice);
		Assert.assertEquals(identifier, clientContract.getIdentifier());
		Assert.assertEquals("clientContractType1",
				clientContract.getClientContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, clientContract.getClientContractType().getTypeIdentifier().getVersion());
		Assert.assertEquals(2, clientContract.getClientContractType().getOperationDeclarations().size());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		configurationType.commit();
		var configurationType2 = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType2.addMicroserviceType(microserviceType);
		configurationType2.addClientContractType(clientContractType);
		ClientContract clientContract2 = new ClientContract(clientContract, microservice2, configurationType2);
		identifier = ClientContract.computeIdentifier(microservice2, clientContractType);
		Assert.assertEquals(identifier, clientContract2.getIdentifier());
		Assert.assertNotSame(microservice, clientContract2.getMicroservice());
		Assert.assertEquals(new InstanceIdentifier("microservice2", microserviceType.getTypeIdentifier()),
				clientContract2.getMicroservice().getIdentifier());
		Assert.assertEquals("clientContractType1",
				clientContract2.getClientContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, clientContract2.getClientContractType().getTypeIdentifier().getVersion());
		Assert.assertEquals(2, clientContract2.getClientContractType().getOperationDeclarations().size());
	}
}
