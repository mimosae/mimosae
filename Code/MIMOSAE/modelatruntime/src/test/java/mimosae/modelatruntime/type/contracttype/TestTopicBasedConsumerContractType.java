// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.filtermodel.AMQPTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.filtermodel.TopicBasedSubscriptionFilter;

public class TestTopicBasedConsumerContractType {
	private TopicBasedConsumerContractType contractType;
	private String identifier;
	private String version;
	private TopicBasedSubscriptionFilter eventFilter1;
	private TopicBasedSubscriptionFilter eventFilter2;
	private EventType eventType1;
	private EventType eventType2;

	@Before
	public void setUp() throws Exception {
		identifier = "id";
		version = "0.0.1";
		eventFilter1 = new MQTTTopicBasedSubscriptionFilter("f1", new Semver(version), "a/b/#");
		eventFilter2 = new AMQPTopicBasedSubscriptionFilter("f2", new Semver(version), "a.b.#");
		eventType1 = new EventType("et1", new Semver(version));
		eventType2 = new EventType("et2", new Semver(version));
		contractType = new TopicBasedConsumerContractType(identifier, new Semver(version), eventFilter1, eventType1,
				eventType2);
	}

	@After
	public void teardown() {
		contractType = null;
		identifier = null;
		version = null;
		eventFilter1 = null;
		eventFilter2 = null;
		eventType1 = null;
		eventType2 = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new TopicBasedConsumerContractType(null, new Semver(version), eventFilter1, eventType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new TopicBasedConsumerContractType("", new Semver(version), eventFilter1, eventType1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new TopicBasedConsumerContractType(identifier, null, eventFilter1, eventType1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs3() throws Exception {
		new TopicBasedConsumerContractType(identifier, new Semver(version), null, eventType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs4() throws Exception {
		new TopicBasedConsumerContractType(identifier, new Semver(version), eventFilter1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs5() throws Exception {
		new TopicBasedConsumerContractType(identifier, new Semver(version), eventFilter1, (EventType) null);
	}

	@Test
	public void constructs6_1() throws Exception {
		contractType = new TopicBasedConsumerContractType(identifier, new Semver(version), eventFilter1, eventType1,
				eventType2);
		Assert.assertEquals(identifier, contractType.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(contractType.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter1, contractType.getEventFilter());
		Assert.assertEquals(2, contractType.getEventTypes().size());
	}

	@Test
	public void constructs6_2() throws Exception {
		contractType = new TopicBasedConsumerContractType(identifier, new Semver(version), eventFilter2, eventType1,
				eventType2);
		Assert.assertEquals(identifier, contractType.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(contractType.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter2, contractType.getEventFilter());
		Assert.assertEquals(2, contractType.getEventTypes().size());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new TopicBasedConsumerContractType(null);
	}

	@Test
	public void constructsByCloning2() throws Exception {
		contractType = new TopicBasedConsumerContractType(identifier, new Semver(version), eventFilter2, eventType1,
				eventType2);
		Assert.assertEquals(identifier, contractType.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(contractType.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter2, contractType.getEventFilter());
		Assert.assertEquals(2, contractType.getEventTypes().size());
		var contractType2 = new TopicBasedConsumerContractType(contractType);
		Assert.assertEquals(identifier, contractType2.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType2.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(contractType2.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter2, contractType2.getEventFilter());
		Assert.assertEquals(2, contractType2.getEventTypes().size());
	}
}
