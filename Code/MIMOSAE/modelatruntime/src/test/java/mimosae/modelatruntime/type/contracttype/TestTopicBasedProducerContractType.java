// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.filtermodel.AMQPTopicBasedAdvertisementFilter;
import mimosae.modelatruntime.filtermodel.AMQPTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedAdvertisementFilter;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.filtermodel.TopicBasedAdvertisementFilter;

public class TestTopicBasedProducerContractType {
	private TopicBasedProducerContractType contractType1;
	private TopicBasedProducerContractType contractType2;
	private String identifier;
	private String version;
	private TopicBasedAdvertisementFilter eventFilter1;
	private TopicBasedAdvertisementFilter eventFilter2;
	private EventType eventType1;
	private EventType eventType2;

	@Before
	public void setUp() throws Exception {
		identifier = "id";
		version = "0.0.1";
		eventFilter1 = new MQTTTopicBasedAdvertisementFilter("f1", new Semver(version), "a/b/c");
		eventFilter2 = new AMQPTopicBasedAdvertisementFilter("f2", new Semver(version), "a.b.c");
		eventType1 = new EventType("et1", new Semver(version));
		eventType2 = new EventType("et2", new Semver(version));
		contractType1 = new TopicBasedProducerContractType(identifier, new Semver(version), eventFilter1, eventType1,
				eventType2);
		contractType2 = new TopicBasedProducerContractType(identifier, new Semver(version), eventFilter2, eventType1,
				eventType2);
	}

	@After
	public void teardown() {
		contractType1 = null;
		contractType2 = null;
		identifier = null;
		version = null;
		eventFilter1 = null;
		eventFilter2 = null;
		eventType1 = null;
		eventType2 = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new TopicBasedProducerContractType(null, new Semver(version), eventFilter1, eventType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new TopicBasedProducerContractType("", new Semver(version), eventFilter1, eventType1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new TopicBasedProducerContractType(identifier, null, eventFilter1, eventType1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs3() throws Exception {
		new TopicBasedProducerContractType(identifier, new Semver(version), null, eventType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs4() throws Exception {
		new TopicBasedProducerContractType(identifier, new Semver(version), eventFilter1, (EventType) null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs5() throws Exception {
		new TopicBasedProducerContractType(identifier, new Semver(version), eventFilter1);
	}

	@Test
	public void constructs6() {
		Assert.assertEquals(identifier, contractType1.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType1.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(contractType1.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter1, contractType1.getEventFilter());
		Assert.assertEquals(2, contractType1.getEventTypes().size());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new TopicBasedProducerContractType(null);
	}

	@Test
	public void constructsByCloning2() {
		Assert.assertEquals(identifier, contractType1.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType1.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(contractType1.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter1, contractType1.getEventFilter());
		Assert.assertEquals(2, contractType1.getEventTypes().size());
		var contractType2 = new TopicBasedProducerContractType(contractType1);
		Assert.assertEquals(identifier, contractType2.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType2.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(contractType2.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter1, contractType2.getEventFilter());
		Assert.assertEquals(2, contractType2.getEventTypes().size());
	}

	@Test(expected = NullPointerException.class)
	public void isCompatibleWith1() {
		contractType1.isCompatibleWith(null);
	}

	@Test
	public void isCompatibleWith2_1() throws Exception {
		eventType2 = new EventType("different", new Semver(version));
		MQTTTopicBasedSubscriptionFilter eventFilter3 = new MQTTTopicBasedSubscriptionFilter("f3", new Semver(version),
				"a/b/#");
		TopicBasedConsumerContractType contractType2 = new TopicBasedConsumerContractType(identifier,
				new Semver(version), eventFilter3, eventType2);
		Assert.assertFalse(contractType1.isCompatibleWith(contractType2));
	}

	@Test
	public void isCompatibleWith2_2() throws Exception {
		eventType2 = new EventType("different", new Semver(version));
		AMQPTopicBasedSubscriptionFilter eventFilter3 = new AMQPTopicBasedSubscriptionFilter("f3", new Semver(version),
				"a/b/#");
		TopicBasedConsumerContractType contractType3 = new TopicBasedConsumerContractType(identifier,
				new Semver(version), eventFilter3, eventType2);
		Assert.assertFalse(contractType2.isCompatibleWith(contractType3));
	}

	@Test
	public void isCompatibleWith3_1() throws Exception {
		MQTTTopicBasedSubscriptionFilter eventFilter3 = new MQTTTopicBasedSubscriptionFilter("f3", new Semver(version),
				"a/b/#");
		TopicBasedConsumerContractType contractType3 = new TopicBasedConsumerContractType(identifier,
				new Semver(version), eventFilter3, eventType2);
		Assert.assertFalse(contractType1.isCompatibleWith(contractType3));
	}

	@Test
	public void isCompatibleWith3_2() throws Exception {
		AMQPTopicBasedSubscriptionFilter eventFilter3 = new AMQPTopicBasedSubscriptionFilter("f3", new Semver(version),
				"a.b.#");
		TopicBasedConsumerContractType contractType3 = new TopicBasedConsumerContractType(identifier,
				new Semver(version), eventFilter3, eventType2);
		Assert.assertFalse(contractType2.isCompatibleWith(contractType3));
	}

	@Test
	public void isCompatibleWith4_1() throws Exception {
		MQTTTopicBasedSubscriptionFilter eventFilter3 = new MQTTTopicBasedSubscriptionFilter("f3", new Semver(version),
				"a/b/#");
		TopicBasedConsumerContractType contractType3 = new TopicBasedConsumerContractType(identifier,
				new Semver(version), eventFilter3, eventType1, eventType2);
		Assert.assertTrue(contractType1.isCompatibleWith(contractType3));
	}

	@Test
	public void isCompatibleWith4_2() throws Exception {
		AMQPTopicBasedSubscriptionFilter eventFilter3 = new AMQPTopicBasedSubscriptionFilter("f3", new Semver(version),
				"a.b.#");
		TopicBasedConsumerContractType contractType3 = new TopicBasedConsumerContractType(identifier,
				new Semver(version), eventFilter3, eventType1, eventType2);
		Assert.assertTrue(contractType2.isCompatibleWith(contractType3));
	}

	@Ignore("tested in TestPubSubConnectorType")
	@Test
	public void isInstantiable() {
		Assert.fail();
	}

}
