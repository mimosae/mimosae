// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.connector;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.instance.connnector.ClientServerConnector;
import mimosae.modelatruntime.instance.contract.ClientContract;
import mimosae.modelatruntime.instance.contract.ServerContract;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.connectortype.ClientServerConnectorType;
import mimosae.modelatruntime.type.connectortype.ConnectorType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.FullyQualifiedClass;
import mimosae.modelatruntime.type.contracttype.OperationDeclaration;
import mimosae.modelatruntime.type.contracttype.OperationSignature;
import mimosae.modelatruntime.type.contracttype.OperationType;
import mimosae.modelatruntime.type.contracttype.PrimitiveType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;

public class TestClientServerConnector {
	private String identifier1;
	private Semver version;
	private OperationDeclaration declaration1;
	private OperationDeclaration declaration2;
	private ClientContractType clientContractType;
	private ServerContractType serverContractType;
	private ClientServerConnectorType connectorType;
	private ClientContract clientContract1;
	private ServerContract serverContract1;
	private MicroserviceType microserviceType1;
	private Microservice microservice1;
	private Microservice microservice2;
	private MicroserviceType microserviceType2;
	private ConfigurationType configurationType1;
	private ConfigurationType configurationType2;

	@Before
	public void setUp() throws Exception {
		version = new Semver("0.0.1");
		OperationSignature signature1 = new OperationSignature("op", new FullyQualifiedClass("java.lang.Object"),
				PrimitiveType.BOOLEAN);
		OperationType returnType1 = PrimitiveType.INT;
		declaration1 = new OperationDeclaration(signature1, returnType1);
		OperationSignature signature2 = new OperationSignature("op", PrimitiveType.INT);
		OperationType returnType2 = null;
		declaration2 = new OperationDeclaration(signature2, returnType2);
		clientContractType = new ClientContractType("clientContractType", version, declaration1, declaration2);
		Assert.assertTrue(clientContractType.isInstantiable());
		serverContractType = new ServerContractType("serverContractType", version, declaration1, declaration2);
		connectorType = new ClientServerConnectorType(clientContractType, serverContractType);
		Assert.assertEquals(clientContractType, connectorType.getClientContractType());
		Assert.assertEquals(serverContractType, connectorType.getServerContractType());
		Assert.assertNotNull(connectorType.getClientContractType().getClientServerConnectorType());
		Assert.assertEquals(connectorType, connectorType.getClientContractType().getClientServerConnectorType());
		clientContractType.setMustHaveConnectorAttached(true);
		Assert.assertTrue(clientContractType.isInstantiable());
		Assert.assertTrue(connectorType.getClientContractType().isInstantiable());
		microserviceType1 = new MicroserviceType("microserviceType1", version);
		microserviceType1.addClientContracType(clientContractType);
		microserviceType1.addServerContracType(serverContractType);
		microservice1 = new Microservice("m1", microserviceType1);
		microservice2 = new Microservice("m2", microserviceType1);
		clientContract1 = new ClientContract(clientContractType, microservice1);
		serverContract1 = new ServerContract(serverContractType, microservice2);
		identifier1 = ClientServerConnector.computeIdentifier(clientContract1, serverContract1);
		configurationType1 = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType1.addMicroserviceType(microserviceType1);
		configurationType1.addClientContractType(clientContractType);
		configurationType1.addServerContractType(serverContractType);
		configurationType1.addClientServerConnectorType(connectorType);
		microserviceType2 = new MicroserviceType("microserviceType2", version);
		microserviceType2.addServerContracType(serverContractType);
		configurationType1.commit();
		configurationType2 = new ConfigurationType(configurationType1);
		configurationType2.commit();
	}

	@After
	public void teardown() {
		identifier1 = null;
		version = null;
		declaration1 = null;
		declaration2 = null;
		clientContractType = null;
		serverContractType = null;
		connectorType = null;
		clientContract1 = null;
		serverContract1 = null;
		microserviceType1 = null;
		microservice1 = null;
		microservice2 = null;
		microserviceType2 = null;
		configurationType1 = null;
		configurationType2 = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() throws Exception {
		new ClientServerConnector((ConnectorType) null, clientContract1, serverContract1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs2() throws Exception {
		ClientContractType clientContractType2 = new ClientContractType("clientContractType2", version, declaration1,
				declaration2);
		ClientContract clientContract2 = new ClientContract(clientContractType2, microservice1);
		new ClientServerConnector(connectorType, clientContract2, serverContract1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs3() throws Exception {
		new ClientServerConnector(connectorType, clientContract1, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs4() throws Exception {
		ServerContractType serverContractType2 = new ServerContractType("serverContractType2", version, declaration1,
				declaration2);
		ServerContract serverContract2 = new ServerContract(serverContractType2, microservice2);
		new ClientServerConnector(connectorType, clientContract1, serverContract2);
	}

	@Test
	public void constructs5() throws Exception {
		ClientServerConnector connector = new ClientServerConnector(connectorType, clientContract1, serverContract1);
		Assert.assertEquals(identifier1, connector.getIdentifier());
		Assert.assertEquals(connectorType, connector.getClientServerConnectorType());
		Assert.assertEquals(clientContract1, connector.getClientContract());
		Assert.assertEquals(connector, connector.getClientContract().getClientServerConnector());
		Assert.assertEquals(serverContract1, connector.getServerContract());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new ClientServerConnector((ClientServerConnector) null, clientContract1, serverContract1);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning2() throws Exception {
		ClientServerConnector connector = new ClientServerConnector(connectorType, clientContract1, serverContract1);
		new ClientServerConnector(connector, null, serverContract1);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning3() throws Exception {
		ClientServerConnector connector = new ClientServerConnector(connectorType, clientContract1, serverContract1);
		new ClientServerConnector(connector, clientContract1, null);
	}

	@Test
	public void constructsByCloning4() throws Exception {
		ClientServerConnector connector = new ClientServerConnector(connectorType, clientContract1, serverContract1);
		Microservice microservice3 = new Microservice(microservice1, configurationType2);
		Microservice microservice4 = new Microservice(microservice2, configurationType2);
		ClientServerConnector connector2 = new ClientServerConnector(connector,
				microservice3.getClientContract(
						ClientContract.computeIdentifier(microservice3, clientContract1.getClientContractType())),
				microservice4.getServerContract(
						ServerContract.computeIdentifier(microservice4, serverContract1.getServerContractType())));
		Assert.assertEquals(ClientServerConnector.computeIdentifier(clientContract1, serverContract1),
				connector2.getIdentifier());
		Assert.assertEquals(connectorType, connector2.getClientServerConnectorType());
		Assert.assertEquals(clientContract1, connector2.getClientContract());
		Assert.assertEquals(connector2, connector2.getClientContract().getClientServerConnector());
		Assert.assertEquals(serverContract1, connector2.getServerContract());
	}
}
