// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestOperationSignature {
	private String operationName;
	private FullyQualifiedClass cl;
	private OperationSignature op;

	@Before
	public void setUp() {
		operationName = "op";
		cl = new FullyQualifiedClass("java.lang.Object");
		op = new OperationSignature(operationName, cl, PrimitiveType.BOOLEAN);
	}

	@After
	public void teardown() {
		operationName = null;
		cl = null;
		op = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() {
		new OperationSignature(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() {
		new OperationSignature("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs2() {
		new OperationSignature(operationName, (OperationType) null);
	}

	@Test
	public void constructs3_1() {
		Assert.assertEquals(operationName, op.getOperationName());
		Assert.assertEquals(2, op.getArgumentTypes().size());
	}

	@Test
	public void constructs3_2() {
		Assert.assertEquals(operationName, op.getOperationName());
		new OperationSignature(operationName);
	}

}
