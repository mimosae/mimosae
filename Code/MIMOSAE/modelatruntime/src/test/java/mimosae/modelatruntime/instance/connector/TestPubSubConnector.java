// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.connector;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.instance.connnector.PubSubConnector;
import mimosae.modelatruntime.type.connectortype.PubSubConnectorType;

public class TestPubSubConnector {
	private String identifier;
	private Semver version;
	private PubSubConnectorType pubSubConnectorType1;

	@Before
	public void setUp() throws Exception {
		identifier = "id";
		version = new Semver("0.0.1");
		pubSubConnectorType1 = new PubSubConnectorType("pubSubConnectorType1", version);
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
		pubSubConnectorType1 = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() {
		new PubSubConnector(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() {
		new PubSubConnector("", pubSubConnectorType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs2() {
		new PubSubConnector("", null);
	}

	@Test
	public void constructs3() {
		PubSubConnector pubSubConnector = new PubSubConnector(identifier, pubSubConnectorType1);
		Assert.assertEquals(identifier, pubSubConnector.getIdentifier());
		Assert.assertEquals("pubSubConnectorType1",
				pubSubConnector.getPubSubConnectorType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, pubSubConnector.getPubSubConnectorType().getTypeIdentifier().getVersion());
	}
}
