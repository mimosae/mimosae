// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.instance.InstanceIdentifier;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;

public class TestDatabaseContract {
	private String identifier;
	private Semver version;
	private DatabaseContractType databaseContractType;
	private MicroserviceType microserviceType;
	private ConfigurationType configurationType;
	private Microservice microservice;

	@Before
	public void setUp() throws Exception {
		version = new Semver("0.0.1");
		databaseContractType = new DatabaseContractType("databaseContractType1", version);
		microserviceType = new MicroserviceType("microserviceType1", version);
		microserviceType.addDatabaseContracType(databaseContractType);
		microservice = new Microservice("microservice1", microserviceType);
		identifier = DatabaseContract.computeIdentifier(microservice, databaseContractType);
		configurationType = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType.addMicroserviceType(microserviceType);
		configurationType.addDatabaseContractType(databaseContractType);
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
		databaseContractType = null;
		microserviceType = null;
		microservice = null;
		configurationType = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() throws Exception {
		new DatabaseContract((DatabaseContractType) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new DatabaseContract(databaseContractType, null);
	}

	@Test
	public void constructs3() {
		DatabaseContract databaseContract = new DatabaseContract(databaseContractType, microservice);
		Assert.assertEquals(identifier, databaseContract.getIdentifier());
		Assert.assertEquals("databaseContractType1",
				databaseContract.getDatabaseContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, databaseContract.getDatabaseContractType().getTypeIdentifier().getVersion());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new DatabaseContract((DatabaseContract) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning2() throws Exception {
		DatabaseContract databaseContract = new DatabaseContract(databaseContractType, microservice);
		new DatabaseContract(databaseContract, null);
	}

	@Test
	public void constructsByCloning3() throws Exception {
		DatabaseContract databaseContract = new DatabaseContract(databaseContractType, microservice);
		Assert.assertEquals(identifier, databaseContract.getIdentifier());
		Assert.assertEquals("databaseContractType1",
				databaseContract.getDatabaseContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, databaseContract.getDatabaseContractType().getTypeIdentifier().getVersion());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		DatabaseContract databaseContract2 = new DatabaseContract(databaseContract, microservice2);
		identifier = DatabaseContract.computeIdentifier(microservice2, databaseContractType);
		Assert.assertEquals(identifier, databaseContract2.getIdentifier());
		Assert.assertNotSame(microservice, databaseContract2.getMicroservice());
		Assert.assertEquals(new InstanceIdentifier("microservice2", microserviceType.getTypeIdentifier()),
				databaseContract2.getMicroservice().getIdentifier());
		Assert.assertEquals("databaseContractType1",
				databaseContract2.getDatabaseContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, databaseContract2.getDatabaseContractType().getTypeIdentifier().getVersion());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType1() throws Exception {
		new DatabaseContract((DatabaseContract) null, microservice, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType2() throws Exception {
		DatabaseContract databaseContract = new DatabaseContract(databaseContractType, microservice);
		new DatabaseContract(databaseContract, null, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType3() throws Exception {
		DatabaseContract databaseContract = new DatabaseContract(databaseContractType, microservice);
		new DatabaseContract(databaseContract, microservice, null);
	}

	@Test
	public void constructsByCloningWithNewConfigurationType4() throws Exception {
		DatabaseContract databaseContract = new DatabaseContract(databaseContractType, microservice);
		Assert.assertEquals(identifier, databaseContract.getIdentifier());
		Assert.assertEquals("databaseContractType1",
				databaseContract.getDatabaseContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, databaseContract.getDatabaseContractType().getTypeIdentifier().getVersion());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		var configurationType2 = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType2.addMicroserviceType(microserviceType);
		configurationType2.addDatabaseContractType(databaseContractType);
		DatabaseContract databaseContract2 = new DatabaseContract(databaseContract, microservice2, configurationType2);
		identifier = DatabaseContract.computeIdentifier(microservice2, databaseContractType);
		Assert.assertEquals(identifier, databaseContract2.getIdentifier());
		Assert.assertNotSame(microservice, databaseContract2.getMicroservice());
		Assert.assertEquals(new InstanceIdentifier("microservice2", microserviceType.getTypeIdentifier()),
				databaseContract2.getMicroservice().getIdentifier());
		Assert.assertEquals("databaseContractType1",
				databaseContract2.getDatabaseContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, databaseContract2.getDatabaseContractType().getTypeIdentifier().getVersion());
	}
}
