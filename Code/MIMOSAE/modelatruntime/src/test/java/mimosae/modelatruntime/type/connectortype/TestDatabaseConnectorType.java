// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.connectortype;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.type.DatabaseSystemType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;

public class TestDatabaseConnectorType {
	private Semver version;
	private DatabaseContractType databaseContractType;
	private DatabaseSystemType databaseSystemType;

	@Before
	public void setUp() throws Exception {
		version = new Semver("0.0.1");
		databaseContractType = new DatabaseContractType("databaseContract", version);
		databaseSystemType = new DatabaseSystemType("databaseSystem", version);
	}

	@After
	public void teardown() {
		version = null;
		databaseContractType = null;
		databaseSystemType = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() throws Exception {
		new DatabaseConnectorType(null, databaseSystemType);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new DatabaseConnectorType(databaseContractType, null);
	}

	@Test
	public void constructs3() throws Exception {
		DatabaseConnectorType connector =	new DatabaseConnectorType(databaseContractType, databaseSystemType);
		Assert.assertEquals(databaseContractType, connector.getDatabaseContractType());
		Assert.assertEquals(databaseSystemType, connector.getDatabaseSystemType());
		Assert.assertNotNull(connector.getDatabaseContractType().getDatabaseConnectorType());
		Assert.assertEquals(connector, connector.getDatabaseContractType().getDatabaseConnectorType());
	}
}
