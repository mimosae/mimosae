// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.dependencymodel;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.type.MicroserviceType;

public class TestMicroserviceTypeDependency {
	private String depIdentifier;
	private Semver version1;
	private Semver version2;
	private String msIdentifier1;
	private String msIdentifier2;
	private String msIdentifier3;
	private MicroserviceType microserviceType1;
	private MicroserviceType microserviceType2_1;
	private MicroserviceType microserviceType2_2;
	private MicroserviceType microserviceType3;
	private MicroserviceTypeDependency dep;
	private String versionRequirement1;
	private String versionRequirement2;

	@Before
	public void setUp() throws Exception {
		depIdentifier = "dep1";
		version1 = new Semver("0.0.1", Semver.SemverType.NPM);
		version2 = new Semver("0.1.1", Semver.SemverType.NPM);
		versionRequirement1 = ">=0.0.1";
		versionRequirement2 = ">=0.1.1";
		msIdentifier1 = "ms1";
		msIdentifier2 = "ms2";
		msIdentifier3 = "ms3";
		microserviceType1 = new MicroserviceType(msIdentifier1, version1);
		microserviceType2_1 = new MicroserviceType(msIdentifier2, version1);
		microserviceType2_2 = new MicroserviceType(msIdentifier2, version2);
		microserviceType3 = new MicroserviceType(msIdentifier3, version1);
		dep = new MicroserviceTypeDependency(depIdentifier, msIdentifier2, versionRequirement2, microserviceType1);
	}

	@After
	public void teardown() {
		depIdentifier = null;
		version1 = null;
		version2 = null;
		msIdentifier1 = null;
		msIdentifier2 = null;
		msIdentifier3 = null;
		microserviceType1 = null;
		microserviceType2_1 = null;
		microserviceType2_2 = null;
		microserviceType3 = null;
		dep = null;
		versionRequirement1 = null;
		versionRequirement2 = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() {
		new MicroserviceTypeDependency(null, msIdentifier2, versionRequirement1, microserviceType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() {
		new MicroserviceTypeDependency("", msIdentifier2, versionRequirement1, microserviceType1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() {
		new MicroserviceTypeDependency(depIdentifier, msIdentifier2, versionRequirement1, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_1() {
		dep.addDependee((MicroserviceType) null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_2() {
		dep.addDependee(microserviceType2_2, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_3() {
		dep.addDependee(microserviceType2_2);
		dep.addDependee(microserviceType2_2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_4() {
		dep.addDependee(microserviceType3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_5() {
		dep.addDependee(microserviceType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_6() {
		dep.addDependee(microserviceType2_1);
	}

	@Test
	public void constructs4() {
		dep.addDependee(microserviceType2_2);
		Assert.assertEquals(depIdentifier, dep.getIdentifier());
		Assert.assertEquals(msIdentifier2, dep.getDependeeIdentifier());
		Assert.assertEquals(versionRequirement2, dep.getDependeeVersionRequirement());
		Assert.assertEquals(msIdentifier1, dep.getDepender().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version1, dep.getDepender().getTypeIdentifier().getVersion());
		Assert.assertEquals(1, dep.getDependees().size());
		Assert.assertTrue(dep.getDependees().contains(microserviceType2_2));
	}

}
