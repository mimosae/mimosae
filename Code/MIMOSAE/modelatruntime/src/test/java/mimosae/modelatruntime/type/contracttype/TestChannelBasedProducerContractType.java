// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.filtermodel.ChannelBasedFilter;

public class TestChannelBasedProducerContractType {
	private ChannelBasedProducerContractType contractType;
	private String identifier;
	private String version;
	private ChannelBasedFilter eventFilter;
	private EventType eventType1;
	private EventType eventType2;

	@Before
	public void setUp() throws Exception {
		identifier = "id";
		version = "0.0.1";
		eventFilter = new ChannelBasedFilter("channel", new Semver(version));
		eventType1 = new EventType("et1", new Semver(version));
		eventType2 = new EventType("et2", new Semver(version));
		contractType = new ChannelBasedProducerContractType(identifier, new Semver(version), eventFilter, eventType1,
				eventType2);
	}

	@After
	public void teardown() {
		contractType = null;
		identifier = null;
		version = null;
		eventFilter = null;
		eventType1 = null;
		eventType2 = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new ChannelBasedProducerContractType(null, new Semver(version), eventFilter, eventType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new ChannelBasedProducerContractType("", new Semver(version), eventFilter, eventType1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new ChannelBasedProducerContractType(identifier, null, eventFilter, eventType1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs3() throws Exception {
		new ChannelBasedProducerContractType(identifier, new Semver(version), null, eventType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs4() throws Exception {
		new ChannelBasedProducerContractType(identifier, new Semver(version), eventFilter, (EventType) null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs5() throws Exception {
		new ChannelBasedProducerContractType(identifier, new Semver(version), eventFilter);
	}

	@Test
	public void constructs6() {
		Assert.assertEquals(identifier, contractType.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(version, contractType.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter, contractType.getEventFilter());
		Assert.assertEquals(2, contractType.getEventTypes().size());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new ChannelBasedProducerContractType(null);
	}

	@Test
	public void constructsByCloning2() {
		Assert.assertEquals(identifier, contractType.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(version, contractType.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter, contractType.getEventFilter());
		Assert.assertEquals(2, contractType.getEventTypes().size());
		var contractType2 = new ChannelBasedProducerContractType(contractType);
		Assert.assertEquals(identifier, contractType2.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType2.getTypeIdentifier().getVersion().toString());
		Assert.assertFalse(version, contractType2.getEventTypes().isEmpty());
		Assert.assertEquals(eventFilter, contractType2.getEventFilter());
		Assert.assertEquals(2, contractType2.getEventTypes().size());	
	}

	@Test(expected = NullPointerException.class)
	public void isCompatibleWith1() {
		contractType.isCompatibleWith(null);
	}

	@Test
	public void isCompatibleWith2() throws Exception {
		eventType2 = new EventType("different", new Semver(version));
		ChannelBasedConsumerContractType contractType2 = new ChannelBasedConsumerContractType(identifier,
				new Semver(version), eventFilter, eventType2);
		Assert.assertFalse(contractType.isCompatibleWith(contractType2));
	}

	@Test
	public void isCompatibleWith3() throws Exception {
		ChannelBasedConsumerContractType contractType2 = new ChannelBasedConsumerContractType(identifier,
				new Semver(version), eventFilter, eventType2);
		Assert.assertFalse(contractType.isCompatibleWith(contractType2));
	}

	@Test
	public void isCompatibleWith4() throws Exception {
		ChannelBasedConsumerContractType contractType2 = new ChannelBasedConsumerContractType(identifier,
				new Semver(version), eventFilter, eventType1, eventType2);
		Assert.assertTrue(contractType.isCompatibleWith(contractType2));
	}

}
