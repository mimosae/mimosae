// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedAdvertisementFilter;
import mimosae.modelatruntime.filtermodel.TopicBasedAdvertisementFilter;
import mimosae.modelatruntime.instance.InstanceIdentifier;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.contracttype.ProducerContractType;
import mimosae.modelatruntime.type.contracttype.TopicBasedProducerContractType;

public class TestProducerContract {
	private TopicBasedProducerContractType producerContractType;
	private String identifier;
	private String version;
	private TopicBasedAdvertisementFilter eventFilter1;
	private EventType eventType1;
	private EventType eventType2;
	private MicroserviceType microserviceType;
	private ConfigurationType configurationType;
	private Microservice microservice;

	@Before
	public void setUp() throws Exception {
		version = "0.0.1";
		eventFilter1 = new MQTTTopicBasedAdvertisementFilter("f1", new Semver(version), "a/b/c");
		eventType1 = new EventType("et1", new Semver(version));
		eventType2 = new EventType("et2", new Semver(version));
		producerContractType = new TopicBasedProducerContractType("producerContractType", new Semver(version),
				eventFilter1, eventType1, eventType2);
		microserviceType = new MicroserviceType("microserviceType1", new Semver(version));
		microserviceType.addProducerContracType(producerContractType);
		microservice = new Microservice("microservice1", microserviceType);
		identifier = ProducerContract.computeIdentifier(microservice, producerContractType);
		configurationType = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType.addMicroserviceType(microserviceType);
		configurationType.addProducerContractType(producerContractType);
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
		eventFilter1 = null;
		eventType1 = null;
		eventType2 = null;
		producerContractType = null;
		microservice = null;
		configurationType = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() {
		new ProducerContract((ProducerContractType) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() {
		new ProducerContract(producerContractType, null);
	}

	@Test
	public void constructs3() {
		ProducerContract producerContract = new ProducerContract(producerContractType, microservice);
		Assert.assertEquals(identifier, producerContract.getIdentifier());
		Assert.assertEquals(producerContractType, producerContract.getProducerContractType());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() {
		new ProducerContract((ProducerContract) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning2() {
		ProducerContract producerContract = new ProducerContract(producerContractType, microservice);
		new ProducerContract(producerContract, null);
	}

	@Test
	public void constructsByCloning3() throws Exception {
		ProducerContract producerContract = new ProducerContract(producerContractType, microservice);
		Assert.assertEquals(identifier, producerContract.getIdentifier());
		Assert.assertEquals(producerContractType, producerContract.getProducerContractType());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		ProducerContract producerContract2 = new ProducerContract(producerContract, microservice2);
		identifier = ProducerContract.computeIdentifier(microservice2, producerContractType);
		Assert.assertEquals(identifier, producerContract2.getIdentifier());
		Assert.assertEquals(producerContractType, producerContract2.getProducerContractType());
		Assert.assertEquals(
				new InstanceIdentifier("microservice2", microservice2.getMicroserviceType().getTypeIdentifier()),
				producerContract2.getMicroservice().getIdentifier());
		Assert.assertNotSame(microservice, producerContract2.getMicroservice());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType1() throws Exception {
		new ProducerContract((ProducerContract) null, microservice, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType2() throws Exception {
		ProducerContract producerContract = new ProducerContract(producerContractType, microservice);
		new ProducerContract(producerContract, null, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType3() throws Exception {
		ProducerContract producerContract = new ProducerContract(producerContractType, microservice);
		new ProducerContract(producerContract, microservice, null);
	}

	@Test
	public void constructsByCloningWithNewConfigurationType4() throws Exception {
		ProducerContract producerContract = new ProducerContract(producerContractType, microservice);
		Assert.assertEquals(identifier, producerContract.getIdentifier());
		Assert.assertEquals(producerContractType, producerContract.getProducerContractType());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		var configurationType2 = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType2.addMicroserviceType(microserviceType);
		configurationType2.addProducerContractType(producerContractType);
		ProducerContract producerContract2 = new ProducerContract(producerContract, microservice2);
		identifier = ProducerContract.computeIdentifier(microservice2, producerContractType);
		Assert.assertEquals(identifier, producerContract2.getIdentifier());
		Assert.assertNotSame(microservice, producerContract2.getMicroservice());
		Assert.assertEquals(
				new InstanceIdentifier("microservice2", microservice2.getMicroserviceType().getTypeIdentifier()),
				producerContract2.getMicroservice().getIdentifier());
		Assert.assertEquals(producerContractType.getTypeIdentifier().getIdentifier(),
				producerContract2.getProducerContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(producerContractType.getTypeIdentifier().getVersion(),
				producerContract2.getProducerContractType().getTypeIdentifier().getVersion());
	}
}
