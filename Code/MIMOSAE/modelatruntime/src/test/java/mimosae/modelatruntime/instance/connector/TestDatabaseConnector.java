// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.connector;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.instance.DatabaseSystem;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.instance.connnector.DatabaseConnector;
import mimosae.modelatruntime.instance.contract.DatabaseContract;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.DatabaseSystemType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.connectortype.ConnectorType;
import mimosae.modelatruntime.type.connectortype.DatabaseConnectorType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;

public class TestDatabaseConnector {
	private String identifier1;
	private Semver version;
	private DatabaseContractType databaseContractType;
	private DatabaseSystemType databaseSystemType;
	private DatabaseConnectorType connectorType;
	private DatabaseContract databaseContract1;
	private DatabaseSystem databaseSystem1;
	private MicroserviceType microserviceType1;
	private Microservice microservice1;
	private ConfigurationType configurationType1;
	private ConfigurationType configurationType2;

	@Before
	public void setUp() throws Exception {
		version = new Semver("0.0.1");
		databaseContractType = new DatabaseContractType("databaseContractType", version);
		databaseSystemType = new DatabaseSystemType("databaseSystemType", version);
		connectorType = new DatabaseConnectorType(databaseContractType, databaseSystemType);
		Assert.assertEquals(databaseContractType, connectorType.getDatabaseContractType());
		Assert.assertEquals(databaseSystemType, connectorType.getDatabaseSystemType());
		Assert.assertNotNull(connectorType.getDatabaseContractType().getDatabaseConnectorType());
		Assert.assertEquals(connectorType, connectorType.getDatabaseContractType().getDatabaseConnectorType());
		microserviceType1 = new MicroserviceType("microserviceType1", version);
		microserviceType1.addDatabaseContracType(databaseContractType);
		microservice1 = new Microservice("m1", microserviceType1);
		databaseContract1 = new DatabaseContract(databaseContractType, microservice1);
		databaseSystem1 = new DatabaseSystem("databaseSystem1", databaseSystemType);
		identifier1 = DatabaseConnector.computeIdentifier(databaseContract1, databaseSystem1);
		configurationType1 = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType1.addMicroserviceType(microserviceType1);
		configurationType1.addDatabaseContractType(databaseContractType);
		configurationType1.addDatabaseSystemType(databaseSystemType);
		configurationType1.addDatabaseConnectorType(connectorType);
		configurationType1.commit();
		configurationType2 = new ConfigurationType(configurationType1);
		configurationType2.commit();
	}

	@After
	public void teardown() {
		identifier1 = null;
		version = null;
		databaseContractType = null;
		databaseSystemType = null;
		connectorType = null;
		databaseContract1 = null;
		databaseSystem1 = null;
		microserviceType1 = null;
		microservice1 = null;
		configurationType1 = null;
		configurationType2 = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() throws Exception {
		new DatabaseConnector((ConnectorType) null, databaseContract1, databaseSystem1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs2() throws Exception {
		DatabaseContractType databaseContractType2 = new DatabaseContractType("databaseContractType2", version);
		DatabaseContract databaseContract2 = new DatabaseContract(databaseContractType2, microservice1);
		new DatabaseConnector(connectorType, databaseContract2, databaseSystem1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs3() throws Exception {
		new DatabaseConnector(connectorType, databaseContract1, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs4() throws Exception {
		DatabaseSystemType databaseSystemType2 = new DatabaseSystemType("databaseSystemType2", version);
		DatabaseSystem databaseSystem2 = new DatabaseSystem("databaseSystem2", databaseSystemType2);
		new DatabaseConnector(connectorType, databaseContract1, databaseSystem2);
	}

	@Test
	public void constructs5() throws Exception {
		DatabaseConnector connector = new DatabaseConnector(connectorType, databaseContract1, databaseSystem1);
		Assert.assertEquals(identifier1, connector.getIdentifier());
		Assert.assertEquals(connectorType, connector.getDatabaseConnectorType());
		Assert.assertEquals(databaseContract1, connector.getDatabaseContract());
		Assert.assertEquals(connector, connector.getDatabaseContract().getDatabaseConnector());
		Assert.assertEquals(databaseSystem1, connector.getDatabaseSystem());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new DatabaseConnector((DatabaseConnector) null, databaseContract1, databaseSystem1);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning2() throws Exception {
		DatabaseConnector connector = new DatabaseConnector(connectorType, databaseContract1, databaseSystem1);
		new DatabaseConnector(connector, null, databaseSystem1);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning3() throws Exception {
		DatabaseConnector connector = new DatabaseConnector(connectorType, databaseContract1, databaseSystem1);
		new DatabaseConnector(connector, databaseContract1, null);
	}

	@Test
	public void constructsByCloning4() throws Exception {
		DatabaseConnector connector = new DatabaseConnector(connectorType, databaseContract1, databaseSystem1);
		Microservice microservice3 = new Microservice(microservice1, configurationType2);
		DatabaseConnector connector2 = new DatabaseConnector(connector,
				microservice3.getDatabaseContract(
						DatabaseContract.computeIdentifier(microservice3, databaseContract1.getDatabaseContractType())),
				databaseSystem1);
		Assert.assertEquals(DatabaseConnector.computeIdentifier(databaseContract1, databaseSystem1),
				connector2.getIdentifier());
		Assert.assertEquals(connectorType, connector2.getDatabaseConnectorType());
		Assert.assertEquals(databaseContract1, connector2.getDatabaseContract());
		Assert.assertEquals(connector2, connector2.getDatabaseContract().getDatabaseConnector());
		Assert.assertEquals(databaseSystem1, connector2.getDatabaseSystem());
	}
}
