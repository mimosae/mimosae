// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestOperationDeclaration {
	private OperationSignature signature;
	private OperationType returnType;
	private OperationDeclaration declaration;

	@Before
	public void setUp() {
		signature = new OperationSignature("op", new FullyQualifiedClass("java.lang.Object"), PrimitiveType.BOOLEAN);
		returnType = PrimitiveType.INT;
		declaration = new OperationDeclaration(signature, returnType);

	}

	@After
	public void teardown() {
		signature = null;
		returnType = null;
		declaration = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() {
		new OperationDeclaration(null, returnType);
	}

	@Test
	public void constructs2_1() {
		Assert.assertEquals(signature, declaration.getSignature());
		Assert.assertEquals(Optional.of(returnType), declaration.getReturnType());
	}

	@Test
	public void constructs2_2() {
		declaration = new OperationDeclaration(signature, null);
		Assert.assertEquals(signature, declaration.getSignature());
		Assert.assertEquals(Optional.empty(), declaration.getReturnType());
	}

	@Test
	public void isCompatibleWith1() {
		OperationSignature signature2 = new OperationSignature("op2", new FullyQualifiedClass("java.lang.Object"),
				PrimitiveType.BOOLEAN);
		OperationDeclaration declaration2 = new OperationDeclaration(signature2, returnType);
		Assert.assertFalse(declaration.isCompatibleWith(declaration2));
	}

	@Test
	public void isCompatibleWith2() {
		OperationSignature signature2 = new OperationSignature("op", PrimitiveType.BOOLEAN);
		OperationDeclaration declaration2 = new OperationDeclaration(signature2, returnType);
		Assert.assertFalse(declaration.isCompatibleWith(declaration2));
	}

	@Test
	public void isCompatibleWith3() {
		OperationType returnType2 = PrimitiveType.BOOLEAN;
		OperationDeclaration declaration2 = new OperationDeclaration(signature, returnType2);
		Assert.assertFalse(declaration.isCompatibleWith(declaration2));
	}

	@Test
	public void isCompatibleWith4() {
		OperationSignature signature2 = new OperationSignature("op", PrimitiveType.BOOLEAN);
		OperationType returnType2 = PrimitiveType.BOOLEAN;
		OperationDeclaration declaration2 = new OperationDeclaration(signature2, returnType2);
		Assert.assertFalse(declaration.isCompatibleWith(declaration2));
	}

	@Test
	public void isCompatibleWith5_1() {
		Assert.assertTrue(declaration.isCompatibleWith(declaration));
	}

	@Test
	public void isCompatibleWith5_2() {
		OperationDeclaration declaration2 = new OperationDeclaration(signature, returnType);
		Assert.assertTrue(declaration.isCompatibleWith(declaration2));
	}

}
