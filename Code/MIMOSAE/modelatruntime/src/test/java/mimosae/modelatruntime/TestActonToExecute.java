// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class TestActonToExecute {
	
	private String name;
	private String[] arguments;

	@Before
	public void setUp() throws Exception {
		name = "action";
		arguments = new String[] {"argument1", "argument2"};
	}

	@After
	public void teardown() {
		name = null;
		arguments = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new ActionLog(null, arguments);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new ActionLog("", arguments);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2_1() throws Exception {
		new ActionLog(name, (String[]) null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs2_2() throws Exception {
		new ActionLog(name, "argument", null);
	}
	
	@Test
	public void constructs3() throws Exception {
		ActionLog action = new ActionLog(name, arguments);
		Assert.assertEquals(name, action.getName());
		Assert.assertEquals(arguments.length, action.getArguments().length);
		Assert.assertEquals(arguments[0], action.getArguments()[0]);
		Assert.assertEquals(arguments[1], action.getArguments()[1]);
	}
}
