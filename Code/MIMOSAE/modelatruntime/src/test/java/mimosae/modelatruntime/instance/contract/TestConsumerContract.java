// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.filtermodel.TopicBasedSubscriptionFilter;
import mimosae.modelatruntime.instance.InstanceIdentifier;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.contracttype.TopicBasedConsumerContractType;

public class TestConsumerContract {
	private TopicBasedConsumerContractType consumerContractType;
	private String identifier;
	private String version;
	private TopicBasedSubscriptionFilter eventFilter1;
	private EventType eventType1;
	private EventType eventType2;
	private MicroserviceType microserviceType;
	private ConfigurationType configurationType;
	private Microservice microservice;

	@Before
	public void setUp() throws Exception {
		version = "0.0.1";
		eventFilter1 = new MQTTTopicBasedSubscriptionFilter("f1", new Semver(version), "a/b/#");
		eventType1 = new EventType("et1", new Semver(version));
		eventType2 = new EventType("et2", new Semver(version));
		consumerContractType = new TopicBasedConsumerContractType("consumerContractType", new Semver(version),
				eventFilter1, eventType1, eventType2);
		microserviceType = new MicroserviceType("microserviceType1", new Semver(version));
		microserviceType.addConsumerContracType(consumerContractType);
		microservice = new Microservice("microservice1", microserviceType);
		identifier = ConsumerContract.computeIdentifier(microservice, consumerContractType);
		configurationType = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType.addMicroserviceType(microserviceType);
		configurationType.addConsumerContractType(consumerContractType);
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
		eventFilter1 = null;
		eventType1 = null;
		eventType2 = null;
		consumerContractType = null;
		microserviceType = null;
		microservice = null;
		configurationType = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() {
		new ConsumerContract((ConsumerContract) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() {
		new ConsumerContract(consumerContractType, null);
	}

	@Test
	public void constructs3() {
		ConsumerContract consumerContract = new ConsumerContract(consumerContractType, microservice);
		Assert.assertEquals(identifier, consumerContract.getIdentifier());
		Assert.assertEquals(consumerContractType, consumerContract.getConsumerContractType());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() {
		new ConsumerContract((ConsumerContract) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning2() {
		ConsumerContract consumerContract = new ConsumerContract(consumerContractType, microservice);
		new ConsumerContract(consumerContract, null);
	}

	@Test
	public void constructsByCloning3() throws Exception {
		ConsumerContract consumerContract = new ConsumerContract(consumerContractType, microservice);
		Assert.assertEquals(identifier, consumerContract.getIdentifier());
		Assert.assertEquals(consumerContractType, consumerContract.getConsumerContractType());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		ConsumerContract consumerContract2 = new ConsumerContract(consumerContract, microservice2);
		identifier = ConsumerContract.computeIdentifier(microservice2, consumerContractType);
		Assert.assertEquals(identifier, consumerContract2.getIdentifier());
		Assert.assertEquals(consumerContractType, consumerContract2.getConsumerContractType());
		Assert.assertEquals(microservice2.getIdentifier(), consumerContract2.getMicroservice().getIdentifier());
		Assert.assertNotSame(microservice, consumerContract2.getMicroservice());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1WithNewConfigurationType1() throws Exception {
		new ConsumerContract((ConsumerContract) null, microservice, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType2() throws Exception {
		ConsumerContract consumerContract = new ConsumerContract(consumerContractType, microservice);
		new ConsumerContract(consumerContract, null, configurationType);
	}

	@Test
	public void constructsByCloningWithNewConfigurationType3() throws Exception {
		ConsumerContract consumerContract = new ConsumerContract(consumerContractType, microservice);
		Assert.assertEquals(identifier, consumerContract.getIdentifier());
		Assert.assertEquals(consumerContractType, consumerContract.getConsumerContractType());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		var configurationType2 = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType2.addMicroserviceType(microserviceType);
		configurationType2.addConsumerContractType(consumerContractType);
		ConsumerContract consumerContract2 = new ConsumerContract(consumerContract, microservice2, configurationType2);
		identifier = ConsumerContract.computeIdentifier(microservice2, consumerContractType);
		Assert.assertEquals(identifier, consumerContract2.getIdentifier());
		Assert.assertNotSame(microservice, consumerContract2.getMicroservice());
		Assert.assertEquals(
				new InstanceIdentifier("microservice2", microservice2.getMicroserviceType().getTypeIdentifier()),
				consumerContract2.getMicroservice().getIdentifier());
		Assert.assertEquals(consumerContractType.getTypeIdentifier().getIdentifier(),
				consumerContract2.getConsumerContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(consumerContractType.getTypeIdentifier().getVersion(),
				consumerContract2.getConsumerContractType().getTypeIdentifier().getVersion());
	}
}
