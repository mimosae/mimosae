// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.connectortype;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.FullyQualifiedClass;
import mimosae.modelatruntime.type.contracttype.OperationDeclaration;
import mimosae.modelatruntime.type.contracttype.OperationSignature;
import mimosae.modelatruntime.type.contracttype.OperationType;
import mimosae.modelatruntime.type.contracttype.PrimitiveType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;

public class TestClientServerConnectorType {
	private Semver version;
	private OperationDeclaration declaration1;
	private OperationDeclaration declaration2;
	private ClientContractType clientContractType;
	private ServerContractType serverContractType;
	private ClientServerConnectorType connector;

	@Before
	public void setUp() throws Exception {
		version = new Semver("0.0.1");
		OperationSignature signature1 = new OperationSignature("op", new FullyQualifiedClass("java.lang.Object"),
				PrimitiveType.BOOLEAN);
		OperationType returnType1 = PrimitiveType.INT;
		declaration1 = new OperationDeclaration(signature1, returnType1);
		OperationSignature signature2 = new OperationSignature("op", PrimitiveType.INT);
		OperationType returnType2 = null;
		declaration2 = new OperationDeclaration(signature2, returnType2);
		clientContractType = new ClientContractType("clientContract", version, declaration1, declaration2);
		Assert.assertTrue(clientContractType.isInstantiable());
		serverContractType = new ServerContractType("serverContract", version, declaration1, declaration2);
		connector = new ClientServerConnectorType(clientContractType, serverContractType);
		clientContractType.setMustHaveConnectorAttached(true);
		Assert.assertTrue(clientContractType.isInstantiable());
	}

	@After
	public void teardown() {
		version = null;
		declaration1 = null;
		declaration2 = null;
		clientContractType = null;
		serverContractType = null;
		connector = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() throws Exception {
		new ClientServerConnectorType(null, serverContractType);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new ClientServerConnectorType(clientContractType, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3() throws Exception {
		serverContractType = new ServerContractType("serverContract", version, declaration1);
		new ClientServerConnectorType(clientContractType, serverContractType);
	}

	@Test
	public void constructs6() {
		Assert.assertEquals(clientContractType, connector.getClientContractType());
		Assert.assertEquals(serverContractType, connector.getServerContractType());
		Assert.assertNotNull(connector.getClientContractType().getClientServerConnectorType());
		Assert.assertEquals(connector, connector.getClientContractType().getClientServerConnectorType());
		Assert.assertTrue(connector.getClientContractType().isInstantiable());
	}
}
