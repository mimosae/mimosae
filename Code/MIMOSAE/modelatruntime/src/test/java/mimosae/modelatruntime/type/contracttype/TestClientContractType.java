// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

public class TestClientContractType {
	private String identifier;
	private Semver version;
	private OperationDeclaration declaration1;
	private OperationDeclaration declaration2;
	private ClientContractType contract;
	private ServerContractType contract2;

	@Before
	public void setUp() throws Exception {
		identifier = "c1";
		version = new Semver("0.0.1");
		OperationSignature signature1 = new OperationSignature("op", new FullyQualifiedClass("java.lang.Object"),
				PrimitiveType.BOOLEAN);
		OperationType returnType1 = PrimitiveType.INT;
		declaration1 = new OperationDeclaration(signature1, returnType1);
		OperationSignature signature2 = new OperationSignature("op", PrimitiveType.INT);
		OperationType returnType2 = null;
		declaration2 = new OperationDeclaration(signature2, returnType2);
		contract = new ClientContractType(identifier, version, declaration1, declaration2);
		contract2 = new ServerContractType(identifier, version, declaration1, declaration2);
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
		declaration1 = null;
		declaration2 = null;
		contract = null;
		contract2 = null;

	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new ClientContractType(null, version, declaration1, declaration2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new ClientContractType("", version, declaration1, declaration2);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new ClientContractType(identifier, null, declaration1, declaration2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_1() throws Exception {
		new ClientContractType(identifier, version);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs3_2() throws Exception {
		new ClientContractType(identifier, version, (OperationDeclaration) null);
	}

	@Test
	public void constructs4() {
		Assert.assertEquals(identifier, contract.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contract.getTypeIdentifier().getVersion());
		Assert.assertEquals(2, contract.getOperationDeclarations().size());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() throws Exception {
		new ClientContractType(null);
	}

	@Test
	public void constructsByCloning2() {
		Assert.assertEquals(identifier, contract.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contract.getTypeIdentifier().getVersion());
		Assert.assertEquals(2, contract.getOperationDeclarations().size());
		var contractType2 = new ClientContractType(contract);
		Assert.assertEquals(identifier, contractType2.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, contractType2.getTypeIdentifier().getVersion());
		Assert.assertEquals(2, contractType2.getOperationDeclarations().size());
	}

	@Test(expected = NullPointerException.class)
	public void isCompatibleWith1() {
		contract.isCompatibleWith(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void isCompatibleWith2() {
		contract.isCompatibleWith(contract);
	}

	@Test
	public void isCompatibleWith3_1() throws Exception {
		ServerContractType contract3 = new ServerContractType(identifier, version, declaration1);
		Assert.assertFalse(contract.isCompatibleWith(contract3));
	}

	@Test
	public void isCompatibleWith3_2() {
		Assert.assertTrue(contract.isCompatibleWith(contract2));
	}

	@Ignore("tested in TestClientServerConnectorType")
	@Test
	public void isInstantiable() {
		Assert.fail();
	}
}
