// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.instance.InstanceIdentifier;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.contracttype.FullyQualifiedClass;
import mimosae.modelatruntime.type.contracttype.OperationDeclaration;
import mimosae.modelatruntime.type.contracttype.OperationSignature;
import mimosae.modelatruntime.type.contracttype.OperationType;
import mimosae.modelatruntime.type.contracttype.PrimitiveType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;

public class TestServerContract {
	private String identifier;
	private Semver version;
	private OperationDeclaration declaration1;
	private OperationDeclaration declaration2;
	private MicroserviceType microserviceType;
	private Microservice microservice;
	private ConfigurationType configurationType;
	private ServerContractType serverContractType;

	@Before
	public void setUp() throws Exception {
		version = new Semver("0.0.1");
		OperationSignature signature1 = new OperationSignature("op", new FullyQualifiedClass("java.lang.Object"),
				PrimitiveType.BOOLEAN);
		OperationType returnType1 = PrimitiveType.INT;
		declaration1 = new OperationDeclaration(signature1, returnType1);
		OperationSignature signature2 = new OperationSignature("op", PrimitiveType.INT);
		OperationType returnType2 = null;
		declaration2 = new OperationDeclaration(signature2, returnType2);
		serverContractType = new ServerContractType("serverContractType", version, declaration1, declaration2);
		microserviceType = new MicroserviceType("microserviceType1", version);
		microserviceType.addServerContracType(serverContractType);
		microservice = new Microservice("microservice1", microserviceType);
		identifier = ServerContract.computeIdentifier(microservice, serverContractType);
		configurationType = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType.addMicroserviceType(microserviceType);
		configurationType.addServerContractType(serverContractType);
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
		declaration1 = null;
		declaration2 = null;
		microserviceType = null;
		microservice = null;
		serverContractType = null;
		microserviceType = null;
		microservice = null;
		configurationType = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1() {
		new ServerContract((ServerContractType) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() {
		new ServerContract(serverContractType, null);
	}

	@Test
	public void constructs3() {
		ServerContract serverContract = new ServerContract(serverContractType, microservice);
		Assert.assertEquals(identifier, serverContract.getIdentifier());
		Assert.assertEquals("serverContractType",
				serverContract.getServerContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, serverContract.getServerContractType().getTypeIdentifier().getVersion());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() {
		new ServerContract((ServerContract) null, microservice);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning2() {
		ServerContract serverContract = new ServerContract(serverContractType, microservice);
		new ServerContract(serverContract, null);
	}

	@Test
	public void constructsByCloning3() throws Exception {
		ServerContract serverContract = new ServerContract(serverContractType, microservice);
		Assert.assertEquals(identifier, serverContract.getIdentifier());
		Assert.assertEquals("serverContractType",
				serverContract.getServerContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, serverContract.getServerContractType().getTypeIdentifier().getVersion());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		ServerContract serverContract2 = new ServerContract(serverContract, microservice2);
		identifier = ServerContract.computeIdentifier(microservice2, serverContractType);
		Assert.assertEquals(identifier, serverContract2.getIdentifier());
		Assert.assertNotSame(microservice, serverContract2.getMicroservice());
		Assert.assertEquals(
				new InstanceIdentifier("microservice2", microservice2.getMicroserviceType().getTypeIdentifier()),
				serverContract2.getMicroservice().getIdentifier());
		Assert.assertEquals("serverContractType",
				serverContract2.getServerContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, serverContract2.getServerContractType().getTypeIdentifier().getVersion());
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType1() throws Exception {
		new ServerContract((ServerContract) null, microservice, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType2() throws Exception {
		ServerContract serverContract = new ServerContract(serverContractType, microservice);
		new ServerContract(serverContract, null, configurationType);
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType3() throws Exception {
		ServerContract serverContract = new ServerContract(serverContractType, microservice);
		new ServerContract(serverContract, microservice, null);
	}

	@Test
	public void constructsByCloningWithNewConfigurationType4() throws Exception {
		ServerContract serverContract = new ServerContract(serverContractType, microservice);
		Assert.assertEquals(identifier, serverContract.getIdentifier());
		Assert.assertEquals("serverContractType",
				serverContract.getServerContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, serverContract.getServerContractType().getTypeIdentifier().getVersion());
		Microservice microservice2 = new Microservice("microservice2", microserviceType);
		var configurationType2 = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType2.addMicroserviceType(microserviceType);
		configurationType2.addServerContractType(serverContractType);
		ServerContract serverContract2 = new ServerContract(serverContract, microservice2, configurationType2);
		identifier = ServerContract.computeIdentifier(microservice2, serverContractType);
		Assert.assertEquals(identifier, serverContract2.getIdentifier());
		Assert.assertNotSame(microservice, serverContract2.getMicroservice());
		Assert.assertEquals(
				new InstanceIdentifier("microservice2", microservice2.getMicroserviceType().getTypeIdentifier()),
				serverContract2.getMicroservice().getIdentifier());
		Assert.assertEquals("serverContractType",
				serverContract2.getServerContractType().getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, serverContract2.getServerContractType().getTypeIdentifier().getVersion());
	}
}
