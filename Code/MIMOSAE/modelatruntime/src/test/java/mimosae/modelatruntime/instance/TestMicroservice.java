// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.filtermodel.AMQPTopicBasedAdvertisementFilter;
import mimosae.modelatruntime.filtermodel.AMQPTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedAdvertisementFilter;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.filtermodel.TopicBasedAdvertisementFilter;
import mimosae.modelatruntime.filtermodel.TopicBasedSubscriptionFilter;
import mimosae.modelatruntime.instance.contract.ClientContract;
import mimosae.modelatruntime.instance.contract.ConsumerContract;
import mimosae.modelatruntime.instance.contract.DatabaseContract;
import mimosae.modelatruntime.instance.contract.ProducerContract;
import mimosae.modelatruntime.instance.contract.ServerContract;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.DatabaseSystemType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.TypeIdentifier;
import mimosae.modelatruntime.type.connectortype.ClientServerConnectorType;
import mimosae.modelatruntime.type.connectortype.DatabaseConnectorType;
import mimosae.modelatruntime.type.connectortype.PubSubConnectorType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;
import mimosae.modelatruntime.type.contracttype.FullyQualifiedClass;
import mimosae.modelatruntime.type.contracttype.OperationDeclaration;
import mimosae.modelatruntime.type.contracttype.OperationSignature;
import mimosae.modelatruntime.type.contracttype.OperationType;
import mimosae.modelatruntime.type.contracttype.PrimitiveType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;
import mimosae.modelatruntime.type.contracttype.TopicBasedConsumerContractType;
import mimosae.modelatruntime.type.contracttype.TopicBasedProducerContractType;

public class TestMicroservice {
	private String identifier1;
	private String identifier2;
	private String identifier3;
	private Semver version;
	private OperationDeclaration declaration1;
	private OperationDeclaration declaration2;
	private OperationDeclaration declaration3;
	private ClientContractType clientContractType1;
	private ClientContractType clientContractType2;
	private ClientContractType clientContractType3;
	private ServerContractType serverContractType;
	private DatabaseContractType databaseContractType;
	private ClientServerConnectorType clientServerConnectorType;
	private TopicBasedAdvertisementFilter advFilter1;
	private TopicBasedAdvertisementFilter advFilter2;
	private TopicBasedAdvertisementFilter advFilter3;
	private TopicBasedAdvertisementFilter advFilter4;
	private TopicBasedSubscriptionFilter subFilter1;
	private TopicBasedSubscriptionFilter subFilter2;
	private EventType eventType1;
	private EventType eventType2;
	private EventType eventType3;
	private EventType eventType4;
	private TopicBasedProducerContractType producerContractType1;
	private TopicBasedProducerContractType producerContractType2;
	private TopicBasedProducerContractType producerContractType3;
	private TopicBasedProducerContractType producerContractType4;
	private TopicBasedConsumerContractType consumerContractType1;
	private TopicBasedConsumerContractType consumerContractType2;
	private PubSubConnectorType pubSubConnectorType1;
	private PubSubConnectorType pubSubConnectorType2;
	private DatabaseSystemType databaseSystemType;
	private DatabaseConnectorType databaseConnectorType;
	private MicroserviceType microserviceType1;
	private MicroserviceType microserviceType2;
	private MicroserviceType microserviceType3;
	private ConfigurationType configurationType1;

	@Before
	public void setUp() throws Exception {
		identifier1 = "ms1";
		identifier2 = "ms2";
		version = new Semver("0.0.1");
		identifier3 = "ms3";
		OperationSignature signature1 = new OperationSignature("op1", new FullyQualifiedClass("java.lang.Object"),
				PrimitiveType.BOOLEAN);
		OperationType returnType1 = PrimitiveType.INT;
		declaration1 = new OperationDeclaration(signature1, returnType1);
		OperationSignature signature2 = new OperationSignature("op2", PrimitiveType.INT);
		OperationType returnType2 = null;
		declaration2 = new OperationDeclaration(signature2, returnType2);
		OperationSignature signature3 = new OperationSignature("op3", PrimitiveType.DOUBLE);
		OperationType returnType3 = PrimitiveType.BOOLEAN;
		declaration3 = new OperationDeclaration(signature3, returnType3);
		clientContractType1 = new ClientContractType("clientContractType1", version, declaration1);
		clientContractType2 = new ClientContractType("clientContractType2", version, declaration2);
		clientContractType3 = new ClientContractType("clientContractType3", version, declaration3);
		Assert.assertTrue(clientContractType1.isInstantiable());
		Assert.assertTrue(clientContractType2.isInstantiable());
		serverContractType = new ServerContractType("serverContract", version, declaration1, declaration2);
		advFilter1 = new MQTTTopicBasedAdvertisementFilter("af1", version, "a/b/c");
		advFilter2 = new AMQPTopicBasedAdvertisementFilter("af2", version, "a.b.c");
		advFilter3 = new AMQPTopicBasedAdvertisementFilter("af3", version, "x.y.z");
		advFilter4 = new MQTTTopicBasedAdvertisementFilter("af1", version, "x/y/z");
		subFilter1 = new MQTTTopicBasedSubscriptionFilter("sf1", version, "a/b/#");
		subFilter2 = new AMQPTopicBasedSubscriptionFilter("sf2", version, "a.b.#");
		eventType1 = new EventType("et1", version);
		eventType2 = new EventType("et2", version);
		eventType3 = new EventType("et3", version);
		eventType4 = new EventType("et4", version);
		producerContractType1 = new TopicBasedProducerContractType("producerContractType1", version, advFilter1,
				eventType1, eventType2);
		producerContractType2 = new TopicBasedProducerContractType("producerContractType2", version, advFilter2,
				eventType1, eventType2);
		producerContractType3 = new TopicBasedProducerContractType("producerContractType3", version, advFilter3,
				eventType3);
		producerContractType4 = new TopicBasedProducerContractType("producerContractType4", version, advFilter4,
				eventType4);
		Assert.assertTrue(producerContractType1.isInstantiable());
		Assert.assertTrue(producerContractType2.isInstantiable());
		Assert.assertTrue(producerContractType3.isInstantiable());
		Assert.assertTrue(producerContractType4.isInstantiable());
		producerContractType1.setMustHaveConsumers(true);
		producerContractType2.setMustHaveConsumers(true);
		producerContractType4.setMustHaveConsumers(true);
		Assert.assertFalse(producerContractType1.isInstantiable());
		Assert.assertFalse(producerContractType2.isInstantiable());
		Assert.assertFalse(producerContractType4.isInstantiable());
		consumerContractType1 = new TopicBasedConsumerContractType("consumerContractType1", version, subFilter1,
				eventType1, eventType2);
		consumerContractType2 = new TopicBasedConsumerContractType("consumerContractType2", version, subFilter2,
				eventType1, eventType2);
		databaseContractType = new DatabaseContractType("databaseContractType", version);
		microserviceType1 = new MicroserviceType(identifier1, version);
		microserviceType2 = new MicroserviceType(identifier2, version);
		microserviceType3 = new MicroserviceType(identifier3, version);
		configurationType1 = new ConfigurationType(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		configurationType1.addClientContractType(clientContractType1);
		configurationType1.addClientContractType(clientContractType2);
		configurationType1.addClientContractType(clientContractType3);
		configurationType1.addServerContractType(serverContractType);
		configurationType1.addDatabaseContractType(databaseContractType);
		clientServerConnectorType = new ClientServerConnectorType(clientContractType1, serverContractType);
		Assert.assertEquals(clientContractType1, clientServerConnectorType.getClientContractType());
		Assert.assertEquals(serverContractType, clientServerConnectorType.getServerContractType());
		Assert.assertNotNull(clientServerConnectorType.getClientContractType().getClientServerConnectorType());
		Assert.assertEquals(clientServerConnectorType,
				clientServerConnectorType.getClientContractType().getClientServerConnectorType());
		Assert.assertTrue(clientServerConnectorType.getClientContractType().isInstantiable());
		configurationType1.addClientServerConnectorType(clientServerConnectorType);
		pubSubConnectorType1 = new PubSubConnectorType("pubSubConnectorType1", version);
		pubSubConnectorType1.addProducerContractType(producerContractType1);
		pubSubConnectorType1.addConsumerContractType(consumerContractType1);
		pubSubConnectorType2 = new PubSubConnectorType("pubSubConnectorType2", version);
		pubSubConnectorType2.addProducerContractType(producerContractType2);
		pubSubConnectorType2.addConsumerContractType(consumerContractType2);
		Assert.assertEquals("pubSubConnectorType1", pubSubConnectorType1.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version.toString(), pubSubConnectorType1.getTypeIdentifier().getVersion().toString());
		Assert.assertEquals(producerContractType1, pubSubConnectorType1
				.getProducerContractType(TypeIdentifier.typeIdentifier("producerContractType1", version)));
		Assert.assertEquals(consumerContractType1, pubSubConnectorType1
				.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType1", version)));
		Assert.assertEquals(1,
				pubSubConnectorType1
						.getProducerContractType(TypeIdentifier.typeIdentifier("producerContractType1", version))
						.getPubSubConnectorTypes().size());
		Assert.assertEquals(pubSubConnectorType1,
				pubSubConnectorType1
						.getProducerContractType(TypeIdentifier.typeIdentifier("producerContractType1", version))
						.getPubSubConnectorTypes().get(0));
		Assert.assertTrue(pubSubConnectorType1
				.getProducerContractType(TypeIdentifier.typeIdentifier("producerContractType1", version))
				.isInstantiable());
		Assert.assertEquals("pubSubConnectorType2", pubSubConnectorType2.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version.toString(), pubSubConnectorType2.getTypeIdentifier().getVersion().toString());
		Assert.assertEquals(producerContractType2, pubSubConnectorType2
				.getProducerContractType(TypeIdentifier.typeIdentifier("producerContractType2", version)));
		Assert.assertEquals(consumerContractType2, pubSubConnectorType2
				.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType2", version)));
		Assert.assertEquals(1,
				pubSubConnectorType2
						.getProducerContractType(TypeIdentifier.typeIdentifier("producerContractType2", version))
						.getPubSubConnectorTypes().size());
		Assert.assertEquals(pubSubConnectorType2,
				pubSubConnectorType2
						.getProducerContractType(TypeIdentifier.typeIdentifier("producerContractType2", version))
						.getPubSubConnectorTypes().get(0));
		Assert.assertTrue(pubSubConnectorType2
				.getProducerContractType(TypeIdentifier.typeIdentifier("producerContractType2", version))
				.isInstantiable());
		databaseSystemType = new DatabaseSystemType("databaseSystemType", version);
		databaseConnectorType = new DatabaseConnectorType(databaseContractType, databaseSystemType);
		configurationType1.addProducerContractType(producerContractType1);
		configurationType1.addProducerContractType(producerContractType2);
		configurationType1.addProducerContractType(producerContractType3);
		configurationType1.addProducerContractType(producerContractType4);
		configurationType1.addConsumerContractType(consumerContractType1);
		configurationType1.addConsumerContractType(consumerContractType2);
		configurationType1.addPubSubConnectorType(pubSubConnectorType1);
		configurationType1.addPubSubConnectorType(pubSubConnectorType2);
		configurationType1.addDatabaseSystemType(databaseSystemType);
		configurationType1.addDatabaseConnectorType(databaseConnectorType);
		configurationType1.addMicroserviceType(microserviceType1);
		configurationType1.addMicroserviceType(microserviceType2);
		configurationType1.addMicroserviceType(microserviceType3);
	}

	@After
	public void teardown() {
		identifier1 = null;
		identifier2 = null;
		identifier3 = null;
		version = null;
		declaration1 = null;
		declaration2 = null;
		declaration3 = null;
		clientContractType1 = null;
		clientContractType2 = null;
		clientContractType3 = null;
		serverContractType = null;
		clientServerConnectorType = null;
		advFilter1 = null;
		advFilter2 = null;
		advFilter3 = null;
		advFilter4 = null;
		subFilter1 = null;
		subFilter2 = null;
		eventType1 = null;
		eventType2 = null;
		eventType3 = null;
		eventType4 = null;
		producerContractType1 = null;
		producerContractType2 = null;
		producerContractType3 = null;
		producerContractType4 = null;
		consumerContractType1 = null;
		consumerContractType2 = null;
		pubSubConnectorType1 = null;
		pubSubConnectorType2 = null;
		databaseContractType = null;
		databaseSystemType = null;
		databaseConnectorType = null;
		microserviceType1 = null;
		microserviceType2 = null;
		microserviceType3 = null;
		configurationType1 = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new Microservice(null, microserviceType1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new Microservice("", microserviceType1);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new Microservice("m1", null);
	}

	@Test
	public void constructs3_1() throws Exception {
		microserviceType1.addClientContracType(clientContractType1, clientContractType2);
		microserviceType2.addServerContracType(serverContractType);
		Assert.assertEquals(microserviceType2.getServerContractTypes().get(0), microserviceType1
				.getClientContractTypes().get(0).getClientServerConnectorType().getServerContractType());

		microserviceType1.addProducerContracType(producerContractType1, producerContractType2, producerContractType3);
		microserviceType3.addConsumerContracType(consumerContractType1, consumerContractType2);
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(0),
				microserviceType1.getProducerContractTypes().get(0).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType1", version)));
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(1),
				microserviceType1.getProducerContractTypes().get(1).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType2", version)));
		microserviceType1.addDatabaseContracType(databaseContractType);

		Assert.assertTrue(microserviceType1.isInstantiable());
		Assert.assertTrue(microserviceType2.isInstantiable());
		Assert.assertTrue(microserviceType3.isInstantiable());

		Microservice microservice1 = new Microservice("m1", microserviceType1);
		Assert.assertEquals(new InstanceIdentifier("m1", microservice1.getMicroserviceType().getTypeIdentifier()),
				microservice1.getIdentifier());
		Assert.assertEquals(microserviceType1, microservice1.getMicroserviceType());
		Assert.assertNotNull(
				microservice1.getClientContract(ClientContract.computeIdentifier(microservice1, clientContractType1)));
		Assert.assertNotNull(
				microservice1.getClientContract(ClientContract.computeIdentifier(microservice1, clientContractType2)));
		Assert.assertNotNull(microservice1
				.getProducerContract(ProducerContract.computeIdentifier(microservice1, producerContractType1)));
		Assert.assertNotNull(microservice1
				.getProducerContract(ProducerContract.computeIdentifier(microservice1, producerContractType2)));
		Assert.assertNotNull(microservice1
				.getProducerContract(ProducerContract.computeIdentifier(microservice1, producerContractType3)));
		Assert.assertNotNull(microservice1
				.getDatabaseContract(DatabaseContract.computeIdentifier(microservice1, databaseContractType)));

		Microservice microservice2 = new Microservice("m2", microserviceType2);
		Assert.assertEquals(new InstanceIdentifier("m2", microservice2.getMicroserviceType().getTypeIdentifier()),
				microservice2.getIdentifier());
		Assert.assertEquals(microserviceType2, microservice2.getMicroserviceType());
		Assert.assertNotNull(
				microservice2.getServerContract(ServerContract.computeIdentifier(microservice2, serverContractType)));

		Microservice microservice3 = new Microservice("m3", microserviceType3);
		Assert.assertEquals(new InstanceIdentifier("m3", microservice3.getMicroserviceType().getTypeIdentifier()),
				microservice3.getIdentifier());
		Assert.assertEquals(microserviceType3, microservice3.getMicroserviceType());
		Assert.assertNotNull(microservice3
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3, consumerContractType1)));
		Assert.assertNotNull(microservice3
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3, consumerContractType2)));
	}

	@Test
	public void constructs3_2() throws Exception {
		microserviceType1.addClientContracType(clientContractType1, clientContractType2);
		microserviceType2.addServerContracType(serverContractType);
		Assert.assertEquals(microserviceType2.getServerContractTypes().get(0), microserviceType1
				.getClientContractTypes().get(0).getClientServerConnectorType().getServerContractType());

		microserviceType1.addProducerContracType(producerContractType1, producerContractType2, producerContractType3);
		microserviceType3.addConsumerContracType(consumerContractType1, consumerContractType2);
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(0),
				microserviceType1.getProducerContractTypes().get(0).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType1", version)));
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(1),
				microserviceType1.getProducerContractTypes().get(1).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType2", version)));
		microserviceType1.addDatabaseContracType(databaseContractType);

		Assert.assertTrue(microserviceType1.isInstantiable());
		Assert.assertTrue(microserviceType2.isInstantiable());
		Assert.assertTrue(microserviceType3.isInstantiable());

		Microservice microservice1 = new Microservice("m1", microserviceType1);
		Assert.assertEquals(new InstanceIdentifier("m1", microservice1.getMicroserviceType().getTypeIdentifier()),
				microservice1.getIdentifier());
		Assert.assertEquals(microserviceType1, microservice1.getMicroserviceType());
		Assert.assertNotNull(
				microservice1.getClientContract(ClientContract.computeIdentifier(microservice1, clientContractType1)));
		Assert.assertNotNull(
				microservice1.getClientContract(ClientContract.computeIdentifier(microservice1, clientContractType2)));
		Assert.assertNotNull(microservice1
				.getProducerContract(ProducerContract.computeIdentifier(microservice1, producerContractType1)));
		Assert.assertNotNull(microservice1
				.getProducerContract(ProducerContract.computeIdentifier(microservice1, producerContractType2)));
		Assert.assertNotNull(microservice1
				.getProducerContract(ProducerContract.computeIdentifier(microservice1, producerContractType3)));
		Assert.assertNotNull(microservice1
				.getDatabaseContract(DatabaseContract.computeIdentifier(microservice1, databaseContractType)));

		Microservice microservice2 = new Microservice("m2", microserviceType2);
		Assert.assertEquals(new InstanceIdentifier("m2", microservice2.getMicroserviceType().getTypeIdentifier()),
				microservice2.getIdentifier());
		Assert.assertEquals(microserviceType2, microservice2.getMicroserviceType());
		Assert.assertNotNull(
				microservice2.getServerContract(ServerContract.computeIdentifier(microservice2, serverContractType)));

		Microservice microservice3 = new Microservice("m3", microserviceType3);
		Assert.assertEquals(new InstanceIdentifier("m3", microservice3.getMicroserviceType().getTypeIdentifier()),
				microservice3.getIdentifier());
		Assert.assertEquals(microserviceType3, microservice3.getMicroserviceType());
		Assert.assertNotNull(microservice3
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3, consumerContractType1)));
		Assert.assertNotNull(microservice3
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3, consumerContractType2)));
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloning1() {
		new Microservice(null);
	}

	@Test
	public void constructsByCloning2_1() throws Exception {
		microserviceType1.addClientContracType(clientContractType1, clientContractType2);
		microserviceType2.addServerContracType(serverContractType);
		Assert.assertEquals(microserviceType2.getServerContractTypes().get(0), microserviceType1
				.getClientContractTypes().get(0).getClientServerConnectorType().getServerContractType());

		microserviceType1.addProducerContracType(producerContractType1, producerContractType2, producerContractType3);
		microserviceType3.addConsumerContracType(consumerContractType1, consumerContractType2);
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(0),
				microserviceType1.getProducerContractTypes().get(0).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType1", version)));
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(1),
				microserviceType1.getProducerContractTypes().get(1).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType2", version)));
		microserviceType1.addDatabaseContracType(databaseContractType);

		Assert.assertTrue(microserviceType1.isInstantiable());
		Assert.assertTrue(microserviceType2.isInstantiable());
		Assert.assertTrue(microserviceType3.isInstantiable());

		Microservice microservice1 = new Microservice("m1", microserviceType1);
		Assert.assertEquals(new InstanceIdentifier("m1", microservice1.getMicroserviceType().getTypeIdentifier()),
				microservice1.getIdentifier());
		Microservice microservice1Cloned = new Microservice(microservice1);
		Assert.assertEquals(
				new InstanceIdentifier("m1", microservice1Cloned.getMicroserviceType().getTypeIdentifier()),
				microservice1Cloned.getIdentifier());
		Assert.assertEquals(microserviceType1, microservice1Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice1Cloned
				.getClientContract(ClientContract.computeIdentifier(microservice1Cloned, clientContractType1)));
		Assert.assertNotNull(microservice1Cloned
				.getClientContract(ClientContract.computeIdentifier(microservice1Cloned, clientContractType2)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType1)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType2)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType3)));
		Assert.assertNotNull(microservice1Cloned
				.getDatabaseContract(DatabaseContract.computeIdentifier(microservice1Cloned, databaseContractType)));

		Microservice microservice2 = new Microservice("m2", microserviceType2);
		Microservice microservice2Cloned = new Microservice(microservice2);
		Assert.assertEquals(new InstanceIdentifier("m2", microservice2.getMicroserviceType().getTypeIdentifier()),
				microservice2Cloned.getIdentifier());
		Assert.assertEquals(microserviceType2, microservice2Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice2Cloned
				.getServerContract(ServerContract.computeIdentifier(microservice2Cloned, serverContractType)));

		Microservice microservice3 = new Microservice("m3", microserviceType3);
		Microservice microservice3Cloned = new Microservice(microservice3);
		Assert.assertEquals(new InstanceIdentifier("m3", microservice3.getMicroserviceType().getTypeIdentifier()),
				microservice3Cloned.getIdentifier());
		Assert.assertEquals(microserviceType3, microservice3Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice3Cloned
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3Cloned, consumerContractType1)));
		Assert.assertNotNull(microservice3Cloned
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3Cloned, consumerContractType2)));
	}

	@Test
	public void constructsByCloning2_2() throws Exception {
		microserviceType1.addClientContracType(clientContractType1, clientContractType2);
		microserviceType2.addServerContracType(serverContractType);
		Assert.assertEquals(microserviceType2.getServerContractTypes().get(0), microserviceType1
				.getClientContractTypes().get(0).getClientServerConnectorType().getServerContractType());

		microserviceType1.addProducerContracType(producerContractType1, producerContractType2, producerContractType3);
		microserviceType3.addConsumerContracType(consumerContractType1, consumerContractType2);
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(0),
				microserviceType1.getProducerContractTypes().get(0).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType1", version)));
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(1),
				microserviceType1.getProducerContractTypes().get(1).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType2", version)));
		microserviceType1.addDatabaseContracType(databaseContractType);

		Assert.assertTrue(microserviceType1.isInstantiable());
		Assert.assertTrue(microserviceType2.isInstantiable());
		Assert.assertTrue(microserviceType3.isInstantiable());

		Microservice microservice1 = new Microservice("m1", microserviceType1);
		Microservice microservice1Cloned = new Microservice(microservice1);
		Assert.assertEquals(new InstanceIdentifier("m1", microservice1.getMicroserviceType().getTypeIdentifier()),
				microservice1Cloned.getIdentifier());
		Assert.assertEquals(microserviceType1, microservice1Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice1Cloned
				.getClientContract(ClientContract.computeIdentifier(microservice1Cloned, clientContractType1)));
		Assert.assertNotNull(microservice1Cloned
				.getClientContract(ClientContract.computeIdentifier(microservice1Cloned, clientContractType2)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType1)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType2)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType3)));
		Assert.assertNotNull(microservice1Cloned
				.getDatabaseContract(DatabaseContract.computeIdentifier(microservice1Cloned, databaseContractType)));

		Microservice microservice2 = new Microservice("m2", microserviceType2);
		Microservice microservice2Cloned = new Microservice(microservice2);
		Assert.assertEquals(new InstanceIdentifier("m2", microservice2.getMicroserviceType().getTypeIdentifier()),
				microservice2Cloned.getIdentifier());
		Assert.assertEquals(microserviceType2, microservice2Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice2Cloned
				.getServerContract(ServerContract.computeIdentifier(microservice2Cloned, serverContractType)));

		Microservice microservice3 = new Microservice("m3", microserviceType3);
		Microservice microservice3Cloned = new Microservice(microservice3);
		Assert.assertEquals(new InstanceIdentifier("m3", microservice3.getMicroserviceType().getTypeIdentifier()),
				microservice3Cloned.getIdentifier());
		Assert.assertEquals(microserviceType3, microservice3Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice3Cloned
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3Cloned, consumerContractType1)));
		Assert.assertNotNull(microservice3Cloned
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3Cloned, consumerContractType2)));
	}

	@Test(expected = NullPointerException.class)
	public void constructsByCloningWithNewConfigurationType1() throws Exception {
		new Microservice(null, configurationType1);
	}

	@Test
	public void constructsByCloningWithNewConfigurationType2_1() throws Exception {
		microserviceType1.addClientContracType(clientContractType1, clientContractType2);
		microserviceType2.addServerContracType(serverContractType);
		Assert.assertEquals(microserviceType2.getServerContractTypes().get(0), microserviceType1
				.getClientContractTypes().get(0).getClientServerConnectorType().getServerContractType());

		microserviceType1.addProducerContracType(producerContractType1, producerContractType2, producerContractType3);
		microserviceType3.addConsumerContracType(consumerContractType1, consumerContractType2);
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(0),
				microserviceType1.getProducerContractTypes().get(0).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType1", version)));
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(1),
				microserviceType1.getProducerContractTypes().get(1).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType2", version)));
		microserviceType1.addDatabaseContracType(databaseContractType);

		Assert.assertTrue(microserviceType1.isInstantiable());
		Assert.assertTrue(microserviceType2.isInstantiable());
		Assert.assertTrue(microserviceType3.isInstantiable());
		configurationType1.commit();
		var configurationType2 = new ConfigurationType(configurationType1);

		Microservice microservice1 = new Microservice("m1", microserviceType1);
		Assert.assertEquals(new InstanceIdentifier("m1", microservice1.getMicroserviceType().getTypeIdentifier()),
				microservice1.getIdentifier());
		Microservice microservice1Cloned = new Microservice(microservice1, configurationType2);
		Assert.assertEquals(
				new InstanceIdentifier("m1", microservice1Cloned.getMicroserviceType().getTypeIdentifier()),
				microservice1Cloned.getIdentifier());
		Assert.assertEquals(microserviceType1, microservice1Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice1Cloned
				.getClientContract(ClientContract.computeIdentifier(microservice1Cloned, clientContractType1)));
		Assert.assertNotNull(microservice1Cloned
				.getClientContract(ClientContract.computeIdentifier(microservice1Cloned, clientContractType2)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType1)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType2)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType3)));
		Assert.assertNotNull(microservice1Cloned
				.getDatabaseContract(DatabaseContract.computeIdentifier(microservice1Cloned, databaseContractType)));

		Microservice microservice2 = new Microservice("m2", microserviceType2);
		Microservice microservice2Cloned = new Microservice(microservice2, configurationType2);
		Assert.assertEquals(new InstanceIdentifier("m2", microservice2.getMicroserviceType().getTypeIdentifier()),
				microservice2Cloned.getIdentifier());
		Assert.assertEquals(microserviceType2, microservice2Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice2Cloned
				.getServerContract(ServerContract.computeIdentifier(microservice2Cloned, serverContractType)));

		Microservice microservice3 = new Microservice("m3", microserviceType3);
		Microservice microservice3Cloned = new Microservice(microservice3, configurationType2);
		Assert.assertEquals(new InstanceIdentifier("m3", microservice3.getMicroserviceType().getTypeIdentifier()),
				microservice3Cloned.getIdentifier());
		Assert.assertEquals(microserviceType3, microservice3Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice3Cloned
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3Cloned, consumerContractType1)));
		Assert.assertNotNull(microservice3Cloned
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3Cloned, consumerContractType2)));
	}

	@Test
	public void constructsByCloningWithNewConfigurationType2_2() throws Exception {
		microserviceType1.addClientContracType(clientContractType1, clientContractType2);
		microserviceType2.addServerContracType(serverContractType);
		Assert.assertEquals(microserviceType2.getServerContractTypes().get(0), microserviceType1
				.getClientContractTypes().get(0).getClientServerConnectorType().getServerContractType());

		microserviceType1.addProducerContracType(producerContractType1, producerContractType2, producerContractType3);
		microserviceType3.addConsumerContracType(consumerContractType1, consumerContractType2);
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(0),
				microserviceType1.getProducerContractTypes().get(0).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType1", version)));
		Assert.assertEquals(microserviceType3.getConsumerContractTypes().get(1),
				microserviceType1.getProducerContractTypes().get(1).getPubSubConnectorTypes().get(0)
						.getConsumerContractType(TypeIdentifier.typeIdentifier("consumerContractType2", version)));
		microserviceType1.addDatabaseContracType(databaseContractType);

		Assert.assertTrue(microserviceType1.isInstantiable());
		Assert.assertTrue(microserviceType2.isInstantiable());
		Assert.assertTrue(microserviceType3.isInstantiable());
		configurationType1.commit();
		var configurationType2 = new ConfigurationType(configurationType1);

		Microservice microservice1 = new Microservice("m1", microserviceType1);
		Microservice microservice1Cloned = new Microservice(microservice1, configurationType2);
		Assert.assertEquals(
				new InstanceIdentifier("m1", microservice1Cloned.getMicroserviceType().getTypeIdentifier()),
				microservice1Cloned.getIdentifier());
		Assert.assertEquals(microserviceType1, microservice1Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice1Cloned
				.getClientContract(ClientContract.computeIdentifier(microservice1Cloned, clientContractType1)));
		Assert.assertNotNull(microservice1Cloned
				.getClientContract(ClientContract.computeIdentifier(microservice1Cloned, clientContractType2)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType1)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType2)));
		Assert.assertNotNull(microservice1Cloned
				.getProducerContract(ProducerContract.computeIdentifier(microservice1Cloned, producerContractType3)));
		Assert.assertNotNull(microservice1Cloned
				.getDatabaseContract(DatabaseContract.computeIdentifier(microservice1Cloned, databaseContractType)));

		Microservice microservice2 = new Microservice("m2", microserviceType2);
		Microservice microservice2Cloned = new Microservice(microservice2, configurationType2);
		Assert.assertEquals(
				new InstanceIdentifier("m2", microservice2Cloned.getMicroserviceType().getTypeIdentifier()),
				microservice2Cloned.getIdentifier());
		Assert.assertEquals(microserviceType2, microservice2Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice2Cloned
				.getServerContract(ServerContract.computeIdentifier(microservice2Cloned, serverContractType)));

		Microservice microservice3 = new Microservice("m3", microserviceType3);
		Microservice microservice3Cloned = new Microservice(microservice3, configurationType2);
		Assert.assertEquals(new InstanceIdentifier("m3", microservice3.getMicroserviceType().getTypeIdentifier()),
				microservice3Cloned.getIdentifier());
		Assert.assertEquals(microserviceType3, microservice3Cloned.getMicroserviceType());
		Assert.assertNotNull(microservice3Cloned
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3Cloned, consumerContractType1)));
		Assert.assertNotNull(microservice3Cloned
				.getConsumerContract(ConsumerContract.computeIdentifier(microservice3Cloned, consumerContractType2)));
	}
}
