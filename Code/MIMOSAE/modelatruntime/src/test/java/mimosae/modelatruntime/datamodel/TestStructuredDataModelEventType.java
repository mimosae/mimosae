// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.datamodel;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

public class TestStructuredDataModelEventType {
	private StructuredEventType eventType;
	private String identifier = "id";
	private String version = "0.0.1";

	@Before
	public void setUp() throws Exception {
		eventType = new StructuredEventType(identifier, new Semver(version));
	}

	@After
	public void teardown() {
		eventType = null;
		identifier = null;
		version = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new StructuredEventType(null, new Semver(version));
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new StructuredEventType("", new Semver(version));
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new StructuredEventType(identifier, null);
	}

	@Test
	public void constructs3() {
		Assert.assertEquals(identifier, eventType.getTypeIdentifier().getIdentifier());
		Assert.assertEquals(version, eventType.getTypeIdentifier().getVersion().toString());
	}
}
