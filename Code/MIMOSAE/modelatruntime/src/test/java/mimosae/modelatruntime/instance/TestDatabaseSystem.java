// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.vdurmont.semver4j.Semver;

import mimosae.common.Constants;
import mimosae.modelatruntime.type.DatabaseSystemType;

public class TestDatabaseSystem {
	private String identifier;
	private Semver version;
	private DatabaseSystemType databaseSystemType;

	@Before
	public void setUp() throws Exception {
		identifier = "db1";
		version = new Semver("0.0.1");
		databaseSystemType = new DatabaseSystemType("databaseSystemType1", version);
	}

	@After
	public void teardown() {
		identifier = null;
		version = null;
		databaseSystemType = null;
	}

	@Test(expected = NullPointerException.class)
	public void constructs1_1() throws Exception {
		new DatabaseSystem(null, databaseSystemType);
	}

	@Test(expected = IllegalArgumentException.class)
	public void constructs1_2() throws Exception {
		new DatabaseSystem("", databaseSystemType);
	}

	@Test(expected = NullPointerException.class)
	public void constructs2() throws Exception {
		new DatabaseSystem("m1", null);
	}

	@Test
	public void constructs3_1() throws Exception {
		DatabaseSystem databaseSystem = new DatabaseSystem(identifier, databaseSystemType);
		Assert.assertEquals(
				identifier + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS + databaseSystemType.getTypeIdentifier(),
				databaseSystem.getIdentifier().toString());
		Assert.assertEquals(databaseSystemType, databaseSystem.getDatabaseSystemType());
	}
}
