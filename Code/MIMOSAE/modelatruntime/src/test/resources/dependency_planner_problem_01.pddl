(define (problem microservice_dependency_problem) 
    (:domain microservice_dependency_domain)
(:objects 
    depender - microservicetype 
    dependee - microservicetype
    dependency - microservicetypedependency
)

(:init
    ; put the initial state's facts and numeric values here
    (microservicetype depender)
    (microservicetype dependee)
)

(:goal (and
    ; put the goal condition here
    (microservicetypedependency dependency)
    (microservicetype_satisfies_dependency_version_requirement dependee dependency)
    (microservicetype_is_in_dependency_as_dependee dependee dependency)
    (microservicetype_depends_on_microservicetype_in_dependency depender dependee dependency)
))

;un-comment the following line if metric is needed
;(:metric minimize (???))
)
