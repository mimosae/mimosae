/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package external.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This utility class generates SHA-1 digest for the snapshots of configuration
 * types and configurations.
 * 
 * @author Denis Conan
 */
public final class Digest {
	/**
	 * the algorithm for computing the digests, e.g. SHA-1.
	 */
	public static final String SHA1 = "SHA-1";

	/**
	 * is private to avoid instantiation.
	 */
	private Digest() {
	}

	/**
	 * computes the SHA-1 digest corresponding to the given string.
	 * 
	 * @param text the input to compute the SHA-1 digest.
	 * @return the SHA-1 digest.
	 */
	public static String computeSHA1(final String text) {
		try {
			var messageDigest = MessageDigest.getInstance(SHA1);
			messageDigest.update(text.getBytes(StandardCharsets.UTF_8), 0, text.length());
			var digest = messageDigest.digest();
			var builder = new StringBuilder();
			for (Byte b : digest) {
				builder.append(String.format("%02x", b));
			}
			return builder.toString();
		} catch (NoSuchAlgorithmException ex) {
			throw new IllegalStateException(ex.getLocalizedMessage());
		}
	}
}
