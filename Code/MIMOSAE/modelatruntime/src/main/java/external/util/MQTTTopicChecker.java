/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package external.util;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * This utility class contains methods to check whether strings are valid MQTT
 * topics and subscription keys. It includes code extracted from the Web:
 * references are inserted.
 * 
 * @author Denis Conan
 */
public final class MQTTTopicChecker {
	/**
	 * is private to avoid instantiation.
	 */
	private MQTTTopicChecker() {
	}

	/**
	 * Check that a topic used for publishing is valid. Search for '+' or '#' in a
	 * topic. Return false if found. Also return false if the topic string is too
	 * long. Return true if everything is fine.
	 * 
	 * See <a href=
	 * "https://github.com/eclipse/mosquitto/blob/master/lib/util_topic.c#L138">Eclipse
	 * mosquitto algorithm util_topic in C</a>.
	 * 
	 * @param str the string of the routing key, a.k.a. publish topic, to check.
	 * @return false when the routing key cannot be a MQTT publish key.
	 */
	public static boolean pubTopicCheck(final String str) {
		Objects.requireNonNull(str, "no pub. topic");
		if (str.isEmpty()) {
			return false;
		}
		for (var i = 0; i < str.length(); i++) {
			var c = str.charAt(i);
			if (c == '+' || c == '#') {
				return false;
			}
		}
		final int maxLength = 65535;
		return str.length() <= maxLength;
	}

	/**
	 * Check that a topic used for subscriptions is valid. Search for '+' or '#' in
	 * a topic, check they aren't in invalid positions such as "foo/#/bar",
	 * "foo/+bar" or "foo/bar#". Return false if invalid position found. Also
	 * returns false if the topic string is too long. Return true if everything is
	 * fine.
	 * 
	 * See <a href=
	 * "https://github.com/eclipse/mosquitto/blob/master/lib/util_topic.c#L138">Eclipse
	 * mosquitto algorithm util_topic in C</a>.
	 * 
	 * @param subscription the string of the subscription to check.
	 * @return false when the subscription cannot be a MQTT subscription.
	 */
	public static boolean subcriptionCheck(final String subscription) {
		Objects.requireNonNull(subscription, "no subscription");
		if (subscription.isEmpty()) {
			return false;
		}
		for (var i = 1; i < subscription.length(); i++) {
			if (subscription.charAt(i) == '+') {
				if (subscription.charAt(i - 1) != '/'
						|| (i + 1 < subscription.length() && subscription.charAt(i + 1) != '/')) {
					return false;
				}
			} else if (subscription.charAt(i) == '#') {
				if (subscription.charAt(i - 1) != '/' || i + 1 < subscription.length()) {
					return false;
				}
			}
		}
		final int maxLength = 65535;
		return subscription.length() <= maxLength;
	}

	/**
	 * Does a check if topics match a certain topic filter.
	 * 
	 * See algorithm
	 * <a href="https://github.com/eclipse/mosquitto/issues/1317">Eclipse mosquitto
	 * issue 1317</a> in C<span>&#35;</span>.
	 * 
	 * @param subscription the subscription.
	 * @return true if the publication topic matches the subcription, false if not.
	 */
	public static Pattern toRegex(final String subscription) {
		String pattern = subscription.replace("/", "\\/").replace("+", "[a-zA-Z0-9 _.-]*").replace("#",
				"[a-zA-Z0-9 \\/_#+.-]*");
		return Pattern.compile(pattern);
	}
}
