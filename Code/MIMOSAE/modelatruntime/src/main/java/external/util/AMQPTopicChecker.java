/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package external.util;

import java.util.regex.Pattern;

/**
 * This utility class contains methods to check whether strings are valid AMQP
 * routing and binding keys. It includes code extracted from the Web: references
 * are inserted.
 * 
 * @author Denis Conan
 */
public final class AMQPTopicChecker {
	/**
	 * is private to avoid instantiation.
	 */
	private AMQPTopicChecker() {
	}

	/**
	 * checks whether the given string is a valid AMQP routing key. This method is
	 * to be implemented in future works.
	 * 
	 * @param str the string to check.
	 * @return false when the string cannot be an AMQP routing key.
	 */
	public static boolean checkRoutingKeyWhenPublishing(final String str) {
		throw new UnsupportedOperationException("to do");
	}

	/**
	 * checks whether the given string is a valid AMQP binding key.
	 * 
	 * @param subscription the subscription string to check.
	 * @return false when the subscription cannot be transformed into a regex
	 *         pattern using {@link #toRegex(String)}.
	 */
	public static boolean checkBindingKeyWhenSubscribing(final String subscription) {
		toRegex(subscription);
		return true;
	}

	/**
	 * parses the binding key to build the regex pattern that will match the routing
	 * keys.
	 * <p>
	 * See <a href=
	 * "https://stackoverflow.com/questions/50679145/how-to-match-the-routing-key-with-binding-pattern-for-rabbitmq-topic-exchange-us">stackoverflow
	 * 50679145</a>.
	 * 
	 * @param bindingKey the binding key to parse.
	 * @return the corresponding pattern.
	 */
	public static Pattern toRegex(final String bindingKey) {
		final var word = "[a-zA-Z_0-9]+";
		// replace duplicate # (this makes things simpler) and replace *
		String pattern = bindingKey.replaceAll("#(?:\\.#)+", "#").replaceAll("\\*", word);
		// lone #
		if ("#".equals(pattern)) {
			return Pattern.compile("(?:" + word + "(?:\\." + word + ")*)?");
		}
		pattern = pattern.replaceFirst("^#\\.", "(?:" + word + "\\.)*").replaceFirst("\\.#", "(?:\\." + word + ")*");
		// escape dots that aren't escapes already
		pattern = pattern.replaceAll("(?<!\\\\)\\.", "\\\\.");
		return Pattern.compile("^" + pattern + "$");
	}
}
