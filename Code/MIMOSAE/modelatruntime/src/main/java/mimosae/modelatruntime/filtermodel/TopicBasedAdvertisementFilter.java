/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.filtermodel;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

/**
 * This class defines the filter model of a topic-based filter for
 * advertisements, that is for publishing. The attribute "topic" is called
 * "topic" in the MQTT specification and "routing key" in the AMQP
 * specification. Strictly speaking, such a filter is not used for filtering but
 * for adding the topic to published messages.
 * <p>
 * NB: the name of the class does not contain "Type" because there is no need
 * for the corresponding class with "Instance": i.e. contrary to the concepts of
 * "configuration type", "microservice type", "database system type", etc. This
 * is so because instances of filters are not modeled: this class is used only
 * the "type" part of the modeling.
 * 
 * @author Denis Conan
 */
public abstract class TopicBasedAdvertisementFilter extends EventFilter {
	/**
	 * the topic used when publishing, i.e. added to messages when publishing.
	 */
	private final String topic;

	/**
	 * constructs a new topic-based advertisement filter.
	 * 
	 * @param identifier the identifier of the topic-based advertisement filter.
	 * @param version    the version of the topic-based advertisement filter.
	 * @param topic      the topic of the topic-based advertisement filter.
	 */
	protected TopicBasedAdvertisementFilter(final String identifier, final Semver version, final String topic) {
		super(identifier, version);
		Objects.requireNonNull(topic, "topic cannot be null");
		if (topic.isBlank()) {
			throw new IllegalArgumentException("topic cannot be empty");
		}
		this.topic = topic;
	}

	/**
	 * obtains the topic.
	 * 
	 * @return the topic.
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * states whether this filter is compatible with the given filter. Because this
	 * filter is an advertisement filter, the given filter must be a subscription
	 * filter. Then, this method checks that the topic of this filter matches the
	 * given subscription filter.
	 * 
	 * @param eventFilter the given subscription filter.
	 * @return true when the topic of this filter matches the given subscription
	 *         filter.
	 */
	public abstract boolean isCompatibleWith(EventFilter eventFilter);
}
