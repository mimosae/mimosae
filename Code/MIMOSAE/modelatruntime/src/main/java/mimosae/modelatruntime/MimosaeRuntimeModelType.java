/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.jgrapht.Graphs;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;

import com.vdurmont.semver4j.Semver;
import com.vdurmont.semver4j.Semver.VersionDiff;

import external.util.AMQPTopicChecker;
import external.util.MQTTTopicChecker;
import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.exception.AlreadyExistsModelElementException;
import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.exception.IsNotInstantiableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.MalformedOperationDeclarationException;
import mimosae.modelatruntime.exception.MalformedRoutingKeyException;
import mimosae.modelatruntime.exception.MalformedSubscriptionFilterException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.exception.WrongNewVersionException;
import mimosae.modelatruntime.exception.WrongNumberOfIdentifiersException;
import mimosae.modelatruntime.filtermodel.AMQPTopicBasedAdvertisementFilter;
import mimosae.modelatruntime.filtermodel.AMQPTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.filtermodel.ChannelBasedFilter;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedAdvertisementFilter;
import mimosae.modelatruntime.filtermodel.MQTTTopicBasedSubscriptionFilter;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.DatabaseSystemType;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.TypeEdge;
import mimosae.modelatruntime.type.TypeIdentifier;
import mimosae.modelatruntime.type.connectortype.ClientServerConnectorType;
import mimosae.modelatruntime.type.connectortype.DatabaseConnectorType;
import mimosae.modelatruntime.type.connectortype.PubSubConnectorType;
import mimosae.modelatruntime.type.contracttype.ChannelBasedConsumerContractType;
import mimosae.modelatruntime.type.contracttype.ChannelBasedProducerContractType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;
import mimosae.modelatruntime.type.contracttype.OperationDeclaration;
import mimosae.modelatruntime.type.contracttype.ServerContractType;
import mimosae.modelatruntime.type.contracttype.TopicBasedConsumerContractType;
import mimosae.modelatruntime.type.contracttype.TopicBasedProducerContractType;

/**
 * This class is the Façade of the type model at runtime. It implements the
 * Singleton design pattern.
 * 
 * @author Denis Conan
 */
public final class MimosaeRuntimeModelType {
	/**
	 * the singleton instance of this façade.
	 */
	private static MimosaeRuntimeModelType instance;
	/**
	 * the graph of configuration types.
	 */
	private final DirectedAcyclicGraph<ConfigurationType, TypeEdge> configurationTypes;
	/**
	 * the last committed configuration type in the graph of configuration types.
	 * This is the head of the current branch of the graph.
	 */
	private ConfigurationType lastCommittedConfigurationType;
	/**
	 * the next to commit configuration type of the graph of configuration types.
	 */
	private ConfigurationType currentConfigurationType;
	/**
	 * the log of actions that are performed on the current configuration type. This
	 * is for debugging purposes.
	 */
	private List<ActionLog> currentActionLog;

	/**
	 * constructs the façade. The last committed configuration type is the empty
	 * "init" configuration type. The graph of configuration types contains only the
	 * last committed configuration type. The current configuration type is created
	 * by cloning the last committed configuration type. The action log of the
	 * current configuration type is empty.
	 */
	private MimosaeRuntimeModelType() {
		configurationTypes = new DirectedAcyclicGraph<>(TypeEdge.class);
		configurationTypes.setEdgeSupplier(TypeEdge::new);
		lastCommittedConfigurationType = EmptyInitConfigurationType.getEmptyInitConfigurationType();
		configurationTypes.addVertex(lastCommittedConfigurationType);
		currentConfigurationType = new ConfigurationType(lastCommittedConfigurationType);
		currentActionLog = new ArrayList<>();
		assert invariant();
	}

	/**
	 * obtains the singleton object of the façade.
	 * 
	 * @return the façade.
	 */
	public static MimosaeRuntimeModelType getMimosaeRuntimeModelType() {
		if (instance == null) {
			instance = new MimosaeRuntimeModelType();
		}
		return instance;
	}

	/**
	 * computes the invariant of the façade singleton object.
	 * 
	 * @return true when the invariant is true.
	 */
	public boolean invariant() {
		return configurationTypes != null && lastCommittedConfigurationType != null
				&& configurationTypes.containsVertex(lastCommittedConfigurationType) // && currentActionLog != null
				&& (this.lastCommittedConfigurationType.getIdentifier()
						.equals(EmptyInitConfigurationType.IDENTIFIER_EMPTY_INIT_CONFIGURATION_TYPE)
						|| this.currentConfigurationType != null);
	}

	/**
	 * clears all the objects of the façade, i.e. all the configuration types of the
	 * graph, etc. This method is a priori used in tests only.
	 */
	public void clearAllConfigurationTypes() {
		configurationTypes.removeAllVertices(configurationTypes.vertexSet());
		currentConfigurationType = EmptyInitConfigurationType.getEmptyInitConfigurationType();
		var temp = new ConfigurationType(currentConfigurationType);
		configurationTypes.addVertex(currentConfigurationType);
		lastCommittedConfigurationType = currentConfigurationType;
		currentConfigurationType = temp;
		currentActionLog = new ArrayList<>();
		assert invariant();
	}

	/**
	 * resets the current configuration type: When the last committed configuration
	 * type is the empty "init" configuration type, the current configuration type
	 * is set to the empty "init" configuration type; Otherwise, the current
	 * configuration is created by cloning the last committed configuration type.
	 */
	public void resetCurrentConfigurationType() {
		currentConfigurationType = this.lastCommittedConfigurationType.getIdentifier()
				.equals(EmptyInitConfigurationType.IDENTIFIER_EMPTY_INIT_CONFIGURATION_TYPE)
						? EmptyInitConfigurationType.getEmptyInitConfigurationType()
						: new ConfigurationType(this.lastCommittedConfigurationType);
		currentActionLog = new ArrayList<>();
		assert invariant();
	}

	/**
	 * obtains the string format of the graph of configuration types in DOT format.
	 * <p>
	 * Here follows an example:
	 * 
	 * <pre>
	strict digraph configuration_types {
	1 [ SHA-1="empty_init_configuration_type" ];
	2 [ SHA-1="0ae5cae86de9b426a74a0e4aa26ff7351428e287" ];
	1 -&gt; 2 [ Type action log="[
		[name=createClientContractType, arguments=[clientContractType1, 0.0.1, m1 int boolean, m2 : boolean ]], 
		...
		[name=createChannelBasedProducerContractType, arguments=[channelProducerContractType1, 0.0.1, channel1, 0.0.1, eventtype1, 0.0.1]], 
		...
		[name=createAMQPTopicBasedProducerContractType, arguments=[amqpProducerContractType1, 0.0.1, amqp1, 0.0.1, a.b.c, eventtype1, 0.0.1]], 
		...
		[name=createMicroserviceType, arguments=[microserviceType2, 0.0.1, serverContractType2, 0.0.1, serverContractType3, 0.0.1]], 
		...
		[name=createClientServerConnectorType, arguments=[clientContractType1, 0.0.1, serverContractType2, 0.0.1]], 
		...
		[name=createPubSubConnectorType, arguments=[channelPubSubConnectorType1, 0.0.1, channelProducerContractType1, 0.0.1, channelConsumerContractType1, 0.0.1]], 
		...
		[name=setClientContractTypeMustBeConnected, arguments=[clientContractType1, 0.0.1, true]], 
		...
		[name=setProducerContractTypeMustHaveConsumers, arguments=[channelProducerContractType2, 0.0.1, true]], 
		...
		[name=createDatabaseSystemType, arguments=[databaseSystemType1, 0.0.1]],
		...
		[name=createDatabaseContractType, arguments=[databaseContractType1, 0.0.1]],
		...
		[name=createMicroserviceType, arguments=[microserviceType2, 0.0.1, serverContractType2, 0.0.1, serverContractType3, 0.0.1]],
		...
		[name=createClientServerConnectorType, arguments=[clientContractType1, 0.0.1, serverContractType2, 0.0.1]], 
		...
		[name=createPubSubConnectorType, arguments=[channelPubSubConnectorType1, 0.0.1]], 
		[name=addProducerContractTypeToPubSubConnectorType, arguments=[channelProducerContractType1, 0.0.1, channelPubSubConnectorType1, 0.0.1]], 
		[name=addConsumerContractTypeToPubSubConnectorType, arguments=[channelConsumerContractType1, 0.0.1, channelPubSubConnectorType1, 0.0.1]], 
		...
		[name=createDatabaseConnectorType, arguments=[databaseContractType1, 0.0.1, databaseSystemType1, 0.0.1]], 
		...
	}
	 * </pre>
	 * 
	 * @return the string of the graph of configuration types in DOT format.
	 */
	public String toStringGraphOfConfigurationTypeInDotFormat() {
		DOTExporter<ConfigurationType, TypeEdge> exporter = new DOTExporter<>();
		exporter.setGraphIdProvider(() -> "configuration_types");
		exporter.setVertexAttributeProvider(v -> {
			Map<String, Attribute> map = new LinkedHashMap<>();
			map.put("SHA-1", DefaultAttribute.createAttribute(v.getIdentifier()));
			return map;
		});
		exporter.setEdgeAttributeProvider(e -> {
			Map<String, Attribute> map = new LinkedHashMap<>();
			map.put("Type action log", DefaultAttribute.createAttribute(e.getLogs().toString()));
			return map;
		});
		Writer writer = new StringWriter();
		exporter.exportGraph(configurationTypes, writer);
		return writer.toString() + "The last-committed configuration type is \""
				+ this.lastCommittedConfigurationType.getIdentifier() + "\"";
	}

	/**
	 * obtains the identifier of the last committed configuration type.
	 * 
	 * @return the identifier as a SHA-1 digest.
	 */
	public String getIdentifierOfLastCommittedConfigurationType() {
		return lastCommittedConfigurationType.getIdentifier();
	}

	/**
	 * checks whether the configuration type with the given identifier is
	 * instantiable: all the microservice types of the configuration type are
	 * instantiable, i.e. (1) for all the client contract types, if the client
	 * contract type must have a client-server connector type attached then there is
	 * such one connector type that is attached, and (2) for all the producer
	 * contract types, for all the producer contract types, if the producer contract
	 * type must have consumers then there are some.
	 * 
	 * @param identifier the identifier of the configuration type to check.
	 * @return true if the configuration type with the given identifier is
	 *         instantiable.
	 * @throws UnknownTypeException the configuration type is unknown in the graph.
	 */
	public boolean checkConfigurationTypeIsInstantiable(final String identifier) throws UnknownTypeException {
		var found = configurationTypes.vertexSet().stream().filter(v -> v.getIdentifier().equals(identifier))
				.findFirst();
		if (found.isEmpty()) {
			throw new UnknownTypeException("configuration type " + identifier + " does not exist");
		}
		return found.get().isInstantiable();
	}

	/**
	 * checks whether the current configuration type is instantiable: all the
	 * microservice types of the configuration type are instantiable, i.e. (1) for
	 * all the client contract types, if the client contract type must have a
	 * client-server connector type attached then there is such one connector type
	 * that is attached, and (2) for all the producer contract types, for all the
	 * producer contract types, if the producer contract type must have consumers
	 * then there are some.
	 * 
	 * @return true if the current configuration type is instantiable.
	 */
	public boolean checkCurrentConfigurationTypeIsInstantiable() {
		return this.currentConfigurationType.isInstantiable();
	}

	/**
	 * obtains the string of the configuration type of the given identifier. It
	 * calls to the method {@code toString} on all the content of the configuration
	 * type, i.e. all the microservice types and all the connector types, etc.
	 * <p>
	 * Here follows an example:
	 * 
	 * <pre>
	ConfigurationType [identifier=null,
	microserviceTypes=
	[MicroservicesType
		 [identifier=microserviceType2v.0.0.1,
		 clientContractTypes=[],
		 serverContractTypes=[ServerContractType [getIdentifier()=serverContractType2v.0.0.1, getOperationDeclarations()=[OperationDeclaration ...]], ...],
		 producerContractTypes=[],
		 consumerContractTypes=[]],
	 ...
	],
	databaseSystemTypes=
	[DataBaseSystemType [identifier=databaseSystemType1v.0.0.1]
	 ...
	],
	clientContractTypes=
	[ClientContractType [getIdentifier()=clientContractType1v.0.0.1, getOperationDeclarations()=[OperationDeclaration [signature=OperationSignature [...], returnType=Optional.empty], ...]],
	 ...
	],
	serverContractTypes=
	[ServerContractType [getIdentifier()=serverContractType3v.0.0.1, getOperationDeclarations()=[OperationDeclaration [signature=OperationSignature [...], returnType=Optional[double]]]],
	 ...
	],
	producerContractTypes=
	[TopicBasedProducerContractType [getIdentifier()=..., eventTypes=[EventType [identifier=...]], eventFilter=MQTTTopicBasedAdvertisementFilter [..., getRoutingKey()=a/b/c]],
	 ...
	],
	consumerContractTypes=
	[TopicBasedConsumerContractType [getIdentifier()=..., eventTypes=[...], eventFilter=MQTTTopicBasedSubscriptionFilter [..., getSubscription()=a/b/#, pattern=a\/b\/[a-zA-Z0-9 \/_#+.-]*]],
	 ...
	],
	databaseContractTypes=
	[DatabaseSystemContractType [identifier=databaseContractType1v.0.0.1 connected to database system type databaseSystemTypev.0.0.1]
	 ...
	],
	client server connector types=
	[ClientServerConnectorType [clientContractType=clientContractType1v.0.0.1, serverContractType=serverContractType2v.0.0.1]
	],
	publish-subscibe connector types=
	[PubSubServerConnectorType [getIdentifier()=mqttPubSubConnectorType1v.0.0.1, producerContractTypes=[mqttProducerContractType1v.0.0.1], consumerContractType=[mqttConsumerContractType1v.0.0.1]]
	 ...
	]
	database connector types=
	[ClientServerConnectorType [databaseContractType=databaseContractType1v.0.0.1, databaseSystemType=databaseSystemType1v.0.0.1]
	 ...
	]
	]
	 * </pre>
	 * 
	 * @param identifier the identifier of the configuration type.
	 * @return the stringified configuration type.
	 * @throws UnknownTypeException the configuration type is unknown.
	 */
	public String toStringConfigurationType(final String identifier) throws UnknownTypeException {
		var found = configurationTypes.vertexSet().stream().filter(v -> v.getIdentifier().equals(identifier))
				.findFirst();
		if (found.isEmpty()) {
			throw new UnknownTypeException("configuration type " + identifier + " does not exist");
		}
		return found.get().toString();
	}

	/**
	 * obtains the string of the current configuration type. It calls to the method
	 * {@code toString} on all the content of the configuration type, i.e. all the
	 * microservice types and all the connector types, etc.
	 * <p>
	 * Cf. Javadoc of {@link #toStringConfigurationType(String)} for an example.
	 * 
	 * @return the stringified configuration type.
	 */
	public String toStringCurrentConfigurationType() {
		return this.currentConfigurationType.toString();
	}

	/**
	 * obtains a reference to a configuration type by providing its identifier.
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance} and must not be accessible by other: it
	 * returns a configuration type object that is internal to the type model of the
	 * model at runtime.
	 * 
	 * @param identifier the identifier of the configuration type to search for.
	 * @return the reference to the configuration type.
	 * @throws UnknownTypeException the configuration type is unknown.
	 */
	ConfigurationType getConfigurationType(final String identifier) throws UnknownTypeException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		Optional<ConfigurationType> found = configurationTypes.vertexSet().stream()
				.filter(v -> v.getIdentifier().equals(identifier)).findFirst();
		if (found.isEmpty()) {
			throw new UnknownTypeException("configuration " + identifier + " is an unknown configuration type");
		}
		return found.get();
	}

	/**
	 * creates a microservice type and add it the current configuration type. A
	 * microservice type is identified by an identifier and a version: e.g.
	 * "authentification_service" in version "1.0.0".
	 * 
	 * @param identifier                     the identifier of the microservice
	 *                                       type.
	 * @param version                        the version of the microservice type.
	 * @param contractIdentifiersAndVersions the set of contracts, i.e. pairs of
	 *                                       contract identifier and contract
	 *                                       version.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the set of contracts is
	 *                                                 malformed, i.e. not pairs of
	 *                                                 (identifier, version).
	 * @throws AlreadyExistsModelElementException      the microservice type already
	 *                                                 exists in current
	 *                                                 configuration type.
	 * @throws UnknownTypeException                    one of the types is unknown
	 *                                                 in current configuration
	 *                                                 type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createMicroserviceType(final String identifier, final String version,
			final String... contractIdentifiersAndVersions)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			UnknownTypeException, IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		if (currentConfigurationType.getMicroserviceType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"microservice type " + typeIdentifier.getIdentifier() + " already exists");
		}
		var microserviceType = new MicroserviceType(identifier, new Semver(version));
		currentConfigurationType.addMicroserviceType(microserviceType);
		if ((contractIdentifiersAndVersions.length % 2) != 0) {
			throw new WrongNumberOfIdentifiersException(
					"the number of string should be a multiple of 2 (wrong number = "
							+ contractIdentifiersAndVersions.length + ")");
		}
		for (var i = 0; i < contractIdentifiersAndVersions.length; i = i + 2) {
			var ti = TypeIdentifier.typeIdentifier(contractIdentifiersAndVersions[i],
					new Semver(contractIdentifiersAndVersions[i + 1]));
			if (currentConfigurationType.getClientContractType(ti) != null) {
				microserviceType.addClientContracType(currentConfigurationType.getClientContractType(ti));
			} else if (currentConfigurationType.getServerContractType(ti) != null) {
				microserviceType.addServerContracType(currentConfigurationType.getServerContractType(ti));
			} else if (currentConfigurationType.getProducerContractType(ti) != null) {
				microserviceType.addProducerContracType(currentConfigurationType.getProducerContractType(ti));
			} else if (currentConfigurationType.getConsumerContractType(ti) != null) {
				microserviceType.addConsumerContracType(currentConfigurationType.getConsumerContractType(ti));
			} else if (currentConfigurationType.getDatabaseContractType(ti) != null) {
				microserviceType.addDatabaseContracType(currentConfigurationType.getDatabaseContractType(ti));
			} else {
				throw new UnknownTypeException("contract type " + ti + " does not exist");
			}
		}
		currentActionLog.add(ActionLog.createActionToLog("createMicroserviceType", identifier, version,
				contractIdentifiersAndVersions));
		assert invariant();
	}

	/**
	 * obtains the string of a microservice type that belongs to the current
	 * configuration type. It calls to the method {@code toString} on all the
	 * content of the microservice type, i.e. all the contract types, etc. The
	 * microservice type is identified by its identifier and its version e.g.
	 * "authentification_service" in version "1.0.0".
	 * 
	 * @param identifier the identifier of the microservice type to search for.
	 * @param version    the version of the microservice type.
	 * @return the stringified microservice type.
	 * @throws UnknownTypeException         the microservice type is unknown in
	 *                                      current configuration type.
	 * @throws MalformedIdentifierException microservice type's identifier is
	 *                                      malformed.
	 */
	public String toStringMicroserviceType(final String identifier, final String version)
			throws UnknownTypeException, MalformedIdentifierException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		var microserviceType = currentConfigurationType.getMicroserviceType(typeIdentifier);
		if (microserviceType == null) {
			throw new UnknownTypeException("microservice type " + typeIdentifier.getIdentifier() + " does not exist");
		}
		return microserviceType.toString();
	}

	/**
	 * obtains the reference of a microservice type in current configuration type.
	 * The microservice is identified by its identifier and its version e.g.
	 * "authentification_service" in version "1.0.0".
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance} and must not be accessible by other: it
	 * returns a microservice type object that is internal to the type model of the
	 * model at runtime.
	 * 
	 * @param identifier the identifier of the microservice type to search for.
	 * @param version    the version of the microservice type.
	 * @return the reference to the microservice type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 * @throws UnknownTypeException         the microservice type is unknown in
	 *                                      current configuration type.
	 */
	MicroserviceType getMicroserviceType(final String identifier, final String version)
			throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = this.computeTypeIdentifier(identifier, version);
		var microserviceType = currentConfigurationType.getMicroserviceType(typeIdentifier);
		if (microserviceType == null) {
			throw new UnknownTypeException("microservice type " + typeIdentifier.getIdentifier()
					+ " does not exist in the current to commit configuration type");
		}
		return microserviceType;

	}

	/**
	 * obtains the reference of a microservice type in a given configuration type.
	 * The configuration type is identified by an identifier, which is a SHA-1
	 * digest. The microservice type is identified by its identifier and its version
	 * e.g. "authentification_service" in version "1.0.0".
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance} and must not be accessible by other: it
	 * returns a microservice type object that is internal to the type model of the
	 * model at runtime.
	 * 
	 * @param configurationTypeIdentifier the identifier of the configuration.
	 * @param identifier                  the identifier of the microservice type to
	 *                                    search for.
	 * @param version                     the version of the microservice type.
	 * @return the reference to the microservice type.
	 * @throws MalformedIdentifierException one of the identifier is malformed.
	 * @throws UnknownTypeException         either the configuration type or the
	 *                                      microservice type in the given
	 *                                      configuration type are unknown.
	 */
	MicroserviceType getMicroserviceTypeIntoConfigurationType(final String configurationTypeIdentifier,
			final String identifier, final String version) throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		var configurationType = this.getConfigurationType(configurationTypeIdentifier);
		if (configurationType == null) {
			throw new UnknownTypeException("configuration type " + configurationTypeIdentifier + " does not exist");
		}
		var microserviceType = configurationType.getMicroserviceType(typeIdentifier);
		if (microserviceType == null) {
			throw new UnknownTypeException("microservice type " + typeIdentifier
					+ " does not exist in configuration type " + configurationTypeIdentifier);
		}
		return microserviceType;

	}

	/**
	 * creates a microservice type as a patch version of an already existing
	 * microservice type and adds the microservice type in current configuration.
	 * The given / current microservice type is identified by its identifier and its
	 * version: e.g. "authentification_service" in version "1.0.0". The new version
	 * number is checked to be a patch.
	 * 
	 * @param currentIdentifier the identifier of the microservice type to patch.
	 * @param currentVersion    the version of the microservice type to patch.
	 * @param newVersion        the version of the new microservice type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws AlreadyExistsModelElementException      the patched microservice type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws UnknownTypeException                    the microservice type to
	 *                                                 patch is unknown in current
	 *                                                 configuration type.
	 * @throws WrongNewVersionException                the new version number is not
	 *                                                 a patch.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createSameMicroserviceTypeAsPatchVersion(final String currentIdentifier, final String currentVersion,
			final String newVersion) throws MalformedIdentifierException, AlreadyExistsModelElementException,
			UnknownTypeException, WrongNewVersionException, IsCommittedAndCannotBeModifiedException {
		var currentTypeIdentifier = computeTypeIdentifier(currentIdentifier, currentVersion);
		var newTypeIdentifier = computeTypeIdentifier(currentIdentifier, newVersion);
		if (!currentTypeIdentifier.getVersion().diff(newTypeIdentifier.getVersion()).equals(VersionDiff.PATCH)) {
			throw new WrongNewVersionException("new version of current microservice " + currentTypeIdentifier
					+ " is not a patch (" + currentTypeIdentifier + "->" + newTypeIdentifier + ")");
		}
		var currentMicroserviceType = currentConfigurationType.getMicroserviceType(currentTypeIdentifier);
		if (currentMicroserviceType == null) {
			throw new UnknownTypeException(
					"current microservice type " + currentTypeIdentifier.getIdentifier() + " does not exist");
		}
		if (currentConfigurationType.getMicroserviceType(newTypeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"new microservice type " + currentTypeIdentifier.getIdentifier() + " already exists");
		}
		var newMicroserviceType = currentMicroserviceType.createSameMicroserviceAsPatch(newVersion,
				currentConfigurationType);
		currentConfigurationType.addMicroserviceType(newMicroserviceType);
		currentActionLog.add(ActionLog.createActionLog("createSameMicroserviceTypeAsPatchVersion", currentIdentifier,
				currentVersion, newVersion));
		assert invariant();
	}

	/**
	 * creates a database system type and add it the current configuration type. A
	 * database system type is identified by an identifier and a version: e.g.
	 * "authentification_db" in version "1.0.0".
	 * 
	 * @param identifier the identifier of the database system type.
	 * @param version    the version of the database system type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws AlreadyExistsModelElementException      the database system type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createDatabaseSystemType(final String identifier, final String version)
			throws MalformedIdentifierException, AlreadyExistsModelElementException,
			IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		if (currentConfigurationType.getMicroserviceType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"microservice type " + typeIdentifier.getIdentifier() + " already exists");
		}
		var databaseSystemType = new DatabaseSystemType(identifier, new Semver(version));
		currentConfigurationType.addDatabaseSystemType(databaseSystemType);
		currentActionLog.add(ActionLog.createActionToLog("createDatabaseSystemType", identifier, version));
		assert invariant();
	}

	/**
	 * obtains the reference of a database system type in current configuration
	 * type. The database system is identified by its identifier and its version
	 * e.g. "authentification_service" in version "1.0.0".
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance} and must not be accessible by other: it
	 * returns a database system type object that is internal to the type model of
	 * the model at runtime.
	 * 
	 * @param identifier the identifier of the database system type to search for.
	 * @param version    the version of the database system type.
	 * @return the reference to the database system type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 * @throws UnknownTypeException         the database system type is unknown in
	 *                                      current configuration type.
	 */
	DatabaseSystemType getDatabaseSystemType(final String identifier, final String version)
			throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = this.computeTypeIdentifier(identifier, version);
		var databaseSystemType = currentConfigurationType.getDatabaseSystemType(typeIdentifier);
		if (databaseSystemType == null) {
			throw new UnknownTypeException("database system type " + typeIdentifier.getIdentifier()
					+ " does not exist in the current to commit configuration type");
		}
		return databaseSystemType;

	}

	/**
	 * obtains the reference of a database system type in a given configuration
	 * type. The configuration type is identified by an identifier, which is a SHA-1
	 * digest. The database system type is identified by its identifier and its
	 * version e.g. "authentification_service" in version "1.0.0".
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance} and must not be accessible by other: it
	 * returns a database system type object that is internal to the type model of
	 * the model at runtime.
	 * 
	 * @param configurationTypeIdentifier the identifier of the configuration.
	 * @param identifier                  the identifier of the database system type
	 *                                    to search for.
	 * @param version                     the version of the database system type.
	 * @return the reference to the database system type.
	 * @throws MalformedIdentifierException one of the identifier is malformed.
	 * @throws UnknownTypeException         either the configuration type or the
	 *                                      database system type in the given
	 *                                      configuration type are unknown.
	 */
	DatabaseSystemType getDatabaseSystemTypeIntoConfigurationType(final String configurationTypeIdentifier,
			final String identifier, final String version) throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		var configurationType = this.getConfigurationType(configurationTypeIdentifier);
		if (configurationType == null) {
			throw new UnknownTypeException("configuration type " + configurationTypeIdentifier + " does not exist");
		}
		var databaseSystemType = configurationType.getDatabaseSystemType(typeIdentifier);
		if (databaseSystemType == null) {
			throw new UnknownTypeException("database system type " + typeIdentifier
					+ " does not exist in configuration type " + configurationTypeIdentifier);
		}
		return databaseSystemType;

	}

	/**
	 * creates a client contract type and adds it in the current configuration type.
	 * The client contract type is identified by its identifier and its version:
	 * e.g. "userServiceRequiresAuthenticationgetlogin" in version "1.0.0".
	 * Operation declarations are parsed in the form "name paramType... :
	 * returnType", with "name" being the name of the operation, "paramType" being a
	 * value of the set {"boolean", "byte", "char", "short", "int", "long", "float",
	 * "double"} or the fully qualified JAVA class name, and with "returnType" being
	 * a primitive type or fully qualified JAVA class name.
	 * <p>
	 * Operations with fully qualified JAVA class name are for microservices that
	 * are implemented in JAVA.
	 * <p>
	 * It is not mandatory that the client contract type has an attached connector,
	 * i.e. the microservice using this client contract type accepts to function
	 * even if the client contract is not attached to a server contract.
	 * 
	 * @param identifier            the identifier of the client contract type.
	 * @param version               the version of the client contract type.
	 * @param operationDeclarations the collection of operation declarations, i.e.
	 *                              the required interface of the contract type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       there is no operation
	 *                                                 declaration.
	 * @throws AlreadyExistsModelElementException      the client contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws MalformedOperationDeclarationException  one of the operation
	 *                                                 declaration is malformed.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createClientContractType(final String identifier, final String version,
			final String... operationDeclarations)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedOperationDeclarationException, IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		if (currentConfigurationType.getClientContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"client contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		if (operationDeclarations.length == 0) {
			throw new WrongNumberOfIdentifiersException("there should be at least one operation declaration");
		}
		var declarations = new OperationDeclaration[operationDeclarations.length];
		for (var i = 0; i < operationDeclarations.length; i++) {
			declarations[i] = OperationDeclaration.parseOperationDeclaration(operationDeclarations[i]);
		}
		var clientContractType = new ClientContractType(identifier, new Semver(version), declarations);
		currentConfigurationType.addClientContractType(clientContractType);
		assert invariant();
		currentActionLog.add(
				ActionLog.createActionToLog("createClientContractType", identifier, version, operationDeclarations));
	}

	/**
	 * creates a client contract type and adds it in the current configuration type.
	 * The client contract type is identified by its identifier and its version:
	 * e.g. "userServiceRequiresAuthenticationgetlogin" in version "1.0.0".
	 * Operation declarations are parsed in the form "name paramType... :
	 * returnType", with "name" being the name of the operation, "paramType" being a
	 * value of the set {"boolean", "byte", "char", "short", "int", "long", "float",
	 * "double"} or the fully qualified JAVA class name, and with "returnType" being
	 * a primitive type or fully qualified JAVA class name.
	 * <p>
	 * Operations with fully qualified JAVA class name are for microservices that
	 * are implemented in JAVA.
	 * <p>
	 * It is not mandatory that the client contract type has an attached connector,
	 * i.e. the microservice using this client contract type accepts to function
	 * even if the client contract is not attached to a server contract.
	 * 
	 * @param identifier                the identifier of the client contract type.
	 * @param version                   the version of the client contract type.
	 * @param mustHaveConnectorAttached whether the client contract type must have a
	 *                                  connector attached or not.
	 * @param operationDeclarations     the collection of operation declarations,
	 *                                  i.e. the required interface of the contract
	 *                                  type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       there is no operation
	 *                                                 declaration.
	 * @throws AlreadyExistsModelElementException      the client contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws MalformedOperationDeclarationException  one of the operation
	 *                                                 declaration is malformed.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createClientContractType(final String identifier, final String version,
			final boolean mustHaveConnectorAttached, final String... operationDeclarations)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedOperationDeclarationException, IsCommittedAndCannotBeModifiedException {
		createClientContractType(identifier, version, operationDeclarations);
		try {
			setClientContractTypeMustBeConnected(identifier, version, mustHaveConnectorAttached);
		} catch (MalformedIdentifierException | UnknownTypeException e) {
			throw new IllegalStateException(
					identifier + " v." + version + " is an unknown client contract type, but was just created");
		}
		assert invariant();
	}

	/**
	 * declares that the given client contract type must have a connector attached.
	 * The client contract type is identified by its identifier and its version:
	 * e.g. "userServiceRequiresAuthenticationgetlogin" in version "1.0.0".
	 * 
	 * @param identifier                the identifier of the client contract type.
	 * @param version                   the version of the client contract type.
	 * @param mustHaveConnectorAttached whether the client contract type must have a
	 *                                  connector attached or not.
	 * @throws MalformedIdentifierException the identifier of the client contract is
	 *                                      malformed.
	 * @throws UnknownTypeException         the client contract type is unknown in
	 *                                      current configuration type.
	 */
	public void setClientContractTypeMustBeConnected(final String identifier, final String version,
			final boolean mustHaveConnectorAttached) throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		var clientContractType = currentConfigurationType.getClientContractType(typeIdentifier);
		if (clientContractType == null) {
			throw new UnknownTypeException("client contract type " + typeIdentifier + " does not exist");
		}
		clientContractType.setMustHaveConnectorAttached(mustHaveConnectorAttached);
		currentActionLog.add(ActionLog.createActionLog("setClientContractTypeMustBeConnected", identifier, version,
				String.valueOf(mustHaveConnectorAttached)));
		assert invariant();
	}

	/**
	 * obtains the string of a given client contract type. The client contract type
	 * is identified by its identifier and its version: e.g.
	 * "userServiceRequiresAuthenticationgetlogin" in version "1.0.0". It calls to
	 * the method {@code toString} on all the content of the client contract type.
	 * <p>
	 * Here follows an example:
	 * 
	 * <pre>
	ClientContractType [getIdentifier()=clientContractType1v.0.0.1, 
	                getOperationDeclarations()=[OperationDeclaration [signature=OperationSignature [name=m1, argumentTypes=[int, boolean]], returnType=Optional.empty],
	                                            OperationDeclaration [signature=OperationSignature [name=m2, argumentTypes=[]], returnType=Optional[boolean]]],
	                mustBeConnected=false,
	                clientServerConnectorType= connected to server contract type serverContractType2v.0.0.1]
	 * </pre>
	 * 
	 * @param identifier the identifier of the client contract type.
	 * @param version    the version of the client contract type.
	 * @return the stringified client contract type.
	 * @throws MalformedIdentifierException the identifier of the client contract
	 *                                      type is malformed.
	 * @throws UnknownTypeException         the client contract type is unknown in
	 *                                      current configuration type.
	 */
	public String toStringClientContractType(final String identifier, final String version)
			throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		var clientContractType = currentConfigurationType.getClientContractType(typeIdentifier);
		if (clientContractType == null) {
			throw new UnknownTypeException("client contract type " + typeIdentifier + " does not exist");
		}
		return clientContractType.toString();
	}

	/**
	 * creates a server contract type and adds it in the current configuration type.
	 * The server contract type is identified by its identifier and its version:
	 * e.g. "UserService_checkPassword" in version "1.0.0". Operation declarations
	 * are parsed in the form "name paramType... : returnType", with "name" being
	 * the name of the operation, "paramType" being a value of the set {"boolean",
	 * "byte", "char", "short", "int", "long", "float", "double"} or the fully
	 * qualified JAVA class name, and with "returnType" being a primitive type or
	 * fully qualified JAVA class name.
	 * <p>
	 * Operations with fully qualified JAVA class name are for microservices that
	 * are implemented in JAVA.
	 * 
	 * @param identifier            the identifier of the server contract type.
	 * @param version               the version of the server contract type.
	 * @param operationDeclarations the collection of operation declarations, i.e.
	 *                              the required interface of the contract type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       there is no operation
	 *                                                 declaration.
	 * @throws AlreadyExistsModelElementException      the server contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws MalformedOperationDeclarationException  one of the operation
	 *                                                 declaration is malformed.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createServerContractType(final String identifier, final String version,
			final String... operationDeclarations)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedOperationDeclarationException, IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		if (currentConfigurationType.getServerContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"server contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		if (operationDeclarations.length == 0) {
			throw new WrongNumberOfIdentifiersException("there should be at least one operation declaration");
		}
		var declarations = new OperationDeclaration[operationDeclarations.length];
		for (var i = 0; i < operationDeclarations.length; i++) {
			declarations[i] = OperationDeclaration.parseOperationDeclaration(operationDeclarations[i]);
		}
		var serverContractType = new ServerContractType(identifier, new Semver(version), declarations);
		currentConfigurationType.addServerContractType(serverContractType);
		currentActionLog.add(
				ActionLog.createActionToLog("createServerContractType", identifier, version, operationDeclarations));
		assert invariant();
	}

	/**
	 * creates a client-server connector type and adds it in the current
	 * configuration type. The connector is identified by the identifier and the
	 * version of the client contract type plus the identifier and the version of
	 * the server contract type. The connector is created only if the client
	 * contract type is compatible with the server contract type: all the operation
	 * of the client contract type have a compatible operation in the server
	 * contract type, i.e. the two operations are equals.
	 * 
	 * @param clientContractTypeIdentifier the identifier of the client contract
	 *                                     type.
	 * @param clientContractTypeVersion    the version of the client contract type.
	 * @param serverContractTypeIdentifier the identifier of the server contract
	 *                                     type.
	 * @param serverContractTypeVersion    the version of the server contract type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws UnknownTypeException                    on the type is unknown in
	 *                                                 current configuration type.
	 * @throws AlreadyExistsModelElementException      the connector type already
	 *                                                 exists in current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createClientServerConnectorType(final String clientContractTypeIdentifier,
			final String clientContractTypeVersion, final String serverContractTypeIdentifier,
			final String serverContractTypeVersion) throws MalformedIdentifierException, UnknownTypeException,
			AlreadyExistsModelElementException, IsCommittedAndCannotBeModifiedException {
		var clientTypeIdentifier = computeTypeIdentifier(clientContractTypeIdentifier, clientContractTypeVersion);
		var serverTypeIdentifier = computeTypeIdentifier(serverContractTypeIdentifier, serverContractTypeVersion);
		var clientContractType = currentConfigurationType.getClientContractType(clientTypeIdentifier);
		var serverContractType = currentConfigurationType.getServerContractType(serverTypeIdentifier);
		var connectorType = currentConfigurationType.getClientServerConnectorType(clientContractType,
				serverContractType);
		if (connectorType != null) {
			throw new AlreadyExistsModelElementException(
					"client server connector type " + clientContractType.getTypeIdentifier() + "connected to "
							+ serverContractType.getTypeIdentifier() + " already exists");
		}
		if (clientContractType == null) {
			throw new UnknownTypeException("client contract type " + clientTypeIdentifier + " does not exist");
		}
		if (serverContractType == null) {
			throw new UnknownTypeException("server contract type " + serverTypeIdentifier + " does not exist");
		}
		connectorType = new ClientServerConnectorType(clientContractType, serverContractType);
		currentConfigurationType.addClientServerConnectorType(connectorType);
		currentActionLog.add(ActionLog.createActionLog("createClientServerConnectorType", clientContractTypeIdentifier,
				clientContractTypeVersion, serverContractTypeIdentifier, serverContractTypeVersion));
		assert invariant();
	}

	/**
	 * creates a channel-based producer contract type and adds it in the current
	 * configuration type. The channel-based producer contract type is identified by
	 * its identifier and its version: e.g. "loggingServicePublisher" in version
	 * "1.0.0".
	 * <p>
	 * The event filter of a channel-based producer contract type is identified by
	 * its identifier and its version only: i.e., knowledge of the name (identifier
	 * plus version) of the channel alone is sufficient to publish and consume using
	 * the channel.
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * <p>
	 * It is not mandatory that messages / events that are produced through this
	 * producer contract type are consumed, i.e. the microservice using this
	 * producer contract type accepts to function even if there is no consumers of
	 * its event types.
	 * 
	 * 
	 * @param identifier            the identifier of the producer contract type.
	 * @param version               the version of the producer contract type.
	 * @param eventFilterIdentifier the identifier of the filter, i.e. the first
	 *                              part of the name of the channel.
	 * @param eventFilterVersion    the version of the filter, i.e. the second part
	 *                              of the name of the channel.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are produced by
	 *                              using this producer contract type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the producer contract type
	 *                                                 already exists in the current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createChannelBasedProducerContractType(final String identifier, final String version,
			final String eventFilterIdentifier, final String eventFilterVersion, final String... eventTypeStrings)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		computeTypeIdentifier(eventFilterIdentifier, eventFilterVersion);
		if (this.currentConfigurationType.getProducerContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"channel-based producer contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		if (eventTypeStrings.length == 0) {
			throw new WrongNumberOfIdentifiersException("there should be at least one event type");
		}
		if ((eventTypeStrings.length % 2) != 0) {
			throw new WrongNumberOfIdentifiersException(
					"the number of strings should be a multiple of 2 (wrong number = " + eventTypeStrings.length + ")");
		}
		var eventTypes = new EventType[eventTypeStrings.length / 2];
		for (var i = 0; i < eventTypeStrings.length; i = i + 2) {
			this.computeTypeIdentifier(eventTypeStrings[i], eventTypeStrings[i + 1]);
			eventTypes[i / 2] = new EventType(eventTypeStrings[i], new Semver(eventTypeStrings[i + 1]));
		}
		var filter = new ChannelBasedFilter(eventFilterIdentifier, new Semver(eventFilterVersion));
		var consumerContractType = new ChannelBasedProducerContractType(identifier, new Semver(version), filter,
				eventTypes);
		this.currentConfigurationType.addProducerContractType(consumerContractType);
		currentActionLog.add(ActionLog.createActionLog("createChannelBasedProducerContractType", identifier, version,
				eventFilterIdentifier, eventFilterVersion, eventTypeStrings));
		assert invariant();
	}

	/**
	 * creates a channel-based producer contract type and adds it in the current
	 * configuration type. The channel-based producer contract type is identified by
	 * its identifier and its version: e.g. "loggingServicePublisher" in version
	 * "1.0.0".
	 * <p>
	 * It is mandatory that messages / events that are produced through this
	 * producer contract type are consumed, i.e. the microservice using this
	 * producer contract type does not accept to function if there is no consumers
	 * of its event types. In other words, there exists a connector type attached to
	 * which such a consumer contract type connects.
	 * <p>
	 * The event filter of a channel-based producer contract type is identified by
	 * its identifier and its version only: i.e., knowledge of the name (identifier
	 * plus version) of the channel alone is sufficient to publish and consume using
	 * the channel.
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * 
	 * 
	 * @param identifier            the identifier of the producer contract type.
	 * @param version               the version of the producer contract type.
	 * @param mustHaveConsumers     this producer contract type must have consumers
	 *                              of its event types.
	 * @param eventFilterIdentifier the identifier of the filter, i.e. the first
	 *                              part of the name of the channel.
	 * @param eventFilterVersion    the version of the filter, i.e. the second part
	 *                              of the name of the channel.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are produced by
	 *                              using this producer contract type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the producer contract type
	 *                                                 already exists in the current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createChannelBasedProducerContractType(final String identifier, final String version,
			final boolean mustHaveConsumers, final String eventFilterIdentifier, final String eventFilterVersion,
			final String... eventTypeStrings) throws MalformedIdentifierException, WrongNumberOfIdentifiersException,
			AlreadyExistsModelElementException, IsCommittedAndCannotBeModifiedException {
		createChannelBasedProducerContractType(identifier, version, eventFilterIdentifier, eventFilterVersion,
				eventTypeStrings);
		try {
			setProducerContractTypeMustHaveConsumers(identifier, version, mustHaveConsumers);
		} catch (MalformedIdentifierException | UnknownTypeException e) {
			throw new IllegalStateException(
					identifier + " v." + version + " is an unknown producer contract type, but was just created");
		}
		currentActionLog.add(ActionLog.createActionLog("createChannelBasedProducerContractType", identifier, version,
				String.valueOf(mustHaveConsumers), eventFilterIdentifier, eventFilterVersion, eventTypeStrings));
		assert invariant();
	}

	/**
	 * sets the given producer contract type to have consumers, i.e. a microservice
	 * type with this producer contract type does not accept to function if there is
	 * no consumer contract types that accept its event types. In other words, there
	 * exists a connector type attached to which such a consumer contract type
	 * connects. A producer contract type is identified by its identifier and its
	 * version.
	 * 
	 * @param identifier        the identifier of the producer contract type.
	 * @param version           the version of the producer contract type.
	 * @param mustHaveConsumers the producer contract type must have a consumer
	 *                          contract type that accepts its event types.
	 * @throws MalformedIdentifierException one of the identifier is malformed.
	 * @throws UnknownTypeException         the producer contract type is unknown in
	 *                                      current configuration type.
	 */
	public void setProducerContractTypeMustHaveConsumers(final String identifier, final String version,
			final boolean mustHaveConsumers) throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		var producerContractType = currentConfigurationType.getProducerContractType(typeIdentifier);
		if (producerContractType == null) {
			throw new UnknownTypeException("producer contract type " + typeIdentifier + " does not exist");
		}
		producerContractType.setMustHaveConsumers(mustHaveConsumers);
		currentActionLog.add(ActionLog.createActionLog("setProducerContractTypeMustHaveConsumers", identifier, version,
				String.valueOf(mustHaveConsumers)));
		assert invariant();
	}

	/**
	 * creates a channel-based consumer contract type and adds it to the current
	 * configuration type. The channel-based consumer contract type is identified by
	 * its identifier and its version: e.g. "loggingService" in version "1.0.0".
	 * <p>
	 * The event filter of a channel-based consumer contract type is identified by
	 * its identifier and its version only: i.e., knowledge of the name (identifier
	 * plus version) of the channel alone is sufficient to publish and consume using
	 * the channel.
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * 
	 * @param identifier            the identifier of the consumer contract type.
	 * @param version               the version of the consumer contract type.
	 * @param eventFilterIdentifier the identifier of the filter.
	 * @param eventFilterVersion    the version of the filter.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are consumed by
	 *                              using this consumed contract type.
	 * @throws MalformedIdentifierException            one of the identfiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the consumer contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createChannelBasedConsumerContractType(final String identifier, final String version,
			final String eventFilterIdentifier, final String eventFilterVersion, final String... eventTypeStrings)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		this.computeTypeIdentifier(eventFilterIdentifier, eventFilterVersion);
		if (currentConfigurationType.getConsumerContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"channel-based consumer contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		if (eventTypeStrings.length == 0) {
			throw new WrongNumberOfIdentifiersException("there should be at least one event type");
		}
		if ((eventTypeStrings.length % 2) != 0) {
			throw new WrongNumberOfIdentifiersException(
					"the number of strings should be a multiple of 2 (wrong number = " + eventTypeStrings.length + ")");
		}
		var eventTypes = new EventType[eventTypeStrings.length / 2];
		for (var i = 0; i < eventTypeStrings.length; i = i + 2) {
			this.computeTypeIdentifier(eventTypeStrings[i], eventTypeStrings[i + 1]);
			eventTypes[i / 2] = new EventType(eventTypeStrings[i], new Semver(eventTypeStrings[i + 1]));
		}
		var filter = new ChannelBasedFilter(eventFilterIdentifier, new Semver(eventFilterVersion));
		var producerContractType = new ChannelBasedConsumerContractType(identifier, new Semver(version), filter,
				eventTypes);
		currentConfigurationType.addConsumerContractType(producerContractType);
		currentActionLog.add(ActionLog.createActionLog("createChannelBasedConsumerContractType", identifier, version,
				eventFilterIdentifier, eventFilterVersion, eventTypeStrings));
		assert invariant();
	}

	/**
	 * creates an AMQP topic-based producer contract type and adds it in the current
	 * configuration type. The producer contract type is identified by its
	 * identifier and its version: e.g. "loggingServicePublisher" in version
	 * "1.0.0".
	 * <p>
	 * The event filter of a topic-based producer contract type is identified by its
	 * identifier and its version. In addition, the producer provides a routing key,
	 * e.g. "logging.warning", when publishing.
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * <p>
	 * It is not mandatory that messages / events that are produced through this
	 * producer contract type are consumed, i.e. the microservice using this
	 * producer contract type accepts to function even if there is no consumers of
	 * its event types.
	 * 
	 * 
	 * @param identifier            the identifier of the producer contract type.
	 * @param version               the version of the producer contract type.
	 * @param eventFilterIdentifier the identifier of the filter.
	 * @param eventFilterVersion    the version of the filter.
	 * @param routingKey            the routing key used when publishing.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are produced by
	 *                              using this producer contract type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the producer contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createAMQPTopicBasedProducerContractType(final String identifier, final String version,
			final String eventFilterIdentifier, final String eventFilterVersion, final String routingKey,
			final String... eventTypeStrings) throws MalformedIdentifierException, WrongNumberOfIdentifiersException,
			AlreadyExistsModelElementException, IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = this.computeTypeIdentifier(identifier, version);
		this.computeTypeIdentifier(eventFilterIdentifier, eventFilterVersion);
		Objects.requireNonNull(routingKey, "routingKey cannot be null");
		if (this.currentConfigurationType.getProducerContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"AMQP topic-based producer contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		if (eventTypeStrings.length == 0) {
			throw new WrongNumberOfIdentifiersException("there should be at least one event type");
		}
		if ((eventTypeStrings.length % 2) != 0) {
			throw new WrongNumberOfIdentifiersException(
					"the number of strings should be a multiple of 2 (wrong number = " + eventTypeStrings.length + ")");
		}
		var eventTypes = new EventType[eventTypeStrings.length / 2];
		for (var i = 0; i < eventTypeStrings.length; i = i + 2) {
			this.computeTypeIdentifier(eventTypeStrings[i], eventTypeStrings[i + 1]);
			eventTypes[i / 2] = new EventType(eventTypeStrings[i], new Semver(eventTypeStrings[i + 1]));
		}
		// check routingKey
		var filter = new AMQPTopicBasedAdvertisementFilter(eventFilterIdentifier, new Semver(eventFilterVersion),
				routingKey);
		var producerContractType = new TopicBasedProducerContractType(identifier, new Semver(version), filter,
				eventTypes);
		this.currentConfigurationType.addProducerContractType(producerContractType);
		currentActionLog.add(ActionLog.createActionLog("createAMQPTopicBasedProducerContractType", identifier, version,
				eventFilterIdentifier, eventFilterVersion, routingKey, eventTypeStrings));
		assert invariant();
	}

	/**
	 * creates an AMQP topic-based producer contract type and adds it in the current
	 * configuration type. The producer contract type is identified by its
	 * identifier and its version: e.g. "loggingServicePublisher" in version
	 * "1.0.0".
	 * <p>
	 * The event filter of a topic-based producer contract type is identified by its
	 * identifier and its version. In addition, the producer provides a routing key,
	 * e.g. "logging.warning", when publishing.
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * <p>
	 * It is mandatory that messages / events that are produced through this
	 * producer contract type are consumed, i.e. the microservice using this
	 * producer contract type does not accept to function if there is no consumers
	 * of its event types. In other words, there exists a connector type attached to
	 * which such a consumer contract type connects.
	 * 
	 * @param identifier            the identifier of the producer contract type.
	 * @param version               the version of the producer contract type.
	 * @param mustHaveConsumers     this producer contract type must have consumers
	 *                              of its event types.
	 * @param eventFilterIdentifier the identifier of the filter.
	 * @param eventFilterVersion    the version of the filter.
	 * @param routingKey            the routing key used when publishing.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are produced by
	 *                              using this producer contract type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the producer contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createAMQPTopicBasedProducerContractType(final String identifier, final String version,
			final boolean mustHaveConsumers, final String eventFilterIdentifier, final String routingKey,
			final String eventFilterVersion, final String... eventTypeStrings)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			IsCommittedAndCannotBeModifiedException {
		createAMQPTopicBasedProducerContractType(identifier, version, eventFilterIdentifier, eventFilterVersion,
				routingKey, eventTypeStrings);
		try {
			setProducerContractTypeMustHaveConsumers(identifier, version, mustHaveConsumers);
		} catch (MalformedIdentifierException | UnknownTypeException e) {
			throw new IllegalStateException(
					identifier + " v." + version + " is an unknown producer contract type, but was just created");
		}
		currentActionLog.add(ActionLog.createActionLog("createAMQPTopicBasedProducerContractType", identifier, version,
				String.valueOf(mustHaveConsumers), eventFilterIdentifier, routingKey, eventFilterVersion,
				eventTypeStrings));
		assert invariant();
	}

	/**
	 * creates an AMQP topic-based consumer contract type and adds it to the current
	 * configuration type. The consumer contract type is identified by its
	 * identifier and its version: e.g. "loggingService" in version "1.0.0".
	 * <p>
	 * The event filter of an AMQP topic-based consumer contract type is identified
	 * by its identifier and its version. In addition, a subscription key, named a
	 * binding key in AMQP, specifies which routing key of published messages /
	 * events are consumed: e.g. "logging.*".
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * 
	 * 
	 * @param identifier            the identifier of the consumer contract type.
	 * @param version               the version of the consumer contract type.
	 * @param eventFilterIdentifier the identifier of the event filter.
	 * @param eventFilterVersion    the version of the event filter.
	 * @param subscription          the subscription key, namely the AMQP binding
	 *                              key, of the filter.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are consumed by
	 *                              using this consumer contract type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the consumer contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws MalformedSubscriptionFilterException    subscription filter, a.k.a.
	 *                                                 AMQP binding key, is
	 *                                                 malformed
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createAMQPTopicBasedConsumerContractType(final String identifier, final String version,
			final String eventFilterIdentifier, final String eventFilterVersion, final String subscription,
			final String... eventTypeStrings)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedSubscriptionFilterException, IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		computeTypeIdentifier(eventFilterIdentifier, eventFilterVersion);
		Objects.requireNonNull(subscription, "subscription cannot be null or blank");
		if (this.currentConfigurationType.getConsumerContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"AMQP topic-based consumer contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		if (eventTypeStrings.length == 0) {
			throw new WrongNumberOfIdentifiersException("there should be at least one event type");
		}
		if ((eventTypeStrings.length % 2) != 0) {
			throw new WrongNumberOfIdentifiersException(
					"the number of strings should be a multiple of 2 (wrong number = " + eventTypeStrings.length + ")");
		}
		var eventTypes = new EventType[eventTypeStrings.length / 2];
		for (var i = 0; i < eventTypeStrings.length; i = i + 2) {
			this.computeTypeIdentifier(eventTypeStrings[i], eventTypeStrings[i + 1]);
			eventTypes[i / 2] = new EventType(eventTypeStrings[i], new Semver(eventTypeStrings[i + 1]));
		}
		if (!AMQPTopicChecker.checkBindingKeyWhenSubscribing(subscription)) {
			throw new MalformedSubscriptionFilterException(
					"subscription filter '" + subscription + "' is not a MQTT subscription");
		}
		var filter = new AMQPTopicBasedSubscriptionFilter(eventFilterIdentifier, new Semver(eventFilterVersion),
				subscription);
		var consumerContractType = new TopicBasedConsumerContractType(identifier, new Semver(version), filter,
				eventTypes);
		this.currentConfigurationType.addConsumerContractType(consumerContractType);
		currentActionLog.add(ActionLog.createActionLog("createAMQPTopicBasedConsumerContractType", identifier, version,
				eventFilterIdentifier, eventFilterVersion, subscription, eventTypeStrings));
		assert invariant();
	}

	/**
	 * creates an MQTT topic-based producer contract type and adds it in the current
	 * configuration type. The producer contract type is identified by its
	 * identifier and its version: e.g. "loggingServicePublisher" in version
	 * "1.0.0".
	 * <p>
	 * The event filter of a topic-based producer contract type is identified by its
	 * identifier and its version. In addition, the producer provides a topic, e.g.
	 * "logging.warning", when publishing.
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * <p>
	 * It is not mandatory that messages / events that are produced through this
	 * producer contract type are consumed, i.e. the microservice using this
	 * producer contract type accepts to function even if there is no consumers of
	 * its event types.
	 * 
	 * 
	 * @param identifier            the identifier of the producer contract type.
	 * @param version               the version of the producer contract type.
	 * @param eventFilterIdentifier the identifier of the filter.
	 * @param eventFilterVersion    the version of the filter.
	 * @param routingKey            the routing key used when publishing.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are produced by
	 *                              using this producer contract type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the producer contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws MalformedRoutingKeyException            MQTT topic, a.k.a routing
	 *                                                 key, is malformed
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createMQTTTopicBasedProducerContractType(final String identifier, final String version,
			final String eventFilterIdentifier, final String eventFilterVersion, final String routingKey,
			final String... eventTypeStrings) throws MalformedIdentifierException, WrongNumberOfIdentifiersException,
			AlreadyExistsModelElementException, MalformedRoutingKeyException, IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		computeTypeIdentifier(eventFilterIdentifier, eventFilterVersion);
		Objects.requireNonNull(routingKey, "routingKey cannot be null");
		if (currentConfigurationType.getProducerContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"MQTT topic-based producer contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		if (eventTypeStrings.length == 0) {
			throw new WrongNumberOfIdentifiersException("there should be at least one event type");
		}
		if ((eventTypeStrings.length % 2) != 0) {
			throw new WrongNumberOfIdentifiersException(
					"the number of strings should be a multiple of 2 (wrong number = " + eventTypeStrings.length + ")");
		}
		var eventTypes = new EventType[eventTypeStrings.length / 2];
		for (var i = 0; i < eventTypeStrings.length; i = i + 2) {
			computeTypeIdentifier(eventTypeStrings[i], eventTypeStrings[i + 1]);
			eventTypes[i / 2] = new EventType(eventTypeStrings[i], new Semver(eventTypeStrings[i + 1]));
		}
		if (!MQTTTopicChecker.pubTopicCheck(routingKey)) {
			throw new MalformedRoutingKeyException("routing key '" + routingKey + "' is not a MQTT routing key");
		}
		var filter = new MQTTTopicBasedAdvertisementFilter(eventFilterIdentifier, new Semver(eventFilterVersion),
				routingKey);
		var producerContractType = new TopicBasedProducerContractType(identifier, new Semver(version), filter,
				eventTypes);
		currentConfigurationType.addProducerContractType(producerContractType);
		currentActionLog.add(ActionLog.createActionLog("createMQTTTopicBasedProducerContractType", identifier, version,
				eventFilterIdentifier, eventFilterVersion, routingKey, eventTypeStrings));
		assert invariant();
	}

	/**
	 * creates an MQTT topic-based producer contract type and adds it in the current
	 * configuration type. The producer contract type is identified by its
	 * identifier and its version: e.g. "loggingServicePublisher" in version
	 * "1.0.0".
	 * <p>
	 * The event filter of a topic-based producer contract type is identified by its
	 * identifier and its version. In addition, the producer provides a topic, e.g.
	 * "logging.warning", when publishing.
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * <p>
	 * It is mandatory that messages / events that are produced through this
	 * producer contract type are consumed, i.e. the microservice using this
	 * producer contract type does not accept to function if there is no consumers
	 * of its event types. In other words, there exists a connector type attached to
	 * which such a consumer contract type connects.
	 * 
	 * @param identifier            the identifier of the producer contract type.
	 * @param version               the version of the producer contract type.
	 * @param mustHaveConsumers     this producer contract type must have consumers
	 *                              of its event types.
	 * @param eventFilterIdentifier the identifier of the filter.
	 * @param eventFilterVersion    the version of the filter.
	 * @param routingKey            the routing key used when publishing.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are produced by
	 *                              using this producer contract type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the producer contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws MalformedRoutingKeyException            MQTT topic, a.k.a routing
	 *                                                 key, is malformed
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createMQTTTopicBasedProducerContractType(final String identifier, final String version,
			final boolean mustHaveConsumers, final String eventFilterIdentifier, final String routingKey,
			final String eventFilterVersion, final String... eventTypeStrings)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedRoutingKeyException, IsCommittedAndCannotBeModifiedException {
		createMQTTTopicBasedProducerContractType(identifier, version, eventFilterIdentifier, eventFilterVersion,
				routingKey, eventTypeStrings);
		try {
			setProducerContractTypeMustHaveConsumers(identifier, version, mustHaveConsumers);
		} catch (MalformedIdentifierException | UnknownTypeException e) {
			throw new IllegalStateException(
					identifier + " v." + version + " is an unknown producer contract type, but was just created");
		}
		currentActionLog.add(ActionLog.createActionLog("createMQTTTopicBasedProducerContractType", identifier, version,
				String.valueOf(mustHaveConsumers), eventFilterIdentifier, routingKey, eventFilterVersion,
				eventTypeStrings));
		assert invariant();
	}

	/**
	 * creates an MQTT topic-based consumer contract type and adds it to the current
	 * configuration type. The consumer contract type is identified by its
	 * identifier and its version: e.g. "loggingService" in version "1.0.0".
	 * <p>
	 * The event filter of an MQTT topic-based consumer contract type is identified
	 * by its identifier and its version. In addition, a subscription filter, e.g.
	 * "logging.#", specifies which routing key of published messages / events are
	 * consumed.
	 * <p>
	 * Event types are identified by an identifier and a version only, i.e. we do
	 * not model data-structures.
	 * 
	 * 
	 * @param identifier            the identifier of the consumer contract type.
	 * @param version               the version of the consumer contract type.
	 * @param eventFilterIdentifier the identifier of the event filter.
	 * @param eventFilterVersion    the version of the event filter.
	 * @param subscription          the subscription filter, namely the AMQP
	 *                              binding.
	 * @param eventTypeStrings      the collection of event types, i.e. pairs of
	 *                              identifiers and versions, that are consumed by
	 *                              using this consumer contract type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws WrongNumberOfIdentifiersException       the collection of event types
	 *                                                 is empty or the collection is
	 *                                                 not a set of pairs of
	 *                                                 strings, i.e. a string is
	 *                                                 missing in the last pair.
	 * @throws AlreadyExistsModelElementException      the consumer contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws MalformedSubscriptionFilterException    the subscription filter is
	 *                                                 malformed.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createMQTTTopicBasedConsumerContractType(final String identifier, final String version,
			final String eventFilterIdentifier, final String eventFilterVersion, final String subscription,
			final String... eventTypeStrings)
			throws MalformedIdentifierException, WrongNumberOfIdentifiersException, AlreadyExistsModelElementException,
			MalformedSubscriptionFilterException, IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		computeTypeIdentifier(eventFilterIdentifier, eventFilterVersion);
		Objects.requireNonNull(subscription, "subscription cannot be null or blank");
		if (this.currentConfigurationType.getConsumerContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"MQTT topic-based consumer contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		if (eventTypeStrings.length == 0) {
			throw new WrongNumberOfIdentifiersException("there should be at least one event type");
		}
		if ((eventTypeStrings.length % 2) != 0) {
			throw new WrongNumberOfIdentifiersException(
					"the number of strings should be a multiple of 2 (wrong number = " + eventTypeStrings.length + ")");
		}
		var eventTypes = new EventType[eventTypeStrings.length / 2];
		for (var i = 0; i < eventTypeStrings.length; i = i + 2) {
			computeTypeIdentifier(eventTypeStrings[i], eventTypeStrings[i + 1]);
			eventTypes[i / 2] = new EventType(eventTypeStrings[i], new Semver(eventTypeStrings[i + 1]));
		}
		if (!MQTTTopicChecker.subcriptionCheck(subscription)) {
			throw new MalformedSubscriptionFilterException(
					"subscription filter '" + subscription + "' is not a MQTT subscription");
		}
		var filter = new MQTTTopicBasedSubscriptionFilter(eventFilterIdentifier, new Semver(eventFilterVersion),
				subscription);
		var consumerContractType = new TopicBasedConsumerContractType(identifier, new Semver(version), filter,
				eventTypes);
		currentConfigurationType.addConsumerContractType(consumerContractType);
		currentActionLog.add(ActionLog.createActionLog("createMQTTTopicBasedConsumerContractType", identifier, version,
				eventFilterIdentifier, eventFilterVersion, subscription, eventTypeStrings));
		assert invariant();
	}

	/**
	 * creates a publish-subscribe connector type and adds it to the current
	 * configuration type. The connector type is identified by its identifier and
	 * its version: e.g. "rabbitmq" in version "3.9.1".
	 * 
	 * @param identifier the identifier of the connector type.
	 * @param version    the version of the connector type.
	 * @throws MalformedIdentifierException            the identifier is malformed.
	 * @throws AlreadyExistsModelElementException      the connector type already
	 *                                                 exists in current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createPubSubConnectorType(final String identifier, final String version)
			throws MalformedIdentifierException, AlreadyExistsModelElementException,
			IsCommittedAndCannotBeModifiedException {
		var connTypeIdentifier = computeTypeIdentifier(identifier, version);
		var connectorType = currentConfigurationType.getPubSubConnectorType(connTypeIdentifier);
		if (connectorType != null) {
			throw new AlreadyExistsModelElementException(
					"pub-sub connector type " + connTypeIdentifier + " already exists");
		}
		connectorType = new PubSubConnectorType(identifier, new Semver(version));
		this.currentConfigurationType.addPubSubConnectorType(connectorType);
		currentActionLog.add(ActionLog.createActionLog("createPubSubConnectorType", identifier, version));
		assert invariant();
	}

	/**
	 * obtains the reference of a publish-subscribe connector type that is in
	 * current configuration type.The connector type is identified by its identifier
	 * and its version.
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance} and must not be accessible by other: it
	 * returns a connector type object that is internal to the type model of the
	 * model at runtime.
	 * 
	 * @param identifier the identifier of the connector type.
	 * @param version    the version of the connector type.
	 * @return the reference of the connector type searched for.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 * @throws UnknownTypeException         the connector type is unknown in current
	 *                                      configuration type.
	 */
	PubSubConnectorType getPubSubConnectorType(final String identifier, final String version)
			throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		var pubSubConnectorType = currentConfigurationType.getPubSubConnectorType(typeIdentifier);
		if (pubSubConnectorType == null) {
			throw new UnknownTypeException(
					"pub sub connector type " + typeIdentifier.getIdentifier() + " does not exist");
		}
		return pubSubConnectorType;
	}

	/**
	 * adds the given producer contract type to the given publish-subscribe
	 * connector type. The producer contract type and the publish-subscribe
	 * connector type are searched for in the current configuration type. The
	 * producer contract type is identified by its identifier and its version. The
	 * connector type is also identified by its identifier and its version. The
	 * operation is idempotent, i.e. no exception is thrown if the producer contract
	 * type already exists in connector type.
	 * 
	 * @param contractTypeIdentifier  the identifier of the contract type.
	 * @param contractTypeversion     the version of the contract type.
	 * @param connectorTypeIdentifier the identifier of the connector type.
	 * @param connectorTypeVersion    the version of the connector type.
	 * @throws MalformedIdentifierException one of the identifiers is malformed.
	 * @throws UnknownTypeException         one of the types does not exist in
	 *                                      current configuration type.
	 */
	public void addProducerContractTypeToPubSubConnectorType(final String contractTypeIdentifier,
			final String contractTypeversion, final String connectorTypeIdentifier, final String connectorTypeVersion)
			throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = computeTypeIdentifier(contractTypeIdentifier, contractTypeversion);
		var connectorType = this.getPubSubConnectorType(connectorTypeIdentifier, connectorTypeVersion);
		var producerContractType = currentConfigurationType.getProducerContractType(typeIdentifier);
		if (producerContractType == null) {
			throw new UnknownTypeException("producer contract type " + typeIdentifier + " does not exist");
		}
		connectorType.addProducerContractType(producerContractType);
		currentActionLog.add(ActionLog.createActionLog("addProducerContractTypeToPubSubConnectorType",
				contractTypeIdentifier, contractTypeversion, connectorTypeIdentifier, connectorTypeVersion));
	}

	/**
	 * adds the given consumer contract type to the given publish-subscribe
	 * connector type. The consumer contract type and the publish-subscribe
	 * connector type are searched for in the current configuration type. The
	 * consumer contract type is identified by its identifier and its version. The
	 * connector type is also identified by its identifier and its version. The
	 * operation is idempotent, i.e. no exception is thrown if the consumer contract
	 * type already exists in connector type.
	 * 
	 * @param contractTypeIdentifier  the identifier of the contract type.
	 * @param contractTypeversion     the version of the contract type.
	 * @param connectorTypeIdentifier the identifier of the connector type.
	 * @param connectorTypeVersion    the version of the connector type.
	 * @throws MalformedIdentifierException one of the identifiers is malformed.
	 * @throws UnknownTypeException         one of the types does not exist in
	 *                                      current configuration type.
	 */
	public void addConsumerContractTypeToPubSubConnectorType(final String contractTypeIdentifier,
			final String contractTypeversion, final String connectorTypeIdentifier, final String connectorTypeVersion)
			throws MalformedIdentifierException, UnknownTypeException {
		var typeIdentifier = computeTypeIdentifier(contractTypeIdentifier, contractTypeversion);
		var connectorType = this.getPubSubConnectorType(connectorTypeIdentifier, connectorTypeVersion);
		var consumerContractType = currentConfigurationType.getConsumerContractType(typeIdentifier);
		if (consumerContractType == null) {
			throw new UnknownTypeException("contract type " + typeIdentifier + " does not exist");
		}
		connectorType.addConsumerContractType(consumerContractType);
		currentActionLog.add(ActionLog.createActionLog("addConsumerContractTypeToPubSubConnectorType",
				contractTypeIdentifier, contractTypeversion, connectorTypeIdentifier, connectorTypeVersion));
	}

	/**
	 * creates a database contract type and adds it in the current configuration
	 * type. The database contract type is identified by its identifier and its
	 * version: e.g. "userServiceRequiresAuthenticationgetDatabase" in version
	 * "1.0.0".
	 * 
	 * @param identifier the identifier of the database contract type.
	 * @param version    the version of the database contract type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws AlreadyExistsModelElementException      the database contract type
	 *                                                 already exists in current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createDatabaseContractType(final String identifier, final String version)
			throws MalformedIdentifierException, AlreadyExistsModelElementException,
			IsCommittedAndCannotBeModifiedException {
		var typeIdentifier = computeTypeIdentifier(identifier, version);
		if (currentConfigurationType.getClientContractType(typeIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"database contract type " + typeIdentifier.getIdentifier() + " already exists");
		}
		var databaseContractType = new DatabaseContractType(identifier, new Semver(version));
		currentConfigurationType.addDatabaseContractType(databaseContractType);
		assert invariant();
		currentActionLog.add(ActionLog.createActionToLog("createDatabaseContractType", identifier, version));
	}

	/**
	 * creates a database connector type and adds it in the current configuration
	 * type. The connector is identified by the identifier and the version of the
	 * database contract type plus the identifier and the version of the database
	 * system type.
	 * 
	 * @param databaseContractTypeIdentifier the identifier of the database contract
	 *                                       type.
	 * @param databaseContractTypeVersion    the version of the database contract
	 *                                       type.
	 * @param databaseSystemTypeIdentifier   the identifier of the database system
	 *                                       type.
	 * @param databaseSystemTypeVersion      the version of the database system
	 *                                       type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws UnknownTypeException                    on the type is unknown in
	 *                                                 current configuration type.
	 * @throws AlreadyExistsModelElementException      the connector type already
	 *                                                 exists in current
	 *                                                 configuration type.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified
	 */
	public void createDatabaseConnectorType(final String databaseContractTypeIdentifier,
			final String databaseContractTypeVersion, final String databaseSystemTypeIdentifier,
			final String databaseSystemTypeVersion) throws MalformedIdentifierException, UnknownTypeException,
			AlreadyExistsModelElementException, IsCommittedAndCannotBeModifiedException {
		var databaseContractTypeTypeIdentifier = computeTypeIdentifier(databaseContractTypeIdentifier,
				databaseContractTypeVersion);
		var databaseSystemTypeTypeIdentifier = computeTypeIdentifier(databaseSystemTypeIdentifier,
				databaseSystemTypeVersion);
		var databaseContractType = currentConfigurationType.getDatabaseContractType(databaseContractTypeTypeIdentifier);
		var databaseSystemType = currentConfigurationType.getDatabaseSystemType(databaseSystemTypeTypeIdentifier);
		var connectorType = currentConfigurationType.getDatabaseConnectorType(databaseContractType, databaseSystemType);
		if (connectorType != null) {
			throw new AlreadyExistsModelElementException(
					"database connector type " + databaseContractType.getTypeIdentifier() + " connected to "
							+ databaseSystemType.getTypeIdentifier() + " already exists");
		}
		if (databaseContractType == null) {
			throw new UnknownTypeException(
					"database contract type " + databaseContractTypeTypeIdentifier + " does not exist");
		}
		if (databaseSystemType == null) {
			throw new UnknownTypeException(
					"database system type " + databaseSystemTypeTypeIdentifier + " does not exist");
		}
		connectorType = new DatabaseConnectorType(databaseContractType, databaseSystemType);
		currentConfigurationType.addDatabaseConnectorType(connectorType);
		currentActionLog.add(ActionLog.createActionLog("createDatabaseConnectorType", databaseContractTypeIdentifier,
				databaseContractTypeVersion, databaseSystemTypeIdentifier, databaseSystemTypeVersion));
		assert invariant();
	}

	/**
	 * commits the current configuration type. Before committing, the new current
	 * configuration type is created by cloning current configuration type. After
	 * committing, the configuration type becomes the last committed configuration
	 * type and is added to the graph of configuration types. Finally, the action
	 * log of current configuration type is created as an empty new collection.
	 * 
	 * @throws IsCommittedAndCannotBeModifiedException        the configuration type
	 *                                                        is already committed.
	 * @throws IsNotInstantiableAndCannotBeCommittedException the configuration is
	 *                                                        not instantiable and
	 *                                                        cannot be modified.
	 */
	public void commitConfigurationType()
			throws IsCommittedAndCannotBeModifiedException, IsNotInstantiableAndCannotBeCommittedException {
		var future = new ConfigurationType(this.currentConfigurationType);
		currentConfigurationType.commit();
		var edge = Graphs.addEdgeWithVertices(configurationTypes, lastCommittedConfigurationType,
				currentConfigurationType);
		edge.setLogs(currentActionLog);
		lastCommittedConfigurationType = currentConfigurationType;
		currentConfigurationType = future;
		currentActionLog = new ArrayList<>();
		assert invariant();
	}

	/**
	 * checks that the given identifier is neither null nor blank. The message is
	 * for the exception that is thrown when the conditions are not satisfied.
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance}.
	 * 
	 * @param identifier the identifier to test.
	 * @param message    part of the exception message.
	 * @throws MalformedIdentifierException the functional exception thrown when the
	 *                                      conditions are not satisfied.
	 */
	void checkIdentifier(final String identifier, final String message) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, message + " cannot be null");
		if (identifier.isBlank()) {
			throw new MalformedIdentifierException(message + " cannot be blank");
		}
	}

	/**
	 * computes a type identifier object from its type identifier and a version. It
	 * checks that the given identifier and the given version are correct: the
	 * identifier cannot be null and cannot be blank, the version cannot be null and
	 * cannot be blank, the version should follow
	 * <a href="https://semver.org/">semantic versioning rules</a>. The methods
	 * returns a type identifier object.
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance} and must not be accessible by other: it
	 * returns a type identifier object that is internal to the type model of the
	 * model at runtime.
	 * 
	 * @param identifier the identifier of the type.
	 * @param version    the version of the type.
	 * @return the type identifier object.
	 * @throws MalformedIdentifierException one of the identifiers is malformed.
	 */
	TypeIdentifier computeTypeIdentifier(final String identifier, final String version)
			throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "Identifier cannot be null");
		if (identifier.isBlank()) {
			throw new MalformedIdentifierException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		if (version.isBlank()) {
			throw new MalformedIdentifierException("version cannot be blank");
		}
		return new TypeIdentifier(identifier, new Semver(version));
	}
}
