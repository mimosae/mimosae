/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.type.TypeIdentifier;
import mimosae.modelatruntime.type.connectortype.DatabaseConnectorType;

/**
 * This class defines the concept of database contract type, that is the
 * specification of a database requirement for a microservice type. The database
 * contract type is identified by its identifier and its version.
 * 
 * @author Denis Conan
 */
public final class DatabaseContractType {
	/**
	 * the identifier of the database contract type.
	 */
	private final TypeIdentifier identifier;
	/**
	 * the database connector type.
	 */
	private DatabaseConnectorType databaseConnectorType;

	/**
	 * constructs a database contract type. The database connector type is specified
	 * afterwards.
	 * 
	 * @param identifier the identifier of the type.
	 * @param version    the version of the type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public DatabaseContractType(final String identifier, final Semver version) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = TypeIdentifier.typeIdentifier(identifier, version);
		assert invariant();
	}

	/**
	 * constructs a database contract type by cloning a source object. This
	 * constructor is called when cloning a microservice type and in that
	 * constructor the database connector type is set after calling this
	 * constructor. Therefore, the database connector type is not copied.
	 * 
	 * @param origin the source database contract type.
	 */
	public DatabaseContractType(final DatabaseContractType origin) {
		Objects.requireNonNull(origin, "origin cannot be null");
		this.identifier = origin.identifier;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * <p>
	 * Since {@link #databaseConnectorType} is not set in the constructor, it is not
	 * part of the invariant.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return identifier.invariant();
	}

	/**
	 * obtains the type identifier.
	 * 
	 * @return the type identifier.
	 */
	public TypeIdentifier getTypeIdentifier() {
		return identifier;
	}

	/**
	 * sets the database connector type.
	 * 
	 * @param connectorType the connector type.
	 */
	public void setConnectorType(final DatabaseConnectorType connectorType) {
		Objects.requireNonNull(connectorType, "connectorType cannot be null");
		if (databaseConnectorType != null) {
			throw new IllegalArgumentException(connectorType + " already assigned");
		}
		this.databaseConnectorType = connectorType;
		assert invariant();
	}

	/**
	 * obtains the database connector type.
	 * 
	 * @return the connector type.
	 */
	public DatabaseConnectorType getDatabaseConnectorType() {
		return databaseConnectorType;
	}

	/**
	 * states whether the microservice type that includes this database contract
	 * type can be instantiated, i.e. the database contract type has a database
	 * connector type attached.
	 * 
	 * @return true when there is database connector type.
	 */
	public boolean isInstantiable() {
		return databaseConnectorType != null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof DatabaseContractType)) {
			return false;
		}
		DatabaseContractType other = (DatabaseContractType) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "DatabaseSystemContractType [identifier=" + identifier + ", databaseConnectorType="
				+ ((databaseConnectorType == null) ? "null"
						: " connected to database system type "
								+ databaseConnectorType.getDatabaseSystemType().getTypeIdentifier())
				+ "]";
	}
}
