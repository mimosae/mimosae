/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.connnector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.instance.DatabaseSystem;
import mimosae.modelatruntime.instance.contract.DatabaseContract;
import mimosae.modelatruntime.type.connectortype.ConnectorType;
import mimosae.modelatruntime.type.connectortype.DatabaseConnectorType;

/**
 * This class models database connector instances. Currently, there is no other
 * data than the references to the database contract (of the attached
 * microservice instance) and the database system, plus the instance identifier
 * and the conforming type. The identifier of this connector
 * 
 * @author Denis Conan
 */
public final class DatabaseConnector extends Connector {
	/**
	 * the database contract.
	 */
	private final DatabaseContract databaseContract;
	/**
	 * the database system instance.
	 */
	private final DatabaseSystem databaseSystem;

	/**
	 * constructs a new databas connector. The database contract type of the
	 * database contract must be equal to the database contract type of the
	 * connector type. Similarly, the database system type of the the database
	 * system must be equal to the database system type of the connector type. The
	 * database contract must not be already connected (to a database connector).
	 * 
	 * @param connectorType    the connector type.
	 * @param databaseContract the database contract type.
	 * @param databaseSystem   the database system type.
	 * @throws AlreadyConnectedException the database contract is already connected.
	 */
	public DatabaseConnector(final ConnectorType connectorType, final DatabaseContract databaseContract,
			final DatabaseSystem databaseSystem) throws AlreadyConnectedException {
		super(computeIdentifier(databaseContract, databaseSystem), connectorType);
		if (!(connectorType instanceof DatabaseConnectorType)) {
			throw new IllegalArgumentException("connectorType is not a database connectorType ("
					+ connectorType.getClass().getCanonicalName() + ")");
		}
		var databaseConnectorType = (DatabaseConnectorType) connectorType;
		Objects.requireNonNull(databaseContract, "databaseContract cannot be null");
		Objects.requireNonNull(databaseContract, "databaseSystem cannot be null");
		if (!databaseContract.getDatabaseContractType().equals(databaseConnectorType.getDatabaseContractType())) {
			throw new IllegalArgumentException("type of DatabaseContractType provided in databaseContract ("
					+ databaseContract.getDatabaseContractType().getTypeIdentifier()
					+ ") is different from connectorType");
		}
		if (!databaseSystem.getDatabaseSystemType().equals(databaseConnectorType.getDatabaseSystemType())) {
			throw new IllegalArgumentException("type of DatabaseSystemType provided in databaseSystemContract ("
					+ databaseSystem.getDatabaseSystemType().getTypeIdentifier() + ") is different from connectorType");
		}
		this.databaseContract = databaseContract;
		this.databaseSystem = databaseSystem;
		this.databaseContract.setConnector(this);
		assert invariant();
	}

	/**
	 * constructs a new database connector by cloning a source database connector.
	 * The identifier of the new connector is equal; hence, the two connectors are
	 * equal. The connector type of the two connectors are the same object. The
	 * database contracts of the two connectors must have the same database contract
	 * type, are equals, but are not the same objects. Similarly, the database
	 * systems of the two connectors must have the same database contract type, are
	 * equals, but a priori are not the same objects.
	 * <p>
	 * By construction, the database contract type of the database contract is equal
	 * to the database contract type of the connector type. Similarly, the database
	 * system type of the database system is equal to the database system type of
	 * the connector type.
	 * <p>
	 * The database contract must not be already connected (to a database
	 * connector).
	 * 
	 * @param origin           the source connector.
	 * @param databaseContract the database contract.
	 * @param databaseSystem   the database system.
	 * @throws AlreadyConnectedException the database contract is already connected.
	 */
	public DatabaseConnector(final DatabaseConnector origin, final DatabaseContract databaseContract,
			final DatabaseSystem databaseSystem) throws AlreadyConnectedException {
		super(origin);
		var databaseConnectorType = (DatabaseConnectorType) origin.getConnectorType();
		Objects.requireNonNull(databaseContract, "databaseContract cannot be null");
		Objects.requireNonNull(databaseSystem, "databaseSystem cannot be null");
		if (!databaseContract.getDatabaseContractType().equals(databaseConnectorType.getDatabaseContractType())) {
			throw new IllegalArgumentException("type of DatabaseContractType provided in databaseContract ("
					+ databaseContract.getDatabaseContractType().getTypeIdentifier()
					+ ") is different from connectorType");
		}
		if (!databaseSystem.getDatabaseSystemType().equals(databaseConnectorType.getDatabaseSystemType())) {
			throw new IllegalArgumentException("type of DatabaseSystemType provided in databaseSystem ("
					+ databaseSystem.getDatabaseSystemType().getTypeIdentifier() + ") is different from connectorType");
		}
		this.databaseContract = databaseContract;
		this.databaseContract.setConnector(this);
		this.databaseSystem = databaseSystem;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return this.databaseContract != null && this.databaseSystem != null;
	}

	/**
	 * computes the instance identifier as being the concatenation of of the
	 * identifier of the database contract (cf.
	 * {@link DatabaseContract#getIdentifier()} and the identifier of the database
	 * system (cf. {@link DatabaseSystem#getIdentifier()}. Therefore, the identifier
	 * includes the identifier of the "client" microservice.
	 * 
	 * @param databaseContract the database contract.
	 * @param databaseSystem   the database system.
	 * @return the instance identifier.
	 */
	public static String computeIdentifier(final DatabaseContract databaseContract,
			final DatabaseSystem databaseSystem) {
		Objects.requireNonNull(databaseContract, "database contract cannot be null");
		Objects.requireNonNull(databaseSystem, "database system cannot be null");
		return databaseContract.getIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ databaseSystem.getIdentifier();
	}

	/**
	 * obtains the collection of identifiers composed of the database contract only.
	 * 
	 * @return the collection of identifiers.
	 */
	public List<String> getIdentifierOfAttachedContracts() {
		List<String> ids = new ArrayList<>();
		ids.add(databaseContract.getIdentifier());
		return ids;
	}

	/**
	 * obtains the database connector type.
	 * 
	 * @return the database connector type.
	 */
	public DatabaseConnectorType getDatabaseConnectorType() {
		return (DatabaseConnectorType) super.getConnectorType();
	}

	/**
	 * obtains the database contract.
	 * 
	 * @return the database contract.
	 */
	public DatabaseContract getDatabaseContract() {
		return databaseContract;
	}

	/**
	 * obtains the database system.
	 * 
	 * @return the database system.
	 */
	public DatabaseSystem getDatabaseSystem() {
		return databaseSystem;
	}

	@Override
	public String toString() {
		return "DatabaseConnector [identifier=" + getIdentifier() + ", databaseConnectorType=" + getConnectorType()
				+ ", databaseContract=" + databaseContract.getIdentifier() + ", databaseSystem="
				+ databaseSystem.getIdentifier() + "]";
	}
}
