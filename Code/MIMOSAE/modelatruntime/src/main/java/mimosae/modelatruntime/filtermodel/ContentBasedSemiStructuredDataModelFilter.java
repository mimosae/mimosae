/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.filtermodel;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

/**
 * This class is for modeling content-based filters for semi-structured data
 * model. Up to now, we do not consider content-based filtering in the
 * framework: This is for future works.
 * <p>
 * NB: the name of the class does not contain "Type" because there is no need
 * for the corresponding class with "Instance": i.e. contrary to the concepts of
 * "configuration type", "microservice type", "database system type", etc. This
 * is so because instances of filters are not modeled: this class is used only
 * the "type" part of the modeling.
 * 
 * @author Denis Conan
 */
public final class ContentBasedSemiStructuredDataModelFilter extends EventFilter {
	/**
	 * constructs a new filter. This method is to be complemented in future works.
	 * 
	 * @param identifier the identifier of the filter.
	 * @param version    the version of the filter.
	 */
	public ContentBasedSemiStructuredDataModelFilter(final String identifier, final Semver version) {
		super(identifier, version);
	}

	/**
	 * states whether this filter is compatible with the given filter. This method
	 * is to be implemented in future works.
	 * 
	 * @param eventFilter the filter to compare to.
	 * @return true if they are compatible.
	 */
	public boolean isCompatibleWith(final EventFilter eventFilter) {
		Objects.requireNonNull(eventFilter, "eventFilter cannot be null");
		throw new UnsupportedOperationException("to do");
	}

	@Override
	public String toString() {
		return "ContentBasedSemiStructuredDataModelFilter [getIdentifier()=" + getIdentifier() + ", getVersion()="
				+ getVersion() + "]";
	}
}
