/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.contracttype.ConsumerContractType;

/**
 * This class defines the concept of consumer contract instance. It has an
 * identifier; it conforms to a consumer contract type; it can be attached to a
 * publish-subscribe connector without being aware of the attachment (no
 * reference to the connector in this class); and it belongs to a microservice
 * instance.
 * <p>
 * The consumer contract is identified by an instance identifier that is the
 * concatenation of the consumer contract type identifier with the instance
 * identifier of the microservice to which it belongs.
 * 
 * @author Denis Conan
 */
public final class ConsumerContract {
	/**
	 * the instance identifier. It is the concatenation of the consumer contract
	 * type identifier with the instance identifier of the microservice.
	 */
	private final String identifier;
	/**
	 * the consumer contract type to which it conforms.
	 */
	private ConsumerContractType consumerContractType;
	/**
	 * the microservice to which it belongs.
	 */
	private final Microservice microservice;

	/**
	 * constructs a new consumer contract. This new consumer contract is created
	 * when creating the corresponding microservice, i.e. the new object is added to
	 * the collection of consumer contracts in the calling constructor of the
	 * microservice afterwards (Cf. {@link Microservice#Microservice(Microservice)}.
	 * 
	 * @param consumerContractType the consumer contract type it conforms to.
	 * @param microservice         the microservice to which it belongs.
	 */
	public ConsumerContract(final ConsumerContractType consumerContractType, final Microservice microservice) {
		Objects.requireNonNull(consumerContractType, "consumerContractType cannot be null");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, consumerContractType);
		this.consumerContractType = consumerContractType;
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * constructs a new consumer contract by cloning a source consumer contract. The
	 * new consumer contract belongs to a microservice that is different from the
	 * microservice of the source consumer contract. The identifiers of the source
	 * and the new objects are different, and thus the source and new objects are
	 * not equal. The consumer contract types are the same.
	 * <p>
	 * This constructor is called when creating the corresponding microservice by
	 * cloning, i.e. the new object is added to the collection of consumer contracts
	 * in the calling constructor of the new microservice afterwards. (Cf.
	 * {@link Microservice#Microservice(String, mimosae.modelatruntime.type.MicroserviceType)}).
	 * 
	 * @param origin       the source consumer contract.
	 * @param microservice the microservice to which the new consumer contract
	 *                     belongs.
	 */
	public ConsumerContract(final ConsumerContract origin, final Microservice microservice) {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, origin.consumerContractType);
		this.consumerContractType = origin.consumerContractType;
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * constructs a new consumer contract by cloning a source consumer contract. The
	 * new consumer contract belongs to a microservice that is different from the
	 * microservice of the source consumer contract. The configuration type is also
	 * new. The identifiers of the source and the new objects are different, and
	 * thus the source and new objects are not equal. The consumer contract type
	 * must belong to the new configuration type. The consumer contract type of the
	 * new consumer contract and the consumer contract type of the source consumer
	 * contract are different objects, but they are equal.
	 * <p>
	 * The consumer contract type of the source consumer contract must belong to the
	 * new configuration type. Similarly, the microservice type of the microservice
	 * must belong to the new configuration type.
	 * <p>
	 * This constructor is called when creating the corresponding microservice by
	 * cloning, i.e. the new object is added to the collection of consumer contracts
	 * in the calling constructor of the new microservice afterwards. Cf.
	 * {@link Microservice#Microservice(Microservice, ConfigurationType)}.
	 * 
	 * @param origin            the source consumer contract.
	 * @param microservice      the microservice to which the new consumer contract
	 *                          belongs.
	 * @param configurationType the new configuration type.
	 * @throws UnknownTypeException one of the types is unknown in the new
	 *                              configuration type.
	 */
	public ConsumerContract(final ConsumerContract origin, final Microservice microservice,
			final ConfigurationType configurationType) throws UnknownTypeException {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(configurationType, "configuration type cannot be null");
		var newConsumerContractType = configurationType
				.getConsumerContractType(origin.consumerContractType.getTypeIdentifier());
		if (newConsumerContractType == null) {
			throw new UnknownTypeException("consumer contract type " + origin.consumerContractType.getTypeIdentifier()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		if (configurationType.getMicroserviceType(microservice.getMicroserviceType().getTypeIdentifier()) == null) {
			throw new UnknownTypeException("microservice type " + microservice.getMicroserviceType()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		consumerContractType = newConsumerContractType;
		this.identifier = computeIdentifier(microservice, consumerContractType);
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return the invariant holds.
	 */
	public boolean invariant() {
		return identifier != null && !identifier.isBlank() && consumerContractType != null && microservice != null;
	}

	/**
	 * computes the identifier of the consumer contract as being the concatenation
	 * of the consumer contract type identifier with the instance identifier of the
	 * microservice.
	 * 
	 * @param microservice         the microservice to which this consumer contract
	 *                             belongs.
	 * @param consumerContractType the consumer contract type.
	 * @return the instance identifier.
	 */
	public static String computeIdentifier(final Microservice microservice,
			final ConsumerContractType consumerContractType) {
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(consumerContractType, "consumerContractType cannot be null");
		return consumerContractType.getTypeIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getMicroserviceType().getTypeIdentifier();
	}

	/**
	 * obtains the instance identifier of the consumer contract. The identifier of
	 * the consumer contract is the concatenation of the consumer contract type
	 * identifier with the instance identifier of the microservice.
	 * 
	 * @return the instance identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL identifier of the instance identifier. Characters "/" are
	 * replaced by strings "ZZ". The PDDL identifier is used in PDDL domain file.
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return identifier.replace("/", "ZZ");
	}

	/**
	 * obtains the consumer contract type to which it conforms.
	 * 
	 * @return the consumer contract type.
	 */
	public ConsumerContractType getConsumerContractType() {
		return consumerContractType;
	}

	/**
	 * obtains the microservice to which the consumer contract belongs.
	 * 
	 * @return the microservice.
	 */
	public Microservice getMicroservice() {
		return microservice;
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ConsumerContract)) {
			return false;
		}
		ConsumerContract other = (ConsumerContract) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "ConsumerContract [identifier=" + identifier + ", consumerContractType=" + consumerContractType + "]";
	}
}
