// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;

public final class ContentBasedStructuredDataModelProducerContractType extends PubSubContractType {
	protected ContentBasedStructuredDataModelProducerContractType(final String identifier, final Semver version)
			throws MalformedIdentifierException {
		super(identifier, version);
	}

	protected ContentBasedStructuredDataModelProducerContractType(
			final ContentBasedStructuredDataModelProducerContractType origin) {
		super(origin);
	}

	public boolean isCompatibleWith(final ConsumerContractType consumerContractType) {
		Objects.requireNonNull(consumerContractType, "consumerContractType cannot be null");
		throw new UnsupportedOperationException("to do");
	}

	@Override
	public String toString() {
		return "ContentBasedStructuredDataModelProducerContractType [getIdentifier()=" + getTypeIdentifier() + "]";
	}
}
