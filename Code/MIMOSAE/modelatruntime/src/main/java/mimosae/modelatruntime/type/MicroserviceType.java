/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.vdurmont.semver4j.Semver;
import com.vdurmont.semver4j.Semver.VersionDiff;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.WrongNewVersionException;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.ConsumerContractType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;
import mimosae.modelatruntime.type.contracttype.ProducerContractType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;

/**
 * This class defines the concept of microservice type. It is identified by its
 * identifier and its version. It contains collections of client contract types,
 * server contract types, producer contract types, consumer contract types, and
 * database contract types.
 * 
 * @author Denis Conan
 */
public final class MicroserviceType {
	/**
	 * the type identifier.
	 */
	private final TypeIdentifier identifier;
	/**
	 * the collection of client contract types.
	 */
	private final List<ClientContractType> clientContractTypes;
	/**
	 * the collection of server contract types.
	 */
	private final List<ServerContractType> serverContractTypes;
	/**
	 * the collection of producer contract types.
	 */
	private final List<ProducerContractType> producerContractTypes;
	/**
	 * the collection of consumer contract types.
	 */
	private final List<ConsumerContractType> consumerContractTypes;
	/**
	 * the collection of database contract types.
	 */
	private final List<DatabaseContractType> databaseContractTypes;

	/**
	 * construct a new microservice type. A microservice type is identified by its
	 * identifier and its version. The collections of client contract types, server
	 * contract types, producer contract types, consumer contract types, and
	 * database contract types are all empty.
	 * 
	 * @param identifier the identifier of the type.
	 * @param version    the version of the type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public MicroserviceType(final String identifier, final Semver version) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = TypeIdentifier.typeIdentifier(identifier, version);
		this.clientContractTypes = new ArrayList<>();
		this.serverContractTypes = new ArrayList<>();
		this.producerContractTypes = new ArrayList<>();
		this.consumerContractTypes = new ArrayList<>();
		this.databaseContractTypes = new ArrayList<>();
		assert invariant();
	}

	/**
	 * constructs a new microservice type by cloning an existing microservice type
	 * and in a new configuration type. The identifiers and the versions are the
	 * same; hence, the two objects are equal.
	 * <p>
	 * This constructor is used when cloning a configuration type: see
	 * {@link ConfigurationType#ConfigurationType(ConfigurationType)}.
	 * <p>
	 * Before copying a contract type, the method checks that the contract type
	 * exists in the new configuration type. Before calling this constructor,
	 * {@link ConfigurationType#ConfigurationType(ConfigurationType)} has cloned the
	 * contract types.
	 * 
	 * @param origin               the source microservice type.
	 * @param newConfigurationType the new configuration type.
	 */
	public MicroserviceType(final MicroserviceType origin, final ConfigurationType newConfigurationType) {
		Objects.requireNonNull(origin, "origin cannot be null");
		Objects.requireNonNull(newConfigurationType, "configuration type cannot be null");
		this.identifier = origin.getTypeIdentifier();
		this.clientContractTypes = new ArrayList<>();
		this.serverContractTypes = new ArrayList<>();
		this.producerContractTypes = new ArrayList<>();
		this.consumerContractTypes = new ArrayList<>();
		this.databaseContractTypes = new ArrayList<>();
		copyContractTypesFromOrigin(origin, newConfigurationType);
		assert invariant();
	}

	/**
	 * constructs a new microservice type by cloning an existing microservice type.
	 * The new microservice type is a patch version of the existing microservice
	 * type.
	 * <p>
	 * Before copying a contract type, the method checks that the contract type
	 * exists in the configuration type. This explains the presence of the argument
	 * {@code configurationType}.
	 * <p>
	 * This constructor is private and is only called in method
	 * {@link #createSameMicroserviceAsPatch(String, ConfigurationType)}.
	 * 
	 * @param origin
	 * @param configurationType
	 * @param newVersion
	 * @throws MalformedIdentifierException
	 */
	private MicroserviceType(final MicroserviceType origin, final ConfigurationType configurationType,
			final String newVersion) throws MalformedIdentifierException {
		Objects.requireNonNull(origin, "origin cannot be null");
		Objects.requireNonNull(configurationType, "configuration type cannot be null");
		Objects.requireNonNull(newVersion, "new version cannot be null");
		this.identifier = new TypeIdentifier(origin.getTypeIdentifier().getIdentifier(), new Semver(newVersion));
		this.clientContractTypes = new ArrayList<>();
		this.serverContractTypes = new ArrayList<>();
		this.producerContractTypes = new ArrayList<>();
		this.consumerContractTypes = new ArrayList<>();
		this.databaseContractTypes = new ArrayList<>();
		copyContractTypesFromOrigin(origin, configurationType);
		assert invariant();
	}

	/**
	 * copies the collections of contract types from an existing microservice type
	 * into this microservice type. Before copying a contract type, the method
	 * checks that the contract type exists in the configuration type.
	 * 
	 * @param origin            the source microservice type.
	 * @param configurationType the configuration type into which the contract types
	 *                          must exist.
	 */
	private void copyContractTypesFromOrigin(final MicroserviceType origin, final ConfigurationType configurationType) {
		Objects.requireNonNull(origin, "origin cannot be null");
		Objects.requireNonNull(configurationType, "configuration type cannot be null");
		for (ClientContractType originContract : origin.clientContractTypes) {
			var thisContract = configurationType.getClientContractType(originContract.getTypeIdentifier());
			if (thisContract == null) {
				throw new IllegalStateException("client contract type " + originContract.getTypeIdentifier()
						+ " does not exist in the current configuration type");
			}
			this.clientContractTypes.add(thisContract);
		}
		for (ServerContractType originContract : origin.serverContractTypes) {
			var thisContract = configurationType.getServerContractType(originContract.getTypeIdentifier());
			if (thisContract == null) {
				throw new IllegalStateException("server contract type " + originContract.getTypeIdentifier()
						+ " does not exist in the current configuration type");
			}
			this.serverContractTypes.add(thisContract);
		}
		for (ProducerContractType originContract : origin.producerContractTypes) {
			var thisContract = configurationType.getProducerContractType(originContract.getTypeIdentifier());
			if (thisContract == null) {
				throw new IllegalStateException("producer contract type " + originContract.getTypeIdentifier()
						+ " does not exist in the current configuration type");
			}
			this.producerContractTypes.add(thisContract);
		}
		for (ConsumerContractType originContract : origin.consumerContractTypes) {
			var thisContract = configurationType.getConsumerContractType(originContract.getTypeIdentifier());
			if (thisContract == null) {
				throw new IllegalStateException("consumer contract type " + originContract.getTypeIdentifier()
						+ " does not exist in the current configuration type");
			}
			this.consumerContractTypes.add(thisContract);
		}
		for (DatabaseContractType originContract : origin.databaseContractTypes) {
			var thisContract = configurationType.getDatabaseContractType(originContract.getTypeIdentifier());
			if (thisContract == null) {
				throw new IllegalStateException("database contract type " + originContract.getTypeIdentifier()
						+ " does not exist in the current configuration type");
			}
			this.databaseContractTypes.add(thisContract);
		}
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return identifier != null && identifier.invariant();
	}

	/**
	 * obtains the type identifier of the microservice type.
	 * 
	 * @return the type identifier.
	 */
	public TypeIdentifier getTypeIdentifier() {
		return identifier;
	}

	/**
	 * obtains the collection of client contract types.
	 * 
	 * @return the collection of client contract types.
	 */
	public List<ClientContractType> getClientContractTypes() {
		return clientContractTypes;
	}

	/**
	 * obtains the collection of server contract types.
	 * 
	 * @return the collection of server contract types.
	 */
	public List<ServerContractType> getServerContractTypes() {
		return serverContractTypes;
	}

	/**
	 * obtains the collection of producer contract types.
	 * 
	 * @return the collection of producer contract types.
	 */
	public List<ProducerContractType> getProducerContractTypes() {
		return producerContractTypes;
	}

	/**
	 * obtains the collection of consumer contract types.
	 * 
	 * @return the collection of consumer contract types.
	 */
	public List<ConsumerContractType> getConsumerContractTypes() {
		return consumerContractTypes;
	}

	/**
	 * obtains the collection of database contract types.
	 * 
	 * @return the collection of database contract types.
	 */
	public List<DatabaseContractType> getDatabaseContractTypes() {
		return databaseContractTypes;
	}

	/**
	 * adds some client contract types to the microservice type. The vararg argument
	 * must not be empty and must not contain null references. This method assumes
	 * that the client contract types are created before the call and that they
	 * exist in the same configuration type of as the configuration type of this
	 * microservice.
	 * 
	 * @param clientContractTypes the collection of client contract types to add.
	 */
	public void addClientContracType(final ClientContractType... clientContractTypes) {
		for (ClientContractType toAdd : clientContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("clientContractTypes cannot contain null element");
			}
			if (this.clientContractTypes.contains(toAdd)) {
				throw new IllegalArgumentException(toAdd + "already in clientContractTypes");
			}
			this.clientContractTypes.add(toAdd);
		}
		assert invariant();
	}

	/**
	 * adds some server contract types to the microservice type. The vararg argument
	 * must not be empty and must not contain null references. This method assumes
	 * that the server contract types are created before the call and that they
	 * exist in the same configuration type of as the configuration type of this
	 * microservice.
	 * 
	 * @param serverContractTypes the collection of server contract types to add.
	 */
	public void addServerContracType(final ServerContractType... serverContractTypes) {
		for (ServerContractType toAdd : serverContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("serverContractTypes cannot contain null element");
			}
			if (this.serverContractTypes.contains(toAdd)) {
				throw new IllegalArgumentException(toAdd + "already in serverContractTypes");
			}
			this.serverContractTypes.add(toAdd);
		}
		assert invariant();
	}

	/**
	 * adds some producer contract types to the microservice type. The vararg
	 * argument must not be empty and must not contain null references. This method
	 * assumes that the producer contract types are created before the call and that
	 * they exist in the same configuration type of as the configuration type of
	 * this microservice.
	 * 
	 * @param producerContractTypes the collection of producer contract types to
	 *                              add.
	 */
	public void addProducerContracType(final ProducerContractType... producerContractTypes) {
		for (ProducerContractType toAdd : producerContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("producerContractTypes cannot contain null element");
			}
			if (this.producerContractTypes.contains(toAdd)) {
				throw new IllegalArgumentException(toAdd + "already in producerContractTypes");
			}
			this.producerContractTypes.add(toAdd);
		}
		assert invariant();
	}

	/**
	 * adds some consumer contract types to the microservice type. The vararg
	 * argument must not be empty and must not contain null references. This method
	 * assumes that the consumer contract types are created before the call and that
	 * they exist in the same configuration type of as the configuration type of
	 * this microservice.
	 * 
	 * @param consumerContractTypes the collection of consumer contract types to
	 *                              add.
	 */
	public void addConsumerContracType(final ConsumerContractType... consumerContractTypes) {
		for (ConsumerContractType toAdd : consumerContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("consumerContractTypes cannot contain null element");
			}
			if (this.consumerContractTypes.contains(toAdd)) {
				throw new IllegalArgumentException(toAdd + "already in consumerContractTypes");
			}
			this.consumerContractTypes.add(toAdd);
		}
		assert invariant();
	}

	/**
	 * adds some database contract types to the microservice type. The vararg
	 * argument must not be empty and must not contain null references. This method
	 * assumes that the database contract types are created before the call and that
	 * they exist in the same configuration type of as the configuration type of
	 * this microservice.
	 * 
	 * @param databaseContractTypes the collection of database contract types to
	 *                              add.
	 */
	public void addDatabaseContracType(final DatabaseContractType... databaseContractTypes) {
		for (DatabaseContractType toAdd : databaseContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("databaseContractTypes cannot contain null element");
			}
			if (this.databaseContractTypes.contains(toAdd)) {
				throw new IllegalArgumentException(toAdd + "already in databaseContractTypes");
			}
			this.databaseContractTypes.add(toAdd);
		}
		assert invariant();
	}

	/**
	 * states whether the microservice type is instantiable, i.e. there is at least
	 * one contract type and all the client contract types are instantiable (see
	 * {@link ClientContractType#isInstantiable()}) and all the producer contract
	 * types are instantiable (see {@link ProducerContractType#isInstantiable()} and
	 * all the database contract types are instantiable (see
	 * {@link DatabaseContractType#isInstantiable()}.
	 * 
	 * @return true if the microservice type is instantiable.
	 */
	public boolean isInstantiable() {
		return !(clientContractTypes.isEmpty() && serverContractTypes.isEmpty() && producerContractTypes.isEmpty()
				&& consumerContractTypes.isEmpty() && databaseContractTypes.isEmpty())
				&& clientContractTypes.stream().allMatch(ClientContractType::isInstantiable)
				&& producerContractTypes.stream().allMatch(ProducerContractType::isInstantiable)
				&& databaseContractTypes.stream().allMatch(DatabaseContractType::isInstantiable);
	}

	/**
	 * creates a new microservice type as a patch of an existing microservice type
	 * and in a new configuration type.
	 * <p>
	 * The version of the new microservice type must be a patch of the version of
	 * the existing microservice type.
	 * 
	 * @param newVersion           the version of the new microservice type.
	 * @param newConfigurationType the new configuration type.
	 * @return the new microservice type.
	 * @throws WrongNewVersionException     the new version is not a patch of the
	 *                                      existing version.
	 * @throws MalformedIdentifierException one of the identifiers is malformed.
	 */
	public MicroserviceType createSameMicroserviceAsPatch(final String newVersion,
			final ConfigurationType newConfigurationType)
			throws WrongNewVersionException, MalformedIdentifierException {
		Objects.requireNonNull(newVersion, "new version cannot be null");
		Objects.requireNonNull(newConfigurationType, "new configuration typen cannot be null");
		if (!identifier.getVersion().diff(newVersion).equals(VersionDiff.PATCH)) {
			throw new WrongNewVersionException("new version of current microservice " + identifier + " is not a patch ("
					+ identifier.getVersion() + "->" + newVersion + ")");
		}
		assert invariant();
		return new MicroserviceType(this, newConfigurationType, newVersion);
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof MicroserviceType)) {
			return false;
		}
		MicroserviceType other = (MicroserviceType) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "MicroservicesType\n\t\t [identifier=" + identifier + ",\n\t\t clientContractTypes="
				+ clientContractTypes + ",\n\t\t serverContractTypes="
				+ serverContractTypes.stream().map(ServerContractType::toString)
						.collect(Collectors.joining(",", "[", "]"))
				+ ",\n\t\t producerContractTypes=" + producerContractTypes + ",\n\t\t consumerContractTypes="
				+ consumerContractTypes + ",\n\t\t databaseContractTypes=" + databaseContractTypes + "]";
	}
}
