/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;
import com.vdurmont.semver4j.Semver.VersionDiff;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.WrongNewVersionException;

/**
 * This class defines the concept of database system type. It is identified by
 * its identifier and its version. In this class, we do not model the structure
 * of the database system types. Therefore, the only attributes are the
 * identifier and the version of the type.
 * 
 * @author Denis Conan
 */
public final class DatabaseSystemType {
	/**
	 * the type identifier.
	 */
	private final TypeIdentifier identifier;

	/**
	 * constructs a new database system type.
	 * 
	 * @param identifier the identifier of the type.
	 * @param version    the version of the type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public DatabaseSystemType(final String identifier, final Semver version) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = TypeIdentifier.typeIdentifier(identifier, version);
	}

	/**
	 * constructs a new database system type by cloning an existing database system
	 * type and in the context of a new configuration type. Since a database system
	 * type contains only its identifier and its version, the new configuration type
	 * is of no use. This constructor is called in the constructor of configuration
	 * type by cloning. This new database system type is added to the new
	 * configuration type in the constructor
	 * {@link ConfigurationType#ConfigurationType(ConfigurationType)} afterwards.
	 * <p>
	 * The identifiers and the versions are the same; hence, the two objects are
	 * equal.
	 * 
	 * @param origin            the source database system type.
	 * @param configurationType the new configuration type.
	 */
	public DatabaseSystemType(final DatabaseSystemType origin, final ConfigurationType configurationType) {
		Objects.requireNonNull(origin, "origin cannot be null");
		Objects.requireNonNull(configurationType, "configuration type cannot be null");
		this.identifier = origin.getTypeIdentifier();
	}

	/**
	 * constructs a new database system type by cloning an existing database system
	 * type and with a new version. This constructor is private and is called by
	 * method {@link #createSameDataBaseSystemAsPatch(String, ConfigurationType)}.
	 * 
	 * @param origin     the source database system type.
	 * @param newVersion the new version.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	private DatabaseSystemType(final DatabaseSystemType origin, final String newVersion)
			throws MalformedIdentifierException {
		Objects.requireNonNull(origin, "origin cannot be null");
		Objects.requireNonNull(newVersion, "new version cannot be null");
		this.identifier = new TypeIdentifier(origin.getTypeIdentifier().getIdentifier(), new Semver(newVersion));
	}

	/**
	 * obtains the type of the identifier.
	 * 
	 * @return the type identifier.
	 */
	public TypeIdentifier getTypeIdentifier() {
		return identifier;
	}

	/**
	 * creates a new database system type that is a patch version of this database
	 * system type.
	 * 
	 * @param newVersion the new version.
	 * @return the new database system type.
	 * @throws WrongNewVersionException     the new version is not a patch of the
	 *                                      version of this object.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public DatabaseSystemType createSameDataBaseSystemAsPatch(final String newVersion)
			throws WrongNewVersionException, MalformedIdentifierException {
		Objects.requireNonNull(newVersion, "new version cannot be null");
		if (!identifier.getVersion().diff(newVersion).equals(VersionDiff.PATCH)) {
			throw new WrongNewVersionException("new version of current database system " + identifier
					+ " is not a patch (" + identifier.getVersion() + "->" + newVersion + ")");
		}
		return new DatabaseSystemType(this, newVersion);
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof DatabaseSystemType)) {
			return false;
		}
		DatabaseSystemType other = (DatabaseSystemType) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "DataBaseSystemType [identifier=" + identifier + "]";
	}
}
