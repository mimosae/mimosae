/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance;

import java.util.Objects;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.DatabaseSystemType;

/**
 * This class defines the concept of database system (instance). A database
 * system is identifier by the identifier of the instance plus the identifier
 * and the version of its type: e.g. "database_users_replica_1" plus
 * "database_user_type" in version "0.0.1". A database system conforms to a
 * database system type.
 *
 * @author Denis Conan
 */
public final class DatabaseSystem {
	/**
	 * the instance identifier.
	 */
	private final InstanceIdentifier identifier;
	/**
	 * the conforming type.
	 */
	private final DatabaseSystemType databaseSystemType;

	/**
	 * constructs a new database system. The instance identifier is built using the
	 * given identifier, and the type and the version of the given database system
	 * type.
	 * 
	 * @param identifier         the identifier for building the instance
	 *                           identifier.
	 * @param databaseSystemType the conforming type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public DatabaseSystem(final String identifier, final DatabaseSystemType databaseSystemType)
			throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		Objects.requireNonNull(databaseSystemType, "databaseSystemType cannot be null");
		this.identifier = new InstanceIdentifier(identifier, databaseSystemType.getTypeIdentifier());
		this.databaseSystemType = databaseSystemType;
		assert invariant();
	}

	/**
	 * constructs a new database system by cloning a source database system object.
	 * The instance identifiers and the conforming types are the same, i.e. the
	 * source and the new objects are equal. This constructor is called when cloning
	 * a configuration, e.g. when committing.
	 * 
	 * @param origin the source object to clone.
	 */
	public DatabaseSystem(final DatabaseSystem origin) {
		Objects.requireNonNull(origin, "cannot clone null origin");
		this.identifier = origin.identifier;
		this.databaseSystemType = origin.databaseSystemType;
		assert invariant();
	}

	/**
	 * constructs a new database system by cloning a source database system object.
	 * The instance identifiers and the conforming types are the same, i.e. the
	 * source and the new objects are equal. The conforming type must belong to the
	 * new configuration type.
	 * <p>
	 * This method is used in scenarios in which a new database system type is
	 * introduced or an existing database system type is removed. Before modifying
	 * the current configuration, a new configuration type is created and the
	 * current configuration "moves" to the new configuration type by applying this
	 * constructor on all the database systems. Afterwards, the current
	 * configuration can use the new database system types or can no more use the
	 * old database system types. In consequence, before being deleted, a database
	 * system type is deprecated "during" at least one configuration type.
	 * 
	 * @param origin               the source object to clone.
	 * @param newConfigurationType the new configuration type (the database system
	 *                             type of the source object must belong to the new
	 *                             configuration type).
	 * @throws UnknownTypeException the database system type of this database system
	 *                              is unknown in the new configuration type.
	 */
	public DatabaseSystem(final DatabaseSystem origin, final ConfigurationType newConfigurationType)
			throws UnknownTypeException {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(newConfigurationType, "newConfigurationType cannot be null");
		this.identifier = origin.identifier;
		var newDatabaseSystemType = newConfigurationType
				.getDatabaseSystemType(origin.databaseSystemType.getTypeIdentifier());
		if (newDatabaseSystemType == null) {
			throw new UnknownTypeException("database system type " + origin.databaseSystemType.getTypeIdentifier()
					+ " does not exist in configuration type " + newConfigurationType.getIdentifier());
		}
		this.databaseSystemType = newDatabaseSystemType;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return identifier != null && databaseSystemType != null;
	}

	/**
	 * obtains the instance identifier.
	 * 
	 * @return the instance identifier.
	 */
	public InstanceIdentifier getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL name of the instance identifier.
	 * 
	 * @return the PDDL name.
	 */
	public String getPDDLIdentifier() {
		return identifier.getPDDLIdentifier();
	}

	/**
	 * obtains the conforming type.
	 * 
	 * @return the type.
	 */
	public DatabaseSystemType getDatabaseSystemType() {
		return databaseSystemType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof DatabaseSystem)) {
			return false;
		}
		DatabaseSystem other = (DatabaseSystem) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "Database [identifier=" + identifier + ", databasesystemType=" + databaseSystemType.getTypeIdentifier()
				+ "]";
	}
}
