/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

/**
 * This interface is the declaration of an operation type, which is used in
 * operation signatures. This interface contains the static method to parse an
 * operation type, i.e. either a primitive type or a fully qualified class. 
 * 
 * @author Denis Conan
 */
public interface OperationType {
	/**
	 * parses a string to get an operation type.
	 * 
	 * @param stringToParse the string to parse.
	 * @return the operation type.
	 */
	static OperationType parseOperationType(final String stringToParse) {
		return PrimitiveType.parsePrimitiveType(stringToParse).orElse(new FullyQualifiedClass(stringToParse));
	}
}
