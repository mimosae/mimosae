/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.type.TypeIdentifier;

/**
 * This abstract class defines the concept of publish-subscribe contract type.
 * It is identifier by a type identifier, which is composed of an identifier and
 * a version. This is the root class of all the publish-subscribe contract
 * types, being the producer or consumer parts, and being channel-based,
 * topic-based, or content-based filtering.
 * 
 * @author Denis Conan
 */
public abstract class PubSubContractType {
	/**
	 * the type identifier.
	 */
	private final TypeIdentifier identifier;

	/**
	 * constructs a new publish-subscribe contract type.
	 * 
	 * @param identifier the identifier of the type.
	 * @param version    the version of the type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	protected PubSubContractType(final String identifier, final Semver version) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = TypeIdentifier.typeIdentifier(identifier, version);
	}

	/**
	 * constructs a new publish-subscribe contract type by cloning. The two
	 * contracts have the same identifier, and thus are equal.
	 * 
	 * @param origin the source contract type.
	 */
	protected PubSubContractType(final PubSubContractType origin) {
		Objects.requireNonNull(origin, "origin cannot be null");
		this.identifier = origin.identifier;
	}

	/**
	 * obtains the type identifier.
	 * 
	 * @return the type identifier.
	 */
	public TypeIdentifier getTypeIdentifier() {
		return identifier;
	}

	@Override
	public final int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public final boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof PubSubContractType)) {
			return false;
		}
		PubSubContractType other = (PubSubContractType) obj;
		return Objects.equals(identifier, other.identifier);
	}
}
