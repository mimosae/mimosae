/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.jgrapht.Graphs;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;

import com.vdurmont.semver4j.Semver;

import mimosae.common.pddl.HappenBeforeRelationships;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.exception.AlreadyExistsModelElementException;
import mimosae.modelatruntime.exception.ConnectedDatabaseSystemInConfigurationException;
import mimosae.modelatruntime.exception.ConnectedMicroserviceInConfigurationException;
import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.exception.IsNotDeployableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.UnknownInstanceException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.exception.UnmatchingTypesException;
import mimosae.modelatruntime.instance.Configuration;
import mimosae.modelatruntime.instance.DatabaseSystem;
import mimosae.modelatruntime.instance.EmptyInitConfiguration;
import mimosae.modelatruntime.instance.InstanceEdge;
import mimosae.modelatruntime.instance.InstanceIdentifier;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.instance.connnector.ClientServerConnector;
import mimosae.modelatruntime.instance.connnector.Connector;
import mimosae.modelatruntime.instance.connnector.DatabaseConnector;
import mimosae.modelatruntime.instance.connnector.PubSubConnector;
import mimosae.modelatruntime.instance.contract.ClientContract;
import mimosae.modelatruntime.instance.contract.ConsumerContract;
import mimosae.modelatruntime.instance.contract.DatabaseContract;
import mimosae.modelatruntime.instance.contract.ProducerContract;
import mimosae.modelatruntime.instance.contract.ServerContract;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.connectortype.ConnectorType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.ConsumerContractType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;
import mimosae.modelatruntime.type.contracttype.ProducerContractType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;

/**
 * This class is the Façade of the instance model at runtime. It implements the
 * Singleton design pattern. This façade requires the instantiation of the
 * Façade of the type model at runtime, e.g. class
 * {@link mimosae.modelatruntime.MimosaeRuntimeModelType}.
 * 
 * @author Denis Conan
 */
public final class MimosaeRuntimeModelInstance {
	/**
	 * the singleton instance of this façade.
	 */
	private static MimosaeRuntimeModelInstance instance;
	/**
	 * the singleton instance of the type façade.
	 */
	private MimosaeRuntimeModelType facadeModelType;
	/**
	 * the graph of configurations.
	 */
	private final DirectedAcyclicGraph<Configuration, InstanceEdge> configurations;
	/**
	 * the last committed configuration in the graph of configurations. This is the
	 * head of the current branch of the graph.
	 */
	private Configuration lastCommittedConfiguration;
	/**
	 * the next to commit configuration of the graph of configurations.
	 */
	private Configuration currentConfiguration;
	/**
	 * the configuration type of the current configuration under construction.
	 */
	private ConfigurationType currentConfigurationType;
	/**
	 * the log of actions that are performed on the current configuration. This is
	 * for debugging purposes.
	 */
	private List<ActionLog> currentActionLog;

	/**
	 * constructs the façade. The last committed configurations is the empty "init"
	 * configuration. The graph of configurations contains only the last committed
	 * configuration. The current configuration is created by cloning the last
	 * committed configuration. The current configuration type, i.e. the
	 * configuration type of the current configuration, is the empty "init"
	 * configuration type, i.e. the configuration type of the current configuration.
	 * Finally, the action log of the current configuration is empty.
	 */
	private MimosaeRuntimeModelInstance() {
		facadeModelType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		configurations = new DirectedAcyclicGraph<>(InstanceEdge.class);
		configurations.setEdgeSupplier(InstanceEdge::new);
		lastCommittedConfiguration = EmptyInitConfiguration.getEmptyInitConfiguration();
		configurations.addVertex(lastCommittedConfiguration);
		currentConfiguration = new Configuration(lastCommittedConfiguration);
		currentConfigurationType = currentConfiguration.getConfigurationType();
		currentActionLog = new ArrayList<>();
		assert invariant();
	}

	/**
	 * obtains the singleton object of the façade.
	 * 
	 * @return the façade.
	 */
	public static MimosaeRuntimeModelInstance getMimosaeRuntimeModelInstance() {
		if (instance == null) {
			instance = new MimosaeRuntimeModelInstance();
		}
		return instance;
	}

	/**
	 * computes the invariant of the façade singleton object.
	 * 
	 * @return true when the invariant is true.
	 */
	public boolean invariant() {
		return facadeModelType != null && configurations != null && lastCommittedConfiguration != null
				&& configurations.containsVertex(lastCommittedConfiguration) // && currentActionLog != null
				&& (lastCommittedConfiguration.getIdentifier().equals(
						EmptyInitConfiguration.IDENTIFIER_EMPTY_INIT_CONFIGURATION) || currentConfiguration != null);
	}

	/**
	 * clears all the objects of the façade, i.e. all the configurations of the
	 * graph, etc. This method is a priori used in tests only.
	 */
	public void clearAllConfigurations() {
		facadeModelType.clearAllConfigurationTypes();
		configurations.removeAllVertices(configurations.vertexSet());
		lastCommittedConfiguration = EmptyInitConfiguration.getEmptyInitConfiguration();
		configurations.addVertex(lastCommittedConfiguration);
		currentConfiguration = new Configuration(lastCommittedConfiguration);
		currentConfigurationType = currentConfiguration.getConfigurationType();
		currentActionLog = new ArrayList<>();
		assert invariant();
	}

	/**
	 * resets the current configuration: When the last committed configuration is
	 * the empty "init" configuration, the current configuration is instantiated
	 * from the last committed configuration type; Otherwise, the current
	 * configuration is cloned from the last committed configuration.
	 */
	public void resetCurrentConfiguration() {
		if (this.lastCommittedConfiguration.getIdentifier()
				.equals(EmptyInitConfiguration.IDENTIFIER_EMPTY_INIT_CONFIGURATION)) {
			ConfigurationType configurationType = null;
			try {
				configurationType = facadeModelType
						.getConfigurationType(facadeModelType.getIdentifierOfLastCommittedConfigurationType());
			} catch (UnknownTypeException e) {
				throw new IllegalStateException("empty init configuration type null");
			}
			currentConfiguration = new Configuration(configurationType);
		} else {
			currentConfiguration = new Configuration(this.lastCommittedConfiguration);
		}
		this.currentActionLog = new ArrayList<>();
		assert invariant();
	}

	/**
	 * obtains a stringified graph of the configurations using the DOT format.
	 * <p>
	 * Here follows an example:
	 * 
	 * <pre>
	strict digraph configurations {
	1 [ SHA-1="empty_init_configuration" ];
	2 [ SHA-1="d05411bb5250d89f76ab4b472b7ae4958785ef6c" ];
	3 [ SHA-1="0adde7f14bf6b2396c9eb1a91c915458d940fc9b" ];
	1 -&gt; 2 [ Action log="[
		[name=createMicroservice, arguments=[microservice1, microserviceType1, 0.0.1]], 
	    ...
		[name=linkClientServer, arguments=[microservice1, microserviceType1, 0.0.1, clientContractType1, 0.0.1, microservice2, microserviceType2, 0.0.1, serverContractType2, 0.0.1]], 
	    ...
	2 -&gt; 3 [ Action log="[
		[name=unlinkClientServer, arguments=[microservice1, microserviceType1, 0.0.1, clientContractType1, 0.0.1, microservice2, microserviceType2, 0.0.1, serverContractType2, 0.0.1]], 
		[name=removeMicroservice, arguments=[microservice2, microserviceType2, 0.0.1]], 
	    ...
	}
	 * </pre>
	 * 
	 * @return the stringified graph of configurations.
	 */
	public String toStringGraphOfConfigurationInDotFormat() {
		DOTExporter<Configuration, InstanceEdge> exporter = new DOTExporter<>();
		exporter.setGraphIdProvider(() -> "configurations");
		exporter.setVertexAttributeProvider(v -> {
			Map<String, Attribute> map = new LinkedHashMap<>();
			map.put("SHA-1", DefaultAttribute.createAttribute(v.getIdentifier()));
			return map;
		});
		exporter.setEdgeAttributeProvider(e -> {
			Map<String, Attribute> map = new LinkedHashMap<>();
			map.put("Action log", DefaultAttribute.createAttribute(e.getLogs().toString()));
			return map;
		});
		Writer writer = new StringWriter();
		exporter.exportGraph(configurations, writer);
		return writer.toString() + "The last-committed configuration is \""
				+ this.lastCommittedConfiguration.getIdentifier() + "\"" + "\n" + "The current configuration type is \""
				+ this.currentConfigurationType.getIdentifier() + "\"";
	}

	/**
	 * obtains the identifier of the last committed configuration.
	 * 
	 * @return the identifier as a SHA-1 digest.
	 */
	public String getIdentifierOfLastCommittedConfiguration() {
		return lastCommittedConfiguration.getIdentifier();
	}

	/**
	 * obtains the identifier of the configuration type of the current
	 * configuration.
	 * 
	 * @return the identifier as a SHA-1 digest.
	 */
	public String getIdentifierOfConfigurationTypeOfCurrentConfiguration() {
		return currentConfiguration.getConfigurationType().getIdentifier();
	}

	/**
	 * obtains the configuration (the node of the graph of configurations) of the
	 * given identifier.
	 * 
	 * @param identifier the identifier of the configuration to search for.
	 * @return the configuration found.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 * @throws UnknownInstanceException     no configuration corresponds to the
	 *                                      given identifier.
	 */
	private Configuration getConfiguration(final String identifier)
			throws MalformedIdentifierException, UnknownInstanceException {
		facadeModelType.checkIdentifier(identifier, "identifier cannot be null");
		Optional<Configuration> found = configurations.vertexSet().stream()
				.filter(v -> v.getIdentifier().equals(identifier)).findFirst();
		if (found.isEmpty()) {
			throw new UnknownInstanceException("configuration " + identifier + " is an unknown");
		}
		return found.get();
	}

	/**
	 * obtains the string of the configuration of the given identifier. It calls to
	 * the method {@code toString} on all the content of the configuration, i.e. all
	 * the microservices and all the connectors, etc.
	 * <p>
	 * Here follows an example:
	 * 
	 * <pre>
	Configuration [identifier=d05411bb5250d89f76ab4b472b7ae4958785ef6c, configurationType=0ae5cae86de9b426a74a0e4aa26ff7351428e287,
	microservices=
	[Microservice [identifier=microservice1_microserviceType1_0.0.1, microserviceType=microserviceType1v.0.0.1,
		 clientContracts=[clientContractType2v.0.0.1-microservice1_microserviceType1_0.0.1-microserviceType1v.0.0.1, ...],
		 serverContracts=[serverContractType1v.0.0.1-microservice1_microserviceType1_0.0.1-microserviceType1v.0.0.1],
		 producerContracts=[amqpProducerContractType2v.0.0.1-microservice1_microserviceType1_0.0.1-microserviceType1v.0.0.1, ...],
		 consumerContracts=[]],
	 ...
	],
	connectors=
	[PubSubServerConnector [identifier=amqpPubSubConnector1, pubSubConnectorType=amqpPubSubConnectorType1v.0.0.1, producerContracts=[...], consumerContracts=[...]],
	 ...
	 ClientServerConnector [identifier=..., clientServerConnectorType=ClientServerConnectorType [clientContractType=..., serverContractType=...], clientContract=..., serverContract=...]
	]
	]
	 * </pre>
	 * 
	 * @param identifier the identifier of the configuration to stringify.
	 * @return the stringified configuration.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 * @throws UnknownInstanceException     no configuration corresponds to the
	 *                                      given identifier.
	 */
	public String toStringConfiguration(final String identifier)
			throws MalformedIdentifierException, UnknownInstanceException {
		facadeModelType.checkIdentifier(identifier, "identifier cannot be null");
		Optional<Configuration> found = configurations.vertexSet().stream()
				.filter(v -> v.getIdentifier().equals(identifier)).findFirst();
		if (found.isEmpty()) {
			throw new UnknownInstanceException("configuration " + identifier + "is an unknown configuration");
		}
		return found.get().toString();
	}

	/**
	 * obtains the string of the current configuration. It calls to the method
	 * {@code toString} on all the content of the configuration, i.e. all the
	 * microservices and all the connectors, etc.
	 * <p>
	 * Cf. Javadoc of {@link #toStringConfiguration(String)} for an example.
	 * 
	 * @return the stringified configuration.
	 */
	public String toStringCurrentConfiguration() {
		return currentConfiguration.toString();
	}

	/**
	 * obtains the string of the last committed configuration. It calls to the
	 * method {@code toString} on all the content of the configuration, i.e. all the
	 * microservices and all the connectors, etc.
	 * <p>
	 * Cf. Javadoc of {@link #toStringConfiguration(String)} for an example.
	 * 
	 * @return the stringified configuration.
	 */
	public String toStringLastCommittedConfiguration() {
		return this.lastCommittedConfiguration.toString();
	}

	/**
	 * changes the configuration type of the current configuration. A new
	 * configuration is cloned from the current configuration and with a new
	 * configuration type. The microservice types of current microservice instances
	 * must be present in the new configuration type. Similarly, the connector types
	 * of current connector instances must be present in the new configuration type.
	 * Microservice instances are cloned: same identifier, etc. The connectors
	 * connect microservices of the same types and with the same identifiers, but
	 * that belong to the new configuration. It is possible to clone a configuration
	 * that is committed; the new configuration is then not committed.
	 * <p>
	 * This method is used in scenarios in which a new type is introduced or an
	 * existing type is removed. Before modifying the current configuration, a new
	 * configuration type is created and the current configuration "moves" to the
	 * new configuration type by applying this constructor. Afterwards, the current
	 * configuration can use the new types or can no more use the old types. In
	 * consequence, before being deleted, a type is deprecated "during" at least one
	 * configuration type.
	 * 
	 * @param identifier the identifier of the new configuration type.
	 * @throws UnknownTypeException the new configuration type is unknown.
	 */
	public void changeConfigurationTypeOfCurrentConfiguration(final String identifier) throws UnknownTypeException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		var newConfigurationType = facadeModelType.getConfigurationType(identifier);
		if (newConfigurationType == null) {
			throw new UnknownTypeException("configuration type " + identifier + " does not exist");
		}
		currentConfigurationType = newConfigurationType;
		currentConfiguration = new Configuration(currentConfiguration, newConfigurationType);
		currentActionLog.add(ActionLog.createActionLog("changeConfigurationTypeOfCurrentConfiguration", identifier));
	}

	/**
	 * sets the configuration type of the current configuration to a new
	 * configuration type. As a consequence, the current configuration is "reset"
	 * with no microservice instance and no connector instance.
	 * 
	 * @param identifier the identifier of the new configuration type.
	 * @throws UnknownTypeException the new configuration type is unknown.
	 */
	public void setConfigurationTypeOfCurrentConfiguration(final String identifier) throws UnknownTypeException {
		currentConfigurationType = facadeModelType.getConfigurationType(identifier);
		this.currentConfiguration = new Configuration(currentConfiguration, currentConfigurationType);
	}

	/**
	 * obtains the identifier of the configuration type of the current
	 * configuration, i.e. the current configuration type.
	 * 
	 * @return the identifier of the configuration type.
	 */
	public String getCurrentConfigurationType() {
		return currentConfigurationType.getIdentifier();
	}

	/**
	 * creates the microservice with a given instance identifier and of the given
	 * type (identifier and version). The microservice instance is uniquely
	 * identified by the identifier of the instance plus the identifier and the
	 * version of its type: e.g. "user_service_replica_1" plus "user_service" in
	 * version "0.0.1".
	 * <p>
	 * The new microservice is created with its contracts (client, server, producer,
	 * consumer) and is added to the current configuration. The unique identifier of
	 * the microservice instance is calculated from the instance identifier plus the
	 * type identifier and the type version: e.g. "authentication_service_replica_1"
	 * of type "authentication_service_type" in version "0.0.1".
	 * 
	 * @param instanceIdentifier the identifier of the microservice instance to
	 *                           create.
	 * @param typeIdentifier     the identifier of the microservice type.
	 * @param typeVersion        the version of the microservice type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws AlreadyExistsModelElementException      microservice instance already
	 *                                                 present.
	 * @throws UnknownTypeException                    unknown type element in the
	 *                                                 configuration type of the
	 *                                                 current configuration.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createMicroservice(final String instanceIdentifier, final String typeIdentifier,
			final String typeVersion) throws AlreadyExistsModelElementException, UnknownTypeException,
			MalformedIdentifierException, IsCommittedAndCannotBeModifiedException {
		facadeModelType.checkIdentifier(instanceIdentifier, "instanceIdentifier cannot be null");
		var ti = facadeModelType.computeTypeIdentifier(typeIdentifier, typeVersion);
		var mi = checkInstanceIdentifier(instanceIdentifier, typeIdentifier, typeVersion);
		if (currentConfiguration.getMicroservice(mi) != null) {
			throw new AlreadyExistsModelElementException(
					"microservice " + mi + " already exists in configuration " + currentConfiguration.getIdentifier());
		}
		var microserviceType = this.facadeModelType.getMicroserviceTypeIntoConfigurationType(
				currentConfigurationType.getIdentifier(), typeIdentifier, typeVersion);
		if (microserviceType == null) {
			throw new UnknownTypeException("microservice type " + ti.getIdentifier() + " does not exist");
		}
		var microservice = new Microservice(instanceIdentifier, microserviceType);
		currentConfiguration.addMicroservice(microservice);
		currentActionLog
				.add(ActionLog.createActionLog("createMicroservice", instanceIdentifier, typeIdentifier, typeVersion));
		assert invariant();
	}

	/**
	 * states whether the given microservice instance is connected (to another
	 * microservice or to a connector) in the current configuration. The
	 * microservice instance is identified by the identifier of the instance, and
	 * the identifier and the version of the type.
	 * 
	 * @param instanceIdentifier the identifier of the microservice instance.
	 * @param typeIdentifier     the identifier of the microservice type.
	 * @param typeVersion        the version of the microservice type.
	 * @return true if the microservice is connected in the current configuration.
	 * @throws MalformedIdentifierException one of the identifiers is malformed.
	 * @throws UnknownInstanceException     the instance is unknown in current
	 *                                      configuration.
	 */
	public boolean isMicroserviceConnectedToOthers(final String instanceIdentifier, final String typeIdentifier,
			final String typeVersion) throws UnknownInstanceException, MalformedIdentifierException {
		facadeModelType.checkIdentifier(instanceIdentifier, "instanceIdentifier cannot be null");
		var mi = checkInstanceIdentifier(instanceIdentifier, typeIdentifier, typeVersion);
		var microservice = currentConfiguration.getMicroservice(mi);
		if (microservice == null) {
			throw new UnknownInstanceException(
					"microservice " + mi + "does not exist in configuration " + currentConfiguration.getIdentifier());
		}
		return currentConfiguration.isMicroserviceConnectedToOthers(microservice);
	}

	/**
	 * removes the microservice instance from the current configuration. The
	 * microservice instance is identified by the identifier of the instance, and
	 * the identifier and the version of the type. A microservice that is connected
	 * to other microservices cannot be removed, i.e. the "unlink" operations must
	 * be done before the removal.
	 * 
	 * @param instanceIdentifier the identifier of the instance to remove.
	 * @param typeIdentifier     the identifier of the type.
	 * @param typeVersion        the version of the type.
	 * @throws MalformedIdentifierException                  one of the identifiers
	 *                                                       is malformed.
	 * @throws UnknownInstanceException                      the instance is unknown
	 *                                                       in current
	 *                                                       configuration.
	 * @throws ConnectedMicroserviceInConfigurationException one of the contract is
	 *                                                       connected.
	 * @throws IsCommittedAndCannotBeModifiedException       the configuration is
	 *                                                       committed and cannot be
	 *                                                       modified.
	 */
	public void removeMicroservice(final String instanceIdentifier, final String typeIdentifier,
			final String typeVersion) throws UnknownInstanceException, ConnectedMicroserviceInConfigurationException,
			MalformedIdentifierException, IsCommittedAndCannotBeModifiedException {
		facadeModelType.checkIdentifier(instanceIdentifier, "instanceIdentifier cannot be null");
		var mi = checkInstanceIdentifier(instanceIdentifier, typeIdentifier, typeVersion);
		var microservice = currentConfiguration.getMicroservice(mi);
		if (microservice == null) {
			throw new UnknownInstanceException(
					"microservice " + mi + "does not exist in configuration " + currentConfiguration.getIdentifier());
		}
		currentConfiguration.removeMicroservice(microservice);
		currentActionLog
				.add(ActionLog.createActionLog("removeMicroservice", instanceIdentifier, typeIdentifier, typeVersion));
		assert invariant();
	}

	/**
	 * replaces a microservice instance of the current configuration by a new
	 * microservice instance that conforms to the new given microservice type. The
	 * new microservice type is a patch version: the new microservice instance can
	 * replace current microservice instance identically.
	 * <p>
	 * The "current" microservice instance is identifier by the identifier of the
	 * instance plus the identifier and the version of the type. The new
	 * microservice type is identified by the identifier of "current" microservice
	 * type and a new version.
	 * 
	 * @param instanceIdentifier the identifier of the microservice instance to
	 *                           replace.
	 * @param typeIdentifier     the identifier of the microservice type to replace.
	 * @param currentTypeVersion the version of the microservice type to replace.
	 * @param newTypeVersion     the version of the new microservice type.
	 * @throws UnknownTypeException                    one of the types is unknown
	 *                                                 in the configuration type of
	 *                                                 current configuration.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws UnknownInstanceException                one of the instance is
	 *                                                 unknown in current
	 *                                                 configuration.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void replaceMicroserviceWithPatchVersion(final String instanceIdentifier, final String typeIdentifier,
			final String currentTypeVersion, final String newTypeVersion) throws UnknownTypeException,
			MalformedIdentifierException, UnknownInstanceException, IsCommittedAndCannotBeModifiedException {
		var currentMI = checkInstanceIdentifier(instanceIdentifier, typeIdentifier, currentTypeVersion);
		var currentMicroservice = currentConfiguration.getMicroservice(currentMI);
		if (currentMicroservice == null) {
			throw new UnknownInstanceException("current microservice " + instanceIdentifier
					+ "does not exist in configuration " + currentConfiguration.getIdentifier());
		}
		facadeModelType.checkIdentifier(instanceIdentifier, "instanceIdentifier cannot be null");
		var currentTI = this.facadeModelType.computeTypeIdentifier(typeIdentifier, currentTypeVersion);
		var currentMicroserviceType = this.facadeModelType.getMicroserviceTypeIntoConfigurationType(
				currentConfigurationType.getIdentifier(), typeIdentifier, currentTypeVersion);
		if (currentMicroserviceType == null) {
			throw new UnknownTypeException(
					"current microservice type " + currentTI.getIdentifier() + " does not exist");
		}
		var newTI = this.facadeModelType.computeTypeIdentifier(typeIdentifier, newTypeVersion);
		var newMicroserviceType = this.facadeModelType.getMicroserviceTypeIntoConfigurationType(
				currentConfigurationType.getIdentifier(), typeIdentifier, newTypeVersion);
		if (newMicroserviceType == null) {
			throw new UnknownTypeException("new microservice type " + newTI.getIdentifier() + " does not exist");
		}
		currentConfiguration.replaceMicroserviceWithPatchVersion(currentMicroservice, newMicroserviceType);
		currentActionLog.add(ActionLog.createActionLog("replaceMicroserviceWithPatchVersion", instanceIdentifier,
				typeIdentifier, currentTypeVersion, newTypeVersion));
		assert invariant();
	}

	/**
	 * creates the database system with a given instance identifier and of the given
	 * type (identifier and version). The database system instance is uniquely
	 * identified by the identifier of the instance plus the identifier and the
	 * version of its type: e.g. "database_users_replica_1" plus
	 * "database_user_type" in version "0.0.1".
	 * 
	 * @param instanceIdentifier the identifier of the database system instance to
	 *                           create.
	 * @param typeIdentifier     the identifier of the database system type.
	 * @param typeVersion        the version of the database system type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws AlreadyExistsModelElementException      database system instance
	 *                                                 already present.
	 * @throws UnknownTypeException                    unknown type element in the
	 *                                                 configuration type of the
	 *                                                 current configuration.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createDatabaseSystem(final String instanceIdentifier, final String typeIdentifier,
			final String typeVersion) throws AlreadyExistsModelElementException, UnknownTypeException,
			MalformedIdentifierException, IsCommittedAndCannotBeModifiedException {
		facadeModelType.checkIdentifier(instanceIdentifier, "instanceIdentifier cannot be null");
		var ti = facadeModelType.computeTypeIdentifier(typeIdentifier, typeVersion);
		var dbi = checkInstanceIdentifier(instanceIdentifier, typeIdentifier, typeVersion);
		if (currentConfiguration.getDatabaseSystem(dbi) != null) {
			throw new AlreadyExistsModelElementException("database system " + dbi + " already exists in configuration "
					+ currentConfiguration.getIdentifier());
		}
		var databaseSystemType = this.facadeModelType.getDatabaseSystemTypeIntoConfigurationType(
				currentConfigurationType.getIdentifier(), typeIdentifier, typeVersion);
		if (databaseSystemType == null) {
			throw new UnknownTypeException("database system type " + ti.getIdentifier() + " does not exist");
		}
		var databaseSystem = new DatabaseSystem(instanceIdentifier, databaseSystemType);
		currentConfiguration.addDatabaseSystem(databaseSystem);
		currentActionLog.add(
				ActionLog.createActionLog("createDatabaseSystem", instanceIdentifier, typeIdentifier, typeVersion));
		assert invariant();
	}

	/**
	 * removes the database system instance from the current configuration. The
	 * database system instance is identified by the identifier of the instance, and
	 * the identifier and the version of the type. A database system that is
	 * connected to / used by microservices cannot be removed, i.e. the "disconnect"
	 * operations must be done before the removal.
	 * 
	 * @param instanceIdentifier the identifier of the instance to remove.
	 * @param typeIdentifier     the identifier of the type.
	 * @param typeVersion        the version of the type.
	 * @throws MalformedIdentifierException                    one of the
	 *                                                         identifiers is
	 *                                                         malformed.
	 * @throws UnknownInstanceException                        the instance is
	 *                                                         unknown in current
	 *                                                         configuration.
	 * @throws ConnectedDatabaseSystemInConfigurationException one of the
	 *                                                         microservice is
	 *                                                         connected to / uses
	 *                                                         this database system.
	 * @throws IsCommittedAndCannotBeModifiedException         the configuration is
	 *                                                         committed and cannot
	 *                                                         be modified.
	 */
	public void removeDatabaseSystem(final String instanceIdentifier, final String typeIdentifier,
			final String typeVersion) throws UnknownInstanceException, MalformedIdentifierException,
			ConnectedDatabaseSystemInConfigurationException, IsCommittedAndCannotBeModifiedException {
		facadeModelType.checkIdentifier(instanceIdentifier, "instanceIdentifier cannot be null");
		var dbsi = checkInstanceIdentifier(instanceIdentifier, typeIdentifier, typeVersion);
		var databaseSystem = currentConfiguration.getDatabaseSystem(dbsi);
		if (databaseSystem == null) {
			throw new UnknownInstanceException("database system " + dbsi + "does not exist in configuration "
					+ currentConfiguration.getIdentifier());
		}
		currentConfiguration.removeDatabaseSystem(databaseSystem);
		currentActionLog
				.add(ActionLog.createActionLog("removeMicroservice", instanceIdentifier, typeIdentifier, typeVersion));
		assert invariant();
	}

	/**
	 * links a client contract of a microservice instance to the server contract of
	 * another microservice instance. The "client" microservice is identified by the
	 * identifier of the instance, and the identifier and the version of the type.
	 * The "client" interface is identified by the identifier and the version of the
	 * client contract type, i.e. there is only one client contract instance per
	 * client contract type. Similarly, the "server" microservice is identified by
	 * the identifier of the instance, and the identifier and the version of the
	 * type; the "server" interface is identified by the identifier and the version
	 * of the server contract type.
	 * <p>
	 * The connector cannot be created and the link operation is refused if the
	 * client contract is already connected (to a server contract via a
	 * client-server connector).
	 * 
	 * @param microserviceIdentifierClient     the identifier of the client
	 *                                         microservice instance.
	 * @param microserviceTypeIdentifierClient the identifier of the microservice
	 *                                         type.
	 * @param microserviceTypeVersionClient    the version of the microservice type.
	 * @param clientContractTypeIdentifier     the identifier of the client contract
	 *                                         type.
	 * @param clientContractTypeVersion        the version of the client contract
	 *                                         type.
	 * @param microserviceIdentifierServer     the identifier of the server
	 *                                         microservice instance.
	 * @param microserviceTypeIdentifierServer the identifier of the microservice
	 *                                         type.
	 * @param microserviceTypeVersionServer    the version of the microservice type.
	 * @param serverContractTypeIdentifier     the identifier of the server contract
	 *                                         type.
	 * @param serverContractTypeVersion        the version of the server contract
	 *                                         type.
	 * @throws UnknownInstanceException                one of the instances is
	 *                                                 unknown in current
	 *                                                 configuration.
	 * @throws UnknownTypeException                    one of the types is unknown
	 *                                                 in the configuration type of
	 *                                                 current configuration.
	 * @throws UnmatchingTypesException                client interface is not
	 *                                                 compatible with server
	 *                                                 interface, e.g. a required
	 *                                                 operation of the client
	 *                                                 contract is not provided in
	 *                                                 the server contract.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws AlreadyConnectedException               the client contract is
	 *                                                 already connected.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	// CHECKSTYLE:OFF: checkstyle:parameternumbercheck
	public void linkClientServer(final String microserviceIdentifierClient,
			final String microserviceTypeIdentifierClient, final String microserviceTypeVersionClient,
			final String clientContractTypeIdentifier, final String clientContractTypeVersion,
			final String microserviceIdentifierServer, final String microserviceTypeIdentifierServer,
			final String microserviceTypeVersionServer, final String serverContractTypeIdentifier,
			final String serverContractTypeVersion)
			throws UnknownInstanceException, UnknownTypeException, UnmatchingTypesException,
			MalformedIdentifierException, AlreadyConnectedException, IsCommittedAndCannotBeModifiedException {
		// CHECKSTYLE:ON: checkstyle:parameternumbercheck
		var miC = checkInstanceIdentifier(microserviceIdentifierClient, microserviceTypeIdentifierClient,
				microserviceTypeVersionClient);
		var tiC = facadeModelType.computeTypeIdentifier(clientContractTypeIdentifier, clientContractTypeVersion);
		var miS = checkInstanceIdentifier(microserviceIdentifierServer, microserviceTypeIdentifierServer,
				microserviceTypeVersionServer);
		var tiS = facadeModelType.computeTypeIdentifier(serverContractTypeIdentifier, serverContractTypeVersion);
		var microserviceClient = currentConfiguration.getMicroservice(miC);
		if (microserviceClient == null) {
			throw new UnknownInstanceException(miC + " is an unknown microservice in current configuration");
		}
		var microserviceServer = currentConfiguration.getMicroservice(miS);
		if (microserviceServer == null) {
			throw new UnknownInstanceException(miS + " is an unknown microservice in current configuration");
		}
		Optional<ClientContractType> clientContractType = microserviceClient.getMicroserviceType()
				.getClientContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(tiC)).findFirst();
		if (clientContractType.isEmpty()) {
			throw new UnknownTypeException("client contract type corresponding to " + tiC + " does not exist");
		}
		var connectorType = clientContractType.get().getClientServerConnectorType();
		if (connectorType == null) {
			throw new UnknownTypeException("there is no client server connector type for client contract type "
					+ clientContractType.get().getTypeIdentifier());
		}
		Optional<ServerContractType> serverContractType = microserviceServer.getMicroserviceType()
				.getServerContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(tiS)).findFirst();
		if (serverContractType.isEmpty()) {
			throw new UnknownTypeException("servert contract type corresponding to " + tiS + " does not exist");
		}
		if (!connectorType.getServerContractType().equals(serverContractType.get())) {
			throw new UnmatchingTypesException("unmatching server contract type: " + serverContractTypeIdentifier + "->"
					+ tiC + "->" + connectorType.getServerContractType().getTypeIdentifier().getIdentifier()
					+ " is different from " + tiS);
		}
		var clientContract = microserviceClient
				.getClientContract(ClientContract.computeIdentifier(microserviceClient, clientContractType.get()));
		if (clientContract == null) {
			throw new UnknownInstanceException("no client contract "
					+ ClientContract.computeIdentifier(microserviceClient, clientContractType.get())
					+ " in microservice " + miC);
		}
		var serverContract = microserviceServer
				.getServerContract(ServerContract.computeIdentifier(microserviceServer, serverContractType.get()));
		if (serverContract == null) {
			throw new UnknownInstanceException("no server contract "
					+ ServerContract.computeIdentifier(microserviceServer, serverContractType.get())
					+ " in microservice " + microserviceIdentifierServer);
		}
		String connectorId = ClientServerConnector.computeIdentifier(clientContract, serverContract);
		if (currentConfiguration.getConnector(connectorId) != null) {
			throw new AlreadyConnectedException("connector " + connectorId + "already exists in configuration "
					+ currentConfiguration.getIdentifier() + ", i.e. client contract already connected");

		}
		Connector connector = new ClientServerConnector(connectorType, clientContract, serverContract);
		currentConfiguration.addConnector(connector);
		currentActionLog.add(ActionLog.createActionLog("linkClientServer", microserviceIdentifierClient,
				microserviceTypeIdentifierClient, microserviceTypeVersionClient, clientContractTypeIdentifier,
				clientContractTypeVersion, microserviceIdentifierServer, microserviceTypeIdentifierServer,
				microserviceTypeVersionServer, serverContractTypeIdentifier, serverContractTypeVersion));
		assert invariant();
	}

	/**
	 * unlinks a client contract of a microservice instance to the server contract
	 * of another microservice instance. The "client" microservice is identified by
	 * the identifier of the instance, and the identifier and the version of the
	 * type. The "client" interface is identified by the identifier and the version
	 * of the client contract type, i.e. there is only one client contract instance
	 * per client contract type. Similarly, the "server" microservice is identified
	 * by the identifier of the instance, and the identifier and the version of the
	 * type; the "server" interface is identified by the identifier and the version
	 * of the server contract type.
	 * 
	 * @param microserviceIdentifierClient     the identifier of the client
	 *                                         microservice instance.
	 * @param microserviceTypeIdentifierClient the identifier of the microservice
	 *                                         type.
	 * @param microserviceTypeVersionClient    the version of the microservice type.
	 * @param clientContractTypeIdentifier     the identifier of the client contract
	 *                                         type.
	 * @param clientContractTypeVersion        the version of the client contract
	 *                                         type.
	 * @param microserviceIdentifierServer     the identifier of the server
	 *                                         microservice instance.
	 * @param microserviceTypeIdentifierServer the identifier of the microservice
	 *                                         type.
	 * @param microserviceTypeVersionServer    the version of the microservice type.
	 * @param serverContractTypeIdentifier     the identifier of the server contract
	 *                                         type.
	 * @param serverContractTypeVersion        the version of the server contract
	 *                                         type.
	 * @throws UnknownInstanceException                the instance is unknown in
	 *                                                 current configuration.
	 * @throws UnknownTypeException                    one of the type element is
	 *                                                 unknown in the configuration
	 *                                                 type of the current
	 *                                                 configuration.
	 * @throws UnmatchingTypesException                unmatching of types when
	 *                                                 considering types of
	 *                                                 contracts in the connector
	 *                                                 type and types of
	 *                                                 client/server.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	// CHECKSTYLE:OFF: checkstyle:parameternumbercheck
	public void unlinkClientServer(final String microserviceIdentifierClient,
			final String microserviceTypeIdentifierClient, final String microserviceTypeVersionClient,
			final String clientContractTypeIdentifier, final String clientContractTypeVersion,
			final String microserviceIdentifierServer, final String microserviceTypeIdentifierServer,
			final String microserviceTypeVersionServer, final String serverContractTypeIdentifier,
			final String serverContractTypeVersion) throws UnknownInstanceException, UnknownTypeException,
			UnmatchingTypesException, MalformedIdentifierException, IsCommittedAndCannotBeModifiedException {
		// CHECKSTYLE:ON: checkstyle:parameternumbercheck
		var miC = checkInstanceIdentifier(microserviceIdentifierClient, microserviceTypeIdentifierClient,
				microserviceTypeVersionClient);
		var tiC = facadeModelType.computeTypeIdentifier(clientContractTypeIdentifier, clientContractTypeVersion);
		var miS = checkInstanceIdentifier(microserviceIdentifierServer, microserviceTypeIdentifierServer,
				microserviceTypeVersionServer);
		var tiS = facadeModelType.computeTypeIdentifier(serverContractTypeIdentifier, serverContractTypeVersion);
		var microserviceClient = currentConfiguration.getMicroservice(miC);
		if (microserviceClient == null) {
			throw new UnknownInstanceException(miC + " is an unknown microservice in current configuration");
		}
		var microserviceServer = currentConfiguration.getMicroservice(miS);
		if (microserviceServer == null) {
			throw new UnknownInstanceException(miS + " is an unknown microservice in current configuration");
		}
		Optional<ClientContractType> clientContractType = microserviceClient.getMicroserviceType()
				.getClientContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(tiC)).findFirst();
		if (clientContractType.isEmpty()) {
			throw new UnknownTypeException("client contract type corresponding to " + tiC + " does not exist");
		}
		var connectorType = clientContractType.get().getClientServerConnectorType();
		if (connectorType == null) {
			throw new UnknownTypeException("there is no client server connector type for client contract type "
					+ clientContractType.get().getTypeIdentifier());
		}
		Optional<ServerContractType> serverContractType = microserviceServer.getMicroserviceType()
				.getServerContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(tiS)).findFirst();
		if (serverContractType.isEmpty()) {
			throw new UnknownTypeException("servert contract type corresponding to " + tiS + " does not exist");
		}
		if (!connectorType.getServerContractType().equals(serverContractType.get())) {
			throw new UnmatchingTypesException("unmatching server contract type: " + serverContractTypeIdentifier + "->"
					+ tiC + "->" + connectorType.getServerContractType().getTypeIdentifier().getIdentifier()
					+ " is different from " + tiS);
		}
		var clientContract = microserviceClient
				.getClientContract(ClientContract.computeIdentifier(microserviceClient, clientContractType.get()));
		if (clientContract == null) {
			throw new UnknownInstanceException("no client contract "
					+ ClientContract.computeIdentifier(microserviceClient, clientContractType.get())
					+ " in microservice " + microserviceIdentifierClient);
		}
		clientContract.unsetConnector();
		var serverContract = microserviceServer
				.getServerContract(ServerContract.computeIdentifier(microserviceServer, serverContractType.get()));
		if (serverContract == null) {
			throw new UnknownInstanceException("no server contract "
					+ ServerContract.computeIdentifier(microserviceServer, serverContractType.get())
					+ " in microservice " + microserviceIdentifierServer);
		}
		String connectorId = ClientServerConnector.computeIdentifier(clientContract, serverContract);
		var connector = currentConfiguration.getConnector(connectorId);
		if (connector == null) {
			throw new UnknownInstanceException("connector " + connectorId + " does not exist in configuration "
					+ currentConfiguration.getIdentifier());

		}
		currentConfiguration.removeConnector(connector);
		currentActionLog.add(ActionLog.createActionLog("unlinkClientServer", microserviceIdentifierClient,
				microserviceTypeIdentifierClient, microserviceTypeVersionClient, clientContractTypeIdentifier,
				clientContractTypeVersion, microserviceIdentifierServer, microserviceTypeIdentifierServer,
				microserviceTypeVersionServer, serverContractTypeIdentifier, serverContractTypeVersion));
		assert invariant();
	}

	/**
	 * connects a database contract of a microservice instance to a database system
	 * instance. The microservice is identified by the identifier of the instance,
	 * and the identifier and the version of the type. The database contract
	 * interface is identified by the identifier and the version of the client
	 * contract type, i.e. there is only one database contract instance per database
	 * contract type. Similarly, the database system is identified by the identifier
	 * of the instance, and the identifier and the version of the type
	 * <p>
	 * The connector cannot be created and the connect operation is refused if the
	 * database contract is already connected (to a database system via a database
	 * connector).
	 * 
	 * @param microserviceIdentifier         the identifier of the microservice
	 *                                       instance.
	 * @param microserviceTypeIdentifier     the identifier of the microservice
	 *                                       type.
	 * @param microserviceTypeVersion        the version of the microservice type.
	 * @param databaseContractTypeIdentifier the identifier of the database contract
	 *                                       type.
	 * @param databaseContractTypeVersion    the version of the database contract
	 *                                       type.
	 * @param databaseSystemIdentifier       the identifier of the database system
	 *                                       instance.
	 * @param databaseSystemTypeIdentifier   the identifier of the database system
	 *                                       type.
	 * @param databaseSystemTypeVersion      the version of the database system
	 *                                       type.
	 * @throws UnknownInstanceException                one of the instances is
	 *                                                 unknown in current
	 *                                                 configuration.
	 * @throws UnknownTypeException                    one of the types is unknown
	 *                                                 in the configuration type of
	 *                                                 current configuration.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws AlreadyConnectedException               the database contract is
	 *                                                 already connected.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	// CHECKSTYLE:OFF: checkstyle:parameternumbercheck
	public void connectMicroserviceToDatabaseSystem(final String microserviceIdentifier,
			final String microserviceTypeIdentifier, final String microserviceTypeVersion,
			final String databaseContractTypeIdentifier, final String databaseContractTypeVersion,
			final String databaseSystemIdentifier, final String databaseSystemTypeIdentifier,
			final String databaseSystemTypeVersion) throws UnknownInstanceException, UnknownTypeException,
			MalformedIdentifierException, AlreadyConnectedException, IsCommittedAndCannotBeModifiedException {
		// CHECKSTYLE:ON: checkstyle:parameternumbercheck
		var microserviceInstanceIdentifier = checkInstanceIdentifier(microserviceIdentifier, microserviceTypeIdentifier,
				microserviceTypeVersion);
		var dbcontractTypeIdentifier = facadeModelType.computeTypeIdentifier(databaseContractTypeIdentifier,
				databaseContractTypeVersion);
		var databaseSystemInstanceIdentifier = checkInstanceIdentifier(databaseSystemIdentifier,
				databaseSystemTypeIdentifier, databaseSystemTypeVersion);
		var microservice = currentConfiguration.getMicroservice(microserviceInstanceIdentifier);
		if (microservice == null) {
			throw new UnknownInstanceException(
					microserviceInstanceIdentifier + " is an unknown microservice in current configuration");
		}
		var databaseSystem = currentConfiguration.getDatabaseSystem(databaseSystemInstanceIdentifier);
		if (databaseSystem == null) {
			throw new UnknownInstanceException(
					databaseSystemInstanceIdentifier + " is an unknown database system in current configuration");
		}
		Optional<DatabaseContractType> databaseContractType = microservice.getMicroserviceType()
				.getDatabaseContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(dbcontractTypeIdentifier))
				.findFirst();
		if (databaseContractType.isEmpty()) {
			throw new UnknownTypeException(
					"database contract type corresponding to " + dbcontractTypeIdentifier + " does not exist");
		}
		var connectorType = databaseContractType.get().getDatabaseConnectorType();
		if (connectorType == null) {
			throw new UnknownTypeException("there is no database connector type for database contract type "
					+ databaseContractType.get().getTypeIdentifier());
		}
		var databaseContract = microservice
				.getDatabaseContract(DatabaseContract.computeIdentifier(microservice, databaseContractType.get()));
		if (databaseContract == null) {
			throw new UnknownInstanceException("no database contract "
					+ DatabaseContract.computeIdentifier(microservice, databaseContractType.get()) + " in microservice "
					+ microserviceInstanceIdentifier);
		}
		String connectorId = DatabaseConnector.computeIdentifier(databaseContract, databaseSystem);
		if (currentConfiguration.getConnector(connectorId) != null) {
			throw new AlreadyConnectedException("connector " + connectorId + "already exists in configuration "
					+ currentConfiguration.getIdentifier() + ", i.e. database contract already connected");

		}
		Connector connector = new DatabaseConnector(connectorType, databaseContract, databaseSystem);
		currentConfiguration.addConnector(connector);
		currentActionLog.add(ActionLog.createActionLog("connectMicroserviceToDatabaseSystem", microserviceIdentifier,
				microserviceTypeIdentifier, microserviceTypeVersion, databaseContractTypeIdentifier,
				databaseContractTypeVersion, databaseSystemIdentifier, databaseSystemTypeIdentifier,
				databaseSystemTypeVersion));
		assert invariant();
	}

	/**
	 * disconnects a database contract of a microservice instance to a database
	 * system instance. The microservice is identified by the identifier of the
	 * instance, and the identifier and the version of the type. The database
	 * contract is identified by the identifier and the version of the database
	 * contract type, i.e. there is only one database contract instance per database
	 * contract type. Similarly, the database system instance is identified by the
	 * identifier of the instance, and the identifier and the version of the type
	 * 
	 * @param microserviceIdentifier         the identifier of the microservice
	 *                                       instance.
	 * @param microserviceTypeIdentifier     the identifier of the microservice
	 *                                       type.
	 * @param microserviceTypeVersion        the version of the microservice type.
	 * @param databaseContractTypeIdentifier the identifier of the database contract
	 *                                       type.
	 * @param databaseContractTypeVersion    the version of the database contract
	 *                                       type.
	 * @param databaseSystemIdentifier       the identifier of the database system
	 *                                       instance.
	 * @param databaseSystemTypeIdentifier   the identifier of the database system
	 *                                       type.
	 * @param databaseSystemTypeVersion      the version of the database system
	 *                                       type.
	 * @throws UnknownInstanceException                the instance is unknown in
	 *                                                 current configuration.
	 * @throws UnknownTypeException                    one of the type element is
	 *                                                 unknown in the configuration
	 *                                                 type of the current
	 *                                                 configuration.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	// CHECKSTYLE:OFF: checkstyle:parameternumbercheck
	public void disconnectMicroserviceToDatabaseSystem(final String microserviceIdentifier,
			final String microserviceTypeIdentifier, final String microserviceTypeVersion,
			final String databaseContractTypeIdentifier, final String databaseContractTypeVersion,
			final String databaseSystemIdentifier, final String databaseSystemTypeIdentifier,
			final String databaseSystemTypeVersion) throws UnknownInstanceException, UnknownTypeException,
			MalformedIdentifierException, IsCommittedAndCannotBeModifiedException {
		// CHECKSTYLE:ON: checkstyle:parameternumbercheck
		var microserviceInstanceIdentifier = checkInstanceIdentifier(microserviceIdentifier, microserviceTypeIdentifier,
				microserviceTypeVersion);
		var dbContractIdentifier = facadeModelType.computeTypeIdentifier(databaseContractTypeIdentifier,
				databaseContractTypeVersion);
		var databaseSystemInstanceIdentifier = checkInstanceIdentifier(databaseSystemIdentifier,
				databaseSystemTypeIdentifier, databaseSystemTypeVersion);
		var microservice = currentConfiguration.getMicroservice(microserviceInstanceIdentifier);
		if (microservice == null) {
			throw new UnknownInstanceException(
					microserviceInstanceIdentifier + " is an unknown microservice in current configuration");
		}
		var databaseSystem = currentConfiguration.getDatabaseSystem(databaseSystemInstanceIdentifier);
		if (databaseSystem == null) {
			throw new UnknownInstanceException(
					databaseSystemInstanceIdentifier + " is an unknown database system in current configuration");
		}
		Optional<DatabaseContractType> databaseContractType = microservice.getMicroserviceType()
				.getDatabaseContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(dbContractIdentifier))
				.findFirst();
		if (databaseContractType.isEmpty()) {
			throw new UnknownTypeException(
					"database contract type corresponding to " + dbContractIdentifier + " does not exist");
		}
		var connectorType = databaseContractType.get().getDatabaseConnectorType();
		if (connectorType == null) {
			throw new UnknownTypeException("there is no database connector type for database contract type "
					+ databaseContractType.get().getTypeIdentifier());
		}
		var databaseContract = microservice
				.getDatabaseContract(DatabaseContract.computeIdentifier(microservice, databaseContractType.get()));
		if (databaseContract == null) {
			throw new UnknownInstanceException("no database contract "
					+ DatabaseContract.computeIdentifier(microservice, databaseContractType.get()) + " in microservice "
					+ microserviceIdentifier);
		}
		databaseContract.disconnect();
		String connectorId = DatabaseConnector.computeIdentifier(databaseContract, databaseSystem);
		var connector = currentConfiguration.getConnector(connectorId);
		if (connector == null) {
			throw new UnknownInstanceException("connector " + connectorId + " does not exist in configuration "
					+ currentConfiguration.getIdentifier());

		}
		currentConfiguration.removeConnector(connector);
		currentActionLog.add(ActionLog.createActionLog("disconnectMicroserviceToDatabaseSystem", microserviceIdentifier,
				microserviceTypeIdentifier, microserviceTypeVersion, databaseContractTypeIdentifier,
				databaseContractTypeVersion, databaseSystemIdentifier, databaseSystemTypeIdentifier,
				databaseSystemTypeVersion));
		assert invariant();
	}

	/**
	 * creates a publish-subscribe connector. The connector is identified by the
	 * identifier of the instance: e.g. "rabbitmq_for_log" of type "rabbitmq" and
	 * "3.9.1".
	 * 
	 * @param instanceIdentifier the identifier of the connector instance.
	 * @param typeIdentifier     the identifier of the type.
	 * @param typeVersion        the version of the type.
	 * @throws MalformedIdentifierException            one of the identifier is
	 *                                                 malformed.
	 * @throws AlreadyExistsModelElementException      the connector instance
	 *                                                 already exists.
	 * @throws UnknownTypeException                    unknown type element in the
	 *                                                 configuration type of the
	 *                                                 current configuration.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void createPubSubConnector(final String instanceIdentifier, final String typeIdentifier,
			final String typeVersion) throws AlreadyExistsModelElementException, UnknownTypeException,
			MalformedIdentifierException, IsCommittedAndCannotBeModifiedException {
		facadeModelType.checkIdentifier(instanceIdentifier, "instanceIdentifier cannot be null");
		var ti = this.facadeModelType.computeTypeIdentifier(typeIdentifier, typeVersion);
		if (currentConfiguration.getConnector(instanceIdentifier) != null) {
			throw new AlreadyExistsModelElementException(
					"connector " + instanceIdentifier + "already exists in turrent configuration");
		}
		ConnectorType connectorType = this.facadeModelType.getPubSubConnectorType(typeIdentifier, typeVersion);
		if (connectorType == null) {
			throw new UnknownTypeException("connector type " + ti.getIdentifier() + " does not exist");
		}
		var connector = new PubSubConnector(instanceIdentifier, connectorType);
		currentConfiguration.addConnector(connector);
		currentActionLog.add(
				ActionLog.createActionLog("createPubSubConnector", instanceIdentifier, typeIdentifier, typeVersion));
		assert invariant();
	}

	/**
	 * removes a publish-subscribe connector. The connector is identified by the
	 * identifier of the instance: e.g. "rabbitmq_for_log" of type "rabbitmq" and
	 * "3.9.1".
	 * 
	 * @param instanceIdentifier the identifier of the connector instance.
	 * @throws MalformedIdentifierException            the instance identifier is
	 *                                                 malformed.
	 * @throws UnknownInstanceException                the connector instance is
	 *                                                 unknown in current
	 *                                                 configuration.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void removePubSubConnectorAndUnlink(final String instanceIdentifier)
			throws MalformedIdentifierException, UnknownInstanceException, IsCommittedAndCannotBeModifiedException {
		facadeModelType.checkIdentifier(instanceIdentifier, "instanceIdentifier cannot be null");
		var connector = currentConfiguration.getConnector(instanceIdentifier);
		if (connector == null) {
			throw new UnknownInstanceException(
					"connector " + instanceIdentifier + " does not exist in current configuration");
		}
		currentConfiguration.removeConnector(connector);
		currentActionLog.add(ActionLog.createActionLog("removePubSubConnectorAndUnlink", instanceIdentifier));
		assert invariant();
	}

	/**
	 * links the producer contract of a microservice instance with a connector
	 * instance. The microservice instance is identified by the identifier of the
	 * instance plus the identifier and the version of the type. The producer
	 * contract is identified by the identifier and the version of its type, i.e.
	 * there is only one producer contract instance per producer contract type.
	 * 
	 * @param microserviceIdentifier         the identifier of the microservice
	 *                                       instance.
	 * @param microserviceTypeIdentifier     the identifier of the microservice
	 *                                       type.
	 * @param microserviceTypeVersion        the version of the microservice type.
	 * @param producerContractTypeIdentifier the identifier of the producer contract
	 *                                       type.
	 * @param producerContractTypeVersion    the version of the producer contract
	 *                                       type.
	 * @param connectorIdentifier            the identifier of the connector
	 *                                       instance.
	 * @throws MalformedIdentifierException one the identifiers is malformed.
	 * @throws UnknownInstanceException     one of the instance is unknown in
	 *                                      current configuration.
	 * @throws UnknownTypeException         unknown type in the configuration type
	 *                                      of current configuration.
	 * @throws UnmatchingTypesException     unmatching of types when considering the
	 *                                      type of producer contract in connector
	 *                                      type and the type of producer contract.
	 */
	public void linkProducerToConnector(final String microserviceIdentifier, final String microserviceTypeIdentifier,
			final String microserviceTypeVersion, final String producerContractTypeIdentifier,
			final String producerContractTypeVersion, final String connectorIdentifier) throws UnknownInstanceException,
			UnknownTypeException, UnmatchingTypesException, MalformedIdentifierException {
		var mi = checkInstanceIdentifier(microserviceIdentifier, microserviceTypeIdentifier, microserviceTypeVersion);
		var ti = facadeModelType.computeTypeIdentifier(producerContractTypeIdentifier, producerContractTypeVersion);
		var microservice = currentConfiguration.getMicroservice(mi);
		if (microservice == null) {
			throw new UnknownInstanceException(mi + " is an unknown microservice in current configuration");
		}
		Optional<ProducerContractType> producerContractType = microservice.getMicroserviceType()
				.getProducerContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(ti)).findFirst();
		if (producerContractType.isEmpty()) {
			throw new UnknownTypeException("producer contract type corresponding to identifier " + ti
					+ " does not exist in microservice type " + microservice.getMicroserviceType().getTypeIdentifier());
		}
		var connector = currentConfiguration.getConnector(connectorIdentifier);
		if (!(connector instanceof PubSubConnector)) {
			throw new UnmatchingTypesException("unmatching producer contract type "
					+ connector.getConnectorType().getClass().getCanonicalName() + " for a pub sub connector");
		}
		var pubSubConnector = (PubSubConnector) connector;
		String producerIdentifier = ProducerContract.computeIdentifier(microservice, producerContractType.get());
		var producerContract = microservice.getProducerContract(producerIdentifier);
		if (producerContract == null) {
			throw new UnknownInstanceException(producerIdentifier + " is an unknown producer contract in microservice "
					+ microservice.getIdentifier());
		}
		if (!producerContract.getProducerContractType().equals(producerContractType.get())) {
			throw new UnmatchingTypesException("unmatching producer contract type "
					+ producerContract.getProducerContractType().getTypeIdentifier() + " for producer contract "
					+ producerContract.getIdentifier() + ": was waiting producer contract type "
					+ producerContractType.get().getTypeIdentifier());
		}
		pubSubConnector.addProducerContract(producerContract);
		currentActionLog.add(ActionLog.createActionLog("linkProducerToConnector", microserviceIdentifier,
				microserviceTypeIdentifier, microserviceTypeVersion, producerContractTypeIdentifier,
				producerContractTypeVersion, connectorIdentifier));
		assert invariant();
	}

	/**
	 * links the consumer contract of a microservice instance with a connector
	 * instance. The microservice instance is identified by the identifier of the
	 * instance plus the identifier and the version of the type. The consumer
	 * contract is identified by the identifier and the version of its type, i.e.
	 * there is only one consumer contract instance per consumer contract type.
	 * 
	 * @param microserviceIdentifier         the identifier of the microservice
	 *                                       instance.
	 * @param microserviceTypeIdentifier     the identifier of the microservice
	 *                                       type.
	 * @param microserviceTypeVersion        the version of the microservice type.
	 * @param consumerContractTypeIdentifier the identifier of the consumer contract
	 *                                       type.
	 * @param consumerContractTypeVersion    the version of the consumer contract
	 *                                       type.
	 * @param connectorIdentifier            the identifier of the connector
	 *                                       instance.
	 * @throws MalformedIdentifierException one of the identifier is malformed.
	 * @throws UnknownInstanceException     one of the instance is unknown in
	 *                                      current configuration.
	 * @throws UnknownTypeException         one of the type is unknown in the
	 *                                      configuration type of the current
	 *                                      configuration.
	 * @throws UnmatchingTypesException     unmatching of types when considering the
	 *                                      type of consumer contract in connector
	 *                                      type and the type of consumer contract.
	 */
	public void linkConsumerToConnector(final String microserviceIdentifier, final String microserviceTypeIdentifier,
			final String microserviceTypeVersion, final String consumerContractTypeIdentifier,
			final String consumerContractTypeVersion, final String connectorIdentifier) throws UnknownInstanceException,
			UnknownTypeException, UnmatchingTypesException, MalformedIdentifierException {
		var mi = checkInstanceIdentifier(microserviceIdentifier, microserviceTypeIdentifier, microserviceTypeVersion);
		var ti = facadeModelType.computeTypeIdentifier(consumerContractTypeIdentifier, consumerContractTypeVersion);
		var microservice = currentConfiguration.getMicroservice(mi);
		if (microservice == null) {
			throw new UnknownInstanceException(mi + " is an unknown microservice in current configuration");
		}
		Optional<ConsumerContractType> consumerContractType = microservice.getMicroserviceType()
				.getConsumerContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(ti)).findFirst();
		if (consumerContractType.isEmpty()) {
			throw new UnknownTypeException("consumer contract type corresponding to identifier " + ti
					+ " does not exist in microservice type " + microservice.getMicroserviceType().getTypeIdentifier());
		}
		var connector = currentConfiguration.getConnector(connectorIdentifier);
		if (!(connector instanceof PubSubConnector)) {
			throw new UnmatchingTypesException("unmatching producer contract type "
					+ connector.getConnectorType().getClass().getCanonicalName() + " for a pub sub connector");
		}
		var pubSubConnector = (PubSubConnector) connector;
		String consumerIdentifier = ConsumerContract.computeIdentifier(microservice, consumerContractType.get());
		var consumerContract = microservice.getConsumerContract(consumerIdentifier);
		if (consumerContract == null) {
			throw new UnknownInstanceException(consumerIdentifier + " is an unknown consumer contract in microservice "
					+ microservice.getIdentifier());
		}
		if (!consumerContract.getConsumerContractType().equals(consumerContractType.get())) {
			throw new UnmatchingTypesException("unmatching consumer contract type "
					+ consumerContract.getConsumerContractType().getTypeIdentifier() + " for consumer contract "
					+ consumerContract.getIdentifier() + ": was waiting consumer contract type "
					+ consumerContractType.get().getTypeIdentifier());
		}
		pubSubConnector.addConsumerContract(consumerContract);
		currentActionLog.add(ActionLog.createActionLog("linkConsumerToConnector", microserviceIdentifier,
				microserviceTypeIdentifier, microserviceTypeVersion, consumerContractTypeIdentifier,
				consumerContractTypeVersion, connectorIdentifier));
		assert invariant();
	}

	/**
	 * unlinks the producer contract of a microservice instance with a connector
	 * instance. The microservice instance is identified by the identifier of the
	 * instance plus the identifier and the version of the type. The producer
	 * contract is identified by the identifier and the version of its type, i.e.
	 * there is only one producer contract instance per producer contract type.
	 * 
	 * @param microserviceIdentifier         the identifier of the microservice
	 *                                       instance.
	 * @param microserviceTypeIdentifier     the identifier of the microservice
	 *                                       type.
	 * @param microserviceTypeVersion        the version of the microservice type.
	 * @param producerContractTypeIdentifier the identifier of the producer contract
	 *                                       type.
	 * @param producerContractTypeVersion    the version of the producer contract
	 *                                       type.
	 * @param connectorIdentifier            the identifier of the connector
	 *                                       instance.
	 * @throws UnknownInstanceException     one of the instance is unknown in
	 *                                      current configuration.
	 * @throws UnknownTypeException         one of the type is unknown in the
	 *                                      configuration type of the current
	 *                                      configuration.
	 * @throws UnmatchingTypesException     unmatching of types when considering the
	 *                                      type of producer contract in connector
	 *                                      type and the type of producer contract.
	 * @throws MalformedIdentifierException one of the identifier is malformed.
	 */
	public void unlinkProducerFromConnector(final String microserviceIdentifier,
			final String microserviceTypeIdentifier, final String microserviceTypeVersion,
			final String producerContractTypeIdentifier, final String producerContractTypeVersion,
			final String connectorIdentifier) throws UnknownInstanceException, UnknownTypeException,
			UnmatchingTypesException, MalformedIdentifierException {
		var mi = checkInstanceIdentifier(microserviceIdentifier, microserviceTypeIdentifier, microserviceTypeVersion);
		var ti = facadeModelType.computeTypeIdentifier(producerContractTypeIdentifier, producerContractTypeVersion);
		var microservice = currentConfiguration.getMicroservice(mi);
		if (microservice == null) {
			throw new UnknownInstanceException(mi + " is an unknown microservice in current configuration");
		}
		Optional<ProducerContractType> producerContractType = microservice.getMicroserviceType()
				.getProducerContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(ti)).findFirst();
		if (producerContractType.isEmpty()) {
			throw new UnknownTypeException("producer contract type corresponding to identifier " + ti
					+ " does not exist in microservice type " + microservice.getMicroserviceType().getTypeIdentifier());
		}
		var connector = currentConfiguration.getConnector(connectorIdentifier);
		if (!(connector instanceof PubSubConnector)) {
			throw new UnmatchingTypesException("unmatching producer contract type "
					+ connector.getConnectorType().getClass().getCanonicalName() + " for a pub sub connector");
		}
		var pubSubConnector = (PubSubConnector) connector;
		String producerIdentifier = ProducerContract.computeIdentifier(microservice, producerContractType.get());
		var producerContract = microservice.getProducerContract(producerIdentifier);
		if (producerContract == null) {
			throw new UnknownInstanceException(producerIdentifier + " is an unknown producer contract in microservice "
					+ microservice.getIdentifier());
		}
		if (!producerContract.getProducerContractType().equals(producerContractType.get())) {
			throw new UnmatchingTypesException("unmatching producer contract type "
					+ producerContract.getProducerContractType().getTypeIdentifier() + " for producer contract "
					+ producerContract.getIdentifier() + ": was waiting producer contract type "
					+ producerContractType.get().getTypeIdentifier());
		}
		pubSubConnector.removeProducerContract(producerContract);
		currentActionLog.add(ActionLog.createActionLog("unlinkProducerFromConnector", microserviceIdentifier,
				microserviceTypeIdentifier, microserviceTypeVersion, producerContractTypeIdentifier,
				producerContractTypeVersion, connectorIdentifier));
		assert invariant();
	}

	/**
	 * unlinks the consumer contract of a microservice instance with a connector
	 * instance. The microservice instance is identified by the identifier of the
	 * instance plus the identifier and the version of the type. The consumer
	 * contract is identified by the identifier and the version of its type, i.e.
	 * there is only one consumer contract instance per consumer contract type.
	 * 
	 * @param microserviceIdentifier         the identifier of the microservice
	 *                                       instance.
	 * @param microserviceTypeIdentifier     the identifier of the microservice
	 *                                       type.
	 * @param microserviceTypeVersion        the version of the microservice type.
	 * @param consumerContractTypeIdentifier the identifier of the consumer contract
	 *                                       type.
	 * @param consumerContractTypeVersion    the version of the consumer contract
	 *                                       type.
	 * @param connectorIdentifier            the identifier of the connector
	 *                                       instance.
	 * @throws MalformedIdentifierException one of the identifier is malformed.
	 * @throws UnknownInstanceException     one of the instance is unknown in
	 *                                      current configuration.
	 * @throws UnknownTypeException         one of the type is unknown in the
	 *                                      configuration type of the current
	 *                                      configuration.
	 * @throws UnmatchingTypesException     unmatching of types when considering the
	 *                                      type of consumer contract in connector
	 *                                      type and the type of consumer contract.
	 */
	public void unlinkConsumerFromConnector(final String microserviceIdentifier,
			final String microserviceTypeIdentifier, final String microserviceTypeVersion,
			final String consumerContractTypeIdentifier, final String consumerContractTypeVersion,
			final String connectorIdentifier) throws UnknownInstanceException, UnknownTypeException,
			UnmatchingTypesException, MalformedIdentifierException {
		var mi = checkInstanceIdentifier(microserviceIdentifier, microserviceTypeIdentifier, microserviceTypeVersion);
		var ti = facadeModelType.computeTypeIdentifier(consumerContractTypeIdentifier, consumerContractTypeVersion);
		var microservice = currentConfiguration.getMicroservice(mi);
		if (microservice == null) {
			throw new UnknownInstanceException(mi + " is an unknown microservice in current configuration");
		}
		Optional<ConsumerContractType> consumerContractType = microservice.getMicroserviceType()
				.getConsumerContractTypes().stream().filter(c -> c.getTypeIdentifier().equals(ti)).findFirst();
		if (consumerContractType.isEmpty()) {
			throw new UnknownTypeException("consumer contract type corresponding to identifier " + ti
					+ " does not exist in microservice type " + microservice.getMicroserviceType().getTypeIdentifier());
		}
		var connector = currentConfiguration.getConnector(connectorIdentifier);
		if (!(connector instanceof PubSubConnector)) {
			throw new UnmatchingTypesException("unmatching producer contract type "
					+ connector.getConnectorType().getClass().getCanonicalName() + " for a pub sub connector");
		}
		var pubSubConnector = (PubSubConnector) connector;
		String consumerIdentifier = ConsumerContract.computeIdentifier(microservice, consumerContractType.get());
		var consumerContract = microservice.getConsumerContract(consumerIdentifier);
		if (consumerContract == null) {
			throw new UnknownInstanceException(consumerIdentifier + " is an unknown consumer contract in microservice "
					+ microservice.getIdentifier());
		}
		if (!consumerContract.getConsumerContractType().equals(consumerContractType.get())) {
			throw new UnmatchingTypesException("unmatching consumer contract type "
					+ consumerContract.getConsumerContractType().getTypeIdentifier() + " for consumer contract "
					+ consumerContract.getIdentifier() + ": was waiting consumer contract type "
					+ consumerContractType.get().getTypeIdentifier());
		}
		pubSubConnector.removeConsumerContract(consumerContract);
		currentActionLog.add(ActionLog.createActionLog("unlinkConsumerFromConnector", microserviceIdentifier,
				microserviceTypeIdentifier, microserviceTypeVersion, consumerContractTypeIdentifier,
				consumerContractTypeVersion, connectorIdentifier));
		assert invariant();
	}

	/**
	 * commits the current configuration. Before the commit, the current
	 * configuration is cloned. After the commit, the graph of configurations is
	 * complemented with the newly committed configuration and with an edge from the
	 * last committed configuration to the newly committed configuration. The
	 * current configuration becomes the last committed configuration and the
	 * current configuration is now the configuration that was cloned by copy before
	 * the commit.
	 * 
	 * @throws IsCommittedAndCannotBeModifiedException      the configuration is
	 *                                                      already committed.
	 * @throws IsNotDeployableAndCannotBeCommittedException the configuration is not
	 *                                                      deployable and cannot be
	 *                                                      committed.
	 */
	public void commitConfiguration()
			throws IsCommittedAndCannotBeModifiedException, IsNotDeployableAndCannotBeCommittedException {
		var future = new Configuration(this.currentConfiguration);
		currentConfiguration.commit();
		var edge = Graphs.addEdgeWithVertices(configurations, lastCommittedConfiguration, currentConfiguration);
		edge.setLogs(currentActionLog);
		lastCommittedConfiguration = currentConfiguration;
		currentConfiguration = future;
		currentActionLog = new ArrayList<>();
		assert invariant();
	}

	/**
	 * computes the PDDL problem file for the AI planner to transit from a source
	 * configuration to a target.
	 * 
	 * @param fromConfigurationIdentifier the identifier of the source
	 *                                    configuration.
	 * @param toConfigurationIdentifier   the identifier of the target
	 *                                    configuration.
	 * @param happenBeforeConstraints     the collection of "happen before" that are
	 *                                    going to be collected.
	 * @return the collections of predicates for the AI planner: the first
	 *         collection is the "objects" part, the second collection is the "init"
	 *         part, and the third collection is the "goal" part.
	 * @throws MalformedIdentifierException one of the identifier is malformed.
	 * @throws UnknownInstanceException     one of the configurations is unknown.
	 */
	public List<List<String>> computeAIPlanningToReconfigure(final String fromConfigurationIdentifier,
			final String toConfigurationIdentifier, final HappenBeforeRelationships happenBeforeConstraints)
			throws MalformedIdentifierException, UnknownInstanceException {
		var fromConfiguration = getConfiguration(fromConfigurationIdentifier);
		var toConfiguration = getConfiguration(toConfigurationIdentifier);
		return fromConfiguration.computeAIPlanningToReconfigure(toConfiguration, happenBeforeConstraints);
	}

	/**
	 * computes a microservice or a database system instance identifier from the
	 * instance identifier, the type identifier, and the type version. It checks
	 * that the given identifiers and the given version are correct: the identifiers
	 * cannot be null and cannot be blank, the version cannot be null and cannot be
	 * blank, the version should follow <a href="https://semver.org/">semantic
	 * versioning rules</a>. The methods returns a type identifier object.
	 * <p>
	 * The visibility of this method is {@code package} because it is used in class
	 * {@link MimosaeRuntimeModelInstance} and must not be accessible by other: it
	 * returns a microservice instance identifier object that is internal to the
	 * type model of the model at runtime.
	 * 
	 * @param instanceIdentifier the identifier of the microservice or database
	 *                           system instance.
	 * @param typeIdentifier     the identifier of the microservice or database
	 *                           system type.
	 * @param typeVersion        the version of the microservice or database system
	 *                           type.
	 * @return the microservice or database system instance identifier object.
	 * @throws MalformedIdentifierException one of the identifiers is malformed.
	 */
	private InstanceIdentifier checkInstanceIdentifier(final String instanceIdentifier, final String typeIdentifier,
			final String typeVersion) throws MalformedIdentifierException {
		Objects.requireNonNull(instanceIdentifier, "instanceIdentifier cannot be null");
		if (instanceIdentifier.isBlank()) {
			throw new MalformedIdentifierException("instanceIdentifier cannot be blank");
		}
		Objects.requireNonNull(instanceIdentifier, "instanceIdentifier cannot be null");
		if (typeIdentifier.isBlank()) {
			throw new MalformedIdentifierException("typeIdentifier cannot be blank");
		}
		Objects.requireNonNull(typeVersion, "typeVersion cannot be null");
		if (typeVersion.isBlank()) {
			throw new MalformedIdentifierException("typeVersion cannot be blank");
		}
		return new InstanceIdentifier(instanceIdentifier, typeIdentifier, new Semver(typeVersion));
	}
}
