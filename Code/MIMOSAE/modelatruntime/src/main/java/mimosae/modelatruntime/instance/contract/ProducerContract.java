/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.instance.connnector.PubSubConnector;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.contracttype.ProducerContractType;

/**
 * This class defines the concept of producer contract instance. It has an
 * identifier; it conforms to a producer contract type; it can be attached to a
 * collection of publish-subscribe connector; and it belongs to a microservice
 * instance.
 * <p>
 * The producer contract is identified by an instance identifier that is the
 * concatenation of the producer contract type identifier with the instance
 * identifier of the microservice to which it belongs.
 * <p>
 * A producer contract can be attached to several publish-subscribe connector to
 * express the fact that an event can be published into several contexts: e.g.
 * several AMQP exchanges, or several virtual hosts, or even several brokers; or
 * several MQTT brokers. Typically, a producer contract specifies a routing key
 * (AMQP technology) or a topic (MQTT technology).
 * 
 * @author Denis Conan
 */
public final class ProducerContract {
	/**
	 * the instance identifier. It is the concatenation of the producer contract
	 * type identifier with the instance identifier of the microservice.
	 */
	private final String identifier;
	/**
	 * the producer contract type to which it conforms.
	 */
	private ProducerContractType producerContractType;
	/**
	 * the publish-subscribe connectors.
	 */
	private final Map<String, PubSubConnector> pubSubConnectors;
	/**
	 * the microservice to which it belongs.
	 */
	private final Microservice microservice;

	/**
	 * constructs a new producer contract. This new producer contract is created
	 * when creating the corresponding microservice, i.e. the new object is added to
	 * the collection of producer contracts in the calling constructor of the
	 * microservice afterwards (Cf. {@link Microservice#Microservice(Microservice)}.
	 * 
	 * @param producerContractType the producer contract type it conforms to.
	 * @param microservice         the microservice to which it belongs.
	 */
	public ProducerContract(final ProducerContractType producerContractType, final Microservice microservice) {
		Objects.requireNonNull(producerContractType, "producerContractType cannot be null");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, producerContractType);
		this.producerContractType = producerContractType;
		this.pubSubConnectors = new HashMap<>();
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * constructs a new producer contract by cloning a source producer contract. The
	 * new producer contract belongs to a microservice that is different from the
	 * microservice of the source producer contract. The identifiers of the source
	 * and the new objects are different, and thus the source and new objects are
	 * not equal. The producer contract types are the same. Finally, the new
	 * producer contract does not have a publish-subscribe connector attached.
	 * <p>
	 * This constructor is called when creating the corresponding microservice by
	 * cloning, i.e. the new object is added to the collection of producer contracts
	 * in the calling constructor of the new microservice afterwards. (Cf.
	 * {@link Microservice#Microservice(String, mimosae.modelatruntime.type.MicroserviceType)}).
	 * 
	 * @param origin       the source producer contract.
	 * @param microservice the microservice to which the new producer contract
	 *                     belongs.
	 */
	public ProducerContract(final ProducerContract origin, final Microservice microservice) {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, origin.producerContractType);
		this.producerContractType = origin.producerContractType;
		this.microservice = microservice;
		this.pubSubConnectors = new HashMap<>();
		assert invariant();
	}

	/**
	 * constructs a new producer contract by cloning a source producer contract. The
	 * new producer contract belongs to a microservice that is different from the
	 * microservice of the source producer contract. The configuration type is also
	 * new. The identifiers of the source and the new objects are different, and
	 * thus the source and new objects are not equal. The producer contract type
	 * must belong to the new configuration type. The producer contract type of the
	 * new producer contract and the producer contract type of the source producer
	 * contract are different objects, but they are equal.
	 * <p>
	 * The producer contract type of the source producer contract must belong to the
	 * new configuration type. Similarly, the microservice type of the microservice
	 * must belong to the new configuration type.
	 * <p>
	 * This constructor is called when creating the corresponding microservice by
	 * cloning, i.e. the new object is added to the collection of producer contracts
	 * in the calling constructor of the new microservice afterwards. Cf.
	 * {@link Microservice#Microservice(Microservice, ConfigurationType)}.
	 * 
	 * @param origin            the source producer contract.
	 * @param microservice      the microservice to which the new producer contract
	 *                          belongs.
	 * @param configurationType the new configuration type.
	 * @throws UnknownTypeException one of the types is unknown in the new
	 *                              configuration type.
	 */
	public ProducerContract(final ProducerContract origin, final Microservice microservice,
			final ConfigurationType configurationType) throws UnknownTypeException {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(configurationType, "configuration type cannot be null");
		var newProducerContractType = configurationType
				.getProducerContractType(origin.producerContractType.getTypeIdentifier());
		if (newProducerContractType == null) {
			throw new UnknownTypeException("producer contract type " + origin.producerContractType.getTypeIdentifier()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		if (configurationType.getMicroserviceType(microservice.getMicroserviceType().getTypeIdentifier()) == null) {
			throw new UnknownTypeException("microservice type " + microservice.getMicroserviceType()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		producerContractType = newProducerContractType;
		this.identifier = computeIdentifier(microservice, producerContractType);
		this.microservice = microservice;
		this.pubSubConnectors = new HashMap<>();
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return the invariant holds.
	 */
	public boolean invariant() {
		return identifier != null && !identifier.isBlank() && producerContractType != null && pubSubConnectors != null
				&& microservice != null;
	}

	/**
	 * computes the identifier of the producer contract as being the concatenation
	 * of the producer contract type identifier with the instance identifier of the
	 * microservice.
	 * 
	 * @param microservice         the microservice to which this producer contract
	 *                             belongs.
	 * @param producerContractType the producer contract type.
	 * @return the instance identifier.
	 */
	public static String computeIdentifier(final Microservice microservice,
			final ProducerContractType producerContractType) {
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(producerContractType, "producerContractType cannot be null");
		return producerContractType.getTypeIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getMicroserviceType().getTypeIdentifier();
	}

	/**
	 * obtains the collection of publish-subscribe connectors.
	 * 
	 * @return the connectors.
	 */
	public Collection<PubSubConnector> getPubSubConnectors() {
		return pubSubConnectors.values();
	}

	/**
	 * obtains the publish-subscribe connector with the given name.
	 * 
	 * @param identifier the identifier of the connector searched for
	 * @return the connector.
	 */
	public PubSubConnector getPubSubConnector(final String identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return pubSubConnectors.get(identifier);
	}

	/**
	 * adds the given connector to the list of publish-subscribe connectors.
	 * 
	 * @param connector the connector to add.
	 */
	public void addConnector(final PubSubConnector connector) {
		Objects.requireNonNull(connector, "connector cannot be null");
		if (pubSubConnectors.get(connector.getIdentifier()) != null) {
			throw new IllegalArgumentException(connector + " already in pubSubConnectors");
		}
		this.pubSubConnectors.put(connector.getIdentifier(), connector);
		assert invariant();
	}

	/**
	 * removes the given connector from the list of publish-subscribe connectors.
	 * 
	 * @param connector the connector to remove.
	 */
	public void removeConnector(final PubSubConnector connector) {
		Objects.requireNonNull(connector, "connector cannot be null");
		if (pubSubConnectors.get(connector.getIdentifier()) == null) {
			throw new IllegalArgumentException(connector + " does not exist in pubSubConnectors");
		}
		this.pubSubConnectors.remove(connector.getIdentifier());
		assert invariant();
	}

	/**
	 * obtains the instance identifier of the producer contract. The identifier of
	 * the producer contract is the concatenation of the producer contract type
	 * identifier with the instance identifier of the microservice.
	 * 
	 * @return the instance identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL identifier of the instance identifier. Characters "/" are
	 * replaced by strings "ZZ". The PDDL identifier is used in PDDL domain file.
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return identifier.replace("/", "ZZ");
	}

	/**
	 * obtains the producer contract type to which it conforms.
	 * 
	 * @return the producer contract type.
	 */
	public ProducerContractType getProducerContractType() {
		return producerContractType;
	}

	/**
	 * obtains the microservice to which the producer contract belongs.
	 * 
	 * @return the microservice.
	 */
	public Microservice getMicroservice() {
		return microservice;
	}

	/**
	 * states whether this producer contract is deployable, i.e. if it must have
	 * consumers then there exists at least a consumer type of the connector types
	 * that is compatible with this producer contract.
	 * 
	 * @return true when it is deployable.
	 */
	public boolean isDeployable() {
		return !this.producerContractType.getMustHaveConsumers()
				|| (!getPubSubConnectors().isEmpty()) && getPubSubConnectors().stream()
						.anyMatch(pubsub -> pubsub.hasAtLeastOneCompatibleConsumerContract(this));
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ProducerContract)) {
			return false;
		}
		ProducerContract other = (ProducerContract) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "ProducerContract [identifier=" + identifier + ", producerContractType=" + producerContractType
				+ ", pubSubConnectors=" + pubSubConnectors.keySet().stream().collect(Collectors.joining(",", "[", "]"))
				+ "]";
	}
}
