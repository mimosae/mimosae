/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.connectortype;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.TypeIdentifier;
import mimosae.modelatruntime.type.contracttype.ConsumerContractType;
import mimosae.modelatruntime.type.contracttype.ProducerContractType;

/**
 * This class defines the concept of publish-subscribe connector type. A
 * publish-subscribe connector type brings together a collection of producer
 * contract types (of microservice types) and a collection of consumer contract
 * type (of microservices). Contrary to other connector types such as
 * client-server connector types or database connector types, a
 * publish-subscribe connector type has got an type identifier; this is so
 * because, for instance in AMQP and MQTT technologies, a publish-subscribe
 * connector is broker (that must be deployed, etc.).
 * 
 * @author Denis Conan
 */
public final class PubSubConnectorType implements ConnectorType {
	/**
	 * the type identifier of the publish-subscribe connector type.
	 */
	private final TypeIdentifier identifier;
	/**
	 * the collection of producer contract types of the publish-subscribe connector
	 * type.
	 */
	private final Map<TypeIdentifier, ProducerContractType> producerContractTypes;
	/**
	 * the collection of consumer contract types of the publish-subscribe connector
	 * type.
	 */
	private final Map<TypeIdentifier, ConsumerContractType> consumerContractTypes;

	/**
	 * constructs a new publish-subscribe connector type. There are no attached
	 * producer or consumer contract types.
	 * 
	 * @param identifier the identifier of the publish-subscribe connector type.
	 * @param version    the version of the publish-subscribe connector type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public PubSubConnectorType(final String identifier, final Semver version) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = TypeIdentifier.typeIdentifier(identifier, version);
		this.producerContractTypes = new HashMap<>();
		this.consumerContractTypes = new HashMap<>();
	}

	/**
	 * constructs a new publish-subscribe connector type by cloning an existing
	 * publish-subscribe connector type, and in the context of a new configuration
	 * type. This constructor is called when constructing a new configuration type
	 * by cloning an existing configuration type: see
	 * {@link ConfigurationType#ConfigurationType(ConfigurationType)}.
	 * <p>
	 * The two objects have the same type identifier. So, there are equals and they
	 * have the PDDL identifiers. The collections of producer and consumer contract
	 * types are copied. Every producer or consumer contract type must exist in the
	 * new configuration type.
	 * 
	 * @param origin            the source publish-subscribe connector type.
	 * @param configurationType the new configuration type
	 */
	public PubSubConnectorType(final PubSubConnectorType origin, final ConfigurationType configurationType) {
		super();
		Objects.requireNonNull(origin, "origin cannot be null");
		this.identifier = origin.identifier;
		Objects.requireNonNull(configurationType, "configurationType cannot be null");
		this.producerContractTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, ProducerContractType> entry : origin.producerContractTypes.entrySet()) {
			var thisContract = configurationType.getProducerContractType(entry.getValue().getTypeIdentifier());
			if (thisContract == null) {
				throw new IllegalStateException("producer contract type " + entry.getValue().getTypeIdentifier()
						+ " does not exist in the current configuration type");
			}
			this.producerContractTypes.put(thisContract.getTypeIdentifier(), thisContract);
			thisContract.addConnectorType(this);
		}
		this.consumerContractTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, ConsumerContractType> entry : origin.consumerContractTypes.entrySet()) {
			var thisContract = configurationType.getConsumerContractType(entry.getValue().getTypeIdentifier());
			if (thisContract == null) {
				throw new IllegalStateException("consumer contract type " + entry.getValue().getTypeIdentifier()
						+ " does not exist in the current configuration type");
			}
			this.consumerContractTypes.put(thisContract.getTypeIdentifier(), thisContract);
		}

	}

	/**
	 * obtains the type identifier.
	 * 
	 * @return the type identifier.
	 */
	public TypeIdentifier getTypeIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL identifier, which is built from the type identifier.
	 */
	@Override
	public String getPDDLIdentifier() {
		return identifier.getPDDLIdentifier();
	}

	/**
	 * obtains the producer contract type that corresponds to a given type
	 * identifier.
	 * 
	 * @param typeIdentifier the given type identifier of the producer contract type
	 *                       searched for.
	 * @return the producer contract type found.
	 */
	public ProducerContractType getProducerContractType(final TypeIdentifier typeIdentifier) {
		return producerContractTypes.get(typeIdentifier);
	}

	/**
	 * obtains the consume contract type that corresponds to a given type
	 * identifier.
	 * 
	 * @param typeIdentifier the given type identifier of the corsumer contract type
	 *                       searched for.
	 * @return the consumer contract type found.
	 */
	public ConsumerContractType getConsumerContractType(final TypeIdentifier typeIdentifier) {
		return consumerContractTypes.get(typeIdentifier);
	}

	/**
	 * adds a producer contract type to the publish-subscribe connector type.
	 * 
	 * @param producerContractType the producer contract type to add.
	 */
	public void addProducerContractType(final ProducerContractType producerContractType) {
		Objects.requireNonNull(producerContractType, "producerContractType cannot be null");
		if (producerContractTypes.get(producerContractType.getTypeIdentifier()) != null) {
			return;
		}
		producerContractTypes.put(producerContractType.getTypeIdentifier(), producerContractType);
		producerContractType.addConnectorType(this);
	}

	/**
	 * adds a consumer contract type to the publish-subscribe connector type.
	 * 
	 * @param consumerContractType the consumer contract type to add.
	 */
	public void addConsumerContractType(final ConsumerContractType consumerContractType) {
		Objects.requireNonNull(consumerContractType, "consumerContractType cannot be null");
		if (consumerContractTypes.get(consumerContractType.getTypeIdentifier()) != null) {
			throw new IllegalArgumentException(consumerContractType + " already in consumerContractTypes");
		}
		consumerContractTypes.put(consumerContractType.getTypeIdentifier(), consumerContractType);
	}

	/**
	 * states whether one of the producer contract types of the publish-subscribe
	 * connector types is attached to at least one of the consumer contract types of
	 * the publish-subscribe connector type that is compatible. This method is
	 * called when stating whether the microservice type that contains the given
	 * producer contract type is instantiable, i.e. if the producer contract type
	 * must have consumer types, there is at least one compatible consumer types
	 * attached to one of the publish-subscribe connector types of the given
	 * producer contract type.
	 * 
	 * @param producerContractType the producer contract type.
	 * @return true when there is at least one compatible consumer contract type in
	 *         this publish-subscribe connector type.
	 */
	public boolean hasAtLeastOneCompatibleConsumerContractType(final ProducerContractType producerContractType) {
		return consumerContractTypes.values().stream().anyMatch(producerContractType::isCompatibleWith);
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof PubSubConnectorType)) {
			return false;
		}
		PubSubConnectorType other = (PubSubConnectorType) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "PubSubServerConnectorType [getIdentifier()=" + getTypeIdentifier() + ", producerContractTypes="
				+ producerContractTypes.keySet().stream().map(TypeIdentifier::toString)
						.collect(Collectors.joining(",", "[", "]"))
				+ ", consumerContractType=" + consumerContractTypes.keySet().stream().map(TypeIdentifier::toString)
						.collect(Collectors.joining(",", "[", "]"))
				+ "]";
	}
}
