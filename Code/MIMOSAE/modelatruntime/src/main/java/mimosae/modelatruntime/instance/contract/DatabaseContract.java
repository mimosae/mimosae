// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.instance.connnector.DatabaseConnector;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;

/**
 * This class defines the concept of database contract instance. It has an
 * identifier; it conforms to a database contract type; it can be connected to a
 * database connector; and it belongs to a microservice instance.
 * 
 * @author Denis Conan
 */
public final class DatabaseContract {
	private final String identifier;
	private DatabaseContractType databaseContractType;
	private DatabaseConnector databaseConnector;
	private final Microservice microservice;

	public DatabaseContract(final DatabaseContractType databaseContractType, final Microservice microservice) {
		Objects.requireNonNull(databaseContractType, "databaseContractType cannot be null");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, databaseContractType);
		this.databaseContractType = databaseContractType;
		this.microservice = microservice;
		assert invariant();
	}

	public DatabaseContract(final DatabaseContract origin, final Microservice microservice) {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "database cannot be null");
		this.identifier = computeIdentifier(microservice, origin.databaseContractType);
		this.databaseContractType = origin.databaseContractType;
		this.microservice = microservice;
		assert invariant();
	}

	public DatabaseContract(final DatabaseContract origin, final Microservice microservice,
			final ConfigurationType configurationType) throws UnknownTypeException {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "database cannot be null");
		Objects.requireNonNull(configurationType, "configurationType cannot be null");
		var newContractType = configurationType
				.getDatabaseContractType(origin.databaseContractType.getTypeIdentifier());
		if (newContractType == null) {
			throw new UnknownTypeException("database contract type " + origin.databaseContractType.getTypeIdentifier()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		databaseContractType = newContractType;
		this.identifier = computeIdentifier(microservice, databaseContractType);
		this.microservice = microservice;
		assert invariant();
	}

	public boolean invariant() {
		return identifier != null && !identifier.isBlank() && databaseContractType != null && microservice != null;
	}

	public static String computeIdentifier(final Microservice microservice,
			final DatabaseContractType databaseContractType) {
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(databaseContractType, "databaseContractType cannot be null");
		return databaseContractType.getTypeIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getMicroserviceType().getTypeIdentifier();
	}

	public void setConnector(final DatabaseConnector connector) throws AlreadyConnectedException {
		Objects.requireNonNull(connector, "connector cannot be null");
		if (databaseConnector != null) {
			throw new AlreadyConnectedException(
					connector.getIdentifier() + " already assigned, i.e. database contract already connected");
		}
		this.databaseConnector = connector;
		assert invariant();
	}

	public void unsetConnector() {
		disconnect();
	}

	public void disconnect() {
		databaseConnector = null;
		assert invariant();
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getPDDLIdentifier() {
		return identifier.replace("/", "ZZ");
	}

	public DatabaseContractType getDatabaseContractType() {
		return databaseContractType;
	}

	public Microservice getMicroservice() {
		return microservice;
	}

	public boolean isConnected() {
		return databaseConnector != null;
	}

	public DatabaseConnector getDatabaseConnector() {
		return databaseConnector;
	}

	public void setDatabaseConnector(DatabaseConnector databaseConnector) {
		this.databaseConnector = databaseConnector;
		assert invariant();
	}

	/**
	 * states whether the microservice that includes this database contract can be
	 * deployable, i.e. the database contract has a database connector attached.
	 * 
	 * @return true when there is database connector.
	 */
	public boolean isDeployable() {
		return databaseConnector != null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof DatabaseContract)) {
			return false;
		}
		DatabaseContract other = (DatabaseContract) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "ClientContract [identifier=" + identifier + ", databaseContractType="
				+ databaseContractType.getTypeIdentifier() + ", databaseServerConnector="
				+ ((databaseConnector == null) ? "not connected" : databaseConnector.getIdentifier()) + "]";
	}
}
