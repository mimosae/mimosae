/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.datamodel;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.type.TypeIdentifier;

/**
 * This class models an event type. In this class, we do not model the structure
 * of events. Therefore, the only attributes are the identifier and the version
 * of the type.
 * 
 * @author Denis Conan
 */
public class EventType {
	/**
	 * the identifier of the type.
	 */
	private final TypeIdentifier identifier;

	/**
	 * constructs an event type.
	 * 
	 * @param identifier the identifier of the event type.
	 * @param version    the version of the type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public EventType(final String identifier, final Semver version) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = TypeIdentifier.typeIdentifier(identifier, version);
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return identifier != null;
	}

	/**
	 * obtains the type identifier.
	 * 
	 * @return the type identifier.
	 */
	public TypeIdentifier getTypeIdentifier() {
		return identifier;
	}

	/**
	 * computes the hash code of the objects by using the type identifier.
	 * Subclasses should override this method when the content of an event type is
	 * modeled.
	 * 
	 * @return the hash code.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	/**
	 * states whether two objects are equals by using the type identifier.
	 * Subclasses should override this method when the content of an event type is
	 * modeled.
	 * 
	 * @return the hash code.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof EventType)) {
			return false;
		}
		EventType other = (EventType) obj;
		return Objects.equals(identifier, other.identifier);
	}

	/**
	 * obtains the stringified view of the object. This method should be overridden
	 * by subclasses.
	 * 
	 * @return the subclass should override this method.
	 */
	@Override
	public String toString() {
		return "EventType [identifier=" + identifier + "]";
	}

}
