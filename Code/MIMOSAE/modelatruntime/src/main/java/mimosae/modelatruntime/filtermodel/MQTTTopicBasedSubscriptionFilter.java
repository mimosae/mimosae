/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.filtermodel;

import java.util.Objects;
import java.util.regex.Pattern;

import com.vdurmont.semver4j.Semver;

import external.util.MQTTTopicChecker;

/**
 * This class defines the filter model of an MQTT filter for subscriptions, that
 * is for being notified by an MQTT broker. The subscription filter must conform
 * to the MQTT specification.
 * <p>
 * NB: the name of the class does not contain "Type" because there is no need
 * for the corresponding class with "Instance": i.e. contrary to the concepts of
 * "configuration type", "microservice type", "database system type", etc. This
 * is so because instances of filters are not modeled: this class is used only
 * the "type" part of the modeling.
 * 
 * @author Denis Conan
 */
public final class MQTTTopicBasedSubscriptionFilter extends TopicBasedSubscriptionFilter {
	/**
	 * the pattern that corresponds to the subscription filter.
	 */
	private final Pattern pattern;

	/**
	 * constructs a new MQTT subscription filter. Such a filter is used for
	 * filtering published messages based on their topic. The conformance of the
	 * subscription filter to the MQTT specification is done by computing the
	 * corresponding {@link java.util.regex.Pattern}.
	 * 
	 * @param identifier         the identifier of the filter.
	 * @param version            the version of the filter.
	 * @param subscriptionFilter the subscription filter.
	 */
	public MQTTTopicBasedSubscriptionFilter(final String identifier, final Semver version,
			final String subscriptionFilter) {
		super(identifier, version, subscriptionFilter);
		Objects.requireNonNull(subscriptionFilter, "subscription cannot be null");
		if (subscriptionFilter.isBlank()) {
			throw new IllegalArgumentException("subscription cannot be blank");
		}
		if (!MQTTTopicChecker.subcriptionCheck(subscriptionFilter)) {
			throw new IllegalArgumentException("subscription filter does not conform to MQTT specification");
		}
		this.pattern = MQTTTopicChecker.toRegex(subscriptionFilter);
		assert pattern != null;
	}

	/**
	 * obtains the {@link java.util.regex.Pattern} of the subscription filter.
	 * 
	 * @return the reference to the {@link java.util.regex.Pattern} object.
	 */
	public Pattern getPattern() {
		return pattern;
	}

	@Override
	public String toString() {
		return "MQTTTopicBasedSubscriptionFilter [getIdentifier()=" + getIdentifier() + ", getVersion()=" + getVersion()
				+ ", getSubscription()=" + getSubscriptionFilter() + ", pattern=" + pattern + "]";
	}
}
