/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.instance.connnector.ClientServerConnector;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;

/**
 * This class defines the concept of client contract instance. It has an
 * identifier; it conforms to a client contract type; it can be connected to a
 * server contract via a client-server connector; and it belongs to a
 * microservice instance.
 * <p>
 * The client contract is identified by an instance identifier that is the
 * concatenation of the client contract type identifier with the instance
 * identifier of the microservice to which it belongs.
 * 
 * @author Denis Conan
 */
public final class ClientContract {
	/**
	 * the instance identifier. It is the concatenation of the client contract type
	 * identifier with the instance identifier of the microservice.
	 */
	private final String identifier;
	/**
	 * the client contract type to which it conforms.
	 */
	private ClientContractType clientContractType;
	/**
	 * the client-server connector. Not null when the client contract is connected.
	 */
	private ClientServerConnector clientServerConnector;
	/**
	 * the microservice to which it belongs.
	 */
	private final Microservice microservice;

	/**
	 * constructs a new client contract. This new client contract is created when
	 * creating the corresponding microservice, i.e. the new object is added to the
	 * collection of client contracts in the calling constructor of the microservice
	 * afterwards (Cf. {@link Microservice#Microservice(Microservice)}.
	 * 
	 * @param clientContractType the client contract type it conforms to.
	 * @param microservice       the microservice to which it belongs.
	 */
	public ClientContract(final ClientContractType clientContractType, final Microservice microservice) {
		Objects.requireNonNull(clientContractType, "clientContractType cannot be null");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, clientContractType);
		this.clientContractType = clientContractType;
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * constructs a new client contract by cloning a source client contract. The new
	 * client contract belongs to a microservice that is different from the
	 * microservice of the source client contract. The identifiers of the source and
	 * the new objects are different, and thus the source and new objects are not
	 * equal. The client contract types are the same. Finally, the new client
	 * contract does not have a client-server connector attached.
	 * <p>
	 * This constructor is called when creating the corresponding microservice by
	 * cloning, i.e. the new object is added to the collection of client contracts
	 * in the calling constructor of the new microservice afterwards. (Cf.
	 * {@link Microservice#Microservice(String, mimosae.modelatruntime.type.MicroserviceType)}).
	 * 
	 * @param origin       the source client contract.
	 * @param microservice the microservice to which the new client contract
	 *                     belongs.
	 */
	public ClientContract(final ClientContract origin, final Microservice microservice) {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, origin.clientContractType);
		this.clientContractType = origin.clientContractType;
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * constructs a new client contract by cloning a source client contract. The new
	 * client contract belongs to a microservice that is different from the
	 * microservice of the source client contract. The configuration type is also
	 * new. The identifiers of the source and the new objects are different, and
	 * thus the source and new objects are not equal. The client contract type must
	 * belong to the new configuration type. The client contract type of the new
	 * client contract and the client contract type of the source client contract
	 * are different objects, but they are equal.
	 * <p>
	 * The client contract type of the source client contract must belong to the new
	 * configuration type. Similarly, the microservice type of the microservice must
	 * belong to the new configuration type.
	 * <p>
	 * This constructor is called when creating the corresponding microservice by
	 * cloning, i.e. the new object is added to the collection of client contracts
	 * in the calling constructor of the new microservice afterwards. Cf.
	 * {@link Microservice#Microservice(Microservice, ConfigurationType)}.
	 * 
	 * @param origin            the source client contract.
	 * @param microservice      the microservice to which the new client contract
	 *                          belongs.
	 * @param configurationType the new configuration type.
	 * @throws UnknownTypeException one of the types is unknown in the new
	 *                              configuration type.
	 */
	public ClientContract(final ClientContract origin, final Microservice microservice,
			final ConfigurationType configurationType) throws UnknownTypeException {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(configurationType, "configurationType cannot be null");
		var newClientContractType = configurationType
				.getClientContractType(origin.clientContractType.getTypeIdentifier());
		if (newClientContractType == null) {
			throw new UnknownTypeException("client contract type " + origin.clientContractType.getTypeIdentifier()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		if (configurationType.getMicroserviceType(microservice.getMicroserviceType().getTypeIdentifier()) == null) {
			throw new UnknownTypeException("microservice type " + microservice.getMicroserviceType()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		clientContractType = newClientContractType;
		this.identifier = computeIdentifier(microservice, clientContractType);
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return the invariant holds.
	 */
	public boolean invariant() {
		return identifier != null && !identifier.isBlank() && clientContractType != null && microservice != null;
	}

	/**
	 * computes the identifier of the client contract as being the concatenation of
	 * the client contract type identifier with the instance identifier of the
	 * microservice.
	 * 
	 * @param microservice       the microservice to which this client contract
	 *                           belongs.
	 * @param clientContractType the client contract type.
	 * @return the instance identifier.
	 */
	public static String computeIdentifier(final Microservice microservice,
			final ClientContractType clientContractType) {
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(clientContractType, "clientContractType cannot be null");
		return clientContractType.getTypeIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getMicroserviceType().getTypeIdentifier();
	}

	/**
	 * obtains the instance identifier of the client contract. The identifier of the
	 * client contract is the concatenation of the client contract type identifier
	 * with the instance identifier of the microservice.
	 * 
	 * @return the instance identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL identifier of the instance identifier. Characters "/" are
	 * replaced by strings "ZZ". The PDDL identifier is used in PDDL domain file.
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return identifier.replace("/", "ZZ");
	}

	/**
	 * obtains the client contract type to which it conforms.
	 * 
	 * @return the client contract type.
	 */
	public ClientContractType getClientContractType() {
		return clientContractType;
	}

	/**
	 * obtains the microservice to which the client contract belongs.
	 * 
	 * @return the microservice.
	 */
	public Microservice getMicroservice() {
		return microservice;
	}

	/**
	 * states whether the client contract has a connector, i.e. the microservice to
	 * which it belongs is connected.
	 * 
	 * @return true when the client contract is connected.
	 */
	public boolean isConnected() {
		return clientServerConnector != null;
	}

	/**
	 * obtains the client-server connector. The microservice to which this client
	 * contract belongs is said to be connected when the client-server connector is
	 * not null: cf. {@link #isConnected()}.
	 * 
	 * @return the client-server connector.
	 */
	public ClientServerConnector getClientServerConnector() {
		return clientServerConnector;
	}

	/**
	 * is used when creating a client-server link between two microservices. This is
	 * the client contract, that is the client part. In this class, the creation of
	 * the link is performed by setting the client-server connector. The client
	 * contract must not be already connected, i.e. there is no prior connector.
	 * 
	 * @param connector the client-server connector.
	 * @throws AlreadyConnectedException the client contract is already connected.
	 */
	public void setConnector(final ClientServerConnector connector) throws AlreadyConnectedException {
		Objects.requireNonNull(connector, "connector cannot be null");
		if (clientServerConnector != null) {
			throw new AlreadyConnectedException(
					connector.getIdentifier() + " already assigned, i.e. client contract already connected");
		}
		this.clientServerConnector = connector;
		assert invariant();
	}

	/**
	 * is used when unlinking the client microservice referenced by this client
	 * contract.
	 */
	public void unsetConnector() {
		this.clientServerConnector = null;
		assert invariant();
	}

	/**
	 * states whether this producer contract is deployable, i.e. if
	 * "{@link ClientContractType#mustHaveConnectorAttached} implies
	 * {@link #clientServerConnector} is not null".
	 * 
	 * @return the boolean result of the condition.
	 */
	public boolean isDeployable() {
		return !this.clientContractType.getMustBeConnected() || clientServerConnector != null;
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ClientContract)) {
			return false;
		}
		ClientContract other = (ClientContract) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "ClientContract [identifier=" + identifier + ", clientContractType="
				+ clientContractType.getTypeIdentifier() + ", clientServerConnector="
				+ ((clientServerConnector == null) ? "not connected" : clientServerConnector.getIdentifier()) + "]";
	}
}
