/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.graph.DefaultEdge;

import mimosae.modelatruntime.ActionLog;

/**
 * This class defines the content of the edges of the graph of configurations.
 * It contains the log of actions to transit from the source configuration to
 * the target configuration.
 * 
 * @author Denis Conan
 */
public final class InstanceEdge extends DefaultEdge {
	/**
	 * the serial version unique identifier.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * the collection of {@link mimosae.modelatruntime.ActionLog} objects.
	 */
	private List<ActionLog> logs;

	/**
	 * constructs a new edge with an empty collection of action log. The edge is
	 * created with an empty collection and then the logs are assigned using
	 * {@link #setLogs(List)}, for instance in the process to commit the current
	 * configuration.
	 */
	public InstanceEdge() {
		super();
		logs = new ArrayList<>();
	}

	/**
	 * obtains the collection of action logs.
	 * 
	 * @return a copy of the collection.
	 */
	public List<ActionLog> getLogs() {
		var temp = new ArrayList<ActionLog>();
		temp.addAll(logs);
		return temp;
	}

	/**
	 * sets the collection of action logs, for instance, when committing.
	 * 
	 * @param logs the collection of logs that is going to replace the current
	 *             collection.
	 */
	public void setLogs(final List<ActionLog> logs) {
		this.logs = logs;
	}

	@Override
	public String toString() {
		return "TypeEdge [source=" + getSource() + ", target=" + getTarget() + ", logs=" + logs + "]";
	}

}
