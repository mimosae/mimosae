/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.datamodel;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;

/**
 * In the future, this class may be completed for detailing the content of a
 * structured event types.
 * 
 * @author Denis Conan
 */
public final class StructuredEventType extends EventType {
	/**
	 * constructs an object as an event type. There is no content for now.
	 * 
	 * @param identifier the identifier of the event type.
	 * @param version    the version of the event type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public StructuredEventType(final String identifier, final Semver version)
			throws MalformedIdentifierException {
		super(identifier, version);
	}

	/**
	 * computes the hash code of the objects by using the type identifier. This
	 * method should be modified when adding the modeling of this type.
	 * 
	 * @return the hash code.
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * states whether two objects are equals by using the type identifier. This
	 * method should be modified when adding the modeling of this type.
	 * 
	 * @return the hash code.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		return obj instanceof SemiStructuredEventType;
	}

	/**
	 * obtains the stringified view of the object. This method should be modified
	 * when adding the modeling of this type.
	 * 
	 * @return the subclass should override this method.
	 */
	@Override
	public String toString() {
		return "StructuredEventType [getIdentifier()=" + getTypeIdentifier() + "]";
	}

}
