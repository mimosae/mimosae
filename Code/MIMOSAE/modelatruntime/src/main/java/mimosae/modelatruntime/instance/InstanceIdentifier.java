/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.type.TypeIdentifier;

/**
 * This class defines the instance identifier. An instance is uniquely
 * identified by the identifier of the instance plus the identifier and the
 * version of its type: e.g. for a microservice, "user_service_replica_1" plus
 * "user_service" in version "0.0.1".; for a database system,
 * "database_users_replica_1" plus "database_user_type" in version "0.0.1"; and
 * for a publish-subscribe connector, "rabbitmq_for_log" of type "rabbitmq" and
 * "3.9.1".
 * 
 * @author Denis Conan
 */
public final class InstanceIdentifier {
	/**
	 * the identifier of the instance.
	 */
	private final String instIdentifier;
	/**
	 * the identifier of the type.
	 */
	private final String typeIdentifier;
	/**
	 * the version of the type.
	 */
	private final Semver typeVersion;

	/**
	 * constructs a new instance identifier. An instance is uniquely identified by
	 * the identifier of the instance plus the identifier and the version of its
	 * type: e.g. for a microservice, "user_service_replica_1" plus "user_service"
	 * in version "0.0.1".; for a database system, "database_users_replica_1" plus
	 * "database_user_type" in version "0.0.1"; and for a publish-subscribe
	 * connector, "rabbitmq_for_log" of type "rabbitmq" and "3.9.1".
	 * 
	 * @param instanceIdentifier the identifier of the instance.
	 * @param typeIdentifier     the identifier of the type.
	 * @param typeVersion        the version of the type
	 * @throws MalformedIdentifierException one of the identifiers is malformed.
	 */
	public InstanceIdentifier(final String instanceIdentifier, final String typeIdentifier, final Semver typeVersion)
			throws MalformedIdentifierException {
		Objects.requireNonNull(instanceIdentifier, "instanceIdentifier cannot be null");
		if (instanceIdentifier.isBlank()) {
			throw new IllegalArgumentException("instanceIdentifier cannot be blank");
		}
		if (instanceIdentifier.matches(".*\\s.*")) {
			throw new MalformedIdentifierException("instanceIdentifier cannot contain spaces");
		}
		Objects.requireNonNull(typeIdentifier, "typeIdentifier cannot be null");
		if (typeIdentifier.isBlank()) {
			throw new IllegalArgumentException("typeIdentifier cannot be blank");
		}
		if (typeIdentifier.matches(".*\\s.*")) {
			throw new MalformedIdentifierException("typeIdentifier cannot contain spaces");
		}
		Objects.requireNonNull(typeVersion, "version cannot be null");
		this.instIdentifier = instanceIdentifier;
		this.typeIdentifier = typeIdentifier;
		Semver tv = typeVersion;
		if (typeVersion.getType() != Semver.SemverType.NPM) {
			tv = new Semver(typeVersion.getValue(), Semver.SemverType.NPM);
		}
		this.typeVersion = tv;
	}

	/**
	 * constructs a new instance identifier. An instance is uniquely identified by
	 * the identifier of the instance plus the identifier and the version of its
	 * type: e.g. for a microservice, "user_service_replica_1" plus "user_service"
	 * in version "0.0.1".; for a database system, "database_users_replica_1" plus
	 * "database_user_type" in version "0.0.1"; and for a publish-subscribe
	 * connector, "rabbitmq_for_log" of type "rabbitmq" and "3.9.1".
	 * 
	 * @param instanceIdentifier the identifier of the instance.
	 * @param typeIdentifier     the type identifier.
	 * @throws MalformedIdentifierException the identifier of the instance is
	 *                                      malformed.
	 */
	public InstanceIdentifier(final String instanceIdentifier, final TypeIdentifier typeIdentifier)
			throws MalformedIdentifierException {
		this(instanceIdentifier, typeIdentifier.getIdentifier(), typeIdentifier.getVersion());
	}

	/**
	 * obtains the identifier of the instance, i.e. the first of the three elements
	 * that constitute the instance identifier: e.g. for a microservice,
	 * "user_service_replica_1"; for a database system, "database_users_replica_1" ;
	 * and for a publish-subscribe connector, "rabbitmq_for_log".
	 * 
	 * @return the identifier of the instance.
	 */
	public String getInstanceIdentifier() {
		return instIdentifier;
	}

	/**
	 * obtains the identifier of the type i.e. the second of the three elements that
	 * constitute the instance identifier: e.g. for a microservice, "user_service";
	 * for a database system, "database_users" ; and for a publish-subscribe
	 * connector, "rabbitmq".
	 * 
	 * @return the identifier of the type.
	 */
	public String getTypeIdentifier() {
		return typeIdentifier;
	}

	/**
	 * obtains the version of the type, i.e. the last of the three elements that
	 * constitute the instance identifier.
	 * 
	 * @return the version of the type.
	 */
	public Semver getTypeVersion() {
		return typeVersion;
	}

	/**
	 * obtains the PDDL identifier of the instance identifier that is used in the
	 * PDDL domain file. Characters "/" are replaced by "ZZ", and characters "." are
	 * replaced by "_".
	 * 
	 * @return the PDDL name.
	 */
	public String getPDDLIdentifier() {
		return instIdentifier.replace("/", "ZZ") + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS
				+ typeIdentifier.replace("/", "ZZ") + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS
				+ typeVersion.toString().replace(".", Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS);
	}

	/**
	 * states whether the version of the type of this instance identifier satisfies
	 * the version requirement: e.g. "1.0.1" satisfies "1.0.*". It uses method
	 * {@link com.vdurmont.semver4j.Semver#satisfies(String)}.
	 * 
	 * @param requirement the version requirement to satisfy.
	 * @return true if the version satisfies the given requirement.
	 */
	public boolean isVersionSatisfied(final String requirement) {
		return this.typeVersion.satisfies(requirement);
	}

	@Override
	public int hashCode() {
		return Objects.hash(instIdentifier, typeIdentifier, typeVersion);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof InstanceIdentifier)) {
			return false;
		}
		InstanceIdentifier other = (InstanceIdentifier) obj;
		return Objects.equals(instIdentifier, other.instIdentifier)
				&& Objects.equals(typeIdentifier, other.typeIdentifier)
				&& Objects.equals(typeVersion, other.typeVersion);
	}

	@Override
	public String toString() {
		return instIdentifier + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS + typeIdentifier
				+ Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS + "v" + typeVersion;
	}
}
