// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.dependencymodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import mimosae.modelatruntime.exception.VersionNotSatisfiedRequirementException;
import mimosae.modelatruntime.type.MicroserviceType;

public class MicroserviceTypeDependency {
	private final String identifier;
	private final String dependeeIdentifier;
	private String dependeeVersionRequirement;
	private final MicroserviceType depender;
	private final List<MicroserviceType> dependees;

	public MicroserviceTypeDependency(final String identifier, final String dependeeIdentifier,
			final String dependeeVersionRequirement, final MicroserviceType depender) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(dependeeIdentifier, "dependeeIdentifier cannot be null");
		if (dependeeIdentifier.isBlank()) {
			throw new IllegalArgumentException("dependeeIdentifier cannot be blank");
		}
		Objects.requireNonNull(dependeeVersionRequirement, "dependeeVersionRequirement cannot be null");
		if (dependeeVersionRequirement.isBlank()) {
			throw new IllegalArgumentException("dependeeVersionRequirement cannot be blank");
		}
		Objects.requireNonNull(depender, "depender cannot be null");
		this.identifier = identifier;
		this.dependeeIdentifier = dependeeIdentifier;
		this.dependeeVersionRequirement = dependeeVersionRequirement;
		this.depender = depender;
		this.dependees = new ArrayList<>();
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getDependeeIdentifier() {
		return dependeeIdentifier;
	}

	public String getDependeeVersionRequirement() {
		return dependeeVersionRequirement;
	}

	public MicroserviceType getDepender() {
		return depender;
	}

	public List<MicroserviceType> getDependees() {
		return dependees;
	}

	public void addDependee(final MicroserviceType... dependees) {
		Objects.requireNonNull(dependees, "dependees cannot be null");
		for (MicroserviceType toAdd : dependees) {
			if (toAdd == null) {
				throw new IllegalArgumentException("microserviceTypes cannot contain null element");
			}
			if (this.dependees.contains(toAdd)) {
				throw new IllegalArgumentException(toAdd + " already in the list of dependees");
			}
			if (!toAdd.getTypeIdentifier().getIdentifier().equals(this.dependeeIdentifier)) {
				throw new IllegalArgumentException(toAdd + " does not satisfied required dependee identifier");
			}
			if (toAdd.getTypeIdentifier().getIdentifier().equals(this.depender.getTypeIdentifier().getIdentifier())) {
				throw new IllegalArgumentException(toAdd + " cannot be the same as " + depender);
			}
			if (!isVersionSatisfied(toAdd)) {
				throw new IllegalArgumentException(
						toAdd + " does not satisfied required version " + this.dependeeVersionRequirement);
			}
			this.dependees.add(toAdd);
		}
	}

	public boolean isVersionSatisfied(final MicroserviceType dependee) {
		return dependee.getTypeIdentifier().isVersionSatisfied(this.dependeeVersionRequirement);
	}

	public void changeVersionRequirement(final String newVersionRequirement)
			throws VersionNotSatisfiedRequirementException {
		Objects.requireNonNull(newVersionRequirement, "newVersionRequirement cannot be null");
		if (newVersionRequirement.isBlank()) {
			throw new IllegalArgumentException("newVersionRequirement cannot be blank");
		}
		for (MicroserviceType dependee : this.dependees) {
			if (!dependee.getTypeIdentifier().isVersionSatisfied(newVersionRequirement)) {
				throw new VersionNotSatisfiedRequirementException(
						dependee.getTypeIdentifier() + " does not satisfy new requirement '" + newVersionRequirement
								+ "', change version requirement failed");
			}
		}
		this.dependeeVersionRequirement = newVersionRequirement;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (!(o instanceof MicroserviceTypeDependency))
			return false;
		MicroserviceTypeDependency that = (MicroserviceTypeDependency) o;
		return getIdentifier().equals(that.getIdentifier());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getIdentifier());
	}

	@Override
	public String toString() {
		return "MicroserviceTypeDependency [" + "identifier=" + identifier + ",\n\tdependeeIdentifier="
				+ dependeeIdentifier + ",\tdependeeVersionRequirement=" + "\'" + dependeeVersionRequirement + "\'"
				+ ",\tdepender=" + depender.getTypeIdentifier() + ",\n\tdependees=" + dependees.stream()
						.map(n -> n.getTypeIdentifier().toString()).collect(Collectors.joining(",", "[", "]"))
				+ "\n]";
	}
}
