// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.dependencymodel;

import java.util.Comparator;

import mimosae.modelatruntime.type.TypeIdentifier;

public class TypeIdentifierVersionComparator implements Comparator<TypeIdentifier> {

	@Override
	public int compare(final TypeIdentifier identifier1, final TypeIdentifier identifier2) {
		if (identifier1.getIdentifier().equals(identifier2.getIdentifier())) {
			if (identifier1.getVersion().isEqualTo(identifier2.getVersion())
					|| identifier1.getVersion().isEquivalentTo(identifier2.getVersion())) {
				return 0;
			}
			if (identifier1.getVersion().isGreaterThan(identifier2.getVersion())) {
				return 1;
			}
			if (identifier1.getVersion().isLowerThan(identifier2.getVersion())) {
				return -1;
			}
		}
		return identifier1.getIdentifier().compareTo(identifier2.getIdentifier());
	}
}
