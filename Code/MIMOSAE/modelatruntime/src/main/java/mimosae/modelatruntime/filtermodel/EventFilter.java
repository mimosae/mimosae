/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.filtermodel;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

/**
 * This is the root class for modeling event filters in the framework. A filter
 * is identified by its identifier and its version.
 * <p>
 * NB: the name of the class does not contain "Type" because there is no need
 * for the corresponding class with "Instance": i.e. contrary to the concepts of
 * "configuration type", "microservice type", "database system type", etc. This
 * is so because instances of filters are not modeled: this class is used only
 * the "type" part of the modeling.
 * 
 * @author Denis Conan
 */
public abstract class EventFilter {
	/**
	 * the identifier of the event filter.
	 */
	private final String identifier;
	/**
	 * the version of the event filter.
	 */
	private final Semver version;

	/**
	 * constructs a new event filter.
	 * 
	 * @param identifier the identifier of the event filter.
	 * @param version    the version of the event filter.
	 */
	protected EventFilter(final String identifier, final Semver version) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = identifier;
		this.version = version;
	}

	/**
	 * obtains the identifier of the event filter.
	 * 
	 * @return the identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the version of the event filter.
	 * 
	 * @return the version.
	 */
	public Semver getVersion() {
		return version;
	}

	@Override
	public final int hashCode() {
		return Objects.hash(identifier, version);
	}

	@Override
	public final boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof EventFilter)) {
			return false;
		}
		EventFilter other = (EventFilter) obj;
		return Objects.equals(identifier, other.identifier) && Objects.equals(version, other.version);
	}
}
