/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.connnector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.instance.contract.ClientContract;
import mimosae.modelatruntime.instance.contract.ServerContract;
import mimosae.modelatruntime.type.connectortype.ClientServerConnectorType;
import mimosae.modelatruntime.type.connectortype.ConnectorType;

/**
 * This class defines the concept of client-server connector instance.
 * Currently, there is no other data than the references to the client and the
 * server contract, plus the instance identifier and the conforming type. The
 * identifier of this connector is the concatenation of the identifier of the
 * client contract (cf. {@link ClientContract#getIdentifier()} and the
 * identifier of the server contract (cf.
 * {@link ServerContract#getIdentifier()}. Therefore, the identifier includes
 * the identifier of the client microservice and the identifier of the server
 * microservice.
 * 
 * @author Denis Conan
 */
public final class ClientServerConnector extends Connector {
	/**
	 * the client contract instance.
	 */
	private final ClientContract clientContract;
	/**
	 * the server contract instance.
	 */
	private final ServerContract serverContract;

	/**
	 * constructs a new client server connector. The client contract type of the
	 * client contract must be equal to the client contract type of the connector
	 * type. Similarly, the server contract type of the server contract must be
	 * equal to the server contract type of the connector type. The client contract
	 * must not be already connected (to a client server connector). The client
	 * contract must be compatible with the server contract (cf.
	 * {@link mimosae.modelatruntime.type.contracttype.ClientContractType#isCompatibleWith(mimosae.modelatruntime.type.contracttype.ClientServerContractType)}).
	 * 
	 * @param connectorType  the connector type.
	 * @param clientContract the client contract.
	 * @param serverContract the server contract.
	 * @throws AlreadyConnectedException the client contract is already connected.
	 */
	public ClientServerConnector(final ConnectorType connectorType, final ClientContract clientContract,
			final ServerContract serverContract) throws AlreadyConnectedException {
		super(computeIdentifier(clientContract, serverContract), connectorType);
		if (!(connectorType instanceof ClientServerConnectorType)) {
			throw new IllegalArgumentException("connectorType is not a client server connectorType ("
					+ connectorType.getClass().getCanonicalName() + ")");
		}
		var clientServerConnectorType = (ClientServerConnectorType) connectorType;
		Objects.requireNonNull(clientContract, "clientContract cannot be null");
		Objects.requireNonNull(serverContract, "serverContract cannot be null");
		if (!clientContract.getClientContractType().isCompatibleWith(serverContract.getServerContractType())) {
			throw new IllegalArgumentException(clientContract.getClientContractType().getTypeIdentifier()
					+ " is not compatible with " + serverContract.getServerContractType().getTypeIdentifier());
		}
		if (!clientContract.getClientContractType().equals(clientServerConnectorType.getClientContractType())) {
			throw new IllegalArgumentException("type of ClientServerContractType provided in clientContract ("
					+ clientContract.getClientContractType().getTypeIdentifier() + ") is different from connectorType");
		}
		if (!serverContract.getServerContractType().equals(clientServerConnectorType.getServerContractType())) {
			throw new IllegalArgumentException("type of ClientServerContractType provided in serverContract ("
					+ serverContract.getServerContractType().getTypeIdentifier() + ") is different from connectorType");
		}
		this.clientContract = clientContract;
		this.serverContract = serverContract;
		this.clientContract.setConnector(this);
		assert invariant();
	}

	/**
	 * constructs a new client server connector by cloning a source client server
	 * connector. The identifier of the new connector is equal to the identifier of
	 * the source connector; hence, the two connectors are equal. The connector type
	 * of the two connectors are the same object. The client contracts of the two
	 * connectors must have the same client contract type, are equals, but are not
	 * the same objects. Similarly, the server contracts of the two connectors must
	 * have the same server contract type, are equals, but a priori are not the same
	 * objects.
	 * <p>
	 * By construction, the client contract type of the client contract is equal to
	 * the client contract type of the connector type. Similarly, the server
	 * contract type of the server contract is equal to the server contract type of
	 * the connector type.
	 * <p>
	 * The client contract must not be already connected (to a client server
	 * connector). The client contract must be compatible with the server contract
	 * (cf.
	 * {@link mimosae.modelatruntime.type.contracttype.ClientContractType#isCompatibleWith(mimosae.modelatruntime.type.contracttype.ClientServerContractType)}).
	 * 
	 * @param origin         the source connector.
	 * @param clientContract the client contract of the new connector.
	 * @param serverContract the server contract of the new connector.
	 * @throws AlreadyConnectedException the client contract is already connected.
	 */
	public ClientServerConnector(final ClientServerConnector origin, final ClientContract clientContract,
			final ServerContract serverContract) throws AlreadyConnectedException {
		super(origin);
		var clientServerConnectorType = (ClientServerConnectorType) origin.getConnectorType();
		Objects.requireNonNull(clientContract, "clientContract cannot be null");
		Objects.requireNonNull(serverContract, "serverContract cannot be null");
		if (!clientContract.getClientContractType().isCompatibleWith(serverContract.getServerContractType())) {
			throw new IllegalArgumentException(clientContract.getClientContractType().getTypeIdentifier()
					+ " is not compatible with " + serverContract.getServerContractType().getTypeIdentifier());
		}
		if (!clientContract.getClientContractType().equals(clientServerConnectorType.getClientContractType())) {
			throw new IllegalArgumentException("type of ClientServerContractType provided in clientContract ("
					+ clientContract.getClientContractType().getTypeIdentifier() + ") is different from connectorType");
		}
		if (!serverContract.getServerContractType().equals(clientServerConnectorType.getServerContractType())) {
			throw new IllegalArgumentException("type of ClientServerContractType provided in serverContract ("
					+ serverContract.getServerContractType().getTypeIdentifier() + ") is different from connectorType");
		}
		this.clientContract = clientContract;
		this.clientContract.setConnector(this);
		this.serverContract = serverContract;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return this.clientContract != null && this.serverContract != null
				&& clientContract.getClientContractType().isCompatibleWith(serverContract.getServerContractType());
	}

	/**
	 * computes the instance identifier as being the concatenation of the identifier
	 * of the client contract (cf. {@link ClientContract#getIdentifier()} and the
	 * identifier of the server contract (cf.
	 * {@link ServerContract#getIdentifier()}. Therefore, the identifier includes
	 * the identifier of the client microservice and the identifier of the server
	 * microservice.
	 * 
	 * @param clientContract the client contract.
	 * @param serverContract the server contract.
	 * @return the instance identifier.
	 */
	public static String computeIdentifier(final ClientContract clientContract, final ServerContract serverContract) {
		Objects.requireNonNull(clientContract, "client contract cannot be null");
		Objects.requireNonNull(serverContract, "server contract cannot be null");
		return clientContract.getIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ serverContract.getIdentifier();
	}

	/**
	 * obtains the collection of identifiers of the client and the server contracts.
	 * 
	 * @return the collection of identifiers.
	 */
	public List<String> getIdentifierOfAttachedContracts() {
		List<String> ids = new ArrayList<>();
		ids.add(clientContract.getIdentifier());
		ids.add(serverContract.getIdentifier());
		return ids;
	}

	/**
	 * obtains the client server connector type.
	 * 
	 * @return the type.
	 */
	public ClientServerConnectorType getClientServerConnectorType() {
		return (ClientServerConnectorType) super.getConnectorType();
	}

	/**
	 * obtains the client contract.
	 * 
	 * @return the client contract.
	 */
	public ClientContract getClientContract() {
		return clientContract;
	}

	/**
	 * obtains the server contract.
	 * 
	 * @return the server contract.
	 */
	public ServerContract getServerContract() {
		return serverContract;
	}

	@Override
	public String toString() {
		return "ClientServerConnector [identifier=" + getIdentifier() + ", clientServerConnectorType="
				+ getConnectorType() + ", clientContract=" + clientContract.getIdentifier() + ", serverContract="
				+ serverContract.getIdentifier() + "]";
	}
}
