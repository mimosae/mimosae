/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.filtermodel.ChannelBasedFilter;

/**
 * This class defines the concept of channel-based producer contract type. It
 * contains a channel-based event filter and a collection of accepted event
 * types. The event filter of a channel-based contract type is simply the name
 * of the channel: all the producers and consumers that have access to the
 * channel with this name can exchange messages on this channel cf.
 * {@link ChannelBasedFilter#isCompatibleWith(mimosae.modelatruntime.filtermodel.EventFilter)}).
 * 
 * @author Denis Conan
 */
public final class ChannelBasedProducerContractType extends ProducerContractType {
	/**
	 * the event filter (simply the name of the channel).
	 */
	private final ChannelBasedFilter eventFilter;
	/**
	 * the collection of event types that this producer contract type accepts.
	 */
	private final List<EventType> eventTypes;

	/**
	 * constructs a new channel-based producer contract type. The varargs argument
	 * is a collection of event types that may be published; the collection cannot
	 * be empty and cannot contain null references. Publish-subscribe connectors are
	 * added afterwards with method {@link #addConnectorType(PubSubConnectorType)}.
	 * 
	 * @param identifier  the identifier of the producer contract type.
	 * @param version     the version of the producer contract type.
	 * @param eventFilter the event filter of the producer contract type.
	 * @param eventTypes  the collection of event types that may be produced using
	 *                    the producer contract type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public ChannelBasedProducerContractType(final String identifier, final Semver version,
			final ChannelBasedFilter eventFilter, final EventType... eventTypes) throws MalformedIdentifierException {
		super(identifier, version);
		Objects.requireNonNull(eventFilter, "eventFilter cannot be null");
		Objects.requireNonNull(eventTypes, "eventTypes cannot be null");
		this.eventTypes = Collections.unmodifiableList(Arrays.asList(eventTypes));
		if (this.eventTypes.isEmpty() || this.eventTypes.contains(null)) {
			throw new IllegalArgumentException("eventTypes cannot be empty or with null element");
		}
		this.eventFilter = eventFilter;
		assert invariant();
	}

	/**
	 * constructs a new channel-based producer contract type by cloning an existing
	 * producer contract type. The identifiers are the same, i.e. the two objects
	 * are equal. The event filters are also the same. The collection of event types
	 * is a copy of the source one.
	 * <p>
	 * This constructor is called when constructing a new configuration type by
	 * cloning an existing configuration type: see
	 * {@link mimosae.modelatruntime.type.ConfigurationType#ConfigurationType(ConfigurationType)}.
	 * Publish-subscribe connectors are added afterwards with method
	 * {@link #addConnectorType(PubSubConnectorType)}.
	 * 
	 * @param origin the source producer contract type.
	 */
	public ChannelBasedProducerContractType(final ChannelBasedProducerContractType origin) {
		super(origin);
		this.eventFilter = origin.eventFilter;
		var temp = new ArrayList<EventType>();
		temp.addAll(origin.eventTypes);
		this.eventTypes = Collections.unmodifiableList(temp);
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return getTypeIdentifier().invariant() && eventFilter != null && eventTypes != null && !eventTypes.isEmpty();
	}

	/**
	 * obtains the collection of event types.
	 * 
	 * @return a copy of the collection of event types.
	 */
	public List<EventType> getEventTypes() {
		return eventTypes;
	}

	/**
	 * obtains the event filter.
	 * 
	 * @return the event filter.
	 */
	public ChannelBasedFilter getEventFilter() {
		return eventFilter;
	}

	/**
	 * states whether this producer contract type is compatible with the givien
	 * consumer contract type. Compatibility is computed as the conjunction of event
	 * filter compatibility and event types compatibility. Considering channel-based
	 * filtering, the two event filters are compatible when channel names are equal.
	 * Concerning the collection of event types, the collection of the producer
	 * contract type must be included in the collection of the consumer contract
	 * type, i.e. every event type published can be consumed.
	 * 
	 * @return true when the two contract types are compatible.
	 */
	@Override
	public boolean isCompatibleWith(final ConsumerContractType consumerContractType) {
		Objects.requireNonNull(consumerContractType, "consumerContractType cannot be null");
		if (!(consumerContractType instanceof ChannelBasedConsumerContractType)) {
			throw new IllegalArgumentException(
					"waiting for a " + ChannelBasedConsumerContractType.class.getCanonicalName() + ", but was "
							+ consumerContractType.getClass().getCanonicalName());
		}
		ChannelBasedConsumerContractType c = (ChannelBasedConsumerContractType) consumerContractType;
		assert invariant();
		return eventFilter.isCompatibleWith(c.getEventFilter())
				&& eventTypes.stream().allMatch(e1 -> c.getEventTypes().stream().anyMatch(e1::equals));
	}

	@Override
	public String toString() {
		return "ChannelBasedProducerContractType [getIdentifier()=" + getTypeIdentifier() + ", eventTypes=" + eventTypes
				+ ", eventFilter=" + eventFilter + "]";
	}
}
