/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.connnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import mimosae.modelatruntime.instance.contract.ConsumerContract;
import mimosae.modelatruntime.instance.contract.ProducerContract;
import mimosae.modelatruntime.type.connectortype.ConnectorType;
import mimosae.modelatruntime.type.connectortype.PubSubConnectorType;

/**
 * This class defines the concept of publish-subscribe connector instance.
 * Contrary to client-server or database contracts, an instance of this
 * connector corresponds to an architecture entity in the managed system, e.g.
 * an AMQP or a MQTT broker. When it has just been created, a publish-subscribe
 * connector has no producer contracts and no consumer contracts attached. This
 * is why the identifier of a publish-subscribe connector is computed from the
 * identifiers of the attached entities.
 * 
 * @author Denis Conan
 */
public final class PubSubConnector extends Connector {
	/**
	 * the collection of producer contracts.
	 */
	private final Map<String, ProducerContract> producerContracts;
	/**
	 * the collection of consumer contracts.
	 */
	private final Map<String, ConsumerContract> consumerContracts;

	/**
	 * constructs a new publish-subscribe connector instance. There is neither
	 * producer contracts nor consumer contracts attached.
	 * 
	 * @param identifier    the instance identifier.
	 * @param connectorType the connector type.
	 */
	public PubSubConnector(final String identifier, final ConnectorType connectorType) {
		super(identifier, connectorType);
		if (!(connectorType instanceof PubSubConnectorType)) {
			throw new IllegalArgumentException("connectorType is not a pub-sub connectorType ("
					+ connectorType.getClass().getCanonicalName() + ")");
		}
		this.producerContracts = new HashMap<>();
		this.consumerContracts = new HashMap<>();
		assert invariant();
	}

	/**
	 * constructs a new publish-subscribe connector by cloning a source connector.
	 * The identifier of the new connector is equal to the identifier of the source
	 * connector; hence, the two connectors are equal. The connector type of the two
	 * connectors are the same object. The new connector has no attached contracts:
	 * i.e. the producer contracts and the consumer contracts of the source
	 * connector are neither copied nor attached to the new connector.
	 * 
	 * @param origin the source publish-subscribe connector.
	 */
	public PubSubConnector(final PubSubConnector origin) {
		this(origin.getIdentifier(), origin.getConnectorType());
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return producerContracts != null && consumerContracts != null;
	}

	/**
	 * obtains the collection of contract identifiers, i.e. of the set of producer
	 * and consumer contracts.
	 * 
	 * @return the collection of contract identifiers.
	 */
	public List<String> getIdentifierOfAttachedContracts() {
		List<String> ids = new ArrayList<>();
		ids.addAll(producerContracts.keySet());
		ids.addAll(consumerContracts.keySet());
		return ids;
	}

	/**
	 * adds a producer contract to the collection of producer contracts that are
	 * attached to the publish-subscribe connector.
	 * 
	 * @param producerContract the producer contract to add.
	 */
	public void addProducerContract(final ProducerContract producerContract) {
		Objects.requireNonNull(producerContract, "producerContract cannot be null");
		if (producerContracts.get(producerContract.getIdentifier()) != null) {
			throw new IllegalArgumentException(producerContract + " already in producerContracts");
		}
		producerContracts.put(producerContract.getIdentifier(), producerContract);
		producerContract.addConnector(this);
	}

	/**
	 * adds a consumer contract to the collection of consumer contracts attached to
	 * the publish-subscribe connector.
	 * 
	 * @param consumerContract the consumer contract to add.
	 */
	public void addConsumerContract(final ConsumerContract consumerContract) {
		Objects.requireNonNull(consumerContract, "consumerContract cannot be null");
		if (consumerContracts.get(consumerContract.getIdentifier()) != null) {
			throw new IllegalArgumentException(consumerContract + " already in consumerContracts");
		}
		consumerContracts.put(consumerContract.getIdentifier(), consumerContract);
	}

	/**
	 * removes a producer contract from the collection of producer contract attached
	 * to the publish-subscribe connector.
	 * 
	 * @param producerContract the producer contract to remove.
	 */
	public void removeProducerContract(final ProducerContract producerContract) {
		Objects.requireNonNull(producerContract, "producerContract cannot be null");
		if (producerContracts.get(producerContract.getIdentifier()) == null) {
			throw new IllegalArgumentException(producerContract + " does not exists in producerContracts");
		}
		producerContracts.remove(producerContract.getIdentifier());
		producerContract.removeConnector(this);
	}

	/**
	 * removes a consumer contract from the collection of consumer contract attached
	 * to the publish-subscribe connector.
	 * 
	 * @param consumerContract the consumer contract to remove.
	 */
	public void removeConsumerContract(final ConsumerContract consumerContract) {
		Objects.requireNonNull(consumerContract, "consumerContract cannot be null");
		if (consumerContracts.get(consumerContract.getIdentifier()) == null) {
			throw new IllegalArgumentException(consumerContract + " does not existsin consumerContracts");
		}
		consumerContracts.remove(consumerContract.getIdentifier());
	}

	/**
	 * obtains the publish-subscribe connector type.
	 * 
	 * @return the publish-subscribe connector type.
	 */
	public PubSubConnectorType getPubSubConnectorType() {
		return (PubSubConnectorType) super.getConnectorType();
	}

	/**
	 * obtains the producer contract matching a given identifier.
	 * 
	 * @param identifier the identifier of the producer contract.
	 * @return the producer contract searched for.
	 */
	public ProducerContract getProducerContract(final String identifier) {
		return producerContracts.get(identifier);
	}

	/**
	 * obtains the consumer contract matching a given identifier.
	 * 
	 * @param identifier the identifier of the consumer contract.
	 * @return the consumer contract searched for.
	 */
	public ConsumerContract getConsumerContract(final String identifier) {
		return consumerContracts.get(identifier);
	}

	/**
	 * obtains the collection of consumer contracts. There is no copy.
	 * 
	 * @return the collection of consumer contracts.
	 */
	public Map<String, ProducerContract> getProducerContracts() {
		return producerContracts;
	}

	/**
	 * obtains the collection of consumer contracts. There is no copy.
	 * 
	 * @return the collection of consumer contracts.
	 */
	public Map<String, ConsumerContract> getConsumerContracts() {
		return consumerContracts;
	}

	/**
	 * states whether one of the producer contracts of the publish-subscribe
	 * connectors is attached to at least one of the consumer contracts of the
	 * publish-subscribe connector that is compatible. This method is called when
	 * stating whether the microservice that contains the given producer contract is
	 * deployable, i.e. if the producer contract must have consumer contracts, there
	 * is at least one compatible consumers attached to one of the publish-subscribe
	 * connectors of the given producer contract.
	 * 
	 * @param producerContract the producer contract.
	 * @return true when there is at least one compatible consumer contract in this
	 *         publish-subscribe connector.
	 */
	public boolean hasAtLeastOneCompatibleConsumerContract(final ProducerContract producerContract) {
		return consumerContracts.values().stream().map(ConsumerContract::getConsumerContractType)
				.anyMatch(cct -> producerContract.getProducerContractType().isCompatibleWith(cct));
	}

	@Override
	public String toString() {
		return "PubSubServerConnector [identifier=" + getIdentifier() + ", pubSubConnectorType="
				+ getPubSubConnectorType().getTypeIdentifier() + ", producerContracts="
				+ producerContracts.keySet().stream().collect(Collectors.joining(",", "[", "]"))
				+ ", consumerContracts="
				+ consumerContracts.keySet().stream().collect(Collectors.joining(",", "[", "]")) + "]";
	}
}
