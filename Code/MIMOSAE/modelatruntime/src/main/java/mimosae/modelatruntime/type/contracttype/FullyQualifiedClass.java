/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Objects;

/**
 * When the type of a parameter or of the return value of the operation is not a
 * primitive type, we propose this class for specifying the name of a JAVA
 * class.
 * <p>
 * Be careful: The existence of the class is not checked.
 * 
 * @author Denis Conan
 */
public final class FullyQualifiedClass implements OperationType {
	/**
	 * the name of the class of the type.
	 */
	private final String className;

	/**
	 * constructs a new fully qualified class operation type.
	 * 
	 * @param className the name of the class.
	 */
	public FullyQualifiedClass(final String className) {
		Objects.requireNonNull(className, "className cannot be null");
		if (className.isBlank()) {
			throw new IllegalArgumentException("className cannot be blank");
		}
		this.className = className;
	}

	/**
	 * obtains the name of the class.
	 * 
	 * @return the name of the class.
	 */
	public String getClassName() {
		return className;
	}

	@Override
	public int hashCode() {
		return Objects.hash(className);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof FullyQualifiedClass)) {
			return false;
		}
		FullyQualifiedClass other = (FullyQualifiedClass) obj;
		return Objects.equals(className, other.className);
	}

	@Override
	public String toString() {
		return "FullyQualifiedClass [className=" + className + "]";
	}

}
