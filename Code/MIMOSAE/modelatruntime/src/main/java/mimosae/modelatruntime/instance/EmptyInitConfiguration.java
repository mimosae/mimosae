/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance;

import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.type.EmptyInitConfigurationType;

/**
 * This class defines the first configuration that is created at the beginning
 * of the system. It is empty and its identifier is not a SHA-1 digest, but the
 * string {@link #IDENTIFIER_EMPTY_INIT_CONFIGURATION}. This class implements
 * the Singleton design pattern.
 * 
 * @author Denis Conan
 */
public final class EmptyInitConfiguration extends Configuration {
	/**
	 * the unique instance of the Singleton design pattern.
	 */
	private static EmptyInitConfiguration instance;
	/**
	 * the identifier of the instance.
	 */
	public static final String IDENTIFIER_EMPTY_INIT_CONFIGURATION = "empty_init_configuration";

	/**
	 * is private to conform to the Singleton desing pattern. Cf. factory method
	 * {@link #getEmptyInitConfiguration()}.
	 */
	private EmptyInitConfiguration() {
		super(EmptyInitConfigurationType.getEmptyInitConfigurationType());
		try {
			setIdentifier(IDENTIFIER_EMPTY_INIT_CONFIGURATION);
		} catch (IsCommittedAndCannotBeModifiedException e) {
			throw new IllegalStateException("configuration " + IDENTIFIER_EMPTY_INIT_CONFIGURATION
					+ " is already committed: " + e.getLocalizedMessage());
		}
		assert getIdentifier() != null;
	}

	/**
	 * creates and obtains the unique instance of this class.
	 * 
	 * @return the unique instance.
	 */
	public static EmptyInitConfiguration getEmptyInitConfiguration() {
		if (instance == null) {
			try {
				instance = new EmptyInitConfiguration();
				instance.setIdentifier(IDENTIFIER_EMPTY_INIT_CONFIGURATION);
			} catch (IsCommittedAndCannotBeModifiedException e) {
				throw new IllegalStateException("configuration " + IDENTIFIER_EMPTY_INIT_CONFIGURATION
						+ " is already committed: " + e.getLocalizedMessage());
			}
		}
		return instance;
	}

	@Override
	public String toString() {
		return "EmptyInitConfiguration [identifier=" + getIdentifier() + "]";
	}
}
