/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.type.connectortype.PubSubConnectorType;

/**
 * This abstract class defines the root concept of producer contract type. It is
 * a kind of publish-subscribe contract type, namely the producer part. It is
 * identified by its identifier and its version. The class specifies the
 * collection of connector types to which this contract type can be connected.
 * It is possible to specify that this contract must be connected to a connector
 * type that contains consumer types that are compatible with this producer
 * type, i.e. a microservice type having this producer contract type cannot
 * operate in degraded mode when the event types published cannot be consumed.
 * 
 * @author Denis Conan
 */
public abstract class ProducerContractType extends PubSubContractType {
	/**
	 * specifies whether this producer contract type must have consumer contract
	 * types.
	 */
	private boolean mustHaveConsumers;
	/**
	 * the collection of publish-subscribe connector to which this contract type is
	 * connected.
	 */
	private final List<PubSubConnectorType> pubSubConnectorTypes;

	/**
	 * constructs a new producer contract type that is not connected to any
	 * connector type. Publish-subscribe connectors are added afterwards with method
	 * {@link #addConnectorType(PubSubConnectorType)}.
	 * 
	 * @param identifier the identifier of the type.
	 * @param version    the version of the type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	protected ProducerContractType(final String identifier, final Semver version) throws MalformedIdentifierException {
		super(identifier, version);
		pubSubConnectorTypes = new ArrayList<>();
	}

	/**
	 * constructs a new producer contract type by cloning, except the collection of
	 * connector types, i.e. the new producer contract type is not connected to any
	 * connector type. The two identifiers and the two versions are the same, and
	 * hence the two objects are equal.
	 * <p>
	 * This constructor is called when constructing a new configuration type by
	 * cloning an existing configuration type: see
	 * {@link mimosae.modelatruntime.type.ConfigurationType#ConfigurationType(ConfigurationType)}.
	 * Publish-subscribe connectors are added afterwards with method
	 * {@link #addConnectorType(PubSubConnectorType)}.
	 * 
	 * @param origin the source producer contract type.
	 */
	protected ProducerContractType(final ProducerContractType origin) {
		super(origin);
		this.mustHaveConsumers = origin.mustHaveConsumers;
		pubSubConnectorTypes = new ArrayList<>();
	}

	/**
	 * adds a connector type to this producer contract type.
	 * 
	 * @param connectorType the connector type.
	 */
	public void addConnectorType(final PubSubConnectorType connectorType) {
		Objects.requireNonNull(connectorType, "connectorType cannot be null");
		if (pubSubConnectorTypes.contains(connectorType)) {
			throw new IllegalArgumentException(connectorType + " already in pubSubConnectorTypes");
		}
		this.pubSubConnectorTypes.add(connectorType);
	}

	/**
	 * obtains the boolean that states whether this producer contract type must have
	 * consumers that can consume its event types.
	 * 
	 * @return true when the producer contract type must have consumers.
	 */
	public boolean getMustHaveConsumers() {
		return mustHaveConsumers;
	}

	/**
	 * sets the boolean that states whether this producer contract type must have
	 * consumers that can consume its event types.
	 * 
	 * @param mustBeConnected the new value.
	 */
	public void setMustHaveConsumers(final boolean mustBeConnected) {
		this.mustHaveConsumers = mustBeConnected;
	}

	/**
	 * obtains the collection of publish-subscribe of connector types.
	 * 
	 * @return the collection of connectors.
	 */
	public List<PubSubConnectorType> getPubSubConnectorTypes() {
		return pubSubConnectorTypes;
	}

	/**
	 * states whether this producer contract type is instantiable, i.e. if it must
	 * have consumer types then there exists at least a consumer type of the
	 * connector types that is compatible with this producer contract type.
	 * 
	 * @return true when it is instantiable.
	 */
	public final boolean isInstantiable() {
		return !getMustHaveConsumers() || (!getPubSubConnectorTypes().isEmpty()) && getPubSubConnectorTypes().stream()
				.anyMatch(pubsub -> pubsub.hasAtLeastOneCompatibleConsumerContractType(this));
	}

	/**
	 * states whether this producer contract type is compatible with the given
	 * consumer contract type.
	 * 
	 * @param consumerContractType the given consumer contract type.
	 * @return true when it is compatible with the given consumer contract type.
	 */
	public abstract boolean isCompatibleWith(ConsumerContractType consumerContractType);
}
