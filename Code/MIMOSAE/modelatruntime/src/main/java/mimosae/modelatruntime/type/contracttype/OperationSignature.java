/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * This class models the operation signature, which is decomposed of the name of
 * the operation and the list of argument types. See Section&nbsp;<a href=
 * "https://docs.oracle.com/javase/specs/jls/se7/html/jls-8.html#jls-8.4.2">8.4.2.
 * Method Signature</a> of JAVA specification.
 * 
 * @author Denis Conan
 */
public final class OperationSignature {
	/**
	 * the name of the operation signature.
	 */
	private final String operationName;
	/**
	 * the argument types, i.e. the collection of operation types.
	 */
	private final List<OperationType> argumentTypes;

	/**
	 * constructs a new operation signature.
	 * 
	 * @param name         the name of the operation signature.
	 * @param argumentType the collection of operation types.
	 */
	public OperationSignature(final String name, final OperationType... argumentType) {
		Objects.requireNonNull(name, "name cannot be null");
		if (name.isBlank()) {
			throw new IllegalArgumentException("name cannot be blank");
		}
		this.operationName = name;
		this.argumentTypes = Collections.unmodifiableList(Arrays.asList(argumentType));
		if (this.argumentTypes.contains(null)) {
			throw new IllegalArgumentException("arguments cannot include null element");
		}
	}

	/**
	 * obtains the operation name.
	 * 
	 * @return the operation name.
	 */
	public String getOperationName() {
		return operationName;
	}

	/**
	 * obtains the arguments of the operation signature, i.e. the collection of
	 * operation types.
	 * 
	 * @return the arguments.
	 */
	public List<OperationType> getArgumentTypes() {
		return argumentTypes;
	}

	@Override
	public int hashCode() {
		return Objects.hash(argumentTypes, operationName);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof OperationSignature)) {
			return false;
		}
		OperationSignature other = (OperationSignature) obj;
		return Objects.equals(argumentTypes, other.argumentTypes) && Objects.equals(operationName, other.operationName);
	}

	@Override
	public String toString() {
		return "OperationSignature [name=" + operationName + ", argumentTypes=" + argumentTypes + "]";
	}

}
