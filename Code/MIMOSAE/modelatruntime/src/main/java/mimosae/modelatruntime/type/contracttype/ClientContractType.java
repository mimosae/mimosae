/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.type.connectortype.ClientServerConnectorType;

/**
 * This class defines the concept of client contract type. When included in a
 * microservice type, the attribute {@link #mustHaveConnectorAttached}
 * stipulates that this contract type must be attached to a client-server
 * connector type, i.e. the microservice type cannot operate without this client
 * contract type accessing a server contract type that offers the set of
 * required operations that are declared into this client contract type.
 * <p>
 * Method {@link #isCompatibleWith(ClientServerContractType)} is for checking
 * that the collection of operation declarations of a server contract type
 * includes the collection of operation declarations of this contract type.
 * <p>
 * Method {@link #isInstantiable()} is for checking the condition
 * "{@link #mustHaveConnectorAttached} implies
 * {@link #clientServerConnectorType} is not null".
 * 
 * @author Denis Conan
 */
public final class ClientContractType extends ClientServerContractType {
	/**
	 * boolean stating whether this client contract type must have a connector type
	 * attached.
	 */
	private boolean mustHaveConnectorAttached;
	/**
	 * the client-server connector type.
	 */
	private ClientServerConnectorType clientServerConnectorType;

	/**
	 * constructs a new client contract type. The client contract type is identified
	 * by its identifier and its version. The varargs argument is a collection of
	 * operation declarations that are required; the collection cannot be empty and
	 * cannot contain null references. By default, the client contract type must not
	 * have a connector type attached.
	 * 
	 * @param identifier            the identifier of the type.
	 * @param version               the version of the type.
	 * @param operationDeclarations the collection of operation declarations.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public ClientContractType(final String identifier, final Semver version,
			final OperationDeclaration... operationDeclarations) throws MalformedIdentifierException {
		super(identifier, version, operationDeclarations);
		this.mustHaveConnectorAttached = false;
		assert invariant();
	}

	/**
	 * constructs a new client contract type. This constructor allows specifying
	 * whether the client contract type must not have a connector type attached; the
	 * connector type is set afterwards by using method
	 * {@link #setConnectorType(ClientServerConnectorType)}. The client contract
	 * type is identified by its identifier and its version. The varargs argument is
	 * a collection of operation declarations that are required; the collection
	 * cannot be empty and cannot contain null references.
	 * 
	 * @param identifier            the identifier of the type.
	 * @param version               the version of the type.
	 * @param mustBeConnected       the boolean stating whether the client contract
	 *                              type must not have a connector type attached.
	 * @param operationDeclarations the collection of operation declarations.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public ClientContractType(final String identifier, final Semver version, final boolean mustBeConnected,
			final OperationDeclaration... operationDeclarations) throws MalformedIdentifierException {
		this(identifier, version, operationDeclarations);
		this.mustHaveConnectorAttached = mustBeConnected;
		assert invariant();
	}

	/**
	 * constructs a new client contract type by cloning an existing client contract
	 * type. The type identifiers are the same, i.e. the two objects are equal. The
	 * collection of operation declarations is copied. The collection of operation
	 * declarations is unmodifiable.
	 * 
	 * @param origin the source client contract type.
	 */
	public ClientContractType(final ClientContractType origin) {
		super(origin);
		this.mustHaveConnectorAttached = origin.mustHaveConnectorAttached;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * <p>
	 * Since setting {@link #mustHaveConnectorAttached} and
	 * {@link #clientServerConnectorType} are not done in the same method, the
	 * condition "mustHaveConnectorAttached implies clientServerConnectorType is not
	 * null" is not part of the invariant.
	 * 
	 * @return true when the invariant holds.
	 */
	@Override
	public boolean invariant() {
		return super.invariant();
	}

	/**
	 * sets the connector type.
	 * 
	 * @param connectorType the connector type.
	 */
	public void setConnectorType(final ClientServerConnectorType connectorType) {
		Objects.requireNonNull(connectorType, "connectorType cannot be null");
		if (clientServerConnectorType != null) {
			throw new IllegalArgumentException(connectorType + " already assigned");
		}
		this.clientServerConnectorType = connectorType;
		assert invariant();
	}

	/**
	 * obtains the value of the attribute {@link #mustHaveConnectorAttached}.
	 * 
	 * @return the boolean value.
	 */
	public boolean getMustBeConnected() {
		return mustHaveConnectorAttached;
	}

	/**
	 * sets the value of the attribute {@link #mustHaveConnectorAttached}.
	 * 
	 * @param mustBeConnected the new value.
	 */
	public void setMustHaveConnectorAttached(final boolean mustBeConnected) {
		this.mustHaveConnectorAttached = mustBeConnected;
		assert invariant();
	}

	/**
	 * obtains the client-server connector type.
	 * 
	 * @return the client-server connector type.
	 */
	public ClientServerConnectorType getClientServerConnectorType() {
		return clientServerConnectorType;
	}

	/**
	 * states whether the collection of operation declarations of a given server
	 * contract type includes the collection of operation declarations of this
	 * contract type.
	 * 
	 * @param serverContractType the given server contract type.
	 * @return true if the server contract type fulfill all the required operations
	 *         of this client contract type.
	 */
	public boolean isCompatibleWith(final ClientServerContractType serverContractType) {
		Objects.requireNonNull(serverContractType, "serverContractType cannot be null");
		if (!(serverContractType instanceof ServerContractType)) {
			throw new IllegalArgumentException("waiting for a " + ServerContractType.class.getCanonicalName()
					+ ", but was " + serverContractType.getClass().getCanonicalName());
		}
		ServerContractType c = (ServerContractType) serverContractType;
		assert invariant();
		return this.getOperationDeclarations().stream()
				.allMatch(e1 -> c.getOperationDeclarations().stream().anyMatch(e1::isCompatibleWith));
	}

	/**
	 * checks the condition "{@link #mustHaveConnectorAttached} implies
	 * {@link #clientServerConnectorType} is not null".
	 * 
	 * @return the boolean result of the condition.
	 */
	public boolean isInstantiable() {
		return !mustHaveConnectorAttached || clientServerConnectorType != null;
	}

	@Override
	public String toString() {
		return "ClientContractType [getIdentifier()=" + getTypeIdentifier() + ", getOperationDeclarations()="
				+ getOperationDeclarations() + ", mustBeConnected=" + mustHaveConnectorAttached
				+ ", clientServerConnectorType="
				+ ((clientServerConnectorType == null) ? "null"
						: " connected to server contract type "
								+ clientServerConnectorType.getServerContractType().getTypeIdentifier())
				+ "]";
	}
}
