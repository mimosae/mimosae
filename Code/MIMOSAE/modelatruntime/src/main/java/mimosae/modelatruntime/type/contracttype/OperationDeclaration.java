/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Objects;
import java.util.Optional;

import mimosae.modelatruntime.exception.MalformedOperationDeclarationException;

/**
 * This class defines the declaration of an operation. It is composed of a
 * signature and a return type. See Section&nbsp;<a href=
 * "https://docs.oracle.com/javase/specs/jls/se7/html/jls-8.html#jls-8.4.2">8.4.2.
 * Method Signature</a> of JAVA specification.
 * 
 * @author Denis Conan
 */
public final class OperationDeclaration {
	/**
	 * the signature of the operation.
	 */
	private final OperationSignature signature;
	/**
	 * the return type of the operation. {@link Optional#isEmpty()} is used for
	 * {@code void}.
	 */
	private final Optional<OperationType> returnType;

	/**
	 * constructs a new operation declaration. The return type can be {@code null},
	 * i.e. no return type ({@code void}).
	 * 
	 * @param signature  the signature.
	 * @param returnType the return type.
	 */
	public OperationDeclaration(final OperationSignature signature, final OperationType returnType) {
		Objects.requireNonNull(signature, "signature cannot be null");
		this.signature = signature;
		this.returnType = Optional.ofNullable(returnType);
	}

	/**
	 * obtains the signature.
	 *
	 * @return the signature.
	 */
	public OperationSignature getSignature() {
		return signature;
	}

	/**
	 * obtains the return type. The return value is {@link Optional#isEmpty()} when
	 * {@code void}.
	 * 
	 * @return the return type.
	 */
	public Optional<OperationType> getReturnType() {
		return returnType;
	}

	/**
	 * states whether this operation declaration is compatible with a given
	 * operation declaration. Compatibility is here equality.
	 * 
	 * @param declaration the given operation declaration.
	 * @return true when they are equal.
	 */
	public boolean isCompatibleWith(final OperationDeclaration declaration) {
		Objects.requireNonNull(declaration, "declaration cannot be null");
		return this.equals(declaration);
	}

	/**
	 * parses a string to construct an operation declaration. The string is of the
	 * form "{@code name paramType... : returnType}".
	 * 
	 * @param stringToParse the string to parse.
	 * @return the operation declaration.
	 * @throws MalformedOperationDeclarationException the string is malformed.
	 */
	public static OperationDeclaration parseOperationDeclaration(final String stringToParse)
			throws MalformedOperationDeclarationException {
		String[] firstParsing = stringToParse.toLowerCase().trim().split(":");
		OperationType returnType = null;
		if (firstParsing.length == 1) {
			returnType = null;
		} else if (firstParsing.length == 2) {
			returnType = OperationType.parseOperationType(firstParsing[1].trim());
		} else {
			throw new MalformedOperationDeclarationException(
					"operation declaration = \"name paramType... : returnType\" (was " + stringToParse);
		}
		var stringSignature = firstParsing[0].trim();
		String[] strings = stringSignature.toLowerCase().trim().split("\\s+");
		if (firstParsing.length == 0) {
			throw new MalformedOperationDeclarationException(
					"operation declaration = \"name paramType... : returnType\" (was " + stringToParse);
		}
		String operationName = strings[0];
		var argumentTypes = new OperationType[strings.length - 1];
		for (var i = 1; i < strings.length; i++) {
			argumentTypes[i - 1] = OperationType.parseOperationType(strings[i].trim());
		}
		return new OperationDeclaration(new OperationSignature(operationName, argumentTypes), returnType);
	}

	@Override
	public int hashCode() {
		return Objects.hash(returnType, signature);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof OperationDeclaration)) {
			return false;
		}
		OperationDeclaration other = (OperationDeclaration) obj;
		return Objects.equals(returnType, other.returnType) && Objects.equals(signature, other.signature);
	}

	@Override
	public String toString() {
		return "OperationDeclaration [signature=" + signature + ", returnType=" + returnType + "]";
	}
}
