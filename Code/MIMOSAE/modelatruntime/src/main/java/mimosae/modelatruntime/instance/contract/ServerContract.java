/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.contract;

import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.instance.Microservice;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;

/**
 * This class defines the concept of server contract instance. It has an
 * identifier; it conforms to a server contract type; it can be connected to a
 * client contract via a client-server connector without being aware of the
 * connection (no reference to the connector in this class); and it belongs to a
 * microservice instance.
 * <p>
 * The server contract is identified by an instance identifier that is the
 * concatenation of the server contract type identifier with the instance
 * identifier of the microservice to which it belongs.
 * 
 * @author Denis Conan
 */
public final class ServerContract {
	/**
	 * the instance identifier. It is the concatenation of the server contract type
	 * identifier with the instance identifier of the microservice.
	 */
	private final String identifier;
	/**
	 * the server contract type to which it conforms.
	 */
	private ServerContractType serverContractType;
	/**
	 * the microservice to which it belongs.
	 */
	private final Microservice microservice;

	/**
	 * constructs a new server contract. This new server contract is created when
	 * creating the corresponding microservice, i.e. the new object is added to the
	 * collection of server contracts in the calling constructor of the microservice
	 * afterwards (Cf.
	 * {@link Microservice#Microservice(String, mimosae.modelatruntime.type.MicroserviceType)}).
	 * 
	 * @param serverContractType the server contract type it conforms to.
	 * @param microservice       the microservice to which it belongs.
	 */
	public ServerContract(final ServerContractType serverContractType, final Microservice microservice) {
		Objects.requireNonNull(serverContractType, "serverContractType cannot be null");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, serverContractType);
		this.serverContractType = serverContractType;
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * constructs a new server contract by cloning a source server contract. The new
	 * server contract belongs to a microservice that is different from the
	 * microservice of the source server contract. The identifiers of the source and
	 * the new objects are different, and thus the source and new objects are not
	 * equal. The server contract types are the same.
	 * <p>
	 * This constructor is called when creating the corresponding microservice by
	 * cloning, i.e. the new object is added to the collection of server contracts
	 * in the calling constructor of the new microservice afterwards. Cf.
	 * {@link Microservice#Microservice(Microservice)}.
	 * 
	 * @param origin       the source server contract.
	 * @param microservice the microservice to which the new server contract
	 *                     belongs.
	 */
	public ServerContract(final ServerContract origin, final Microservice microservice) {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		this.identifier = computeIdentifier(microservice, origin.serverContractType);
		this.serverContractType = origin.serverContractType;
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * constructs a new server contract by cloning a source server contract. The new
	 * server contract belongs to a microservice that is different from the
	 * microservice of the source server contract. The configuration type is also
	 * new. The identifiers of the source and the new objects are different, and
	 * thus the source and new objects are not equal. The server contract type must
	 * belong to the new configuration type. The server contract type of the new
	 * server contract and the server contract type of the source server contract
	 * are different objects, but they are equal.
	 * <p>
	 * The server contract type of the source server contract must belong to the new
	 * configuration type. Similarly, the microservice type of the microservice must
	 * belong to the new configuration type.
	 * <p>
	 * This constructor is called when creating the corresponding microservice by
	 * cloning, i.e. the new object is added to the collection of server contracts
	 * in the calling constructor of the new microservice afterwards. Cf.
	 * {@link Microservice#Microservice(Microservice, ConfigurationType)}.
	 * 
	 * @param origin            the source server contract.
	 * @param microservice      the microservice to which the new server contract
	 *                          belongs.
	 * @param configurationType the new configuration type.
	 * @throws UnknownTypeException one of the types is unknown in the new
	 *                              configuration type.
	 */
	public ServerContract(final ServerContract origin, final Microservice microservice,
			final ConfigurationType configurationType) throws UnknownTypeException {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(configurationType, "configurationType cannot be null");
		var newServerContractType = configurationType
				.getServerContractType(origin.serverContractType.getTypeIdentifier());
		if (newServerContractType == null) {
			throw new UnknownTypeException("server contract type " + origin.serverContractType.getTypeIdentifier()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		if (configurationType.getMicroserviceType(microservice.getMicroserviceType().getTypeIdentifier()) == null) {
			throw new UnknownTypeException("microservice type " + microservice.getMicroserviceType()
					+ " does not exist in configuration type " + configurationType.getIdentifier());
		}
		serverContractType = newServerContractType;
		this.identifier = computeIdentifier(microservice, serverContractType);
		this.microservice = microservice;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return the invariant holds.
	 */
	public boolean invariant() {
		return identifier != null && !identifier.isBlank() && serverContractType != null && microservice != null;
	}

	/**
	 * computes the identifier of the server contract as being the concatenation of
	 * the server contract type identifier with the instance identifier of the
	 * microservice.
	 * 
	 * @param microservice       the microservice instance to which this server
	 *                           contract belongs.
	 * @param serverContractType the server contract type.
	 * @return the instance identifier.
	 */
	public static String computeIdentifier(final Microservice microservice,
			final ServerContractType serverContractType) {
		Objects.requireNonNull(microservice, "microservice cannot be null");
		Objects.requireNonNull(serverContractType, "serverContractType cannot be null");
		return serverContractType.getTypeIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getIdentifier() + Constants.WORD_SEPARATOR_FOR_IDENTIFIERS
				+ microservice.getMicroserviceType().getTypeIdentifier();
	}

	/**
	 * obtains the instance identifier of the server contract. The identifier of the
	 * server contract is the concatenation of the server contract type identifier
	 * with the instance identifier of the microservice.
	 * 
	 * @return the instance identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL identifier of the instance identifier. Characters "/" are
	 * replaced by strings "ZZ". The PDDL identifier is used in PDDL domain file.
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return identifier.replace("/", "ZZ");
	}

	/**
	 * obtains the server contract type to which it conforms.
	 * 
	 * @return the server contract type.
	 */
	public ServerContractType getServerContractType() {
		return serverContractType;
	}

	/**
	 * obtains the microservice to which the server contract belongs.
	 * 
	 * @return the microservice.
	 */
	public Microservice getMicroservice() {
		return microservice;
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ServerContract)) {
			return false;
		}
		ServerContract other = (ServerContract) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "ServerContract [identifier=" + identifier + ", serverContractType=" + serverContractType + "]";
	}
}
