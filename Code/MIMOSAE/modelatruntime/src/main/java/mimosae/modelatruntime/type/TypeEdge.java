/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.jgrapht.graph.DefaultEdge;

import mimosae.modelatruntime.ActionLog;

/**
 * The implementation for edges in the graph of configuration types. The edge
 * collects the log of actions made when building the target configuration type
 * from the source configuration type: see {@link ActionLog}.
 * 
 * @author denis
 *
 */
public final class TypeEdge extends DefaultEdge {
	/**
	 * the serial version unique identifier for serialization.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * the list of action logs.
	 */
	private List<ActionLog> logs;

	/**
	 * constructs a new edge. The log is empty.
	 */
	public TypeEdge() {
		super();
		logs = new ArrayList<>();
	}

	/**
	 * constructs a new edge with the given log of actions. The list of action logs
	 * is copied from the given list and the list is then unmodifiable.
	 * 
	 * @param logs the action logs.
	 */
	public TypeEdge(final List<ActionLog> logs) {
		super();
		Objects.requireNonNull(logs, "logs cannot be null");
		var l = new ArrayList<ActionLog>();
		l.addAll(logs);
		this.logs = Collections.unmodifiableList(l);
	}

	/**
	 * obtains the action logs. The list is provided as is because it is
	 * unmodifiable.
	 * 
	 * @return the action logs.
	 */
	public List<ActionLog> getLogs() {
		return logs;
	}

	/**
	 * sets the action logs. The given list is copied and the copy is made
	 * unmodifiable.
	 * 
	 * @param logs the action logs.
	 */
	public void setLogs(final List<ActionLog> logs) {
		var l = new ArrayList<ActionLog>();
		l.addAll(logs);
		this.logs = Collections.unmodifiableList(l);
	}

	@Override
	public String toString() {
		return "TypeEdge [source=" + getSource() + ", target=" + getTarget() + ", logs=" + logs + "]";
	}

}
