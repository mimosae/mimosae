/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.Objects;
import java.util.Optional;

/**
 * The enumeration type of primitive types for the operation types. The
 * enumerator corresponds to JAVA primitive types.
 * 
 * @author Denis Conan
 */
public enum PrimitiveType implements OperationType {
	/**
	 * primitive type {@link Boolean}.
	 */
	BOOLEAN("boolean"),
	/**
	 * primitive type {@link Byte}.
	 */
	BYTE("byte"),
	/**
	 * primitive type {@link Character}.
	 */
	CHAR("char"),
	/**
	 * primitive type {@link Short}.
	 */
	SHORT("short"),
	/**
	 * primitive type {@link Integer}.
	 */
	INT("int"),
	/**
	 * primitive type {@link Long}.
	 */
	LONG("long"),
	/**
	 * primitive type {@link Float}.
	 */
	FLOAT("float"),
	/**
	 * primitive type {@link Double}.
	 */
	DOUBLE("double");

	/**
	 * the name of the primitive type.
	 */
	private final String name;

	/**
	 * constructs a new primitive type.
	 * 
	 * @param name the name
	 */
	PrimitiveType(final String name) {
		Objects.requireNonNull(name, "name cannot be null");
		if (name.isBlank()) {
			throw new IllegalArgumentException("name cannot be blank");
		}
		this.name = name;
	}

	/**
	 * obtains the name of the primitive type.
	 * 
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * parses a string to extract the name of a primitive type.
	 * 
	 * @param stringToParse the string to parse.
	 * @return the operation type or {@link Optional#isPresent()} when unknown
	 *         primitive type.
	 */
	public static Optional<OperationType> parsePrimitiveType(final String stringToParse) {
		if (stringToParse.equals(BOOLEAN.name)) {
			return Optional.of(BOOLEAN);
		} else if (stringToParse.equals(BYTE.name)) {
			return Optional.of(BYTE);
		} else if (stringToParse.equals(CHAR.name)) {
			return Optional.of(CHAR);
		} else if (stringToParse.equals(SHORT.name)) {
			return Optional.of(SHORT);
		} else if (stringToParse.equals(INT.name)) {
			return Optional.of(INT);
		} else if (stringToParse.equals(LONG.name)) {
			return Optional.of(LONG);
		} else if (stringToParse.equals(FLOAT.name)) {
			return Optional.of(FLOAT);
		} else if (stringToParse.equals(DOUBLE.name)) {
			return Optional.of(DOUBLE);
		} else {
			return Optional.empty();
		}
	}

	@Override
	public String toString() {
		return name;
	}
}
