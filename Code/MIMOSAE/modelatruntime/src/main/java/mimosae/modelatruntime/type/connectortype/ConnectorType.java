/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.connectortype;

/**
 * This interface defines the connector type. The only method shared by all the
 * connector types are to obtain the PDDL identifier of the connector type. Note
 * that not all the connector types have a type identifier, e.g. client-server
 * connector types and database connector types; hence, the PDDL identifier of
 * these connector types is built using the PDDL identifiers of the connected
 * entities.
 * 
 * @author Denis Conan
 */
public interface ConnectorType {
	/**
	 * obtainst the PDDL identifier of the connector type.
	 * 
	 * @return the PDDL identifier.
	 */
	String getPDDLIdentifier();
}
