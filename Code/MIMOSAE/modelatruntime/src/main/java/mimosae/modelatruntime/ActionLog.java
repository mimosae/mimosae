/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

/**
 * This class models the log of an action that is performed in the current
 * configuration, e.g. create a microservice, link microservice contracts.
 * Action logs are serializable because there are part of the API of MIMOSAE
 * model@runtime. An action has a name and a list of arguments. The names are
 * the ones declared in the PDDL domain file (cf. resource file
 * {@code pddl/mimosae_planner_domain.pddl}). The class contains several
 * factory methods to support several cases of a certain number
 * individual/separate string arguments followed by a final argument that is a
 * list of the rest of the arguments.
 * 
 * @author Denis Conan
 */
public final class ActionLog implements Serializable {
	/**
	 * the serialization unique identifier (version number).
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * the name of the action log, i.e. the name of the method of the façade of the
	 * instance model at runtime.
	 */
	private final String name;
	/**
	 * the list of arguments of the action log.
	 */
	private final String[] arguments;

	/**
	 * constructs an action log.
	 * 
	 * @param name      the name of the action.
	 * @param arguments the list of arguments. There is at least one argument.
	 */
	public ActionLog(final String name, final String... arguments) {
		Objects.requireNonNull(name, "name cannot be null");
		if (name.isBlank()) {
			throw new IllegalArgumentException("name cannot be blank");
		}
		Objects.requireNonNull(arguments, "arguments cannot be null");
		if (Arrays.asList(arguments).contains(null)) {
			throw new IllegalArgumentException("null value in arguments");
		}
		this.name = name;
		this.arguments = Arrays.copyOf(arguments, arguments.length);
	}

	/**
	 * obtains the name of the action.
	 * 
	 * @return the name of the action.
	 */
	public String getName() {
		return name;
	}

	/**
	 * obtains the list of arguments of the action.
	 * 
	 * @return the list of arguments.
	 */
	public String[] getArguments() {
		return arguments;
	}

	/**
	 * is a factory method to create an action log.
	 * 
	 * @param name the name of the action.
	 * @param args the arguments of the action.
	 * @return the newly created action.
	 */
	public static ActionLog createActionLog(final String name, final String... args) {
		return new ActionLog(name, args);
	}

	/**
	 * is a factory method to create an action log. The first two arguments are
	 * provided by separate strings.
	 * 
	 * @param name           the name of the action.
	 * @param arg0           the first separate argument.
	 * @param arg1           the second separate argument.
	 * @return the newly created action.
	 */
	public static ActionLog createActionToLog(final String name, final String arg0, final String arg1) {
		var args = new String[2];
		args[0] = arg0;
		args[1] = arg1;
		return new ActionLog(name, args);
	}

	/**
	 * is a factory method to create an action log. The first two arguments are
	 * provided by separate strings before the rest of the arguments that are
	 * provided in the final argument that is a list.
	 * 
	 * @param name           the name of the action.
	 * @param arg0           the first separate argument.
	 * @param arg1           the second separate argument.
	 * @param otherArguments the rest of the arguments.
	 * @return the newly created action.
	 */
	public static ActionLog createActionToLog(final String name, final String arg0, final String arg1,
			final String[] otherArguments) {
		var args = new String[2 + otherArguments.length];
		args[0] = arg0;
		args[1] = arg1;
		System.arraycopy(otherArguments, 0, args, 2, otherArguments.length);
		return new ActionLog(name, args);
	}

	/**
	 * is a factory method to create an action log. The first four arguments
	 * are provided by separate strings before the rest of the arguments that are
	 * provided in the final argument that is a list.
	 * 
	 * @param name           the name of the action.
	 * @param arg0           the first separate argument.
	 * @param arg1           the second separate argument.
	 * @param arg2           the third separate argument.
	 * @param arg3           the fourth separate argument.
	 * @param otherArguments the rest of the arguments.
	 * @return the newly created action.
	 */
	public static ActionLog createActionLog(final String name, final String arg0, final String arg1,
			final String arg2, final String arg3, final String[] otherArguments) {
		final int nbSeparateArgs = 4;
		var args = new String[nbSeparateArgs + otherArguments.length];
		args[0] = arg0;
		args[1] = arg1;
		args[2] = arg2;
		// CHECKSTYLE:OFF: checkstyle:magicnumber
		args[3] = arg3;
		// CHECKSTYLE:ON: checkstyle:magicnumber
		System.arraycopy(otherArguments, 0, args, nbSeparateArgs, otherArguments.length);
		return new ActionLog(name, args);
	}

	/**
	 * is a factory method to create an action log. The first five arguments
	 * are provided by separate strings before the rest of the arguments that are
	 * provided in the final argument that is a list.
	 * 
	 * @param name           the name of the action.
	 * @param arg0           the first separate argument.
	 * @param arg1           the second separate argument.
	 * @param arg2           the third separate argument.
	 * @param arg3           the fourth separate argument.
	 * @param arg4           the fifth separate argument.
	 * @param otherArguments the rest of the arguments.
	 * @return the newly created action.
	 */
	public static ActionLog createActionLog(final String name, final String arg0, final String arg1,
			final String arg2, final String arg3, final String arg4, final String[] otherArguments) {
		final int nbSeparateArgs = 5;
		var args = new String[nbSeparateArgs + otherArguments.length];
		args[0] = arg0;
		args[1] = arg1;
		args[2] = arg2;
		// CHECKSTYLE:OFF: checkstyle:magicnumber
		args[3] = arg3;
		args[4] = arg4;
		// CHECKSTYLE:ON: checkstyle:magicnumber
		System.arraycopy(otherArguments, 0, args, nbSeparateArgs, otherArguments.length);
		return new ActionLog(name, args);
	}

	/**
	 * is a factory method to create an action log. The first six arguments
	 * are provided by separate strings before the rest of the arguments that are
	 * provided in the final argument that is a list.
	 * 
	 * @param name           the name of the action.
	 * @param arg0           the first separate argument.
	 * @param arg1           the second separate argument.
	 * @param arg2           the third separate argument.
	 * @param arg3           the fourth separate argument.
	 * @param arg4           the fifth separate argument.
	 * @param arg5           the sixth separate argument.
	 * @param otherArguments the rest of the arguments.
	 * @return the newly created action.
	 */
	// CHECKSTYLE:OFF: checkstyle:parameternumbercheck
	public static ActionLog createActionLog(final String name, final String arg0, final String arg1,
			final String arg2, final String arg3, final String arg4, final String arg5, final String[] otherArguments) {
		// CHECKSTYLE:ON: checkstyle:parameternumbercheck
		final int nbSeparateArgs = 6;
		var args = new String[nbSeparateArgs + otherArguments.length];
		args[0] = arg0;
		args[1] = arg1;
		args[2] = arg2;
		// CHECKSTYLE:OFF: checkstyle:magicnumber
		args[3] = arg3;
		args[4] = arg4;
		args[5] = arg5;
		// CHECKSTYLE:ON: checkstyle:magicnumber
		System.arraycopy(otherArguments, 0, args, nbSeparateArgs, otherArguments.length);
		return new ActionLog(name, args);
	}

	@Override
	public int hashCode() {
		final var prime = 31;
		var result = 1;
		result = prime * result + Arrays.hashCode(arguments);
		result = prime * result + Objects.hash(name);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ActionLog)) {
			return false;
		}
		ActionLog other = (ActionLog) obj;
		return Arrays.equals(arguments, other.arguments) && Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "\n\t\t[name=" + name + ", arguments=" + Arrays.toString(arguments) + "]";
	}
}
