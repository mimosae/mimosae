/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.connectortype;

import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;

/**
 * This class defines the concept of client-server connector type. A
 * client-server connector type brings together a client contract type and a
 * server contract type. The PDDL identifier is built using the PDDL identifier
 * of the client contract type and the PDDL identifier of the server contract
 * type.
 * 
 * @author Denis Conan
 */
public final class ClientServerConnectorType implements ConnectorType {
	/**
	 * the client contract type.
	 */
	private final ClientContractType clientContractType;
	/**
	 * the server contract type.
	 */
	private final ServerContractType serverContractType;

	/**
	 * constructs a new client-server contract type. The reference of the new
	 * client-server connector type is transmitted to the client connector type,
	 * i.e. the association is bidirectional.
	 * 
	 * @param clientContractType the client contract type.
	 * @param serverContractType the server contract type.
	 */
	public ClientServerConnectorType(final ClientContractType clientContractType,
			final ServerContractType serverContractType) {
		Objects.requireNonNull(clientContractType, "clientContractType cannot be null");
		Objects.requireNonNull(serverContractType, "serverContractType cannot be null");
		if (!clientContractType.isCompatibleWith(serverContractType)) {
			throw new IllegalArgumentException(clientContractType.getTypeIdentifier() + " is not compatible with "
					+ serverContractType.getTypeIdentifier());
		}
		this.clientContractType = clientContractType;
		this.clientContractType.setConnectorType(this);
		this.serverContractType = serverContractType;
	}

	/**
	 * constructs a new client-server connector type by cloning an existing
	 * client-server connector type, and in the context of a new configuration type.
	 * This constructor is called when constructing a new configuration type by
	 * cloning an existing configuration type: see
	 * {@link ConfigurationType#ConfigurationType(ConfigurationType)}.
	 * <p>
	 * The two objects have the same client contract type and the same server
	 * contract type. So, there are equals and they have the PDDL identifiers. The
	 * client contract type and the server contract type must exist in the new
	 * configuration type.
	 * 
	 * @param origin            the source client-server connector type.
	 * @param configurationType the new configuration type.
	 */
	public ClientServerConnectorType(final ClientServerConnectorType origin,
			final ConfigurationType configurationType) {
		Objects.requireNonNull(configurationType, "configurationType cannot be null");
		var thisClientContractType = configurationType
				.getClientContractType(origin.clientContractType.getTypeIdentifier());
		if (thisClientContractType == null) {
			throw new IllegalStateException("client contract type " + origin.getClientContractType().getTypeIdentifier()
					+ " does not exist in the current configuration type");
		}
		this.clientContractType = thisClientContractType;
		this.clientContractType.setConnectorType(this);
		var thisServerContractType = configurationType
				.getServerContractType(origin.getServerContractType().getTypeIdentifier());
		if (thisServerContractType == null) {
			throw new IllegalStateException("server contract type " + origin.getServerContractType().getTypeIdentifier()
					+ " does not exist in the current configuration type");
		}
		this.serverContractType = thisServerContractType;
		if (!clientContractType.isCompatibleWith(serverContractType)) {
			throw new IllegalArgumentException(clientContractType.getTypeIdentifier() + " is not compatible with "
					+ serverContractType.getTypeIdentifier());
		}
	}

	/**
	 * obtains the PDDL identifier. The PDDL identifier is built using the PDDL
	 * identifier of the client contract type and the PDDL identifier of the server
	 * contract type.
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return clientContractType.getTypeIdentifier().getPDDLIdentifier() + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS
				+ serverContractType.getTypeIdentifier().getPDDLIdentifier();
	}

	/**
	 * obtains the client contract type of the client-server connector type.
	 * 
	 * @return the client contract type.
	 */
	public ClientContractType getClientContractType() {
		return clientContractType;
	}

	/**
	 * obtains the server contract type of the client-server connector type.
	 * 
	 * @return the server contract type.
	 */
	public ServerContractType getServerContractType() {
		return serverContractType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(clientContractType, serverContractType);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ClientServerConnectorType)) {
			return false;
		}
		ClientServerConnectorType other = (ClientServerConnectorType) obj;
		return Objects.equals(clientContractType, other.clientContractType)
				&& Objects.equals(serverContractType, other.serverContractType);
	}

	@Override
	public String toString() {
		return "ClientServerConnectorType [clientContractType=" + clientContractType.getTypeIdentifier()
				+ ", serverContractType=" + serverContractType.getTypeIdentifier() + "]";
	}
}
