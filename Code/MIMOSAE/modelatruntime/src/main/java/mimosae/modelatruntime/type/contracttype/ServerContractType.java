/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;

/**
 * This class defines the concept of server contract type.
 * 
 * @author Denis Conan
 */
public final class ServerContractType extends ClientServerContractType {
	/**
	 * constructs a new server contract type. There is nothing more than
	 * {@link ClientServerContractType#ClientServerContractType(String, Semver, OperationDeclaration...)}.
	 * 
	 * @param identifier            the identifier of the type.
	 * @param version               the version of the type.
	 * @param operationDeclarations the operation declarations.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public ServerContractType(final String identifier, final Semver version,
			final OperationDeclaration... operationDeclarations) throws MalformedIdentifierException {
		super(identifier, version, operationDeclarations);
	}

	/**
	 * constructs a new server contract type. There is nothing more than
	 * {@link ClientServerContractType#ClientServerContractType(ClientServerContractType)}.
	 * 
	 * @param origin the source object.
	 */
	public ServerContractType(final ServerContractType origin) {
		super(origin);
	}

	@Override
	public String toString() {
		return "ServerContractType [getIdentifier()=" + getTypeIdentifier() + ", getOperationDeclarations()="
				+ getOperationDeclarations() + "]";
	}
}
