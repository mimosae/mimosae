/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.common.Constants;
import mimosae.modelatruntime.exception.MalformedIdentifierException;

/**
 * This class defines the concept of type identifier. It is composed of an
 * identifier and a version. There is a factory method to create an object of
 * the class: see static method {@link #getIdentifier()} There is also a PDDL
 * identifier that is built from these two attributes.
 * 
 * @author Denis Conan
 */
public final class TypeIdentifier {
	/**
	 * the identifier of the type identifier.
	 */
	private final String identifier;
	/**
	 * the version of the type identifier.
	 */
	private final Semver version;

	/**
	 * constructs a new type identifier. The identifier cannot be null, cannot be
	 * blank, and cannot contain spaces. The version cannot be null.
	 * 
	 * @param identifier the identifier of the type identifier.
	 * @param version    the version of the type identifier.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public TypeIdentifier(final String identifier, final Semver version) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		if (identifier.matches(".*\\s.*")) {
			throw new MalformedIdentifierException("identifier cannot contain spaces");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = identifier;
		var v = version;
		if (version.getType() != Semver.SemverType.NPM) {
			v = new Semver(version.getValue(), Semver.SemverType.NPM);
		}
		this.version = v;

	}

	/**
	 * constructs a new type identifier. The identifier cannot be null, cannot be
	 * blank, and cannot contain spaces. The version cannot be null.
	 * 
	 * @param identifier the identifier of the type identifier.
	 * @param version    the version of the type identifier.
	 * @return the created type identifier.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public static TypeIdentifier typeIdentifier(final String identifier, final Semver version)
			throws MalformedIdentifierException {
		return new TypeIdentifier(identifier, version);
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return identifier != null && !identifier.isBlank() && version != null;
	}

	/**
	 * obtains the identifier of the type identifier.
	 * 
	 * @return the identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL name of the type identifier. Characters "/" are replaced by
	 * strings "ZZ", and characters "." are replaced by characters "_".
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return identifier.replace("/", "ZZ") + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS
				+ version.toString().replace(".", Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS);
	}

	/**
	 * obtains the version of the type identifier.
	 * 
	 * @return the version.
	 */
	public Semver getVersion() {
		return version;
	}

	/**
	 * Check if the version satisfies a requirement.
	 *
	 * @param requirement the requirement.
	 * @return true if the version satisfies the requirement.
	 */
	public boolean isVersionSatisfied(final String requirement) {
		return this.version.satisfies(requirement);
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier, version);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof TypeIdentifier)) {
			return false;
		}
		TypeIdentifier other = (TypeIdentifier) obj;
		return Objects.equals(identifier, other.identifier) && Objects.equals(version, other.version);
	}

	@Override
	public String toString() {
		return identifier + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS + "v" + version;
	}
}
