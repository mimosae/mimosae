/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type;

/**
 * This class defines the empty init configuration type. This is the first node
 * of the graph of configuration types. The class implements the Singleton
 * design pattern.
 * 
 * @author Denis Conan
 */
public final class EmptyInitConfigurationType extends ConfigurationType {
	/**
	 * the identifier of this specific node.
	 */
	public static final String IDENTIFIER_EMPTY_INIT_CONFIGURATION_TYPE = "empty_init_configuration_type";
	/**
	 * the unique instance of the Singleton design pattern.
	 */
	private static EmptyInitConfigurationType instance;

	/**
	 * constructs the singleton instance. The constructor is private to conform the
	 * Singleton design pattern.
	 */
	private EmptyInitConfigurationType() {
		super(IDENTIFIER_EMPTY_INIT_CONFIGURATION_TYPE);
		assert invariant();
	}

	/**
	 * obtains the singleton instance of this class.
	 * 
	 * @return the singletion instance.
	 */
	public static ConfigurationType getEmptyInitConfigurationType() {
		if (instance == null) {
			instance = new EmptyInitConfigurationType();
			instance.setIdentifier(IDENTIFIER_EMPTY_INIT_CONFIGURATION_TYPE);
		}
		return instance;
	}

	@Override
	public boolean invariant() {
		return getIdentifier() != null;
	}

	@Override
	public String toString() {
		return "ConfigurationType [identifier=" + getIdentifier() + "]";
	}
}
