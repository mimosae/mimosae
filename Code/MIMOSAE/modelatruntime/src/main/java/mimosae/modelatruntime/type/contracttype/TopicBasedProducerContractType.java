/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.datamodel.EventType;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.filtermodel.TopicBasedAdvertisementFilter;

/**
 * This class defines the concept of topic-based producer contract type. It is
 * identified by its identifier and its version. The contract type contains also
 * a topic-based advertisement filter (e.g. an AMQP routing key or an MQTT
 * topic) and the collection of event types that this producer contract type
 * publishes.
 * 
 * @author Denis Conan
 */
public final class TopicBasedProducerContractType extends ProducerContractType {
	/**
	 * the topic-based advertisement filter.
	 */
	private final TopicBasedAdvertisementFilter eventFilter;
	/**
	 * the collection of event types that the producer contract type may publish.
	 */
	private final List<EventType> eventTypes;

	/**
	 * constructs a new topic-based producer contract type. The event filter cannot
	 * be null and the vararg argument of collection of event types cannot be empty
	 * and cannot contain null references. The collection of event types is
	 * unmodifiable. Publish-subscribe connectors are added afterwards with method
	 * {@link #addConnectorType(PubSubConnectorType)}.
	 * 
	 * @param identifier  the identifier of the type.
	 * @param version     the version of the type.
	 * @param eventFilter the event filter.
	 * @param eventTypes  the collection of event types.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public TopicBasedProducerContractType(final String identifier, final Semver version,
			final TopicBasedAdvertisementFilter eventFilter, final EventType... eventTypes)
			throws MalformedIdentifierException {
		super(identifier, version);
		Objects.requireNonNull(eventFilter, "eventFilter cannot be null");
		Objects.requireNonNull(eventTypes, "eventTypes cannot be null");
		this.eventTypes = Collections.unmodifiableList(Arrays.asList(eventTypes));
		if (this.eventTypes.isEmpty() || this.eventTypes.contains(null)) {
			throw new IllegalArgumentException("eventTypes cannot be empty or with null element");
		}
		this.eventFilter = eventFilter;
		assert invariant();
	}

	/**
	 * constructs a new topic-based producer contract type by cloning an existing
	 * topic-based producer contract type. The type identifiers are the same, i.e.
	 * the two objects are equal. The filter is copied. The collection of event
	 * types is copied and is unmodifiable.
	 * <p>
	 * This constructor is called when constructing a new configuration type by
	 * cloning an existing configuration type: see
	 * {@link mimosae.modelatruntime.type.ConfigurationType#ConfigurationType(ConfigurationType)}.
	 * Publish-subscribe connectors are added afterwards with method
	 * {@link #addConnectorType(PubSubConnectorType)}.
	 * 
	 * @param origin the source object to clone.
	 */
	public TopicBasedProducerContractType(final TopicBasedProducerContractType origin) {
		super(origin);
		this.eventFilter = origin.eventFilter;
		var temp = new ArrayList<EventType>();
		temp.addAll(origin.eventTypes);
		this.eventTypes = Collections.unmodifiableList(temp);
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return getTypeIdentifier().invariant() && eventFilter != null && eventTypes != null && !eventTypes.isEmpty();
	}

	/**
	 * obtains the collection of event types that this producer contract type may
	 * publish.
	 * 
	 * @return the event types.
	 */
	public List<EventType> getEventTypes() {
		return eventTypes;
	}

	/**
	 * obtains the event filter of the producer consumer type.
	 * 
	 * @return the event filter.
	 */
	public TopicBasedAdvertisementFilter getEventFilter() {
		return eventFilter;
	}

	/**
	 * states whether this producer contract type is compatible with the given
	 * consumer contract type. The consumer contract type must be a topic-based
	 * consumer contract type. In addition, the advertisement filter must be
	 * compatible with the subscription filter. Finally, all the event types that
	 * this producer contract type may publish must be accepted by the consumer
	 * contract type, i.e. the producer's collection of event types is included in
	 * the consumer's collection of event types.
	 * 
	 * @param consumerContractType the given consumer contract type.
	 * @return true when it is compatible with the given consumer contract type.
	 */
	@Override
	public boolean isCompatibleWith(final ConsumerContractType consumerContractType) {
		Objects.requireNonNull(consumerContractType, "consumerContractType cannot be null");
		if (!(consumerContractType instanceof TopicBasedConsumerContractType)) {
			throw new IllegalArgumentException(
					"waiting for a " + TopicBasedConsumerContractType.class.getCanonicalName() + ", but was "
							+ consumerContractType.getClass().getCanonicalName());
		}
		TopicBasedConsumerContractType c = (TopicBasedConsumerContractType) consumerContractType;
		return eventFilter.isCompatibleWith(c.getEventFilter())
				&& eventTypes.stream().allMatch(e1 -> c.getEventTypes().stream().anyMatch(e1::equals));
	}

	@Override
	public String toString() {
		return "TopicBasedProducerContractType [getIdentifier()=" + getTypeIdentifier() + ", eventTypes=" + eventTypes
				+ ", eventFilter=" + eventFilter + "]";
	}
}
