/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.filtermodel;

import java.util.Objects;

import com.vdurmont.semver4j.Semver;

/**
 * This class defines the filter model of a subscription filter, that is for
 * being notified by a broker. The subscription attribute is used to compute a
 * {@link java.util.regex.Pattern} in order to perform the matching.
 * <p>
 * NB: the name of the class does not contain "Type" because there is no need
 * for the corresponding class with "Instance": i.e. contrary to the concepts of
 * "configuration type", "microservice type", "database system type", etc. This
 * is so because instances of filters are not modeled: this class is used only
 * the "type" part of the modeling.
 * 
 * @author Denis Conan
 */
public abstract class TopicBasedSubscriptionFilter extends TopicBasedFilter {
	/**
	 * the subscription filter of the topic-based subscription filter.
	 */
	private final String subscriptionFilter;

	/**
	 * constructs a new topic-based subscription filter.
	 * 
	 * @param identifier         the identifier of the topic-based subscription
	 *                           filter.
	 * @param version            the version of the topic-based subscription filter.
	 * @param subscriptionFilter the string of the subscription filter.
	 */
	protected TopicBasedSubscriptionFilter(final String identifier, final Semver version,
			final String subscriptionFilter) {
		super(identifier, version);
		Objects.requireNonNull(subscriptionFilter, "subscription cannot be null");
		if (subscriptionFilter.isBlank()) {
			throw new IllegalArgumentException("subscription cannot be blank");
		}
		this.subscriptionFilter = subscriptionFilter;
	}

	/**
	 * obtains the string of the subscription filter.
	 * 
	 * @return the subscription filter.
	 */
	public String getSubscriptionFilter() {
		return subscriptionFilter;
	}
}
