/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import external.util.Digest;
import mimosae.common.Constants;
import mimosae.common.log.Log;
import mimosae.common.pddl.HappenBeforeRelationships;
import mimosae.common.pddl.ObjectTypeNames;
import mimosae.common.pddl.PredicateNames;
import mimosae.modelatruntime.exception.AlreadyConnectedException;
import mimosae.modelatruntime.exception.ConnectedDatabaseSystemInConfigurationException;
import mimosae.modelatruntime.exception.ConnectedMicroserviceInConfigurationException;
import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.exception.IsNotDeployableAndCannotBeCommittedException;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.instance.connnector.ClientServerConnector;
import mimosae.modelatruntime.instance.connnector.Connector;
import mimosae.modelatruntime.instance.connnector.DatabaseConnector;
import mimosae.modelatruntime.instance.connnector.PubSubConnector;
import mimosae.modelatruntime.instance.contract.ClientContract;
import mimosae.modelatruntime.instance.contract.ConsumerContract;
import mimosae.modelatruntime.instance.contract.DatabaseContract;
import mimosae.modelatruntime.instance.contract.ProducerContract;
import mimosae.modelatruntime.instance.contract.ServerContract;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.connectortype.PubSubConnectorType;

/**
 * This class models a configuration, i.e. an instance of configuration types. A
 * configuration has an identifier, which is the SHA-1 digest of
 * {@link #toString()}. It conforms to a configuration type, i.e. instances in
 * this configuration such microservices, contracts, connectors conform to the
 * corresponding types in the configuration type. It contains microservice
 * instances, and connector instances. Either it is the current configuration
 * that is manipulated by the façade or it is a committed configuration. Once
 * committed, a configuration type cannot be modified.
 * 
 * @author Denis Conan
 */
public class Configuration {
	/**
	 * the identifier of the configuration, which is a SHA-1 digest of the
	 * stringified content of the configuration.
	 */
	private String identifier;
	/**
	 * the configuration type to which it conforms to.
	 */
	private ConfigurationType configurationType;
	/**
	 * the collection of database systems. Each instance of the collection has a
	 * database system type that is in the set of database system types of
	 * configuration type {@link #configurationType}.
	 */
	private final Map<InstanceIdentifier, DatabaseSystem> databaseSystems;
	/**
	 * the collection of microservice instances. Each instance of the collection has
	 * a microservice type that is in the set of microservice types of configuration
	 * type {@link #configurationType}.
	 */
	private final Map<InstanceIdentifier, Microservice> microservices;
	/**
	 * the collection of connector instances. Each instance of the collection has a
	 * connector type that is in the set of connector types of configuration type
	 * {@link #configurationType}.
	 */
	private final Map<String, Connector> connectors;
	/**
	 * states whether the configuration is committed or not. When the configuration
	 * is not committed, it corresponds to the current configuration that is
	 * manipulated by the façade.
	 */
	private boolean isCommitted;

	/**
	 * constructs a configuration with a given configuration type. There is no
	 * microservice instance and no connector instance. While not committed, the
	 * identifier is {@code null}; when committing, i.e. in method {@link #commit()}
	 * the identifier is computed as the SHA-1 digest of the stringified content of
	 * the configuration.
	 * 
	 * @param configurationType the configuration type of the configuration.
	 */
	public Configuration(final ConfigurationType configurationType) {
		Objects.requireNonNull(configurationType, "configurationType cannot be null");
		this.configurationType = configurationType;
		this.microservices = new HashMap<>();
		this.databaseSystems = new HashMap<>();
		this.connectors = new HashMap<>();
		this.isCommitted = false;
		assert invariant();
	}

	/**
	 * constructs a configuration by cloning from another configuration.
	 * Microservice instances are cloned: same identifier, etc. The connectors
	 * connect microservices of the same types and with the same identifiers, but
	 * that belong to the new configuration. It is possible to clone a configuration
	 * that is committed; the new configuration is then not committed.
	 * 
	 * @param origin the configuration to clone.
	 */
	public Configuration(final Configuration origin) {
		this.identifier = null;
		this.configurationType = origin.configurationType;
		this.microservices = new HashMap<>();
		for (Map.Entry<InstanceIdentifier, Microservice> entry : origin.microservices.entrySet()) {
			this.microservices.put(entry.getKey(), new Microservice(entry.getValue()));
		}
		this.databaseSystems = new HashMap<>();
		for (Map.Entry<InstanceIdentifier, DatabaseSystem> entry : origin.databaseSystems.entrySet()) {
			this.databaseSystems.put(entry.getKey(), new DatabaseSystem(entry.getValue()));
		}
		this.connectors = new HashMap<>();
		for (Map.Entry<String, Connector> entry : origin.connectors.entrySet()) {
			if (entry.getValue() instanceof ClientServerConnector) {
				var originConnector = (ClientServerConnector) entry.getValue();
				var clientMicroservice = microservices
						.get(originConnector.getClientContract().getMicroservice().getIdentifier());
				if (clientMicroservice == null) {
					throw new IllegalStateException("client microservice "
							+ originConnector.getClientContract().getMicroservice().getIdentifier()
							+ " from connector in origin not found");
				}
				var clientContract = clientMicroservice.getClientContract(ClientContract.computeIdentifier(
						clientMicroservice, originConnector.getClientContract().getClientContractType()));
				if (clientContract == null) {
					throw new IllegalStateException(
							"client contract " + originConnector.getClientContract().getIdentifier()
									+ " from connector in origin not found");
				}
				var serverMicroservice = microservices
						.get(originConnector.getServerContract().getMicroservice().getIdentifier());
				if (serverMicroservice == null) {
					throw new IllegalStateException("server microservice "
							+ originConnector.getServerContract().getMicroservice().getIdentifier()
							+ " from connector in origin not found");
				}
				var serverContract = serverMicroservice.getServerContract(ServerContract.computeIdentifier(
						serverMicroservice, originConnector.getServerContract().getServerContractType()));
				if (serverContract == null) {
					throw new IllegalStateException(
							"server contract " + originConnector.getServerContract().getIdentifier()
									+ " from connector in origin not found");
				}
				try {
					this.connectors.put(entry.getKey(),
							new ClientServerConnector(originConnector, clientContract, serverContract));
				} catch (AlreadyConnectedException e) {
					throw new IllegalStateException("creation of client-server connector "
							+ originConnector.getIdentifier() + " when client contract "
							+ clientContract.getIdentifier() + " is already connected");
				}
			} else if (entry.getValue() instanceof PubSubConnector) {
				var originConnector = (PubSubConnector) entry.getValue();
				var thisConnector = new PubSubConnector(originConnector);
				for (ProducerContract originProducerContract : originConnector.getProducerContracts().values()) {
					var producerMicroservice = microservices
							.get(originProducerContract.getMicroservice().getIdentifier());
					if (producerMicroservice == null) {
						throw new IllegalStateException(
								"producer microservice " + originProducerContract.getMicroservice().getIdentifier()
										+ " from connector in origin not found");
					}
					var producerContract = producerMicroservice
							.getProducerContract(originProducerContract.getIdentifier());
					if (producerContract == null) {
						throw new IllegalStateException("producer contract " + originProducerContract.getIdentifier()
								+ " from connector in origin not found");
					}
					thisConnector.addProducerContract(producerContract);
				}
				for (ConsumerContract originConsumerContract : originConnector.getConsumerContracts().values()) {
					var consumerMicroservice = microservices
							.get(originConsumerContract.getMicroservice().getIdentifier());
					if (consumerMicroservice == null) {
						throw new IllegalStateException(
								"consumer microservice " + originConsumerContract.getMicroservice().getIdentifier()
										+ " from connector in origin not found");
					}
					var consumerContract = consumerMicroservice
							.getConsumerContract(originConsumerContract.getIdentifier());
					if (consumerContract == null) {
						throw new IllegalStateException("consumer contract " + originConsumerContract.getIdentifier()
								+ " from connector in origin not found");
					}
					thisConnector.addConsumerContract(consumerContract);
				}
				this.connectors.put(thisConnector.getIdentifier(), thisConnector);
			} else if (entry.getValue() instanceof DatabaseConnector) {
				var originConnector = (DatabaseConnector) entry.getValue();
				var microservice = microservices
						.get(originConnector.getDatabaseContract().getMicroservice().getIdentifier());
				if (microservice == null) {
					throw new IllegalStateException(
							"microservice " + originConnector.getDatabaseContract().getMicroservice().getIdentifier()
									+ " from connector in origin not found");
				}
				var databaseContract = microservice.getDatabaseContract(DatabaseContract.computeIdentifier(microservice,
						originConnector.getDatabaseContract().getDatabaseContractType()));
				if (databaseContract == null) {
					throw new IllegalStateException(
							"database contract " + originConnector.getDatabaseContract().getIdentifier()
									+ " from connector in origin not found");
				}
				var databaseSystem = databaseSystems.get(originConnector.getDatabaseSystem().getIdentifier());
				if (databaseSystem == null) {
					throw new IllegalStateException(
							"database system of connector in origin " + originConnector.getIdentifier() + " not found");
				}
				try {
					this.connectors.put(entry.getKey(),
							new DatabaseConnector(originConnector, databaseContract, databaseSystem));
				} catch (AlreadyConnectedException e) {
					throw new IllegalStateException("creation of database connector " + originConnector.getIdentifier()
							+ " when database contract " + databaseContract.getIdentifier() + " is already connected");
				}
			} else {
				throw new IllegalStateException("connector of unknown category");
			}
		}
		this.isCommitted = false;
		assert invariant();
	}

	/**
	 * constructs a configuration by cloning from another configuration, at the same
	 * time, moving to a new configuration type. The microservice types of current
	 * microservice instances must be present in the new configuration type.
	 * Similarly, the connector types of current connector instances must be present
	 * in the new configuration type. Microservice instances are cloned: same
	 * identifier, etc. The connectors connect microservices of the same types and
	 * with the same identifiers, but that belong to the new configuration. It is
	 * possible to clone a configuration that is committed; the new configuration is
	 * then not committed.
	 * <p>
	 * This method is used in scenarios in which a new type is introduced or an
	 * existing type is removed. Before modifying the current configuration, a new
	 * configuration type is created and the current configuration "moves" to the
	 * new configuration type by applying this constructor. Afterwards, the current
	 * configuration can use the new types or can no more use the old types. In
	 * consequence, before being deleted, a type is deprecated during at least one
	 * configuration type.
	 * 
	 * @param origin               the configuration to clone.
	 * @param newConfigurationType the new configuration type.
	 * @throws UnknownTypeException one of the type is unknown in current
	 *                              configuration.
	 */
	public Configuration(final Configuration origin, final ConfigurationType newConfigurationType)
			throws UnknownTypeException {
		this.identifier = null;
		this.configurationType = newConfigurationType;
		this.microservices = new HashMap<>();
		for (Map.Entry<InstanceIdentifier, Microservice> entry : origin.microservices.entrySet()) {
			this.microservices.put(entry.getKey(), new Microservice(entry.getValue(), newConfigurationType));
		}
		this.databaseSystems = new HashMap<>();
		for (Map.Entry<InstanceIdentifier, DatabaseSystem> entry : origin.databaseSystems.entrySet()) {
			this.databaseSystems.put(entry.getKey(), new DatabaseSystem(entry.getValue(), newConfigurationType));
		}
		this.connectors = new HashMap<>();
		for (Map.Entry<String, Connector> entry : origin.connectors.entrySet()) {
			if (entry.getValue() instanceof ClientServerConnector) {
				var originConnector = (ClientServerConnector) entry.getValue();
				var clientMicroservice = microservices
						.get(originConnector.getClientContract().getMicroservice().getIdentifier());
				if (clientMicroservice == null) {
					throw new IllegalStateException("client microservice "
							+ originConnector.getClientContract().getMicroservice().getIdentifier()
							+ " from connector in origin not found");
				}
				var newClientContractId = ClientContract.computeIdentifier(clientMicroservice,
						newConfigurationType.getClientContractType(
								originConnector.getClientContract().getClientContractType().getTypeIdentifier()));
				var clientContract = clientMicroservice.getClientContract(newClientContractId);
				if (clientContract == null) {
					throw new IllegalStateException("client contract " + newClientContractId + " in microservice "
							+ clientMicroservice + " not found");
				}
				var serverMicroservice = microservices
						.get(originConnector.getServerContract().getMicroservice().getIdentifier());
				if (serverMicroservice == null) {
					throw new IllegalStateException("server microservice "
							+ originConnector.getServerContract().getMicroservice().getIdentifier()
							+ " from connector in origin not found");
				}
				var serverContractId = ServerContract.computeIdentifier(serverMicroservice,
						newConfigurationType.getServerContractType(
								originConnector.getServerContract().getServerContractType().getTypeIdentifier()));
				var serverContract = serverMicroservice.getServerContract(serverContractId);
				if (serverContract == null) {
					throw new IllegalStateException("server contract " + serverContractId + " in microservice "
							+ serverMicroservice + " not found");
				}
				var connectorType = newConfigurationType.getClientServerConnectorType(
						clientContract.getClientContractType(), serverContract.getServerContractType());
				if (connectorType == null) {
					throw new IllegalStateException(
							"connector type " + originConnector.getIdentifier() + " from configuration type not found");
				}
				try {
					this.connectors.put(ClientServerConnector.computeIdentifier(clientContract, serverContract),
							new ClientServerConnector(connectorType, clientContract, serverContract));
				} catch (AlreadyConnectedException e) {
					throw new IllegalStateException("creation of client-server connector "
							+ originConnector.getIdentifier() + " when client contract "
							+ clientContract.getIdentifier() + " is already connected");
				}
			} else if (entry.getValue() instanceof PubSubConnector) {
				var originConnector = (PubSubConnector) entry.getValue();
				var connectorType = newConfigurationType
						.getPubSubConnectorType(originConnector.getPubSubConnectorType().getTypeIdentifier());
				var thisConnector = new PubSubConnector(originConnector.getIdentifier(), connectorType);
				for (ProducerContract originProducerContract : originConnector.getProducerContracts().values()) {
					var producerMicroservice = microservices
							.get(originProducerContract.getMicroservice().getIdentifier());
					if (producerMicroservice == null) {
						throw new IllegalStateException(
								"producer microservice " + originProducerContract.getMicroservice().getIdentifier()
										+ " from connector in origin not found");
					}
					var producerContractId = ProducerContract.computeIdentifier(producerMicroservice,
							newConfigurationType.getProducerContractType(
									originProducerContract.getProducerContractType().getTypeIdentifier()));
					var producerContract = producerMicroservice.getProducerContract(producerContractId);
					if (producerContract == null) {
						throw new IllegalStateException("producer contract " + producerContractId + " in microservice "
								+ producerMicroservice.getIdentifier() + " not found");
					}
					thisConnector.addProducerContract(producerContract);
				}
				for (ConsumerContract originConsumerContract : originConnector.getConsumerContracts().values()) {
					var consumerMicroservice = microservices
							.get(originConsumerContract.getMicroservice().getIdentifier());
					if (consumerMicroservice == null) {
						throw new IllegalStateException(
								"consumer microservice " + originConsumerContract.getMicroservice().getIdentifier()
										+ " from connector in origin not found");
					}
					var consumerContractId = ConsumerContract.computeIdentifier(consumerMicroservice,
							newConfigurationType.getConsumerContractType(
									originConsumerContract.getConsumerContractType().getTypeIdentifier()));
					var consumerContract = consumerMicroservice.getConsumerContract(consumerContractId);
					if (consumerContract == null) {
						throw new IllegalStateException(
								"consumer contract " + consumerContractId + " in microservice " + " not found");
					}
					thisConnector.addConsumerContract(consumerContract);
				}
				this.connectors.put(thisConnector.getIdentifier(), thisConnector);
			} else if (entry.getValue() instanceof DatabaseConnector) {
				var originConnector = (DatabaseConnector) entry.getValue();
				var microservice = microservices
						.get(originConnector.getDatabaseContract().getMicroservice().getIdentifier());
				if (microservice == null) {
					throw new IllegalStateException(
							"microservice " + originConnector.getDatabaseContract().getMicroservice().getIdentifier()
									+ " from connector in origin not found");
				}
				var newDatabaseContractId = DatabaseContract.computeIdentifier(microservice,
						newConfigurationType.getDatabaseContractType(
								originConnector.getDatabaseContract().getDatabaseContractType().getTypeIdentifier()));
				var databaseContract = microservice.getDatabaseContract(newDatabaseContractId);
				if (databaseContract == null) {
					throw new IllegalStateException("database contract " + newDatabaseContractId + " in microservice "
							+ microservice + " not found");
				}
				var databaseSystem = databaseSystems.get(originConnector.getDatabaseSystem().getIdentifier());
				if (databaseSystem == null) {
					throw new IllegalStateException(
							"database system " + originConnector.getDatabaseSystem().getIdentifier()
									+ " from connector in origin not found");
				}
				var connectorType = newConfigurationType.getDatabaseConnectorType(
						databaseContract.getDatabaseContractType(), databaseSystem.getDatabaseSystemType());
				if (connectorType == null) {
					throw new IllegalStateException(
							"connector type " + originConnector.getIdentifier() + " from configuration type not found");
				}
				try {
					this.connectors.put(DatabaseConnector.computeIdentifier(databaseContract, databaseSystem),
							new DatabaseConnector(connectorType, databaseContract, databaseSystem));
				} catch (AlreadyConnectedException e) {
					throw new IllegalStateException("creation of database connector " + originConnector.getIdentifier()
							+ " when database contract " + databaseContract.getIdentifier() + " is already connected");
				}
			} else {
				throw new IllegalStateException("connector of unknown category");
			}
		}
		this.isCommitted = false;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return (!isCommitted || (identifier != null && !identifier.isBlank())) && configurationType != null
				&& this.microservices != null && this.databaseSystems != null && this.connectors != null;
	}

	/**
	 * obtains the identifier of the configuration, which is a SHA-1 digest of the
	 * content.
	 * 
	 * @return the identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * sets the identifier. This method is called before committing, i.e. when the
	 * content is known: The identifier is a SHA-1 digest of the content.
	 * 
	 * @param identifier the identifier.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	protected void setIdentifier(final String identifier) throws IsCommittedAndCannotBeModifiedException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is committed and cannot be modified");
		}
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		this.identifier = identifier;
	}

	/**
	 * obtains the PDDL "version" of the identifier, i.e. replacing character "/" by
	 * "ZZ" and adding the identifier of configuration type.
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return identifier.replace("/", "ZZ") + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS
				+ configurationType.getIdentifier();
	}

	/**
	 * obtains the configuration type.
	 * 
	 * @return the configuration type.
	 */
	public ConfigurationType getConfigurationType() {
		return configurationType;
	}

	/**
	 * obtains the microservice instance with the given identifier.
	 * 
	 * @param identifier the identifier of the microservice searched for.
	 * @return the microservice found.
	 */
	public Microservice getMicroservice(final InstanceIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return microservices.get(identifier);
	}

	/**
	 * obtains the database system instance with the given identifier.
	 * 
	 * @param identifier the identifier of the database system searched for.
	 * @return the database system found.
	 */
	public DatabaseSystem getDatabaseSystem(final InstanceIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return databaseSystems.get(identifier);
	}

	/**
	 * obtains the connector instance with the given identifier.
	 * 
	 * @param identifier the identifier of the connector searched for.
	 * @return the connector found.
	 */
	public Connector getConnector(final String identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return connectors.get(identifier);
	}

	/**
	 * adds a collection of microservice instances to this configuration. The
	 * configuration must not be committed. The collection must not be empty, must
	 * not contain {@code null} references, and must not contain already present
	 * microservices (with the same identifiers).
	 * 
	 * @param microservices the collection of microservices to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addMicroservice(final Microservice... microservices) throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is already committed and thus cannot be modified");
		}
		for (Microservice toAdd : microservices) {
			if (toAdd == null) {
				throw new IllegalArgumentException("microservices cannot contain null element");
			}
			if (this.microservices.containsKey(toAdd.getIdentifier())) {
				throw new IllegalArgumentException(toAdd.getIdentifier() + "already in microservices");
			}
			if (configurationType.getMicroserviceType(toAdd.getMicroserviceType().getTypeIdentifier()) == null) {
				throw new IllegalArgumentException(
						"microservice type (" + toAdd.getMicroserviceType().getTypeIdentifier()
								+ ") is not in the collection of microservice types");
			}
			this.microservices.put(toAdd.getIdentifier(), toAdd);
		}
		assert invariant();
	}

	/**
	 * states whether a microservice is connected to another microservice (whatever
	 * these microservices are).
	 * 
	 * @param microservice the microservice to test.
	 * @return true when the given microservice is connected.
	 */
	public boolean isMicroserviceConnectedToOthers(final Microservice microservice) {
		List<String> idsOfMicroserviceContract = microservice.getIdentifiersOfContracts();
		for (Connector connector : connectors.values()) {
			if (connector.getIdentifierOfAttachedContracts().stream().anyMatch(idsOfMicroserviceContract::contains)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * removes a collection of microservice instances to this configuration. The
	 * configuration must not be committed. The collection must not be empty, must
	 * not contain {@code null} references, and the microservices must be present in
	 * the configuration (with the same identifiers). A microservice that is
	 * connected to other microservices cannot be removed, i.e. the unlink
	 * operations must be done before the removal.
	 * 
	 * @param microservices the collection of microservices to remove.
	 * @throws ConnectedMicroserviceInConfigurationException one of the contract is
	 *                                                       connected.
	 * @throws IsCommittedAndCannotBeModifiedException       the configuration is
	 *                                                       committed and cannot be
	 *                                                       modified.
	 */
	public void removeMicroservice(final Microservice... microservices)
			throws ConnectedMicroserviceInConfigurationException, IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is already committed and thus cannot be modified");
		}
		for (Microservice toRemove : microservices) {
			if (toRemove == null) {
				throw new IllegalArgumentException("microservices cannot contain null element");
			}
			if (!this.microservices.containsKey(toRemove.getIdentifier())) {
				throw new IllegalArgumentException(toRemove.getIdentifier() + " not in microservices");
			}
			if (isMicroserviceConnectedToOthers(toRemove)) {
				throw new ConnectedMicroserviceInConfigurationException("microservice " + toRemove.getIdentifier()
						+ " cannot be removed because it is connected to other microservices or database systems (please, call unlink/unconnect before)");
			}
			this.microservices.remove(toRemove.getIdentifier());
		}
		assert invariant();
	}

	/**
	 * adds a collection of database system instances to this configuration. The
	 * configuration must not be committed. The collection must not be empty, must
	 * not contain {@code null} references, and must not contain already present
	 * database systems (with the same identifiers).
	 * 
	 * @param databaseSystem the collection of microservices to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addDatabaseSystem(final DatabaseSystem... databaseSystem)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is already committed and thus cannot be modified");
		}
		for (DatabaseSystem toAdd : databaseSystem) {
			if (toAdd == null) {
				throw new IllegalArgumentException("database systems cannot contain null element");
			}
			if (this.databaseSystems.containsKey(toAdd.getIdentifier())) {
				throw new IllegalArgumentException(toAdd.getIdentifier() + "already in database systems");
			}
			if (configurationType.getDatabaseSystemType(toAdd.getDatabaseSystemType().getTypeIdentifier()) == null) {
				throw new IllegalArgumentException(
						"database system type (" + toAdd.getDatabaseSystemType().getTypeIdentifier()
								+ ") is not in the collection of database system types");
			}
			this.databaseSystems.put(toAdd.getIdentifier(), toAdd);
		}
		assert invariant();
	}

	/**
	 * states whether a database system is connected to microservices, i.e.used by
	 * microservices, in current configuration.
	 * 
	 * @param databaseSystem the database system to test.
	 * @return true when the given database system is connected, i.e. used by
	 *         microservices.
	 */
	public boolean isDatabaseSystemConnectedToMicroservices(final DatabaseSystem databaseSystem) {
		return microservices.values().stream()
				.anyMatch(m -> m.getDatabaseContracts().stream().anyMatch(DatabaseContract::isConnected));
	}

	/**
	 * removes a collection of database system instances to this configuration. The
	 * configuration must not be committed. The collection must not be empty, must
	 * not contain {@code null} references, and the database systems must be present
	 * in the configuration (with the same identifiers). A database system that is
	 * connected to other database systems cannot be removed, i.e. the unlink
	 * operations must be done before the removal.
	 * 
	 * @param databaseSystems the collection of database systems to remove.
	 * @throws ConnectedDatabaseSystemInConfigurationException one of the database
	 *                                                         is used by
	 *                                                         microservices.
	 * @throws IsCommittedAndCannotBeModifiedException         the configuration is
	 *                                                         committed and cannot
	 *                                                         be modified.
	 */
	public void removeDatabaseSystem(final DatabaseSystem... databaseSystems)
			throws ConnectedDatabaseSystemInConfigurationException, IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is already committed and thus cannot be modified");
		}
		for (DatabaseSystem toRemove : databaseSystems) {
			if (toRemove == null) {
				throw new IllegalArgumentException("databaseSystems cannot contain null element");
			}
			if (!this.databaseSystems.containsKey(toRemove.getIdentifier())) {
				throw new IllegalArgumentException(toRemove.getIdentifier() + " not in databaseSystems");
			}
			if (isDatabaseSystemConnectedToMicroservices(toRemove)) {
				throw new ConnectedDatabaseSystemInConfigurationException("database system " + toRemove.getIdentifier()
						+ " cannot be removed because it is connected to microservices (please, call disconnect before)");
			}
			this.databaseSystems.remove(toRemove.getIdentifier());
		}
		assert invariant();
	}

	/**
	 * adds a collection of connector instances to this configuration. The
	 * configuration must not be committed. The collection must not be empty, must
	 * not contain {@code null} references, and must not contain already present
	 * connectors (with the same identifiers).
	 * 
	 * @param connectors the collection of connectors to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addConnector(final Connector... connectors) throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is already committed and thus cannot be modified");
		}
		for (Connector toAdd : connectors) {
			if (toAdd == null) {
				throw new IllegalArgumentException("connectors cannot contain null element");
			}
			if (this.connectors.containsKey(toAdd.getIdentifier())) {
				throw new IllegalArgumentException(toAdd + "already in known connectors");
			}
			this.connectors.put(toAdd.getIdentifier(), toAdd);
		}
		assert invariant();
	}

	/**
	 * removes a collection of connector instances to this configuration. The
	 * configuration must not be committed. The collection must not be empty, must
	 * not contain {@code null} references, and the connectors must be present in
	 * the configuration (with the same identifiers).
	 * 
	 * @param connectors the collection of connectors to remove.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void removeConnector(final Connector... connectors) throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is already committed and thus cannot be modified");
		}
		for (Connector toRemove : connectors) {
			if (toRemove == null) {
				throw new IllegalArgumentException("connectors cannot contain null element");
			}
			if (!this.connectors.containsKey(toRemove.getIdentifier())) {
				throw new IllegalArgumentException(toRemove + "not in known connectors");
			}
			this.connectors.remove(toRemove.getIdentifier());
		}
		assert invariant();
	}

	/**
	 * states whether the configuration is deployable, i.e. all the microservice are
	 * deployable: see {@link MicroserviceType#isInstantiable()}.
	 * 
	 * @return true if the configuration type is deployable.
	 */
	public boolean isDeployable() {
		return microservices.values().stream().allMatch(Microservice::isDeployable);
	}

	/**
	 * commits the configuration. It also computes the identifier as the SHA-1
	 * digest of the string obtained by calling {@link #toString()}. The operation
	 * is idempotent.
	 * 
	 * @throws IsCommittedAndCannotBeModifiedException      the configuration is
	 *                                                      already committed.
	 * @throws IsNotDeployableAndCannotBeCommittedException the configuration is not
	 *                                                      deployable and cannot be
	 *                                                      committed.
	 */
	public void commit() throws IsCommittedAndCannotBeModifiedException, IsNotDeployableAndCannotBeCommittedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is already committed and thus cannot be modified");
		}
		if (!isDeployable()) {
			var message = "the configuration is not deployable: ";
			message += microservices.values().stream().filter(ms -> !ms.isDeployable()).map(ms -> {
				var s = "";
				if (ms.getClientContracts().isEmpty() && ms.getServerContracts().isEmpty()
						&& ms.getProducerContracts().isEmpty() && ms.getConsumerContracts().isEmpty()) {
					s += ms.getIdentifier() + ": all collections of contracts are empty: ";
				} else {
					var cct = ms.getClientContracts().values().stream().filter(c -> !c.isDeployable())
							.map(ClientContract::getIdentifier).collect(Collectors.joining());
					if (!cct.isBlank()) {
						s += "\n\tsome client contracts are not deployable: " + cct;
					}
					var pct = ms.getProducerContracts().stream().filter(c -> !c.isDeployable())
							.map(ProducerContract::getIdentifier).collect(Collectors.joining());
					if (!pct.isBlank()) {
						s += "\n\tsome producer contracts are not deployable: " + pct;
					}
					var dct = ms.getDatabaseContracts().stream().filter(c -> !c.isDeployable())
							.map(DatabaseContract::getIdentifier).collect(Collectors.joining());
					if (!dct.isBlank()) {
						s += "\n\tsome database contracts are not instantiable: " + dct;
					}
				}
				return s;
			}).collect(Collectors.joining("\n"));
			final var s = message;
			Log.MODEL_AT_RUNTIME.error("{}", () -> s);
			throw new IsNotDeployableAndCannotBeCommittedException(
					"configuration is not deployable and cannot be committed");

		}
		identifier = Digest.computeSHA1(toString());
		isCommitted = true;
		assert invariant();
	}

	/**
	 * computes the AI planning PDDL problem file so that the AI planner computes
	 * the actions to execute in order to transit the system from this configuration
	 * to the configuration that is given as the argument.
	 * <p>
	 * A PDDL problem file is decomposed into three parts:
	 * <ol>
	 * <li>The objects part that lists the microservices, the contracts (client,
	 * server, producer, consumer), and the connectors of the two configurations,
	 * i.e. this configuration and the target configuration.</li>
	 * <li>The "init" part that lists the microservices, the contracts, and the
	 * connectors that belong to this configuration.</li>
	 * <li>The "goal" part that lists the microservices, the contracts, and the
	 * connectors that disappear from this configuration (that are in this
	 * configuration but not in the target configuration) plus the microservices,
	 * the contracts, and the connectors that appear in the target configuration
	 * (that are not in this configuration but in the target configuration). An
	 * element that disappear is denoted "{@code (not element)}" in the "goal"
	 * part.</li>
	 * </ol>
	 * 
	 * @param toConfiguration         the target configuration (moving from this
	 *                                configuration to the target configuration).
	 * @param happenBeforeConstraints the collection of "happen before" that are
	 *                                going to be collected.
	 * @return the PDDL problem file, which is a collection of three parts, each
	 *         part being a collection of facts (strings).
	 */
	// CHECKSTYLE:OFF: checkstyle:parameternumbercheck
	public List<List<String>> computeAIPlanningToReconfigure(final Configuration toConfiguration,
			final HappenBeforeRelationships happenBeforeConstraints) {
		// CHECKSTYLE:ON: checkstyle:parameternumbercheck
		final List<String> objects = new ArrayList<>();
		final List<String> init = new ArrayList<>();
		final List<String> goal = new ArrayList<>();
		for (DatabaseSystem databaseSystem : databaseSystems.values()) {
			addToInPDDLProblemFile(databaseSystem.getPDDLIdentifier() + ObjectTypeNames.DATABASESYSTEM, objects);
			String dbsInInit = "(" + PredicateNames.DATABASESYSTEM + databaseSystem.getPDDLIdentifier() + ")";
			addToInPDDLProblemFile(dbsInInit, init);
			var sameDatabaseSystemInToConfiguration = toConfiguration.getDatabaseSystem(databaseSystem.getIdentifier());
			if (sameDatabaseSystemInToConfiguration == null) {
				addToInPDDLProblemFile("(not " + dbsInInit + ")", goal);
			} else {
				addToInPDDLProblemFile(dbsInInit, goal);
			}
		}
		for (Microservice microservice : microservices.values()) {
			addToInPDDLProblemFile(microservice.getPDDLIdentifier() + ObjectTypeNames.MICROSERVICE, objects);
			String msInInit = "(" + PredicateNames.MICROSERVICE + microservice.getPDDLIdentifier() + ")";
			addToInPDDLProblemFile(msInInit, init);
			var sameMicroserviceInToConfiguration = toConfiguration.getMicroservice(microservice.getIdentifier());
			if (sameMicroserviceInToConfiguration == null) {
				addToInPDDLProblemFile("(not " + msInInit + ")", goal);
			} else {
				addToInPDDLProblemFile(msInInit, goal);
			}
			for (ClientContract clientContract : microservice.getClientContracts().values()) {
				if (clientContract.isConnected()) {
					String inInit = "(" + PredicateNames.CLIENT_SERVER_LINK + " " + microservice.getPDDLIdentifier()
							+ " " + clientContract.getClientServerConnector().getServerContract().getMicroservice()
									.getPDDLIdentifier()
							+ ")";
					addToInPDDLProblemFile(inInit, init);
					var sameConnectorInToConfiguration = toConfiguration
							.getConnector(clientContract.getClientServerConnector().getIdentifier());
					if (sameConnectorInToConfiguration == null) {
						addToInPDDLProblemFile("(not " + inInit + ")", goal);
					}
				}
			}
			for (DatabaseContract databaseContract : microservice.getDatabaseContracts()) {
				if (databaseContract.isConnected()) {
					String inInit = "(" + PredicateNames.DATABASE_CONNECTION + " " + microservice.getPDDLIdentifier()
							+ " " + databaseContract.getDatabaseConnector().getDatabaseSystem().getPDDLIdentifier()
							+ ")";
					addToInPDDLProblemFile(inInit, init);
					happenBeforeConstraints.addHappenBeforeRelationship(
							databaseContract.getDatabaseConnector().getDatabaseSystem().getPDDLIdentifier(),
							microservice.getPDDLIdentifier());
					var sameConnectorInToConfiguration = toConfiguration
							.getConnector(databaseContract.getDatabaseConnector().getIdentifier());
					if (sameConnectorInToConfiguration == null) {
						addToInPDDLProblemFile("(not " + inInit + ")", goal);
					}
				}
			}
		}
		for (Connector connector : connectors.values()) {
			if (connector.getConnectorType() instanceof PubSubConnectorType) {
				var pubsubConnector = (PubSubConnector) connector;
				addToInPDDLProblemFile(connector.getPDDLIdentifier() + ObjectTypeNames.PUBSUB_CONNECTOR, objects);
				String connectorInInit = "(" + PredicateNames.PUBSUB_CONNECTOR + pubsubConnector.getPDDLIdentifier()
						+ ")";
				addToInPDDLProblemFile(connectorInInit, init);
				var sameConnectorInToConfiguration = (PubSubConnector) toConfiguration
						.getConnector(connector.getIdentifier());
				if (sameConnectorInToConfiguration == null) {
					addToInPDDLProblemFile("(not " + connectorInInit + ")", goal);
				} else {
					addToInPDDLProblemFile(connectorInInit, goal);
				}
				for (ProducerContract producerContract : pubsubConnector.getProducerContracts().values()) {
					String msCcCInInit = "(" + PredicateNames.PRODUCER_TO_CONNECTOR_LINK + " "
							+ producerContract.getMicroservice().getPDDLIdentifier() + " "
							+ pubsubConnector.getPDDLIdentifier() + ")";
					addToInPDDLProblemFile(msCcCInInit, init);
					happenBeforeConstraints.addHappenBeforeRelationship(pubsubConnector.getPDDLIdentifier(),
							producerContract.getMicroservice().getPDDLIdentifier());
					if (sameConnectorInToConfiguration == null) {
						addToInPDDLProblemFile("(not " + msCcCInInit + ")", goal);
					}
				}
				for (ConsumerContract consumerContract : pubsubConnector.getConsumerContracts().values()) {
					String msCcCInInit = "(" + PredicateNames.CONSUMER_TO_CONNECTOR_LINK + " "
							+ consumerContract.getMicroservice().getPDDLIdentifier() + " "
							+ pubsubConnector.getPDDLIdentifier() + ")";
					addToInPDDLProblemFile(msCcCInInit, init);
					happenBeforeConstraints.addHappenBeforeRelationship(pubsubConnector.getPDDLIdentifier(),
							consumerContract.getMicroservice().getPDDLIdentifier());
					if (sameConnectorInToConfiguration == null) {
						addToInPDDLProblemFile("(not " + msCcCInInit + ")", goal);
					}
				}
			}
		}
		for (DatabaseSystem databaseSystem : toConfiguration.databaseSystems.values()) {
			String mObject = databaseSystem.getPDDLIdentifier() + ObjectTypeNames.DATABASESYSTEM;
			addToInPDDLProblemFile(mObject, objects);
			String msInGoal = "(" + PredicateNames.DATABASESYSTEM + databaseSystem.getPDDLIdentifier() + ")";
			addToInPDDLProblemFile(msInGoal, goal);
		}
		for (Microservice microservice : toConfiguration.microservices.values()) {
			String mObject = microservice.getPDDLIdentifier() + ObjectTypeNames.MICROSERVICE;
			addToInPDDLProblemFile(mObject, objects);
			String msInGoal = "(" + PredicateNames.MICROSERVICE + microservice.getPDDLIdentifier() + ")";
			addToInPDDLProblemFile(msInGoal, goal);
			for (ClientContract clientContract : microservice.getClientContracts().values()) {
				if (clientContract.isConnected()) {
					String inGoal = "(" + PredicateNames.CLIENT_SERVER_LINK + " " + microservice.getPDDLIdentifier()
							+ " " + clientContract.getClientServerConnector().getServerContract().getMicroservice()
									.getPDDLIdentifier()
							+ ")";
					addToInPDDLProblemFile(inGoal, goal);
				}
			}
			for (DatabaseContract databaseContract : microservice.getDatabaseContracts()) {
				if (databaseContract.isConnected()) {
					String inGoal = "(" + PredicateNames.DATABASE_CONNECTION + " " + microservice.getPDDLIdentifier()
							+ " " + databaseContract.getDatabaseConnector().getDatabaseSystem().getPDDLIdentifier()
							+ ")";
					addToInPDDLProblemFile(inGoal, goal);
					happenBeforeConstraints.addHappenBeforeRelationship(
							databaseContract.getDatabaseConnector().getDatabaseSystem().getPDDLIdentifier(),
							microservice.getPDDLIdentifier());
				}
			}
		}
		for (Connector connector : toConfiguration.connectors.values()) {
			if (connector.getConnectorType() instanceof PubSubConnectorType) {
				var pubsubConnector = (PubSubConnector) connector;
				String cObject = connector.getPDDLIdentifier() + ObjectTypeNames.PUBSUB_CONNECTOR;
				addToInPDDLProblemFile(cObject, objects);
				String connectorInGoal = "(" + PredicateNames.PUBSUB_CONNECTOR + pubsubConnector.getPDDLIdentifier()
						+ ")";
				addToInPDDLProblemFile(connectorInGoal, goal);
				for (ProducerContract producerContract : pubsubConnector.getProducerContracts().values()) {
					String msCcCInGoal = "(" + PredicateNames.PRODUCER_TO_CONNECTOR_LINK + " "
							+ producerContract.getMicroservice().getPDDLIdentifier() + " "
							+ pubsubConnector.getPDDLIdentifier() + ")";
					addToInPDDLProblemFile(msCcCInGoal, goal);
					happenBeforeConstraints.addHappenBeforeRelationship(pubsubConnector.getPDDLIdentifier(),
							producerContract.getMicroservice().getPDDLIdentifier());
				}
				for (ConsumerContract consumerContract : pubsubConnector.getConsumerContracts().values()) {
					String msCcCInGoal = "(" + PredicateNames.CONSUMER_TO_CONNECTOR_LINK + " "
							+ consumerContract.getMicroservice().getPDDLIdentifier() + " "
							+ pubsubConnector.getPDDLIdentifier() + ")";
					addToInPDDLProblemFile(msCcCInGoal, goal);
					happenBeforeConstraints.addHappenBeforeRelationship(pubsubConnector.getPDDLIdentifier(),
							consumerContract.getMicroservice().getPDDLIdentifier());
				}
			}
		}
		// when empty configuration at the beginning, add a dummy microservice to avoid
		// empty init section
		if (init.isEmpty()) {
			addToInPDDLProblemFile("microservice_dummy - microservice", objects);
			addToInPDDLProblemFile("(microservice microservice_dummy)", init);
		}
		List<List<String>> result = new ArrayList<>();
		result.add(objects);
		result.add(init);
		result.add(goal);
		return result;
	}

	private static void addToInPDDLProblemFile(final String object, final List<String> objects) {
		Objects.requireNonNull(object, "object cannot be null");
		Objects.requireNonNull(objects, "objects cannot be null");
		var s = object.replace('.', '_');
		if (!objects.contains(s)) {
			objects.add(s);
		}
	}

	/**
	 * replaces a microservice instance of the current configuration by a new
	 * microservice instance that conforms to the new given microservice type. The
	 * new microservice type is a patch version: the new microservice instance can
	 * replace current microservice instance identically.
	 * 
	 * @param microserviceToReplace the microservice instance to replace.
	 * @param newMicroserviceType   the new microservice type.
	 * @throws MalformedIdentifierException            one of the identifiers is
	 *                                                 malformed.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void replaceMicroserviceWithPatchVersion(final Microservice microserviceToReplace,
			final MicroserviceType newMicroserviceType)
			throws MalformedIdentifierException, IsCommittedAndCannotBeModifiedException {
		var newMicroservice = new Microservice(microserviceToReplace.getIdentifier().getInstanceIdentifier(),
				newMicroserviceType);
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration " + identifier + " is already committed and thus cannot be modified");
		}
		List<ClientServerConnector> toRemove = new ArrayList<>();
		List<ClientServerConnector> toAdd = new ArrayList<>();
		connectors.values().stream().filter(ClientServerConnector.class::isInstance)
				.map(ClientServerConnector.class::cast).forEach(currentConnector -> {
					var currentClientContract = currentConnector.getClientContract();
					var currentServerContract = currentConnector.getServerContract();
					if (currentClientContract.getMicroservice().equals(microserviceToReplace)) {
						var newClientContract = newMicroservice.getClientContract(ClientContract
								.computeIdentifier(newMicroservice, currentClientContract.getClientContractType()));
						String newConnectorId = ClientServerConnector.computeIdentifier(currentClientContract,
								currentServerContract);
						if (connectors.get(newConnectorId) != null) {
							throw new IllegalStateException(
									"connector " + newConnectorId + "already exists in current configuration");
						}
						ClientServerConnector newConnector;
						try {
							newConnector = new ClientServerConnector(
									currentClientContract.getClientServerConnector().getClientServerConnectorType(),
									newClientContract, currentServerContract);
						} catch (AlreadyConnectedException e) {
							throw new IllegalStateException("creation of client-server connector "
									+ currentClientContract.getClientServerConnector().getIdentifier()
									+ " when client contract " + newClientContract.getIdentifier()
									+ " is already connected");
						}
						toRemove.add(currentConnector);
						toAdd.add(newConnector);
					}
					if (currentServerContract.getMicroservice().equals(microserviceToReplace)) {
						var newServerContract = newMicroservice.getServerContract(ServerContract
								.computeIdentifier(newMicroservice, currentServerContract.getServerContractType()));
						String newConnectorId = ClientServerConnector.computeIdentifier(currentClientContract,
								currentServerContract);
						if (connectors.get(newConnectorId) != null) {
							throw new IllegalStateException(
									"connector " + newConnectorId + " already exists in current configuration");
						}
						var clientServerConnectorType = currentClientContract.getClientServerConnector()
								.getClientServerConnectorType();
						currentClientContract.unsetConnector();
						ClientServerConnector newConnector;
						try {
							newConnector = new ClientServerConnector(clientServerConnectorType, currentClientContract,
									newServerContract);
						} catch (AlreadyConnectedException e) {
							throw new IllegalStateException("creation of client-server connector "
									+ currentClientContract.getClientServerConnector().getIdentifier()
									+ " when client contract " + currentClientContract.getIdentifier()
									+ " is already connected");
						}
						toRemove.add(currentConnector);
						toAdd.add(newConnector);
					}
				});
		removeConnector(toRemove.toArray(new ClientServerConnector[0]));
		addConnector(toAdd.toArray(new ClientServerConnector[0]));
		connectors.values().stream().filter(PubSubConnector.class::isInstance).map(PubSubConnector.class::cast)
				.forEach(connector -> {
					List<ProducerContract> currentProducerContracts = connector.getProducerContracts().values().stream()
							.filter(contracts -> contracts.getMicroservice().equals(microserviceToReplace))
							.collect(Collectors.toList());
					currentProducerContracts.forEach(currentProducerContract -> {
						String newProducerIdentifier = ProducerContract.computeIdentifier(newMicroservice,
								currentProducerContract.getProducerContractType());
						var newProducerContract = newMicroservice.getProducerContract(newProducerIdentifier);
						if (newProducerContract == null) {
							throw new IllegalStateException(
									newProducerIdentifier + " is an unknown producer contract in microservice "
											+ newMicroservice.getIdentifier());
						}
						if (!newProducerContract.getProducerContractType()
								.equals(currentProducerContract.getProducerContractType())) {
							throw new IllegalStateException("unmatching producer contract type "
									+ newProducerContract.getProducerContractType().getTypeIdentifier()
									+ " for producer contract " + newProducerContract.getIdentifier()
									+ ": was waiting producer contract type "
									+ currentProducerContract.getProducerContractType().getTypeIdentifier());
						}
						connector.addProducerContract(newProducerContract);
						connector.removeProducerContract(currentProducerContract);
					});
					List<ConsumerContract> currentConsumerContracts = connector.getConsumerContracts().values().stream()
							.filter(contracts -> contracts.getMicroservice().equals(microserviceToReplace))
							.collect(Collectors.toList());
					currentConsumerContracts.forEach(currentConsumerContract -> {
						String newConsumerIdentifier = ConsumerContract.computeIdentifier(newMicroservice,
								currentConsumerContract.getConsumerContractType());
						var newConsumerContract = newMicroservice.getConsumerContract(newConsumerIdentifier);
						if (newConsumerContract == null) {
							throw new IllegalStateException(
									newConsumerIdentifier + " is an unknown consumer contract in microservice "
											+ newMicroservice.getIdentifier());
						}
						if (!newConsumerContract.getConsumerContractType()
								.equals(currentConsumerContract.getConsumerContractType())) {
							throw new IllegalStateException("unmatching consumer contract type "
									+ newConsumerContract.getConsumerContractType().getTypeIdentifier()
									+ " for consumer contract " + newConsumerContract.getIdentifier()
									+ ": was waiting producer contract type "
									+ currentConsumerContract.getConsumerContractType().getTypeIdentifier());
						}
						connector.addConsumerContract(newConsumerContract);
						connector.removeConsumerContract(currentConsumerContract);
					});
				});
		try {
			removeMicroservice(microserviceToReplace);
		} catch (ConnectedMicroserviceInConfigurationException e) {
			throw new IllegalStateException(e.getLocalizedMessage());
		}
		addMicroservice(newMicroservice);
		assert invariant();
	}

	@Override
	public final int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public final boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Configuration)) {
			return false;
		}
		Configuration other = (Configuration) obj;
		return Objects.equals(identifier, other.identifier);
	}

	/**
	 * stringifies the content of the configuration. This method is overriden by the
	 * class {@link EmptyInitConfiguration}.
	 * 
	 * @return the string of the content.
	 */
	@Override
	public String toString() {
		final String begin = ",\n\t ";
		final String end = "\n\t[";
		final String mid = "\n\t]";
		return "Configuration [identifier=" + identifier + ", configurationType=" + configurationType.getIdentifier()
				+ ",\n\tmicroservices="
				+ microservices
						.values().stream().map(Microservice::toString).collect(Collectors.joining(begin, end, mid))
				+ ",\n\tdatabaseSystems="
				+ databaseSystems.values().stream().map(DatabaseSystem::toString)
						.collect(Collectors.joining(begin, end, mid))
				+ ",\n\tconnectors="
				+ connectors.values().stream().map(Connector::toString).collect(Collectors.joining(begin, end, mid))
				+ "\n]";
	}
}
