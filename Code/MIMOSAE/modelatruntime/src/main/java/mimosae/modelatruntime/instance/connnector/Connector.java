/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance.connnector;

import java.util.List;
import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.type.connectortype.ConnectorType;

/**
 * This class models the concept of connector instance. It has an identifier and
 * a conforming type. The class is abstract. Currently, there are three
 * categories of connectors: client-server connectors, publish-subscribe
 * connectors, and database connectors. The identifier is built in the sub-class
 * because it depends upon the connected entities of the connector.
 * 
 * @author Denis Conan
 */
public abstract class Connector {
	/**
	 * the identifier of the connector.
	 */
	private final String identifier;
	/**
	 * the connector type.
	 */
	private ConnectorType connectorType;

	protected Connector(final String identifier, final ConnectorType connectorType) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(connectorType, "connectorType cannot be null");
		this.identifier = identifier;
		this.connectorType = connectorType;
	}

	protected Connector(final Connector origin) {
		Objects.requireNonNull(origin, "origin cannot be null");
		this.identifier = origin.identifier;
		this.connectorType = origin.connectorType;
	}

	/**
	 * obtains the collection of identifiers of the contract instances attached to
	 * this connector instance.
	 * 
	 * @return the collection of identifiers.
	 */
	public abstract List<String> getIdentifierOfAttachedContracts();

	/**
	 * obtains the identifier.
	 * 
	 * @return the identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL identifier of the connector. It is computed as the
	 * concatenation of the identifier of the connector and the PDDL identifier of
	 * the connector type. The characters "/" of the identifier are replaced by the
	 * string "ZZ". The PDDL identifier is used in PDDL domain files.
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return identifier.replace("/", "ZZ") + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS
				+ connectorType.getPDDLIdentifier();
	}

	/**
	 * obtains the connector type.
	 * 
	 * @return the connector type.
	 */
	public ConnectorType getConnectorType() {
		return connectorType;
	}

	@Override
	public final int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public final boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Connector)) {
			return false;
		}
		Connector other = (Connector) obj;
		return Objects.equals(identifier, other.identifier);
	}
}
