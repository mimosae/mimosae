// CHECKSTYLE:OFF
/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import mimosae.modelatruntime.dependencymodel.MicroserviceTypeDependency;
import mimosae.modelatruntime.exception.AlreadyExistsModelElementException;
import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.exception.VersionNotSatisfiedRequirementException;

public class MimosaeRuntimModelTypeDependency {
	private static MimosaeRuntimModelTypeDependency instance;
	private MimosaeRuntimeModelType facadeModelType;
	private Map<String, MicroserviceTypeDependency> dependencies;

	public MimosaeRuntimModelTypeDependency() {
		facadeModelType = MimosaeRuntimeModelType.getMimosaeRuntimeModelType();
		this.dependencies = new HashMap<>();
	}

	public static MimosaeRuntimModelTypeDependency getMicroserviceTypeDependency() {
		if (instance == null) {
			instance = new MimosaeRuntimModelTypeDependency();
		}
		return instance;
	}

	public void clear() {
		facadeModelType.clearAllConfigurationTypes();
		this.dependencies.clear();
	}

	public MicroserviceTypeDependency getMicroserviceTypeDependency(final String depIdentifier) {
		return this.dependencies.get(depIdentifier);
	}

	public void createMicroserviceTypeDependency(final String identifier, final String dependeeIdentifier,
			final String dependeeVersionRequirement, final String dependerIdentifier, final String dependerVersion)
			throws AlreadyExistsModelElementException, MalformedIdentifierException, UnknownTypeException {
		if (dependencies.containsKey(identifier)) {
			throw new AlreadyExistsModelElementException(
					"microservice type dependency " + identifier + "already exists");
		}
		var depender = facadeModelType.getMicroserviceType(dependerIdentifier, dependerVersion);
		if (depender == null) {
			throw new UnknownTypeException(dependeeIdentifier + " is an unknown microservice type");
		}
		var dep = new MicroserviceTypeDependency(identifier, dependeeIdentifier, dependeeVersionRequirement, depender);
		dependencies.put(identifier, dep);
	}

	public void addDependeeToDependency(final String dependencyIdentifier, final String dependeeIdentifier,
			final String dependeeVersion) throws UnknownTypeException, MalformedIdentifierException {
		MicroserviceTypeDependency dep = dependencies.get(dependencyIdentifier);
		if (dep == null) {
			throw new UnknownTypeException(dependencyIdentifier + " is an unknown dependency");
		}
		var dependee = facadeModelType.getMicroserviceType(dependeeIdentifier, dependeeVersion);
		if (dependee == null) {
			throw new UnknownTypeException(dependeeIdentifier + " is an unknown microservice type");
		}
		dep.addDependee(dependee);
	}

	public void changeDependeeVersionRequirement(final String dependencyIdentifier, final String newRequirement)
			throws UnknownTypeException, VersionNotSatisfiedRequirementException {
		MicroserviceTypeDependency dep = dependencies.get(dependencyIdentifier);
		if (dep == null) {
			throw new UnknownTypeException(dependencyIdentifier + " is an unknown dependency");
		}
		dep.changeVersionRequirement(newRequirement);
	}

	public boolean checkDependencyBetweenMicroserviceTypes(final String dependerIdentifier,
			final String dependerVersion, final String dependeeIdentifier, final String dependeeVersion)
			throws UnknownTypeException, MalformedIdentifierException {
		Objects.requireNonNull(dependerIdentifier, "dependerIdentifier cannot be null");
		Objects.requireNonNull(dependerVersion, "dependerVersion cannot be null");
		Objects.requireNonNull(dependeeIdentifier, "dependeeIdentifier cannot be null");
		Objects.requireNonNull(dependeeVersion, "dependeeVersion cannot be null");
		if (dependerIdentifier.equals(dependeeIdentifier)) {
			throw new IllegalArgumentException(dependerIdentifier + "cannot be the same as " + dependeeIdentifier);
		}
		var dependee = facadeModelType.getMicroserviceType(dependeeIdentifier, dependeeVersion);
		if (dependee == null) {
			throw new IllegalArgumentException(dependeeIdentifier + " is an unknown microservice type");
		}
		var depender = facadeModelType.getMicroserviceType(dependerIdentifier, dependerVersion);
		if (depender == null) {
			throw new IllegalArgumentException(dependerIdentifier + " is an unknown microservice type");
		}
		for (MicroserviceTypeDependency dep : dependencies.values()) {
			if (dep.getDepender().equals(depender) && dep.getDependees().contains(dependee)) {
				return true;
			}
		}
		return false;
	}

	public String toStringMicroserviceTypeDependencies(final String depIdentifier) {
		Objects.requireNonNull(depIdentifier, "depIdentifier cannot be null");
		MicroserviceTypeDependency result = dependencies.get(depIdentifier);
		return result == null ? "microservice type dependency " + depIdentifier + " does not exist" : result.toString();
	}

}
