/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.connectortype;

import java.util.Objects;

import mimosae.common.Constants;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.DatabaseSystemType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;

/**
 * This class defines the concept of database connector type. A database
 * connector type brings together a database contract type (of a microservice
 * type) and a database system type. The PDDL identifier is built using the PDDL
 * identifier of the database contract type and the PDDL identifier of the
 * database system type.
 * 
 * @author Denis Conan
 */
public final class DatabaseConnectorType implements ConnectorType {
	/**
	 * the database contract type (of the microservice type).
	 */
	private final DatabaseContractType databaseContractType;
	/**
	 * the database system type.
	 */
	private final DatabaseSystemType databaseSystemType;

	/**
	 * constructs a new database connector type. The reference of the new database
	 * connector type is transmitted to the database contract type, i.e. the
	 * association is bidirectional.
	 * 
	 * @param databaseContractType the database contract type.
	 * @param databaseSystemType the database system type.
	 */
	public DatabaseConnectorType(final DatabaseContractType databaseContractType,
			final DatabaseSystemType databaseSystemType) {
		Objects.requireNonNull(databaseContractType, "databaseContractType cannot be null");
		Objects.requireNonNull(databaseSystemType, "databaseSystemType cannot be null");
		this.databaseContractType = databaseContractType;
		this.databaseContractType.setConnectorType(this);
		this.databaseSystemType = databaseSystemType;
	}

	/**
	 * constructs a new database connector type by cloning an existing database
	 * connector type, and in the context of a new configuration type. This
	 * constructor is called when constructing a new configuration type by cloning
	 * an existing configuration type: see
	 * {@link ConfigurationType#ConfigurationType(ConfigurationType)}.
	 * <p>
	 * The two objects have the same database contract type and the same database
	 * system type. So, there are equals and they have the PDDL identifiers. The
	 * database contract type and the database system type must exist in the new
	 * configuration type.
	 * 
	 * @param origin            the source database connector type.
	 * @param configurationType the new configuration type.
	 */
	public DatabaseConnectorType(final DatabaseConnectorType origin, final ConfigurationType configurationType) {
		Objects.requireNonNull(configurationType, "configurationType cannot be null");
		var thisDatabaseContractType = configurationType
				.getDatabaseContractType(origin.databaseContractType.getTypeIdentifier());
		if (thisDatabaseContractType == null) {
			throw new IllegalStateException(
					"database contract type " + origin.getDatabaseContractType().getTypeIdentifier()
							+ " does not exist in the current configuration type");
		}
		this.databaseContractType = thisDatabaseContractType;
		this.databaseContractType.setConnectorType(this);
		var thisDatabaseSystemType = configurationType
				.getDatabaseSystemType(origin.getDatabaseSystemType().getTypeIdentifier());
		if (thisDatabaseSystemType == null) {
			throw new IllegalStateException("database system type " + origin.getDatabaseSystemType().getTypeIdentifier()
					+ " does not exist in the current configuration type");
		}
		this.databaseSystemType = thisDatabaseSystemType;
	}

	/**
	 * obtains the PDDL identifier. The PDDL identifier is built using the PDDL
	 * identifier of the database contract type and the PDDL identifier of the
	 * database system type.
	 * 
	 * @return the PDDL identifier.
	 */
	public String getPDDLIdentifier() {
		return databaseContractType.getTypeIdentifier().getPDDLIdentifier() + Constants.SEPARATOR_FOR_PDDL_IDENTIFIERS
				+ databaseSystemType.getTypeIdentifier().getPDDLIdentifier();
	}

	/**
	 * obtains the database contract type of the database connector type.
	 * 
	 * @return the database contract type.
	 */
	public DatabaseContractType getDatabaseContractType() {
		return databaseContractType;
	}

	/**
	 * obtains the database system type of the database connector type.
	 * 
	 * @return the database system type.
	 */
	public DatabaseSystemType getDatabaseSystemType() {
		return databaseSystemType;
	}

	@Override
	public int hashCode() {
		return Objects.hash(databaseContractType, databaseSystemType);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof DatabaseConnectorType)) {
			return false;
		}
		DatabaseConnectorType other = (DatabaseConnectorType) obj;
		return Objects.equals(databaseContractType, other.databaseContractType)
				&& Objects.equals(databaseSystemType, other.databaseSystemType);
	}

	@Override
	public String toString() {
		return "ClientServerConnectorType [databaseContractType=" + databaseContractType.getTypeIdentifier()
				+ ", databaseSystemType=" + databaseSystemType.getTypeIdentifier() + "]";
	}
}
