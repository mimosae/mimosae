/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type.contracttype;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.vdurmont.semver4j.Semver;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.type.TypeIdentifier;

/**
 * This class defines the client-server contract type. It is identified by its
 * identifier and its version. It contains a collection of operation
 * declarations.
 * 
 * @author Denis Conan
 */
public abstract class ClientServerContractType {
	/**
	 * the type identifier.
	 */
	private final TypeIdentifier identifier;
	/**
	 * the collection of operation declarations.
	 */
	private final List<OperationDeclaration> operationDeclarations;

	/**
	 * constructs a new client-server contract type. The varargs argument is a
	 * collection of operation declarations; the collection cannot be empty and
	 * cannot contain null references. The collection of operation declarations is
	 * unmodifiable.
	 * 
	 * @param identifier            the identifier of the contract type.
	 * @param version               the version of the contract type.
	 * @param operationDeclarations the collection of operation declarations.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	protected ClientServerContractType(final String identifier, final Semver version,
			final OperationDeclaration... operationDeclarations) throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		Objects.requireNonNull(version, "version cannot be null");
		this.identifier = TypeIdentifier.typeIdentifier(identifier, version);
		this.operationDeclarations = Collections.unmodifiableList(Arrays.asList(operationDeclarations));
		if (this.operationDeclarations.isEmpty() || this.operationDeclarations.contains(null)) {
			throw new IllegalArgumentException("operationDeclarations cannot be empty or with null element");
		}
		assert invariant();
	}

	/**
	 * constructs a new client-server contract type by cloning a source contract
	 * type. The identifiers and the versions are the same, hence the two objects
	 * are equal. The collection of operation declarations is copied. The collection
	 * of operation declarations is unmodifiable.
	 * 
	 * @param origin the source of the client-server contract type.
	 */
	protected ClientServerContractType(final ClientServerContractType origin) {
		Objects.requireNonNull(origin, "origin cannot be null");
		this.identifier = origin.identifier;
		var temp = new ArrayList<OperationDeclaration>();
		temp.addAll(origin.operationDeclarations);
		this.operationDeclarations = Collections.unmodifiableList(temp);
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return identifier.invariant() && !operationDeclarations.isEmpty();
	}

	/**
	 * obtains the type identifier.
	 * 
	 * @return the type identifier.
	 */
	public TypeIdentifier getTypeIdentifier() {
		return identifier;
	}

	/**
	 * obtains the operation declarations. The collection of operation declarations
	 * is unmodifiable.
	 * 
	 * @return the operation declarations.
	 */
	public List<OperationDeclaration> getOperationDeclarations() {
		return operationDeclarations;
	}

	@Override
	public final int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public final boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ClientServerContractType)) {
			return false;
		}
		ClientServerContractType other = (ClientServerContractType) obj;
		return Objects.equals(identifier, other.identifier);
	}

	/**
	 * obtains the stringified content of the object.
	 * 
	 * @return the content in string format.
	 */
	@Override
	public String toString() {
		return "ClientServerContractType [identifier=" + identifier + ", operationDeclarations=" + operationDeclarations
				+ "]";
	}
}
