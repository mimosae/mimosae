/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.type;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import external.util.Digest;
import mimosae.common.log.Log;
import mimosae.modelatruntime.exception.IsCommittedAndCannotBeModifiedException;
import mimosae.modelatruntime.exception.IsNotInstantiableAndCannotBeCommittedException;
import mimosae.modelatruntime.type.connectortype.ClientServerConnectorType;
import mimosae.modelatruntime.type.connectortype.DatabaseConnectorType;
import mimosae.modelatruntime.type.connectortype.PubSubConnectorType;
import mimosae.modelatruntime.type.contracttype.ChannelBasedConsumerContractType;
import mimosae.modelatruntime.type.contracttype.ChannelBasedProducerContractType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.ConsumerContractType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;
import mimosae.modelatruntime.type.contracttype.ProducerContractType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;
import mimosae.modelatruntime.type.contracttype.TopicBasedConsumerContractType;
import mimosae.modelatruntime.type.contracttype.TopicBasedProducerContractType;

/**
 * This class defines the concept of configuration type. This is the container
 * of microservice types, database systems types, connector types, and all the
 * contract types (for microservice types). It represent all the types that can
 * be instantiated in a configuration. Except the singleton instance of the
 * subclass {@link EmptyInitConfigurationType}, a configuration type is
 * identified by a SHA-1 digest, which is computed when committing or validating
 * the configuration type. Once committed, a configuration type cannot be
 * modified.
 * 
 * @author Denis Conan
 */
public class ConfigurationType {
	/**
	 * the identifier of the configuration type.
	 */
	private String identifier;
	/**
	 * the collection of microservice types.
	 */
	private final Map<TypeIdentifier, MicroserviceType> microserviceTypes;
	/**
	 * the collection of database system types.
	 */
	private final Map<TypeIdentifier, DatabaseSystemType> databaseSystemTypes;
	/**
	 * the collection of client contract types.
	 */
	private final Map<TypeIdentifier, ClientContractType> clientContractTypes;
	/**
	 * the collection of server contract types.
	 */
	private final Map<TypeIdentifier, ServerContractType> serverContractTypes;
	/**
	 * the collection of producer contract types.
	 */
	private final Map<TypeIdentifier, ProducerContractType> producerContractTypes;
	/**
	 * the collection of consumer contract types.
	 */
	private final Map<TypeIdentifier, ConsumerContractType> consumerContractTypes;
	/**
	 * the collection of database contract types.
	 */
	private final Map<TypeIdentifier, DatabaseContractType> databaseContractTypes;
	/**
	 * the collection of client-server connector types.
	 */
	private final Map<ClientServerConnectorType, ClientServerConnectorType> clientServerConnectorTypes;
	/**
	 * the collection of publish-subscribe connector types.
	 */
	private final Map<TypeIdentifier, PubSubConnectorType> pubSubConnectorTypes;
	/**
	 * the collection of database connector types.
	 */
	private final Map<DatabaseConnectorType, DatabaseConnectorType> databaseConnectorTypes;
	/**
	 * boolean that states whether the configuration type is committed.
	 */
	private boolean isCommitted;

	/**
	 * constructs a new configuration type. The collections are all empty. This
	 * constructor is protected and is called only the subclass
	 * {@link EmptyInitConfigurationType} that implements the Singleton design
	 * pattern; this is the only instance with an identifier that is not a SHA-1
	 * digest.
	 * 
	 * @param identifier the identifier of the new configuration type.
	 */
	protected ConfigurationType(final String identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		if (identifier.isBlank()) {
			throw new IllegalArgumentException("identifier cannot be blank");
		}
		this.identifier = identifier;
		this.microserviceTypes = new HashMap<>();
		this.databaseSystemTypes = new HashMap<>();
		this.clientContractTypes = new HashMap<>();
		this.serverContractTypes = new HashMap<>();
		this.producerContractTypes = new HashMap<>();
		this.consumerContractTypes = new HashMap<>();
		this.databaseContractTypes = new HashMap<>();
		this.clientServerConnectorTypes = new HashMap<>();
		this.pubSubConnectorTypes = new HashMap<>();
		this.databaseConnectorTypes = new HashMap<>();
		this.isCommitted = false;
		assert invariant();
	}

	/**
	 * constructs a new configuration type by cloning an existing configuration
	 * type. The identifier of the new instance is {@code null}; hence, the two
	 * objects are not equal. The identifier is computed when committing by calling
	 * {@link #commit()}.
	 * <p>
	 * Firstly, the identifier is set to {@code null}. Secondly, the contract types
	 * are copied from the source configuration type. Thirdly, the database types
	 * and the microservice types are copied from the source configuration type.
	 * Fourthly, the connectors are copied from the source configuration type.
	 * Finally, the new configuration type is set to be not committed.
	 * 
	 * @param origin the source configuration type.
	 */
	public ConfigurationType(final ConfigurationType origin) {
		Objects.requireNonNull(origin, "origin cannot be null");
		this.identifier = null;
		this.clientContractTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, ClientContractType> entry : origin.clientContractTypes.entrySet()) {
			this.clientContractTypes.put(entry.getKey(), new ClientContractType(entry.getValue()));
		}
		this.serverContractTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, ServerContractType> entry : origin.serverContractTypes.entrySet()) {
			this.serverContractTypes.put(entry.getKey(), new ServerContractType(entry.getValue()));
		}
		this.producerContractTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, ProducerContractType> entry : origin.producerContractTypes.entrySet()) {
			if (entry.getValue() instanceof ChannelBasedProducerContractType) {
				this.producerContractTypes.put(entry.getKey(),
						new ChannelBasedProducerContractType((ChannelBasedProducerContractType) entry.getValue()));
			} else if (entry.getValue() instanceof TopicBasedProducerContractType) {
				this.producerContractTypes.put(entry.getKey(),
						new TopicBasedProducerContractType((TopicBasedProducerContractType) entry.getValue()));
			} else {
				throw new IllegalStateException("not supported or unknown type of producer contract type ("
						+ entry.getValue().getClass().getCanonicalName() + ")");
			}
		}
		this.consumerContractTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, ConsumerContractType> entry : origin.consumerContractTypes.entrySet()) {
			if (entry.getValue() instanceof ChannelBasedConsumerContractType) {
				this.consumerContractTypes.put(entry.getKey(),
						new ChannelBasedConsumerContractType((ChannelBasedConsumerContractType) entry.getValue()));
			} else if (entry.getValue() instanceof TopicBasedConsumerContractType) {
				this.consumerContractTypes.put(entry.getKey(),
						new TopicBasedConsumerContractType((TopicBasedConsumerContractType) entry.getValue()));
			} else {
				throw new IllegalStateException("not supported or unknown type of consumer contract type ("
						+ entry.getValue().getClass().getCanonicalName() + ")");
			}
		}
		this.databaseContractTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, DatabaseContractType> entry : origin.databaseContractTypes.entrySet()) {
			this.databaseContractTypes.put(entry.getKey(), new DatabaseContractType(entry.getValue()));
		}
		this.microserviceTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, MicroserviceType> entry : origin.microserviceTypes.entrySet()) {
			this.microserviceTypes.put(entry.getKey(), new MicroserviceType(entry.getValue(), this));
		}
		this.databaseSystemTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, DatabaseSystemType> entry : origin.databaseSystemTypes.entrySet()) {
			this.databaseSystemTypes.put(entry.getKey(), new DatabaseSystemType(entry.getValue(), this));
		}
		this.clientServerConnectorTypes = new HashMap<>();
		for (Map.Entry<ClientServerConnectorType, ClientServerConnectorType> entry : origin.clientServerConnectorTypes
				.entrySet()) {
			this.clientServerConnectorTypes.put(entry.getKey(), new ClientServerConnectorType(entry.getValue(), this));
		}
		this.pubSubConnectorTypes = new HashMap<>();
		for (Map.Entry<TypeIdentifier, PubSubConnectorType> entry : origin.pubSubConnectorTypes.entrySet()) {
			this.pubSubConnectorTypes.put(entry.getKey(), new PubSubConnectorType(entry.getValue(), this));
		}
		this.databaseConnectorTypes = new HashMap<>();
		for (Map.Entry<DatabaseConnectorType, DatabaseConnectorType> entry : origin.databaseConnectorTypes.entrySet()) {
			this.databaseConnectorTypes.put(entry.getKey(), new DatabaseConnectorType(entry.getValue(), this));
		}
		this.isCommitted = false;
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant holds.
	 */
	public boolean invariant() {
		return (!isCommitted || (identifier != null && microserviceTypes != null && databaseSystemTypes != null
				&& clientContractTypes != null && producerContractTypes != null && consumerContractTypes != null
				&& serverContractTypes != null && databaseContractTypes != null && clientServerConnectorTypes != null
				&& pubSubConnectorTypes != null && databaseConnectorTypes != null));
	}

	/**
	 * obtains the identifier, which is a SHA-1 digest.
	 * 
	 * @return the identifier.
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * sets the identifier.
	 * <p>
	 * This method is protected because it is used only in subclass.
	 * 
	 * @param identifier the new value of the identifier.
	 */
	protected void setIdentifier(final String identifier) {
		this.identifier = identifier;
	}

	/**
	 * obtains the microservice type with the given type identifier.
	 * 
	 * @param identifier the type identifier.
	 * @return the microservice type.
	 */
	public MicroserviceType getMicroserviceType(final TypeIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return microserviceTypes.get(identifier);
	}

	/**
	 * adds a collection of microservice types. The collection must not be empty and
	 * must not contain null references.
	 * 
	 * @param microserviceTypes the collection of microservice types to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addMicroserviceType(final MicroserviceType... microserviceTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (MicroserviceType toAdd : microserviceTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("microserviceTypes cannot contain null element");
			}
			if (this.microserviceTypes.get(toAdd.getTypeIdentifier()) != null) {
				throw new IllegalArgumentException(toAdd.getTypeIdentifier() + " already in microserviceTypes");
			}
			this.microserviceTypes.put(toAdd.getTypeIdentifier(), toAdd);
		}
	}

	/**
	 * obtains the database system type with the given type identifier.
	 * 
	 * @param identifier the type identifier.
	 * @return the database system type.
	 */
	public DatabaseSystemType getDatabaseSystemType(final TypeIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return databaseSystemTypes.get(identifier);
	}

	/**
	 * adds a collection of database system types. The collection must not be empty
	 * and must not contain null references.
	 * 
	 * @param databaseSystemTypes the collection of database system types to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addDatabaseSystemType(final DatabaseSystemType... databaseSystemTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (DatabaseSystemType toAdd : databaseSystemTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("databaseSystemTypes cannot contain null element");
			}
			if (this.microserviceTypes.get(toAdd.getTypeIdentifier()) != null) {
				throw new IllegalArgumentException(toAdd.getTypeIdentifier() + " already in databaseSystemTypes");
			}
			this.databaseSystemTypes.put(toAdd.getTypeIdentifier(), toAdd);
		}
	}

	/**
	 * obtains the client contract type with the given type identifier.
	 * 
	 * @param identifier the type identifier.
	 * @return the client contract type.
	 */
	public ClientContractType getClientContractType(final TypeIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return clientContractTypes.get(identifier);
	}

	/**
	 * adds a collection of client contract types. The collection must not be empty
	 * and must not contain null references.
	 * 
	 * @param clientContractTypes the collection of client contract types to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addClientContractType(final ClientContractType... clientContractTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (ClientContractType toAdd : clientContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("clientContractTypes cannot contain null element");
			}
			if (this.clientContractTypes.get(toAdd.getTypeIdentifier()) != null) {
				throw new IllegalArgumentException(toAdd.getTypeIdentifier() + "already in cliencContracTypes");
			}
			this.clientContractTypes.put(toAdd.getTypeIdentifier(), toAdd);
		}
	}

	/**
	 * obtains the server contract type with the given type identifier.
	 * 
	 * @param identifier the type identifier.
	 * @return the server contract type.
	 */
	public ServerContractType getServerContractType(final TypeIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return serverContractTypes.get(identifier);
	}

	/**
	 * adds a collection of server contract types. The collection must not be empty
	 * and must not contain null references.
	 * 
	 * @param serverContractTypes the collection of server contract types to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addServerContractType(final ServerContractType... serverContractTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (ServerContractType toAdd : serverContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("serverContractTypes cannot contain null element");
			}
			if (this.serverContractTypes.get(toAdd.getTypeIdentifier()) != null) {
				throw new IllegalArgumentException(toAdd.getTypeIdentifier() + "already in serverContracTypes");
			}
			this.serverContractTypes.put(toAdd.getTypeIdentifier(), toAdd);
		}
	}

	/**
	 * obtains the producer contract type with the given type identifier.
	 * 
	 * @param identifier the type identifier.
	 * @return the producer contract type.
	 */
	public ProducerContractType getProducerContractType(final TypeIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return producerContractTypes.get(identifier);
	}

	/**
	 * adds a collection of producer contract types. The collection must not be
	 * empty and must not contain null references.
	 * 
	 * @param producerContractTypes the collection of producer contract types to
	 *                              add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addProducerContractType(final ProducerContractType... producerContractTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (ProducerContractType toAdd : producerContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("producerContractTypes cannot contain null element");
			}
			if (this.producerContractTypes.get(toAdd.getTypeIdentifier()) != null) {
				throw new IllegalArgumentException(toAdd.getTypeIdentifier() + "already in producerContracTypes");
			}
			this.producerContractTypes.put(toAdd.getTypeIdentifier(), toAdd);
		}
	}

	/**
	 * obtains the consumer contract type with the given type identifier.
	 * 
	 * @param identifier the type identifier.
	 * @return the consumer contract type.
	 */
	public ConsumerContractType getConsumerContractType(final TypeIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return consumerContractTypes.get(identifier);
	}

	/**
	 * adds a collection of consumer contract types. The collection must not be
	 * empty and must not contain null references.
	 * 
	 * @param consumerContractTypes the collection of consumer contract types to
	 *                              add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addConsumerContractType(final ConsumerContractType... consumerContractTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (ConsumerContractType toAdd : consumerContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("consumerContractTypes cannot contain null element");
			}
			if (this.consumerContractTypes.get(toAdd.getTypeIdentifier()) != null) {
				throw new IllegalArgumentException(toAdd.getTypeIdentifier() + "already in consumerContracTypes");
			}
			this.consumerContractTypes.put(toAdd.getTypeIdentifier(), toAdd);
		}
	}

	/**
	 * obtains the database contract type with the given type identifier.
	 * 
	 * @param identifier the type identifier.
	 * @return the database contract type.
	 */
	public DatabaseContractType getDatabaseContractType(final TypeIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return databaseContractTypes.get(identifier);
	}

	/**
	 * adds a collection of database contract types. The collection must not be
	 * empty and must not contain null references.
	 * 
	 * @param databaseContractTypes the collection of database contract types to
	 *                              add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addDatabaseContractType(final DatabaseContractType... databaseContractTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (DatabaseContractType toAdd : databaseContractTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("databaseContractTypes cannot contain null element");
			}
			if (this.databaseContractTypes.get(toAdd.getTypeIdentifier()) != null) {
				throw new IllegalArgumentException(toAdd.getTypeIdentifier() + "already in databaseContracTypes");
			}
			this.databaseContractTypes.put(toAdd.getTypeIdentifier(), toAdd);
		}
	}

	/**
	 * obtains the client-server connector type with the given client contract type
	 * and the given server contract type.
	 * 
	 * @param clientContractType the client contract type.
	 * @param serverContractType the server contract type.
	 * @return the client-server connector type.
	 */
	public ClientServerConnectorType getClientServerConnectorType(final ClientContractType clientContractType,
			final ServerContractType serverContractType) {
		Objects.requireNonNull(clientContractType, "client contract type cannot be null");
		Objects.requireNonNull(serverContractType, "server contract type cannot be null");
		var connectorType = clientServerConnectorTypes.values().stream()
				.filter(c -> c.getClientContractType().equals(clientContractType)
						&& c.getServerContractType().equals(serverContractType))
				.findFirst();
		return connectorType.orElse(null);
	}

	/**
	 * adds a collection of client-server connector types. The collection must not
	 * be empty and must not contain null references.
	 * 
	 * @param clientServerConnectorTypes the collection of client-server connector
	 *                                   types to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addClientServerConnectorType(final ClientServerConnectorType... clientServerConnectorTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (ClientServerConnectorType toAdd : clientServerConnectorTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("clientServerConnectorTypes cannot contain null element");
			}
			if (this.clientServerConnectorTypes.get(toAdd) != null) {
				throw new IllegalArgumentException(toAdd.getClientContractType().getTypeIdentifier() + " connected to "
						+ toAdd.getServerContractType().getTypeIdentifier() + "already in clientServerConnectorTypes");
			}
			this.clientServerConnectorTypes.put(toAdd, toAdd);
		}
	}

	/**
	 * obtains the publish-subscribe connector type with the given type identifier.
	 * 
	 * @param identifier the type identifier.
	 * @return the publish-subscribe connector type.
	 */
	public PubSubConnectorType getPubSubConnectorType(final TypeIdentifier identifier) {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		return pubSubConnectorTypes.get(identifier);
	}

	/**
	 * adds a collection of publish-subscribe connector types. The collection must
	 * not be empty and must not contain null references.
	 * 
	 * @param pubSubConnectorTypes the collection of publish-subscribe connector
	 *                             types to add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addPubSubConnectorType(final PubSubConnectorType... pubSubConnectorTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (PubSubConnectorType toAdd : pubSubConnectorTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("pubSubConnectorTypes cannot contain null element");
			}
			if (this.pubSubConnectorTypes.get(toAdd.getTypeIdentifier()) != null) {
				throw new IllegalArgumentException(toAdd.getTypeIdentifier() + "already in pubSubConnectorTypes");
			}
			this.pubSubConnectorTypes.put(toAdd.getTypeIdentifier(), toAdd);
		}
	}

	/**
	 * obtains the database connector type with the given database contract type and
	 * the given database system type.
	 * 
	 * @param databaseContractType the database contract type.
	 * @param databaseSystemType   the database system type.
	 * @return the database connector type.
	 */
	public DatabaseConnectorType getDatabaseConnectorType(final DatabaseContractType databaseContractType,
			final DatabaseSystemType databaseSystemType) {
		Objects.requireNonNull(databaseContractType, "database contract type cannot be null");
		Objects.requireNonNull(databaseSystemType, "database system type cannot be null");
		var connectorType = databaseConnectorTypes.values().stream()
				.filter(c -> c.getDatabaseContractType().equals(databaseContractType)
						&& c.getDatabaseSystemType().equals(databaseSystemType))
				.findFirst();
		return connectorType.orElse(null);
	}

	/**
	 * adds a collection of database connector types. The collection must not be
	 * empty and must not contain null references.
	 * 
	 * @param databaseConnectorTypes the collection of database connector types to
	 *                               add.
	 * @throws IsCommittedAndCannotBeModifiedException the configuration type is
	 *                                                 committed and cannot be
	 *                                                 modified.
	 */
	public void addDatabaseConnectorType(final DatabaseConnectorType... databaseConnectorTypes)
			throws IsCommittedAndCannotBeModifiedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		for (DatabaseConnectorType toAdd : databaseConnectorTypes) {
			if (toAdd == null) {
				throw new IllegalArgumentException("databaseConnectorTypes cannot contain null element");
			}
			if (this.databaseConnectorTypes.get(toAdd) != null) {
				throw new IllegalArgumentException(
						toAdd.getDatabaseContractType().getTypeIdentifier() + "already in databaseConnectorTypes");
			}
			this.databaseConnectorTypes.put(toAdd, toAdd);
		}
	}

	/**
	 * states whether the configuration type is instantiable, i.e. all the
	 * microservice types are instantiable: see
	 * {@link MicroserviceType#isInstantiable()}.
	 * 
	 * @return true if the configuration type is instantiable.
	 */
	public boolean isInstantiable() {
		return !microserviceTypes.isEmpty()
				&& microserviceTypes.values().stream().allMatch(MicroserviceType::isInstantiable);
	}

	/**
	 * commits the configuration type. A configuration type cannot be committed
	 * twice. Committing is setting the identifier to a not null and not blank
	 * value, which is SHA-1 digest of the whole content of the configuration type.
	 * 
	 * @throws IsCommittedAndCannotBeModifiedException        the configuration type
	 *                                                        is already committed.
	 * @throws IsNotInstantiableAndCannotBeCommittedException the configuration type
	 *                                                        is not instantiable
	 *                                                        and cannot be
	 *                                                        modified.
	 */
	public void commit()
			throws IsCommittedAndCannotBeModifiedException, IsNotInstantiableAndCannotBeCommittedException {
		if (isCommitted) {
			throw new IsCommittedAndCannotBeModifiedException(
					"configuration type " + identifier + " is committed and cannot be modified");
		}
		if (!isInstantiable()) {
			var message = "the configuration type is not instantiable: ";
			if (microserviceTypes.isEmpty()) {
				message += "the collection of microservice types is empty";
			} else {
				message += microserviceTypes.values().stream().filter(mt -> !mt.isInstantiable()).map(mt -> {
					var s = "";
					if (mt.getClientContractTypes().isEmpty() && mt.getServerContractTypes().isEmpty()
							&& mt.getProducerContractTypes().isEmpty() && mt.getConsumerContractTypes().isEmpty()) {
						s += mt.getTypeIdentifier() + ": all collections of contract types are empty: ";
					} else {
						var cct = mt.getClientContractTypes().stream().filter(c -> !c.isInstantiable())
								.map(ClientContractType::getTypeIdentifier).map(TypeIdentifier::toString)
								.collect(Collectors.joining());
						if (!cct.isBlank()) {
							s += "\n\tsome client contract types are not instantiable: " + cct;
						}
						var pct = mt.getProducerContractTypes().stream().filter(c -> !c.isInstantiable())
								.map(ProducerContractType::getTypeIdentifier).map(TypeIdentifier::toString)
								.collect(Collectors.joining());
						if (!pct.isBlank()) {
							s += "\n\tsome producer contract types are not instantiable: " + pct;
						}
						var dct = mt.getDatabaseContractTypes().stream().filter(c -> !c.isInstantiable())
								.map(DatabaseContractType::getTypeIdentifier).map(TypeIdentifier::toString)
								.collect(Collectors.joining());
						if (!dct.isBlank()) {
							s += "\n\tsome database contract types are not instantiable: " + dct;
						}
					}
					return s;
				}).collect(Collectors.joining("\n"));
			}
			final var s = message;
			Log.MODEL_AT_RUNTIME.error("{}", () -> s);
			throw new IsNotInstantiableAndCannotBeCommittedException(
					"configuration type is not instantiable and cannot be committed");

		}
		identifier = Digest.computeSHA1(toString());
		isCommitted = true;
		assert invariant();
	}

	@Override
	public final int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public final boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ConfigurationType)) {
			return false;
		}
		ConfigurationType other = (ConfigurationType) obj;
		return Objects.equals(identifier, other.identifier);
	}

	/**
	 * computes the string of the content of the configuration type.
	 * 
	 * @return the string of the content.
	 */
	@Override
	public String toString() {
		final String beginCollection = ",\n\t ";
		final String midCollection = "\n\t[";
		final String endCollection = "\n\t]";
		return "ConfigurationType [identifier=" + identifier + ",\n\tmicroserviceTypes="
				+ microserviceTypes.values().stream().map(MicroserviceType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tdatabaseSystemTypes="
				+ databaseSystemTypes.values().stream().map(DatabaseSystemType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tclientContractTypes="
				+ clientContractTypes.values().stream().map(ClientContractType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tserverContractTypes="
				+ serverContractTypes.values().stream().map(ServerContractType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tproducerContractTypes="
				+ producerContractTypes.values().stream().map(ProducerContractType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tconsumerContractTypes="
				+ consumerContractTypes.values().stream().map(ConsumerContractType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tdatabaseContractTypes="
				+ databaseContractTypes.values().stream().map(DatabaseContractType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tclient server connector types="
				+ clientServerConnectorTypes.values().stream().map(ClientServerConnectorType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tpublish-subscribe connector types="
				+ pubSubConnectorTypes.values().stream().map(PubSubConnectorType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ ",\n\tdatabase connector types="
				+ databaseConnectorTypes.values().stream().map(DatabaseConnectorType::toString)
						.collect(Collectors.joining(beginCollection, midCollection, endCollection))
				+ "\n]";
	}
}
