/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.filtermodel;

import com.vdurmont.semver4j.Semver;

import external.util.MQTTTopicChecker;

/**
 * This class defines the filter model of an AMQP filter for advertisements,
 * that is for publishing to an AMQP broker. The routing key must conform to
 * AMQP specification.
 * <p>
 * NB: the name of the class does not contain "Type" because there is no need
 * for the corresponding class with "Instance": i.e. contrary to the concepts of
 * "configuration type", "microservice type", "database system type", etc. This
 * is so because instances of filters are not modeled: this class is used only
 * the "type" part of the modeling.
 * 
 * @author Denis Conan
 */
public final class AMQPTopicBasedAdvertisementFilter extends TopicBasedAdvertisementFilter {
	/**
	 * constructs a new AMQP advertisement filter, that is a filter with a routing
	 * key. Strictly speaking, such a filter is not used for filtering but for
	 * adding the routing key to published messages.
	 * 
	 * @param identifier the identifier of the filter.
	 * @param version    the version of the filter.
	 * @param routingKey the routing key.
	 */
	public AMQPTopicBasedAdvertisementFilter(final String identifier, final Semver version, final String routingKey) {
		super(identifier, version, routingKey);
		if (!MQTTTopicChecker.pubTopicCheck(routingKey)) {
			throw new IllegalArgumentException("routing key does not conform to AMQP specification");
		}
	}

	/**
	 * states whether this filter is compatible with the given filter. Because this
	 * filter is for the AMQP technology, the given filter must be a subscription
	 * filter that contains an AMQP binding key. Then, this method checks that the
	 * routing key of this filter matches the given binding key.
	 * 
	 * @param eventFilter the given subscription filter, which must be an AMQP
	 *                    subscription filter that contains a binding key.
	 * @return true when the routing key of this filter matches the binding key of
	 *         the given subscription filter.
	 */
	public boolean isCompatibleWith(final EventFilter eventFilter) {
		if (!(eventFilter instanceof AMQPTopicBasedSubscriptionFilter)) {
			throw new IllegalArgumentException(
					"waiting for a " + AMQPTopicBasedSubscriptionFilter.class.getCanonicalName() + ", but was "
							+ eventFilter.getClass().getCanonicalName());
		}
		return ((AMQPTopicBasedSubscriptionFilter) eventFilter).getPattern().matcher(getTopic()).matches();
	}

	@Override
	public String toString() {
		return "AMQPTopicBasedAdvertisementFilter [getIdentifier()=" + getIdentifier() + ", getVersion()="
				+ getVersion() + ", getRoutingKey()=" + getTopic() + "]";
	}
}
