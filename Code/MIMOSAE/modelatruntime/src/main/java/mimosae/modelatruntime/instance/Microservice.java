/**
This file is part of the MIMOSAE middleware.

Copyright (C) 2021 Télécom SudParis & EDF Lab.

The MIMOSAE software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The MIMOSAE software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Yuwei Wang
 */
package mimosae.modelatruntime.instance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import mimosae.modelatruntime.exception.MalformedIdentifierException;
import mimosae.modelatruntime.exception.UnknownTypeException;
import mimosae.modelatruntime.instance.contract.ClientContract;
import mimosae.modelatruntime.instance.contract.ConsumerContract;
import mimosae.modelatruntime.instance.contract.DatabaseContract;
import mimosae.modelatruntime.instance.contract.ProducerContract;
import mimosae.modelatruntime.instance.contract.ServerContract;
import mimosae.modelatruntime.type.ConfigurationType;
import mimosae.modelatruntime.type.MicroserviceType;
import mimosae.modelatruntime.type.contracttype.ClientContractType;
import mimosae.modelatruntime.type.contracttype.ConsumerContractType;
import mimosae.modelatruntime.type.contracttype.DatabaseContractType;
import mimosae.modelatruntime.type.contracttype.ProducerContractType;
import mimosae.modelatruntime.type.contracttype.ServerContractType;

/**
 * This class defines the concept of microservice (instance) that conforms to a
 * microservice type. When creating a microservice, all the contracts (types) of
 * the microservice type are instantiated, i.e. the conforming microservice type
 * acts as a template. Here are the kinds of contracts: client contract, server
 * contract, producer contract, consumer contract, database contract.
 * <p>
 * A microservice is identified by the identifier of the instance plus the
 * identifier and the version of its conforming type, e.g.
 * "authentification_service_replica_1" of type "authentification_service_type"
 * in version "0.0.1".
 * 
 * @author Denis Conan
 */
public final class Microservice {
	/**
	 * the instance identifier.
	 */
	private final InstanceIdentifier identifier;
	/**
	 * the conforming type.
	 */
	private final MicroserviceType microserviceType;
	/**
	 * the collection of client contracts.
	 */
	private final Map<String, ClientContract> clientContracts;
	/**
	 * the collection of server contracts.
	 */
	private final Map<String, ServerContract> serverContracts;
	/**
	 * the collection of the producer contracts.
	 */
	private final Map<String, ProducerContract> producerContracts;
	/**
	 * the collection of consumer contracts.
	 */
	private final Map<String, ConsumerContract> consumerContracts;
	/**
	 * the collection of database contracts.
	 */
	private final Map<String, DatabaseContract> databaseContracts;

	/**
	 * constructs a new microservice instance that corresponds to a given
	 * microservice type. A microservice is identified by the identifier of the
	 * instance plus the identifier and the version of its conforming type, e.g.
	 * "authentification_service_replica_1" of type "authentification_service_type"
	 * in version "0.0.1".
	 * 
	 * @param identifier       the identifier of the instance.
	 * @param microserviceType the microservice type.
	 * @throws MalformedIdentifierException the identifier is malformed.
	 */
	public Microservice(final String identifier, final MicroserviceType microserviceType)
			throws MalformedIdentifierException {
		Objects.requireNonNull(identifier, "identifier cannot be null");
		Objects.requireNonNull(microserviceType, "microserviceType cannot be null");
		this.identifier = new InstanceIdentifier(identifier, microserviceType.getTypeIdentifier());
		this.microserviceType = microserviceType;
		this.clientContracts = new HashMap<>();
		for (ClientContractType clientContractType : microserviceType.getClientContractTypes()) {
			String id = ClientContract.computeIdentifier(this, clientContractType);
			var toAdd = new ClientContract(clientContractType, this);
			clientContracts.put(id, toAdd);
		}
		this.serverContracts = new HashMap<>();
		for (ServerContractType serverContractType : microserviceType.getServerContractTypes()) {
			String id = ServerContract.computeIdentifier(this, serverContractType);
			serverContracts.put(id, new ServerContract(serverContractType, this));
		}
		this.producerContracts = new HashMap<>();
		for (ProducerContractType producerContractType : microserviceType.getProducerContractTypes()) {
			String id = ProducerContract.computeIdentifier(this, producerContractType);
			producerContracts.put(id, new ProducerContract(producerContractType, this));
		}
		this.consumerContracts = new HashMap<>();
		for (ConsumerContractType consumerContractType : microserviceType.getConsumerContractTypes()) {
			String id = ConsumerContract.computeIdentifier(this, consumerContractType);
			consumerContracts.put(id, new ConsumerContract(consumerContractType, this));
		}
		this.databaseContracts = new HashMap<>();
		for (DatabaseContractType databaseContractType : microserviceType.getDatabaseContractTypes()) {
			String id = DatabaseContract.computeIdentifier(this, databaseContractType);
			databaseContracts.put(id, new DatabaseContract(databaseContractType, this));
		}
		assert invariant();
	}

	/**
	 * constructs a new microservice instance as a clone of a source microservice
	 * instance. This constructor is for example used for creating a "new"
	 * configuration from the current configuration, which is then committed to
	 * become the last committed configuration.
	 * <p>
	 * The identifiers are the same, i.e. the microservices are equal and the
	 * contracts are cloned.
	 * 
	 * @param origin the source configuration.
	 */
	public Microservice(final Microservice origin) {
		Objects.requireNonNull(origin, "cannot clone null origin");
		this.identifier = origin.identifier;
		this.microserviceType = origin.microserviceType;
		this.clientContracts = new HashMap<>();
		for (Map.Entry<String, ClientContract> entry : origin.clientContracts.entrySet()) {
			this.clientContracts.put(entry.getKey(), new ClientContract(entry.getValue(), this));
		}
		this.serverContracts = new HashMap<>();
		for (Map.Entry<String, ServerContract> entry : origin.serverContracts.entrySet()) {
			this.serverContracts.put(entry.getKey(), new ServerContract(entry.getValue(), this));
		}
		this.producerContracts = new HashMap<>();
		for (Map.Entry<String, ProducerContract> entry : origin.producerContracts.entrySet()) {
			this.producerContracts.put(entry.getKey(), new ProducerContract(entry.getValue(), this));
		}
		this.consumerContracts = new HashMap<>();
		for (Map.Entry<String, ConsumerContract> entry : origin.consumerContracts.entrySet()) {
			this.consumerContracts.put(entry.getKey(), new ConsumerContract(entry.getValue(), this));
		}
		this.databaseContracts = new HashMap<>();
		for (Map.Entry<String, DatabaseContract> entry : origin.databaseContracts.entrySet()) {
			this.databaseContracts.put(entry.getKey(), new DatabaseContract(entry.getValue(), this));
		}
		assert invariant();
	}

	/**
	 * constructs a new microservice instance by cloning a source configuration into
	 * a new configuration type. The instance identifiers and the conforming types
	 * are the same, i.e. the source and the new objects are equal. The microservice
	 * type must belong to the new microservice type.
	 * <p>
	 * This method is used in scenarios in which a new microservice type is
	 * introduced or an existing microservice type is removed. Before modifying the
	 * current configuration, a new configuration type is created and the current
	 * configuration "moves" to the new configuration type by applying this
	 * constructor on all the microservices. Afterwards, the current configuration
	 * can use the new microservice types or can no more use the old microservice
	 * types. In consequence, before being deleted, a microservice type is
	 * deprecated "during" at least one configuration type.
	 * 
	 * @param origin               the source microservice to clone.
	 * @param newConfigurationType the configuration type of the new microservice.
	 * @throws UnknownTypeException the microservice type of the source microservice
	 *                              is unknown in the configuration type.
	 */
	public Microservice(final Microservice origin, final ConfigurationType newConfigurationType)
			throws UnknownTypeException {
		Objects.requireNonNull(origin, "cannot clone null origin");
		Objects.requireNonNull(newConfigurationType, "newConfigurationType cannot be null");
		this.identifier = origin.identifier;
		var newMicroserviceType = newConfigurationType.getMicroserviceType(origin.microserviceType.getTypeIdentifier());
		if (newMicroserviceType == null) {
			throw new UnknownTypeException("microservice type " + origin.microserviceType.getTypeIdentifier()
					+ " does not exist in configuration type " + newConfigurationType.getIdentifier());
		}
		this.microserviceType = newMicroserviceType;
		this.clientContracts = new HashMap<>();
		for (Map.Entry<String, ClientContract> entry : origin.clientContracts.entrySet()) {
			this.clientContracts.put(entry.getKey(), new ClientContract(entry.getValue(), this, newConfigurationType));
		}
		this.serverContracts = new HashMap<>();
		for (Map.Entry<String, ServerContract> entry : origin.serverContracts.entrySet()) {
			this.serverContracts.put(entry.getKey(), new ServerContract(entry.getValue(), this, newConfigurationType));
		}
		this.producerContracts = new HashMap<>();
		for (Map.Entry<String, ProducerContract> entry : origin.producerContracts.entrySet()) {
			this.producerContracts.put(entry.getKey(),
					new ProducerContract(entry.getValue(), this, newConfigurationType));
		}
		this.consumerContracts = new HashMap<>();
		for (Map.Entry<String, ConsumerContract> entry : origin.consumerContracts.entrySet()) {
			this.consumerContracts.put(entry.getKey(),
					new ConsumerContract(entry.getValue(), this, newConfigurationType));
		}
		this.databaseContracts = new HashMap<>();
		for (Map.Entry<String, DatabaseContract> entry : origin.databaseContracts.entrySet()) {
			this.databaseContracts.put(entry.getKey(),
					new DatabaseContract(entry.getValue(), this, newConfigurationType));
		}
		assert invariant();
	}

	/**
	 * states whether the invariant holds.
	 * 
	 * @return true when the invariant hold.
	 */
	public boolean invariant() {
		return identifier != null && microserviceType != null && this.clientContracts != null
				&& this.serverContracts != null && this.producerContracts != null && this.consumerContracts != null
				&& this.databaseContracts != null;
	}

	List<String> getIdentifiersOfContracts() {
		List<String> ids = new ArrayList<>();
		ids.addAll(clientContracts.keySet());
		ids.addAll(serverContracts.keySet());
		ids.addAll(producerContracts.keySet());
		ids.addAll(consumerContracts.keySet());
		ids.addAll(databaseContracts.keySet());
		return ids;
	}

	/**
	 * obtains the instance identifier. A microservice is identified by the
	 * identifier of the instance plus the identifier and the version of its
	 * conforming type, e.g. "authentification_service_replica_1" of type
	 * "authentification_service_type" in version "0.0.1".
	 * 
	 * @return the instance identifier.
	 */
	public InstanceIdentifier getIdentifier() {
		return identifier;
	}

	/**
	 * obtains the PDDL identifier of the instance identifier.
	 * 
	 * @return the PDDL identifier used in PDDL domain files.
	 */
	public String getPDDLIdentifier() {
		return identifier.getPDDLIdentifier();
	}

	/**
	 * obtains the conforming microservice type.
	 * 
	 * @return the microservice type.
	 */
	public MicroserviceType getMicroserviceType() {
		return microserviceType;
	}

	/**
	 * obtains the client contract with the given identifier.
	 * 
	 * @param identifier the identifier of the object to search for.
	 * @return the object that was found.
	 */
	public ClientContract getClientContract(final String identifier) {
		return clientContracts.get(identifier);
	}

	Map<String, ClientContract> getClientContracts() {
		return Collections.unmodifiableMap(clientContracts);
	}

	/**
	 * obtains the server contract with the given identifier.
	 * 
	 * @param identifier the identifier of the object to search for.
	 * @return the object that was found.
	 */
	public ServerContract getServerContract(final String identifier) {
		return serverContracts.get(identifier);
	}

	Map<String, ServerContract> getServerContracts() {
		return Collections.unmodifiableMap(serverContracts);
	}

	/**
	 * obtains the producer contract with the given identifier.
	 * 
	 * @param identifier the identifier of the object to search for.
	 * @return the object that was found.
	 */
	public ProducerContract getProducerContract(final String identifier) {
		return producerContracts.get(identifier);
	}

	Collection<ProducerContract> getProducerContracts() {
		return Collections.unmodifiableCollection(producerContracts.values());
	}

	/**
	 * obtains the consumer contract with the given identifier.
	 * 
	 * @param identifier the identifier of the object to search for.
	 * @return the object that was found.
	 */
	public ConsumerContract getConsumerContract(final String identifier) {
		return consumerContracts.get(identifier);
	}

	Collection<ConsumerContract> getConsumerContracts() {
		return Collections.unmodifiableCollection(consumerContracts.values());
	}

	/**
	 * obtains the database contract with the given identifier.
	 * 
	 * @param identifier the identifier of the object to search for.
	 * @return the object that was found.
	 */
	public DatabaseContract getDatabaseContract(final String identifier) {
		return databaseContracts.get(identifier);
	}

	Collection<DatabaseContract> getDatabaseContracts() {
		return Collections.unmodifiableCollection(databaseContracts.values());
	}

	/**
	 * states whether the microservice is deployable, i.e. there is at least one
	 * contract and all the client contracts are deployable (see
	 * {@link ClientContract#isDeployable()}) and all the producer contracts are
	 * deployable (see {@link ProducerContract#isDeployable()} and all the
	 * database contracts are deployable (see
	 * {@link DatabaseContract#isDeployable()}.
	 * 
	 * @return true if the microservice is deployable.
	 */
	public boolean isDeployable() {
		return !(clientContracts.isEmpty() && serverContracts.isEmpty() && producerContracts.isEmpty()
				&& consumerContracts.isEmpty() && databaseContracts.isEmpty())
				&& clientContracts.values().stream().allMatch(ClientContract::isDeployable)
				&& producerContracts.values().stream().allMatch(ProducerContract::isDeployable)
				&& databaseContracts.values().stream().allMatch(DatabaseContract::isDeployable);
	}

	@Override
	public int hashCode() {
		return Objects.hash(identifier);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Microservice)) {
			return false;
		}
		Microservice other = (Microservice) obj;
		return Objects.equals(identifier, other.identifier);
	}

	@Override
	public String toString() {
		return "Microservice [identifier=" + identifier + ", microserviceType=" + microserviceType.getTypeIdentifier()
				+ ",\n\t\t clientContracts="
				+ clientContracts.keySet().stream().collect(Collectors.joining(",", "[", "]"))
				+ ",\n\t\t serverContracts="
				+ serverContracts.keySet().stream().collect(Collectors.joining(",", "[", "]"))
				+ ",\n\t\t producerContracts="
				+ producerContracts.keySet().stream().collect(Collectors.joining(",", "[", "]"))
				+ ",\n\t\t consumerContracts="
				+ consumerContracts.keySet().stream().collect(Collectors.joining(",", "[", "]"))
				+ ",\n\t\t databaseContracts="
				+ databaseContracts.keySet().stream().collect(Collectors.joining(",", "[", "]")) + "]";
	}
}
