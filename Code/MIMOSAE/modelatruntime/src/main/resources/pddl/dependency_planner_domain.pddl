;Header and description

(define (domain microservice_dependency_domain)

;remove requirements that are not needed
(:requirements :adl :typing)

(:types ; enumerate types and their hierarchy here, e.g. car truck bus - vehicle
    microservicetype - object
    microservicetypedependency - object
)

; un-comment following line if constants are needed
;(:constants )

(:predicates ; define predicates here
    (microservicetype ?t - microservicetype)
    (microservicetypedependency ?d - microservicetypedependency)
    (microservicetype_satisfies_dependency_version_requirement ?t - microservicetype ?d - microservicetypedependency)
    (microservicetype_is_in_dependency_as_dependee ?t - microservicetype ?d - microservicetypedependency)
    (microservicetype_depends_on_microservicetype_in_dependency ?t1 - microservicetype ?t2 - microservicetype ?d - microservicetypedependency)
)

(:action create_microservicetypedependency ;define actions here
    :parameters (?d - microservicetypedependency)
    :precondition (and (not (microservicetypedependency ?d)))
    :effect (and (microservicetypedependency ?d))
)

(:action add_microservicetype_as_dependee_to_dependency
    :parameters (?t - microservicetype ?d - microservicetypedependency)
    :precondition (and (microservicetype ?t)
                       (microservicetypedependency ?d)
                       (microservicetype_satisfies_dependency_version_requirement ?t ?d))
    :effect (and (microservicetype_is_in_dependency_as_dependee ?t ?d))
)


)