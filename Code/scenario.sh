#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Viktor Colas, Tsimafei Liashkevich, Julio Guzman, Denis Conan

usage="usage:
$0 --help
   display usage
$0 [--offline] [--yes]
   --offline: run the demonstration without downloading files from remote repositories
   --yes: automatic yes to prompts; assume 'yes' as answer to all prompts and run non-interactively
"
while [ "$#" -gt 0 ]; do
    arg=$1
    if [ "$arg" = "--help" ]; then
	echo -e "$usage"
	exit 1
    elif [ "$arg" = "--offline" ]; then
        export OFFLINE="--offline"
    elif [ "$arg" = "--yes" ]; then
        export AUTOMATIC_YES="--yes"
    else
     	echo -e "$usage"
	exit 1
    fi
    shift
done

function isoffline() {
    if [ -z $OFFLINE ]; then
        return 1
    else
        return 0
    fi
}

function isautomaticyes() {
    if [ -z $AUTOMATIC_YES ]; then
        return 1
    else
        return 0
    fi
}

BLACK=$(tput setaf 0)
RED=$(tput setaf 1)

SCENARIO_SCRIPT_FOLDER=$PWD
MIMOSAE_FOLDER=$PWD/MIMOSAE
AUTONOMIC_MANAGER_FOLDER=$PWD/AutonomicManager
SCRIPTS_FOLDER=$PWD/scripts
DEV_GDE_FOLDER=$PWD/UseCaseEdf_GDE

function cleanup() {
    $SCRIPTS_FOLDER/clean-k8.sh
    ps -aux | grep minikube | grep -v 'grep' | while read -r line; do kill -9 $(echo $line | cut -d " " -f2); done
    ps -aux | grep './main' | grep -v 'grep' | while read -r line; do kill -9 $(echo $line | cut -d " " -f2); done
}

trap "cleanup; exit" EXIT

if isoffline; then
    source compile_install_mimosae.sh $OFFLINE
    export MVN="mvn --offline"
else
    source compile_install_mimosae.sh
    export MVN="mvn"
fi

echo "===== configuration ====="

(cd $DEV_GDE_FOLDER && ./setup-gde-common.sh)

minikube start

minikube dashboard &
dashboardpid=$!

echo "===== end configuration ====="

echo "===== starting daemon ====="

cd ./AutonomicManager/Executor && ./main &
mainpid=$!
sleep 5

echo "===== daemon started ====="

echo "===== starting scenario ====="
echo "===== start create AI plan ====="

echo "Choose the scenario [1,2,3] "
read scenario_number
while [ ! "$scenario_number" = "1" ] && [ ! "$scenario_number" = "2" ] && [ ! "$scenario_number" = "3" ]; do	
    echo "Choose between [1,2,3]"
    read scenario_number
done

if ! $SCRIPTS_FOLDER/waiting-for-response.sh "Create the use case System ?"; then
    exit 0
fi

# if run online then delete the old microservices images in minikube image repository
if ! isoffline; then
    if ! ($SCRIPTS_FOLDER/clean-old-images.sh $scenario_number); then
        echo "${RED}-----clean-old-images failed => creation of AI Plan failed-----"
        kill $mainpid
        exit 1
    fi
fi
  
if ! (cd $MIMOSAE_FOLDER/scenario-usecase-edf-gde/ && $MVN exec:java@create$scenario_number -Dexec.cleanupDaemonThreads=false); then
    echo "${RED}-----creation of AI plan failed-----"
    kill $mainpid
    exit 1
fi

echo "===== AI plan creation finished ====="

# we write all process in temporary file
ps -a >port_forward.tmp

# initial minikube and kubectl process count
initial_minikube_process_count=$(grep -c minikube port_forward.tmp)
initial_kubectl_process_count=$(grep -c kubectl port_forward.tmp)

#current minikube and kubectl process count
current_minikube_process_count=$initial_minikube_process_count
current_kubectl_process_count=$initial_kubectl_process_count

# each port forward creates one minikube and one kubectl child process
minikube kubectl -- port-forward service/user-service-app 8082:80 &
minikube kubectl -- port-forward service/authentication-service-app 8080:80 &

# while the quantity of minikube and kubectl processes does not correspond to the start of the port forwarding we are waiting
while [ $(($current_minikube_process_count - $initial_minikube_process_count)) -lt 2 ] || [ $(($current_kubectl_process_count - $initial_kubectl_process_count)) -lt 2 ]; do
    ps -a >port_forward.tmp
    current_minikube_process_count=$(grep -c minikube port_forward.tmp)
    current_kubectl_process_count=$(grep -c kubectl port_forward.tmp)
    sleep 1
done

# finally removing tmp file and sleeping in order to not to show the mixe of gibberish in the terminal
rm port_forward.tmp
sleep 2

if ! $SCRIPTS_FOLDER/waiting-for-response.sh "Start the test of deployed system?"; then
    exit 0
fi
echo "===== starting test scenario ====="
if ! $SCENARIO_TEST/scenario; then
    echo "${RED}-----test scenario failed-----"
    exit 1
fi


if [ $scenario_number == "3" ]; then
    if ! $SCRIPTS_FOLDER/waiting-for-response.sh "Patch the use case System?"; then
	exit 0
    fi
    if ! (cd $MIMOSAE_FOLDER/scenario-usecase-edf-gde/ && $MVN exec:java@reviseminor -Dexec.cleanupDaemonThreads=false); then
        echo "${RED}-----test scenario failed-----"
        exit 1
    fi
fi	

echo "===== test scenario finished ====="

echo "===== start AI plan removal====="

if ! $SCRIPTS_FOLDER/waiting-for-response.sh "Remove the use case System?"; then
    exit 0
fi

if ! (cd $MIMOSAE_FOLDER/scenario-usecase-edf-gde/ && $MVN exec:java@remove$scenario_number -Dexec.cleanupDaemonThreads=false); then
    echo "${RED}-----creation of AI plan failed-----"
    kill $mainpid
    exit 1
fi

echo "===== AI plan removal finished====="

echo "===== scenario finished ====="

exit 0
