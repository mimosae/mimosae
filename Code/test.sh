#!/bin/bash

# This file is part of the MIMOSAE middleware.
# 
# Copyright (C) 2021 Télécom SudParis.
# 
# The MIMOSAE software is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# The MIMOSAE software platform is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with the MIMOSAE platform. If not, see <http://www.gnu.org/licenses/>.
# 
# Author(s): Viktor Colas, Tsimafei Liashkevich

AUTONOMIC_MANAGER_FOLDER=$PWD/AutonomicManager
PATH_TO_EXECUTOR=$AUTONOMIC_MANAGER_FOLDER/Executor
PATH_TO_MIMOSAE=$PWD/MIMOSAE
SCRIPTS_FOLDER=$PWD/scripts

if [ ! -v LPG_PROGRAM ]; then
    echo "No LPG_PROGRAM please export it before executing any scenario"
    exit 1
fi

ignore_list=("common/" "config/" "docs/" "mocks/" "local_mocks/" "proto/" "server/")

isIgnored () {
  for i in "${ignore_list[@]}"; do
    if [ "$1" = "$i" ]; then
      return 0
    fi
  done
  return 150
}

cleanUp () {
  $SCRIPTS_FOLDER/clean-k8.sh
}

trap "cleanUp; exit" SIGINT

echo "=====Running planner tests====="

initial_path=$PWD
cd $PATH_TO_MIMOSAE || echo "invalid path $PATH_TO_MIMOSAE"
mvn clean install
cd $initial_path || echo "invalid path $initial_path"

echo "=====Finished planner tests====="

echo "=====Running Executor tests====="

echo "=====Running unit tests====="

echo "=====Configuration====="

if [ ! -v GOPATH ]; then
    echo "No GOPATH please export it before executing any scenario"
    exit 1
fi

export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOROOT/bin:$GOBIN

go install sigs.k8s.io/kind@latest

echo "====Server protoc generation====="

if [ ! -d AutonomicManager/Executor/server/proto ]; then
  mkdir AutonomicManager/Executor/server/proto
fi

protoc --go_out=AutonomicManager/Executor/server/proto \
	--go-grpc_out=require_unimplemented_servers=false:AutonomicManager/Executor/server/proto \
	AutonomicManager/Protobuff/plannification.proto

echo "====Generation finished====="

cd $PATH_TO_EXECUTOR || (echo "invalid path" && exit 1)

echo "=====Mock Generation====="
go install github.com/golang/mock/mockgen@latest
go install github.com/golang/mock/gomock@latest
go mod tidy

for dir in */; do
  if isIgnored $dir; then
    continue
  fi
  cd "$dir" || echo "invalid test dir"
  echo "=====Mock generation for $dir====="
  
  go generate
  


  for subdir in */; do
      if [ ! -d "$subdir" ] || isIgnored $subdir; then
        continue
      fi
      cd "$subdir" || echo "invalid test dir"
      echo "=====Mock generation for $subdir====="
      go generate
      cd ..
  done

  cd ..
done

echo "=====End Mock generation====="

echo "=====Starting Minikube====="
minikube start
if minikube image ls | grep fs-v1-1-0-image:latest; then
  minikube image rm fs-v1-1-0-image:latest
fi
if minikube image ls | grep test-service-v1-0-0-image:latest; then
  minikube image rm test-service-v1-0-0-image:latest
fi
echo "=====Minikube started====="

echo "=====End Configuration====="

for dir in */; do
  if isIgnored $dir; then
    continue
  fi

  cd "$dir" || echo "invalid test dir"

  echo "======Running $dir tests====="

  go mod tidy
  go install

  for subdir in */; do
    if [ ! -d "$subdir" ] || isIgnored $subdir; then
      continue
    fi

    cd "$subdir" || echo "invalid test dir"

    go mod tidy
    go install

    echo "=====Running $subdir tests====="
    if ! go test -v -run Unit; then
        echo "*****Test for $subdir failed*****"
        exit 1
    fi
    echo "=====Test for $subdir succeeded====="
    cd ..
  done

  testName="Unit"
  if [ "$dir" = "orchestrator/" ]; then
    testName="TestManager_Unit"
  fi

  if ! go test -v -run $testName; then
    echo "*****Test for $dir failed*****"
    exit 1
  fi
  echo "=====Test for $dir succeeded====="
  cd ..
done

echo "=====Finished unit tests====="

echo "=====Cleaning====="

minikube stop

echo "=====End cleaning====="

echo "=====Starting integration tests====="

echo "=====Configuration====="

export KUBECONFIG=$HOME/.kube/config-test
kind create cluster

echo "=====End Configuration====="

cd orchestrator || echo "invalid dir orchestrator"

if ! go test -v -run TestManager_Integration; then
  echo "*****Integration test failed*****"
  kind delete cluster
  exit 1
fi

echo "=====Finished integration tests====="

echo "=====Cleaning====="

kind delete cluster

echo "=====End cleaning====="

echo "=====Finished Executor tests====="

exit 0
