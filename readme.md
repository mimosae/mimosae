# MIMOSAE

MIMOSAE = MIcroservices MOdel for verSion mAnagement with Evolution graph

## License

LGPL and refer to file `LICENSE.txt` for potential exceptions.

## Content of this directory

  * Code: JAVA and Golang code
  * LICENSE.txt: license of MIMOSAE
  * README.md: this file
  * Documentation: publications, videos, etc.

## Software prerequisites and installation

The demonstrator and the software has been tested on a GNU/Linux
Debian-based distribution version bookworm with kernel version
5.16.0-4-amd64, and with:

  * JAVA version 11.0.14
  * Golang version 1.17.8
  * MAVEN version 3.6.3
  * (optional) LPG-td version 1.4:
    [https://lpg.unibs.it/lpg/](https://lpg.unibs.it/lpg/)
  * Docker version 20.10.11
  * Minikube version 1.25.2
  * Protobuf compiler protoc version 3.12.4

## Install, compile, execute the scenario

For the installation of dependencies, please refer to shell script
`Code/install_dependencies.sh`. In addition, install the LPG-tp AI
planner and set the shell environment variable `LPG_PROGRAM` to the
binary file `lpg-td`, example: `export
LPG_PROGRAM=.../LPG-td-1.4/lpg-td`.

For the installation of MIMOSAE, please refer to shell script
`Code/compile_install_mimosae.sh`. It includes generating protobuf
code, compiling Golang code, and compiling JAVA code. Then, for
compiling and running JUnit tests of MIMOSAE, execute the command `mvn
install` in directory `Code/MIMOSAE`.

For a demonstration, please refer to shell script
`Code/scenario.sh`. To run the demonstration in offline mode run first
the normal demonstration with the previous shell script, the three use
cases, and then add the argument `--offline`. The normal execution of
the shell script `Code/scenario.sh` will populate all the necesary
images per use case to run it offline. The shell script
`Code/scenario.sh` starts by executing the shell script
`compile_install_mimosae.sh`.

The demonstration in shell script `Code/scenario.sh` uses LPG-td by
default. In order to use the
[PDDL4J](https://github.com/pellierd/pddl4j) AI Planner (instead of
LPG-td), the scenario in class
`Code/MIMOSAE/scenario-usecase-edf-gde/src/main/java/mimosae/scenario/edf/gde/Step2PlanDeploymentOfInitialGDEConfiguration.java`
must be adapted to call
`mimosae.planner.computeAIPlanToReconfigureWithPDDL4J` instead of
`mimosae.planner.computeAIPlanToReconfigureWithLPG`.

To clean the directories, execute shell script `Code/clean.sh`.



